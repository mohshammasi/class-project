// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sat Nov 20 23:56:49 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ platforms_mem_sim_netlist.v
// Design      : platforms_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "platforms_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.5432 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "platforms_mem.mem" *) 
  (* C_INIT_FILE_NAME = "platforms_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18256)
`pragma protect data_block
w81dvS7CJJMCOG6pM67AqXU2wYMYGuFUG+Eavo+axHyJWDZ4winEIfBMMaLhDApyxiUwvk37y7LS
k6/aGnV1woLcrdM065dHu4lidVYekPs0/rhBR7tedOxF1jpMwmoGaKSpDEZRP+V/qSS5HS0By4EJ
vz/cj2n4wt+R6QEj8X0GYtx9o+iImSP8TJKF/H/+t/a/yfeykzCnG+pUcW5Rx5zgBDowEtWKz+dD
4NGWDC8A2hUJL/EhEs7zvFYa4K6wt89q+q6TzVzyZelBS5G7XVPHPXedD4dXEIGOrDahFHjyw9ac
B1ywzSzpjfn04FenlGLYHVOQHz5fLwR5qPvHnU5rFF6cGbcLjPwD8hAnL+3qqeFCbbVmWkLPNpZ5
Gqfay5Va6K/NdNcx4FKHWLRT+t4zqkueqrY6rKg792mhdLBqgM7tf+rZh060j742soZCxb8OMoC+
TdSfyktqORDnlQDinVbsw3ziZaKfXOYL3xsl35DqnraX0ibm+V8smUlCaWcfrDP9A1E2ByIfbpro
6dOFQyVguAlJsDUpKGM/JEDBWU2SAsWhhIJeL7G26NSG1NdRSHUwn4lskbZmWs5QPE906kGqA+n0
sbnG0HYwjTqal3InlZ8uI2yxWKOXBPQCoLhUeORzgarfSnlgx649FkyRFyl7FQ7Fy6dwLH5Zr8TC
nX1DPy86UBElNW050KOIhHdz2wZpnDy6o7tTDnz7uD/b4QnxbMWGcREVus1/gSw34IS6pTlx+ykZ
T2ZBnPyKswUrRkdb72u1d+uvFUDPxSSG4K7GLZ8VL0NEh6msqcmFpRqBMg8B7BVVF5OFhLzpaCAK
zpAsIPi5ZXolBSeEcTehcq4KLje8cJW9T/7P1HY46sLQkjTWIip8xMo7QGYarXmNXZ+ka3yes1hu
PCKYNJmO7oy2G5wsgqYVI/4oqkNsYUlFARm3YXc7Cf4jf2JGHiUmgor43Yckx/94W99hotVSxby3
NnfkmApTgmZRAhlpDqdQU4xvIVwGooDcR6tMSxaB1PnMVrY5QC2M+7japo+uwWfsHPvtgqFnqCtB
JVwVTbRHj7DJvLWt0rIWZs8K9eFwvLNIP2JCLOemUX0Ld3m+pDnPWVL52rufUHb3IsR9TDjhnIgD
EbpuA1k2Awrqlsuchim3Ai9Kh47RLycQPg4Fv78JqdZnEqtrks0UOi+3IncNkjV6YonHxeZhi1/Q
zYvvTJSxA3Ye5D55TZQOAz7N+mfoP14zEQKfxcDqY7Bsmg/PgvZPcnq5oRI+yE95kY8+SlE13nWW
GBa8ckIwISPKB393isGIWDprVfjeU4pX99/rWNvxJaSa5JUzFetqk+B2Ao3rXFLG8MnQQlRQMf8y
ccEeAiaPkGA/VHyrDx9jlq2CKjNI/Qo1sfuTq66t0wAS10L9/d5FmcO7RiY6mBnoPSphTHZPjZEP
X4J4xXC0Q4nLDgRn04rX5GKYmH7VUncV+atCjKT0t3TkzAU5zTrONELeGk6MIY7wD8EdS2nXjTFe
5ns8cg5W+f/UdoCi8qL9X/2reZyqJlfMK573MrlVbiqJgCYi6xX1EPBMviE4ZIR6XL00OK0vsEE1
R9EfddLhgDs8IwGHRVevkna7MRyA7IKimngXRuw0ZzWd4jdVcZ80g/H55ckMKmSOapqHdJbMtPRE
H/t/Cd5b33QV/SYOPIFEXy4KGTmiOv4fxIRRv7eNl/q2b4e1lBFxrwYMy4AhS4DgWOBf85/MvzOU
Q4X5jmVGCJhfg+ZDQD29IesFnEYX6iqodeU/64yzTR69DDyB3SSbMRkGweRW2dP8x/+dlTcmdDLd
IXR0QAkRW8zlvwwVTxMxeVwGgjmMFYD+TQEWKNowoVWfkoKouwOn7UWTF3kmkdgwY8/0cNkNbH5c
LvPnXgapxq+oXERX39sIHAddjSGI3bXcIoDmPl/UrLe7almJOEpiVXaCu3iJhBh1aZ5P8FlyMbGR
UNznVF5J3kOdLyJI02vkIQ543su9EsWH7qRnJU4C1gjY5khD6Q0EC4bVFgk7Ci9DlfA7dZxkKudb
Nx9T4ejHF5LgSLkq6S16QYDnsDIFIMLuJ1RAXqOaVZq5l/RKrLoLELAmDBPsv+mtfnOQ0A8NAW+n
x0i2O6pGEYoetVZ9jlBlXS1yhGOdN/nGeuqqyw6Hiu/ayBQ/tHGS6FF8bxY2fkCTnvjkDuz055qc
7Roc/uHO3UtBsXw/NKgAC3n7gW2F+1rzf6ZB0yn0CkSsZP6JZE2L6D/asiPBQa3epZ+kVuWrTg6h
dXwYejPZOOe9nOrMgvOs9hrsLpQqiYQ9dF+cRUzzn3Jj8Zo6aRJj9JxT0zrQkhZc515SPZ7zhPWQ
3nhzd8iqlK8RdGAisuRqEmPM8CveIQRNx5P5Vbyk/1WbkgvSPq/akhkbQ/DmkQkzvdFeOg3CLWhC
3eqJwKLQHAJMHU0s4f2WbS9qlv1s39iz0IFw15IZnEN13f45vexIJvYFfKES8PhPU+dBU34zs4lZ
sIIKYXr9WD84CAuOzLybJjJUrm+wbLd/4Uj2n0/nvP8tnU0zRf9P+LRkoqsTvUnZsvzMUo4HcIqY
XD27dei0w1EcOly5BDN0IiOR+UpQjSUlXGKyEalFaDtr5yc0L4ddcqTwD1W9sWD4sCEXajWC8/c6
M8q/IZzGFoMyCvGma8Teshse8g1tyg29bIlYiN+Te1N4AG1ErYE9HxYNDGGcJJl+nCKG03RpgmWK
bRa1zxfmg8NgBKFfMhX6YIMH7FQGQ2vLeec8QE4QOGI+xIb0dt0qnPjZPynYcUby9cyniMdUCind
HuH95moYzrN6BODtWRHFJpTbuD77v/3180w1P7LXjPPbmKOrdAXRtaRVrlNdSLZCt7xM4df28wMt
4RLmQ4IbWIGHfDO1sBejkHd7hSUYWvnf4m/kPV9pe0YWe+0Vh4wdpPhbi4IUqRxez+HDV6CVOhXm
BFM1NCbxCG2zB7k+Bpl/sjFqMFjSrBNVPwOMzwMsEu6QVCp3wQDzbwWJ3DPhXJ62pHaiCGJct48M
n4OrkAKtfJQZnHck1KOT8B+WmgLWXuuKo6p1L6IkzyxNPjAOJrBaFaTvuCD+kV5mT8hDP0MbQC8g
dfN56zO/mIApCV3tuChvyVgv3cZkQGE9j22azYn7MwR1+bVXf5TwpLWBxx/rXuHLIoPasHMXM5iu
TtBs0/CpNcWYSJ61u1ejukOECzTyfxpnnNcMbmOS+5KLdhwintuWe5CNnHienWTvJbsZtPAnPXrs
5LorSlg6VSPWVWVzD98SEjld7yCEs+/RuUrX5FSN/uKTQ05vbFkcdYUNAqpOciSweAMW9BZa0cF7
SP+cG7mTu3TLQ6drMM7tcGKOgdpr6BidKn4YpXCfpAKXN4Qk8gteEprBqo5po/NQ1jh1efelr98Z
xvY8goule0JwyJcoUAT5agbCzuN6b+M8hsnUw1bBvZPOntmkpEJpbEZjBm9slJ/llNtF9PSYNEBY
/Z5QDCFI40/svFyg/SVb2imSOIEKWA9qPqqrW6Fnwh50SRsxijVa556c+GYfhOCrMPQfO8MKrDBy
M4Dot3FUW2VQ6rchifG6KADLkHWhgAmloyrcxO6M4IYDzI9TyQrCoyC0YQpls6EDA3aGJG65p9T+
TfDH+VfEMYhYPuothJEo8/JwudLfuW87JvwEbDbJdtBFyxt3CjNhlYxZofLJqO6w8YZG/C7/BfO/
W0wXPczTXbi8gKL04aPRZ2LdxyziWj9lYxkLSCyttSTA7wvWKfMIS1I3cE6LNtOMxQW48N+6Z8a/
i4RCYqKpqlc71TEkZypwTNWB3khmhxwd45KItMowdGhNuuMI/hkkHxW9tPbK5+6JM/vqc6+t+8xJ
n+2nnkGh/gzESVWxcvOLnlX4DuXqspO9ybDlWg8GQZmkl/uGBUIB9og3pRBPYRvXQ6Ew1BD9v9o5
OJz1ZhiI0ZRG1k15xmGVHo5hlMqPGp9EN3L1tocH4/K+7ZtqvSfNfBaqVh1dFeRf1hfZ2/NjVLtV
9lznqSymSzmCx/tL8D/ON6+gEbYcJDN2Ay9GCbL65oJv4jqp8j1aySmM1smkfnj1mcDbpt6A0Q2U
L5Wj3Rcl/17aGcOoJim44Ph+l5Bm4mldffi+lKZi/Nzy8R5K7F2McegsNWQhkJ9zGcxrly6Vzbwe
Hvor+1JRm5WNBW9aGJCzVu8MvDnC1HX9WpAcNC6WARhhV3S7hCZsQCuGHJN5Kve9L+XE1OfCydEI
zW/F+YjGZ4kFXz3yGmWeu8UU29jxue5k5Ls+xFa2bYxKBY/pVPVTVpAiKhnpd5ijzh51JVepLtVw
mia6khVb+X9PQum6epsOVpdE8qYj2xnsn/X1myuZ9fyws+G5sxQfwmJ7lbkQT3laIKnaCDH0xH8/
ynjPZDcbc7NHEydeLm8KavI2oL2ydpzhBGJjprKKlfNEBU4j4HHGc1FPQTblLkQbG1yFq+6grvA2
wrfXKGynwonvUpDXJ809xPzApd2HPC/NV+3VYOVmBiRSegY1RlOOys2du13IeIbMVTx2wiLB4bge
TUbuJgUZpY5SsR9AyjIjiRs8toslSfO/JCp/FU8jaQHXPaa1d5W8izS2zJWon53lUO9PTlPbdcit
3XxPdY/Anp1d++5fTrb6NgAP0eR9OTRMPoCwxXDApwDKVZki47ay+86Ma/6PFvCPo6U1YG6mrIq0
EjDROVzbJ42YqVPFyJLy03QIlFsx/b2TowURq45ZquwofnS+X4YsT+/AXjemQ7zPO+htOBr7hOVc
Wvu9t/RI4zmSciKT5e2TabdHYoNnZT4XVmOnwvI5bd6mrWlf3Q6NR/r8kxy5+2esIE9vOWBegEaK
L71lzELe67wQa0FKJFvv2ybMaX4Cft05tLp4kngDXycS0zJHaD7dwE+k94jNkjsvhgotZkyUDrzO
mrtU6L7zfLqSY8E+3P2YHZY0uUEYn3LiL51q6FM/30n+JMLlSAjWaIpu+XduFxzM7MZcx03AL99B
EvVRufP1U8HahNMqWGH0npThTKwQXLYxASjgdM2KP5gEz81RlgfHkxjK/6481VEfHYM0u21qKufA
6/GfYST6K55FQteMpXZPUCer1U47OtG5RZVvHH4QO6gJH65tFXM3a9Tc3EGJCM0fjBHl9x9yvEB5
zzxHDJgIGJ/qeW2UrtgPaDwckr0YAFehV29UMG/Yz5++RHUCfuVS2cRTtgCZWFRdwvL2fj4jT1dn
udGJgkINyehhoz9xBUlCf1+R0N+up/8QmxZVUu8gu2hDDcmA01HQ2QK1QrlXkgfXC2GgDNj/zw+p
GmNvO+fO6aLqgUG8T+GWRCa/3Qkh9z1I+0znbdnu8nM7bjtFPxxxMWQLaPzPmfSYA14PL9MEsHr3
rRxfR4aA9xK2wCXvL+uyrm/sj5yggGMNebCzxFzIE38pjFtc+SiqO82s72e2InL85GWJWQtXOHcN
4QTgIOHEDRkrFGsdzbuVdeVUcZ40ufEHCuIRPN3vQO5uJt9DF+MusfshVBpYz7nS1AQgyxEEVQdg
EclR3mk3p4WESbzlSN+TUUMvatQKX2YYZD783pi8XX6EV5OnmfcJYwaKWKJVTgPvUU4D6nAwR0DX
vdf6aPmzsCh6JWwsIEfWAPbykc1ATlsbm18FwG5F1Yjkj47UfEGQQaQPq4HRK9rmoN0SWUJuCdjp
vRO18WXL8hLF5SGQVHcahoiq2x6u40On8xjdH5z2PHHUGIa40Pz1kgx5SgR0TbC2gXOtnvjzd8vt
kf40U3T8qik/DcSGrgQYutRQ/TMNfh5fCZjTY/Oc6LhZjfsFE0+tUSfmiFZlyV9mOx9W1odUuKMH
D2nLdM98QhYNMJ2+qBZfWuaoVWum/pSaGWe1R/4opN+MxBYFr51ZbGfzVkOtBtQ8pmXuBGVAp4yi
vJoxDrjk7m4zm0bX8Og6o6GdJVnyk1EpBYyRl3v4FAIvSEwQIQ4UR4zTUUwQgx+LrjkWKv/G5EBr
VYFkE0ZKwqylFYhdfJfBfh4XzpEkXgaH3Je0dsxunX8/cuZSMZDAEAhmoRgmn/pu4PhypZIvcbFh
L1LWHkb8mKf/W7LKaCVIOqmBJCaDzlOBAZf94hiXMCCWGNQe+k1/oXWnl2xJB3enpx9nNmnb/Uam
xOKnuPU0B9hvQT9Ua07uffhuXncU+UGX5EvZUnWA2sU5GhLuayzbK6j3YVpuswDuLnSaM8zaYezg
75JatVkvnSzIvqA6mNSWDJc9uv/aH5imgMpGCk7McP5bAE7tx8e794FvfbrhSxujAPHjJ1ZVrnFt
ezZF0eQgWgSzxH+g+Nfol2jcih/25nY59ZNukWPh+q6J2qApKuQ9a8xcyq9gj3LBeiLiD6o5FhMJ
wXbjkV7znBmx5nR47q+7B0GIa+N/Vmau/w4FEeSTECreJTnzXimnUMeYvF865/NsCPiqvIhzgNCl
T7Lso7kBO+iJqZWLdNFO6BjIhcush8c4Js1OkdbmmSuub3OZZmFKiuLZQL5gMbo7R+yKtmyxNu0d
taH5JNr1Ol04GylAUEJx6Donf7ohqJ4P3WLEWFfzLYUh+pQMzhJUf9rS0OV5ZEclwPvg0GR8k1LL
SjvFTfwKybVv9Cq3Up9A4lVxIpAqhVdpDuu2S8sKfoHSUR6dmduyxURk0uuglWe/IHrnve8wBlCd
yMiJ9tqzrLcFZXjdqpxE9mnDSr/0Mm5ZgJjg3zoEg9JqvPjhbbl5mqPvbYk4rYtKqiBNUJyJAQDO
DK08r3cyiznkwcYH2P826WV7jEdEiwcjNbu5+yf4Ty5yn5EQ9Atz3VKfIbk7QJyLt5cRSAPXw79i
Vsu5ehq/2m4IumPj3O1PtkGGIpC+Gm2NQDsdPPIvPe9dPGWdYqM7CGYkRCnMQYg6/CI/fBH/L98x
4e9TH+nVEMblh/um/YjjNmbHV8V2pXLCK+M2u1PGv/6V0KY85nOHIa+JP7r5OYdcVFM71H6Q2bOy
QhlzmYxFKK6nZ/0nSuT89hcxBLiYw5EGs8Sj+a53WFOYzJZPJ1lWqiKi4LmXOUk4fIF/ICH/LR1K
UKWWuF1DO9pQfP21zf8ZGkrmBZ/yU1tOvL9mQxVuBOXELlkMvVf0NlgQO3TCAvnKbtFibTOFEAO8
jatLp6VZ+qxQQCmTXubSsApqzbIxxEUL5M7WJGzH5DV0sGvvae2sdh4nzeBswMYlsI/PuMmnuFyX
LosFh8/U3Jw6u0MtEP/SoSaRq4bu0GX/NTb/kN7Ggd/TzbHqyV7BnTkWXQ5aACyb2JxzfJY4PGjQ
BeDoHLKHhJocynXLy9516piQ0cK1Bi7WHTMX0pXpnZaw54ZYb5HltSUe9mMyMFn7ibac4W4YXsqR
XxFHtuzXIOc0i0Zq0xUHU2KPehr/ZBSiIluvdj9CymveZKGoALg3+eQoohyPgXFQEWz+FO9xSobx
2twUE7BjSf5w/w3Xwb8IWziIya2eYrnZ/FsJO2j3Js+rNMY3dYLTVB0KCB2V5AhHs9IgDwQwSdnD
Yb30JvNkayWfAlWGDg2bcn4mXcYmmqJ8VI+CdkdV/Bg7BSdS+bIb5CoywV4q4qoNnBqFL4apmEoS
ZL2aGgdLiaR67QwyN9m811GsLXrR+iNY4sf1RE0H3zg4KnjxuZ9yjYW6nAo3zATiDuW1m0/sMKek
JpMb2ul5B3I1w5cTy2+jlHvLQ1EyFdf19tUfnXD7Gt/Syn7b3S5kEEYDlOYjMWcNwmL/+1nvqmMU
zbkAhybgG4zbCeONzVc6IBWPcM32QDU/j2g6ZpoZshobEXry87L8vhTG12AoJNuevtNdv4d3esRD
q7Ockvo39B1IP4T7ylehzbVDKTTusdtnOf5rITCa6jcFegEsCP3Fs3j85Jj4DFsTvjuA+NT3I/zF
kWVew8AnBKG013GZ0bQZvQ29FNIJsy3mMvE6yTF8+crvPgoDs7L5cXZkaUqvyt/L8ma0NsB0M81u
GksLvtOE1wucUBdA2SgmaCeC47RjUNzwDGnfWw/qqhYbIVvQ/bvszY1mgm4z0WrRoTBDRyN9rnHJ
u3c5ostVj/aUjw4MVF/46Q48u4PndQDdCktcJnA1hcEgdMt8t00VKz3m79BQIJzAFPzDctOFSLBc
4zRyfVHDpiX4bSr+ZQocPLBsDCdLcBLrtm0BhiPbbUeibvTC1X0RCsqugvLpMViWQggleBDtIc01
JjrV/mSEHuf6J6Ps/Vp39BWGZTwa5/58ieEmV3mBDZEnQEIitMLVOLkvUCP3nC4OCPoL85H/fro2
NhxqyqXHGkRw2Q1BmSrEPKtyaWTv4MdWcXoNmNBGLEDTdI7wCpfwUXGsHjSmPNMXV05KcYXRJNUi
kty7ISsfDm7/YeyL8FN45WKgRTkN6c09BNO7EFa554SVEdb0umJBUMw2j019SEJLKxFVDno0DX46
HwbACTXNFmHXycm/s4aIctuRWcMkxw68e34k9vKCV/eiiArVQrd4l6bqXobut3dxhisxX9GvbVef
xcOONvlC8jJhO5T5bvMh7DzNE+ySO1nMZGE1NK4MUFvJ1A/163w3znL1hbqnIXb7UiQTysRJOA8A
xYw32ndPWNLlLQKpktzX+c+ZLIDtF8VH8XTHtyJZODs/OIuRmTv4Z95ak/GmwJkERZSjqL0VFCKX
DKx3zAETkwhW7/dw8GoVcmqNKQOLkOQl4He3vjR7ipMOnNJ8JyJXydMBYNqbcp5H3K2L4YCmWFVk
YOyFBzR3Uu5X9vdN5NB8dcel5F8/8258zGJdTgtj3RMPE/YGiXAtxVsuQVjppZl+xQkd3RaZ25kn
F/uiUYYozc5v2U+Zt/NcS6mNXvSx0LSXEcksJG2YgXdTgfCsDdCbF7Igd6cStgaywyd2lOzzTZL0
SNFN2OXSikRAwVbm1StGWddhk2k/faICeQsDWI7VT+d2EJaoyyw8Y90fdQM4j5KdCEUykut3nH8K
KvxhqkFzlAkg1GwddQK5Ixz4P72Q6vx67AhcYNPyFzic9ls1eoiK2a2I/dy55VdFKNtpPPOO/3kz
gndU89/7IlYM3n5rhOoPuOTycc41EEBvg031SCKfN0OpOD/Gi2nulSLglqe8qus9HA7uUeWnbnXq
Tj1UG4+JT5zQ5S4f6mZ+1K9K/LeVUF99KHX0ce3ixqfnXvPCSNEv9flO5ub+RiHgLKEw5V1lhOHl
JafSiyh5Epa5EuRRp7c0xzVHN1L4+KWo0ohlWly7olNK2kb+QdCXooug6wN9wJrJO84xtE+IAsp5
S9SqOds4BHwWz+3sbzZALcN/EtBAA8P1PlJWX9D89MVXTZN2e+zxJ8469AAI94lJJuR6zsjIsiqX
fEf0h+zY9ISolDhwtucL/GYtpb/pAx/rtLEEIBJvAT3hbeCKu3FuXgDEtCqxfii4QNot3D9jQsbe
DD2BKJpgwt/LgY4nzlx/eMb76Amd4MhshZzR0b4qo/eZwUylw+CsudjrMGgXpJbVSOZKcvhA9BVT
Z3e+KabalxcmNkuT6IkITqhJTXqBdgJWaD1xzkwI2q6TMsd/PiNtfVN9Ev6H7rg3zTf4UaQl2cei
lLPjCEBJaZHjSzA675I04VtvpMrH8pzm8bifnoWutCK2bsggZFlXmmjpT0FjMSBP6pmwkpbMiJJB
/E1EvqLc99hYcz6M5QOx//CYhNAPn8TbEf7Rdr6M08Ii2AglQTyjQRYVBsAWfs81j9T+XUEFJhOi
avHh2Cj0qSCvCdraN8+UEODXL+uWql6JUv6vPdel0fulNQ4tLHTyPYXMbwo8+MPMGPRPiJSif8DW
xPKhOPCvc0ALxVSbWiCtnneCKvF22BErdD0Ms49QQFQ4YNcNFDOcbzZdh63+AkcEhuqRGjcPDn3s
1BH/5GEg4LnanUWNZraBPs8QkcNMVivkA9M3dEW3uHSWrZqzirMUyn+xzj/1UDICMexzI3P5i86R
uMgRoa0O0EbgOAw0h3hor3cIPUm8KwrpLzt3SOhzaGqTGMy5UPYhMv4z4NrzFUOQKcF3i2yrKXWY
jM9ArUcXQJq5sTnnofJtjRHr173rv/Yl0h6HnH/f3jb57hwAmb0yiGRilDsnEaOP9uHNW6sUf8UW
o8r2S6EK8XexlswxuAtG3nYzFsaXByXYmSNKRLgcUIYf8pkK1lIbSlMaeUN0J62sOmnWo3dxTLYM
sjO7+6kh0QJ0iKt8HP4YcqctvmIm5bOi2kCyEFzqwxEicSk1fkcyn+B4Oat5cZNy6Eve7cTTz5Jo
45EUSP6c69qL2xMTa1dRLwXM9P+i9LIQ1QvuGFj0gaSVv8s/UXKktcvILSO5ZRkrspq6qoi4vNvA
SVbKg+QqlEQIKQH548WrdMckAXw3b+vC2AvUfwbS2ZEiBsbOj9rmnJXhNm514RQewnXxgd/CIitA
HWxTIWT2EChFaFS7ZNn7FatkyAQ7stn8e+61v1Fw6zFTGTQ19kWh0nuWaDSc01tgRQm+gSIc8Jnt
OdAhd+oE67ffgqhljuA8XgA7hYhBsCIj6pUxtr2ofz2zNvMJo7NXXwjsxQcVIZMYR1TB3OgQHcvL
txRIgMfaWYyTUzrJ6NTg2t6cbiVaWbblHuPHQJbh0KS6lBBNdcJaOpNG9hgz/SECCu/2u75ysPoT
KVtNBQjcufS4gUMk/rAU1CeVEAhV4rseMsXIGBMLt/dTFeY0nPH8Ym/ivfmWnjeiGJ4hT5h4sd6l
ebQDEnzNnNHRzoS5Ad3lUms5ZP4ULDwstptatEV5Bm8lCRRhL20EJhwm916LLnYRkih7c5oQHZ+o
3YWcJHYPOK01dgyNUw2dpDdlnzJ7VfJ9nKAEwklTXom8ST+ek3mcW0TPM0JDeCS6ayvMpME8LYrY
BTQO6UxrtPN5ZyXG+BxdEE3+g0ByM98kQou4/aHR1CEHfbrMTxX7OZc2fnYiz+/89T6npf1euNYL
aRoJ28LY60DJ8/spGibsGx/07LXoJYDPubJ6YqDpMhPb4IzcUm5ZKuJPCRKvFPKrLcKyK52EP5ob
Km85Cjhh2pPTmAZvS8ZFNfCRbUaayQF4PTemYYEDw2iZA3ZQd/sdrKlp/gmeUMMeIvtHrlGh9F4I
xdWs7xHluig9zBqQuESIisZK7Uwg5qqUTAgwAW7NKSNwBcpunH08e4O+TkOjZEqdI2xHBjZD99aS
37HdnFMuf5tgVR/4ejKrD9KUYFPgJopIW15Nd+jej5/e5bM8uBNcW2asMPmfgZrIMQY+KawFKOGj
QCr01jLPnMvMZ8IiGA3Rj2kvGkUq9ZRT4qo0tpdKRKuK++painAudb1AxcBdWjb+8WJ+HQPU4Xxx
7t1x5pe62TJzSerNARvfidIOO56l24EOa4El1gWjymKpkdd0bWXaHqgLkJoLODkJ/cb6oSmzTbRm
QWQr0EbiTGDDtT99ylsfDqwtkZPFF+fmChS0mxVy12sMiqfMYRKYuV2V/U4OaRTeBG8YLaRm3lsR
0ftar4dWw4xq6f0JzSFoBfBeoPJEp1VlD5lKZJ0QwNN9gP52XbfsntbYLOXvPcmwu1ggk4mnu3pl
Ltc3OndgqDxUyTbiE7GthnMSJPQEIKUMPnJrmoI3uXhxVUnU5Uugxim10N3dDT3s9W65ZVnrbmBr
La0p9FlFCs8xRMA1f6Hwg5bNprdOj4vmT50iTLO6+oLsRf/Gw551hp1T3dfjQtvKLTbwEiOsSQXp
RrGd0UbT+64HaFQTpdjB7Qc/Bd+t1sf97sJLhClgQGchzmJz8YD6kzJ8AH1w1ojacXoYpM+3C/gy
kQv6cbdgAcpgGkKns1TfFu2J3Nz5Epg9fj3U4ijbkNrEUbo+AgdTqNpgaNKdbhVURoy2EG3Q9yuM
LL0VJ0lMzN5aC1rYxF7h3/TMRWscm0n6H8HFgszgpM/3DCpOtbykT8qGt+pmxrkkBaJApg8d51id
1H2xjJ8gxNNuU7yuamrm9naqKBk1Hx4Bz2j2GZCEdXJqDEOhkRJkrN71IJFLBaIVBzQ+xDVKd9lz
vix6IDfkTH34neIgDe+EOp3bdivTitsP2XzNHtllOm/GnYXo7BnU4BhiG+9FsZLloaVsbxE6gR5V
RbEl23sVX7BW/FvFDw7Hp8UHDHmRoSIwg7poS1/ZZOLoB+Mig5cXmj4VkrTEuVq9s063SCsklwq4
aQMiwpDkaE/CVlmFgUdmwi5z6cGv+e3UVxsPZM2h2ycJmwcXZO8BmHpQ6pcnu9maOByiWm7GsUXR
YPrDc+GdrMNXt3iF5t5C/lR8kuYQu8rokeiDM5wRoep4l4NfOstEObCTGsCm1zoasyQmI0oBfSdN
npis7LsKUR89+aq+G1Q3U4u5Ru+D7Pj4VOM2Jk4bgj6Wv074b0+bx01RxztgiveiwuAFvfVeSU/x
P/LwpetJae5z4QT8C//sP2heyyotBQSQbFoEW4oXUyT6OO8QYo+EIBUX2A/iYIN9I6SoG7hrRc0e
X5PTwtKHcvlW0FkEcH8QAwF4VZLPJ/uPLs+5qjUEDaKpgSkdMTdDT5cpdFvRwEiwurBqQ81dk7V8
5jOpXwgJfJ4dqVrUnF6k7Mz9VeIk4vgWG5cBIj90qZ9ss4qiJoMPzejfdIeAc/ArKimbFC+Qkcsh
YUvtcOCCMrxS5f26jfkJo9reHeCI3kmw3jEFsNJgelzuvpMlMpE9wyJUD0w+g4f+BcrCs5E+3RrG
NkzRDJpTg10/CXCfV0YDtrjwiAFdwYYpjMkysUSRoBfdl4vKbrSKTcSl3pW7GbNXp1806PioJ9T7
l0gnH4VTaAQsYqaMrYxZ1z8uLfNYxDYUVRs7NAuJ5DS+Zq7/6pTtj3Qb4csv+JAp78MHgA0Dudze
kWoRC/y7nyVngqoSBSvmclI0FLYYF2A5XSgv4G2SrfEfxCOyULvux72BxaEloMDWyWFwV/qSEa/S
1+P+81YI+TiFGG0PvVj2v+Be4uRNdSRzdbgeqN3WHKlZ8x4iMjOTioYAV5YPYMiOsXyTB5ACHJ8s
VnvDGTd1sGX+qLLqF6K9ogUhwvQmEYDEQpw/7NmoOA4LlwunRieJYmESjDG6u0XikDlNrMI4dsIr
SDnF9SxXo3cby2uH+bu/qPhb25tkQpqTcWRAq0GWnx87SvBD/L9E6461U/IV75OwBkCx1vushsT7
hefFZ7HeuUFTy+JFvvR2bCeo8qDrQYqK+0fXyQF1AhXdozRHeYq8DNs4Ay89zSLfn0MYQUMTtM5O
Mc48k88MFT1eDY8DPbgySVaYFFY3aS5pIf7xPsY1kUoIuMOHkU2q/wsFr6XByb9mgoUpKD/bli98
enYj5dA5hM7CkZcSBcFXNnAeM01nYpovNKK0R34NXLTDEz8VqGMmARL+e8tFR/GFc/GsEtpVzrL9
sORBjl5EWk7E3I8mBe2xf+FgEdg154C1rwtR9OmDMv8/MpWzxdk7QPJ6G3+q57nnV+3gzbDwOaCW
z+mMoXZOt1NiVXlHgEgK59uG37O75ZWVhKucnx6mev4Vj5OZkvN67jLOEECZ3Xy6m9+4mEur/HMb
JF15vAgoVgd17m5hTkY3UYovwRV1DZGQkaJrI1yQXqq9dHIFHP0DOAP7xJ//kprYu+Gty4LcNiA7
IIhNRVBZJ6unr/sLvwMFz1bnf0aDIzUl5C3sYX7m19xUoDO7X0QIH0qssvtlBz57u7TzXtmhsmVB
Mzl+PY+sNvp01Zt8379D7eGvN4C2a0LF/GTuSQElEww4xEkah7M1C0KF8Epviqmqx+5HiNz2MqkJ
lEOYyaypgNIniQKCYvALl/qnSJtxfpISpXFJmGO3MyiEA50zB5u9cLsvFd0g7m/7S6JGL3WFpnW6
P8o3x4nHlcsBWNQfwrjvzto09LrEgIyXw89sfc6FroUtX7vb9umncOhysTfAriZoe2vDEzMiyaEf
EVG/x6K8/5k/xjmWM7KoHhdWJfm80GNr2+JdcsCYivMQso4Fqp4iZtXDeIZeYacI0UnE8Q4IPTYn
ZXJj0qdXLlcaC5WvI5jhY4c3BGrt0tcMlf1sldVwtcuA74itHnfrVlPitN7rYucwi4JwBNCuH9/A
2xMtAv3XnYXinK5hH+LHJzCuB7vfn6HkoXCZ9TClcNhvC4Qw5sOh3Tq2FSk4A3nCgcTJzgsinrJH
skKy5nmuIVKpGaXuslY28alsvjEtVcFCfPJqGvoadVkfQfC2uXih4S9x3LhYyvYc3m+6T0XCTXJu
Q4HTXhghORGSGZyhI8mMcySu4J/PpdlyAJMKzvp8DTy+X3W9gwJ5D431zjYIW72FVsYaBH7Nl9r+
o/Qgkli9fF/vlEy/FaAH4MTAWi/TS0gaZzrf2QJuX1mz1rG9MS4mEu4fkHUBRzuasa/B+rO6NxfY
y0e1qmMGvzk3fyPS+YaDN2HhPjjAhyecGsCEFrpD1V9raEpES0Gg6ajF1WkPtXQUKbSeUfgpRuGk
KTpxOoXrT8027hA085v1Juu6xF48Gbcy/7lvzkKTOQrpiLS4geiXrw8FtfiMp9YtQpQhc5lV+r5e
oaDe5x8oILFiOP1x/MRjSOA5RGYW2jEgVkZk71lNTUYZ8C1j8P6fCiztGvaC+G/VGcLfiwXyyQGc
hgkIe4J+fjJCJysLTFgeC3zpS70YS82t/8cJNgaOSmMMKoSbjVkCTXAP6EWVfO/Wv5ZipO44em0D
EYQa9RuSpLOqCuX910tigwhMqZQM8aeyEhVCofhBi9qH2UmXXR/l2AK2uYxGa9n2pbvl4dp5uL5E
vp/SYmMkkZi3mJ+4EXX8Yo0xCdX5VGqWRWRv/IKleehQSj7lZsrJS+42QJgGr9sbYHXBKQNAn9Ku
QKgGDNcq3rHJMXRoHEkCdjcSiyc+9CC87EajoRbYRDRvh897vBwyraYzgjt3VC8SVCT+xnFmE8nC
80riBVTE5WCDmZ0RTMS1jNtEromWXHoYrXB1UG0bQ14JSokssIcRq1Kzmqc0XK4APyWlJX4thRVt
zZhbFG+3ALzYV9atH0PGKFNqycUqEVWHnMx6M0loQejPRchcQ5VjxShfDjUFF9aKx/3TOAfD2k1R
AwVa89Vze8cs49dugZBmY14MR9Q/v79xLlgbQLxSlriw28JgL3M2jMEymX4SDl+FgK+mc+LvZa23
wj1DF5nID7dzNCR9lQo7ok+YvgQlrq5VcagSx8+NSeaj5LFmZgAT2ZKnuUawNoy2v34ILinaNQnq
TlJSUQA3Wo8qgXfQUV1lvmtVX+T1NGktDbOtAXdT8jrmUEKWE94WKHeZAcW45kyedPMtBMi7VRt+
aJDC8pZx63tOj9IXqMSwMWPbco1i178xvcn3y9TYV7t3H3lHfPcQJC5NeJDmUpbD5l4dVzFnoFQA
T1QbFufKeiELgNT8Ovus+NUaLToqAV6MC4+JTOb7MLV2FmbF01QektNz7yN6Vn1pPLFv3f5Zcf5G
3I1UI6+aVLp9HTh//kaP65DSc1IYPgfKOqAYc+8KqgWrrM8JC5oL2F4cIhRf/ccN4tlokUQ8FzLX
+FvMast/owbrwPzPpcsHTMKlXC7FY8EdEeyRN8f53dWoibE58Gl0KXC5ax4lcWx51FcIlpOAapOZ
MaoOzyKsBIsQt44p1diCwhFi0/ftSVBWdCahHqsRhMOkmwznbRs29Jveh7Pen1/yHkp3EhlERuuj
SHlpPDv9OcKQMm70xaCj0zR2+nm2Sn+2HTjYzkWPqPex6sFBemee9IXXDiZnFc0dGZwDcAK8SSY0
uac0J/McZKpjKyw/4G6bP/aZ0K4fIquFNZXxfib2qGH3XUflxlZcsa5yA3g/kdekyRiDEt2pgQD/
PabHH9+q0N2nME5KCcn3ZFhePdWyh0rk0pKMnKfiWleEy0D9s750OO8T7vOluiVAzmp24x4rE1l+
3JpzzuvIWonNSQ6LU89PmTikGd0Ho/1kjPyOv4PRSAOnNX9hDTkYR6YLAJRLahwOmTdSm5AeX66N
bubMTuE3odpHaRcgr98aeumwU+soN7QOBQW9097QIFvI8L+aaKZ///Zp72bL1AOEmIAh2tVgZ6Hn
hw05BcnFBmjrk0JGZzzWWYTgezPup7kcA6JWsu3vsCBGikPwxJEGS/a7gThRs/1XNMXnAeMdBtSg
RXMz9/gtLyWo6qUn/Ujpr8/uGWRBvUCPKBD0H3jHA8EsKOSDhMO8eLkXXcHkLPKoFEBDIIup78Zh
NrrZoqSkjQYcG4SmnwWv/UqsFtYn8h2M+sXZfyabUxJ6XP1goik0dp8R4huU2bah01pVY+bJ73hK
jmRAHNI1wwjItrNJCo0VysdY8lwMFu1UfD7BLnwqUmVRUDcauP38lYAYcoghUKmRb9namxjpiaM3
1N4Tx/oQWVSX8nz5sFZsi6l3h16dRQ0/Gg8+tmlnzIh4ke+5DHE1WqEkK1ir1gqdGEos6JiCFyCO
uxeI/2Palr2DDax7EtxVQ35gLF2qptLcNBV/jLygPMvricewekIq6MD0tycC5L9KUV825YX9uuvK
/h7Xi2NKBQH7QZiWSC4MIpVMJFPhcP9ikTDrXuQdqoC2F36aYhKUReLflSyWHepjXZZd6CbLsxU+
oIAuPGBmnxWctv5UT7N43dSCHZCa2TsGM23Y6QQVzX8PAc1jgOhmiWb1G1HB53/alozieMLnPsRF
hdlV7lu8pv2IQw+S3eu79uHOQ3cUsnd+iIFl0mURtovCEW7gDy3PScCiniTYkC8sXFQdzPB6s+8q
/ICIYbhssppdpHXt1Bu9DBmgnubxKQo7Bn0mEJuLpSrVphRuNo2ojYdVRoxBTiixjhQL/VVgKZK1
bb/IVZmuh51cvyTurWfxS7fsDTpF075Vc4mcG76qLcGcPPn3ccR5qCoAUBR+7t+KIbNE87l7V+7b
7ZELHfLrfiLe9ygAWDhibKJ45KUQ8oI0LXQ5/evjn5JIx2qFBGPSxtzjliY8beS0fFUV9o8lwDlJ
ZcVx9TUi3ZWqG8YLPLFuNLMVZSX0C0frmbgCeD71tgrlble+VvRb4L9qBGosluIbHmawp/+w4YgI
6dMuT5t6YZFCrZYp8jcDaBX25M2VcbEG3Lq5ZgGnEuqDnpskANkvFEnrZylsKYE6TdHDD0wFCzvC
MTcFBlu8QorYX8JF7gknfXhEUFVlx91PoNX4pCJj7z5BvRBWBH+PQwtLcp0ONZM+EGNqSpNsC5/8
rHr3dlD8vHv6mdjmWwWIXFtg2Acu6KwWv4Cw+1bcIMkLa7Fc5XRd0B4hU4i6HsqKucW5PZyjtPyX
mlzQNEEQtjsHclxumjWRN0DOErxLBfllNQ2q7jxtxO2gcS06LVR0X3EKFdvsi1DQXlsUN7XR0UUE
QhrL/1z5l7GyyDj44Y8hZ7DqmSVnnytAlw88Z0UeUSamGX7eqsqvrxwQZzD599RRf5jtGsBBeQlo
3CQZb0DHBXJh5WQjD4eP8G7I10eFNGdahtGrIqYxQxrpTLdCoUg3jLYrFJRBI9qVBTU9FsoGVklQ
0qBa+k1TogLY0HLCOQULxLfFlOhpOQNuF5VjT0O683bjaSHCf+dO+9YRY65wu0Df8CVnESzDynjy
OmU8O1dsbQjyB7c8L7V4i/afGVPaTewqWLGPa6Uv1aEM9KucjwE1DWH1wcNZ+OOMPXuaPkwk1DWN
RDLYz5oDTG6/BczAlhWAYpg6yuiVkl6svXZS6kJ6fbN4wtEvkvw0sbA9/0WEbcNJThhxLlwhU41n
mN2Z7R8DmnzhSryDTFI49q+cQ55RbdFPe8DqJkzYEQEmilJKsAG5z7mYBf9HUMxj89x5XTZr/umT
RRnb3Z3WdFhgHhWwg9CxzV75QyfUuXG72ZsFaEPf0hmVqa7SDReOvBNWI/X5L0s9z2nAj7rJDfUF
+WXtLpnV3W3gF3UdA48zIqLNLGHGZSIqMuZxWjfpzPLAPW8GLDlgIsqvU1LCAkAH82ZjtQslKlf3
Z1mG/IXo4mCcDW6onIQkhS9DeNoCSo8JcPYGtRjQFxYED4XGKw/Gx/5j2nRx4SfF80xvbHxSvyy0
qNK3O7oSlHh9hkenNOeNNR4IroPTwCoaYTt+IgKdw2HEOdAAkPIHQnoMeOQZWYUZI47qCfaBW6lr
KjBVVRiLrh1CNjj4Nj9wgQrZ0tEd28M0hEM9qvZWb87E5wNv8PmNLPjq5k6XJ+lEgew1f6K2QN1t
NhCtxJHIVIixo/nAEeE9Rt7qvBL/W4oPQ5dxEB+edxHvnejxk4m2KvAlx34UMCfqjkvldb+NsffU
EAh67GW9/KMb2U78PsOkUABuXDFhIsPu6NMAoyn0uSuGwam61RHtA9MGtaz+4ptBYwt7gORztyaw
K9ZA2B0cm7DbIGCVwLfAJjJdFfOSDdDJtBHXK1JaC3tIwMPhiAl1V+4Ygfp+S50BnAbZjscvWiDt
JZMHqzPBPPVgNkgBK7jJvxnWl9mn3bDO88KL91kfNjG97+FtIjZ87A0QZdHtKbhfCoGrIwUKFC3N
D1nAPT9xSjDDMshluAL/ztpSvm2j8hLp/Mg3VYw9wu3Mfs2dOBXEvq8F6NprNpMu17nLRRLsrhvj
0gAUMbFZYiuqPSQGVS/UL2poT+m8n+TiOA3aIRnjvs3PZ2wR/ShWwuoQhNdwUBv1NkLjtrIi4UKV
aEnfZL/Go4gcX89yRY9eANgselP428U+6+vGoqTyI/UjpgPfWYFdqf/X2BTrXyQy7qRq4496XD4W
xCTSb2CEYueeKuca3sEfhfKDQh7Rkhi49Ix5LUN0G+e9v83Smo9z1TCPCGQpr3Ovduu66pPIcxmj
R7OaG9rC8aZFUIrACuTqF1DSUv+ZIho7vPZypeH9mmIeedVRnOKwCZRJMiHCCapREMNNhAdMdr5P
NrQ2MgVnf9FEjxPJnoaShXfOAb7aXgH/fZImcRouxogTXah/XgrZMXqVdJlY/iml0mO5X1W90L+t
gpPmWVupQ1f3I38CQcmjc/Ucv9CVLfDYA5DgoEoXhnpGqbpb5HCm3IT+BJxijzJ6edgRAKynF6Ie
AMag83P00jCkj0mb9YMnjEWbJypNBptikebGZZdHqyKrScUOemnbCfAhQuvBO2U7ekq1XJxgUjrC
62ff3JnOWEil6aXrrimA5Ve2yka+poQikGT7g0S5kDzlJYqXuBpb5Xhw+76NpIURh7PXNSBRpctU
4RqzQZq8vLnKaNmIcp2OhVMC3GeAEop1A/LeQYwF88RuPha+n/FvJVB4YbiYoEtx+fHCXDVJOqAo
Egvcb+hlubpVpPhs0LrFuJmq3LpPgIQCtydBDMct4aWgXNVL7e6qVQg5X92eRw3AJhICFty/gi4E
aqiHc+9XZjRuytddCxrIHOtaFookI10qXvQfbZm+pbwO1+LyQG271AH2msnHntwlr/6GgCUVcVVJ
YmeglrShW4DF591OWFz0GEjWVpABiWgiF/lGDwuInzbnOsAzpavuwsUF5N3XUwdrueJ4mAVMqaOR
3gRlabGqntZpGBXjaotUVRA2jjUphMU4OsG4r5wsaStc8CnpDBuo9ApOmiVaogSUu8x7rZwMjkvx
vkMP+tzSTXGY87ukEDR+eQIASS4aYxvYj4GJN7Yw4WYsO74Lk5gjcnGFS/QcuG4CZ9qDLdbHrdeG
/ihnICClkyqOOxf777phX1S0yGlYHKYfPYtTPqhVTAOfVsboCoel+tw+2/fFReUDcQaq++KH1dTW
OZS379gW0V7OtkSIfY4jHihpAKf9cTgb2vT0J8ViDueyH8Z+b8n1z94XZKMygub7PhcjpyRl57bp
Sw+KJkiJt+eaW+EW8GLp4jCO61RiNCRUzuOom3W3mUd9gjH9VSSf1KDIuudBNxq9ikgOaSRujUeT
WGx6f1qfaqQQNeQSPmQhdUm4wZ8VQpwP8s/2dI4/Ci8wH5GOA7nfOD6aTR0XGpwTxqncOLp+VIQ+
DHlnOhNEZvKSipXvt8wWPJ+kVkIyR0s46uD/JpQYyTnLsuwiOKgwYy6tL1PXR5WjJ5UX6HjrhCvb
pRzVaFyXlWLpqQGwLh7B9qsMiC+pxYaQ5aiFaXX/bJMHI8tZRpgut+9ab75LtEdrcJyzYopV+0cU
xp5c/VZ2gWh2a24C7Qrc3sTDgBmGo8dUE8s45ix7Me0dAT2+UOGseaLMvdBzf3DaxB8yqNv3Q0od
lEFTWquFBcr2sxj6bACG/gFW4quKn6ggkNAZILSjQ5q9OdRQXK9ZsHkYujWAEbwLG328/kB7oGLQ
fwcwhKPVsz8o6l3Eh3tMQ/EeNt33aStDLM90DJqhHfUzo9x3ZIC/YPCsWizELQE1JlGyW5lkiUak
/fw46qlx0lr4k42+S44FG9RHZ2n7sz0vDaFgAR7KZTIcVVfzzyGWg7Bh7vsf1eT48MZ0zF5KcTJ0
UjTwoyHtPkr1OMXEyPfLWpJKcMwGUk6ZDFkvK2ouSQvCgJAuMLf9q35At3j+U0Vp50D/ut59iQ4c
DbDd/EJhLAG7h/g5lAbcJVNaQIwgZ3bcXkWMsx5E0PLc0elPdkmFeGE462nj/IkperBwe2NiBnTi
ys1wb2EGwCCetyuNScUU5uHkhTzoz7+XffOkA81GwnAvdytCWruTAMKoaDjb3aVkZdNyo0CLN9Al
QyeW7pOUEoerbAnKlc3WJhyCtVdoFg1IpLFbgBz3ePvMLSsKMLVMTvUfvOXNmWEnG4s3xAueGCfW
h8HdB98c5T2DV7f7arLux4Sfhcp6vuXb/nqUJINc1iUP2bc5EGRjVmf8hOo9elHtTH2eSLkAtIB9
qz+6B6/OibrTNg9GVAfIliaWFP+lSkh4x2Ibu1rOVDtSiBB6kFuA1EGFG+CpZL+vd6qfD6Vl2TkH
Ztj3NIhbTALp2jfGT5gHlblWc8QfusCnJR4vaho/1WK1dzAvD5fRW5Y5gbw1SKuPJFKkCYM83GCm
Ah5gh3b3b3sOk25F9OlU/9hVVvorJww/n7G49sHri8YBezmNEJXX/vPRTIyn0WQnQmmPO1jQ2Yve
7HkItIqFH+beeC8WnSYhMXBmGIRKYpNEtQdTYSoSa4AISvspdPf762I4I1DBjSIPRJuHFAG7DufZ
VHvIXyk7ENP+nM5ywBryijgBNcuU9a663KelxFE91kQMl02/wmDrSyXiOIchPr68Ks25fgEwFHIZ
Ut7N1LQDYtVXOc3XDKnGLAH3Z5VA07MPgLTnnmHEcQ3daE8BXZoHHSThcVR5maPqdiqkeZEhs+aY
ktd3sSLvRHXBSuQiPseg6swXpPQv+MOyWW9YbISxEAOcFmdGuszC0PqOo5HrfPEXwF4GSbKcVWLT
evYIcFxfW0h000FolifH8q9FAG4bCxk6ukAj1JK8X5U/SFjhETcNDl6KdpOaQuoausWx7S+HMDRj
Kp5gVGoV2vUVHTj+QaicpoYn/kBoCGN3lQVfsdEM8MpxIztZa2DiBX7PGiy66XtGhwTGXv8pNe7q
YCYwf0oDIiOJLaK3iG2abtsiNNK8uAWKhz/NyaKA4TC7XmfYA1bSIlAUqXKFUhtI+VhzkbNA4yNX
xaocpQ+SK6+IKBK4Ei1nhNQMZhUr1LE2Z+pMGWEH6uxwP4bvoCx4s9VcF1TMgLbwoJm2LfKwdDjW
a5fYsZu7gi50rA5s66JGe1cnDcFnQyFbBil5cD2T+IBzZdNPoSJIIwwBjyZu1MmzUJkB/dyWDL1D
4be2Yfc3hRrPmw+l1BrBvH7yQwi5bPEq9YHGP0SuDxk+8kjWc4xi6iN1wm6kr9eBvJwoZ9suYaYc
AZdguKJfnpONRXVbJa8ns7aEc5DgHRMN11cQ1R/Ayb6eWY8i63dcRh73V+R+amipdXef3w82g29f
CQ9KJTtKQn2EC2M7Ndi2+zYCRs/JEXy5o1hbHdxY1WoIRvFw23DZFxWVJES0QoypYNA+S1OuIKzd
0qJKLQuMlUDq3lGcdrJv5zt6VjICBk6gpnCzVoqQD4rB3P3j8yeZTyJMXipSpfve5FxePcsY95Ou
vIyHqQI38Jn8VuNnqBzmsQZ+BeJyaz0DEKxGXPrXlNRBW1d/Yzz+kVZHq2iWZ2hN6eWsIZWu2NgY
P0jY8a64eydSFsZe9xUOLEJ+QGAjjO6kN4PVaA8SaZRXYDhQ6SIhs5eOfFQ5vbHfPvDOGUF9F8Hd
/f0RKg+IbF7XdKnfKe9oO7MxpSFhGaHGA0fOTKh3h6Q4lPru/j2OUe7ErM1x4Gi1hK1M60mS7XhJ
l0PA2IRDVnbzZ1pwh767KRwL5Yc1EUkpvew4PCjR9HeVFTgmfTqOx3QVVFwXdMDYTC1Tl2pKWvSe
8qYsxyIIzL1oGTznDk7Ad7ZJfzsboLDo19QMCAK9DpDlsHgJBvubspgYsj0hMhMvPjAUSWGUBRtx
io/CesCGBUoI60oW0PcP8ABBAHBNYEGIvZd9nfu/UMCCZYFCmMFqt7GAP3En87Fnzr+qOahuz6ep
+2OOKvvw5Gx76idNijhCFrFNn/0jKNNB+WDJjqqK5YPgohaFbwlps2gRnhQsFPnq3kxlDfR98pih
Tk82uBxtsZjSZbqi3+22904XD8EozQAFpTBYqKkpqe4GiNIi0YZpf37EdIECULkg2vCE0trMqmQu
CsM2x/UOdayfysNeegUMHy6XCASJBdErRPlyPVr/vjMHdy44C692a0Ijk+lF4bb2c1gApq4aRRKD
cWu+5aEdJng4FXHumCzwzVyjIqqLdloF+5Rn/A+wDqyiukG8pp0aU8LWhkZ2nqGmVWzfhZtYSc9Z
ngf084/4vzVNU35cPNqLq2DQFmYCa0/W2srlB9fAMeWbWcwfIm691pzXfJrVIkrdes9/tZ2BwX2g
pQFA3etRHK2CluRLDKY6X9UXgTyhO+eV6T+mYup5BQcTUAxXXXPJibU2WiJs+rZw0n/xF6T0LbuV
IeMn1L1XDbtbpW30jFTuZeWeMKtd+w12ElYdq3SQ2CD/7C4irtqBN90npGQQXr/+JOC2xVRC7bAZ
2gID1yHiyLGYn6uR+f5gXjlgQpMBPv88zaz9pdLn8vF3pPtPWBG5T0dQ25orfcT8fmkA2Qft0ko/
9DgR8RG0eCwzZKZrRSjfpAvugXzufstGyBeeEk+JY/Eyt08BNRqTTTG0+HhAGoRq4zSDLopW+3kM
IjabbxlJV1t+L0iSWFt71LGPd0rBzXWfl6/rV0/7W0BQ3jBfwYy6hoi3uY7xlzGq3eFkII6hI0mK
omkXQR3jpXRcB3e9DoDCkMlf8uWjp6zPsRRTYKZWlg7beyEPcnKaim3KOdww+0qXOJA0DSKEyp4+
SXVOErL8qOD1jjnGGOws0SgrKuxcmTK4cl7hgLi5QXaYRvQHzJ3woKfTxTqLJ7RgpnFXDKbyHunO
pufXFGy6BySR5wAF03TX5fVw2UBL150NTSHvTu3+l2y0GXujXCgFmbMLk1txdcqxi7XszYRjRbda
MTz12DbKijpWKils4X0HNAW/ugVBBo9JYGjpM/hsq1oY3fiS1m2xZHuv1w1pVdrng/LK0fh2NU5p
ObVtMN+txnbQd9bfg2PrzFOgJWQrmc+wUyFxg/VxTQKmW1KfyTfEUBdJSTJNR2DStkT470RT4mvu
3cGN6OWS2Zb08gKpylKn+2KTc/oK9SzBRzLegCQb15PZ6unH9jUscWxcBrrGgbbrlftmOVewyAOY
Yj1rNOw8JEl7O2/uuBFUZr6NmImMbSzA2lGy70bhEPAnvVCgOh+msHSjqAsOHEnPVmcjXijQIM2z
pYfNmOf0Bng2EMkuH9Sjkw1CIRDm7+BqtSqrq8ct5wcOVH7I8yMSCmeMu/YfBV5NkWW4PbzaBhc5
ST8msVn+rz7jCFKVkZuj4EsvcGPwvRggtOHcN2a3Y34v91yeiRppr9nTdbaK7OFRctZ5jKZLFxif
ALHTHKWpEGvnzEPFZjk1IXoAVSQp6HgXb6jDg3ZkY7zEtHNfaOUUyvQy+AMf5l9Ef1dHbXxQ7Pm2
jsULk+Mp/ciFtBtklHSz2HYIdtEP3G70XGgxMxgPvDi4zY+zQPrpUterSqshAuDSneAZUbZeMVI0
K7DMm9HsXtEKsfUqfQ+OJCZPiivzmrUfuKPYDXisJtIzi1Odi6OSPqFKPK9rqimKsvMjPxAgGM1A
MmCbpe/dAgW2cTzb68IiBwoknlQ++J114SapqK41VS62QYw+tbOr2VQ4oeN5O5s58bMcoAD40afi
GYFWPDEOd/pnHiFPxtJRPxc60YO1l3SFOLdCiXAl94vwfwKBdriSFmHA2N0V0K1rfdYr2hdFrdUI
oOOUy2rgrf4FV+Fz2QpBbhJ0pE4SK3HGHmr5CZaoeXFiNbADSu0l17+P6myd/wUZYjCGtAUWrYhG
o4VzHktD+Z/a96EYH5PGvQ==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
