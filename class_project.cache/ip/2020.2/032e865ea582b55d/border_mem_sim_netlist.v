// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sat Nov 20 01:34:55 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ border_mem_sim_netlist.v
// Design      : border_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "border_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    ena,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA EN" *) input ena;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [11:0]douta;
  wire ena;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     10.1728 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "1" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "border_mem.mem" *) 
  (* C_INIT_FILE_NAME = "border_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(ena),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18528)
`pragma protect data_block
85/XvFEukvLoQT12txs/lxBYOD4ficuP8nGPqUEubzX9t54WnfoAfiI0wL58TE7az6hDOxLvqxhx
KUA3V7AtaXyklwWMF9ahS9naWlfcZ/RdehbgCzw80L3AUDElIg/k5rpDAbA8GpQlwQJuNdxIyIm6
tC+JdGrbhjBB85LKHmvjda1dulITkL1DRZUb0uryZ+JcGzOV+vpCcaJyJCmWpGdfHWBekQLj+Hmc
9ZAzGoWijylbcb3Hu/B+NkpAdrWikZNydtb1qwrMYAYeb7dE3I9NvMQQRdndKc+qEwnSDjm0iqTP
pMILjynCJ2AaoNO+D8gKReyqqtAzL0iavIKM1QlSBh5AGvnxc606U+QJfVyQznHcUeJRHp8PB1LF
f0HOjLpAcZA4JIvJbypuS6Ef2Ul7ujtKNjsR9CTksatm516qHR6dk8xcCniVx4s3HRbjw8h20SAC
PJT5k2+12d5aq1U6x9E4OJrWUgDusn4/KBOr0NNjWdjMLlUzqTZ44PRqbFg5hCGgdiedpuitMqCR
TxofsftCiZOjiibahKCKKQX4aiTIwBCTNAocQ4rhLcOmLGR0CBYqs+Bl7FoMZnAGzYecStd95iUa
L7Jp8Xxf3wxHUsrfZu2yzI8x8TJFa1BnZgs7dLprcCIq4Ut+fSdD8RCLsYk2TBSfe+xcLExpDBmj
ZHNDdnpqSI9pU3gS1aoTF22e2ptDAh9KV+Kd+sDmWzfa18O/ZVdECoviiIxRV9a2UwggBBoLunsQ
EmefL5v52yRHaU48VuaTtscabplz+isAl4DvLu77GgDgAOudvMk726Il7P4jOsR/5i3O0ogaXLfL
bndSXLxrAm1PVxuD4cYOdqid0GqGo3e7F9Pg/OdKBWVgu3T/FZICQhhtc1vGbaLYQ7TatbqgvFep
JKgXkOX7hTfWAHcwLkQsFTtot4NTRxF8f1qlkmG9VAFCrEEHbDZHDEIE2Qo0aGjAQuWJ1SS/L0o+
0uV1TDCgp340kdxxxkz31QoOu6404j8mUM54ZHGKUmeZTLHsDwmheOC5rO1sdCdesg+TkFUsKg/s
tt1D34JeeP6a5YM2MhDHnRz2jwqWUvoGUAS+66hDl0So8DXudTyXe6kCFbch+weEd3i6loDH8Gqu
ZXcRB0f8WrNGS4MEONtf0iwn4xefrDCHZrYs5BW2DjDPQv2kacIY2Utlcrp+ld9jReHrxEhxL1wY
+zx7Jrsb3u0gFNuPsnPFg80h22NI1n9GfgrrFDzzc7C+9jhHh54eFoEcRU1jCQP/zawsitq7XtD2
fUcipCYGYNeQC7tnGmcRx44XeMZjtPeGBz/DE4e8ZVkziFgkn7OE7J8K/xfdRC+cmnwJtUY8PrTH
3b/eGsD1M6kvhCQ4OAmXOJ2cWXvBNu/MfTOCW7MrueMBjd08LZ8GM6oIltfuAyZzyn6pB0uLIPwk
qommAcj4KmJC211vWioAw7FDLosWGn7iGpCTAd99yUcVFfuWHK6F/bNInQwiTjame1A1M4gTxVdA
mhFxNeylJ26hC4PmIH474fXuHc5TbqON0ruN0/60pGVd+jAeoNol9WBEw1YY2wQ780PzTg/cCPDy
ePfYs+KoWq+c9t58n+mjK/HfcCbGiDRMNC3AXy4DS9Ibkj12uOOLeXjW6m7c7xu6dIxZfIIpEKeQ
oXS9u376zlIioR0lAracOzDQTw4H0KqmLGnyXOREzpi32rxTPTJu4wb0hleHdZJlRIRFR7xnNGN6
TRpmS2xoS91CLnVKTmhZbqiUNxV5h4PnOB8uDhSQdJGLUE36fyqynjZxQpUthJs49dcdLtuFLmIs
O5l4sJB7/UmeV7BdQUJbObefe61jxZZJP7ybCyNIb/CehyBYCOOiAXeezA77SxU5ZBpcNQQaTHl3
+5EPQPxQsIcIA5GE5DCeXfC3sAB6KLiOrO6HbBwiL7XNRr6XsntluGFG+9mR6Dy2gsl7zbBcYct0
9XXVM7kcSGn2S0T3mWfsiKnIf2jJMhzUCVPZe5/HUq/tfe5u5KtUUCaq6OfiBKWj8eXhCLw0NhDZ
oqW3beLyOOb9lpOFTkkLBD6vRTkNUBUOakQDol7EdYPbDmYAaHgjOHWhCZbPBJno1vcXoorPdSq0
r71r0udUFarcA4Rrz6m5ZA0kWQSyP6RNw+PPoea8OiUfdwhiz3KvlFOYc6CSNgcnIRUunXcH+lme
L+GC6lnGeVVybJrSgILoc2ijIf91xY3EeAVYlpRthdK4j9yy9V3p8CagZVqsbaxMFE60WRe4APmq
KipVxPMss+ebAtoAjfYRly7nf0CuxE5QMi5pp1yaEFhnvp2HPXnunmuGVZ/6gGe1QnF5sEuNFWMK
4PP874Eru+U+yjBMwh+ZrpPnWgf7B1zFIj2wx6pOxoFlKnHa/snQD0KLIUbp/HzOe23lr8rrkSQp
GDMsEGrYS9pgCPh9sKfkzBbAB17bKvfdftYLczWuzSh+CwDXB0qL9LpBlmcrzi13l/CO8i/Q0EpX
/czPZHnG9aLfOT+OF7o0bm81dk8r9wIfueo2Ifn+p/PEd3ktA0ZyU161Bamljz1H0o+Nffl/GVuP
6FiuTzb9NH05lg/ekZCUv/QvpH5dYZ4t+DXDR7ekZW76k6aWnGEfBTRmoi6kC+z7GUP6Cu5o+WUe
lJ/6Vfk+rMpL4OzVdmMxZELducMEgCTUGYlbMi+9zRz985yRXABNv6uRqYSgc+hMY1WrTNc49Nb5
gLTaTNm0zgVRL2s1fFfC4U7cZdDZzxGdYH8iuQKwJcP8Ta/T9zdy6bt9k3D6KyJhvUG9251UHH4Z
H+6lbRo8bGIxEOyB5RoneNFgwX89KrUtPOpQsi8ypBOoTiCcuMeXwzkTKdrWeT0SISCwjjWCuAJO
AxXzqWy9+oNCw1pZvOZO9/VEvZR9l666dqwJh6vWcw3RjdY0xt0dzO5al0LMg603CZJ73lSDC3KN
VgC3l8pF9llNd1dRG2k4E4IMwKi1Z5tRri/2BrH4Scz1Z+oVaN2B34bWxTpNQJDO/mRGdbLBatVG
Jd2sOZXV5K2A7xzd9GBpP8rjgC5aPINdGhWK2blgFyyvvotnquQrWVSo/gcPOOlJTo1ouaC+yLdu
c5pBmstwJMAWWjQ7XWSKkVvuVy0NNy3lCjzl2EKqaHIus5vBuVn33a+MVlyJl2Q/H9cO7+NPjtpj
/3gbj5jG1GJz2wOLxgqzIc33QHsnU7RoA+Khp2c3aqZ0he5/W12yjLcmF2XySvw2LQC5mRO1AG86
NDAJ9vnfc/YzdxY67mBRpqHka/u5aQ3Z165sKhjjYWtKBrFaPIACoFkHpGQmnVdT+0p5+p3vYiNZ
ztUJN3APtTqfd22EYS8DO6qVaqSnNX+OrOiJ5l277Gtvh62BpOEb0kEgp34gV9TR4KRtaqUYEyTf
wx/5yj3RDE+TFjaY34aKeGkn0Eo9XZT7FuXKd2YUJhjbqjp8zEI+pzDpi4YGblmZlO/gQTP1Lwh7
WXPNuMU64QEq1h7nzQsQProIDSYbXfdnPVKMLG2/33hD9C4lHzWNX6y2n7pgeQARD7acr8VHRhEF
+2Ao0LnzC7RGorXla1Zt2ILn0yNm1ehkmYvBfbzC9YMgoJ2csbbvd+nW6jDsgg+Yvqq6uUPWfNdE
FW1C1mtcx4gcI3HYFDsWv5V/ouSnnia2Ne3guXc9G5uMr5DonGii+Tuhm7Un6y2/b06q8hokUKh5
5myYwiJhvxVEKIDSjnzGdM/bTiid90aivEOW54echKhgiuYhAXxut48n0d4dwG+eJ+4NMZ5Zm4vm
hIZJMYBOtSaRbUOPtZcbpcZO4y0xOjwmQkVr8jduDt4cxWTbTDSxW3h2fQonbYNDAPJF1zLiJ+FB
eRt+QyI5Nvv6/yNOf2SXKDyiGcuMeRZHL7VFZt8/6FoQIQQaqo998yGNYGuyNRvIAbePxm9Twahs
3WdT1FLne4HYRozEN2Kzj3NVlneA5WwTMSE8toxHrbRrqP+qx8Txp2WhW2Hv/A6QsHIfEE6WNu+R
eDeJh6TdbskXEzC3CjaidIHaCYpep3zqo3jEdIM3Rw8B7tLEYkM8uS3tyghNq9cBCJzSdpr+pnJB
ldg8j1dBNctPvKU22fxN3t7PPUy3rXZmUt0l31NFBCDPOmAZOrrraKs1Hqrrc97uq8SLNsmnVHju
XAykKBBaME0OZflqLnwzakTZgTLmONa6DKRWZXusTXTzpokm/RY7ySKzpJ7AtrWVswbYVzHXg+uN
kFBydCXvXZWmpqWIv5xZA8mKabSTHdN6u6wVHnrlT5OqIeTexUKQnOa0taNu4T21Wun5+pFqh2Oz
5C4jKHlOwe67V6PsJHZSyiHHX807ZHFcFcUdN0fGYe9PpPDE6RKWWWIvja+PuAR+EEvfhNJVxzNF
tdtdJXeQ+qb1zMUQ2dmX09XASzNLkL7PODOF3eNUBHSNV95Vr1xHyzVxxCA/iY++DXNpwKD5jdlE
Xwkai/y+x7vWIBi1UjCiLJzHCcmbfkTlrmb8h9bWR+GMyeBAl35DF8Szk4BHKxnrHgdSoxoV4NaC
/NkPRrkQDsG3rNje0cfT06LWFjCEO2T5cvvq1b/4IvGIDBtNkoi7i1/GvWgs4n3VZheFPOtaAmcr
2/EnIWzwoe8mGfbcEvJ0i7A3MBlmUvR7Bw3jwRiBSndOPRl0e0GSr0PQF0m38SYNFTyc2w3DYd95
EGARwOzHOxtb/wuh3Xe2KyoXdIpbZp9NYdFfqmhzS0P5BEKF2sMubUw0LUw0ldZH4pq+KpvKSV6q
IxB9Q5hEX4l/6c00uvbiFI9wqtGNl1z2KH0k/E2+ke0fIaSn9kgIjIo/nHuHC1+luBqsQFLuPzi4
4veIVJo89alMrtK7qSNg8yL7HJIlnSah1tt2pr7ptW8tRnXK/jwPk5QC3w6tHLPKR83XzVJ9VFaL
ZvgVH4bcTdoW2qW3gJRXcIeGOfe6SGXgpqC08CUbzxSQ78G6T/GtgSKGsYCLH/M0kSQc2qMrcwY0
4JpMdzr869fibhA4zWNzqZPSqVq+e7AShstN2nq+wMe5lfv0jjWFvr2HA9qqT3ngUdh8r08l3NRh
Y7zm+/zrAYaQVNc36TKS0CiVje+l5EQ1jd4009B8s5fY/6Jm1y68WlyWw4uPiyn9Z2BcE7ZhVBcU
fQlcHHf1j1yl9KDuhP20bCCEat7lw6bvOhs4/n1OrZlQHpswfTGORo6C/kXA/72ooIam+BsHJ8cb
tp95UbqUU/NLqEi3OixpfqK8Q5dZbYLb/3/Q+qBEB1eH7fTaJ9tSiS3QfTUh7BiFNMMqF/E3WDOa
9LLyG2n+5qgGl9CNsWEYHvAOIOvHvTq2NfkZunj4ZroNNq8zuw1WZ42E7btSLk9ZTQnSva+lIEFe
fJChER46nv7vz8lfAm0uDFlp/tBu0W38Ej+b+Tb0axc083ZUxJ3mgmvVT+zV5arVPbEt+g0/tAdu
5EeoSgMeMdCLSWx/202pQDe8f8G3bjRM4bjJXyPG8OcBWtsmJnAfvcGEsJW5IKyQmJaymdPfbz/l
yI1HEgBijF7emgmKmpcsS+vJds2ezn+fXR2TuVwJwE7FcYFmJPehzCBWokY5aFM445wrnlM+ESEA
EW94d8+ejctrY1GPa/cUW5fRGSbRj/JeWTwoQD2qhNfE3lLx6YvuNVaMxgfAEoQ3oFfK0bMSzNr0
+VVt/BjNaVZ0+5hc3x9sPuH3SHkw/evvghOKhgB7ch7XhRBpV5hIu2gvges+aq/+kTbHNgJRs4pZ
uqjrEPQIF3lwlY77d2C0jm39FX7zOruLITm9O/qypK5Nk8B1jG2EvGepKhLyFcMUBMXp5goMM5Gr
brAJC6y7NqKU9F6vGZdq/ome1nh9BzeIU3jzEa7yCS5WLGz7n+GUWa6mDf9H2HkqK5JN3aD05VS6
l4pD6Xi7uWjgy0kA2RtK9RHe+OtCbLyAQYpOlA6bXRQQiDoYw00/ZGzTLppST6fqDWtrxMgS8F64
f2hz1nHjBCmW2Duqz3Dny6cOm6c70TJQYER0e8QBVTOHWrNTpAiKIWeQa+cII8CTUPnhGhpx28nX
5gwxgIzYcLvWP0SJNi2BoMMW5HA8Kx0DbGlw31rY5uO8LPaFz3NQtvFx2U43wHLvnsER/+v4L4/R
2bkGC7RmGXlvH+dqDk+9cyz7cWUu6Y9qrLVlsfN00kyiTSmeL2E9FidRml1IL1RIfZmcYI2cbYYy
EftU5X7Zsu5xp3T8Jya/eLaMdtY1Whv8MzO7qu7E807dukOOLbh6H5BioTillwQCXA1bPxNvScVO
HAyjb2i3Sx6sBl0k9y9dOZK9OUK6R7q81bY9fCVy6lHZoh7F/q8yVXzMYRyjXnKUNfshriOWg7bh
3WUhLYAEIEeHswBHFkbQTyYpuluXDaFyBMbF/y2iCEWMV/vGZX7mBZZpCjKo2Yap3jRVSzQc20tS
uaD3ek17sDnWDu0bf1s/kpVsVG+geWGzIOKMpYKFTWnwB6WPKvk5kXrR+9m0TcUjWpjM1A7BeKxP
dzURUAMnX/+ifiRxzhAGJrrbQwwonjcZkJ3QI56l9sdAwW7EDpI8ZFM38x//K+iRI6gI0fuFczLy
zFXNgvLmwR1XvOT801o+joRnICArjNb2fa5TYYxOMXMrS7vc5qRgIJHXz+EaNydOYq9YTsKr0/S2
8b6lWrdKKK7gBrIDAF+ffuVKD5pe8tbg12cyS9KvJ4gBu6vYKKZzgE+a2bxox73Pqg91RGOh4pNm
ZUN8hyyNBcRB2END/hqFtGcKVSaK9BXPEvlNjwnVFinE9Uc9HX3NQsCiZMU/2zhpRBttxrVMqzML
X5iGHF6sTyA0J9FL8cKABxQwavj+Dc6PDnq81yXoGFOQT3DKIKm1ACO4Ld1Vz3PqqDxe3y+L3ATl
91PF53pyKgfGCaHZgx3sk7oiQ1/1CEVoq4aDQORq7eb/5jLQiGHK+mKX6Aed5y4ZhFPhSfY4M7zb
oKYW0u/aAREmIxLVwHFn20DHQhvGVfyCWgJ+6G927sQCrReF2Wi+bIURxrmBDpSQ5A2qmVa8cSMb
oheh4VlyCLCpPzs9STJE/5v6PZJkDKt5XnDPWHWDqqZAEblnivCPbwQXcsMnf40UOOdx+JiHqKaB
MXF1YLaz0JJQJAN7Q/db75fmgYlDxOMDBpwb/8oBkHwzC3X2upL/59sQwnbjYVnuLZ60FE+LIn67
343U5gJ5Aos/bQPqPRf0UylxirEjj+KV4pbVqOTD906QbJ1BZ6cZpxszR44p2q4S90ir/7w71YKt
ci1QqKBK5xgm84qiL1/PUsfXbqXE0EHKwa5fXrbn5YIEqtGfEiyR/Om/1d1pzS1lZPlMcXFWOxwy
rEVnfP2a45sSZ4oGUnPBBlMTwNVZCC8caTJijgj1rJmIyJCSu46FgEmcB6u9eA5cZz1zxEsNVSjc
Eq7+UmPalGliJak/htp0+bkrTXEtZRBRXTEeCfaFk5efQ3SOzxnYj3Uh8au/FUwC0xHwG2h6POJy
E/8kwMy8bgi56w46Am0+B91xdU2Ej5cM4Bk2asNaDyUJHdKgzAgMVqwNpN3RhH3Pf34rZ6wwMWu/
6OGkapcbo8liw43NVCZ3c+ANG6+q88eLlXqSxnOO1x3Fp16VP2zTt/F2WGdDNBcL2idsbKKCp4o3
mm2vz22Z13PcwsjPMlwEQkd33fOQlniuT9/CY0EsbHXzihvkTVP0M/YFBgBXGsxzLSnn2pxzCLVG
xsunpCmzZp4OqwVLhA6d4jhabGiPf38lTov7VkL7l1zzBV2gBMLBchIgMwAEOTEGSV2j+LrVyFU7
eze9q7QQSWniHmeo3VBVPMKVE0M+QvgzVzB56fCzXorgATneENYqPJOFdSwtlL6tB19txB9LVJOQ
p+TzAGfTPonSwiXXQQI8VAAKXEeL9F8WAh4d7KedQHxPi/FNa3WR/hGeLjGNuSx2F6yQptg4XhsD
9Zzogn3mJCd1GoIaCFKidKiRJUPC3EabQWl5orjb4R4ycFgKzW5Xd7J9qL5rOPs55WDqYMfa7omE
GaoE2EgDh4TVOkLzajRa17Bk52uEjENaH8wxYoidQuZoT5B3oC/Ky8tcgD6fpLgNhZzTTQu1w6OV
a6WcssGG6dfboWpVLGrTZCCG8A5/nGS61YWoI9HaDLE3aZqkVtz1fFTeVzo6lVtvvsh9G6J1vXTI
ZfFB+XrKLMxNg1H+38/ku2CUZJYjvVYggszumfOmPG7kJGbs84uKJ+U99kiX1n+lk5ajLhyORtjp
1YGQgbApyYXmSMxZ2UY/Y991kyUpLamBgSf3cmYW+9DtBfxKEpfLzo01C/ypDSDz0NYBsBUqCbm+
sA9eR4fw9E4AdUdT/3mIxXw9DXoJ214MdDJbsaiEk2oJ7g/l4NGz03ZvzIoSpef7FqWOekMlvEli
fk5jmnJ5AjA1CBYqr6bpb6TI0IgEocnGpOL3iah9BUdXnOuFO3nUhr8qL7/PsmltXoSf5B6+Nni6
AWzIvC7C2wzSDSyWMNaR8GizxgfBXCAgpcM9hRf3peJbUCBjdM9FEdomlxwBZanRihwJQlF1pTIj
suTwlBEbFEL0mZd4glu1qSmwcMLh4ztRiByf8vlAtcamIEZ6hMD+mucUuw8YEMA5wrhHAKdw7WE9
fx4MsZTGSrrG//9etDyyZNfjazB85Q/c4WG5OfhVM3gZ0ZAtifojN5LSVa68IwnLtsGsONvd5v8f
+XRDgyhUecQcbjNejGgFlf//Tddtur9fMkjT8RLBUG9oDeUC9pq4krrDJ5lkLFuQq33dAjvuyXtO
sEkj2KOnZ2ZJ9rIHnVHQrMmZ76LF1XLPjIdl1xPZubs3R9WZWlsksJyDPQwRrX/alGVkuuFJzTDA
DnffKeE1VRfWiQWJx69XlF6HbWRHoIrHhioDBCafwEgQIZhm3zsfhl/TudAF3PwEJAFqa702U8hv
pjhM6bdIRvELBHg6DWMMjhF/cTfL2FiAOaVJlfvkLC0oMHZkCwiD3nVWiH9F1TMpTAXEQ+z9Wlbw
fQICtn9FQv9RV7/QUAm6dpN2U2nGNSLguQvTJHv6i/8ABoXBlDDr14GmcBxuvwjVImnPJWG1N/ZW
h3LsI0eRIauUh9678KYFTvU+Pt3+s8Fb8aHMY4AhmZGefUPM5Iat0s9MIPfxg8TvYU2VtYOntbFH
bhMw9tQzKIv1u/QFIy3IFScbyO1aGGC3RicJO/UaPBlN0uujPNSo5hvXo2Q6HkbB3mrUVgxmPcCs
HDX/AwMhc15qDxLwBJ1buCI4Yr9mdL81DhAD+5M1UYxciRzJQqt5+SOMS72SVENVADicq/N0cOeZ
ohE1IFaw9BKE4G42BcIWHUGtBTIpVQZvMFwcDCAxQWOXdF4omMud2lfgJIj5tlcS/vHc+D1SbyqD
yVehsccDD6kGRAct7W3WOsSF6phB0054WCEbLuPaODExvLeNETikAQs+JXVUuCKUWKiQwEkklHKy
oSOfojYWXzyK8217KiIwEKrBH8pPGb7/Kro8L7SbJV/tLdt1WomJcKT9bZwuyqovI/QrC5WLpZhv
84YlZb6xzAA0tBoyNF638wJPP0qINP6pxscEOUf39qq6TJeejPzloLHDmOJbFQdyDd/6X8+aQcJb
DOnmT7KEuPgHy13jkgUxi3bh3YYwR8dVPL+cKCnx4HZN4Ou95emUZU74rvB9kBB9hgMj0bXupRm8
ztFekA6Nc2wI1wBqaWywapRC4kUkIoagF1kGmPtkGmTIhpGqC0hT3L6YJAOibk0SD7pXt1NcvUBP
OfI00bJrq/Neds4L6+DZlKBQebw1+HIrIujbzgt+5XuuGmMBpWptjbcv9UnbYvOUWgY+itnrBFN5
giCu65BZSkGGArQyMibOJK6kNYJCA7kNgJUQOUm1eEXgLi1o1jZKiBQhWS8+Tjqug5LXDjkPWot2
OhDFmw5Mbjv7GW6DL4zk5a0SYSw2oPr1Fn1Cy4qawzLhdPwMRoEnbJHDbHOua64CCEX+moa/FsLi
QX8P8QbQJqtjkZ0r91+9eVTxaciiM0LV0tUFYGNvHhUi7YnSYSkBTp8c0NQ10ImbatF73CcGZZMw
7VNSBalDw+Pto0J3F19ssTlOIYjl1JglKTgphmKw5JKHwuKpJIAeKr4zv2uY2l6QL7KqrXeHE6s8
hPvFxXWLvx7p+45G+l1E62f27FpwDZvwXO/Tap69WqpLdBecY2ZVx2P1+2Q08qj8JNjwdsDrv0KP
r0EEudh/32/S5ViyfR9Apn5z9bjzeuBSDVNKtIMaxi2gjrFD4DjIHbn0LoG2fdOokaz0+m8/hm/e
4x72SwhMx5TIu4LQgExjBgTkyxnWxJ0lZT7zeSFG0yDfaNVeVGD32btEjXToseJDB6zEJOx6/+jc
Y1Bxde1mCt/2f8q1Kxkss0FlKqDRN35aBD2Et5vOlDBK+zEf0A9YFGs6jpDskfIFyxzsD+hQO8ti
tLiBJh3z/wE4cdb9D4N2f1M2zPnxLCd0zg7U18zHlBjeONHuXUYkAj8Wb7ePkKtjHiU4rjB6E9h4
34AlNY9l7OUav+Au6YspetxZmsbRGNQEWl9w2xQyJCCBA10YhPv6MaejPSnYKVrDOj6PTO3qdjYc
LxchZ4Q4YOfnRZOfHKVii2PbP6bPR9niEbL8FwCkVq7S8gJuc1Wvjv44epUd/jtlJda3Z97aI50b
zBAjYipWz0QjfKr0dFw8UpMKP9lQWGUNSffa7BdWgma553b6jZJgmcALKkG0RKPPNOWLLKfKFmd8
flE9QB93R0ojgwJwEwic16bvmTIBQloYUxTvWiBP83jGHLnHTPvniDBF0kojd7v5J44Ih56AALbz
RZpXGKbXgt/Fa/5RunLCyEP4ie1ukC0NMjiwQu4n5hdP31YlK9KBEWXFxf0q7fas5X4O4Um+sUYL
BnxUejP/9nwce669UQAk2NCKjwKTbUEGGolFfKxF2tVROmSwAyLb1bzZ8e0QtVICqihCl8LVPCJP
Ttdn5qSM9AlRky+U1ht4xnecrALp5p6KZ3iPfkppLdWYS4wkK8n21zbiLPRspfDXkkhOfnNoBX/Y
1xM/usjSu3YmxcK+uG2mbwIQwQlv3UI5d1DzRmS5dWp2BWBucsurI9FqZxiQws4SecBEg1oMe2H6
ydkq+pdkW+73lpzAqjt1B/MJJit6rSbK5LhdjXvT3q/l5Qawg4zRiRuz4b3c6q9FgCWu6FBRGaWh
rEZgzIqx6VIQ55zwSjNH28thco4Q7n11WRMUpXc8NTq/5mkcWXkXXNcAhJUJD21OhDp+9jf8SwkR
SrGmgvkyfLtG+Hv1YuwKNG+1T230z3NdK40ejQBXsEuk4OrkCfmStqVFqHCHNt1yqiGl5CmQrQV/
AerRxcgkKgvqRXKE15jsM84ruKihx7UbeJ1EIy7009lasnjyNnYh1YxjAeBTAWzctZ5VBc7qALUq
f8o3Nt23qzXV670I2F6791DKuIFm/jeKm45cFMJhdmqpqujortbv8Z5V7c9Y/+lXYqacQDW/ZWvt
D8JHljJZ/LLT5/6VWJp1rzoflmaCJEISrClJVB6oYi7DV74tmBhpVanruup/8lSvufKF4fy0wqJw
wab3xKZaMAv8InunIWMm5SCciFV8fIyE8Dorc/0y80xZN4ybfmQnKOjcS3YVJlp15EbP6enYTaxY
xYctmhk8coAOv1oaQCEiqR+tCUaTcXy09EIusCbKaAzOohcFQD6NdMq5P8hEgS0vLh+gLn+ptEGG
VTzHMTdxb/zi4htq3bUdodkeAIxnk/PpgF4iw3puPGUoMmBf8+ALdQAxqg3gHlQ6OwAqxvsdUvxm
mM+xkdNWFtuQZ/FROvyb+Z0lMM/4inXWusGYFJKTwOhMM+2LDivA+k1xdU9+7NVBiHJ/BMBuDMl/
8aI0U+kdf6aXHlYml3Jtyam3bOUMFncP3htVDmAqKvA23T8o5qDjCaHgtiJGyEjXalUHjFY6AlQ6
FTBGmmJs3YJX+HVSSMkFOY+MDHBxqK7n7BsP59MjpwAOZJYjfiObh3ppKUPBUQe0XmxDgFn/i9vl
4UuXCUzxpHjm/tqBBMLhaUnNGjVeNGpRMwCU7sSeSIqe0etqrxl/XKCae66UqjHMdyWxxceLUUmd
nn4kW76RlrQN57vmuMqnDRXEjO1KVCjO06Bnr/doifZMKsPoBjns8i2MHQZ3Dyvbi9tt9U59NiXY
Xe8wVaY78iT4mqjCvj/5K9cdVWUDVzgFuWezhz9eX5iwQSdAHG+TbYEUMj8noXysvUSUXbujbagB
rkVR4VixY84wvvs/MEl9K4nqt8LiL7jHfBvi955m3EssIkhBhPm5BNTxBxEcVr69l/Lip0cjAlqR
IaoL9KWSSPEusi+Ub9POuzo03uLTsTsF4GLruzVEV3s7g+FPF6WIX6GNj9cfqQXSniyJJYFaKpNb
bbILL4k2MM6khNUpxhdpMNIq0KCk4q3hO6dZEJTfWbvnWzUj/ln+ncf3/un7ncORH1E06udocaQF
i1XufGS1ZPZLewS/8d+GOcMu5j5xWvbp4FMxLm+iFtx+vYMa270N57xJ0ScbgPwm0lI6RKmOgNig
Y1hNRIYoKB1+q6gaITJYhmpKm/7/79aEzErbO3zpCeY4UfYEip7gSXqzcQfS/xhDCqsGkpsnIRny
MQWr+6EGitMyMDU4qFD/KL2vO4VPgUQjKgR+l/O+C1LFzSNALNpXqupWN3bX2sqU2+sPCcqVT2mx
GygEZuDsyoATvdUdIOqllKH50PbgbkplQRrcchvsvLH3l5lkgywo0rbP3oHnTDE3Lb45hYXNCnqU
Fqtp6rPEvTJ/ynO33Bl5yf9uUfwNUyo4QVDA7mOYdkbnHH7saSj9i1Y0mHC4ZieQOfFeiK1flP/u
enrHHwoTJiBISHwcSpx2GZo9JXm9hkE8/OSPYUxs+cTe1oD8aUACgrpQRMaRyyy7NLRHGHCV74ig
qZ4RK9lvR+YrKkbIRFTuAGgDvLu9fCtUvQx5/OVm9pqV0qTsVa0XyP5G6lZMU45jS24D6ueWGtr4
MvXPCvmSx6tZpH7OZj7tE6Gj3koIK/7pTdIi+O8ydbJZq9PgkVqsE8Gnh3r/fCpcfnHZqD4F1yxP
FAisQYPnHARKykaKwlCGZciX5liThdFXLgGOOrlfi5lChn41x5W/iZPJGmUAa70YGFx2Pr+/WCUp
91ExtoqgesBJSh1xRP94RH7plFWCxxKP2SOcv8nXZZ2vn1FYJrSWpwDA8n6uOtPCF+PrtcDFNONs
XVZ/WySVTlJXdEmefZUpvc+buBgoJ5hta9x3j7YCNR5u2ov/WOC8WmGmminuy0K8l9u8kNNRCl4n
24Qp73/+Qz0uFDeb1V1k2prA3G7pAB2fdKpjJHHgC3/G4D+qVbHLyipnJ3kV+DMmOQxwGoxzLSu5
H1rmXLWmXTLMC7j5gzDgTzzPBkS2JOitv+s1RvVx+L3dp5PmKD458dvdSYL3mg3HSLIoyS+LiDgi
3qoigmJsyNH+kIkL3Zvc2jZpRb2H8U+zSHTOV+CCyJNrMUaxRepUR6lf7PDOp3edJ7U8yAGwI3os
Yr+rUrQS7ov+l/Nmln9fibmJGYPZp8vPG8mVv2akvHfcbux+2Lf+eTLaEpyPJrNr0XJc7zqZAnBh
Zx4vFiAHF5DiRaAE/pXW+phBooWORFrKOUIPADLHCGsjSrgONvPt1T47bxuE28UBl054nrTHdVSz
3jbFluPLtsLVvBu78+pAdUTUc5Q75/g/QhosbBJhV88R4bc8JnuCoNKve8iAWiFwZ2IMiRfEohar
hd9VheLGWY94/NwqbHPdaQyndfdcgfIJZgby5FtPzBdTweMD6H6PaXiaQbRUKKFgD1xbThGczuLP
S7dmS++rNO9i/tJWkcLVylba1lktCLLliC1+Cdx2zuDxXeVwNO3t9WYNMfW9s0YNr6/FafTIZeiF
l01r9SNfpAiXmL4G2gaYiEstjJcDgz5bWh5P11SY01f94qHvjeg+3Sn5+VwlE9X3Errwsd5mkR77
7sJLPJrcnQzyJ0GueFJ7CDQretu4rT2CHH/pKC3TN/z9Y1faNY2XvLyELu5DkXzNh+Enu+jN9aLU
VrWY0Il0PVMgtbV4TDAkgtjtQWd9S6dLMiaV+GuVDKs9Mq4we8jNNr4TGLPiM1lc2XHsbk0uyJqi
TC/C27rDp569yVdgWs0t8HNsnE8UUKbGSPcOzVtsd+LD8keLPuZx/lB0Fgb9OJjSthotVXO/8xCm
a4LnnB2LQpEC3HCnmk84fqk25Jv0ullDfBEAjn3X1du/8ZFv41SgSYoDLGe4TuyAg2svypGE6HO3
riyhcYY1z4I6l7s1qn5amYD3mddhivXox0jFDxKQD88hIasfUQenOPpeRdFc6DPMtMUSg8YzkIT8
MPdBuF9euy9crOqmOthU2rvqh6RczShOTu2zbqQUQrjjQAZIE/YyqliCCvKOzyH2AfSO3LiTyG7w
6ZyBGfqjRGKyN/beewEYOWK/FY5DcMyUUYukKFBqCecFP4UX37xqlx0tM/31UPGuZ6kjpeNDLixj
hZj/z1A+2OyvXpaQ/fy9l9LfgqlhOWZVusfKFHwYYDImmtCeGEzY/jRmDSRWv1ezF8nnVGolG1Hq
n5cneIpwF/iIbQQglUz47rrK29iR4A+xkPKt1sE+GUpjZxj9MQQYwODRO2wk9G9AnmyVLleLWSsx
Iw4/hgu/rPTxC0+Rp49wC8voSETMFulTkFU5kI/V+Toc5xYitjlEg3Qz4tAXSsH/eCE9rL/gg4qj
JfcE76WaMWauN9JHvy+6jDBMGYf7phBq51+QgckE5sq9hVAQhLcHqKv8hIVnRWb+6j/Oh76SETZy
r15M4MG6mx0qjAzFdf/+Yrlo7RPY+sT50+2vrLK0ba6/LFEGunOxlogm8axyYHVGuRuYeJAx5CJc
egY/Z1fbgilCeHUCHUmSMW33Xf8c3ZTDtjSmE4zkDVBVUQJdbY6fsLFcMDR1KsNAmNCIi8ok4Qpj
L5MrQDd6iXoOP5Vh/j6hMTwiYYJyg4SaomDrq0+wTy1VpedI31swdz2iVp0HjA3tlFirfN/2g5lX
8jH7jM68odg+atHj2cYSIHlHg+I0jVkpAXlt1MmCpurFlVWdcalIIBxBh0NeARsh2rDJG9gy+7VG
DU0828a87OWEacThSDsnNnIEDIVctCOrKK4PGCKTjpSuZP8l0makKvIJ41xFngXEZF9bNqMttnqr
J1KP6pzfyk5lILMdyuYYfpBhhnvGbfK27p8lXDiV/qZxPzasnF1s0MM4bYhOPalcx4RMJDv90Rpx
Y8Uv3RULopUtFmZ9jtGTq7PYNY4oUPhzYNGhX5jdl2LnAq2KBPRjSIQ/gl1Se4SPW5SnPNtm6SLO
RvQTWXVUvtbAJB80qOiyMSOPNrGLllECGgLjzvDnI/yEWBYXZevFCU0qKW3ecFjraskhQXlPhDKT
jn/YS+SmWC2lVv2o9h4QYI3+W+3XtHSkzNqj2kSOkRO+RmFnu1VMg08fpwd0V3MlMmFhehZhpI3J
DUUGAnFqpRO/NdzEAJfdJpT/lU+fnf/4sFbPmjUM2OTNTRIVEOJ3YfN2fE6VjxS9c2bkrfxvc9Zt
6fZGxIeFUIF9F+Y4EGX6v1IZkX6sSk0671OC8KQh9ZBknzSh9nbR6T7rSgaf44LI7Gge8DkNsOEn
3iQTD4WDNQQOm2dPrvjNF+42xJNB2n4dxtMaZCs93oH6dAYTDDBohnTezT7VahOOqDe3vGzB3LTQ
Wh40RSxYllRKahX3ytyrWT25cJApV+8MfTkJVGzyLBexc/W0wdlKuNmcf3mR+oZRwA8wY4IfADZY
BuPrlMowGxExJT9gwTn/R68ZyFxS23v0el+Ho4SxgtTjPp+cQ4FwWMipXKOF5yrldbT40FDmDNVN
GH45LRi90gYKxJtYJK3sKOOxXdD/l9xp8qMC7H2+tvl1W4aKZE6m1DC2Vymi3YDdVf94RI3KYl8e
OmZqbW7wJU0pi9GwmSK+3O3FueNsxRKqbeZZFfMYAB8OJX2o6Zi0TUp+k6ub/GM4jNY2DcyWwfCa
suibH6ObSX/AEEpiPZiv9Wi3KPiAq+QcVuQh95m9iEjWBL39FuHPM6rXPHnTCsNQRROdYBXL45Pf
Kdf/GFZfc6NYrXASMEpfCAM8i/n4UhMGYyteTM3O/aIueHMer2ushYYFNdFpU3OdTmuWJpzog6Lm
P88biG/GUXRtj5M97SSg5K0Inq0aWO1nfNa2kJY5X2jqyOPflagFqacviDw1g84M8mXcouk/V/bK
B1jvBZNzglnCCcR5X/1NIxTaAs07Dwrc7vhY4s0xoZgOUaM0tp9Ji8Vyvk8L7JpMzJY6q61YqMTz
0PHkxG+W7tbxdupxaq70UskTm5MKitVZVCVlf5zkVhGoCHte4x85I0uOpbfQd+829XggOpxefnFb
Gpm6WgjdOdqQ+RsBkR/TPS4lcwccRCKjUoUpXdcVwMfeqHjF197FxPf4M+AKOf1wWN/Zn1WCsg85
Ux3Qt6jpS/AFr6A7qeaP7g3CO/KTs0Y8NQBfRuEN5QKqSNzcMbWl1sItRd8oU4JtzBqdyXWZtybt
igdaMKdxDqlC2Ulumzuz8tEJTF0Aiqt0qXu46V6aEnCT1ua3+BaZ2ch4DDTf3ru79gcShYhjltY5
rvweZCJRYNanYWEMG6szReYwakhMg7AaK26E2NksIe/zmr/6xdpCjy1XdSLlZZKqN0UcOOMol5p/
ykw04SGTZZdTBshcLfs1iRTUeOYK1k2tjMQvQJEiYCeeIfbqHB9e0ZLrAnKdJ5yeIcac1TdrcVm+
mmD2OCtrkUCOf/0EUWmKN4BovMVg7F15wb1qqUfOiGBPChNFFgKoRlMuQuxjSZKuQiAX6Z5C09Id
8X1eqRv9ab8Egs0OwMcUU947W7EPY1Jf6HLDwu5sQZ3RmEEgKn6Eh2yod1FHOqwee4lSxlCLUsbt
HxAO1agO0Z5rov8yXRNi4FWOdC7PQkLBKrOdEPBuM300ASTUXu9+mtDdiJl4v5/l3jY+3EuPFb0f
SOwDSm80UUTbE3X/rLfHd9vb6g9kPFv5um88ZlY6FLZy2bjSlDU81e7bFj18/QiMeF0YqliwEKJS
HNhMK8xsRsm9feBupjYnIsgSOhrfZ+CdlwxJp95YZuhsa8+Q79RGSefdQd3WlWCh7BdRe+iIKI6P
4v0lgrlSdH56yCMgVRaJWY9zvipylJ2AX87HIUf+VTc665Ccz/6sK//5cESDax9M3OMN2aBXNY5N
zg1F4bQ/J8+882IHzm+ht2lLaooEtVI9CCSMdTYSR6xnsRWxJ5hDBledYl42SVv7xfwA88JMrwNq
qBdI8aR1kX6WmKlwUjHyRJaM8T5E0Al3v6l0WZlIzu+eTVqX+WqVb7pMihExFxLhlhVYzGcA28jc
SDPh8b5q8X7DgEXF9gwWe76upZvfveZbUEBHPT2EjuxEq+J6/VvdYPZKt+VEPR5UBamatwv65Hjp
rglsIFb5zMobD6OfDyQT7ugrPCzK/Zl7nejl1LV7LWGq0U+zylCwikBPIJo+E2JWVEEFvuBN1N0N
KgJyo1g+fncTCcf8X/cpMHkbe4ij31lX9tDT3yiKcy7fV2koUiR49yz3k/dqCbahXSBPKW80iPqI
Rkn3S0DuZrzbPmEQVxsJy97BdJFbReQU3Ep4B23FitZyscYR2LX8f3TDXbCXtD1TD7s4JBjTR7Hr
TJ5b79USJnVuXhaCYA96dWXbzAA3wMGx9QppRk+OSkwFxnQHfYCsxpnQFceFEUvqOG1JT9b7rQxZ
goQBlA7hBPOhnhBOcZxxzSUDqF0dAV8T6zJWLCZv4GnCLR6i3LzfjgkJanaV9FfXtqx97HYnvhhA
J57HsPrOpxjg21X6a0FDwZYtkgIvRgCidrVmZ7bs3Zml1KWI03Hoa6jHApgUEB/q/Izt8bn3TrzF
VOX/LGfgkj5ARfJeRMUid51yuUmkrjjnYzADXRIj2QiXb6WqgWRAJAfd2xVsCzxlnsp8lSphvFhv
bKDU/3NsTLqgGhvyMrUe8uYvPISpuhK5rqp3sKdoZPV9umahKMZGY2jfxncz0WireDWtJE9IopXU
gMp/kJ3j5OMY3hmBSpEt25h/RkpboTgTrF0VeLRGUCZJwdZWb5JtDt9X4Wdwf2dOUCmSY88YufVo
HxlJgHTeVBBYSDYpvYjxRaBzF7MatowwTWK8vxRZsslpbcSpoJS1cUF7wUbE1aDOtm76vHznO53j
uUS3KKGqipRWGsGXAZVitAqlQoNZYgl30fpKK9SBTgXyj5wfkDrRsU8JIQ/LnauwMfbohFIklQZu
KFwEo/YsxImSLBF26G0yBjfxc+S+tnUlTtxYGUq9EpFQyHLKfMlzbeH++/58DhcAX77Urae8iL7E
Q4cv/Nk8J4410sYxJ/mZOvqwZLRaOHPcUKOsvSxv0IsQo4S24uN8N5vbNhOme/irfGadnDRdaK5h
OJq3nImt+MLimHaUOYLcADlK5EJakf3Pg17nEHp96IMPoohrILvy/zGFQstu9cLsG9HgkHj1J64W
ToixBc2v0Lyf+yElMuGFikCue4qff7pK8XHDDZUzCOZt1WZTH5gLeie5pB6AFpRQkiC4boT/Kyfn
dOP0uhbErv0OKV1UBTcIuTuNI2CUuHFBHTYR/uglzGPP/P8kyGfP7mr2NTqAq2AHTPY3pTPpZf/p
SUbgzBLsDV0QYR8i9Mde8yKsu/f7fgmXzXwhlmHiGem6f6PaApzHY+o4h4hIjh4+UKw6n/C9lgrG
EalLtibOwM77P67TYiwL4bqgK+Kg2tlUJwmGgHfX8HqW4LtCyknA+k/h4ZQ8f/hPv+heAwXQyqir
GrEz/bmovX2LuIxBrZ5DJi+ldu8j2nouz06hfDxVoWiie59W11OElvgtIXZogSvjaj9KC+TYhGjh
Y9aWOBlV3u9v5LilHIygfAksqcA5xGWh4RCq9a+GsF7LW2oD/Si2jP6ZwHHbbeCitlMp5fikzxxo
wURcQQsKJGTmP8FCUsoZdnZ1BUWI+Ngqnly1fcGdvW5ePMvF5jzq5gU+9ZstqQFzXe7GYy4lIhF8
8EEt37aRJnByBCW24iMcaZ531NgCopyeMNIxjkRGbtmKMP6s48sKHRstuX2XPbEbiFGWQyf1UMIC
9XVEDdW0gmpNuMPzvl7ejmStZ+vic/UefoXqY0WqFOs2mC+Aj0h8Fg85iOP7zdfElPpm1W/3r1ba
5VrHnKwlpcoPYGAxUGsQzsFyO9kPdhXaBT2T/jfizPHy8buj/TCutRfqHCAdK563ICANptIoV3HD
tTjFdfFuFs73JlMjOLDkpvBIkHgdEm5PsfSxm1/z8PKUxqwx9tjCrY+eVV2L7IHNX4Lzx71H5ANh
A+Vg8qetGx4Nk5z5AyuTcHjqsntRLVToM3M9r995gelzVdt38Ud1hmGG+6EuZonvDhZBF0LdN0rU
M8GqSZA0q77ZN3SGADFhIeaMJXghRE4w8Bif8VzGyEo9iZbVydm4XQ79o0u6myp6Sd7Tc3orI8/z
LgrZSHSuJ55+jHxa1Aaj2scyO/CH5Fq36CCZz4hmTE0d2D8uq6hf/pt98069ycmufwxMt9PMPxNM
y7EUWgOqQag+ubvE520xm/5ixIDsYZGl/SIDQnDoZJVn9B//Bkk0LIYipAlqqzPfvHckjVEvGZwu
xh8kZpTnwnutUkT5sZbWvVJ0x7ZTquQXUOJrPo+A+o8qE2dczknJFfNdfxUowkyLdJCLqiXhSuik
+zhv+HwJPWF18jIZGxj2wB2uDWDfJvQr4F42G9bUD5hmCYsjQCsY/hehYnyQBDCnWVWLsI5qtFqr
PamyIl8aDQDr/MwrmocX/eEc/dvmfgrLusYxP6CmHHvqGk5qe6qcN/fwrDn7pPzcAjvJrQS6naU6
yZcclhGKpyST3ZDTC0Q4Vp09MMdstZ68H2gr1SKZBQBDWNOarE0KgHsGnQMtex5Ow2tz0SuvJaba
s/qUXFkWqfS2ADmTO3Zdj0XfHA+Q4f+JiFaH04edCvEWks8jxkrbEXLt2mEWTOMJeLTYIfJpGr18
v7INeNYPuXAIdP/dRTPTOa2o8HK8TCJ8HPelG20YE0uE2ft6lx8RCs/h8VhLo/CHZceYJTI8kZZk
EEGsR8U1VH0AEUPr4tnR4SVUpje7KsynfcueRrRf0PxdMfNDOFqBT0MYBKrjDVaMWI5Uc8GSgNdh
b/RoZockQviCRg+AkKaglnclMndVC4+3ELy4qQ8kVWQs+1BYs2m3ondgepwrjMyNxNFQ5icT8j0d
bVXUpjNtgHAjXtUp0B+AX9xP1XAmsqdpQ/lirG/9w9itKpuaLAa1yqGtBRIWZRm+5XOi3vL/QnFv
GYr41JMoGCLzzKfp2/xVanDhq4oMFEUg7m/no8EVANI8q1Cys4Y/qVlPtJ1iK3Bu6i3Hh7ouuSc6
EQDTvb7S0mX4ggChzceARMvjUj/gQ1o++MmG7nb22OnnbRkwHBml1//sitbO2NdhUCDzkGsgiyQz
8UiXY511A8uYri0w8Dagriq/rbGTR78jyzUk9BaGGMpMAvf5ag5L3KbNJ7D3eijylNKYCTTjtU3Z
5BJGfrcP3uZgd7cz8glWekv7sIZ+OHcmjLrT0jj+n0jWU09G+DZwdjqSNcLcysuZmrCP/IsbPPv4
I+uiCY8+e/Yj72tugNQ5PV8Vu91joBnnWBOIBrZKng+KXXNYPBBcdyF6DDnuXjgTp3MfXA/3pR+B
CQLn8Yj2xHAsjrm8NSpMeOfVX4xqt7j2j06K++boE2RCniF1ZQPFpfR5PgNwvCL6tBMpeemPT+KC
7lsgW3p6zzMsniaI+4EfY2UJ5IUjZsUk3RuqD+Xpl/o2mIj9ilDustBHA7rJGE6zGsq1k098NYxF
HgcH5/N5UBG1ZZumDQ9CZYI1QK671/ntuAfVOgx3OUnacEEv1LtpygptzpahdnE2GD2eatrcoYF1
+YpDKaaW6MxrT176JMNrc4QniWFWrriZQTf5PpdHpNw25om+P3Q9X+UoqYdnKBGUSNSvQD5H9xcw
0CehvQJIFeoAZp13rpihxjNBPB2o9Q8iC75Mu4d28OWWPQKpxtyp9qqSE5bn0pkBnNfYqbKCVOFR
oaA+/SyMl6vDPQIutqG0AaVkUQhAmjjwoSsT0SnNNqzFIz1jaoB5MA90uAO9BzPB0EJsNkQclycT
JfroBx2naWK5q+km0Uzqi4VE2I/jBFLA3GtbB/3Fh+EBABVIFcd50pZLo0gP1HYCue0qEMKxmhNo
PFxlad2mTYcMfAQd0G3aqTh2nIj/knG6O8MtOe1IiBa/OUNNK6W323ojZMccc+rukjaFCUgDDfUp
aHyRZRSMjcO4d3br5Rod9eUDGlMJpkaFWtPLVBcHHteo8YpcdYQVRFfxdDLzFyq/Yv9f11R7f4d4
sEU4hbuHJwtfYJqQdvBAusJorYR157kzpSwY5mrAnCi6UqgoAXLiRbG5Tq7BkEXOGb8np0dsExeu
MfsuOxoXNGUY9DegrPUhdWrbwCJzrj9u87Gzu1fO7o1e3KsyAeaLsZPqEOsCFRbMwkIvgzT2fVBo
CKXXzHnq8CREO/aZvfdr1wSYx8y1HCfxe0kupjUSr4sGeZA2cZV8qsgzKG1lBsmU/A0FfuCsI7rg
fcJMCSogheq5aaqD7atTj7Ur6X14twrBtQtK1usTtqxvIhlIWf8riuIhU3JXhuuVhz2KoO8fUFyn
f2MabOCXvSsUpf+rdDI0GE+DHMnq9dsKuULIxn2owrRIDZt9mGPJuMwOBtYN8z2rzq99HBUYbtDJ
5cgQwzseoItP1mNDcPs1pzaBEG5mkHdCj9HKCRYc6iLrwn3egVYhlWKgSip0WAkdAeiaK09aBiJ4
0pbYbkQEZVD/OKK8BRO++jzYyQ6wADxK3ve4MlJ/F809hp+hLgxFgeXSHaTWZUe/8gd3glxv5n1E
WqRut2NgxiRs3xwcb/mKEvGqENNvTlrK3K85whl+5LFQs/2A84qr/TPsY2bF5Zw+hanmyM6Lc6mG
nm63hOC13xkAjq6/cXhXALp/mgWZEvh+3sIkpoHARHcPLRlG/5NvgAH3NZ2bBS97yu3LLb55bDCo
Hj8FDuUanAVdVKPlfoDaxntzQZdcyi6g71QWvHYfam6zj5pvy6okEKsAnXxCc/u6YO0TmsfFdNVh
fITepO4/PfbgBV7DcpkG78H52rhCLVOOL/QWQilY5LOl9tWedGgsVl7y6otBvWLvEb6bZ9TlQRes
j0Ttl7o751rEc6FFvBKWg+Mo12V4/Znp6A5GsiauYYtlXC6cY0PXR/ZijzewgYSSw0z43up2DcP4
QLJBtc5cyZN+fG/kxVauqgGY5arfJp1sUcwuSbwKRlBBnlqlUOUpWsQOGC4FGogKYfTdZuo/r0uE
d3z22UA3oHimrJhEo8BC4jxr1l/cp6A+Gsjt62jX2LiW/y736DGSPGELpUP/ae5B51ngRj3lPIne
tPoRlWbwb7L6hAMhWLDOGydNGDVCGMxieANj5ykuRwfrb55So3jHWdqGtGutSYsdAbo7YTGsiSxn
7Ibjt6R7lVgAdrXt56mR/VqKP5JLinrN38psLjUYRVV6fWLH0QbAY3OiLJoN6ch+mw93qsXvOZnV
i2nKsuxj6aswaj3lZZ8dErVhtQzdi/Vd7DHMsjmxaaJ9uYhDcjx5bVMSit3boxuBIRx0WUPrXSWT
h7vrmHZWOLR0C1usjAxKiEfyn38UhNdjLYdI6BtC1IcuOF7kKlaRYQh0hRCpy82nqU4cofzSZcJ2
al0GmPb9OxMLBbqS8lDn7PGOq98tD8ozGacs0akX27q+7d5WcUgK0Kx7uDkTTGzuuFRX2QM9auiA
zVqeCvARvngea+EANk66ljRSGKiEX6d9uSYS6rfJMuISp80bD6Ba/rEYoUezu9kGgK732W5kkUxk
3D7Y4tWHYZl6JczQbAxsEO3Rsf614IEYHGNhPr7wj/D0y4pF8QVpA7nTNX+FZfnyqJCbIaGZiHtD
Q+XclDft5qO5G2n2oTtwvmGmxRVRhpBMia7G/HVAvHB4qCt/Tp8DhFUIIqDDN1z2XGR7/vvEX4+4
B1cwGwi7VGEPTI9xEXgMT19sxUUg0yyBz0DXQ8KwmyD54sZBzbiuJAkpB7tZQfj6cYionPG445xz
OFGf4W6UtEkL7tJngBokn8VNRKlPdxdyyaTB4DKfoUpA4aVQWB3DsEMesib03ao1iCUg9grrEDDK
ga36ChEFkPrfiqWX2iCSHgMIfreFCAjRyCGW2Mi9w6Wg5YtCj9XimsEq9dZsf83YcoCuHxPnMcCD
H/UlV5A44vBT5AMAZWcQCzSMYqLBfhu6BIiwmuCkPBYprHPM0Y2eeMrBLoTLL1bJOlJl1SHNSuIi
xuxyxBa3xTY0kGC7URR778JFFPGS5zTrO0oCTp88M2r3SFUd9XjuGYzgFZ2wYNAk2Kz/GC6BDm+6
c5t8Q8ccN0Yw8FVDyUeTW1lpzIQIbvCfdCqUwod4FRJhxXDBqTnuB09eTv/qJ4iRHhyokMKsLw82
ki2DryQLG9iZOHQ86omwVTKhhc33w8UDnUvEOU1UVBWXHokP3H/1zndRCtYkWFuUL3JuWNkbIO6Q
azy6aIZ0DWxZDq8BpWkIhRG85UyyyOvGp2qoGBP/5tO5RLAfpNMu6lG/wmTI+QpLtUwZL2cjxUsG
YBImte2OlhF9O7OuvhD2YFuDijqQ7KQCxNb5w58Hh1FQYCWKzRbYP8xtAJ/WGi+Xpsr9UD3nvWme
ZXO1t5lUS5rX2i4ZrKVLOjHTM9OfFwPqHHjdWmXnoIPhU3m4jlvTI2W+aB7cyjzQlB6zifJbTgwf
oSXgiuqPOlr2qvS1KX0D5d2qPdXQPlv2kFcr7Oy3NI5+sSU1C4jxKMI5JBceP9GrjxV7Kn7gs0zW
lQXXXlLzkFVlHuymXZ1XA2VD3o8Iu7yx36gStXeW8Rdy7TPxH+g+6T9/kA2fd562mHInKKv6I9SQ
KEHYw3IDQSoeMCtgPYBDA8VRxgDPCbeuRtZPL8/0sWHDSn+YCiyp2nspaVwCm86iolRhpk3rx5sV
il5TQYNUlFiKDU/NOSgD/qQH8EqzeVzXrcjsV2omUTtozyV141mCWZI3LrsuqIw2MxAndYTWPbcE
giVZLJHzXT3ZRUPyLm5mgI4wBvhz8KFjwO6zOD2UCh1iqm8nor4F/jjTHVQxejmjHeS1Zwg3Nk8V
2jDmBCT1ZymJPavj9uCeAJpoMKDFHomWWpENPzjUqS8gkJZueizlDd1nyhvaIcjhLzphsGpxk9mM
imRuVBAVFr9lLHrzPiOdjZTFrLo81HpHLXEazxGoN8HUcJD/JrSeONFBLP3RI4LZxn5D0YakadUY
yXE0bcdrzHWmrfCz2FzN8ehQD9evyr6y4DgK/5op1iJpZh9/KLkZRMfS98m+Lb2AIngXoMILru29
aK8LmoUM9oORSC74QrG6TnEGB/UVNKaInyYbJ41dM00LbP+FcLIrDt4tAUhB4Plb7CnCoIbXMJ/s
I6/V+R27rILx3eGVHwxcLbugfKeepFyKHw/r6OJ3/b/p4ZWQH4VgmPnOiyGghe9Gm+zvHObPQlv/
30VI
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
