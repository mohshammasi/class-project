// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sun Nov 21 01:42:05 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ ghost_left_mem_sim_netlist.v
// Design      : ghost_left_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "ghost_left_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.2716 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "ghost_left_mem.mem" *) 
  (* C_INIT_FILE_NAME = "ghost_left_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18256)
`pragma protect data_block
pSb2E758vGxB+tWioGooQ03MTSBcFuy1jgwMi1qABwhUGxP/DIGNrSv1LnT0PjYLtsrRIE66w5fU
VCJhvJ410AC8oRRJOPz92QW2OwNMyDGjZtmYu2WKYVMjujZm2WOtJ6VfaIdSFngMtmn0qIwT9yyb
RY6Xu9Dd1dwUYJtayyff7K7pChUa1KEW/T1s6CAFSB7cbrF3uYj/gqahWkTGGz949wWTBSlnq7DX
sTJi8epxcFr2dfOgNYy7hFpcaQYWNKw72HzXlEds0S465kBzQTaYsXb7ZqO4uShVx5Opf/uuzBQn
rTR4zSeVmrgFnKYPtUArWbQxzXEdcz80/JTFUurKqpsWKYnbkfCxRTEzU7fsh+xlptUBrZ2S02yQ
NYuHj9AkzKvZh6Yj8BEzp/Y5xpT6gUbCAYSaLddq1B6c/p9SYXz3xZvXdTMde429v8LHMOXGzQ1s
fzhqDgxHD81RO01mp9XCUO96Ec1DspXtDT49zjX5o8XFIoiAGMbynD0IoLUil2d0o9p63BLJNMU6
Kpo9U8NI5aHaGbmLFM7WvNjDR6FMZS9z4V9Vx7OoDD0esheGCSLM2lmNf/fxp6u9MDSPiPxuNf92
uMb3G5YamO6uet/o7TEryBJmjeKc9po7XeOgg9rIgx3iaFN+vKWRqC5dYHZdYnL2f2WwjszMIFdu
LtM/Npt+6tkwXVPu4pr+IIKVpz2nMBGTb1NKwMHjCFhYHgoGwrLmZhaXJxurc60tHjUY++jbbGt2
ozVvcYeQ42v+zNy7+7GEHIQCfK/0HXNizNT6JkBSOaBCa5LlA7sGikBAjxMP3zxgrLU9w+zoPxvi
7nEGfVtZ5bQ32N8S5copdctZbkV7Amh87GUQBEYpApb02qewtbosWzZrZMO5PdWq9QxJ+UVtmKi+
JmIT0glvaHM7+rVszv8UaYmZyWro2jdGLipNqKDFWxrrprQ1M2dcHQR7HSHnfd1xapaZ4BvnGRcL
EpN4RQacjNIQNkDJ5s7BAwZMxgM9ACLXuFHQkHsPeg3LbEdadPDWGA+PdSYWdEXYKej4p7CkgLwu
wOfhlI9hsf108eOZ+vRNXkp1w7RDLUBhX8PNHtOiNW2nF92+7UJsaRW0nwVHzp3u+bVsDUiRaBd2
/DhJlWjtPcApx8+X13tj1iHnDcWnFM2dgjF2gKn+CesVeoq1L4Zm4w+3HCQCNXA6g3rr885Ybfwx
CSivKTAFLm57yg6PupIriYbOVSJqcq0zup3cZqLfsXkU97EpZ74n+iKAPC0nWFkbM+Li+/yZvl87
aWXUPYZ2DoFUKX2pH0m/MOoYH8MJEdLlKJlBzRcsnlT56dK73qVOmqZV4X/aNce5iF+2QUqBAM82
Q7AjAPCVYKnsMmkCti5Xdew1c2lNzjwBaeMgKZ7QNtAWb0+HTaY+gIAcmlwvWIETEQffsgHdfdO2
km9ctCfMmaoJiU/lubR90QHfUxDyqq7JwlhvAteTJjEZN9JpBcHjLBT+iWmVaFl0gUexgbRk5u8Y
zZXkpnaNH9v4f8OEEhc4KdCezGN+GlHJM+D025+2Z83r0p1KYcnHI+MM4puc7CDrpWwsvd8x4GaS
ZztoHsDM5uJyv7FK8xKD7Y5OjgVJRKA9YDTuGG3ur+wjmGwN+C/S0Qhh2YDEs37NKLcqdB/Xg/T/
vf2NXVLl9TAlaC7w36+APxtQksS8EXjXR77q8tQSg9nmbfxWNpn/xnXyvOo8yqSUuwRFSlW1SIlX
fv1BCYHf5B9KT1Ca0wKLZ5/AkW2JbZWfEMl6+pTgd1OGOVlQ9pCe7Dt+5+GAjONt1UZ1Ksod3n0+
5X37FgcS6TF+vRtFLowGybiseENQaMY8a9O/qMcbrzdPUAqVQM3x2eddcsMy2IqWXTav5wL6oqoq
suSHmIQTLCa8VaSBsRSIDfBunrqWGlwJPETzXoSrJVp0TO+rIlesKqR/KhWatCCH8vlONCpa4Eq+
usnN40biaLlJFdk8Ac3dcdnm+dTJy3oJB7ikSJmdEnn1bdZ4jwHrj17PYHslPygjs/RFBkFJHTzY
Em5nBrcolpaWbt2aeAICCr5sMZRbabIW0julSEdii1Lb058EdkAQu4E/QUXneQnSOOYk/eL4F8vl
xA0KCyHh3wqArlO8YjaFFByJGEmm9n/CjGSr/stY0m+C45pqZoWsRdLv2sgSChWtA7+MO2P9jDzO
LRNds5MNbIrc5btn/tTp7LsDUvgdWQWWP99EJO7Ua8gWT0e+u/W5oBhtbQ1XhtiWSiOCgwP496uV
/53z3fseqhHL7ozgq2yaaOfJN5ClS/Z0Ub554uNeehowyIvJZxdhxxOlfyWYzI3qm7S1G4q2O0At
fQUowOGdMmaLeOscwFfoIu89fy5Va/3bLzUpaNJDnh/sF6gi1DFa6+Ru/eK5A+SRx/DCa05iE2z5
eytHjY4Ob4uvxBEVxob9/58tz2JYY8SgdRpEfLc4rDPTxvq/Z/kJ6tIe9KdtaqqqcDkBUDlQt8uu
vaoCPai3nJG1rhIpGesa2nUc3eRNqiNu6iF5zk05/6AjJTDvNmxM+B4lbsMQyL0bR6U0AzGTFv7B
qkFfekfUe4VJmDEHLFmWacQJ/BD4sG10N82HkQh5l3Wzt250O7FmYniJFL2ZLv8QHgjh3qRCwDOl
DIdAHfhSJ+4j3AHyut6UDErvut9Wdc4JSOwmMI6MJ6UOveueuD+Btkw+aqz3z0RYQcVdesUV4Xl7
fmv6z6X/gfUmrSlQtIopvPVtopSTCyunbUHetBcY8fxgJRZnxny744yQCDv0ET2JzyBhjmYnyy+8
69QupuJ4XopNutpW2qMLWYDfV1aGl6Xpkyn2MIp8dnOvUkagnns0s+0/WBfy6TXz8iC7A92qigGM
IYdekueaVqusa6C41iD2YVxyv+Scq/8R7jffFsodK2y5RyKvLQViEryqXNlnzokdCKGJK5tNMPIb
lwd2i9tpH41MEjSPf3zxJAOH/SFr44MPcp79rM6/aFZLdgylOfwtUjiksNqEZwIyfLkYKcXNWlMw
U9YNLFZhHBGEuHZL/f1lnZ0ZvQ+s7YXO358TMXA70F9Q5BuzUeaj559BXL6QWHlHJoZfjT/h6bQG
vaullh1+fi/uDU5VPvZdYo4twN/Ew+TdnxVi1d3q45WmOyT2mFi6pCtLkBWkM+KWwhDnAGdrqhd+
rgHqYYSsSe7U+LXr2QJMCJDgTEB8jx4Puz3CaTvoVcn38K7/9IkplzFSBIgWQciBA4HhldTVJUex
ipP7tNSLuQEJV33MHGyekupyHtKEwsNHta7zYIDVe28M320PbMQw1/nVD1SB/yIHrGA8nbJKlttT
OMbFhAKei41YE8qDmHK7fNVNgi/ciDtwEUE/ipMosSAd9G17ipYQdbg/LKNTwYDVIesd1vRhTiwv
PpeTeUs2WAGIwjl68Oa7dSVn5+yR1/LMy95Splt2k7ntmmGIt/jNKi8iUD8/EBpOWBon1Q1dyKzC
rpTDheP4/o7TdLtGSoUUu1KfPqgVfW81pmbw2U7SDFjmVs6b+8HBjV5USm5lbSJ6hqTWdyPheIoi
meXAMLeJcaNTMHkSkJpF4vRzjtlB3SBMDzVAYBuep6yysVtXSa2RP/qIx5zLQFOYpfQ6HrNPvqIl
Zd5RnfQbe22T2JPXrf0ftk84idLUOgbUxEyFlDFAMrZhOXuqNzXubooMR84nNkVA5ysl2yX/Pj7b
FUdX8n9/zslLrPNCNp/mH2s7mBKtVjj7RPnRHKNIaioSQ4ZkoSVNVIOuWPWU6RMcZBSe5O2jwpwS
PEB8ps14qL6RrE7V9pCw1xYDWiBbsB0Qg8oVEx5fpCJh9cJvY0WNJfCp3loU+sOhrLsmXjLsT/aI
kKHMBh/wEixDy57Cr3sT9QoeDOW9Mce8UHXsuWPrFwfhzP2kEqM0GcoJqZQfl7js+Gq6XQ1kj54d
qk20pSJEGgcKvRS6lCmdZu8gcmIdsRNlrWqHtHofLI4VIQSo9KW4R4i2k6mweutyGF5/gmza2+nN
Uu2se+GtyRiQGxYd8M0+PycaHAOB3ALyijLS+44mC+P5Q3Cdk2KMcEaN/AeaLEysaHvXje2zo4xj
QEapnei0EeE13ITu9lK+Te/lqaaKVvPshCPkqhpwHPRMDChhRhqbXPJda9ZpiI8XnJuZjga+G8ko
MjRy9togjpED7QFDV4n9BistznBhf6c7VqYKjTwJIi20iLGIr5M672lgC47d+52kBPZa8YVJxZNF
RmGThK1HiocOM9ADhTAjQXyvtgconyfyBNozs6o8vRTJbNRq4ZgCihaatcpRh6usUmWTtxETxXeB
bpppKJWMBF2dRd0hS6HQioYTStghlTTJ5+mZTaXq49RSECpw1S0Y9LvirGmXeVWqQrFLYvWy7cWy
jGm0niDQNlOXEMxaZ1RNPQ6MHfwtioV/PunhlbD7RsyYoNrWzAyBSMjd5C6ZAGEw5j/P5IqfRHUr
hihRwA0iCc1/fal6f1e4oIrPgZNNzHtz6qUEB6yI57GIxbC74Nal/hr6foIruHOgLK3XoRhMJWA9
BNl4LVLyJIIp30alf6eH5/emdkxFCmNbZoJHVK/iKgMdyBb8Y1V99DT8TBdfs7NijTWMuO5a2pUJ
XqkQXhPvobGQYAZnrlgnbs+rV9wxbu0Uow3FehI7k8PWjExn2GGIR56541m9oMTW62n2BU0A0osB
wZ8Yma2MKkRt/kePTLWGx584pNApK5evE4D6Ay3/vAyh2y4NS7qahDXEIEMfYEcNkTORP/TznIU7
8xRsAcT/sfMj758pc69tIqJmTTgOcgH84A3GPLKdyOqg7F3VT+t34jYb6HfgssbEnrTXloA9kSEE
0hpF0QwSTOrcdfRGLihmUUPBIaaUS9de/30t1QgtzOUy/dPGfIzKdSQ84QdsgVwPhb1sJjWlGldd
1SJAlRK0e2dHTKIWt3c3W/KzNCzCD0shgIHvqV8kUX8U9OPJFr+D8dXZ0WVZDl1NrcsviN34tmjO
Fic1z+0As98rQ6Lx8w/QJ8JTsClxe2ycPz80tRUMq1KaG4zWyz2nNk72VVJmNsUYvw/A50cSvuJR
sNSTJakMSYksi73wcwRyh4Y/kccF5duAQWfUx8jSWqxPIA7R0jqBF/AWxnyqVREzPIajA7Lxi1xH
DnTaF/m7glPH7+lOcvJl6TFkBmXsni3pUHCwr98FmVg1Z4KAlDIVKOK/lAKuu791VBaSU5APulOB
TdFVxb80aF306Cw7w9r92BNKEU15nP0EHQPiVO87kQmJHI8Ytn/kWUTMw+mk+Wv6P+xoKdoZHZPi
QM52wCbuADUPKtgoNWCmo/TDJP+lgOFk8qnUc4NEkpZa+DhM8wAVzYmKLj1bb4bio2RB5WG7SKLI
oCRR8MI0IOreZLFxh6GaL4dHCZpcUuoz50LN8hCJ/wSgBpQi3CK2ih3sxaiQj3+MFqZBKZ/mgt0o
KKB4ooOebS/LLEF0xKn1mH/ZEceGTFgtz242rFQj61j7cjwG5z0TvfhtYbnccAOEFRHHWDnQ1166
AtfMKeeIPDxi5bpWpC7gJNMGx+yPLK1QdGH2jtf1bZrbH3QImdgp+9RHvldwL6yCzpGPySImpGXK
bJ2kh4c+d8TIbQInbrXk6xiEtabGhQQiqjpE87+5wnTwxbFzYN3mq1kumkjpinFDmSuKb+rQC8TI
Oeb4pQpQComlbzqaN7/nujJ6ruddIR2wHkTfTkSiIo7qn0PT/hrGK1hWjYuWQIMlqSdJvJFbfO9b
mV+e3aTY+kgHYLMDT3xpSHyjNcpUbs8l9Rob3wcX+tblDh575cWoJ9lRsi2ysAKlOa7bZ3xlkOwL
LqKNZmso49zXloGTA2T0xP6zuekM62Va8INkl13OK/drqewmrfobXfqeTGCUEIzEKyX1Nabclfti
3fHOEepoMjNnbMWklHNtfClwQfW2X0l2h0SmvmJomSZVqkyyArLN4g/DNeAmy/847dweEHuy3s+D
JJNF5jcVdMqbQ1C9WB2RvUFmY9R/2MClDeeGtRCcwtaRzxfpIUWgfivLfT5Djhlyvw4sRwKrduWl
zZ0CX8proTAclGndZxi8CB/macrvFH6VMO/jASPsNDYH3h73SnCuvFga6rX3DYCz25X455+tj8n3
z6xCEW2w7dS4safgM0HT5VMIV5coC+KN655EEeQS5UPLZPYwzpNLKlwDQg0OYWxUUhLmTWNIWrNg
y2b6ANVRDFG8Kt/mz7G7K5PjaXl2jVKZGoXvIIUKzsV2G14Jf+l+ont5AIyw6k+ILh6GY5B8zpGs
3UIUWVOIvN1lYwy8xD9VY4ZhS34kSdUDpv53EMmPT/L8+OorvUGN2xQQx2VR/63WklpEI5o8gIGH
jEN88nLazpl15tLjiAiUf2UrlQ4PNwxYy62i46Ru6XgKDSmC4UbuWgpNOv+EGJfNeh7hn5IpZdcz
TXno1tgfGHV1wMxaE9R2T22Hk5jH4446aMhPVFKJ1vJMwagpKZ70rzgtnaTUuIPCbSamH8pbFLXu
pXjyZG1KajcqelKUc4lX4/OPnrliqZ4KzpG7adWlp4pPEdsx48xfthFWni+G/gu/J060sZ4IT8XW
1szxFh9Jr1Y5J4a41x4649FsIa4kGMbpMsHoSFzIM3eN6q59WPNKcPpzr78latjdN9EJcK5I8dKu
giztpkU8dYJLR6dMldAh6haG1saVL9UNqNh6lKOk5B8hhccJ/+HrgmOCJPLye0q0nQ0F3hLXlWM/
Vbl0yDTH6nJV9v1FgLmNKw97mpyx9iuZTgu7IsgwFhOC0XU42cGa+4ljhRBx+sNVlTiSI+w1sAhY
jHcCiCd6cB9Nlm5P36S6e5vLH41sHfHAuG/5TNiMFeqS12WCy9V8Tk+eq2XRX5TwqA0VB1c3gms3
vGtpoQGKHGp/piy9KQIE4zrfJRLRNrgpKqJbhFZ4wHZUOxW1KwPIpI73a8gKVm+7XV6hzbIGE/EV
vBJQOnJwMwBJ9YLoqfukZwT5heo/xkPO3t3TU+h3ZddfWrsP4VDtf10bA4cAQq18tlJWgU9m8A4o
hIB4uw7HsL5Bbe5GyHLchF0b7pIvzcsPRm8G6XA8mHFby5k5G8394TMlybQH43x1vuvxKZ2cst/V
8a1saEPk8tDdkk8U91W7AVz/xi+qZFunROt41Ik2F81sieZ8NdAmjXpTHXDzirNu5B6bpagxaPQE
Iczaq4vjjLJINcY9OUo1KtcGGAh8TU9CClJNFchAnFf+91oB4YFhmYBrhj2TAsq2096xSwryB7Rs
uAcMiYeWI0haqR8H9UGBEbVyMdhD5l874beUioqccvrC4z6c2ydJu+ZXVOqebTQsLm2onOH/XK65
MWMESNdQzVFQobZoZ86LvgG+5/fQbF61IoOv7PZyCvDyRogYeID6exTh6v384VOSqg8q+YrIYSN4
ug4tMru0piRCLgNS6tbN6Z/dgo5agJeXS6sxGv0J6M/qQlPvDJzcXeIfSLmTKPpnAxdTfFA56Sht
6hI5G1LOEnpA/hNNkV6PQTkF4saoONj5hti+QaIXEHGgJlfrQ1IeOz4nnK0pQ4C6tbxOKSV/TMrP
EuNuMT8Mzx0Xa6t/33cwofJulW0hXG0IgzDDrNT45oRpNcw/qSAzCok87QlAhQbcd6/cHs75RI0E
2RqkItaKd32cZz/HFC88p735QU/HsWaBml5WR5E31EAeu+M9tIRgNoFqFdSQh1UmhtLrW9/K4Tuk
nIC0fhfgquZSNN4iMTgAP0M1XkqMxSaACKkyAw1pIhGqGE5YAZj11RNc+nVwX9Qg1i62tPtlSrXw
D7V7S8EWVsCQhsr73/et1g7d12l6IjBT0VA3CyWNg1P/8ebKGsEVzebhh8eSskc5ldTMPkGeuXDE
nC87zetZBzViRoUDU4jKft69CubbcU933uzM98MRRCHvjd5UWRTqNt8LvTVgKOleiMbfYMOx6olU
zVlcqKdbLkC0OKPrE4tZcW14E+TQhHTQRwQaMw0HcP0oqbt4rsv5KPYRneAZS7esp2iKplrufG7/
cSAssoHew21PEhrnMvIjoT/V0/qefdMGG6+ZakA9yZm+VRhTMt4a9DpeVp8T69n57eVcSfnYOSw6
7TY8H0TjNdb18Wz4sRWmamNvwuqN0hoReJSBK6D6uxBtFzLUKaQTjnLPzVM4BfBoApQkbojynv8A
m0+FFcfJjFcVOv+p2pRBq25kqf/tVbHI2eNYNGtlEUG9A5HS3RbyTb4iUob4KZgDQgOL1X0wu2OE
LqiiTVUf76SpLZyJySGSbFaYmTrM8M/ZQZjoAlDvp3OzkUU1EWjiUSe1/7SvBXZSH7lLhePDJdI3
pm0DaLEzTK7BvnGrLr0W6NZKCNLIvD/tAHjvRiLSUhFBetWNS/K3vOWdFTIsako0Lnui5M6L7bLw
V542QH296kriUe8lMsFsD21hUtezw8Zk7/epXAR5irOpZ/10hGajiLypGhr1zibBzVxNR2t887cb
bVK70+aAP4n1po9HUawTGVWqvrwtY4vVIpbh/rFdKwCmTrlD+PwPe8QlIp836/Wyuty8Q4wLPE+T
hn/uMfw2iGa+WC0iUW089+I14ACUhnboxaE2hxOI92T/GSHZaDw4qjDGYkJV+Y+2Pk1lBVyNU41H
af11HT7NkO83O0vGtJ5g6BlN4y55vTULyjF2BVrhnd0NrRjiihDEhBUUMUEkQIdi9j7zZUhfjzv0
QXNT7/AoB2zaqSAkQrKLOhrfQK8bUPxhapFcLK9UD2epCWlSTDLFJOMdwpyByhWQLLnxyLDkMnlw
YnjUghi4okdz/XgGGbvrLHONh2eowzUAzs7MPMMxzgRUOypKEAf44tUah8mmDk+XzjrZlYaqTNVN
MNLB285s8Z2BRFSat3qALhXSJZAGtYyDEIKwU/okDA4MN2c0MvPlEUTigc0hWkjAvOw8QeqocPL5
JjYnJfBEE+pypoAE4txklTE9oBUj4iPnswfiJ4howbYFqy8C8RvG+9E4RT+/Y+00p+gVhajcfSk0
0ekDpZ1Mt5utS+G7WJCD6tzJcBSYQ0Bdp+fGeSbOtLQs1+/6gWVRFzrggeueuE8+h6kEaiV8lnGa
o8ddn7ebDBrrYZZ0SFapwYOUm83rlQP52y2WPRoGXNox4xY4gHPgFDr3kJURl+DeiQH9/8hylUCr
zTkU4Kf8uet7qd2y6l0SFyR5J9ir5/IdUoGQysRsFr1KY9evpseJy5wev0fGMAbfJVJXVuQwsBR7
jyMet+UHEvx9gHgSfFHzVaZSx72C38iyhQNAE0/EN3pVNbmGw6n8PQLai2PZyVju3MtcfkxwNi8X
+1fww7bSwK3yOGQuq7S5ztlol3/CTn78u/C/lNMr246zlMrouSDQb8VbaibqeGrS2BElZUdMOHDx
r3Sz9GHRK7UQTgTXGLZR3B/8hCYOYmF0eLT11TSLlsdE3rEWiBSWRIgA+kgIP6KGvdnug/9Bdke6
xbVcncEe8L5TLCY1hOvOMJNWUoEmsvq6Pjwd2VbOCOnTRvBR9WjyyFkYodo9jCa97kIKuX31WDoK
u837jiBEPNLy3joCp7tBwxH6yM8ZnrGFvNNb0vueQO2glQNLNzhVypGpZm+pUPgeyFwSBFcTL4Po
KTJWVyaV7ZJ91Z0tRb+Nkr450SMUdB6uhZu7RbSZx/GSU5TMbYHQ8jtCMdWOGxDSzy4gDXvjABk0
dh2fyAO5Goni4xEu4+DTw9TJQS0O/FOUFh2sh8w40oKb9CnqR6SDFeRIlY426jUUI2i5aTaL31U7
sr2CQuWqpVmZ7wHFyTp46FvMaenNkylZ0v1wU+qAy95GAs1OZHndpyZrQtXbZ3iPvWxRZBgHTwwi
WUcsP9fX9dl7Zq7TPcflatOOSE4P/z1HWzpI50m2K66HFWEnMmgyYDjf0jKA/mNm1teN1z7ZaMOL
LrkH0rzy9m1fvy8aOcZCAjX4JDqvbvGi5ToLU0Cs7+qTZrQC60FS4VK3n4RLgKU+QyGeFn1cOvA2
47Q3AQGzf3+R45arBkTwhr/5G1pWihFvEXfYA4oZtBTNIfn3Xt/6mOA3kiuT1rZSq9tt8XguVb8n
kLCZktD821f//0E6yun6l2bERXeHcD0m9N85Dt5Lxkq5UDNxua7b18pD8a0J47VoHir0Cm2wavN1
uUhrj5o/74sHKYmmMziPQoVvsHuEm3aTsTESFCJHcCss+/vjJS2qlDsZvHuZfqPWWdLMF6RclZgy
zZevK1zF5mvO2ZRbUOEgO7bLADI1IpvcCoZLESfwI6Z6h5oBBE208kLJ0y8Oy0Y2WQQ08mdhtjNV
nw2W1Cn8VudTCR1adhn83GEazQEOAgO0JiWR7j25ZYLQzXwlVghrB6XpFtGy2VuNaPvJoiU/2xI/
6KqlBiXPzpJ0DOIX6g9F3djskIaoXNcPEg5itKpzU+BzON5G/7MkDOCNgOsgd18MFwstJzXLJ6yT
8L1cUZStti/8lQycch1aBFM9+vXI9qAGUcw/QyTrD82UA6D5YoBDULGGUCbmHAdL8oOIbf4S698y
pfEweYpaUwvDpplUDBCGBvcAQkioPuuanhHOBIExswGIw8EqcKj9eoEmU1odDyAzlfS2gHZaBQ+E
NTycjUAS9YwTNFefDEwdkx6zpB4jBRqoEEjNtZGp8aRFRGOQn7nUEl6aLWUBdbA7q8PsXHUuKAtQ
aUTzNqqrwd50cxiHkC+ym+RIRDrekTDWT6Evkbvs6INOUKQff/Oe9aKWqWZUfJVxz99B6bWp9Gyz
V9i5/nlyffWkeS+s+qWIHV3FbkvfLQiCozPhJd18kIOq2ODeE1bl/3hCgylodhREKYqV6K3VJ0/k
caCgwLEWqRUzvWYEuzNMVYuJtLon07vPMwBSY3fb+HrThHXPuomvUQJssFxAD6o/hfJerWY19v/j
IQNDZfx5wV9NcTLZZ8DJaA8jKXjS4gBOOuWmCRQB8c8yocLTO1374bWNgkC30oVCK1nUyJf7HxFv
X+2Fm94IYPcQrfzHVE67WTjZmZms3R6DPSOYVE1+GWKygwqQgXJ1LROTHR11w1H8FDMDGhB3+ecQ
5Nq41dp9XDxn8z8q4ML+3qeyGsFHhTM93Ck0MI8Bk6SMYw0dCelzjzfXhvJ5VlQe5EyU63UWK8fW
KUgHR/I0rpitNf1MP1Q9VHOy2L5/usaEi1vAJXNql7hEZv4C0p1jpypulKKUDzw6HuV4gsDZnbHa
4wbYXZxUlJpdSPJ3kvYJxgBg638RnOAiQ7ze8YRHubwC/FEl8ppBcsX5CYHhv8dI0Qy8/wz2u8Jd
iVVxWWbW4v9N6JRw1hi4hHMkEocQTKCLxV+3oyhZoWinat4u6kmNRzQ8noHBWnwuaFEDjaEw45kT
wYRo2VpgBGMsMbi82Zem5OF9kvLCM8CcDwHsPuOyXzDwiu/lq4VZuCt+rkyXL2J5BMQ7AjWR5CNv
8yK7eHxa+SqmKo6ktSQFQm55mEWNn5QB1F45zadqLOQXi6sND7KdPFJVqtHu3CcbmNcELtEsrAsc
1jdz8/tDh50DRiG0qM4avoLNqKmO8pk/WAc7GNDk8EJJso3269lP8o9odtN84aRxHp+Mhojz5Z2q
u1DHOTP/WcHoIdXglqMygRP2xzLvQ1xY88wfB6O9xnaTglk+AXt4BD+7ypr5AFzdARHJBNn8cikl
iT2s+tNYHHzRrKAdpn84JqQTB3MNmGGkgWbv9JgCdlPgUsz0kdm6JHCcUe6UrvmVSK12oSPgFSeR
Qx/vi7HqHerruUL1wYCl0pgKFME+hVcgZcEWC1x2NzvCNxTSuz7Luyxs0RoU5oVhOCCD/t1ezjvy
24tbqtJoOkWLaxkPkEUwh1zMhesf80xPBnLsM1AHzx+BAakUE9Dn1u6gXyUcfnYkSv2xzM1oAFHv
FZ+V7HVzv1YKmNXiwnQsc9980BxjHQpQaeyXMzVgaQNgMl3k72yp2UDgF86rIB22K2ILCWgEzIO6
766Edsj9MhtKYwf7eLxrI0LIGxirnE/jpe/w1zXcL3t9G+GddA9W4afqzr5jxewHJChXD2CDXTGY
F312psCSUzf9Y+4OsWN4jz4RA5P2E9IO30xfykQc8sFCM57uRFnaRn9Fn9ktQbcwn2gj4M3itXAy
OeCd2ApsZAoZYhEummlIbWoNO7lIQAl3vIAeZMWWbU0/3pDyxjOIFWtbfoXyQSl8d+/OW9g7I/zi
T4SlqWZDgWx7aQLDNCmpCAhLJickmxEsCJmXVR11o3bO31BL4iSLALEh4sRrWVhb92hTz+jFTvjE
6+QTbAxPiPH0IuV8XkARN9EoMxETqZjNRzSm3Cwfc9IBbzRRQI2Q6b3IE18KQYyh1SivCx3rOYAy
bpy6CuNStypZxbGcfXbHBGGXuSTf4qeeDJIBDrQPV87EP2wHBYiM9meaQoT9NwzEPVJWDCWLgeJr
FXdQRWtIDT+5/07CDlZuyaCbCXhBtcskb203NKuOga8bgPI98P2yyzAjqnr5sT+V3D7t7sZPLVD/
ZqzvHq2p85ADT7fRs5XXS/e28KTgEi9o6xYH9lh8H8OGdUIxzTXmCezAZKz1jmLtJDXh0FUXYYlX
dP5h/Cenhmv8Rg+FUZnRMhboUBjAekcFRnGz3MEV/AMDo2l5TX1YmctqpRXw2gPW71P/1tA9TD3D
XsVi/wIXcBwNumcLWlAKGThoHhc+qkKviYpO4GuDwc+oJg+39CmS/0eYQ92k4hFkNuuK5cI6hTRT
ZBKFdio73EFiWUPIjERtJbM5/G30fe2C6+wk0TXj8zMo3XXqHWEiUZBM4X+/rAe8g0jIfe/77Crz
1MAoxP+lnkx7GJt37qGHyRvWKxDiTR3sDBypdTEDfhWJMjwH7hCT0d2AZT822peErlaF9dhqTrKI
azQjiDSD07K11APEra7NfSJIes+ZKVkisa81KLOSF5YCo5V12XhOV2hJg/x1nR+eZK97brnn2Knc
gEh3wvPQyxNJRYkh2DPt4BgC0p3BXtI/Hnb+eePROgTK3ZL5wC1JNs5237IfLunLzn4HU6GtMqJf
xF8EEomXKlpdxOchRh1VGL2nNg57nwjLkGNV5GEB+5jLf5fZgS7uNBsYr2N6osmQ1UmGR5H2bnfy
xKMMiGqn48bhAlL0ffnLiKQX+AFwhUjiPpvR1bTV5gnMLI5e9o4cxWdYsc4a55qFVIwqeCjdO+Kg
drCNwbKpxmaVTWPeSY1ymK/tDNmi2nRXRfdgYwyRSkHE+VxSGUhBH2Wm3lITZDxv0PpXVw9m+irC
qlHuVO0rWkAEpt0osgY+7s2kCUWGfcFpptecRHLRqmJjLLDCZyRxfY0qgYjtwE2IDM0YTE68757X
4+LXWh3aHD9s/M03g8R0a0CVy6QJoHyHewHDozoKTB9EbNQBd0IuL+M0a7UFBiSl54HhZs+Rh/uk
tKFYxhxsuUGdQQDwBMb9LVRUj7eSqe0CsQ5Flqlrkl0UN7I9C6mcv7815bUttMHEu7+UTH3Jf8rD
kcH6igmK8+pBIM003Y7CMn56o0YskmzNv/T7AaNddenl6Y5kgbkvFqidLEtU/nYuKEh2aaOuTMvB
8X7cPa/iITDINLjUDlkHuwHXNxa9CoMiZ36HFbBsOwVHRwjJYxgKl5Q058dhxDoTJj/FaGmmyZbp
obFfq3vjfRuz60StxWQSW4coIrmCHZ1Cnz+qSGOfPbgfIRsuUiG23iW85swjqwZBDOY1evNtNvv7
T01Ga5uHkCHfhBf8TQb7Oj7u7r4U0wTIQwe+ofqL0v+MxD0lVdmL4zYNtMYiCJp8ty4TOzxTh7c7
UZUZIdw7mLdGmPQdeukdjeHGPc96b8CXYtWPIwC1fPbfnVkv1ShfRxLYgTx9zm8SchvOad3Qr9w9
3Cylh0hdQ87E8gboHNId/V/iIxeEHsOed1DK20+mVPUzfGAUZ+51bn48GwE5f+UKdsfOaTVnYL6n
Fg5oLX3hwiViowP78Do5B6DUhHBJ4JNQE/uS8iy5r//JD9zJ5Tdzo6VFJubUZAb+deMGWGMCuGxh
o3LHYmh4Lu+xPUqCscC/O+KEVN2iWSPeAz6ddPB1dFTozIsgQtItWczmbJTUTe1+xynqTVs/+a7T
Vg7fq8Y1r1SQx51nUrAyuiPMO9LJ4INo1Ty3UAEjM15nOTfaG/Cr3DD4EFGDlWDjcLZvNM0HCJl7
xNo4xWozeSC9owPcgleXsMTHDByoiaM3uR9jlPmROfazwA2i9UQbBzKqsyCEyULfzS1yKdbmssFd
kMleyR6B80cyXJ53eGZxfvrQlbzSHW6EOt2ivwwq5PgKJ18zHED0L8nBw2gg3FdViWH7YXS5Foka
zD3ZJAxpp/dA7H18u+l2wR206RFOaeDyhIpy0MfxlO+bDO117aoPwVN9ordw04LgQdjsPhXAEZ+l
DhEvaURHsF4aWvh2eFC2oKcJ1cFMtExeQqLOAs3kp7e0RgyzDR/4v1qC57GSjROtsnA90H5QrZTZ
b4YD5A+apOpx3Tm7wbV8EoECJ6nnOaCwjjn7feOvODNW/QK7w6b6T+z3nVIxSAIj9UgOMBgvJuiH
eY1ss40QHawoWE8+qq3BTFVrJj+xdePwV8xXWcd6pw/vfjtR16spDpJrxIi0WLiVhOaiKrqHggDS
W8zsFNsBPqNBQLw0NA2gfjYMVNr4wkzCQKx+CJGEXoSGmCvb0pRA6IaetnOqZflaoM3L2vGW7brJ
Nan3whF0bHpwocbnFwINMG51rE/+yiWtAqLpRRjC437N1rs1SY+DdEzXaCx/5px7jUsyJhSRRcmw
Ft6SBdni82NLeCk1Ee28tFhZsh9xyB+kg9e+cX4+raWPWga2fgwyBbAblG50Jwmeg8FY2hJcFftt
oyMwhq1h9Oa983ZzFCJhwAh4oRWB4nvTiECFiKakVJ9LxukAZCtGtHJazsuCjhCcj3wlQI93N3NN
8ZOijv00VF1ZW8vuSCb96Lmsd1xVHbgT17o3xSiMZhKv0Fpd9BI0k6ODr9daKHMmqL0z03qsrunH
AtBP1dFv7G2Fa3mHiAJ+d4VhGSoxgDBvyqSa7LAjEeLAV0TfkVvjqrlfH2/ci/j9Y1S+W9soaoXj
0cnH7ZQkFvarCRA02hVAa3Zog2UuPfq9PrPhfGn6RZkA338DaSUI/db/y4hajmLON0nTLkh1ei+c
hp2jSDSxyzLnet9Fw8IPECyk0XEwZCzw6PrKxmRW1c4fyx4KK0vXESkTj/OpDH3g0e4cvoZpyeVi
uW4N+EbkhsuFUVCvabOLq/mHvoIrNxAdffHsqGWsX+BGKuDHOwH2InoXLNSlTibd5avgy4GSVxaa
aT0CILWjxeWqzImjqwHtgMiWhIFyM4olTvzdjP5SEOyRCQzf4tICvhnnH2HOxXjE+8YW9ZXaSwpb
yZFDrrblpUMVEDe+l+SNXrOmw5x18mXc6f6ckT3W0gAr0AEg/5BN36QOo5ZAfbTNJEsrOWvajoFP
j/Hn+CCibGqpVPIpqM8Piae6YvUtgPxJw18dFZZewE+nmCdd+avsS74nNk0E4QxVbSy+HJA75al8
4BE6UJZXfPpCtONQIcPTIzjnI35rk57TWYaswdA1CoML6qKr85+OQfDMY5R/yFmXbNFlaN5MSaCQ
sSNX32wRUpK4pOuPfassnWXfLifakDOD3lWubuB2Fevr0CxTRMJEh0+74zELD0eHUB3ITHlL3wOn
PjNxmUhUxgd6M+YLT5eLiZ12HV5xrnLf8w7DHbq0lB4ezdnZNxiQVP2xLO9y04hd6dWmI7Z5EJ/2
uAaOivBFM6g/iLZUKQXoV40IczNWOzlrdhvUipn7tiG9DsqPpVjEOZ9zah9ze8nnxTrDZYtIfm2R
hvEi2VRaPP8IsuxBhIpDl/H8ngbyqpCeuQGm8AS/vGJjrzzWIGftoTRCX3a8HOtDnXFBMuH0wTgJ
aijaG0uh+OMRsPvX4/5Io/+quwr/zDEqLxgKXmlLTOHdXQcUsPhCLqp2ZWrh7m16HvGXZWp/ZSwf
tQO4jWrgGpxJnJ27q/KjgvoXOjw0vQ/1Nzfiy06LSYrr8fgMq3Aauj/5rTfSXgpF8KaINIJA6aHH
mgSknKZtoR5DBHMexXZY9i6t+tp0gAu4f/Mi1jeVZT2mrcUWHIdt76Cun7esLhhuzeb0gP9sTiat
dQ6njuZT4PE6PmUhypmEkzvafap8tuttH6+HQJf9tkM70XWj/YSqkV6oQOPN1eqS/QqeEq0sOn85
1nkn/sxc6hk8+5Cw7udobgVzYgKOOi4FkQwfsKnQMnFtq8GCyfknfnEZIVSvyALZc62jKsfhoTUT
B3t4R5VTuSh7Vk1wMlHFNxfRUW9bfbpwIHiWmWHsHWY1OnjfrvGIyIfbo+Cjc1ah8mfuBVq1rMMV
j3FNlNxKUH1VMrdOiNN9CBVzrox9fdjXycVq661E6vOUfkW2xTUiMaQivmRULHRGigmavs5T6bVu
jk6EYgCmPtP3Xr3z847V8eA3M9bYVzcCY9nH2yy3hYIJ8X3dnNSn7AV6FUNxu1sXEhyBm0RcIf4U
tX6tbwONHdwcKQSN3OQA8tgWRXwTnqQvwK6DswETe9gKeyaHW3rY+CZxDmlA+w1nk8Ffo1ThUKuN
5zCsrSLd7MOTdCPOimkDsRPa0RtZalaL0Dv8WXoB15StRpTiAb+fJLdcoc8INt8O/w+V6/yEkQBM
wzJCiiN8sl/TDAsDWSWgGUH5gOuPli21DKUO+tQ81CI6pOtSgtpQEYfE9ecsvQWtgx34nazQDVce
ks80QiH7c2J5xazP1w7o+O7Ii+J/kKsjnUSqNw/rSQirNAAYohRZUAAWtZVocUxl2xJX3mqRq5B4
RaCjAlELOCkfFfV6kn9X06JqBjkr5wzt5ufHevKiXbvfil1ZFW2e48f+eENzQSIa0gPwik5lBQBD
NFCwcwjmQ9F4/Us4VfuqHB/ff425E6xpVOCCjVqDhO0oPF4J6e1F500DHkhilT/HfLICw4r4u5p3
1exMkd6uJBt5TWzO297AL7Yi7DGTzxXek9RncGxHCy7Qp0iH3+5YYgmJy/O5RbLycoIFkJNiCGn2
uf/Wqp+THs8h1G1jU3kV77NLLiRmksOk8oIgKF8isECMwtlQEt7Ql7rUkUGIAiHVyyAM0ifV5W5D
IeYJ+XtdtGFo22Mot+rBhMnR6DAwzlNaMHK1qpzQ5OBZ2P8JrqijmWZneDVPvm83B2e9l57Cz+u5
0UcSgCWMeZ18FJjMTVWO0Cq1KL5aOQJ4uIrj7QIvWAATv/vkBDzODungzf3+JR5cl61c8tVNUJGs
4afBOBAq+uoflZobksmKp8oO2GD0Y5dc6mQiPKWKotrQf95jKOV6etOTx7u1LTz3rnfXvHoon1km
NKbX/WwU0r04yJJSb1TE3Ru94ytXkAIz5YGA9WOUzjOkahq3E3KefMBTFqSXzjjdMjmRZEM1qyKT
duWXsA+y1eBI4p0oBZRRRVJQwryFM03qstWG81yHVXl6FAJbZqhXjm0dXDL97YZhNEb7vo1NwWAd
++oli+/GX2Us6wgqDMkZF65fsfGY8BhLlRCshYAIiL7SxE+SuLJd530lviLH44wP7gQx4GKoN/TA
D9gUXu71Nqs/OieBzz6jBaYu91IHx+2gwkYmCYX8KbZjD7gTpvULtRwOhzsR6swZYnUZGDX36oHA
nw0F0pT+RTZqEBuSl8yGAL1lVTGrzK4xa4HrJvN8SI60tQLzHPl9rE22lYQmm+21Onm8pSY7LUQB
7HxRX2gZu4msVuIzsLrowbiKERwOKjgM+LP4rEBpxvIMFE9lrACD2zXdo2fTtw0o6sjrT3fq+KOP
QQ0wsydeN/xRnWTuLR71cepISFMRoTf1avccDKpZMzx2wGRuNpT/XLDA2okuqPA6lSTT7AcoyhD3
5qqv+lMOXYdYQ8sIjcOiXPAgBvJuO7fOWCgyGiGyDsnZIZ7WtY334Xsw89o2ViCYW5Un/2OkE72c
Th1RW+V7zZXcGbrzYmeoY1VRmAeQn3DZp9ci9Eivq5nf6djGu752GLu6Y1rWGX0Bg08yP11wIQoE
nWLSWGvfSeOuZfLmYN2g2o/Q3dR3lFlfZlNqn/J8jSgLaOu9J2akWZEe7u3efvR2KFIwyjhuuyyh
9UY6lyNpGi+xboIggpZxxwS090RIPdCaE0Ey/XKSD6GAeNpuf3vdTiVjpbHIn2s9BofLg5bFqgZt
M9iv2pVz2/J2h84xPgLWrCEzYZOK/m+e6eU3uxQG2Q1AjH4CYGLyg6pjsIH16l07MlCV7yPa1N2Z
JCk9ndpbJ/POqtESuErekxMjEiJviGANAUkorzJGYf8MN4i5BpmPWXec+GOKOlio+bNjn4SlMzCr
DeDofmdNn4HxbL2i7qLc3QEQSe+znkhZu3ZlBtiTMz01W6Bwvlf9rJpTnLLTXrb4AB7a9ZN9nj/a
pJ3B6hG+1dNV01JQIatb12m3YNGm00bMzb0HaCFvkYnsQN1o39UlCZQjrPDwgCrZXGYCKEtqulkg
OQvZ5EWmZxrK+zUDOzxZnMdwR4bgRquUwoVvJlvbZ1wfE/WmM8WokBKJtFxISN55uQvYJBc1BRzg
KYDs6wt+YRKXEm2ucbQyz3YIxk79p/0fqOT9Ogs7EUonFwHhAslch4WC7dW7jPdwKKUYjRwvUW6E
pjDJUbZXxcN469erexBAqF8RwRcYOLrZ5JEozu6WOeNp+WdP/ajdB+B+5rzKTbJpJgcS5Ez8u8Vf
PImE80IjvgejmGEmSTN/v1jfs58Hqif7MXAnP0IhQ9a2ez7Vgpbm3M1Y2yiHEX1R14EBmAtTCof3
FO6nop+WLjPYzk0+QWttBV1pSZRAEUv2uvsUyxPJ6WXxUDGNEDZcuRT4F6WKaKFvjMxIXqvv7ysv
XZ1muqlvqNf/g2VZkCUDE+dGpPB4zESk4vdl4ckQvD6lRZ6Qy42sPb3dv5eDt0wnRc4FxHTadURR
bGCF6d6ZkhIkc61oZiLCAOUtyLDwnSUnDYGbEVo0/blaCUmn2pxLZHDyT7kiZohy6B7egv3hts60
Mi0yZGkZeXet3wAZp/Ms63m9rczakXaiwx524d17std4O2V68I//JZISeH1R/jG1u8FbgHopd4rn
UrNtQQ1SzQFx5RuJChF8l346npIkDTTdxVU3xDyHpDki1OJx8Ef1jW7vT5ho/FNK1aa0UNm/OG1q
c19GfAXPVLssVvNjuY8Et1L44AOzyggr05kR14fzWMX3jd2lscevfqP3rK3Ck1qSGSQXlg19jiz3
YPF/hDoS4p60WdoyAWY28QuAeF5iFigl/8hMM6QqCJ1R+zY9SFTdwGIt8FM88PIwo+cRIBkUy//6
LU93C5GIT9At32warC9/BFiq2KSuDJ/eGqgAaDaFgSRq/NfktaSsOJhgCHcIyg2l1p/dpAkfaTxQ
xwM+KWH1U9hPsLAtcU+WzOX3mid+f0n8q45hBquR3QcJXCoCWehLPYU41R2ztjJcPqhFpw3kTwbW
wjyFIqOIAoHG68DJEJn5c5pv6mjqDykWuniFoR0/S/1qRtlrcRHQBCv/WybzGm4PPfYZCbc90pbq
06jZlxMEAstKX1vdrgzlKlWdlie4HN92Su+KVARs/h2pHs0wYPfvRkMW41i49uPygNwcCZIpsxcw
iFtxI2G87KMaRf1nHVFAvln8FgPE6EO5N5gL86CPZRiYGWUcMvmUKqn7bSEdEoWyyaTjArDlsVW7
PrJs8fpayCYOt5bBT8q9GW17w5Mmwzf02HdueuVOgAfwP80FMt+EtprKkR8Wmaw/3f6HACijYwyM
3JHNdTmlC5FZ1er8y1ESVyLIIfU55Fa1lE7SEqguEZVEn3FleudJR52NWpFBcFReHEvksBrozGrd
fz1cTd0VdNczDBJ3VwBMeiHSr4lG8P+WpoWVtQkBzuadMKHtQ7W0aklgqedMlL7hQHNiz8JN/FpJ
MMyBi8uIfNQggG7JeEdOdhS5IrkJQ828TA2y9x+qQLmc/RrKYK1iNLoICqVJDYZ/XoTlusSzpRfV
QM35Tn/p0b4o5GTqW1pORH00aiqzuahEcRh3hFTwpDrFtxaTWUBRVKn+H3U8F42tyVD/zynXSW3T
9aw5UU8vKqVQtv/jrgYoJVNutz+dr4GuUQbbP4kqCNG8VfSiom+YeINCEaQCT2ROLH9aQXfrH3L4
3TeojQK6MQzd3RtNzlNX/TSGWf2LuqnolI4DfEiu447RSsWV4lrfEa03LQRz3iUmCwMykpyKXWe+
DSoSGaZylG2Af9QwuKQ0wn7YDPgVdk+hqpOrerHPIpKBT0IpgbQPMMwoSwpwB065qKdtR+8K+cMh
GNaF2fzO4hP/B9ypxp+Tpo/t9l4oN8b39wRFVi859vu9VFtRfiDff5eXpC5vM3mbyxHjlY1sVybg
CKVzx0YqBLIq0OoeyJcr85Kt9zEBDZXZSrEJc5HbFS2dM82nhWud/MhPnAAi7I9IBgmum4cDt1vc
UDNaeXw8Iw7AJ/2lu3PGxjxfBn5d8WU9Jtj1n9L8XlcZlEkWQ8kE1MOJjEdLCPelp+u9Uo1Z3RQt
zb5ytk1AVY+Sk2pEHva/9D4pX6NgdNzjrcYTmpwssSd20jMOJCI7mrFIZ1Is+sH5cSfZnU6dWpD7
4kkbduNI+itSxgtMJ891ng3T2zSgdGtMIRTWfDnxHbvmud/e1wmAF2jQAnHjiFRkMnECZu2gU5Sy
e53DH6gc/SsFoyAiYe+e5u+qUuAMv7K/OS6sU5bPmXAebw35RPFa1AO+8hH06XJhoTq/XC0pabA5
VFioalCYNrSaZRqKzj0T11a/sWZ8pLSAbmzf6Gt7KjrBGniU+zSfPlAT7xl2eOkR0JgZ8f1EzkA4
aD42NPEosxH0YB9cCOvidO/oVdfKGOcxuU56BVVW6JhRQck8+jaLmvFl8Qkuq91pG6d83exhoixh
cpJhOpVNtZmrsNyEQ6XYiU6R2NclSiwoXgQ72A5JDfjByhiud31LU2shzhpFa8sq5RkwhRN32sik
BKMwp7DAn0gQ+JVIWhZHoAIjMRh5EJfYgWoN4ClItCl50TFrSJPPxJ7Teaf1uUC5bqrCbtQufqpk
t7X/XHQhOOJnkklXU6+YrkDG4Ru0qyhZ9dIUcMZuAlgpkVU7rPGKXL/7H/a3cBx7F9cKohP9Sp/y
EVuIlJr8mbxSHmfON8uGqZc533OKLGb566QbWuLMW5iuqPVVbLNEZ5NdcXJSefKYJhyeBEOXzp1o
Mgyac6FLBVH2YF1/f0avUl0n6yYH4xD8ywAGZ+/0XGP7X9rm2fgMbnKPlA8as9h+tVj6A6U5MQE8
PiIiETKVXPG5XLsT5U1a/oPzVvk5qKNVIbzbbkva1vpmzypVn86wwzhGhbwllRDQ/2ahbgWCylcf
O6iy8Ea8y1QZNH7pEddQpSGj7wRXhYzZXwftDgykuxUI4F7GzuhtFBo63cnbE0I2A1bYpN+81GIQ
fxQN0LepEswiCc7UTX0RVsmXRIuUECpl+7td2b6UTnl367y/qIsFyONRaMUzdpKWbnbmxb178NoQ
hLOJ/Z2jUBnItmNF/MgCpVxMPKQAG0yFemqXBLsfMQwac/gt/wz7dQGUq1ds3uRslUzefZnGtlth
MCVTQVgD7iu2V0WGD2pyan/q7O4hpbVWP2+XfC400aTCvydxTt2Mr/oV/tcuQSFKXtFl714JA66c
X6wNV4gj2En27VqNjRnpvFsFf/64AfmUvVhNjmxlgROhbHmLreu2MdyrMcYLeB8kilMmYJahrmrF
CGm6WDmA4BRd02EWErRr3CsGmCzKiWu0K0HkKJrmHl/KqNf7qoFr7U0V+Ozi1MdvMFyrZ9nPhQQG
sZRUNZY7vCS0kWFhdsd3e9B04S5ehPTciKANkKXRjhvk0Q02+lGPdMfW10yX0Bh8U51RFYJYBZCp
W0SBheU0Bbet4ilvoNIiXUDX+OaXd1hgaorMgNQ22FWbBWGGX10rHZ1QkqvHdvqGBTbUyVoTqcOt
yTq7wJNi4nJRBbql1QlTr4sffbCb+h6atjbncYHrY8eG/Bd4tFQz/0SMOIaeyefvhNfTILiPl2em
pWa8k78sSUciD8OykxsxuIgVxroDCZdBI4rn1S8em2IJ+DEb9n64HWEK/Esq4E0RnabylNBP3JWj
KgCGCrU7pfXfnF2voHePUrXVIN9tCeC5HakB/NzE05riLqQi6XiNgMUYaD472WM75jjRCg/pt5nc
i64FdJYwjWepSl2uPaj+6653yFGejN2Tu9E0MYT/gMX5WXlLbUMGLldjolvCPu/SSjcqa8sNsFpD
5qYv5R5NDVpB9PjRzcmi24LggVek+EswEC8bzaQTvTyo6tcywnbVCC165UMrhSIQAiKYprEW6tpm
65Zr3XXfTvLidEU5ivUFXo3WWV1ikH8XiZ43CpAscGa8t7Cd1K4YdhJAfbK1odMFLNkZWJE7PsqD
6PFq2Su4T6L7L2vVwC0WG+EKha2KczS++speTzxT9jtMMcm2RNhkSHIafzhuIXbNuYNwO9FrYNwt
rFyyGPr0aTv0RQWmX2IKM6eOFqEkHw5qsNw31TFx8/pE8WRFNvBm5obk+86oCewNhxwtff1BNij+
3lIUgK7Ha9Lfe7SgQTD+rr3OfLBWgSnzQ6TnD/JOqha2KFkhS6HM3i2R0def29OLiP2TsBrVY7E+
QE3/vTL74GAxR845ompSvKh0gZv3tJJcls4fvt2DcZb6uUKS/7RFmEngo9tMmkAbp0obmbmFPXgu
ihTqk0FvTQAR0M5ZNQE23PZ/vRaPS5R7G5vfcPyU9977TYyxm0kPVuJT8vPVskEoNvVoF48boNeK
MAo7A1BfSd2FQbx9ZHmcVJYbYkMbBY7ua/Y5744GBDPnSaUeer4G/hagRKKWkPCdwpGWf/KPFFR/
gAp0YPQ69M30TPzceA9QwEO9N9fgyK8SZ4YA4VxTbGVtisNCvF3jD0s5leC3IL5reycih3R8vK8J
O11qLhienulNw26JO36kZ1t+pOorRupzyvEeoSsH2LB0E/LQTL7htMQwuQ17q6d7/Eb6qcN863KI
5oGvW1qHrTSdM8ywz4M5k36UcVnUrLHP1jZFXKFWDp+Itey46kKn1FfX98gxkDkWW5Q4BOpG9WzG
0gBwyS71GuXXVqMzQlFePOikMBrL+wpz+uSuiwCrMo4gT/hm2utkfVyVNkeTqebP+oZGsgIEAzF+
ZsZ0Tr9MoKg3OUkg90hAC3ZY32F7XlzZPw06am434SLWy2zY1omwLR+J1sxI131mM2Xllm534RQG
f2ZSuU6l+p50GilCM0zYqSCjCJRXqHbGzseDNPNY98xzJ1iuPbyMOIYYE0h2joBbB1jBxD0syESa
ZrvrXn5PF6EMHQ5MiK4YW9Wu3qgE8+WS9R3v0/pPhcNun+780gGUXQswAa+cWNJ50dZwSh5f/nTJ
lawBP9QazUJfbc5aIBpqoFZpO2M54A9FauT8m9II2YwtnQ9qXoMscjuFtkSChCXZ5p0LabbXdbEj
968RmsqFxLWE/5Ua6FZnAPVNiSvAN2frHRDEdMVHvvcSZnW8UZC1p1qLs6bW0VpuZTotAtfNmYmZ
P8o2zof7kKGr2/vTtHUCA7JvdSvuBCDc/Dyx+E3+lZTImFEBcQ8PG4i3wn7YambqUbe0Ppxz0e12
DAtPbMX/U5s+yjd1gQiTlAvlwqx8LZzjHGHbizUIddFBuMq5p830DtqsjHBfhYFPMQKRxtMIhrHW
xSYugy/ym4TGi/ZH/RSsNmBdHVF3vnuWp3+6Q4WLyGt28WPDGP4LjHYtgaCIlmRXdXVAIr+iyHxO
SmS7Vr260DUNN5C4eukzaHRPc0nCsRPtM9cdpWl/aJUoqFsXo2kPlV55f8angAJb2xRJ2KLEfne6
lOImfsOcyEcPBzmRNBJ2w2oo3mvmu1a3/Oo8UHNCuEecfm2Ukxz32eyyORWhZKfcSRLlVo+NNaVk
/sP8e5rIaUBVFx4oBzJ3Q9+Iiz7kqXC99ELWOGKBZc1ZI/Kj9u6cHKKMlnFdHpBH7gXGnMtOunP1
PFvmx5MooLvxZ5ZDmqVG4NNfKoPeLfrDZyPrS/8pVKY8B6hAspCbewTphW/+xfSyqySfkYCCX+yN
I6cijCrL6clfm+pao0OfcuqDu4dVktaLGn89BkjaolaTtdtWIqLPSQlrkmKjsY8pk7Rdmj0SwfIi
T/E4mdZd6xolkzdCGnGsMfW4IcnG8miQaoHFVRFWpMcATcZTTgG9ZAjafAvlPjsf4Jqq2T0kpJaA
FS7ytTMVTgxB20mhxpnmHA==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
