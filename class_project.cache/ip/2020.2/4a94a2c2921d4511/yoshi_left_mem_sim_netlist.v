// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sun Nov 21 02:41:36 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ yoshi_left_mem_sim_netlist.v
// Design      : yoshi_left_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "yoshi_left_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [10:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [10:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [10:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [10:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "11" *) 
  (* C_ADDRB_WIDTH = "11" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "1" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.35235 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "yoshi_left_mem.mem" *) 
  (* C_INIT_FILE_NAME = "yoshi_left_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1344" *) 
  (* C_READ_DEPTH_B = "1344" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1344" *) 
  (* C_WRITE_DEPTH_B = "1344" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[10:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[10:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 26400)
`pragma protect data_block
BZZ6ozXaZx+IdKUhRql4GDNkVmNCG6QnYGvbLxNI5Q/Tzz+aLbwVixGos6MOg28/vnB8SP1pkFis
Sebaffaw4Yd99Nk3h9AqpK8uN+uvQvK87dJjiA/Bn4GVaVpffFU9G7qrnPShthiFD/YAutN7yPeb
vTawJI3p4m652F1OOKm7KotUc0OvkqQkT4FbE+0aBmemFcMipfs1K4THIDRRMsIi4K+CTGWtTLPy
QG0KhQMWyLROFy1ERJav/buKB1TpLxWgjIA8qP4CgwGgw8m7nbGiLKYpNLWFHHm0QE8SkIQTOhcU
jol4Xq9A1YupFrmenfG0V8xwS6I1ErK5jVUQyhd6SYNcOO1+yQbAzqX0ITB0Nsoqblrzua8M7vZr
pBJMUNVK8w0JLiBKc/a5RCXfGLZBLTwl6BGF0R6CRWJ8LAOE5wXvwV0OAfB4/nEfoq4uzH4dESp2
jgpGHvlk88RxuAexEpp5zm0T4HQi452I593xKWQ8KG+rZFBQTIkqIk2+IFv0LNnMR2Y7rENfB9hQ
En+Y9ggdqkULFqJUeBZpX5e/O3u5lNhoHMaskam8XPwp9qFuVZC5aOhKxl93ZRanXvQcYSEs4ogV
962My+eVCtzs4FZGk9CJYbMj0VeseN9/rSU/EW93aMS9/Fk6+/Z53Tq/ZaIk8j2/z5YqUPF1Lld2
r4sfzaltMG+sGhkVeDQRpUrI7ViF3B28WXujXbKY1+ee4YLzN+l6BLY3B+oLCNxhi8ufkE3/dT0J
WiIuIYXHHcbn9/gQUYBivSwTqH2MsYoX8CdvuRQeAG1f0vNeR6j78aWRIbcq+1HEn1gc1IBSBLoZ
wFKw3XHm9PjwXMPFRRXN8wLcXk7/t/UZ8ojD3oRJTK5gq+L4Sz8+G8Mu8M9DKJU22FN+K4XEhmN/
eOUsbsI/gAbPHFRK45vmJiLhq2g63Sd+xM4wrTtkzsdnpt5kJn5cCAtmXm+qeyQ7GrbcfX9fZCFk
Je1e9RoFx190x8gVWZebs/ueQmZRcM4bLR5bafIEEF5WA40iOUM/INQxcK9mBQKb6CuHagC36mcP
IJkOFZpYp+hqdlue2Oy+fUY+C8gPybBcUCbcalSmYXck4gC50BdEAIgojhcBpMzq/BNfXvNxJ7hK
iDvkhxKo6yyVgpmB6LU2OwOa462d9XPhi+sEaXTTA0A3feBTA0QB3CFdpHO6V53d6OfO24qw4UOl
SM3XdqrbnC1hEKHXJfWkFg4yRm773SY4yMRH4BJxu9c/w1UD61VoDXG1M37VBxQIMz8VY7bGsXRL
wcoBX5QZm0h26Reo+/5At/b5yrOJkU0jq5AcxaHSXq9Xw/qgLfifsHv7q8HjeW1m3aQ35jspjlok
FyMBamc7gtQ/U0eo038M8/Bzy0+H0/sCZ8Wez6yQlPHyC6+2lbmOh3Y+v7y4wfxL0yvdvDJJeV3z
ImLnOTwb8Eo/u/4T7IL4zAv+QRVmsEugbDhDr4OoHSiYE3L/Rklt1Ejq8Rq09nJqwPl/aHCKeymk
4L8w70eYSdusO0AGkJjcAFlBeR3nkxDq0IXkfZmuXynniQVdpYIl0sFG3oZUI0EXfy2oQYe0s7TZ
nCy/S4e7nOnLBxcGFEc/2pfzrFtN5Q7YkskJYdRzLD3hAbHy50W28Yzz7FDc+LhN9Ttr9JQ47s+p
IfyqIBzMeJ9/BWtBAOTemHbWqH9dH8X9AivML4/JILTX72f/w5o2PXhedKSFSCMTrC0m8ni7zzz0
ZW0xHD+fFtm7BZ+XQj7QGQvJr7I1tn781XU+7S9H02L8IcHfylHll3hNsNUYZ8FPy5JB6PK2EjZZ
TEIA7o7nQZfN486AQNNuRmBdgq4Ksrco/205yvccMq+e76Vx3iPGeB5Xp52g+3jZcVPo7OssMeN9
YDYWXrm8d0FggfNoJ2xB0umcdmT2X7qpPmxm8D9Y49iZG/ARHHL2L6LqBPil5ap9MsyY9yiNI/U/
2KAAKjY73/ZLQw69d2rI/DT/uOFECGM2AikFAIbAKdTf4RN/TLT++r9jxta5228LVtnZrnuZKScC
onXFVTesmx/XHXb1hpyI8vOHFqrauH0fYpsq1b2ZEQD2MIJBqsfxfNFppagVqaEGbbGZhsSuP4Qn
9rM42edIpkRRAJaP60eC6UpgNB1INs13fcA8QpOD/Vd9k8HYtD3NK0WbXHBkwOmzAHZk9dgrQSRg
Xu6yw4VvQ1+FrQNq+9NqG/i+54ZV3HVMfg6tLVEghbwSQAh1ubiT3SbSKsDKM2/PlWFZ5QjjLi9s
dHOwkBFdn5CT86pg7A0+sJbr+KbOv1vRS6+WBXSwbkG1qBfEkluN1Uk+AfmDL6MKRWBScGme5lhZ
31yO0nP6ieYrAPghRsxaSlFKtez+V8hnO2kXMX3TsbT6xnGOj0ZUNPKGNh574GU3aWKcqUxkUHdy
IplMbEQcdFlPRnawK3HnoVYBn73UgU+6CB/zmx2MPm2kx00Kv2e7tpFJUv0reR0B8HbJFNvGwh/R
vvBUzoCvxzJPdawU4jfQSXmlCecd83dltjwUgf5hHuailB2l3gfow7A0xMTOA0abjXSKThMHFueG
DpHUKOc+qkiDZ0tTFmAGc9G5eteJ+5J9hQAkgtXXFOOXxIDuRlvCiKooOmDytqmGJMyxgJUFa72X
6264sNkOTkIf+jU387cqZMzTNMJWZhgSbm4xuIDiqivjs9fyGNNgNhdMsLexlDdKYTpiEXfR1UN8
gD7Fa1iqMiCxXiINFJ9lDcEGFqgG2d4azUwMgPW5hJr3ty2wyt9MvQnCpSQkOEPudAj49fYPg6Al
0RaxppbtcfBLS2Z5pFTPVo7P7Hi0brqXpzLAVAi1aOK9ObCZoehDmgU7+7dx5TNLJba7xgQT3lP1
9hUUAjX3BrAVER/pl+hjSaVk2U5wyb4Cb1hglAOWshUyUXOs40pwvpGTVVylsM4ust67YvBYmhZ2
1CXie5sK55eUVSMZ11rVdRMlReRHM/oqE/acaqV+Zd8JUdT5h4FCYIC7Eu4SOlsmfEmGLsqFFqdv
IlqLYXZijs4bJRFbYMrqYrw5ygEjJKjyFUQT7EDqXs/JmfZ1GI1BXVdvXSV/DqqJyvN5ET5RfOba
r/oFfoEFevh56kuMLKWe8pAZNQYEAgFE7unVewtEMo0ksbEdPSmnf90tijJuIRCY8Xqv5luFAbvw
W+nxwrUjQSDNe8JJR4DQqdrey90kyj+lceUSyaN5ikd2SqN8QftdsoVuH5uP/0Ty6LwFgkzvSySO
MoiuY6+tuuY0u0rkPbSriIl2eKcTxDbHX6V8NwKAPQYkhFMwEbOpqsKM94haTuR3DmIISyhEnfwR
OTxJrjSKh/IeFvd8PMWGWwOMCRsAd9DKW5lCN9UzGGkw5xWxn8vkq6GCK2D2e1eT4qDfVubLi/mb
CE8swLjkTwkFmWeX9jpbIOnvHH0dp1UbaAfo8k+vrkV27f4m4hpxJbKdVH+f+qJ3mQaKH/wWxCWI
HxVnRv8FH9FT899Vj29EBS7b17OmHPG+pP6lv2LiyepIuKimjTlh3qNAt2Z+sBBpJluU0zSfk2Q4
MMFPVz4m74scg7VmHOBVU87Wh1y4E1FP5NQz7oi00IFRy3DmXgHWFFT8AhmXzcz7O7iYOLD7obxy
f/mmhIpLd/1Kkou8RfZgNHM9TV8pLe0jRvJ13ULlXFtm3iJ/Tk0o3YEx0mLS+Sz1Ek4dsZ5CqvPd
s0MV/yE+rSxsM57VC/Ahpp0gx8T8vpGlDEKqvaSqJ0w1tHVgBFJN8eZNs1IhitrMIur2duIFAf67
NykL7fL3Hvyk3vZPHoDyACW0Sc7dF1NvP8XkhSF7t89g0PBpE00uTfpZBP4sXQNw+nIA5nnymmTf
pDNBxAY1Fz3iOWL6J/EUqQ+DF+dBXJVEEl/Tx+IIlB+peqyW+U4b+v+A6CZEnUZ6XeXRFkVpqg5Y
auU9BW585M6ruWfI1h2u0k2/CYfdh2Za1j0ysYlJsdOre9XGxL7HR+MiNQML+sY2ACD+jyjX7Z2W
Gs5Y2/iVqNbA7FMKNhQlPYZx1Qo7zbnBfHIMvNOFPyT/hYRIPAzTPIGaNl1IRQdMjcNYdyLg7r05
l2Wx17RA18lXWPjaVNU+GUk7o+PhXtRhraL5K02U/fBvFdsH6wJFHwfVGa2Ndavye5J1FoQoCIu0
qlsL5A0X0ZN0XpzASmmXqEvOqOJ7v088CqX/vq5A6NaphrWGc03nzcZbcUywCBdp0YAtneS0NXkh
cTsC43pxsgxZkkVePJdLTJj6NqXqNy+DFw4IAw7zOK5WyV5NC2Dhu5Z3DXsg90DnzToIlYsGUqyJ
IXqfXcIWDqG0FVz9QKKI/8EDfFgvAF3MI9Np5DprUVrHYpwlyHqzgd0M9h6XshpL/PVuATTdIZ/s
83T9ml+ZV/fueUyjGnAYRDWILRn3ef/VpWxdaa4wCnkU4XDWexzPWVtsfTXIzh0Tm0D4rAjLBNWz
sEwxxGMIU6wwYQHsTRLGV86hts492iDoQ1v+k5KmIKMUcMEchRTkKR0AjpF2clpC8RBuWLqqQno+
zbfGXF/jq8kIu6UurnpAGaJtbo6cO8Nmx7qk8/pwa07Wa6KwpYEp/f0+orqjAhl9DMjo4cYfVBVK
A7NjLH47cnPuyyBH5pLuQw3ZGgJncVtihLkx1yIgPW7FhNctQE5Ebb1ee0yn7bhjxSud8Z8PZAyB
5RqOZk5Cy2UNl1GUkplILg4Gr86fPNJF26ADKnFXXHTH0YokpbPIYIvIaEdAFhfdvyZqz6OT1d1c
DpIRo7R/8/zPIz1Xub0VbTXorAobakY9PLnNrSI46ekDPgDsCzkIt6ILGMmiy7j/u84rMiZjXqnq
Lw7Y0J1hcsKkt3pYqhJa9lLH+Q/Qya8MutQuOh0cgbI9PLuyN4LwA9LhA1YqO/2a6OqJpGCK7UTw
b7vWjk+Tj3yNHit1VFrgyUxUr7PGD7oxsVX4wn7IwLiIJz/tyolpu0Mpp5DHC3JOeDH5bcX37CAu
4jO3ciBmd6Alj18rUtkzcfEheFPhx3IPAt90+TygT2YbvjS4WKFvy+R5esckrqkt5XcDU9Peh499
Hxdp9YqdLowoQATEfk8f6oLXkxetwSbwofG4YT7/pW6WGb6wLKq2NHN/ZsTpJ4moJEa2JPsfrTZP
52JI1x85zpQVumdGhgOvI0Wxu/RGeNrDExcUb/jv2kR/2FvLZ9h8vELYIWC2btl2NotvR0peSCp+
pSSQIO+EkTCfXfOdmDaBdTZx+NDnlNiw7caipwhLwmS/wiHBfFVQ3K75oDZ39zEF4gZtR504Iz24
L9IqI82MH+Z1krmDEKUDN0gOLHVsuZEkV5Hjo0H8QZUWGu+9nREmG+mmrbM785Co4Vba8e5XgmX6
6iL5Z/gxiwT/t5WQK2KP3u0KHBCcEFJTRwYJH6sXomrl72Av6XrGQ9nG/YQuIeLGuE40nSFKNuYU
NWZRg0p8RxD5mENrSr2kEouqMxI2Ull8jkGiYBvUIn5xQNUU3S/+UVVrTYsvBdjHw9qfUUNK47fy
d+QPwQphTYez8YlL6vyha92h3cwGUTLjSP9XBIe06lFMqBH0iBAjzDLaMDKQ/Ec8+y3WLwByRlGy
+aYIVHC6jpLMlFp0bKWSPEIZLt4cLvy7ML++1HMXgn7Hg4QHn1iUFd0sNOeZdIKp1pURqqxCF6vU
kwT8FH1Mmi9MlGmkYKxH3tfPz69FvuB7zffW7kS41bJfFMeI2I+cxaBSGKl0MAnZxM0S2D8JNYEJ
0VmrDiU0DklV1aBhpu/gXbx84slwDdpTgNkw7y03+1PFjkL+Daf+B1uhMIMnkSwstXW5fEiIGpBt
HN3cH/KlHSVc0TQoJ+MZNjXrRoXp1rQycjacFhCNaleyjsOHikPFYhI+5UXW+AQkE+27qEhL4MoV
b7QX/DKEeEUG6NOKy7ZLCgw/0S+F3NyvA/Ivot17gL5ojJRcqYQo95o+d9Xw1FEVxy/des5bmQCK
ix/cqaOQPbphFvXk84A8hjoVu4U+ANXk5zyuwQ83ILSNv/rmhW6lIow1n66j9AR3WVCQ6VSMkskW
dyaNAGBOA/ZwFL9XgpqtcNwXodfFCR4MU/8u+M1WMLmtdueqXcELK3/sSWYPSop9bSYr1CPlkEX/
RGI4I+pabU6VlA3RrsccV9iYPQUzrsQyH0vZFhAbPxyo0GC9wGtkMEcvWd5CTa3DsqCrd8JjnbV/
J9Ke/8hTOKj9cFpCRAVlyaEx141bNDclQqvQQBtmBxeFBo8+rgCidUTf36zNVtqTnI/bo57+xtug
BZkaX3NyEgjZy+/wuY57MNv68guJjVdZ0e74vSylH/m9TEoifUSVbB69CHfLAnODeKF5zHpxMQ7P
XnO4NpgKbpCeKjJ1qkbEl+KKnifx1zyRZB0Vl172wrWkeyLZS4JJTNoyYM7pHdajccpOX54xJ+W6
rsAexiqJXwOQ7uckPx39TtLKhsrv1eiNGtdx5ydnPGLafqyw0iuPYwqUBU+LFapvtUESHJiLSmgZ
XI4vE2GM4AKz8Vwc9CU1niI81KJVOChtvMoMPj/QK7rbtim0R8cUUaZsMI2OkYkjWA3hWVIT/0bO
soB/NncLKFUzMy0r+zuRa2s1x419DOfr2ma65SlxqzNra5O/neFYEMUN4JZR00BObPeB4VvGUAzn
2ukpF5Y4mv2BbyOzDPqBYnNJSDX3YbRj2jjfPzsUxM7nqn6engK7ytB5mvRSfTdpWyyHiygG6UZE
bLqcrEtyJVrQTjpgvFfjEK594orjf7e6LnL/OpGO1ZPjvUQPYhdsZBFPbzoGt4ovOFJV+siIYJkk
ebSXnEscz/fBBE2QacBNkQJZrhXkkxHquI0wpYoI1eK9oDmY3VsY9Jnn+J/WzOmNAE+TM139hT1K
owCJozn+YS2ptEl3HZBTDyoIwSmkAcpuT1a/CACfn5zFIahwnbk647dP3Ixol/rZM/ZqSK2TRDZZ
h3p6n6LuDECqQWT2RUw8ONaJ5+Q8i9ADsVWVScz0rKvVLO53TOBfN9fN77onT+ixw0n012PsDCVN
sLoWYeqYKsc2cAJiQAXtQBGUBrSvc0raBRHACrw5vEWXf00sMuMjTAmXnWrPojhJHvmFSelxCfZU
dsgRbwQm8XFy8yM6FvIwm9WIL/3D1LdCY2pD5TQ7EXMbxjEaZv5q7KkYtIB3yzMVA3YkpcJA9EyS
E3Gopdd2wFNpU7b+b0+VSzaw7vhTNMRmdMwAQdoKB4KYpUoZmbItFp2kU4AyEdp5g4x0H4H7G43b
Oq2FmrVc0RfuQRiLX72hcPT0/e7QgAmVCWs/FNFYBjQ7rE9ORQ0KtJ/IDzR1HTEk6DuLKxgCIVN7
ziMSEffRKVXQhioEYdSmMUpCv8YVfEtS/jCUyvVV3ipHNyUltlXFePtcTtoVwnSJJQ3vXw+Wn1H5
8o/tkUa4qx9/x+fBBkDjHhZDGsqCEZgSQ0hD6vZVD13dm1JdVCPkDZOaR6PdNwPt1ptPVpVcuk9P
T4WeMEQU5HB/rS4DPF7U0uBWy3xXMJnaDv/qhks9sg4GUOkozFAPJDZ+mzWkPf/kued8fEuC0hlA
MeFCJZpro1T1bgz7tyeE1X1kxP2s909naL52RfPGYY3u1Xzp/JUx7Zj2JN/8pQaSTNel1zQgN15W
bGlSh+2HoQ7hY5xDYok0jxCG2K8vYuSn7Dy7K/vLKIARehHw5JinpDru60B5y9HzgffiVgLAtLL/
ny0+zaT+ojmgHSZ6b26JF2QX5+vJ/6TsaHkczrHrr51/Hv5mOlS/AwgK3J38lDip33AZfLNxe9wY
/2Yks019KBQq5BkarqnnlPXujBsUO3vJe86n48aVoCGUdWfJ3BNqkUFCHSuWzIjstSWP8wNamrxB
XfMqa4g1rEsnvzr7VY7Kf3UvTpXUPKHHLFLcfu1himoPBLhVQJ5bOCeTgzGPWZ+ZeLsx3u1uLjK4
9+I8QfqM82axPDFSy6Onyatg08RdF/ijmCmsyF+f41nte++Cv7qH5UQBQYw1NlhwpxwC4UAw8IW1
yglRF9PbeCENe3/dsUeZ61NogP24At+Uus/z5COrYc5aN1YU99u92RodOY5SywHqmU8z4uxNNm27
zWdGHJfwTbYqy9GGIzR1LKrPM32XbW53tJsH4QQ/Cpeos2f1U0HZnstaxdDnw+BTgmO2zV2i/m7g
KLnNAH+ow7flqqfIExk6M8jq15cyuWIITsSF6pXJixrv8LFxsfenFII98OQLbIREKcidFAgu3frh
dYKqvKtAOe7r0V5zS8HnxRe/IwBNurhvdWXEGZjgysDVs5msSZEB75XHPAhq74imIwKjABtI8wk/
Jxzdg/ute/6JX3a21qpZ1RgkZruWdf2DmHzGpwuoGEvM2bSlCs/qiVh+9B0M3wh+b7akqAyn2UuR
6o2rr1KdeTDKIQ+aINM/BaPrjYd0Kg8tudPqgGs9PIPNfcwNGb5bGPKHOZ1lAFVXLmDgfdAllIB3
1GzNW3MTrWM10zs/7RnnRBgaTtTu+HGLnwK4dySIeSmwJ1Hv3GBxhcvXl2Sp21zwrFZV5hblaI7C
bI0gIcaOJYNWlNOyreNJuMmdjQWmf5kf/XiQ0ThwXKYRWCDG2o9xrJYbGKZAqPD+R/QHJCRIJWTx
x9f3h9HiQSdBQJ5tzTXT/YwIWwCyHyey3j7/F3hJQR6RznSWyw++I5l3UWoQ/+Tmon7OwPmTI2ix
P68/bWMRyb1u+XLUuf5Oy1u8Bt+0uCksULGkQZHlU64dlNkUp/WHtkWaE6WK7g/6Kb75U78gSHN5
4jdW29n6Zd3Pri89V5atP00OoMqGIEZbCG70hGg786TAoXqR5ylraradPw8K1x7uvjtCVS5AW6bV
PNqG7Au+d73sbMiyWeJ8hzaDkdTRklPi9QMQ+C6IWt+dhlSnAUB7Ox4+YVVojX1SOq/euOtLLWYL
qpkejEfS0QXEkzmKwUwDPSqSN2wlby+YUCPaV73SnrMu7zPGcuVbf06yzHvJNesSqhvYANAcrBYN
K+Jhnr5GLi/i9fug3TTPxu0p8vpakGk6DY+3k08ucJef2UqPMB0Um7lUw/RWLv90soTcbd6bZgWM
Kk5xjQl3RJd2T1JIPw4k6zVwzI+MBI9+l1/NlffP0zdjptS+7qaaIEUc3onQaDFXOehs/ezvZCyU
RYkce4wESk5lhuLzWoRBY73bAImJkyUEPQt5l3Lw5sN0lD+bEWs1KVU3ZaGV6PyakndkDBFhnk8h
eh7i1B+/bzQpdkED7lwtOhbD05gGGakK3Yxpm/m5RFEi5zMEcB3xQqhkaOAy/OZ6AKHddwKzb0KG
0xwb/frt06epXfd8G/1xlzbiQhXq4mu3RNGOr9NB+/NbZN72WbFnwNbK05c0dJrtY/BazL835wrg
t58o4LdBDvQcyyB4hgK0w01g6FFuIsitpyWuyPGm9UX/QVRRWIsJEgzklRb8POEszPizZnLsQAVT
2XqIls4kSjeMe8FrtLoIu8G5LUoGNIYd2i8HG9Fv0y5H+9YpobNrTdCOBvQtej9F7CO8h6Qj1wn1
OwHfuUIR5Mpm8mn6Mis/oMXMRg88OWEk2yoY/Hjb95C6ZzK4bPua0z2XOTQJsuDh5tY/IJpPs/A0
jLN/LBPbo/b9sfyfm2shmF2TGY/uqMJK1+ugyylnm9H5W2tNGT0QET0OF4hJjbptnVtp3a7dW19W
/FWvSKkB+SuA1d2csYA5UWOAlaj/2E/z5C6+vxbmnXlYEQ66CVWRa9KFNU/rXHL9HBJowrD+/ggl
NsxSdK882e58icXSVXN4UHv34w0Fw0nhKiGB5o2mx8HJk+ftHp43vp8Uy1PfuDXyv0Fdnfp+q0xM
QIbr3CgzNjpA463IXuJ0hEQ5H+JvmmZiVIOM6jNVwD++xNdnIzHX8+uPjY3i3LvjigWCajwRlXYx
W1Eol2Rrd8jHvH7DUhprqVOjfjWNdzoDAcEY0ZP7NYesxAfc8DNnOFclCl4Ibx229zp91X/+itXz
fPZhCJujkqfchW62fFchWQBu8QwC/5wl009Nz3nGLCh58OS7DqNFJ9Ns0GsbyA+3PqjVBOGwfUFT
HRkMJz8RXoYeoHcFNTQgv7j0TXV+Bn4i2y6LUZWrxdJy12McIhfIqZZuXvtFt0gT2mfWYUFQynqP
i0YTTCTTYZFQ+0QcmqZuc1tWgAn4zzJkADr2y81iJDd0xboDkYn3HuOGbGkj5MoG0+NXXQ6Q/5vR
TZRaQ4/AFTH+g84bT2/LspLSUWx6lNBk9PK2PQCPsfUHD7x0yZu8rP7Y2bNlnTQEQnBtMTH9Wa5T
AHfhkpz5GFlTnJ6w/aOJzzPDzA5xF2jsmgdnLgxFiQ7ilENhvfk9aeajoXgpbkIz6Vdio7wO4RUZ
huoq6Oqo8tU49/bp4ebMtGKO65f0Q5yf7xgdWWz01mdjtmpqySoF9r4s+4sWsLzUtFFGZgq9OWCY
NgcV40UhUQsihSpDlyTWHvHc7Z/66+52da0/XFblCXhTyamNaL/uTZsgfzhmyL59933bwCuAGLQB
CLErv5edFIv/B2p2nUpgqKiHu5Wo0VH33ejhsj6dwbuop+hgWTQXbexPo9jM+98I/LoP/y+8epuF
aVSyiCKPfmBcanBdt+mfm5ElMEAns2cvBtQDjuAbFB4MkCI2dhIhSowWF4TTD8T4A3zepK3zAACd
vxZVW3nmMlPwLIey/SLvuhlZzhjo5o7zTpKT2QZUBrgibSq9NS2QkLlgmCz7W/XGltou31bQXvQu
VOyuA8/grym9DRgpoRIRviSKAbykM6AXn7JTTG9RBxZLhmLgO45a7Li/QatXzg5ngtxUrVCITpM4
VoF4sQW7yXMhVhPVhUgs/YkiG1OhwEsXVmAItd0cRqXHbzVhuWQKgwBbpo+P0wocZKL0D1LD7lWT
NobWn9osbOY14RIB/ypiubSQfZ4GnyQ7mm/EDkkiq8iKLLnyusKBrunVE/lLESDBMLaPiObkJ+nH
piAS0wcxaxpam9DPZmiKgmOR9DvAg7QY59O+Bijns1FlzIbw4TH09lJ25CoPlQxPC/TN0Q4Zgo0T
ipWnggCyEmgU0TdyOxw+gYfXd5EDU3GfvjUHPb/1CvnQE1+ZZE/yTD5jurF9yN8D4wRjG0ocafkl
3GXYb9rjdvIpHolXpTZWjeR3wEdL4K1/Z9QE9ukjTMEDCSZWxY+nwOWG/AdZ1GyBS5x7zUK8+Joz
+kMSQEXdwXSuy+0mB7RPg1FDW4Zv0B7yRRuwjlKo64jnXZnBh/fcf/Xn9JgRBX/RE2iK9hw3EtPo
+y7ub+8X5IimFGgQWEGgn3sG8bw4ZkrBQfBQFlkDCUmBprvjNIHhZZRzYUmo+ebdUb3A4IPkjkZm
FyqYdU+HmD/gyG2L9tRTUa7cvYybSIiznOExaTSEDKJeVXqC9sUY+6cmHUuIuzIFU+mmeK4Q5E8f
T/YXH8ZFxT9PoJh06qZyA20i6PHg8rRtkxd9F3Athy7lIphttknAE547k4WUOvr5Oi2Cb5KK3drA
/o/Uy+X4Oa0/9rceH3trnTFgiM2ZER0qXtvyx8sn8PnxmRu1CTUA8rnyMciHIwCCTau8TGfNvQT4
82/B4oXD8QxoRupfyZjKU80l2mdLlrEmgFiSsP+1njTybkhwaI3J97PjeuZiy8fLiUl+T2e9BmG3
ZxYmxshv4ovGDe1wYi6CDAxFHs1k4xm0uwS2dlNb0ZiG6Fn6Z6qP7Wi9AOoOQ9m8sn4dLWNX7QQv
SmZoLIR7IhyUdM1HvwkhT4ANPKXwbE5/MIcZ001QBhyGJ8jg5YEopue0YRJUNZntb3IOLD1dDJrN
vBbelPjZUMqupiu6MJGrjp0wcjgCFNutVkylKH9UhDib10uZvyQ5rOFaBNB/y23AS0cCp4jHivns
8F7eHnesrp6WO1iJsmOARvFIOO/bJBzn+ccSgQr9MuCs3jlO9pKLeqqFg5qukEFmcmF4fO3KhiAA
Wld1MLiO7TW2arccxfjUiwC66+FYa3arp8ob1b6my2e31ip+M2vOaHAcGUWlPWpGz8/zs1dBCXRX
EtU0lsbvG/N6p2AaxQwF1HhE06BYz3CJSpr/7+LrBly29QvP1M6eFQz7MPGv12KofXPrGPVsVpGs
r18qOFtcZMOnpvHWbm+lvdI8IuO1drXyndgNuxYwX0xRIsSE7e6rllOhuTfe7OSekWKF7zuf71qv
2EWvs3g3Kprcv0bjSG6Lx6cMfy9MH9hXvB1GSjzLVcUciZ+pumdKy7MmZBpo6JETju2X/XEGd6Qn
oCs1TIkn1VDJ4i2TbltuQvQztToSdKyEdoJHjFF57c9im3Ko41sS4ZuVHygS8Kacd7GZG6EVXCmI
QSFAj/2bdKqm/Q4zhkfQRObvKKMUaXOKOvlY+s3OhNKLiiRl5DdckZGdKnbiCm5pTzkr8D8leGxj
izl4lP96Ss+4mc8Pl8kTZB1M6gRSfJq8ipTNEstMNooJsMeXUE/5/KAxKxeLiaOXb0Yv9RP27LrQ
zYIGP+n5BqbLe3QiIqje5FuQ5Yh+ekYf6EF4NKRDY/RSeOsKc/nmaR52eTqedfKX0sc+d7zkwr7n
zVY2+F4E+KLCdQlZ7Tj6G2Y0h0P97+w8nFnzKuOdTyNLihs3CTiDIdwCTfySfVdF2MyLo/nMSXVa
U7CAI4Vm4LZ7VJYmlKaDAkoOffThF0aSPP8iHrAbqAGqIuzCfM6jJO/24bCAhV+HBXqagg3wYiD7
NBbB5O886o472YhzQIQi06nnE81i6CevRZRCs2JjfMt9PW/PQ76BEeTtteT9AK8093D6bAkr5ygX
JuCjxxDmqnZyRn0XjPeFJLVFy7z/2gqoegJdFmdjxC7+IEHITyer/MFxutptkm2BZ5WQb0zOUfWd
lTSBcu/UGNHJTTk2doa1JEwCb7J8kVAV1bAYcUqnuZxsOqP//qINN6AJx46SRQWwFqnG5MJnW6Xt
LUx612ULU2VAlHu6eENGN+JNI1nYVIR7y38jXEb0bxf511YGBHKtnIqbAdrnCz6lhsMqa9SPA1kK
rfd3qKmW8SIaWd5m5D2KQv7ZiNgqkxdg6RKo41sFsZ4beM4pP22CnRY2z/z0HqVqr6j9OCXwXIFW
CwDOY4tQQMdVMkFrIaGbCXNcCW5ZXXJt558bwW7Nyb8luBJohjgZMXFczsqexWJdS8WM+siZze+f
lzuKnYVi4dc9QcHeEVbgenmHeNQOwke//oOZn98nSLz5ydyAE04KvCCKQ/7Fv4P5dZfhCJZaF1HW
4Wtx7boUKNK1n6Os6LfXn4Sb13o9b4dByT0mLfQHqrQXLs1o6J+b3EWnq1RM4hEppstfWh/YmMlm
5ENQSb8reEp1UiLjCxq5UjoXhSBkFOS8ysdPur/1FE5o0tY6aOgdpNhahOc6avAv2yuCt6BXYixO
xeOFIK/0GphvLQri6dGa38bvUD+WUxQ2mbTBYIIVDAMvGSyVz2eoglhDiMOdS+tBnfN4faQ3FAVL
PIobmY40lw1coRzILNxM7H8XhtiEZpILAsnzJy7bNZ8gWFMMiz2Ht7O5nvdmH0WeBsWxUlHth3r+
OxKZiLaO7X+rx0ugvLlhnmIsPz3UgYQbEvKSjaNTavt2BUwFOkWhW37U6np9b+SsRuy9+iZ0UY7m
Bdj4hDmfHzFlPeIiYCHTeX972KEVe5NS3tMP2lCIGHOLF1sxy95rSr6PPJBtFxtIcRXkHQm5hJNA
HmBR2klpHTLk+yUhBZM3ddaoZwF8iuR07iU9PxzRQTUKypsA3+ya3yEAHB7swx83RLz5panjCLqs
sUFgeSFMuMSwh4W9WKE5Qru+l7Ucu5MoauBPX/+8baoePR2MIwRxfF4Ov29XmVkWFb/LEaEXtJjc
GNfceaukQej9TB0zGT84IWebZyLQDd9G7xtJoBSUdQnRUPINURvG1lMWdcMcC3YP3Sj0lZLzffEJ
Q4PC1qWj7R8TstqY3Ex2yp9stfsk2Z1N24Pg0E7nQQWr4s2JGqEcrmNvYdgGF2Z3Tuq1Dg5DPT2F
e9IPJMYzvw8WkCHOLJqhMMeWup0jJ2x/lCxnrlIedUy6XmS+vAjcKf/U74DEBAj1nb+8ExswiPj1
V5JqzKsncvB7+VFIveuFNipFcTKNyKe0NOF9+Yo8GH2khJEQFjpQtv/UDDTNbz0ozmtl8/6YeY9e
12thytt767iH77HAnzAqBiihxNvNwkyuZNq3H28/h/usEwhj2ZSWLcp4Pl+iMfpKtYOg4GcZ/nMP
z+YwaN44oWckkOECuJP+OaPwA7bdp/hyn3cCoGr4H559DigcmQwXEP59BXusuWdfSYKS0+4xYrn+
APFUhPf7oCR8FCxUjPtTZV0mTg2RvkEo1RWrlpmrOBbPr2WufQw/U8MCxd6Sy6NA8jEPY2MeLpAT
wvAZLb/WBnTZPMqrwWiXKRrVDi0op3MU0eBz8NdWLXP+o1GZtKiw2G6zVWKdi6YHMNxmssd9xbRA
puLqhHRQbLrFxwYqA2w2cCw3uXfXtRBUh9i9QPfDxXQ1lSJorYOZoDFqZwSYAOpkurEcDoUzUzcW
C0RFMv5v+/KEjU3RccydtAgqQacN3ltvmuy18EzQS24BxH5pzdokzRL7gZhViGCqpAAvxHyHAiIx
4QkgMuYTMqOhm64LpTG2lUuNYwmrvthA00vHoFecVW+V0joRCaJWPa9HCJ+O+7JFMP1JT3I6DbUg
3TfmjOU4hzqgxLnOR3RDDfKzuplk14KlWPvMQJr0klV9XZ7B2w3RREjRPLJ+/AC2Dpnkh848IKEH
mz7iTP3jVW2OuPoTGsddTDzc0EiZIojmdIefh9HDpXL2LJGocH8F6Y9kF4WAoe+68NryrX+aOltK
g3igqhbUiCcIdMtnOP0XgmMWbbxhUbSKSe1WpT9qhocZRsU527fOCmFUBtwTk66ymejFB3I/Id8c
T5ra1NJsIjM5zwkkTYn/XKCRiLrCQwUzaDPGbpqMq6Ao3ZJgpj/lr63Cp6f4izjc9Sga/gu4+QaV
zNkLkeODJjYmNVEBRPb/o8JN5ivDnYmmjeBDSA9cRCteHSXn1wHWWcFt27CUA1CbMqdw587NeF6m
yyKpaulJGX0A3c1zPuTGKJVT03lwLiEAk7cGsoDp96sl4NATlf4Cs4bLU2nQAtvbUUJHUuf2iLbO
L39rjMJfvkgm/6PDWa8KXBsALuy+kFfaokpvgki+cuj/7O/9dZ0rhMfavyd+pzcKTUKAUSWrJwV9
Hsl32D5ji4WUrqsSxdP2h7AAGFQXgw6NC1Y0OBmjOTdgETrTOAynbrnn1mazNFNmk+S2u9eXtRk7
FsSqyhD1ylRT74pd87Me8uoUnRzGe4gAEP9LXIzX82dZbeNgjyyPu2DdmXMNP2N9Fxcs6gLXTzUQ
G3Fjb0yQsV0fWSTfPs6HysbfnWmR1/uf87plPNmklaOky9PRcEtO4Bi+CQ0MzvcjLiK9o/A6pzUE
pFNB85H8hizoaNBsNJQlu6w4e2u7jShXBFH6nZjFdt/E3hDeXh5u3icFI5FR1G2Eu+kegUMJgOmv
rTdOSXjhqc6s9v8zQ331i2aBzjnwFO9bq7afPG1+h2sAJ4lM3+L5UfVKkJgfKfCZ5Lm5t9opLTVv
7BAjZI1gRh9pBtQVoUhtluhSii4TD1+TErqg9zulDLUcD2xleP3GW3nImy3gjWBgOIJLEqSoixiF
PvMsueEIYA77Xy+2k94XllM+eomZlbDYEDwN1AeeAzKzFPBYPtJf50Nmv3oz4BeIlDdrVcCL+jid
5c2uqcpxzvLMscO1OGRFxTDdNY1y5PoKyE61jBoPfBcxtGFkpglWAXtOc5/AExbyObU2R/7Qu2DP
LWg/f2yLkRJiBzabVSpF1BKIT26v1vwX4UQfcZGDRtahCSfzQVAtnHj3PWM0IHoOQS9uMjSQ4QbG
tjfvX7KpABKyOk/URCHQXhx+uGBQp4UXOnpDDg9Q0pVodVb+EQxBSFH5+xtc4bL7OcZkJ7lvJwIb
twr9SnZdN8eB5rMkCfz2W1cXH2AeuxJuvbj8yb23tHODL4J3k8b3EDtH/JPo70j1ZG+EopLVeUt+
X2HLHdj6BAvB6k3zgszfwLSvA37FGgeTbwyTk73gY1xNBZrREBPW2MFxn+a2NY3Tb1cAHXmJxWOu
8mdypYYa/FaZIzFrvdXSA6kVLvGG9XlrGYMdyzQUhjFlZWABWuwPEBcOIfhg+S6D1NPedN3l1n8s
2DNItxK396ElmzQb/7nW7p4/IVCJ5D4TJ2U12XH09FDW4vLcvFmr8Ek5LH2r4cDHyMTmWLdPffpl
j8wlYgqxlYW9D3VdEkD7Qh+hWx08Punq0LJN9kDWyWVr1AYjeanV0hJqQdeoE+Js6x+2ZZ1otk2s
39vgaaAvhBBSGL+GmZ1YYmPDhDsRC7o0NPOgbcl7X3+9EqtnWYzMjks1Z4dAo54f8cziCQZElZA8
8SLC5K8tD8j22ZvQuWKNocXHOVzifD3ez+YKw9fUzaXeiBFkcG9ZWJuODHPM4Q/hWbaa+2DutRwC
Dwws5FvZqoaB871T1oLaGz3I2yGiVoplvs2kAD3hXZgU9Nqw2r6Wdg8nD4YiZyT9cPvXgV5Boz70
9mILWd1ctLF6kT/064vjsp170Ta2hANKQQOOnKh4PeM809bLAhPpnc7NRbACBiBO51fHR8/QshAS
DoeYytdgjDbLIsuLy0Qd0Bt/hiLS8MM0Fdt7B3bnbAGNhpsnguQLiu52IeMULn1fij8PUVF2v4Ll
WPQQALmo9cot51uB0zDgRitp2F81/g873rpx+DfDFEiY6p3SUD3sQhfw9SeFvAR4iMgr6QN7eKgN
lH18f3t2UuR0kvXJf9ulLAUkDCtIAtkaKR2+JagdcA6hH82RUIymYDMAq3UE3KC31WmByENoZbn5
QIe8FCnYWsd3W0fdDkYGu+W1d8yKrovRSk9uzLftgCHKX3EFiHhihQtCEgtbUKDoxB3ZGHCGPLx7
M1nAVu9puFlssXDLHa1kryH4ZyBWyESYpeDPgTeqdN3RYlN8xOdXX6kfcZe23mvnyUYEc9MvEvRe
83BcGVbftFWptpKFdzuTWscmLEesXwJF5dohX/Tq1/Tms5mxn40IJC7nVyrwxoYwYN+WvGCHrw99
EW5TPNBAy94jRY8fTW3FLNuqtnXUZ4EvHQME+YlfOlb4qpyuTKL5ii4fEb9l0vEnLSTJeBaH/y1u
AFooJ7uv016SYiHKVom5EHWlFB1ePBfTt8C6xkOKUX1IkKQxd3gyOXg1u32LndjbkmJ7613cMeYq
umLF7J9p3ts34bVjIl+6DL25GB6wPkI6QNzJh/42ZsaYcVXCBhoHjO0bcCINR/WuUD4alqOwTynG
eJrlMRuqb8UHFUcoQi+lqrww9JDySzV4pfQeTBdFJHXQ2B1c7N8tmRztKFA3sNNUOiPgppP5HXqw
PcLFf417oEzz9KP0zrszry20mpWrQHTU4XP1XftN8M7um5jXNFH7CxNmZQQmlwOs1R28m5dSC6vo
tL3Zd4RRI5qQdS6QK/C7ypk32lrdyfbFZC+vnZn7HkJi3normAjEXnxj8vijcQ07hbEfEM4IL041
cNFN8YlA/6XIHdVkNgyt7v7e98+v7rOJB/L1RljEZndFurGoJXJlj/vJk3NfI0AsVYC0661cKQJb
H67uDRZzWEPLJvvlHGUh76y3eolsxhXtz2LQf8GDyhqlsEB7Tua2KofHE5YTvAv3c8DnGh9WnhCC
Zj4BstG8tr4deX4v8mSrR3DsSQPyasdkgOlXVJFx/5EFcZAc7xTPi9A15ij3ydBElSuthb6V5E4E
OxGowf6ToMty7rkhmqEwR7u0sm3JoWHqZBswC7xg61VJTzpBfRyegKhq5QPjNg68AWn5GCo8R/RH
6bvt6gCNMGDdK0bE5MGEHn6aLqLa8tiHf6Dxy0ykYJmnp7CQE1OPJfwFQhk1d7Hri2iFLW9B/kJt
UjAQBvXT5uQzqmcoGp4SK4RGN83/MSI1zaV1Zz6vagqtjubWk/YjJSnP5O3uYvgOisEWXlqGxyDc
FE0vlDwpVOQe303PVAAsxQP6cBalu81zC1hyh6Q7N7a7Xm5Sx1d/G66gSvT54zAOU4ZCFrvpO0SE
VO1ckiOF6aIerZ01zgm/7i9lQuWntBakrdseuVxBkFFElwhmqKSZh/MFo0HhbzAT0YP2PzLWuKnn
cDsPIEyQm/wRR3HOj2Do85xf7f7zUgOFEGhm6Zo8J+hmGtSeNe2SGLk49w4TbOLsD0cer06eCByX
m7hyE1BO9ixEmf9O3L1L+quDKaXQ9Xd2nyIYk22ooRE2nmdsQE8mzabIpuc1VIonB/6NHpnrBI6l
1AWA/noCcEiHV/6JHX8iWpupGXXkgYUhHuIQG15w8CgpO4/1Msj3Q87bH4M3VdgaJLbjvUMmS/Jt
KWfmmzc6w+ucJ+pDMCJ2055RoQoz2j1cZSsjdwPCeXUMLUDVeiRxQN6v2p0fgXquTeM5UDkv+EiC
VxtJllpuuJPCEYEqOslAlmMHySspfbhBD/DePJnTqnxSVS4wqzefEDBSbnK4eEwDUGMzkiJVY+5q
M1HoLda3CaLKtWjgWM8jWKgnNQSD19yP+zk2Fha+VruS3KdmFwhBzh4rBCIntSxFE6SxAcJZ6L5v
aW1ALbRF/RToROI+l5BUjVZV6jb7/ZXWXQZQlBIM1vxRdoaUbTJoOQGQTLm0dIg+9fi7sJ25b4M3
EZ35vN+Jhk8WI5ZHV7HIaNnHB1lQAdLAqFSEPdskv7k4YSt0qTQBmiOd1j2UYvpkXyVrpbZt3aQq
nr2asN5l96T0ma87roq6FJasLRKcXYN5oKaMkNOWpIEs+kfCf6Z+D3YY7ix57LDusPMJWEvgEq04
dprdCBwqn/0D0rlosOz7kpJ7BUXdIEl6L4pcIYtT1y8z25F2DhJXzEa4klg9Qkn0oYnPOiAxVMOY
hXT55F67TEU3ssN0Vhn1GOlb/bZ6dxU6gPgIiPVGDimJDqGXgGRe0zRGUAFssSheT1l9jMgiL0wP
FlzWPh3UN5/1d7YWbgDAbz1y77gOUtaefzdnwUvK0j9UtKDpQTCZe4CQAwwgGPcceOhpwC3SBoEP
Sc8wCWBAzT0c26GgeOy374qIMD2hzwqhR8CuEnWUDJ7wJNO9PDdOlPM7/8+KXFbgLC/amocXh5ns
EpW8KqhW2/T08ozMCSFs/WMq4oB2Yy7EeEm7vjPVUcQ1R83BLaDSSJCCLtNFJ9r0pLlR9pp96095
FjbNOw5KO9jDPSFEhISNwVKYlgqCjjdqKUZKRRMR+YaJAHALttwAKcjmDrPN7kSmte92eqaMPtAA
DufLyzMDec4wALGnvZ62tZ71LXMp+tfq8wbFP7oCKH5Sn9TqkUMvrGeGohEf6aaz2QVzJ4GnQAJA
tbQhTZxWgBR5rNxYoILQckuVIFlAHdj+dDHcZEX3QJ/bH9YUEeL/uduHxzZSrx5bClDRaAWJxAms
MiyLgU2oQMMOsWjVV4TO0IX/zfz15E2Q6x7uR6b9T9es3aSgIaVdaH316uAAiHKvtN/QiBkAXkmU
HAnljIHlIDnwEewCbgGdGf2brzT8JpptNdP3fpW1kTnrS4W0B5ZqlZH8ATM0saN909mylPZ70yqN
eOcpQmsqzairrjoQpQznXQxwGZEM6yiSKCHNZ6hbX1uC1YPL9ZU5k2FNts3oY2dy64k/LVa0rPWV
EN+af4de1SUBhz9wDhHnJjc6G4WEDAM5BoJYMacPKqaQShbg9hHLzlnNZftgPf8gdhJGKJdkhPU0
DtaoanSQQrVQhxqwzXOnw0iGTZ46D58Xt6famONcZu80o49t3uDYG0721kpeEJliNST1CLVOxKt/
zbIRvFxsefJxXh3nKNQKnzJQTaqdUFfNgvgbU+C61cupakpCF8vdvQt2K4ftdbuTz+L8cJe6E8gz
S5hVm8N9WC/pNNF1E6bFvOe444pIGgjW3v1r/knoMMmwJ4FvPPK1PX9qABcLJoqNEiXulzJLTyJl
nVvsWG5qgPCAFR77aa4qfrXD/EfFh3uuBaxX+NZiryM5/RkMl7qY/dD+oBnpzBQNeYM+HXFd84qj
nwdOX4cOPO66miz/RPq8AhLxIhj6O89m905yjgUAHX095GCa0VD7CT3zVqNF/NQkcSpfSQipJdVD
nrcLPNJIBk3MQQ3ItZf5XmGkk9G9Wt2Ol7n6M7oORKdTVvM/bVDrDVm9Wf3IJPanI6bmZp+HT0+8
GDXtAic800HJHu8XcT7qts8nIjx6nddmMfBp+3vRsxmjovl7gsl/LNOgESoirvEhocKvlodaOwCc
z8VVfO50KwinPpbYnyX5Swkwwbolhw7KdVJCID8PRbpGykVZHwvgxKqVyJHCJoQWXbadcee22Zi1
YQbDQ8mhoLxirTvvRIKmAeycACX5vQuWCseXB7oWxFRmS+rVkIEpLCN8u7hq/J1R78o5mChMmoYj
3GvpchfKFjh/5H/U7J3G80xAI5kV1/xTEUsMLVOolInHDtQsDxalKnf9kq6iqtJVWyEgbjZ8knid
6DmGM/EL9VeMM4H0ZHlbAENpKaujl6z/Z17QbsP5VRzaTgqoQSWbS+wAGC/hmHegCah8Q+5pdcPl
i/rY7VGTKfr8zIpKskNaDery5Qm6da+zsRHnAjBzhgjAhhp6ZXZlKcqx9/qydGmn1RtJygpnP3Z3
fgtey7ksVfdDYlKtoKjaWEwqfCVZkPAPo04J73vPzT91u0c9ZPyxDGGFLkfHQVf6eLe12stnfVaV
BhnZwBVoTTfCC+TklhXolQ3QX9D1zYmaH0FqeUX9FaiIJ2VdOmZowOb0FxFp6UZqEwBarstq7AnV
OaXiK60Wyt9n1fx9VA0YhHF3mtrFYhyW72UgTcMtqZiW4XqltiX/rqQqowI8hIBX+iAtV5t2sWL9
6B8mg5wTiJP690jddRmoxRc0wuYdjv0xd0DP9EquxiU/MaTbb98MGhmwz9BIG/REYVSWYTlhXpLY
bQZ0QjoKrBOJ20rMYMW/pjDvijc0U0NImNCwgkyxd8HbhqCeVELsr1WxHmmSKk5QP1j5BiUkFZ4R
vozYHcG72qgxWr15gmXKZejja/cFIMqpocjbsIBGpJff3qoLmgsSwdLQM2WT+HKplzRH6GkJe0vA
/xH/UhyYBkszfZJuGpFubeZNsjhsyv8QpsYtpuFRDQEt9QS6gpSZ91tzrKgLF1t2J9cGDnf97Gv1
pbPhm2MuT2mYw7LnhAb5kEloBVBk2fRGCxx7kfBBVGf+S7qfaUL5wFoQYKF5ieRY+FBSyM9uWLoH
h+o2hzPfk6qw7kBUCep5s2+a52dLanbodCpWPvMFL3z4lx9A7oObeQWA8tZUWBXIvl6OMVsV7SqP
XIAI0m3cMEwd9lag1PDRg+DEm2qMaNmhUJNSW/hxDFQqS7b7Mk381DQBSq/L+t113keQN20yieHB
OnRxpt3628WwqiJJk3W/W1bUYDikUtQwGAx8TZ/hhjt9fZU4SavJ+wvBGkm5iyrMlSWmfCgdQh+R
OqqqxRX/yuu1oGOrTUEQKu93TEaNSL/7UECxKW9mJUGS4sobpzYtUL8m83eeCrBZ6y5GFW3Mb2oE
h8lgQNrzC/XrGngP4xrs11ivHQqHLGZUvLRPnKDZrPzmHZnQj17twhBCl5tjb6pmlifKQ6mzSuoS
V1svIjhWBo11neNzlhPgiTGpC0o2eU32NJUtPsw2DK0ECWDGg+6sFuP/p8XbY7il6hWBye+VILj7
KNq87yMkkMZQFNfF7Xxvobis6KaC8dZ1FWAGy/1ETuVSqJxI9uRagTI3TzAuAN6LQXRsuWlLvQe/
Dq7SkOnsg8owgIgGgRG02nBE7GAoRkFDRI7Kz+uyU1QEtkTV/sVDiH8umTj7NSNyeM+Gawp3tUeB
oxanBlowfjXMxG7HePjzo1Ta7goDwb2i8m1+gO3Y1KU/YyorEqR+I7K9zuMMq/3AnRCkMNt+AzsC
bSEhKoPZPNUFM0JgskKvv1CA7iz4SZOo00hk7GKhw6pWHsLYNv5086BZ4T3WDXBuAoaY+CpSFnN9
chUAinWEp+5zMQq7j81hSFhrf7+z2uTChSw35pZqgFsk9BPRle0gcIYp49HAv/eccG/jZ5K9yb5T
zU1IO94sH/bFH4YEUIdt3FJ0oA6rpO6Bc7aNaZpDNNGQ34MlUuN5siGU6tr4RYSgXPh1RG/CN7zo
EHtXlHSgzcKXRXMTvJUdnTQoHhOanWR4TOcsOi+LIcdb07FGvKKZ3DS965IMQDcgf99CNgC6KySz
wgue+aBzJceZJPUT4iAeXCHy53om4NwBhBW/W/YFLTZo+cpPSKDg9MUY42ivlblkqESQFgwgJyWp
hlGvAc9LAlvCbjDHFSb+/nTEatlcxaQmn3pBtUMb6qQp/Gr3YP1jSWWsn5HydADMRPyAnsKfcuv9
GIYXcjGlDKenjj1wYcUpFfR6a6ljvTU9S1C5n9DWxHaMk8e1RpIJp5c263Y8818XkBwa2OPQ/OMG
VAUTn+cyC0hoJU3zyO8g5EMQZIoBfYiUtlvcOhVHRUgQpmFcv13DoZfuEbw9GUVyEc+SdbEQkLqj
LFydtc3eSxZA4gOqo/c2X/Idt2nrePsLovVCguc4O2iB85y4R6gF4teywUH+DNyl3Etv5Ox0I69T
KxaZFrHWQ7Ey4mhlH8sXw8Gty/Iw+mpKNQVyWd7b0405CShnZiqH6txLhMF3Qx8U4adqJ0WU5UQV
/8jWl3oYdpXbMwc+4uRBsMs587o/f1beXcfeHqKk1GrY0+ecD1c2hVSm+tY4xoMfYTeIMezum3OL
ufE8uMDwh6+nlG+3GjHzGUUSnY7vW4bTAkOpRBHDPr64KxU+BYon79kLkxnXtYi2ZBAanoEd7M8Y
3Pybxu1Ea1r4E5lQqbxq1mG2EaRXmqs028/vD4JP91llh4KCSrGPvZjROmngfYrqtBMZWE5Chs5f
5SLrA6IKS26yg8x2MlEQxCqzdjDfRmAPX5SdQD9wphquT1dKtxdWVlRwbknRV3ZCdfL1ielecdO4
PRXtlt8rdrkQ0OMeEhWM2Grk7p+PF/CzzFKVSwYXmIqXS081YehWdk9dFQI7vREVwdhtjr+ioj5O
QjNgQgkBpoMrQXvJarOkCuz2awqrlWQKmsGHC1IT0M3eoX2/UZM9gCphkPgfYLk5kd+BkUrYZdxF
dPx33MfUu/pjhVqKB26bt6GA7FfgMNm08Az1vF1NAYcC48RtFV8ERTulkKF0bBKiXt2crdX9JdAj
BzPV7vCr49n+KzHqQq1bUbwJKkbR+o1qmyBXzYbsQE1E0AuItO08CKnXOirhnPua67dpyiRNmemw
pnFA+zOXTstezJAOK+GuUN/7hsRz79C0vluYHew8BzjIxcK32dAn6zTTF47FoTVOZXvaUHBJxmqY
r46nqEiPNSNBzGwDs4vx6/xRohgZNEAorUdYdrZU2ZbVq9jXnvhfEs+hHdh0D2ln69HRKYDtkWXV
zFEsccL1F5YiFZankMQqdPYA+3dzrB3xg7LsAVxa0mWEb2ukoSWPJQZa896s4maGY3K3gkxWNfEH
meH/WfxuZlMez7B2z3CZqLPN8k8yYCVdv9BMvXJk2BcG/terKgY8ayLwSUIgZWoakX/0etX+b1Cu
LWya/tCVwPNismuklPiHRFh/4nknEAK0qu/tJdO2Mwcs4Tap/SXtADLa8j5QslMiwJxHZLscnVCz
HgHNB7y3+LCPvjC4fm2r14gTVdQCRoeF7XijYxyzL6bGvzHLGzXMYZmey4I8E7hc4OrqZFBAAejp
9YRM3lLpYOU2tKflkBv+IvjNN7JJdgoREVHiRWkOM8Dtw75kqDTktL5PI4SGDn1zyRJoqewIR0QY
jBLAdC1BM4lBmWpAQcWgfYRPoUBabCQXehPxaXR2Ki+TtVOZPKxe/RnFrRZNeeX2xgkoA6SLTrHM
csYsw2JVTqVy1xrV0UoKIhUPeslLKHSMlVrLcB3X91v/Z+hL1B0uLH8JhcoGvHGAySjJ2Wo4VFr/
bcoeCbTG89ZpRlQJbUrP7MyFqFSc0ez620+CGFqRerEPtjZgiOk/htRlXM9Vb4i/Ti5ZY7baXa3W
FBiJ2PtfunG2HcJeFGNbJNyP/w/PKwO8NUc/Ai+2n3lTEQeO4KJ4BBeaAvMHBqgmLg3SseuhvztO
QjQi08WPZhI+1KKcqqlpP99f9cERVEK7BiqbopgeSQ71u7ITPm66Kty45lYxj8DYTCmuo+0MZ2fh
rLSzHekga+26Re583JyQogW3kUNO6NsmGyyPyrkg6GALdp+fDCyDyOKu2DOvODdLJ0lOljWbMPPS
KxgTxb8ZbG3U+92CcvfmYNRjHX6SVYr9orRMoOCY0utoOlU3SkN5nPwlHLd5TEHBeAC60FzJlRaZ
8rfy2UMq27tXndblQ+TRnIoPeqp6At57Ehb6e6HwRMLPo/U+GBxHbweckWTKwI4HKDpSJXGVGD2B
Hv9St9CDUvGLfYHpPb0PkpBCg1YzG8U2vZ1Vnj14cszURBOY4kZQZK7SrcorbM7duiu+INB53ECQ
tTQrgNTHwjRQ7+0tux9nVNO2H7fIuyIJYFwGg2mIs07P6cCZl/zxbySBkcJrVsmdJ4VEWhS+X+l7
CwNDYTDxyQuu7ug6BW+eBG/TgI9M1VOxyozz+HsvA7Wfz220md/U8qnPabL4DYGdrDNKajjBRHzq
V0a6nSEczE88lUNrX0cuW7rzVuDJa56DAyfZZJ4Q8+ZggKlONc4x00B1Vb2ya3qzY61Q7Zg/SP9H
NPBpiPo62v1WHVO3jxUOUtPymWOIbF+6LgSxptWll7pK1/IgsQcp4YHFFhYFtPmtOucsZ5p0j3Id
/FngpuAk/cccnDrUcjZbg7zyW44+QZDovsO4DdsEfWuUPlvDGnGCxuvm/M19Q3r5MlnkBY+yz5Eh
sZdIeFfM8pNHPttcw4p6oniPKqMU7iLHT90R+SgKf9cDVUp3BquybL/xK1Ihpj6V1/yxy3xclLM9
LDlT3jwRIxaBzUMxpOdnc3MquyMT4XT2VVbeAaUj2YX5yn/hZQCXNv2HLofZPp4HJcf/ehloWBFS
ySI8a+Y5+7MGm7Mo/NjZi8VEyz3Xu0SBKn87q0dja2wotDhe09sDjaIvnMug8FiUQ1l1tahT4hzV
2tsNTC93lVg8eQ6ilO0XuOMi2ex+lTUGJf3kSRUmeO41KaQl4kugJksl+cTCrhT4FY8MzQCeOFNU
yvVpQRKt1X13N4XwBRxBdmr7CtmzAhl9EslwvNy8qA7pSgG404VaotRiE45hh+qGFyQU4kQVNT6J
8KBIfK7kbb7/y3/0jxW0+AluLPfgXjXw/8Sx0s3dsB+nFmBhE5y1JnPts0bdJ/I9yu9j/TjiwMzE
QF7sbt/P4ePAc8rMekxsPKu1AF9vsrnukccuFf/WflrbSgaJu8vnrFfFF3fpcr73ly/Rx2rNHfqA
6RhAgpCgqrb92BmDH4tyuUSWknMZ0yVUWcAHF/EBYw7l8FByxhov8gYMuxuILIG/WCeDMjdD92oH
hSjkHJr5e9BPxUsUzSe1wfrEDKMZLfpQQQB073QABLI99Fqp83BtXqjoD888rFJNo0CrK9/WSO1c
sHpBFtkS9MYEpkmUK5TFLigNcltQL7sTfah10y0V55wXHFbcwvOkypCeBD+Mo6uPlxNmIRbT8GvU
cDKqUOYIdUf6z89dI6kRXPBUfoy/UYatsoQkkdTrvTh2ViCLfcmEAgr6SZx6/wfyjJVwIEg2H3F4
cZJwM83nhalHTAw1DKgWyNIq6qBtAn7P/B5XjoSi5bxha4v+oiiOC2Sigsirxi3HwxUNp9iedAoY
FOWvd1FJJUMH0/M08fXoVmDlJdzi0XuD86c9iDWfR7q8Pfb4xvRpyCwe6AfUf8DBzeKE6bkgMa4C
8/3zjeADJ0nkKr/MAT4A0GZAvBcai38U21Nj5Zzy6leiNL2T7NP2tsHMSrWQTlWx5LnuzQ2QeZs4
KlClpE6lShruU5paWqQ+Hq5RxxUNgR+84GAhcVJeZULbH0gBpXN8yN+xX1dIKdcPXiuG6OpqV894
6IYmdSA4W9Iy0KLWqdZvLHd5kqlvUFFJHk/0bTbBQ39AedRcr9guqOeD1kIvDrf2YbnYOoF3FGdn
nDXGImGjuhWQMb9JKNrvf4FxOXCtEjDr3+C9SJg74ofxz6kuoHPr2/F+vstmGLXZ0NiAnxz1xmWw
3t2rOMw57DAHae4qyGdcBF9ocdDkjydNv4/ljlO/Qd4f307zKQ1MEz89B4C1m6HdiIN0JszJg1l8
ZZqUw13ePdvmF99aUYkESM1L32hazyh70di323ysVFI1aDk8V+BunpemoAoJb2YLDnGJWhhnIkfr
7ycDbyS1F31MN8lqL5W63Y8xqsCrl604mn89gJoZ+EoeQvTQkJbvB42gSow7rLYokjtyaub/oX4X
DQtknqaWbzc1QK4zbE7Qx23tiN58Gkt18HLzuuiMU3lAQleFpAl9WWKO84iQ+Ynw9EZcQZEQKWxX
ugikQFSdw5+cQPbf6y5WVU1V5WT/6uZAZFQkm9UQWC1lPwZrgmLwWv5QLKaQcU7MgnMDBpliWDAA
5XV27wicrAbePtHan668jTR5kFHsU7hCjlDuhGnLm0CaKz2/tYDoMEUTG7aOC44gdFNML9BwQZQE
iD0xY+G0l/WLpAac1lXy10Y/yg9/xP9M1Oy6qPaGD4udFVeNAfVvmsukH/xLS7j2lBqw/94SnAqn
f6+hoWYbJdWWWmAoLm8kfknYMea8Xf00yRwFnK6ZObiaqpFtFl/7CMr7e6u4kQcgMDth6lbmSGmJ
/HDymbnSFVreRp267zbGc/yW9k5JSqPTP5s9AW96ouv3aWu5h1c/Des5ful7XUA5Qq7NA2QdLSWx
E+1lLz3NsBTLLnH6dNHV6yh7Y+b7JG0mQ06ReN2941llKKQXjsmy1K+UAllhb/SNsfawUzAWMPe/
4r9nuxwdkf9XdB5+7VRTurXsI8/o7+eNs+2IHBBxnh9tO9dHb9OldRK3yHaa/qo+aAbqHmOeCgO1
5FrG563dtCxGh/lHe8unsRb/hOgHDNleYU5t3meyNMzOQwGug6l+Jb01zs5Z6lTkMRXuScinpz7Q
j6RboZ0gbBE4iJxMl+v1zPYVLrl+Kd3A9wawesmRjmHWxYTKkACzV5qZJngsDMdB9ehE6GKI8Rvv
SJFIoyv9lv9gRMsCgH2IkzIEa7/Vuxv6cG/6L7XxjhYj2Db7gn6YB5LJdpCj9y9jxmBcC7Yc/vTX
/evDEZf/C/bk98bgQaALi7P0FE9v+5SekKrab4VO7ncaLXiWaZUDZtS7sFpUyTMG58yKGIQKVkqb
0J977S/8TxlsyHSw8cC3vg3MUC+1+sK0sdRuouLbfsuJgTPiH+Dwf2fOwEgmhB7QCPZ34u/gBajd
28pIQo74/eWPP6jfb558YB3S3sIyt8tLka0F0qC3RcPdtl/nxAEY9daYVSRZ0W91XXO04Eo5r8iR
QBsOsHWI3d7Ku4klra3SiTZPEH9Toc65vcR9s794XcRKXq7TtkqXtmmIxg1Cu++0PdpbXoM7Bsik
qbFtmbpExx8ISa1pDFSJId/qJ25yviwRpD9n7Di+mqIrsVUwQhxKkISMhNeoZq3Nz26oO4R7AnxU
GjMjqeB64519D5eaTQMxiBU3lo6B7AkK8XhYYZlgLyAxG+ftbcDhT8NmJD+nKuKjjDeNQ20a0R+G
KwjB8kIoEXxTlKeilZR7cG4ETv/eSmIB9nSr5xL7iYIxa4CtoabfS3svMHfp8HGzwnVRdsvw3TTV
XCyb13RiOJOgAhoNX2JGz9g2hCUTD0B1AxnFLYpVGc3whPe4FxzF0zz672MTEnfWEEzzPHmqbS11
qv54pciTdxQXJwHm40MtpAc93+MU073/jcuL+LskROQGPBz7w+PPBccMDv73AGpLglyLxGTIe1fN
F3DziFeJX393pQ1FbSOy7HT4W3T/LKDYNSvsTWX/GIsNsiJQ1qSti+DFz3itA4R/fCrYiCFymfRc
ZtKv0SztKNSYRXmejPHyX5RLSaKmI8mMJO6ThA7Nhr9xEzX1enwx3JZo3dvacwjFeGk6wYpuQZvh
8iJyIOrtNbYR/IbhOHrnr48xu2K14nKAhCwZrfsVMCiBXTdl9B+MGERb+o4k2qElu/rMsYSCIzjw
3rqGFYP6hV6BfTQZ/7nyJC18TA9tna+xbQSjZoDv4VNH3iX4I2+CHyUBgBvk6j4kB1JrCsJ2IoJt
tieEztVRxadIrECSbhzzVMfBWOJMIeyV+pgwLJD8Y9+jRPkf88bvLuplkh1+bZfQLyhEueDC0Ik8
QF7vdwlmuBfb4nr/tA8rsVFy3Q+nDxRcUODVQOW4NW4QyOxNEdh+ta19L2kAzk3iRObf3V36sgDR
BkTRc/Bh4CKXsEmIgfuwQqNwFklxTjE8Bm3n961zS4ziBTg5rlKIdMdtcIwHjO3Om2GpVFD4R9w8
+ZX2Nogg5aOtZbCIplFmoorPkQs9QUbwOb/NJqKg2S3xOyA/XPqIfQUruLiQQZ/wfM16oeTyOT3A
IownSTP51g71NMgeJvHnYRbjvgiy6LUyOf9/SXTd/liposGUhpVbEx47xxHMZcQvgDgZ0bOGFx7g
QBbvwkiFLKV8odpTXnWT6CTTm0Ryl4t1eFlh0jxy0qgDASdAyLzxJ41KP8aY9X0HdMCTuI8cnvT/
0a3vGJVSoS9Z3WrBhZU2MPcSOthZmMeF2yorI0IUD6RrzU+8Xz54IzXMT2kE1k8GYgfT61QaJ9ah
qYpZJzWJ9A8r8DtWGo90lAAvL6XF5DrCwWfZJLR2kfPwiSBK/tUW7viCVZEtF9dIjkjzQEijoJ0c
zjOGqtsRT0YRxtNgx2R3+GoZ2VFVlPUZzmdz0aGl1TWzpCHGQMQAFh5zTaf3oKeQHCdDgZe6uvWn
als2Bkbqla4pOH7ug7zOFMPcyqIB6QLsZsCOpCPhmEkj17dPgWeqOqSdR2TMmsxjIVVbal6lcduX
zawx4Q/SE063ZqXdtjh0nSM7DlHRuqNhY0AnJ6cz1p+d2G57YB4Rbzss/to9mJM8sxPeE8b5Qipo
LYIpMDvPqQG/C/nAGxDZBQ+BEbqjCVltNL2y/uWUwa214WAvKGJtxHd4w/hWd3lXC8NL9rbP4nnB
HOdj7hd0G8BgT1RaNQCl8cT+m2qhbU7jcNjX7g5Uc4NqEJWcrqjrKts6Fz5BzVGPu6DpvVRUMj5x
nkGOnlNjmL1R0C8dfaeNV7VZzNLi/1jOjo8IfgrMEJIWrxrxmOYf2SIqSxrmVLObQSF9BOKOQRun
jAFREOGLGPJ3S5X4vo9BhYK2q66X4m91/8z4CLVEJvDVAIRTUvrJVai1LFJ+QXBdK8tjjIZqjlWp
ERESVV97XgRpiM14YAFopn1k4PcGQK6Nb+qY5KA9bpxGhebv8ktdBjxJNgMJto5/6TCZvldNNoZL
ORW79f53auEEJHWQjSwzUsBfSFGcOOsFCn5m24Gv75++j1RO8Dx+yBfg9kwQHtI+jA0fXFBa641x
kASg0gCD3efgIiwt2si74RzWunQA1Xi+EmDOWI/kzd/9gsC0cV+feAX1vrfzdjB7SGpCqFTIczDU
KJvt2Gz8SZxutf4U1cOiYKkBCuuveNqARZW7wY7QlazTFBLNSNEjKoSEY/W4Pk9WEwn1M4rjVOVi
F7lws0rCuJi7RpndSp8uvV5V3QtUaOZu+bzNLhsrQsWtYQTG7T0iR+7c1NeS0L9/yp2o4xnTuMWh
9yq5+aSM1eKuPL0RM4FyMdBOSAkmdP38KW9K3+RxW8xz/Yg7sD/HDCC95hzK/oygBk/jLWgpcFd3
do3mcq2tBPgGT4R8Bi+lgXu0Z3+5Y09XYNG0sWiMfGubtAE5ra3L1jPHagaC0Xt9uPYltYgPBLgh
TfFU0QXzz7YONL5u2V1d5M8f+gMouqkp7N080e97sm+Prh3xOWkKGEE+EajQ7rsHvuxHMGBmva17
qGaRJBX8o94rD2TxD9iJep8hw4G8ABBFA+t7zfU0pgX2Nv75NHB8OYZ9ssBI4dnX4pTXIrCGkTzA
F8S3mquX6WYnHpxlETfibM/WtbHMbOPynlX0jylKGwZ4ha3rcSj5vaO4BvrlK6x9kEoO86Fx3ZRw
dj5yeo0Fgl7pxqLsxfqqD3YhlH52BaASmLllHOrQSCMsM/thQJR+ChOjfiLB77Q0S5bIWOmCvf31
DZbl1/Vh//LEPRrR2bCtHkX9znHx4MyH3xadyZlTLFDu+57+1eF8AxSfgBryJDfnXWF13kPWYfSm
nwHUkkPxDI7/kvsBxnv0+tD3IdGksNykUYvmr5yhr28lzgKaAaOONaOqt5H+2BygyQURwgQE5quy
HkQF/tKq+YGsUFMJOc1gUL8yW+Oz8a2J/RlvvnQVB3uGzycI3Y8hCN7ebKWlHTF7enyNxOlcTBwb
NonwM19WbXEtzzGXb7Q/DQW9HlXVznQ7MLCoMYKKDLPmOyptWS6OCQWMUikFOpU4kygh6QP2bjvm
KISOgMpzYnCQwGvUgw5sTenkaJoEhRXIm/TVinZzzrz5EbNtfoG+7etikaw8IvQcm8+QncW0GUUx
ybHXNZMMs4ycaEHv1AdlnMsh7IH7Bc8LUXMaWQEh3gHXzPvKKsz6/XftiKiFx5RbDg4jF1Gz6M9Y
rYdtCTl+WjOWIJO/AHjqYlHFDsRk5H5lfpXo3YsUI+/Ykj2ZaArJ6saZxRFfXKdDV7X72/T+5Y2j
4lypSOcOKGjCBiw/aE22ofNqTttNg2j6IVYBKZ6cNsXZx8gLT3JMR69EspcPvKdxGwa6JUsK9ArK
dOBl0O6xlDTsR/octuWvz4JhncTm4H9IB453rWa02tsAHmggHTPpGO+F7zcY0yFPlm374iayYqGe
WUCtes/Th6wyMlZolPUSSeBAXLnSR/Ac1531g1Os75s37wJ+dp1JTPJMpCLO8JkaglvqQYqgtEex
n3peRRmJ3K8ImrvFVrz0VjqxSovxW/qYGlGer4KFYI3iDPd+h9/ZIY21vhsvFKb5iZ1nWsSt0h45
CqzxguHwVYB34q6pYIG8u3QO7hozQ14tkSg9vt/AHoO2OlhbgqYHLDoPdn8W+++0rQVAhWq2lDRK
1GqsSQge4dP9LGIDO+A2lZAfehS80imu/8waf/ur5xKR8dJwT4xjYJgs2ZaFRrw3hXuG0m+hMPQy
CTOpVNiX/N+C5QP6H1WXuhf5glQ3HI1cMi3HKN7qX/qWzKS+z6nIXrQNiImkGGkChK7eOsDXvs4F
GZKdQhYHe0Gl5a5GzT/rXGure6ueUWQwLiolMDAu6bIR5Ae4IHEN1pbLBZ6ynuYQbmUqTDl6dXFs
GryAiBw70RiKeo6dueGgTWlfALaDexz/ldkz0ssBRkqsjXwGcKFjhm7LblXtWiqWksvsogigoDa8
s5q3cnO6iR9FXXMX7OVj+hdVQtsa6nKEfPT3eZ6o9T1rYrffTAuXWtuoD4Pvd5EJgUMEA3Y1YIuu
axG24Mzumv8s96ws+t8hKNvv3uJCwVv/nS2UPCT3xvpVoYYEgrXfqXsou1pNhQ7V0UcGhm3K4KiZ
lOAT/OU+PoEG2EQQBMjW/4ykBXgP8ora32bJAT8AiLlgsw48vKwt9gcjwXmCNvMWXkNRXyC/87qy
WEgU1vd4Ji+2kaX7sBqM+/x+ACXPKgfJyf7OnbF1Sc/OgHqo3fESCCFzb4RbgxXym39Wu99cEOdy
xSiqxNfmHtPaSg9p/crAx26rDUmCrY6jybk4nvm2VDMsq+hkhLQUbH4Ag1rDIOkwmWnwFMCc/RQq
9Zg2L6UpPBkjFy3vJo/PzhJLp2/aVq383Vhgrcqo0FN+vLhfCGlWOd71W4hz1V6TvI22xeNwPySE
fuLSGqIuBEMXUnZAFJmX7A/qZCw3ezJL0N4HvATg92WVn4DZ03RcqyefKyr/GYO4mUIHIFdSdKpD
7ERyXqFuUziJjTa7TCdbKuE+6aB3OZ2IA1K6Nt1ft4v4MUAqzUPkNk1f8uPKVWPD4Qcks4F4lr4d
hRKTRjjVIliZWMzJFHkzQR6sPYcQyfMQjF1G/w1CL8Hr4JymdPLUJ0TGvMwuEDwFKR7av8gk41rH
aUmve8XRj2g1vOM9OP/sD1kwp9I32hTCyNgFKP1xN6JlsI3JFZ9XvJKaDfs6I00pFl27Y6VNuCtC
qKlQRXjUISniVEVXtAduJRtz+kVQ/opf4KuKVw2yBNsfZKPZUfqeajKCy6GscXwhHmOM/RM+tj20
szDQTmu7GUfR6DFm6gN3pOk2NXNvRCTA1uLc612u1M1/fnuECr6cNTjXnwjuz48E9p7U4VsfLr2s
S5Wn85MVvVkGajeLgS1twrWckfiHh3YrzQBEivLJ5Dy8WZG1+be6wE1zcBz4gnSQjL6RhNMxLefE
AidQkwQpCweEwzNBF7jJJl7CIGyIVdYBaN1s2+V7OJDe22Sxp38cQT24ua8Rt+j9+zxjLMcLc6fO
SzCwLozvieNHfQKDBdhkBIeTK8YN458UvRnJpvMAMDfnog7pXLpFMKUd2dKdytZALgGL6/aTex2x
gVg0PnQSZW9meY2YnVdPbw4e98T00ndcxE2TWmnWBqiohBnNvZtDy7taNB3x5hItuQek80sWU7SL
ipdSLTd7f3oagSWqnkFmArCTIlME41vAMtTLAWzCpa90EnTc7tGU3jee3LdyKThRofwJZnbm1n15
+3gNskFFKGxV7LlboZia+4dIOFLS/NC6xt18X7xEBYS67RABTTj525RfQx62GmRdwYsdot3nnZcb
F5jJafpzxnqb311Q4K2PdUqg0Nf5PTAtcH4KZFMSV+tRwGA5G6TkI7ru5Y/0/QZiejvGIMR0+dnU
4rCwamO751mk/hxkS9jDL9Ax61NTK/tjyOdmrStwmaNgAZvr1lGIfBQr8WPHeaKUwChxp6IgaFOY
6FoNKpSTkzT0GhcvqSiUEGCxQxAN9KpdMolhqhdrn5wWfmDdowW24BFR8F7fVVi53BP63ON7jlWP
1qQPhsSFMkvUZCP9mQerl+2GUZuSGD3e2bwbJD9p13PNTd5dZtiu5n3UuSIV/tblfcArhasdLy5+
9uctkj9oHf2FwI7HJUyGUn8Saj+2h9vTZUEipaIM+e9XSBy2DyPFXq4B6kALYxZDY9uctM5t2pp3
VE9iMwo+/o3llqnFZ/ymLWKjQBltySccJR6IC4ynlpFYlsuC1hNrrB8ISzJROFQNJtmCmp5LvJkj
ngQSc8mchMtAh+JbQR2AQzDK3s5/0sRvJRQnyj2zkvu6o1GOtCrmNBduG4hiTh8jU2BCGze/3rxR
AN0b1ucoLiJt/ICv43azQNzBAp03Kuf/n8RGgB8MckubW5GLwCEdq3Q+gFRoSOmve0R8O/IR/tXV
n+z69Cv/NY9KVdOwz/ycgjkKqBOvhJ4ilWzBVQbkuRRjn5neaZyi09J7yF6igmNc+bvKzqGpE8NO
sF6xYt+85FS37eHCqTgWytwTwJrJh8S6WOyf7K61SAhamrf3Mi7oChNYQ9wqb7qEVuE9DCkOZizJ
rsxzQu5zA77vBKdqC3W+/FCsReRqratqPPV0qOfd5OxBHCACT9HcBOJjIJEmELdcVhn49fU8tJmG
q4ID0P10DyhiAUE5OsgM79us2bcvpn1n1qSl2H5NnTrH/Eq/vbjMuOJjBUNvsyPmKmxYC+dLk9Dv
GWvYAT3rjcU5ngN6X3e1dfIvEbu/QX7PAk9UyjAtiYMssHSVBlv4FmvpJENrtGXZb0wV608Z+5il
NeMbe6DrD4HBDqvSpr4DZg/ZzCC6U2Vha6Td9Vjpt+aeyhQDett0I7x3ttCq/O8FajjHM+l2dASx
kISA76BU/aLwZeVDF9GeS7TEHk72j65x6SoQOePMKzssal/KXxzgmT6Gu8SQ1Ul959p3ebZRAy5C
1EJ/V/KGwqkalOTHqIj9mfnnKnxS9ZOVV9GBwJvYZUzztZOg0TE+3zmJqV2gORMtVBXr0lsVFnBV
eG4NLSqMkaxqzSpT4aqgfnN+ZyRAXTKx76A/V8vdwxep8LhzjRqEx1WCTCa+tHpz146gzHLRMy15
Ctlpk02TNlyhVlNKYLrbJpYlMhl5mZ2LLBADRmP0xbkOcbREzyQtgiKvnee8vNHGbqFQFdyUVHeA
S0dEJqJz+nZ8kgb8KHgScKbSRS5k+EGX/exHcyzoixKvCZFDYmt/rsWWZRJ+f/ern9R+xiOrgFsE
j1vSQ8NOWOkQHyHdDcyKbABQDV8pxNdS9P6b/++ZWVYzKfM6IvzMlX8peFcYCxtQyR8kHCuqzftm
y0ByfLXC8KWsFAE2KG/f3xblJhevNDOT6Y0VKaoiXpS7XWjPsta4g3gzyhoSoE1DggQABnWPGEjJ
S4BxObQCs2+QmkWE3+2EqapXcGboMrkfHPD2cofX4dhUVGoNN6BpsDa7NYInqdGtx0zSAH1/N6GI
2zCy5LBXalAQuIJfe9gf2m21qfYtoUDlLpQ3V7OihjwKwTgiBbsuh97KuVJypBrk0iwNhhv5xLhJ
ZUX+L0XCOv3YqcR8LliyYXkE70DoBpmDjXhP6nAe5o8fshSCy5NKMmAoa4XtPrjGW7EIuRhgwox4
zcPxv09ZLSGlhLQdgqKzLw7Xvntc7iosnl2aqOtvHn6wNNGZ2yGu7t1g8P+CGaJDK3WHA+XC65hd
bBuJ+LxW3h8TzrVIUYuFHp6LJqsrbJ/GrVM8Yfhk/jxdfdX5JU1+KvGp90xaqGT3q6YgT+AwCNGy
Wdp/5pqPt1GktmUEl/XNJ4/3kWJXUfQbMXAGZ4xFaj8zVtvcdJWjk0he99gNQiXg03QnNbhNJSvP
Ny9h7GmXnjBglJLX1+l+mZCkMGARjlQnSj8MtrlKlEJGchlKdG5yZxcYYD1bI8Myfp5h65tU3L0K
tnSQP02HWSrt7KagWHVMHt3LD+xiZED+6d6A3ZKaudKG34awMJdwmKsWi51uJjY2Pchdwy+W+Jzl
i6TjuY812JflzakywzqTUFFJBfbDw3iXEEbmEi+Ws/cAXUJhsbL4sqQPYog/O6jMVtZcy9euwpIm
3mAjod/JsIoR
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
