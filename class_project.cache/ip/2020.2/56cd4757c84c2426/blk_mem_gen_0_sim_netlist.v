// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Fri Nov 19 22:07:40 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ blk_mem_gen_0_sim_netlist.v
// Design      : blk_mem_gen_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "blk_mem_gen_0,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [7:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [7:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [7:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [7:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.1884 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "blk_mem_gen_0.mem" *) 
  (* C_INIT_FILE_NAME = "blk_mem_gen_0.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "8" *) 
  (* C_READ_WIDTH_B = "8" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "8" *) 
  (* C_WRITE_WIDTH_B = "8" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[7:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[7:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18416)
`pragma protect data_block
FSJDhgR4/02tXKmtISraPZqVmcjZKw1Dmje14cmyvNsLJz79ZhqnmO0rh8XHwBrp88A/aOP8wjEG
vFWyg4jq7AFHD7lsMip/2lQT8Lgc+0gr2tL89bxi6jVVefSCC8WKFutpF5UQPOmqWjjaBvOHoBh5
TVDhzaqMCO7SYgZAQs48/2UCOSlWIMBuBnwvVj/iTHHDeHFIOLA/d9yLlMawSLEUNDR+HPAnLcSI
OF2WMWQqa9eh5KvCvrIxIIhLHiux0XUvJBff2E86kFprW9yTrxei5omMYRnIsPWOQ9lx6QLq4WOe
BoXv48mINiNjS3sEa1jfdbJq66i3Zyt2LB+VHYNTzJMIhAdQBV1fPlctzJSLndthJ9GtPtl62aUS
gBxDca63RfqXRvKQJE2per9gClptjfYDRF0/MmdGcdRIB5cQQstXATY8eA1lWkBUjzT2JXnCtkIl
scOIhdwL7d+Rgs/ZDw9KgYJzD3En+Je/QPH2Ohuvor5f+Pstw/aJ2KY2zMsXNIRmFmYbBhdSj5pU
R7LXI1aUlQhxViS4laoYWWuIRaFsAlRF2/i494KN8n1CR0LnH95kKEOI3Z2AjpBWHAACSVO4BE3X
OG3xQ34gRe6yo26U2zrSmHMX/Jfktud4JoR2CdCDDEOvt42WYw4taJFJG2n/SY7AQSfdoOQ2SW0k
E4XHnPkOuJv+dLHO/LXLpkqs7WNzm43NmfQ4eTdzT84HNWb8rA5iZWEWRAjNk6P0TYUySkWDHWQT
uNLwtIVYG8FVPOFkzGhYGWBz2/UmPEkvGVUZAC7m6ze2B0Hgmv5fEcOLiI8Ol1oukqp9ozhMNlzY
zigL+zNVZUILW9LdnIINigBSocf/wv7gPOX19OIomCdbZhHB+snlfjTebyKg2eOTG0z8idvk6Vvj
JHVZ8WbtsoJYOSRvntp4PKVTw2F7EreoNt6HiLGJHsaS2FkL+Yus8lWDFDVMOMmmtqhkTx/GnhLN
FSYbIx5wO5iaw+O6c3fvalTe3ccYJl5j/HUu/PJI3YGm/h8PgvJlHTCEDyCHOnqjcG9ggDKRhqGn
xnARUrCalZUmHkHmtJBABtCEXBTO5MfVdAq7Xwhmu1y8FemJIlgUst8WSSIMgQMBhNgLhTCRjnl9
6FFGxUU4DAwHM7mH5Ngvw2naHZcPraaVgab0FZ7lKgbOe4GoKA0f+Rir6CskdpSuNkzeNI9IN273
Ne7FYnFpMzAy4KSx/3wBpv/izNl5uxKlCB4WyVoI8/VgsZZly0DQXspJorP6M3pSuZ2D3zM/BV5Q
EChcoLFKaFapmxFsCuTJebuT8p9aN6DkU/cfeg0OBrBa83CnwvToSogculK10D+Vsz9XErfKn3S6
JKKatvQgaY7zaGrv+OtFwbzlXJZFrpTe84/nfAHvAfglk5CqJtYNCb8qWS5CCt+/poV7GRAKKj6+
43q8nKimOe2pGUBjBuk7TvLX4/h95NT0/Fo+NKHU5TV6KxSwOH0qEWNLWk6D7h0+/S4Ms5pGuuy/
SHoGSnxu8q9S76Ju14WvZgwc7y6p5IClqFLyNubwH+uMn6ZiMAIVNYkvzIJc30yFmhP3qj2PKN/r
YLxTkQY47niI3t789dXCOfdhc2V4vyOYE0/igO9PGbgK6c/N2QjmzfTN95oU2oBe5YSwkapiAXuG
IVTluuMz7ijJOtnSAEBVO8Tu8njf+Ns9GpDQYfRSSSVsyc/fGlvUYeH0qXSpUOCdc3wzJ+ciOGAs
3x6AHLRlmjDvZlvsqXHcT10BhAiJNnDCgicQS3ofdh+YC5xsTpF9J46r16wiGlq9/VovA2Oq14F8
s97JZk1RWneQPfSOQqzmcsgUePijGfyp/ZyE7Y4Tm9cxT91VG9pQc2aq/RsxjkB8DGlMWcXbI5s3
deAOr9rbDfcsY14PJ/5uAeT9D2FZFsYtaGy3+/4NhvfIdt3BNnDkhCd5E2h9xLPySgK++pkhqooi
BnXcCX4bbFVqWIiaZ5jmwiSPw4beVJ2GFkItSAH6TcFhr+wg4CGu++2j5Xov3/ec6EXX1Lh6GBvE
6F3zW4/5LinySQduyuafNLbKz536oyVMfZbGgIebCAn/7dLQEQxnT0tGwmx+Du7Rheoo4wxs5o4N
k9zc4pzHQqlEAr9dLytB7/zdvA6P1I3xCHw88azeYwc4FcW2KysnfdtbyJnj472MuNRMwto020Ei
d6w/Vc01PPc2zR7eyNLICdNgoTNXu/kgPH8T8TuiS/VjjDK/2heJev4qP4Hrjm1KjNtXYPmcwu6C
OS+N7W54o7IdQ6TJk8CJj0F1PT/xJjZuAnFwkCU/cJi7YSMfXoU7M/qohz9eS+HCfG067u6pmbY+
uYUj0NWYyduumd1OjY7TTCwkMHKGiGQyiCnPPh2E2xMTD7LJGynl+EFneH8g1WR6GyRquptRBmG7
wRl79EK3fbMJqijDHYGYoHh7yHlyyUtLdpKUpSiHY7yu5reDJ7lexHZQ/jCekN7w+ObN4PYdm76Q
yzyRaOuZP/2fYdJyn96opqtm+ZBfSYZ+2UW10ATBPH5DKgPsM2vXGTl8ni9Pi2lmilKO7im2or4Z
wikZ0ubRld2FE05MPtVmJUTNRkUrx7XULJqDcvRqC908TBS0+zYO14f00V2aJ3c1h0aa2clovCTA
aZ/dopnpzse5zYLch6pFMhBL7Nxi9sPwfNff9ZoFUVfmQAmHAieTc/0lBYwbWLuJAnvs2VKFEXtz
682xb9N4v22o1OuzyEjmyIJoRC9ii3mPqkqKYpZ66fRBsAu+pS769B/goG2XCWkAGxgt1OE6dSp1
quWRnNn++jnNQm6Py9bGT0he+q6yP4evlovcvrnXiz88QasW6IlAyhYMvT7V5Hgi1UpssmkTWIdy
bgvsMGSnZL4qLDjsJ3G+DcjBgak8ezTLmmmOMg8K6ZI0tKblnjIY6fmQfXJ4CDsBZiXsGAX8KkNs
lfD+Ea6j3uroAzymLx6aVgI1D4bmmOM5LciAlqE9L3ybIqfvjdB7oB5KSaeU4wxCBktkEwMGbLzZ
faTVwIkAuimxztL9szGv2OMGwjnDKTh3ofL/nghc9WQlUfIRTr3lE1L0ONbFOONpS3gs+qJ8v9qg
yIhuEK5j+KtGQ5kq/momYznQTafDDjnVVwuI22wbg0SC/9qElFGcUR6AJnSLd165hmPWy6eWz85/
LOBJ/UpNIplNJHwvqmauwHtSximQs5nRI+FLToN9AQgJT7gLizcXkwWungzg+TyNrvxM+A3C3oS1
uE8X3mCOFkzsKAOjdhnEqxDhIG59MxW3iNqnmqZdZ0aV7wqQqzfwQHTLXAnieXTm9c83eIzu6Kfr
p+aqsiFlgED4IpRBJI/irqer3Wob2Ozg7RztDbiDjx+aWmLShGaJsO7jM/LLTWAnFE4RJ7Zm4ja4
a7jC/hWN/k2SSPiG/QtKLSeiNJPCFyzxKPzWbIcNzYQ6GH6GBYOLQ8ooNOgLpe63B7sMmVuO8yoe
RIh6ppEOZzclMujJkPG8TwTcwb1Xy+jjTBUC5ijRKkIPTY0Jyc1gHqEhIZKta7GwQ9eObas+ACAi
4WxewwEq4ZnCAUksnnTWJsLtU+BWYC5LqKvivq6lwf6qznx/zz7B1XPXMNcUffa/USlDJh1fJmNc
DJJdkal6Z1MJHOLP1szR1PjcQfrAkWFXAxMUl/5PpWK6cCC/F1JeZSo+jQKmQMrw9kbd49VaCnFR
cFPbwgDO54tirNQh2yobbqfa8eCrHVNTx7wf6DWE4aj2GJubHhrxP36q5h7sOLDODbjX1e98UKlS
UpB5GGKlotl38YJ3Nje6MdS/XvYlCKpo3slduXtjR/3QCPf5y0UKQEpjNhk6ETqfBP3cAS1hYat6
1tKnwwwoLDeu6ZZlGcW1JzmjudcsGJZFxh9pBZM55uCjKrB4O6Y2MNk2CGVk073OGcbcdgdqiGTi
rCR883/cimX9sokUaS6FvWEL7x2NMZ6Y4EzZF2Fm/sZI4lrQER0pmO2mBrDXJaiUtmolTDjRKU3F
aJnAYCMysxVRl6oZu08vryTt7kvTs05aSpsxKNj4+Ah4nGlXadYYuZrTdao0nSwKCKweSnbL/6ZQ
lsXkB19Rhj4o5c3Jvshw8jfVk3HtrF2xS+ORgkr4VD31fxP/HRZFzRGXl2xhfaZpW1ARiEmbrqI8
IQX2j9baSF1dn5vZStMM4taZUZ5Twmd5d6auFMwRojZ9UEs516E7DVbItZC4eYi9rFtxj8LrqBu+
Yl13t5evJlRYSqJf03IuNf7dwg6BtLKDyjD4LTfIo5riwb5tYfKLfZ0fkGS/vwTw3+O4tq59W+eg
lOAfoHWQ6grG1gFDdGBL1tx+sixVx5oMGBuxCYGUuJ+NoYA1S24Qn/SNWcYa+RJ5pwAnWxKoegaJ
PNefvNYTlQ3G1PyeAiTLBrCfOb+5qraeij16fI8qJHSsCbPgl87aPpDSx55nLs655qJk9PTljFLy
qROoC3fvdQhukr+Vj2joqTkxy6l2ap8lhH7J8pJk3eyLFT9zCpn35ta2h66Q80nRV5M/P0aQSx9Q
R8mIeHd0aMps3v2oujfF6HJ3a1/c7uR/Cd9swfPwtTvKK6mIraYFatIPLLNg1LNH/o5ZMrYRS/Kg
dkLyqXpmDmTavHmyhPBs0ZOZRskcW5ZxSb2XDqn22Y3AxdLw/6+9h04Kfb7U/n4ceMqQrlVw1a8n
EWI7mGUIPuQ2xCCtRUBN9oyVLwM66aIEYCYSsFV6Rj9xwW7xGCLL+en6mqGBOiQQRpkMQF1K9lR3
YGFksMJRvXe9IQyDkvxX2so+YTPLyRyfOyoxuq2Hg9IAlukGpa9Y6QQSmcZ/ePXNHkdndQI273iH
ju+E1MFByRiPGaSYOTuubZDtxJnsnl1CKrORd7nv+lFqDNzT4uPcXisvlqEj8vUk+YJpcBYdSUZt
O/saVqjpm3WCaFQjqEbOY3zW2TNUQfY/wSNt6nH33ukaYEf2s4mRiLepFun7rYewV5tk602M6mv5
VW8ZVc5yMZbFZ9tUzEiLdLpxU/RFOesXVpGuGnnL1X3GefkxnJYIEOkpb3tfdiT499QOW+NxnKsR
B1wmGhN2l6c+BQUDUJeWVLFgKaJeWA2pRkODvpCS0UgYnXojc+F51G6zFYzkrkGZ1w5FBORItRWg
pU8vIBdP/hrEHJIMutO+JXAEOjvqWcuQPTmVBSCawmDwZC/4YlBeqH+hiL8SIDOPT2rKJk2A1bPt
z+zZCShiwf2WQhWLHhJtmTnNR2f6pXyKxaSl6iETAMLL4BXSWc6rZD4rrVz4lg24Mo44m2M3lXe1
iBtjBpwrG5m/ZX/+SCwiU+0vveToEDkgdyBhtzW9MifTVeAnj9Y0+OA7gCfJjFVsIDPYn5lzTMnb
UCWN9tm8taTQKw4jSF1mhort6YZPsPbHZGu2bWfc1hBFuuff5LW1+G/QWQlym0xLdB9K4yyY/a8C
dgoDKP1olKyA/yXvbnqijGKis39lX+fz9lVmOryya2ZEMH9TSGetCcgCBc0Pz49nur0D583DPHEC
+tQYRR7YDuMWgHJ9/mVMDtrbvzS+Y5RYEuuVeYgr6SKDLXIjI4ha7vnEJAKE5jCBFTzpGG+WdJ+E
VOrUtpqBIFDlhT9HbeMN1IXlwAnurPCRXyWQXyORcb68dlSZuAd1nSJwdU8xwP4GlRen6y0H8fzK
HClStJLRB6X1iUU+UTjzIZApu2Ln2A0nUDGcg6JJIGaYTUqzyrWYLr0q1k2YikW0FuhHC5VFFrtU
tqrbntMy1jMJFQ3gv3M8F+HGA4RedE9xu+eVQpNg+MPDtm4K6DnJ5U2QfoV+uLM60p6mxFd9UKTN
6tINlN8sPOts97Y7n3H5Iwy1Na5qbwmkM0VEkea0NOCvTt3sPXjrBlKB71tyzkywytYpA6JXMTFr
TOuFyJ/uT+yQsdlwOAw9zpSuNsdZXlwDU51xzn1tSIYBPkxCoSxuoST3Hz0I+8gbEAQxuJYngksr
bNWcjQLY7h603lAWUatUff/UQhd4H/ieceRj3pxjS/yWpBB/V+Chz50sz2SOJAz27OAbNydPkAuW
i1HivpG4JJggEh9nferQ+dTgaRcSRkIoaW/MpbBvRgTGeUS+en5RPhqRzkxa5lXffePPpHJPtocD
gOlwMWP1vRpeN9r8aVLqFlvkFaJ/Dl81bwwmM3ZpxVsbL1NMko5VvtGMqNFdmYOvaPyOhFw3mUkq
tNjuMFQzUih3UniLd+KPzHqOiWK6N/erRj2mrTWbt1166WPYNPj+my3+1ix4JR8aYPr+LDrUiL6w
EQTNLZUgFGozxSKyQo5r8TjJFQ1CoNjWXlr9Ee+CUyYkRBE2Wpxz1C+aecZrjAwsI8Lxji19lLVE
n9W9Y1/ecPIae/RZPtm6YiOBEVyki7vjjWs0ppVGER9l4ClmkRrFk7Ilds4ZftpKMNM2iRUA9cuB
bc9wh+jaQ2nk3Tag+Q5aGEGcdjBRevBqHRUs3s5lMpppmfvHfPNVwjbkRCMzdEN1SP5BG0wyxol3
zrxKL3LknycaFRRaWhtQEZN3mR95gElkH3CgOr2CJjvRLfbOOi3S88s5mFdfKBEApuesIeejr2KV
Tj/Ne/pNa9NdiLou1NUZTmWxPcFYIRJqvPPwYl/PXrtP0sSeSVMTJL9fnbgJorlJ/bVHshM/mNqY
yjJujcT8Js28IRSxOhOs7lmhK41PhOlWDL2cTbIuczuLp1ygjJ8Csx5gQ24B0ELOTTd0fh4EzhAd
cWYJHdmXiTzYBclSO0PAkmtDmfzyR7/xgPLUEFGcI60Mtp//5v9QsQCUsaVe6zEm9LGQKn/Z569Y
vY+dzc5JsL/y4Oj7MrS1fcBSAfzL+p884M7T93n7KVQ1J3wNci2CBy/5RYIMIVfZdW+nyCIg7AcL
oFh26UgR2J2w6kGaB9Q7VQlNT5qIEmlriEDPrlFA9/ndRF1y1lcAeZ1X5jt1emLLxAEVO1ljT3PY
p0k8OK0SZgBcKr6nWEfqRNupymlUgAQQu8sqCES8xICoRAKFN4KjcQSAhYLlIcx+CWYOm+x2R5qW
vtvRS/LcsCq0YvhiGDuQEGksQXscYngAzqdcIPX4UaAl13LylKHblOQnx3KSS4dx+sgFRkHG++vA
qtBINveFiuc4ecYmupJ3x+vnvWO1nOVrQ2MKaKaJ3XQKcbw9qJHc7pCs3v9KFkMupgm61yuXw15a
Rsl7IYXc+gHWAfqCqR6VXAs+fqV/eDlqWyAexWKCSx/UH7BK4O/K8DBr9WLdIO1Wq9V8pHlDZ/tH
1YWgUyudaOL075ZmaSp4UJt+U8A/ox9/+4x4gMqoFTe+MdcqW8gYc18wTtAPIyMGAqfGv3ifJ3zW
H/DZqoORdJ3v8WpYSfRKLTOqryHxNip/PZoSSJXHUGWBwoId4qGJgCLkIjbnUAk12WsUtRJnkZ4x
ViKLpg2u33HyouQ45h1EPLCZC1vurUvS/pI+jtsciTvKYaRwts7IgUXBjz3uvpdjCjzDtqe18o95
gSBAbMnBXli0dmEZdzoxcdjzjl9aNak7bawCJxQZZmijb/JCAF76mKI5HybdrAAkL2FAejvPwYJR
mxslsubXa6um+96H7S/BNuVRLiMLZTLS03tzpRIKFOnxJxvWG94pVs4macZ4I7NMZgy8vBJRyXf+
TfvfYNUSumJurcAkCRwLvd9EAnJz0/QlP8qiIwYjE7bJyB0We5Jy+Mx3zesMh1iCPqixy0cNyLES
TdWDZvbfZh7CUWzD0yjP4ci7olqh+1OWBOgMwuCILuIcX4sA+7gWhsI3jCq+4Cw6PZEWnwbydPRa
qYZrSkD/ZLikShXLqyCqwlxjrRCWtQC8sf5U7PHhNS6EJz/8UvrXOFFkCfjR+8mQb+iWL3vGLphc
cu+mf+VeSP3vlHvjwqf8gqFt7TPgDPnFPZyLuEf+oOYR9OmWjzWAAVMF3YtKjTKup6g7lqx+6vvx
3LR8oWf6hxgw4arZWXQ38YJBCsW+if/1hbckf4y93hn578CMSo/C+gYCNuZviz+3LkAIV0WS48un
hJmOVaeh6Le+y0b6XBHHuo/N952FKo/zIl9p9dPAfP3bil4BuWBInw08piUnmoRQty6vu/ZtucRW
7AZOwLAMcNRyKINIl36n/3Osy+aE3z2Snwy1i2RJ+WM4C0eNKS7z9gckD1GI7MM0cWAY3xr9MCvp
DAB+DqZXejK5yXGztf7OiGHMnz3nKesfli1vzILsSK5NErYdJ3WyupImaPA0BnJRU1SCamznIK4e
+LTBdqImb8Nr03mzlpRAZctCXi9dLbEC6Zw9TIoqM9zD4HVwXHzrvDM6Z0Ub9ytz1K2b2aLNTtbW
XDUw9Ngo1OVghbQz3hUcOzQUL0/D1lfEsJ6x3v+/+lJ5/61Ua/N0mSvA0zS1XykiuI1YF8wDwexW
E9c1/Ju9aF6dUSsQqayCjKjh6NnTfAPryHv77H23HRhLV9oqQwRBK2rOODoavq37XHU60DqbI1PQ
J4IZ1f9sPBDeZMkVbs2jAeWCtlS2nW61LJtIMccn9k+VDQRqcC/lG891UXmcvwAVSD9xEmhLGbpD
rVqoLeFUCA4x0QjjHxMkhuSorTUyP8u/8B+HVqeUcM4VBfvEKZTfmMQn53Wyucc3u+nkxSUpnQ4N
fCBBItlycA9Wa0+gqa9Q6Y5Z+S5Eg4lqzY1SzCMixpk2Vd/lhtTtA8OHCvSWBeorGOhku8mcIHWq
lBywVcYavqHv45NT3IIM/qF5MaCxD3dglG1wHuglOlw80Ojum1GyKKODWrjVwbUbK9ZR9UWbLItv
NUCMc/4+j4kTjwhfjSP1LjEidrvSWQZxJf6xyHjMaHOdtW4QnqALPvVQswJ4B8dtZ+eLfEjR8+NE
eZKyu2u8lQUDQ6XGFAuhrBGFc9SUFbZm+j5MEdR5QjdSb/x2/TpeKV1Pa294FOWqx8XoIEF3Y1XX
87wVFcQA6p+UZZ/kCZQZyvT1iN6yZLzYtebOp4UeHG3OwmV/3U7dNGnolxgb4vPAChqd2DDLOIbx
uR7F6fhtuhfZWhUsF4zT7zN8J0IQT/6HCbeCsnSQKh7U+FHdxbf2fwYs+s5hB3qis6+p1Bd64ctq
+YmmvoW2rRtEg1zQQbjEGcZSCdCP/byLl67OPPCe7R7a5Wpc5so8Qvy9vbVw22LmIhH2CJvwTeVu
7aFKAqRK1/wh0fXMbk2Z2RTILVQLzdxI+lreXm5CGgUSVHNDS9pNvh2dUwpbQOXl1+pMBmwsxFZQ
ulsXHb+qP/JBGNwTuLFKwQQx3Oad43P+BmRhKWJWR34tTv75icAmLDOfSxlo5ljmXkar2MjIrscb
BW52MA0C0wqt9BHhylaPFXtpTlNA1/mvIyiPYtPAyKVhTfFK7allnSJBhuGpZU+5+Hf8UQgt+uKw
6sMp5ferU2VoeR/W1MQkDq3WOV/cGM5f+5YMWwi6iL1BjNA12JxzFMpjvyAPlGJWQZkDMOksoS66
t3wZXhxYEqYFXa9L5at7awxBfzXnyGASWcPq+6w/zx3rFZw4YUFC9kv3LEfqxJNL9cayxY11OpLX
xhlZHMM18J+sLa1fuTYodxgZu7bZix7ILIwalpxXgP4Rfoik8wNg+s6+bp/uE+gFFWO2e8dIpu+I
j02Sg0U90qwtlGTp0CaXFIICHr3POLb3dO1WrGaSqcwyG40BrT+e8G8d5TDuovb+HuynznfLSQSL
gJhOt6GqlZe0zUV9aQgcsbKUYC1NQAQ/+N9fmKRTNSeGldMaB3vyRlpcB82LvwDAMwRYfghr9YYY
YFz0ge68qzVjaJOIYrGg2OJyTXFwS7OsZijRPPjCSuJdRMcF7AV2YuK5XPiqHD+GIr8cn154/n/w
Waz1nk+G2lF0Vn9tx1CT/bQWkfm8I/h3H10uYKXCGCLAUxuaWC5JU8Zt1JX8wcZ0rs2dnpCLxkvc
HRNxmlrjThoKQlftBaoygdCVOy9KykSCSqNxalXrO4hm/Gzf/T0UfNESydmpg65ahNgLgYlJ0Uvg
Q3oV7RkXoNNNZcPNZ55lzuksBGUBA2ThSAkwLJL8tRY2GaRKvPvL4QMO8g/vQh1PUKMVZucxv0EV
Li8EhuMLAiFdI6eXUJPeDwW/GM3nzHOXh825tXzqHGCb3H6e7JNhNoKO0/P2h1qxRLrO61JTZNh7
CiZwq2HMcVmcXYDLeCPUeP88LYjfpc/xTbRfQEwKt2JGdOv4tUytqTOu5NGq8mJcJBHvTzJyphjC
Yi0dfNAC2SM7cVUpV3JSKcfX/aqBy/kXFQdQF7efq3Uyur4S/is8o/Ydik+8Mx9hfJuxehYcCZSx
uXwCIV6Xzd18dY6GAxO08IA5QwdWVV5upropV2Ji8/8Wc7No4oM8pvMWx4uMiy2vw+fgGt8wdAsu
/Trj4whWMmH/u6C+jaz2gPK0qUxAvLjgm9Rtw4RKM2MgaBexQ7CHVTIi4SBMWyDt6InQToUWt6Qa
UOKcr0dUS34eL1M+UuScqJoa7tpyJ/IYT3bE1GWSgQsDUrcZlKXRdiy2WlQN0fvTJDJIh6Hh9QJj
8vie6ejwL3vIxZa9a9vZfkZ29JXyMZEWMkhH8AJ7PVlRdbIhNxJ4ohn6ux1s1VlKOTdbYzwbiDa2
raDp1uss35YvgEA4F8YeQPx3oltWG8GBzcTVpBegOHypUAgPUhfm6pGOblRhlteApNXbvkitUZG4
R2kTg6AocMa/mBZEkaOrITSTNZLM7Qvcdbv6GgzMbs3kNLmDNGphTkRQViFXi6aoh4/3h/QiOwWx
Rx+1Y1hxn7m24hgLwqWQXIYXpX7eDSn4gAxacoG8SLN3RsMk3bcuJV1fspJYWZqikstpbtrhXyUP
xwDKobzwZJyUw9bD/6CGYt0UovCy07dpXW5NbqA6bOUQZk3KEaKjNEyE/LrhpFzaJ+SONvYMFdlb
+gIYeMYktk9g9f/wJe2kXrYsx8/skuuKD6OJoK+1au9/tvgrfG2DQlu4feLMTUt7z9E2KDnjchcQ
SjT6DDsRghqgPszInDb93aIi/VBiIT9NlGGtIxALJKPAZghXtkmLY05dKpBuwucS6be78Y9rUdmr
rooKEyCul4tOQzrWVXI1ExyLKS89OmzRat8IQNDBd8112GL4zbhINJYoCmvfS6WXTuKgARjfEgVA
od93fO/RDqtx17n5k0U+AnOiIB9B6FSDG1OkUv9UndrucwkdVdhL3jHbnQ9rCJyL6bF/BBlZb7HY
3j5Kfs9AE0kwOx/i+QeBeDLZhhdSSAlEFF2PGgYU4YuHKxqnZLq196EFexYzva+Vn1j/0Xe2+rv2
DQnYlNH3LafqRsPRLMglLlNDmUKm/qarOMZXgobWsvOnBw5uaaD19uZc/a2o1+nC29deODA6uJIC
mffSOtAnSyX3uY69Dq2feXBqPb/lDiIrL98tbMbi5Nl0BfCuNRmkAFe5rCuNvDQsz0ApojXUdenW
8fxhgeW7B3VsOiBQLJaVnAlrptGhUEn7uHNwENnltbgamp2SeP7YBmOGGEbe7LUEvJlKJrsRLwPz
+CRjACUjG2y3v+UyEUBVklOVKwZioW2eGH6eeioKnspsUQW2Y8EPhS4z2Ih4iqPObT/lzhU/dhjs
4no87pL2qLrvEpfXPB0TsxMdgY82VMFaTPn73y150XeXb/K+PnovZOWWKH9IS8qUnoJj0VoZqe3z
ZIR8iyOKWO+wAJwqvafbltlxrC2CyYaLU8hPfOq0YJnBTc9tKoGTYI7wsSqPPR09m2h5pUquE2Wo
qMduWRVMjpwZNGyCZCYu1Tzz5tk7g5AkkcjC8Pm2G9CeVbWZ8TFyeoY3UHZpMNlQ9TiXKpilkeTT
X4sSxJQkJEMf6lEHeVkeifJE9vHTOKsyzzVugLLI687X1/pE1yvB0DHGN8bYn2XZYobVjWrdrHN6
wpUU5/PQNzn7ijwCQVGnfpfDxDaQ4Dp9zzrh2mo8BOiO9UWsgj4KAV70D24ZT3nu+5BbXbDYgDpS
YEt/S4191Hc9knGqUC9YH32go8QnRKNzSfQmkWxi4rFAv1UPJFBpceXUiDcVP6R88hTEs6SccV4g
xrKVUL9RaZ7MBglY4aQ9ssjU8FGu3AvwUyWF+68NDlVxsbQaCV7zvBBVpqFsHE2OR5VT5/XfA6K7
oi2rDekMiOnMioRno/ciVWSNgoD5tUcdRbtjiDqRra8f91JQVDhFwdL6O2PnAfNTjNMTr44ngHLW
W3MSttgp8sIOZZKtLgtLKcrdCP2Jx8fHOkMZf7XLz8YiGrUoR8mSmgWczBoFBbQ5ZNVsnDc1KbsF
YwCJy6ebKGsz4qJ5GL/JJPLcJNZ6ey5oEMgfsQFsntjbwa6ZBrIp5Xg3eCJ6pNPPOSH/nkrPKNCm
BfT4aGJjZDuj0TrM7a7v/x62IDOOiP3N5JCgHYgF4PV6LcGuxTttkGHNczzJpZX1VjGu5hK0z/z8
GS3KkUhUNdGnvlK6D2r12P8eOY6Qds/31HNQjQfmrFCeevg8DFr8tMcqBNWBIfjBjbZWU+JIwDHl
kgVDpF78nAX+J6yJ/2gDmkGsM1Aa3pDhBZWUv7hLxS1Iyh+SvbbsRc9/1te6iuopEvjknJAufZkM
2t+mAcqm5HzhNQiKvbhI57+n0qMQFsqKldfT0Cct3hjahEJysboNEw3bMFrVg5KclSHT4XSGWPMK
cdi6hUdNa5R/h08qF/pVxNPxZaXpHCF94AT/+fMrf/xd1vQQuc15yr8KBTBAkKw9VuGYd8yExftT
5i6R7ButlbNNjmn5chd5+FzUkUjkIC91OTSnotxs0S+dIMF/gHXlaBzSaljCBS7w2yHDB/g+mofY
rhQZQhhwe55IeP1GR69ILpD5EZ+TFm9uhy++ezpormy0lyHr/LBCor27JdV0EitzRc2eNggvS8oM
+f+DmN4B+nIGiOvE4OK6jwbKaZNgkzF36tNDu1OFi2N7XV1h+PzoFN9IIhopN8yY0TK/UZtw543M
tJMz8YqjhTc/CoIgvpjreEwDbTOKAI94J0hAxEW0iSD0Wri9z8Z69yHX6ek7+5TLtfw/yb5a3Hg6
Zl7/Fx3j2hhllfbJPZfjPNdZTg7/TZgIHPwPVhNL7e5tuCBwPmyGbtcsWlJ4bnO/M1PDxdNEdyXi
gafh93VJJpAv7oYtCsNO4ys+5Pxl3Xn0eEAgP7iUfbZg/OAhxqemmd43wvcjF0y4KBJcMaJ6iJkP
mM1KrTPw81NYg/I9W0HdMa/703dL7+z+HYzJzRfnB7VDSkZXAdyUk+PUG7GsmqzQRrmxoqbMZOgT
alZ7orYh8w+Jnp2/andq41ipwjEy1gUUoGWvEF3ZAxL4UZ7R8nTxDiiW6RmgcXXU8FmcMTg9MxdQ
3xtW/99/QOus8iWcttZTGKhhE25ebvnYoJhO0XH5FKeA+elQVDmE1us8VQkLZHYpNKLlwJWZd1An
bk7ZNaTLBqkD0mhyfrvDGnWwebwzztLF81M3vk6yGWCr9f4KsdvZMsHNoj7jAOMudywKFTDIrBtk
ct3mKWx4tsDYDxK1UVHovWOp1Ck7s1bf79V7WPlCrYJOlTa3LckEeqECVAbUXjjJCADKoqinAi7r
Zp3cCiTucDtwiBWepoROSYsz12/JTNdVH33CXAtSdursTXy5cyCTsx0QmE7tycjnjDDtJpInJFk4
O+P3tDONT5FStpH/IPKKcB88RQpt+rI7HSCeR6+Q8L1aWfH/1Qag0Gpykba1f01ckUc0JMEdv4Em
Wch2prJgifbw9yaSV3Q3en90Rbp3jtfyv8ZLtj/YE9WBob4nts5LymQRvUfzBWuNars7qDVMwDJj
/m/HEjN1i5Iei9ikuEn8imY11UelBUWq4bet+zq/pAf32oYGomFzwMmzX6Y+gRY3qil7Fa0rrPzu
C34/CgtfBx9s2T/eLoNG3HFv2JXUJXO3f8wsM/xjBPVwMOZItEJAUgmbrwpiJVhj9swiMRDzk/rD
Kb+hX/57LgSuatAsIpkfHSmWZukhMnLqbcpEUfVUFb7SvvLox2QnqbJW2Yw1Mc8CS+L6Ke7lAuQL
TAQB/nZ0iso9mmsS7XUvYt06vFSj8hQnicdBwlkxLtCE1dPnQoPh3JUvarrpPqpwzYLGDIRE1aD4
89Rwf2Gnf2Ohf3qo7QCrjn1hVLmwnfcrfzLj71k8F/wGEHrQEHJTGJa2JPe6yM8m0Kjdi5OWH2KY
IYDySYSb0dUFXyQin7+ir06R1BUsHU6vBwjMGCI7pHNol9eXa8SBxt+RdbRvOvniZBZvz70iBtz9
iKaOPESNGbJSpOWmv97ymWdQF2lGcpoMdkOvZNdFNXsvdgfhRy/E2GH7kwVhfZa8Zw9hn3btcCmH
uzlqympv8C49+MOKYlZf909anHuMg47/W4DmfbCymjKcIeAlmmK0B+5YYcVY+9TeZFFsa0MfGR+s
8lLVMwBGIYIXxFMhRQcphU2xeJuAdqTqGJG2k07Y4KLDhc7Jzj0fB2TYTW/ot8NOH8mfcdwdeK1h
0pPf5DvnfkoBC/Sw/j2A1X0pIqUIhkNTHbQQQk0Bp9DlARShKAPD5vZFoy7JCAg5eaGWAelw7jWE
D/LffCkswZeY4xstD6L15PxvWBA9USsOAavh0UQJr7O8n7x40vnIM6wIfEU/Ldo/N/L6y9/u3ouS
J26zSV5VCcEKbwL/FxmQXhnPj55l8YrwzoyEPtulX2/ZWiOGHEn76gMGHKjoXfyZ1CWzgVkaP0by
kWr9s1/4PbLfICPSMJiET3mwT8kKnt8iP9pVxyGftYXaUxgTwWJV++AuZWdA1FQ6C/xRRhmRChYY
nipU+47W9Ut8jIkszrcqbJ7VEua5YpBkHv/pl3AH+y3tG0I7DpCM/k71wPt4kc33BDUdae19723d
h6aMRUWEDdCjWDSJHl72eV2LZnVJIFsDT40Q7KMYQgZ0kq/FYE0wQfmq/ef7kiwvdY5ibTOBR3pG
Xbsrw7aRIEsH9EP15cYcwlIp7rCvAhGRcT6pvBdm8C4lfYmNmI0Fj2GYFm+pAXiUbBSQjryax1H8
ZzB1zd0+55qZ1bzExvYtCTe0rTcrbizAKKoE2ho42c4Bl29rgOKPGPjxR7AnuKomJLnmI5PO+C+U
5IpYPUSn6ey3LIKOv/FoUOXDufa8KRQDjWlVOowo7gV+asJb1V9iSmy7M+Pey1ixqe6PFIdIQpKp
XEwtgMIVu39/1IgPFdQILq3v+y9PDT4IV++kJQjrTfSdYk+i0uaRd7RjRJeMSJGgsARzc2OMi6yF
4IW6pnpT8rEfc2LucINg38xu+WuAzt+YDzNoksmnto46YjLKvFpBKmD7hz0aLEIveidefUzdi9UW
072u/GUdeTOx8F6Rx7kH18BzlZgYRKmjMLKGl8RQCRTFhwpEdHPJ+NbuAJpVB/sfSR2EHbBnOc2w
MwS1+vJW+o9MTtsHV6yeQ2+0cNoFBNgLzoMCQ9t5Ki7mDRenDAP8zm0aXbFRuJqu+ZQopwW0vAyV
ULIUZ/vbWJ6mqsFk2K5uhXicTQPDOufmmEhl0V+GY43m5ame7HJ3wrCAHpS4vGH7ZSPi2s6kJ0bg
xbk+cK0WM9d9+jP/dM+WdcjvAm9H+Osp1X/Ef58p4z/dM432SDoSrpt8D7XwYeqpszStrAyzw8vf
JMFcKqbAoikYK/+JV2MVM1ay3w8cZY9TPdSCj0fazcLBRrclHBZxoGbNDBVmJc5DJyXvVCoGqyGu
yzbUAvknSl5umO7lP6P10TaWBypF3cRP+ZRyTSxyh2N4g5u6OX9aGHj3ogTyw17AAk/ZL9976GfX
ohm+XxViS7fmnXrBUeJxEEy1PgacEX5bGZrof1KVo18bCws1Gfd0j3osvK8rLO9JS5BFE78WrB3x
GDy86wpDv+7ddlcn4cmpChcLmwF8Gnubmmby75x+Rl1yQNlPXJQfSvzd51NN+2v0+GZ3D0c+OZhH
wpZYd1Hehx6mVrQb4QaJobpmO/fxr2vXNAhjodF8vidNjB1ID6vg9Wamym5XKYSmN8o5S3tqjqKf
1WfRBATZ4zFnXxl1rMrgdQExJOmSMsApZszFufReyIs/jel0BJtiJUFo9TJJbq8bqsVne/PcwRGo
Z02/famMaRLm78CekygQmOdYgh53TfkrLaB91vm+OzeAXE0yMrimGnf0WHVo2LZz/qTtR/7f+SLE
D/w/wYRcKYiHbxflm3xT2ahNFH5feuv7M18YG3/tl5QzwxbqBb9jna98RexohCO8eLEDnAmuXvbJ
QhHN5Uz1FzCyzdBMGUUKg3/4Vz9fpaq79nqOxZF6v5ywPSvKACsH/7eVsezr1hOWPeWiRkFsMKvk
7Un4dJSKScmc+JUdAn+2jQGdBgCB0E9/pMOXBJVGcEpdszpVQUtqD2h2LpWGn1/4EkO75TEkPxOU
OCDjT4jfn9aHCgR9EzsKA44GMT9aZ4leSXvXoUPGzONWxObW8ard/FHBbE1pOKwf1c1/Yc6a3Nsm
tPusfwMut1w3s26IGMtarHWKBRm2zr9nuKgXRb6WjzKpUeFpQoiiYY10HGvQa45e/Dzb/JTSbxxC
mNYhFH/P1laDFxMuGrTSW/yxSJ4jkyYi60PIvYbG+cPEgtfDLGywXWC/+qyP1Bd9O5NehjgKBJXc
ic37nJpPk15AQv3Ar7tlFYBwHBfLWC5Qn1FLLh/ZXp3RH7nXWHYxhW10oOJXfwK0q7u7+tK0xD1h
2xT00Of0KNWmfaUHsd3iVNsJgkwnXWv8qXYATsZc3rYfK8snNUbfH/oPUVFzOdfM6r5Sy1Pg0huP
yhNj3szoNWM8reCo1Tn+vC0K76HtR+hRcfSR20N1GLJPdlAWZM8mt10+yeOKdT3rnLMRSX5hYAGm
O3CiAt++QhfPOcpQhlznlPnhil2fPOwY2mHGMgUp2jswKKdACl4i/UON7VcZBc7aRT60gKnhrb8K
BV9OGLD56RsBvC3P/0KlvFNyzHl1wxROWKlxxXaqvZhKyV9C4QkbPu+mrYREgdPQdIPirMYRno9U
j9gckP+8m8LirB8zm+R/RqQIuAEzo7+QaShnFdtxbVFpuBpGvJGeIvnDNExLs9y7CVK9J7znHJlq
Kk/bzDRPIyGtrfMwZR9DDBRS38IsEMqhZZkZMhF0T3liHfw6DFxzxYAl2KK/rGqJYKGWerPDV93d
INCnyhm9BBJDfoZh7owd6g9iEE0VzheR9iXoHOZQldH+hAiC/dBILjnRn8pEImBWjlXjKF6awnGd
f146VUsQrxAme0Jqryf8CdA4SxzSuymb9CKZgjfNdqpP7ZWdT1oMts0e0EU4Sc+W+UXOO5Cv97WJ
vvQK2G9lsXSI+IAcQbesO/w9Yny4CPfwYbfJkmI5hmB3akjxIKF9p5ujiIsruIDRexxqYztMWZjM
eSe7iZ53+KcO9YgqmNOJZlpbIEda8V3UrmSEunz+91Zf1rM8ygOHGOXskh0JqfXxzfZJX3KJQtnb
Rv/BXrpdzVlhmqquAAWJOppSv3H+7zZ8s8RRrYXPXVR0e7DtonaQZwGRNhd6L17hlLD6qRko+6ec
Bewr2+e33lqyUMQ6iHSNHt/vSN7lmuf1p8jJHEJkLYSCDAkdchMhb/gCh6gUuhIviQhjGkB5qJtH
nQt2KKy2Bh2KKWEWhBzG+Bt2fMHKTa8vgj7VPniG/9ze1ZxmZ1Kh7wwpZbicy6vyJtzxP6b93gV2
ZCr2Xm3Gn7jJo2G7LWm0TB9fRjFP7ci4Os5NRJD4iTiOS6Rnzq3Hk1Qw7BlpphxtHvlWGRhHY35K
fk/L6bh1vSz6503aH/hDncYyXW4VjD+215SlRCqXpuq4jyLM/bzPl5Zqs9JORSTLOB1PGB4Ce1kj
m9HbHJurhTDlE3XpprGp8JG/utGXphBEy7vvIbp4j+EurfTGYaRXu2NkYSE0l0cksfTb7WeIUwsO
MvL3YpJUPBfGNsNHzt0Vmk+BDkYhzkQtmQKfOwayEHynpg8bQyAtHrsWqrLsRJBA91Pf0UYJ/YUi
QAWnEyRpUnY9zFPKUqDG2ts30yFt1CzufJT3XTRExil06Ne9B3HQws5lYW9L0PRPYf/oAuQbUfyU
qhUb11ISPYu8kdYoGxflQcGbkhh3HGFIMNCmSD4IfHP2qPzTP9QvZZHhk3SNkpsUISgXIjygnYsQ
Pd0xm0DGpeTp2lxaMyPYFuUdumMcF7JXGAnMnjT8234TWH4uS6Ums8FZ4/BNNtoZt2bOsdTRX8DH
64yMAoAKM24PpfWrJp1UX2F0qWd6gAw7IsHWty9cy0Dg5aMLhywSH4Py2H9CRvAU0MPQrJ7SDMj6
D1rxlQzW5ic5XCjHHB219Lqdf3uLd3ohy0lbJQxDAyzyqSeZtjBGl56uLkubtyC+qdeaeVwD2g1t
qWGCzkQiJIVJszrTbhUHe6l2PZNL9Js4iKFz1ewSW1IC2QVK0YsKJC9hFdSdztLlqXMHD2fuPIwb
9KEFrSPotGpSijtxfJ+SFBPLBPRT7p+6AMEd2QOhL2Gwa0bBnEYbMb9DYQAVl4M15KSanrdVHfVu
iaq7vqMNUX7KjlVaXvdOil5lvqXegRzg7twWhjMLMjVO6bpP+KJkqFUxKJG++QVfM8UpOPdLA7VV
2PHg2m/ZxwgypDgOyuBqPKOhWqTDW9MMXeWbUc64wnDV95O3pAmQnKxds9xwWwR7mmZO6xH9cb3C
araLGgvTpzyeJYCFih3xNSQUNIICgmn/1cbrEm97bYpoYZOMNn7dTpOKvyXFHgRX5yDQXDBfrcyA
JQqeZhESZVyPRLocxBmR5HJTR8F7b+jex25c7bNynLBoh32Y10aUo1f2ygagR7e/2lHDshxkcbOH
ZlcODkujZM6+c8kqWd+2kPxbcn5mlmJ51INudgl7fCGwWO+lzKA0Ir1RAoakf2/GXW3DXbABMYiy
vKUBkKIE+26EkLqhhJhkGHHfDDWwORV6OHZ0jrZQG0Dxd9G6Lw9Tjmmgn5ntjXpuZs1AsEvpvTPO
heDYCIdmZQKOn0LrDemu9Vqds4aJt+WHtqqF9tjEktdgF9j7tZlz98aWcLMvewyDYjr/S75s8St9
UwlNuzyPUU1PMyOWEcngkQ8N9U6NYFbOvCB4MwlB2u+F2zrTc0wuPtW5GOy08F5tLRyxuXOEwlu/
Rbbfh8d74CqgiLp+2rzLgx6xRGLk3Gz++3ICtgLw9Mj9mRtyOcZZo8fHT8ce/Mix0D/3VcoRBHl4
JuW7sFtl0LkcNY1zcGEdlUqIla0uuCuPSd/W16Aqwgis+KME5qBBPfWrmrlW27Dk3ke5shj0H7Lb
p3Bp+uHSN/oIAk0tWqiITErGhg+m3ksXfiCeTZ9RuuBLUOJ1gGzt2sNrDZicXg3suqXwCZWFX/CU
rvJ9Lc6d1G3gunIdT3BKDaD0Va4bclsFPA0U3SUkvJbymIm/if9fmHeBBRKKD69TzroirXRhJKvN
EBNUbAXBI6+05zduVvW5TFTuXIm5fqfM2uxuzyXahL49NBQOMsqsd2FWYvSWYqSIUADuWU5VARAd
FA+4tkA51JfQPJvmlIKvcgXk6aJ9TJGYuJzFAABca98Pi+qGef/gQ66RNa35QS35Yuawj8piv6OK
ZgGaKnBkd45djMKhrQAfl8up4lDPNWznADqgdMY/EK74k/pTTzextQ5LtN49ehbmqwS8xHQGgVlH
5SFpPb9DOiDR7PsEOS8nNqt4Gk+OYDg66/7ac9P/6RBjaYvfKD/N2p4d7iM0/OxdngaYMpLlIX4+
WadUMDPgCfcCWlm9PsfJQ6ynqEAfBl2xuYimOgDbGmNGS3ws2BfBFC1KA7T6HVPNJYKmFyZqcmo6
PXRq+7yaBAyGeuQRiC4cyE/ZUmu57/8nsLjoeCUgOCXKl32Jcmxq7QWJuijP/LMuaRPS/T+S0dMm
SobOj5+KCETeUJlNxnO2OfWO6O7pYJSvlvHIl2sV6oRTvYU3oWBP9im5ZkOk/kNopLaXI+qLD2Fn
TcIGwdKVSEXR+NGbZOGA8nptFPl9TtN6E8hK3QCNO7D/Iqf9cfZAT+ctLfYrDCMupKGD3B4h3iJA
niGNhr8q+1D3cJ5Sx9/e4MOYt7iH+Pg8XFk+wMijS9hPz3KRZUai414ICkz4igLcO29iUfdSlxRA
K2xYchE6ebX6ZYZVFFOhFW1xqnJG3PPCBwPyAn38zalEZwQchtdrGvhzMXTHv1Nq83y5Z75FNkzS
WGn1XeVciziPzuyGq+4GG4lMJQErLGWBEzTVgwjsCuA9Txi4mtWkxul6ymvUcbWmE/nKkOv8em3s
UhijHXputcL/K3BlCTzN6SlqVp951MAcU/+/DYhOpydUrmc3Pu0UnosT2xYvXo+d84dr6NNopxm4
PIVYb1g0wYmfX9ad2m3myvTMvcfAIi9p26rVS7AvPua9pHV2AY/HmUaUdzlV+j9DhomdlySwuvvA
/S7sZIn/8kKpI5ar1XuZPONDe6GatmB14RFT6V34uOxstgUpsngWZuDMnB9EAEBtao9qiK7922+X
YQZStKyjgRF1wR0yx5hp0Tvv9S6r1+LCyfd4OJsGusEE9CVd7aQd/bV4yUXSAargSGt69DMOtTFq
jWr2nwbLOI5qzlx9jOoUiPAvBileZ0T9zuLAwwTyWU+I5E6Bn2S8mTGjBoyFpPCBmHjFIjXnxG6D
T0l8Y8lcf5WKUW7JANmL/WM9olXbTVzgbA84ryfNUA/mzhIczwpzFpxeMcHOVMY7k9AlTae+KqDi
6911fbbQo/79DX2fcDG8VJdLJuEUHYPmHifHBuUWztrw+zNP4GlAiM44YSQY5uMNOpC0ZQDJNwPD
38ldoNACXXkMv9RX7Ch2Ztzcf0B8Fdwk8IhyxI/2UWvC2/2CphvLSze4uNysVPcuEvtkL74i/nnz
lORlqTs4+BujJhFf8OZSafqFtWaKGHaWXMa1Nmb9hsNWHVjOcDh36dqJMEU/kLUFM1CKGkCAjqYK
KiitKZ0ISq2edoiWbLCiGcnfmdFVRr5zg/RK+d0hiW/5XifypyDjP7JlklTOfMOupYsRi1nidL0f
68FlUW8Ro3iVgiS0fsvBb///c+Haefu/+ZhUfS619YAQLU6Kem/AF0/JAEeCobQUQOr7aSSF3OJi
fTPrgbJgN/gYjxoF0VdSSXlxu4YGih8bUu0pY7kVLWS931n2Nq8mRwILKYj/FYRLfbpnR0FDfhYu
kuXAY2nIM7cWviKdId44qCL9nQWRwExLI60f8abt5akg4OykPyKbUq8PzTvsZMBu5QkIPddnXDrb
kjGLs4cofYAWYjfyboCZj5vtE8i2x2xUportNw/fqXKSFfHftoCroMr1z6yTW3AE/Xm2UTH5xtaV
Yyx0/c4Yj+7W2YiIVKkiOqryfZrjOKARwjbH5B3AG8H0O1CPvFmuEX62bQlDUQyV69Z+ohQK0/jW
fJv4fiJ7j69e+9FwVsYshR3//vsAkvkqRv27AWIPh9Ar8wDEho73hEFPak0c8zvDkHxAJknYATXk
u4PbGZXXsKvDhl0H/Xn+8piqcSO5A9inyfs8Yzxal+tApNnloyfVG08F5Yd2oKKtN4LXS+UUXz6I
g/g/i2zCJI7sNGG6rH7xzE4gmZPFb3PbdeKuuqJ1Umv4xb9RnBqKTT7PhK/qBWtx8cmfJjT0mx6P
52FtWP+8xqKV6YzHlvUAYXD0W6gZGvZ/vcoFBsIn3M0RucwsTWA/2Xi+0Lvuyh40vssWcUnjnCoT
hqyharHZ6VVK066+TaGVnlFU92hnASYtqsyputmvlMaPtBySzbyEdfpT1J5Y6zC/P7i67lYaLqHR
1wdF6VPUGhPKOrEgT2f5aSplWgP+MhzwbtD/0zyOMYA+yW3EUZXuINSqJZtjXlf1f4rzJ7wVTW8K
x2rotCfm+DB7K84ikFvZooNSIji76HHkJxX4C8HiReoQ2AEmoiU2NRN56VxV6dAB+8ArOovRlQxn
1HcFkvVXcOoWci4SBB5JcWu9jy5G615DR6ZTsprWoKgnv8+j0qSgBVINmQ7GN2EiXJ9l9TiMih8Y
jLRubhlSbh2bkBW3zBg3TRg2jKa61TjIhWqYkwYMxngy6Ume+7cKfPhOqnpBwKxo9jb586PP0yKp
6noUXqqRA8lml2vxZUVX3c/KTAJeirt+KqZ+CUtgQCheo4+BP74g3y1475520RPIvuiSZ7RTGtNx
aS6nU+ZyoqMYhrlfegQ8MI261a5Q4nsj1GlIJt8jxmq0iIqwInkq6ECbSbO/rOE7bX9li3DkXbUg
oFbd96YVMMQmd98LlHXnIOIstwwWv5y/WvWuVnMy8CH4TEItjgaF2/HqJBBroFEZrIuy7aXHEQ9Z
eJQBWYGnKucjk2Ygl8hP62uCqk6XyuiUzcXcudWUEsUsWdFcf8jA5JQflgxL27kJ6iKkIWMdPlWX
Xp/gI3aSnU9sB5NGCqBEs++X/LVDyCZ2Q9WZYmQ1aWOfly9EiYgcEmO1/JCwrj9eQqYBbekVIiv4
ASakfe7pmzHhXzZlC3dNUpAjy/nkhRCZNfO+BVZDEGlE2A4rGwxZXXR0qIMGCuXroKFVbSOuHS2c
AmIciDJ0sXNJ81+mgDOb9yOjE8IHnxPwrRdisjAL5YhhhImlM6T84jY15LEfB8x3ou2ibHJg4oLJ
7xwb1PYAYQ6WSe3cK5zwpTj3OwP4rhO427wb087xRNlQXvzZ25s0f5+QnrGoif498eJtUtDh03bd
S1+h0HoihRwbAj2D22bc9Nl+kTsuMEyIc53dFsNiefUVSf+aV61tohVYIDrBp8EFgYxGA2gcqoKI
ILAhMcOmZDENIkZ2CGPYX6jzgpSPG7PqapjQlidEoCpzcvuzR3bUh5CZVZHKC21TNqaw7WToYEQ3
BPJWKoHphg8NpsxaxbJ9kHR1n/5Z2E99yTDddXA+JRmEB/M7E/Md/IgHXO/eC3VilW+Lfl/vAGoU
KDFy2Wy2WDAnEwOOfV1fJ+WxbOjiE/8T24VfrBxmyzfzfFX9ikopYBuF0Icul1/MfXgO1xbMypMc
IiGHquPg8be0MlTf+1YTYa3oQkZmIeHQ934WlA96pRMY/jMleR+nqeCsMOWQ4X1yRnBPBtNe+zYb
BtA6bBH4xanf9820e83rHdDczHAX5koew66sKnb7LQlOqEkxOGH4wePA6UCiCRaafk5Eq/+q4ERA
RFHSp5D4Z3HCbUGfsH2Jf6kHLRrKQSHfOA2rsW3W0eDAwHvcAImgOAtLW3gnGE58Sh6fve6vl6Dw
LAbeikEh3/DppB4kQy1m5soAsj6kX4nI6PRTCFk7oRiAAhrN3FR+yaqdreql4xG/wyWCQa9qhmwy
WUrNJizwmN4hFPyiAQohMeO+3pqCmPKnY5tSr4vkcUdb43sQ9qwUPhRAAwunsD85VaaTNM2SXeoK
aVXh9ABHL4+LpeXDKTF4jxquvqDFWzoCLTax6UXzMCxADc3rpAL3LY3tfMo0vUqzw4L0DffuNzmS
9mQoZDxxodt3p23zYlQjaUiI5gGBvVAC4F025uXvDJXb6kkSnV3bT9704UXGZIBlGahQDi4+ZV6Q
TUCFYenUTPO6YbO8oJwg+awFCaeCtG5hSycDgdKft5ehPIcfODIfj1qHVIxSoQoaaMn4Khzqo4qq
+SRkzMoOkzKkszwqk5jQy3jLCAcAIlOrW4VJt4XQlTWTkZjxCEdfTQy74tNFpzc7I+r4RzDDHLUi
a/t3I3G9Qw5XKd9qVTjp3oweR50Z6yKf1NQKU95YYaxfugITS7Gfy8rklJf9im5Onm8U8HfEvCn9
iZUvT/LHnVdRu0jfm6jBvsgid2R3VKadjBry8yRWr5nXzVOZrAwGdw2N4razA12e51YrgUfq0R/D
gzecE4qPWaIbJY/SjkracQKLEf2U+PfM6hjHdVMyMKbMNoskG8avhv30QE3EDbTSmcdXlm++MkDP
GAcw1pOp1YKyVucMhubs2k39m3+kYYo7uQioRVHdlfN7Ufa5JGhPCrgoW60rEwlA/Jwj3u11VRyU
tT8pYE03iokqrHs9+Jjl1bi736QCJg6RKHQUMoB/VxDjyqwxTqeyF8pvNDqqlIGPHFFd8s1J5mR4
DrqMZOI3niuBUuE/b81/ie5jcQzdJzUPE/RFcOg9UsBT+FLHtrSC1xLf0l3te8pUlqp1GcfO2nuB
TzOYqmSkerG2QaSP4fg1/38POOJ5+d6g3MkOgBWPungnn6OlCX2Rd+PWeoMFcwn05ZSa+KeVWaS5
EVIoxe47PdGeg2Wlp48hSklEqGBSvwrCMScu7fS+LJjj8fRN38S7rvPYONcLrv9sxWfgHozu0EED
8m0mqMKRthHA03xgD+K97kl1uymapatuIbAXsnE4FdtKkcyb9Tqg9dMaLbV0WgzMIyneMK/AP0LR
p9k9ynz6qr3a5iUCmZrj+0rmgqEYbb6u79xH0Qf1UDVM1hTOVrmzalNGzFdR+iszQ+iKGFPqsbEo
HYTPv8DtwHyMY4Qw24LyprP/MG4/+OX1DvWVof0WUb4koA8MvQ123Un6xdYSlgkTQ7aiOupyeut7
AgUyJZQ=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
