// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sun Nov 21 01:42:47 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ ghost_right_mem_sim_netlist.v
// Design      : ghost_right_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "ghost_right_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.2716 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "ghost_right_mem.mem" *) 
  (* C_INIT_FILE_NAME = "ghost_right_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18256)
`pragma protect data_block
0nQJcvJ6sf7PEyVIngOId/B8bUYIlLL9mv4jWl+uUAW/Y+Ch/1qS5deLc/5OYTj5zXi1GFPN78SE
xOc6onf0DHmauk086kUclTPZ1c9+eCp01N52Ak3nrfmls/KTfXb0mZNshH7qdf4ZK1NbLIsE/42n
7seRVVRfi9pg+SgrIND0yFTrTS6sIicnzdXVT/oPv6K2bkOwVAZbhmRk6Wy+zaIKTRaookQoEmnX
WEgQZ/rApkZDtOcaNKpPGDBjl6/hV4aFscC4rbjRaH39ouMWW7ShD907cRuo21v049ejFT98cJKI
g0HqrrAgGnHJRz/xt7iiOkLx7IstldquEVHjDra+nTx5VUhZFRdZcM8tjKJaC8bU7fwjHc9aM8j1
QWMQF9UqRxG5pXSdYvlds90dznO1xAuGfZc5mCXjB1z8cZnYtHaSz24CyeNCNkq5qe77KYhzYEwl
AZu8LypFZl4LYzuVHurtW7Qo2B8CvWkfGjy3JApEtqn/5NtebLARtWA3CZEgOV9P/UlBru9PrAAk
HzNXxRgV4DbwvCQ9+ZJlt9my/AmpKnbQGPMMU2IRb9xIyjT7TqJmlP58wq3QkFLHgiedZKYIeiNG
2Eaj1ItzC+pxbnx1yf9zjsp0S35yvAe3qmSj8PPRKyFOFEjCtJZxrMJ2KCMO7V2Z3dS/cY2quhBP
LlFVJZjYsDABzIvZNBvbj7/NvXI1EjwrovmsvW2LJaic/1/p3mlXoLTKK0dPNhh1XDVd7G2wnbPo
yiJqATYZaIMpYUOvsleoHQDRK7V8gu+iylamMsbrpsP20LUV1/HmYknv6dpNKQsAIIKRaqpSvOG1
yAN6KZWsC/5wvUkrdDEC8sD3hP7KNoG+ohWhGcrPYON7gp2i3l0ztHvcG1BsHzMLQ94T0HotxUt0
6fyYJBNBNf0tIyCA0xMZJEMdYzg9GeCg8YYcg8+pVeB4Gko3+92TYTQ0LJH4wKoRJrjf4Gf9SpNt
+EQZ2ZHdg48NBeq8Yb2JBuVX7NhUEQmp7QDMjy64XOGO4CQbfn9M/5vCfvRkx6SFb4+uTImoEDkT
CuCdLv3ykTrNyWCUqcpUR2UNofWRIhlHyGS/U8VTDkF9cWiSATuGCgK/7mIuJTatIw3zLeAkIq26
ruQTDktmppoJ6rf2j7ds2B82blvvpFSViOXbY+BKT550hijQPdbGeHesmkUbCSnq7E31hGxXtyM8
dzdw1Ac15wOszWokLBWJ7LnsalNU7CfWX2JsRF8p8Yib5kSPgZY86KU1YCdvDCX8jNkhmRPilDSv
3rorAWbNVpmGmlyHgDyMV4udG9QAEYlinCgAKAujSwSio5wG/IcGC7y03hwI5h3FNTvyV/G0Rw/E
KRXZ5JpeA7k1En2e0Xc15XfOmV/tzcWZ1SrWRQP/oWlwPu+Soborjt/vx0fTpUQAktDJPA9DsL3G
XJqV1qJAELzhkmweWcfekv0mbOgAJRfV/IlNeRpnro7uT2oT1gpa0SM1WWMUB+Z4ZBIS3xy5cgJE
IJmlHOI8QWOzExCUDgqiGbQ0IRuMzrJvU4Ra4+cDVTMctbeEu9tEMAPx6pZM5w79pTObPaDxY37a
txjd0z+QHHHDm+lKVpf0w7Ne+45TkSLpR/DK7py15F4imk2n+GRvjfwIUQ398fnEWbdDtpqFrGzo
wBgModkTldj4XGyXz3S1PUJdNSnbanQo/SNTvVeYsXeWbw1bbWuknlGq4gl6DAU1CxuUlVEp8ZvA
KVCrPpfDUH5K8ARAWKjshPbBOBbTZABhEAAlcU+71oZH/1NIR9k+oHjIb718oVMO6LVAaVtCxBK2
+/4dSb5TR0rWn/pc2RrYaDFuV/HTEiLdjBjnmBvUMr5uMngYEm9PNouO28KQrPu6GVUCN+caurkS
fhJ7AuXSmVGGMTWkaovrv8Yjbyn9Y9foEdPwVEnFfnCkyJE/tVs7E0+6gC6iO/gfTWakG/rIqicP
hQViX1SIbuop9vnsPuoiUtfqkJZs08MSt59iN40+pX818oFXvGLQx+0R+1W05EJBOe2ICtVSHXnL
w0tzsiO2pyhRrp5+cyVoicU1FMvFfejk5qtKp4jF3/G+EflmXUXUQMLBI7+gLeu61jBouKa8zeCm
so0pW88xoKwTa+gItFgu5nk2wdR2VFFVfvMspNv8hGGGOwRP1xNnQ12BiFzFdvHbIxxZ4rS33gtU
OsgRubNUxrQJB7sgoyy4gDRRpgLXNAbEl9ACxViWVbTx5YX4vpHe8filywU2771MXcP8nlznkkkw
aK7Sc84DI2D2RF49PeuCdg074W4+IG7PTeA+y895BMm3ZS/JdVDvCLDtViYgoQ+8cakfmWWcaI4q
FQbreV4+BxzsZTCuotD+c61lQGgX1aXwOg20rbnlPhpFJUbDboj/T/IFZYvnJiOvF+yQEp0AaI/x
W6UvQc+HrkunTSpWWMuZD9ASl2RsNPqGbBCS/sTBQXpAyC3wyBgnFyI1v8PQ+syPOs9nHfbdSHjo
zWPN/xeoyJsgskO6TkV6rQ9NJRUNHLHbZrSd9uEUSBh7g1b56VaVN4gZ8NJe8p2ksudkRrNwWSqq
YeKJTSnSG1eod4DL7D5+6QfK8fqbvWc9WyRVKm+2qSYpLy2jyQwge/TcnpDime28lRoFAPPkuOy+
Rihrd+SrPyb8ewYuzMAbT7XgumP5F89gp6Dc8Rcj6NXe0B8RSedrUZU/72EfxBRB/SIYagT0uEJ7
oZifOGNVdOAlf+xnZ+uLJ43DpMemdwsxWSuWUnf6bwe/7bxvno9zt8nsh3YL5LnWol7Z/McL3ILn
ZiuFxfaotwkubqlSlrBVpTOZlxlmnKjlPZDcisfFL0/RtrzIBMd/fX7PtEqbTZj4v4GHGRNFPIrd
hP1NSmbTxs5dm+kyupc6dFFytO7EB0rCoM1O561Ji5fMoJxQj3f6vG4ycUF2qGdHN7QWFWhz6Diu
sdUr2NNVfntxW74a3QCuWrvmsSCmHj5GWPfoTqTe9RUvpACkF3ptuc1Pr8E7BBSqcbqKpG11L05C
ldCI7X2PujAUNWsEcn5c55klf/3YS5aGfK/hyh6KwlpRwbvb3/StHolZdjZZVPYZuw8GKmseGqg7
uV6b9mCTScAOfkjJQmJCY0x7p4ID3mlp7bHe3OnDZz/hLk7C+kUKWof8p3G3qVKzdm4UjzLIgBFv
7UwVWjS3Y6HMPLCOlcjQt7dFjCmdEMOsYCshciIluLo5DW081GEtM6YYXbdaqOr89svDzpsVrugy
UCz5K2KXRXvHfHF89z0/i3cksCMXkC1jyClsI1HxZEIUWNH78SY4whJctKrRwkZ6ckJqQxiSZ5Q2
LUtifUa2RTbYGJXUCkibT7oQedYep5yCPZ4Y9+VKYbhVpqZHZbLyNGmUnfDkUD/9XlBENVpXIbky
MqzjBMy+vMHRUENlkDN5WJJ3QfhWyhc6XBljEkRF9s7Y56HNPxD8M3YrabVwKkHju2sinlW01Wlh
2sVNzq5Er5CGOwADo22xVK0UWvSeCyVSoDXM01ssrampvB17ss+PbUlPgPH/ldrYV2wEmdPaLl5I
CcC5TgH05Dtw7eOydL9yTZ71gD4qSkF3xhV/WHXLItnKSEJwUrhPzMk8VkAf9Fip7L89zRrM7Qcm
848uRvSE10Oc/Nk3d1lCwgGPnnL+pvR32LB6Y62iTeXaUYTZg8wYoqIjKrAmpjT3BguU96dWYR1k
K5SGY18piGWY+FVgf8eHLH7HndQ5INibMrSC6qMS1fqM30t96MR+cTDuZ1iFKVULOIjD35DUZH/I
Pgo9UZNPJs7Reahgl/iHHuDRQTa7AEI5gIxLs650Fg1nj92ZQtmS6qFOiaC4hbmzYYyH4u6fzNTL
I7lrRhWlfYJstkiXMoChzLAo6qnIHoo7niChHhO4b7cdPejNe4FMhaaYI2vxzoK2qsiCIgozNxAp
VNG1IY0QS729FZQfqBPfn3E/3mKAeLL3qO/Y9Bnn5AmXWU0vljQxdYHEC4jOnfFs/GD7qC/LAGlg
u5jmoS12qtHT51m9jyKz1X2ToZMj6SmPdJPpaj1HgYgWJotgiaEz8PibgiFCSGz/MR6gNFsWlFta
UwG+izhQIRYVCPSuQs0j1l7PVHPBqbVGpCAYCGg5g3JstxgKbDGjODybQyKV/03CwhyeEw0NTpmR
dDJXeDLOJNQuLRRo+oMrIPyT2da53OaYs9KOPUSaSGnazrVsxJzslXgkBQoULUUDy9eHYAIvJQo/
LNJO4qTYzfcMprHHpQ42yzNs4A+sISWLTopTbgbp3gOYwcDqcygKm22fel510ez/md+p2HL9payy
3FbUSLwxKlVl9h4PVyjVxTXNKol4caBbrQjScN5+m8rM9t7/tSDgT1N3UIVH/hma1OFAqNHgxZlJ
wxokTodmVvHJbg/V7we3qww5PpQvG2rsGMX87CzF1GC2oIoVNQdLLAqXhJmHxd996VqTFeS2mHw1
NXeLwDy0WJgL8Up/jqOiTOEF119Yis1jHaLjxbbWGxqFFtPEtpPVzjZMu5p2qmi7gx99k+ZPwDfE
dgfInnSKTvfTEMvP4Ug/6mLnO4gAmU5IhIzx3H+g4P6oDuooXOUrCcOj503XmwhlX+d03OgdA8Jq
gavOLg2BQmFZa/Swlt9LfXYYWYMSESCN7zQyUj1xECu1mrNlBSjd7s7sgjvZaJZyYhzDim+C77bK
f7OW9qVfCQU93DhsP13Z9NNrGfFpRmIp8myd60tHyoLmvfnGnKnvGsEjXAsO5szz8rRvvIIi/u93
rH8dmZeG2d1EWH/VKxeW7DmIdMduCvkSKOMujr9ouOTz0d6zkyXZQUlk5JNQNtlGwhocQO4C8pQG
E2tiu6aMP/wRO2V0eyiJkxBkhEDJdbgyxefjo10rsencQO4x81uqAb8aEAB1HYNou5klViYFja0E
LYcN623yw62eoD4u7pEbce/inMlU8sHibeOc3fcmaxQdxF1aHpfwqcOwZdb0mTQ8lcc9LN+Mj2sl
6QQxfoBr/gC1cok3o9AvfskPF7sVnWhBreWHyT9pBgNS/83DynnSj/RhkOFw5+dhWXCv74Me0tc+
jpvxffMxZaTbMn2/Dri15HbFgu7L8I8ktLY0/CSoZvaDhqeLtdoqwvAKPZQVeFVWbl1Wx09HlxLw
2JiJDOh7MLPW7oDFjBSIxLOMyWjAvgnJved+buKyJLf189t9B4BYnqD43HBD1iNHyW9o4kPlv7OU
hsxBYdFDkdT8r8onlt4uQSflKAnEWf8p3iudyaLpTdxAiZR0tGGNrMFrnYUKWw0W2FqoTvl1yAmR
F26eCiUvT2neVf2Z5O/pubPAJe0wMaAYz8D11M3+XWXoHtcPTmPGsd937CiPRNB4NfdHMcgPhQew
MSaHWAZDZdIWvhD+kBU31UgfmguQ5UdXwhnWmvInbPpUfGTP3VBBOhwHguQl6myRojJ7CXrXhHsD
4MyoYQnNWguKVkLUJy26E5XJiLYsu71M7gSB8SJzvqZ83qZpk6ouVT+DhzN7+GXypC9t5dmu61ok
1f+7ZL0H+aLiE1oe8SsTpnzwb1REuqXFY7X/ok2MbdG8y+qyd2pRvzwRUt5y1ZOC6puIuU8er84Q
2w4022UmPXmuZn4ZXIaHEOuCKXrcPKNcVre87jip/oF0yLERoJSFzElbqY+YYDnJvceM9NwztP8g
+MsVlJBlZmnkTqil8NvYGaVvD8eNWD3ejgpNYr/MvVhmf33DAOCyPz/nm+TSftS4zK9rqa3c5Fw7
jh36OrpnXlzZ+G+YvOndAuiddiMp8VfmqOxxQlYwlNOuavaYjx5AkUa46v1elWK3jKgv5m2jE73X
7edJfQ8aRVTY3LXf/2ZHPjnknkuHtHxN+kbn2DjG9rlLjVcyj6utRJxKOhaC/BG1op7vstLTjC9q
FSSzBIOTpYt16flyTxQQaPa7r5ECPyq+7umzZD+mj/IsM1BszD38N36h+OL9ZOvTWCWsy9lBw7Ii
t/v9bKeKiFg8ELoaih3jWru/Wm0uh6Z9zQ2TEN3wT7xifIVytgBTfyFviBJUIGpOHbcbeh2fku+v
2h/6RUxCXt7hZNiyZTcjnQXqrhCYFXFfQxUmYUhgzoHhCdhnlhojv1mXkvUXRctAmkF+H2kaPl3P
Z3qWHD1GvNNPrq34FrNbgWBE/xlypNn9CXiownkiTN7R1NV8AAkS2a1+Ll86DmFADOStCG1cWnqL
DCL2oouktl1gFS1ZcVWv9IylpyslmN46vAu8FALpOTAO23mUiZHPFdf5ChOk8lTvdQnVGcvblRVK
f4chI9BOzsdnOWv/6ZwTl+MyGf8sjRjM5vfIJLlP1HrVKf8YMqJMgw+5ffI+CsmsQ8UGlllc0OkI
F5jK/vhiZ9Gn5+FIzD0qTX2lh5Bn4L1mtol4P9xLWaLyVc1oNzdGQF7MsaPniWlexFHGEkr9X8Ec
mR8SXfZEew3q1GrWmfEa9Z7BuoXQU+klsivijSM9pAbUkQHi18xpT0JVHca1y5c2Wf91BeNzX32X
K7k7yQ869niNuVYHOKVIVR0YSQKQhN2PznNAruTj3wxgKfES+nykoFEF8GA7JqaPkh2Yf7htjc4c
N+9WbF76+IRCTeYXV4yxO17GYxA3mMuopSPD13yDwbsijLhKylydADRCOOwdukFBs8BT8LixIvGS
zteD2e80P6/5yvNMQ8sASapgbyiyl3M9w6brl3Jualk+vJz3+h3WoF3QlhCru1xwmm42/RmHzCBm
dghUQ0zT1qjfGXgH19qljZiyHgOiRURqqmK6pu5zGntDxCToYRiBy++NUD5hKPZgYz4ygSI5Qyn+
Bwm95GXCk9IVRM/07+XwAUVfF5n3x4jitH2zJy3xyKW3zSS9A/091kiSFtBJdFUvNdrX3Ow3RkAp
Pi8/BjiWM1LnPfSsQKECTroz0Wnqm/20+EXIN/RX9Qieb5jutbwfcOzjIpyn1AaL4jZfvSHgqvwB
VfgXzmDvS5jcSPxXR6CVmglWqSZkbWLC0PiU83lct8wqMnb/E7+wdPX/BRiA+XmTuT2ka+qvR0PL
IXpOhsJboZNtFP2kkCrkUn3bLIqxbg+a7Nc7ayF6WH2l1dvH7P09lPb42DQREyxY1AIsJUMSiLcv
Ki35vZvnLHt9QJl3yAduNPs4TBwaF00MGdh5bQQuQl8o8BKlrj76hIzKDZDH/TLWBrYAlKRmix6+
NNzb3hEYWljasQQ86/nOiKNljUH+cBdQhLTwOejzJtFBvizLhdL/vqXRuYJrdi5q34Rf8AX4ZLhi
lEnmMSFc5t0X5ym/221WxN66Eb6REteBZF2vORvTzoyy04yCyba0WMycSghMXas44Bz5Pkvl2bRz
haav7a7oIK6uyF9HQ1nYsAa/z+34Va9KnutnJCtrrwU5og2YKoagYB8biJ3WmEJjmNQyQZWcytwu
vrqNWx0qAYCIB2i0mTr2aVJ2cSkVZYve1JfV9Lbz7lWfytber/XDUrwf4MOE1SOkg+aGNSn7c4P8
qEOD0zIQDE1Tfv/oxUffTRc4py1L0SYu1mKtJorfjfRl/uggjHRum41yDPVNTlGJQf98Zu2hF9iH
PYaAEdaX7C5m6OKCCUDjnYADaV2t/hcyOzgeaamvbJhH3XvqjlEko6wZuEyrUsqcSOZSUgRlZdS0
epfcFeX1+dgu6PQbY6N44L+U+TrAwkcMXF84iz/vZ3+h1fo7MHnaJjxIdwWFRRvTklqIzMg4XIih
eW5MplrRK523wlXnk9EiH/+NxTXMYQshqDCGqvUZxWu1wwxkSPqBfitMFo4K3v/wv6rgzRvLz94P
fozvWpCtOtZ9EyP+a+7ROzfhLX+J/UnKeoQ6Tp5o52wfGSO+hP5+SOdvLbG8ZwIxpQNxY2lJxnO/
ENkPeRy/xZDzOu4N8DvtFOPzVBsUEd3farsEjeyeDjGqIkeiYlzgCQSzu2fn31xxbYBeilK5sKFV
B9bgy6RB1LUaGZ4uWsfni3XX5QgxrIhSBIVw+2nSaPL8n6KiUjf84QMqqKx6FNHO7lQc7BSZnQ3s
DClYg0gqMJ3KR6E6CRLpO2PqjzPKv87Zc276huHBSJYZkOeCdq2Vkj0SVAm5pKr/2ppeYcejXBbw
mNFPIsViF7HU1U9+oiTc3nA/3p302+aBUQhiBFgSouw8LtZL3mDW6htdKx0HipVd7ASLABNnFmC0
DtgQa0ZcvOEmL4q9MMD4B2V76uAJXCGHec+GdCoxuxOM8de1uLN26VAue9uFAhoVA2fXz0/TslIY
QJFjFiDUFbaH+439Ei9IlfcKuaLo/TBNU06c4Caa+UyNUoL/42W1nescnaajiWtEPh1zroqZDvmD
B3Tc7OJjXbS2HNjmSwkuVxgH1EOnyCOVd678iaJDWKmw7BZQCgYhGr5hUV5c7aXnK8WABbv5yFM6
m9n+YyB47VTvMAD1cpt99cPgmJQEn6I0Ssq6zn0tU890CxVwo9n8wNp9mh4rNYC1wrVD9zIZ3r5v
AFrkZ6yzgW9QeseBAjTAL9KHxW8kaYdpj/X8nnt1Zbj22pKw5FkSenqn4xL+bg4uEUQwSybN9vIt
38pnIPLTw2Pzy0E9eZwWbPaRvRsgpptF42qqmTiwlJoGRppNmDHUI65Y9VEeV9YaUY6gEaZs5aTz
cAQ8ESbJvPBFdGZFpnYxeBU2KK2gqLgCw9zCI6jrfRugBd2J8td2u/6d8WvhnCoMepeksoPAxoGu
DnN5B/j7SqmEheoYs0sfOofSH12a287H54Rr/MRDIUFVCRaCIKR9o7S1ENgjrwSZfrYGF2jiz0D1
+lkYLzsiXZnlLQi2SOEepcA8aZ+nDMUDy6krKAB3pz4gSEBS6/E3z7vlW5QxfxOYMT9L/m4fCPjW
Pmk2maDG7FaBNtg0AVMxA7OQxEH//fUe/YMZorrX3Vr8LmacvfYkXyNQ2qqBOFKenxQY+TRO4JJc
mCYyAUVI+EtPDMVSS0yiDpHnXGjv9O6xRUFY/LMteb6H0KeeJOEh3YQCQJ636t7Y6ttmxMOkwH/C
BCS7aFW20snvsf+uzNTj3edjopz6gzkLGs1ALkJxTYTa50bCTbSgAux1I8s2n7aMSXCrw6+AUD98
Af8cpjkOmZzGNXXtv0nsVFlvJiTGSSUO0gjp4pNBMGKh9Rhz0jfrq6WNF5ExCtIByY3BsQw7AOzg
9Pyt5nhBjrF2VNlrgPHFz0ZpYT6GnbmAlYnR/TLgBg7avP/YM5wWQcMRz+j8A4D2JE02kPANNhv1
UPhmT+GBTH2pUL3R5qzsHYGqCO5HpRvY6WvZh6i1ZhKyXzudpvfQeueBwuO+BcS3BE7RQ8LZe3oL
qYwACYcfIXDq/9+PbIyOZKyjVeGKuwcOvjPF8BPFDMtLnbi4/AKEg2CDy/ebaF6stAXOEISOkmqC
Xof7G6+SWiDwy/3Vxn9kSHkMxxH4NzcYYE7fqREumnR5zA8/nGb2QFn5dGKbgYtVfJ5u6pSKMrrB
kouxp4r7dkIRw07PUWutF2Q/TQaQ8vEWvhvFjtJuYE7t3gNqMPObQ6J+fjwjt0yKuvDKMdg9cyVP
TfXglRojYk8H1FsqpbnoXMNZOEX7m+72lZwhatqFd+IrOch4hneO6OC9Nd1yEsPpXjqwEHiwioq8
0QY2G6YphBv+iggah78BlXp/cL27XTX97bQeuHUAGoeGVS3eYr5pP/kQ9ygDlFjwGXlx8J2FtwQQ
29Y0eJ/iYJ2lDWqa/IOzqkTqyc2EriwHcIkfdPnnPEG01/rQIrD2CUQTWUwDh8mgt1RpThBOX6f+
HiFQHT2U9RUwNs6nlrj2jEdWEDxEr8d0KXi05KakUwymtKve+RwOmZPA/JGNLWWcXkFI8aQlqdIz
Tf7rPMpbiCvE4gA+y+lzuK0dpCPeuz3EL6QmxaIfW8G8ngsOoHNXZ3fWJYwjBvkANZSnIrU/Tss4
NauwfTYX3IBb6ipOpPT93GZ9g4sFmA0J4/8wK0bXszOoim+TLsSaHCh60TcV3pzbilTM/6yQfA+Z
emn+4gVbG4haSpBAURR9HyyRhGZj1oJ6gCIsBB7hmbWEmjqKLB0EehwhlvGIrcE2EC9K3u5Wp/1u
ffU1P2eP39CEJv0iEzx13HPPVHKYtXhqMswVwaW2oIs8jACK/wZwQOyqkMtnI3IfWoIktOGWylVn
WJBKpgHjhpp7ucJw1GOIZzpcEEkal2dAX8XqaoAocWEncbwcG0+Yjb0WsSLGxoMDJ4guziZPyTKP
VS2pMeofHT/4GmNFR/x8xEZ8nqdNoA246/eSiJuFSN+pSzPFCV2cZG2JUbzrwa/YIBITCd+l6etF
1jVafz89bQy4MtmMV+MUWYQvTYczVNIdPegogIRalnF4iPmATpybh/1McKwi2oZV2ta+qrdlXwYx
nmSU1q1qPSfrBbqgaN7Afz4Yo2etsMR8YieLWP/Hyn8pYYGKJkjze3DZA7AS2Pm0BV0G2uQsGUh3
USyD5Nyf2q4lYgg/xvRo0xK4VNFZx7NFqM8l8/OvqEcsE80da4vxj3WO653MeIIstw9HcnyDbJ7b
1qvfQCLBO0h+A9ObimObgb1fGImHv9xP485ljyK5874I+zcKHHaVebZphf9RiIcxRO6xVKCeg1nw
MP3/m65Ma08QII028K3X5eJf4N40C85bteZGKufCtpWUWN6FNDtaZNODAuYMLym+rMsywpyl4PE1
eBMAAfaoAxqSAhQboC+w4mIgBfgd86He157Wy8oLjwTeAFAkscHEhVD8sHQ9TUkJRpydEP5QL5xs
z8YDSrpvL7Pb9RXvMAq/Dmfq4nc5nfHDX4azEMLv8yEFLaxkcgLbFTiJalq+bCkVrvy/l9xyKJlA
cK27PIicoDyPvWQr5XOt2VZiykr6BJjnwqVUMegkxNpaa5zByfESP3CIjq+e+HyPkLvP6h+q0hDt
mAYBGgd5KjZj+HtBObvKlHzYmaetvFx799Ue1q2om2z5dEHf4hx64/Hj9gOOvk1Izhi669LivnA6
NiBffDwta66m1qu/HAO12BmYrL/C3IHq9naoDtPQmgV2zX554b3f6S4Ut5TEmYTx1Bk4f8vfbCnw
QXMi4KsrgOjV7clzWbm644zlUkhkJJuyUJr7EkBbyNJ7kpiHfJmrgpCIwyL2KlrYhe82wg2bfNm5
r7dLWac7xQl5HhYg4pKiQ3C3tD67MXs7pr8ab9bYApC72B0hzo2f6NYVTG2xr7A4XY5oDGc63Jp0
9NQhxAfBb7LFNLA+7amQ6QKse3jlejVk0KpRDtyshahJER+RWi7auyNAmxVI6/4t0L6IfuVMCeLn
H9Yj7tDoCEihKRPToOJsRKZgIf/KgevOJP5Q5uaESrNnYCFjmbO6cyFmNK9GYjZ+LzdLRVoqpqPB
shM6W7Lx1tDR1MU8b8bAXnGdfNxmVpLJWmPMGm/7mWqI3m/FPUZikrqRN9MR4D40H80XzrPfYcbJ
r86kRx3our9uZi1hukYCpttrbA7+XUYkiIK1etqTeGpP4nWabr6gQ1GM/gyLB055aovlzJYPTzqI
zUMuWvf8qHWkzGx9ZpGUhGz5Fg9fLOXHCoT1wn0bmTG1M+VoStsCR8MMn1EUaRjPB+ohKCIMSwxC
DiUwYKWxs0NqXkBpwsp3uAVYP3ay1jZW10AIAidbRPMeSgoUEU3j5TdXTM15tVN/BYOqrcmy2l/o
EFQjxNm2ARv2kOo75bYK32FBYb6W7ldNBekmCYaiJQHy7HC1n3Z0R0RtQsUa1CJrGD7a8Zeymt2V
Fi+UHMn4YCULqjdxwC2pwVMI7375UdFUNw/YlB1uCN6drnpaxmHQd/tAeCw0GtXHAOZq6NY6visG
ncjjxQJooUMNVQypGYBQb7cvY1kUPktEW1uhsOUq6mNoZMQ8u727RkljBzlrA7XMS0kKubDtXj9e
7FV8gE/K0IMYQ9USBeuCTvqHbYPGoJ0Eh6kOpA6fxZ6leypEtZJqZyU1TRCZ14IKLwQ9LXOjldVM
6nsfe9nuaOqG0sPWz+skibn797skgIWn2oIW/bmw9plvTh5bCRpDehndG6eW/OlMnZ1by4wKTqOw
nR8R4cdlJY3/905L9Xyqt9UexlAe0S2pU4QYwU6oWWB4HZdKcpSS6YCwSLt3WVIeU7N5RK5FTh7A
K9mR1A/X6EFAiyrluisaCFVENsXohaWzu+1DGkAmLq7w0EVX4TxshkTuIOAacu2dc9rfXQ7OA60I
0NDhyTzFR7oFmbvZv09VH53YnaeWeUqW9tO0PIr05FP456U3BpOwtsze3nZZI8nI7jfpvNPwbbWW
n6oWODDjDmzbzW1MXX69n0Dno15H/LvVkvvSIqeYB4QlW+CpSi46w7mh3spzEZXTS4EKFUyCMPUZ
9h9TcX3jKvypHB2Jr7ZHVDX47/8GfPO0J/PZsOPcfVW5BSZn2evgSReksZxZvyUcckVXxQ52KS+m
2Za5QKkAABOjlq352d2RAvN4vnGvOiC7AFXgPCtkcKIZODSLsWqUoztgzBNzv4WbMGJvzlznssgE
0KkLYS8pac4PNUmoSolvOw9oe8Wmq2G4hxZMssGh/HqCh6Mx+qbI8o6gxX0F8Um8wOTA0NYzJy0f
mmlFysSQqZfh+Ljh8W+Td+c4qCkWV2KCWKPJRcvm5cp62ZiHUiJQ3CbLpLRNPYRINOQA/b7GLPtP
WYtsMBdW0+FGvVGQR/Q5oJPbJrCqfWCtI21HN7a2hcy4Hq4jIM15B3obStBInvymBXNG3jFBxM9d
7LhLSq2dinuBM9qWoldJcQ2a4rpnetbGDA1wT9t0NI3L3txOpQyapd8CKwWZ2tK0hN4cEpKMJp8U
l1iw8JglcWm+1Nph806DPsugc4g5UF9ynHWj8KQIFn2n6gDpDfVZUQO3e5Vn9XyBd7IJVl/iCPg7
Bb/EpakrytNMc+xXFr4O6chQEJkAK0Wk8XRg6DsqoKBilxQNDypiQL+DC2QfYG1OMlIMd2QeKQzP
HX8a4j2OuF3aST0e+g8qf58qtNkIWnYK2G4GmsMTEvWOufuGddTC6MeRLYiiqHOYEGxi519usRbo
pEVurdZreT/I6PqFVCcXtoFFOaBQ1Vgb6Cds8eMox9V96VX6PriykKJD0AFkSGYg4IxDmMUeeNVf
c/fQzoFi47E7u3mEfQP5g92eg9dMfNAdyhfX7Fn9/bVa4xSYtcNe+6ej23wxoDyDTGj+PpVXDYXa
tCjoGLlRw8h7kAdOzxnoI8EJ/LhoMI/botm5O2Z/kgwO+3Eb+55zsUOUKf8AuJgauLLK4P2bzrXM
6C1l51uNz+j+n/SngVtqUrtYfIpIrSQjEzTPjFrB0jz0k2ERWzt9nAIX590VvLKakOcVoRGufIL0
lBsrMjd94teXY5RTZhHKYqnmcj6874LdXYtTgWIYblobTQeyYS1UBKgGGZRpfF9a7OFO0aK7t52r
KCk/fEQL13T0MxBo++sU9jNGqyl8+9oG287XSDRft4z7n1SLIBLq9+9PtAV2/wvFLpiTTmo49UxG
MZWtEdq80fuGrhBDGjLQNqSH6VxG78XGochkgnFTISS/L/bAvtXo9RSsmUfPFmrkVri3jzZ1zESQ
zu6vQUwwqYFiUH4ZprVtOVf/N24pDzt7fvfB7Q6vUtHfoulfJmU1E5Dhf4PVscq8nNDtO5uN9nQe
SozHGwVkHg0lmuIpz9LRq6OuYQNM4gbWUOswlZlQ2UBHIKWxW7A96ViAaFtr9QZUV1LGare8WJc8
QvjmyhtDVuOCrijemKwsLyyS/Bc6HPoOB99ow1outu96dch60+Ak8htOKRFXcKILrZiF5+uZh6Fw
nwvwjjTZ4htECElsj7aVFE8tgVe3Uub96igCsRegj2nzV7rl5Yy0tW/zsP6GKpT0A5ls3YvfVzlh
tTo0OKqHd/jjAQq72Muz68HMwZClhDbHXaufdD3xevnKFCP2VHsRhlNgXlv2bmSUXRpO0zNlHSfX
OKb2Enz5sgbimBk4wwRl/1KOBhSX6rFOnYxl1q9bIBXty1BVaIqMXzQ+p5tidoW7WTUqPwiD0wRM
7QwjxqgdAEwxXfpqS4QR5DtFrU1DeaKIoUtm51vmm3FcfFGL+2HWFu8rG/otHpmlfGQ0RqdAl6n2
MEqKryRojD4VwJ0Zohq7DiKTNMzLrDtpnpjmNQ1WzjINUpIC+o54tEzvkCvPXlDc1z8KxsAp5jBA
I4aGOvOY8D+nmOGjestnzJ7eXHWloMNT06MXDzhKGhR9t1j53NyyHr0K/NFf/40b9sZ4s9asfkpV
rWCgrQAF8evEcuMDM6GTl9ap2QKsK7M1W/MIEv3iO9yXU4Rc5fBumoTeMlTAcTULYJKgQWJhUTn2
opdhCPIFm+8nB3BcmbHji9ifZBJCbm3neyky/Zx2aL8XRJICN9VnPptP6QlKk3HghrVhlU5SQ8VS
ZKPhbt8Wx1EMKpGkFXnJo8jJXWeXul6UcGaF8CJ7nh7pZGrCDNR7mQTSg+kPT860NSNt2JujO4y5
Qd98eWFBN5bm9tdfd4wwFoh/L+tUgdkfOqDHrMT7uU3rkyIHBO56FCy6RK8K5YaQJmritANTYqyh
rDwrvsmumaFwWxM6IxWUFH4Eit/C3DQRujAwAaqRwGLHyfTo/ggVkwN0MMKaHjcpqP3ZbeK4W1Vj
g7yfw/736e+B6GsDxnJve2egxtj+r0LnXqHqpYneBFuXt3xLSMyp2R5qgfb014WQFFVdXw6GX6ES
28qlsUt1txx4rXnGV6/awiljnp/laeJPuazrwXDampysJ6gIvu1QKHhE7AqchDS3vsYEvy37nzkM
9ZfGWfLbnm0Zsn3aRkO9yBhzKnB2rinb9N/1feeyq9ygAYD9pF1I7mGZCYEjICA1vL8kCnYkXbVA
nTcpzYamX7Ek1p7xIDRhSffTw79Edt9o9l3QvamKq1hUX/cxt9j5LY3AqGMk3GOWCZvd8gaaN6gz
BSw89oNG+I9y9NVbPwYNaMTONXrmrgWNQ50QgfbCVOhuEvv/uxqGgJRQK4l6SJkBHGWxr0fN44yF
fbrpQfWDxwjLCFiey/z3O8S2+2udhfNGAxLGG77FnShhjXrFpjhx5fIIDvqucvoJcxZ58v3Qvl4h
W3vrRXPJnFsUG7UixJz6dHNEPfJYmAZWszocetHD86deVRNyTsr6C9Lk9dicLiJ2RYCWU0e+oXIm
w5gBE8PdOOkl3+iJjUPPxCGtLgzKy0kiReL1TdCaEWdLqk1Kg8e1eFe5R8pIFzL09r7FidGL9M0k
fIS5JXxR0CR75f2uZqA0MxEG9I50g5WhjMUVqxm47Ky5xSJ/0UgyPW7IWR1BPHslG0BTGRE6Md9f
rueBZZhcHv5k/J4qp8iUkU9IjakSvlFhpyndwaljMgYWAVHvzaPw8tMha/ScIaBKWI+ZKvwwk+yo
/kUV3EaNpV7L2/cycQfO4x224sekbLhH7LrzL1WoOshpHGFoZW1oYyPGQfZhoPC+w+pe9L+EQfAp
oyUszf57pqqSYZmGftYH/s3vIIneEaI7p/tRIgQYk0jUrFe7EOv1UXiczaPEF6zEd0uxM+2llOSB
VLC9hWATH47h0hySwvecpCuG03zB4L5UcMUGwJ2gPiIo3V32r6PmwRZHltfTKy6SH9reCWpYHdDS
fH5riC/alsODlOq2lmh5yIMzYUA3e2ZAzV2CA61zEEtUsFWIgP1IdqyOkh/3wktt104TfGi2LdLW
xAJLU5QcF2dpEivbU8+HB6Wjxtu5aouxgnd4rsuddfx/2lJLp/I6MFRa2Ggq1fHf/Ov/SWt5In2N
aGvdjo2F6ciPgElVXK85DypufDFjvs6veOZvjVqvAB53zvggA9j2dBHunNFMca8zuflepvSbE8fc
qkQVw3eoED99nDcA9qxGEOOwU2BFH/3grS8MzjowkGHlhUtglMaT3HqjewiS4StfJUcVggCmu26l
daUnSHfq2iS51+00ExLL+lZCEsNuw80+rKozR3VQHnThu+l8MRrZsnsuDlRh0KpS2p/NFDDobCxU
B5TXHy+vqqaDgb8aBpdlviDQbhnfVm43ufItkITNqL9rILJWTUFVMpyhIdv8tYOJsTkJ7MU8GkOu
FMD+ZgC0MP+L7+/jfmeHe8hRpTFQI0RPgy7/8oKZfSgGg3V93cvcAHgJc1xemdXA5rEuTIQzkpCP
4GYfJwlfzdnyeJOjkIbOkAae593AB2hQBRg23NHq9JdDot42S01e3OXbdts2Xf20xzQwsHeELwwQ
pFjeW4GspRehGxA/dlAnbI8XC/KzIXKDYmPT8yUMnzoLTurESOfQw6wBzQcZuBhXEsdwC5VXZem/
FRjPz/YrpQYbaVZxZQh0jxSo6Zhbxq60+F/o3MMTH9FWC8Dy3zz0xdJkrCXsjMWTR8QzFg0jU27t
g8M5X2KitlIo3bDtlCi7d8Fl6qQQ/bsvFzCqzqw2R7+9n0o3C6q2fDW+4O+miwjm4T4sEACbkAPH
SXFA6o/c5csxe84caAYooeXLOA82wWT0UfvM3NHca+mR8c9n+s0WGWoNXsz9dAXvmEQHa0oMm6h3
TJZ8Xwcqy21S4FGY9GgvBS9N758M7Vgl0opSEjvNh4YERo+TDWJ/Dv2Nbjw680W31wIPrTSNPU4d
tFw7qPDUOIFpTVyJkzXbZgukt9U12NGJFA9kimMRWs30EmnaEdDIEXYlxB+rWFZkOycMn5tjZika
ni/Ag/Ott+Fw6Nqr2/+d4YvC+4/spa0XAaJHwti+/3JvjzFJGUnUtzOOhVCIK3ZlLsl7lPqWTOiq
dp/RM6Z8DYeL5m0kZV5f4Pjwyk3v9iSBMHj0Pw8d08Dem7DanfYFqR3rupCrU0tvN9z5GYU7xkUM
qQpJeOggvV5yujlTyecZIn1RRCCQP6N+DnS9ETWWiX/Qgooif8eM2cvzgPVP5QMcweBNSoFvvufI
ZiqM2tQ0eOwnJFF19D/7ThcW+B2LNQFKyNHfzDOSbOikCMiSPbpifGXTB8F50fjU9e0BE/2cbqye
jNTjWr9GuKzxOjxKLPV15D+bbEisyP7jInvCavc7h3jVVlD9LoSIPmbODScDvVM6NSKRjK6f7ms8
qYC/EFAXWFKyeknm12K2bLGvHJGEMhWR2TbWz0jm5p/Mgv/0Cj4n7/naCH3Ra04aAuNPPNYz6aY9
CcK/ifXvT8LV8yybfhla8uwfrue9EKUfCd8NvR8XrueXTwb6jZMA8XqW5W53lZUVUjpwXlx5VVec
wHrBZ8rwL//cFaYMDrIWU6+1LJfRBDT4HwW2GQHy+5+mtEVFRSRXoI+5XnFnerAjm97pq9STMJgy
HHxybzNIkAkDKwbHuDt1X/mPAlE71WTFOMgthcEKoedkwZQK9pYdmbacBWd5tb0crCFtd9DdRnUV
yNnQCzjWRuZsneCHe+PYdtaM0yd8RLd/thUkbMHtJklrMkYQnxIXe2Des8EOvnm4R2dlKtPByYuH
y0UBknU9lA6xHv5UFZI77jAcMQCPmwVRwEx+vBEXspw78niyjueKVxqmBx3xJvNX4aNwCmocNFik
GIbtppVOIMvogDH4CkKfscBIxXFPvWYkwG2+3PE1OVxT3cs9BId2P8LdFn1ub5gb9uflkv8uWxpI
o7Xveah5xZ+ko1C4dP3NrXZ1yBHzKS1XlP6Wout/nsx+vJ+A/v2FVHq7qmy5tLKDLY7YGwjSRGUn
9+jySu0cMAM7o3f0Zp4Euy1O7bKIv4ViwuBg2MfzjD1lz3f69r8xgS1UeYSIi+BlB34i/CSwdmeI
MEVm6D2aoizk7lCQx+NOAIL2Rmd2Ly9fXFRoqGp8MPoaTAwqkQV/detaoRqujbco3fWXqzMe71hm
WhNx07Nj2u/HmSn6NsjP75rfOxeiAIqZlnL84qTlt3Po8/yFrzrDmNJMnTZyCcNAciVbRe3F28r2
bEuS6H2wSV088dQ4HnuaSvXU92t21HC/cLxR6FU3lXuPeSsoo19MWTYeOLK2RxZWCfsDlZ+frLPl
SLHQVdu0Tfq66Y4ndlVbnbY/NO3HvxS6q8xWcjtMlJcppkxcLua3XF/2bNZ8hgPhn051msdupFho
OrznQb9sGPaa6zOxBLSv42rFOfYaWX7sl/tnqtxcx1IPOPgjGuypAeqMX0VlvMkmzYf5TnxrnSjO
wJ0BU4bdXBbxLj35W+CGeYdBG0HbNZNkJRMiE8KNTsdISTXyD/sLB/myv5Z5OGazsYtrPFWtO360
St4qC53JVA/Pno0R6daqR6Qs0E6Zwwbe5V+3+tipAnZJLdnj9xme6hbZNzS2T+fd4JFpHI9nMGD2
aCi2sQXz/R/PU1Sl84VO986xzaeW2ewG1GHexkXoFKKCjdFk2m1E3OWmxFmBgjnhs1SMl/CcfyG9
QwBhBAVwkgGFG3t07/uoOfZZy0wf+8wmdkAN6XqQ1bPFrR/mO9dlOAbNoAdOAVw6QdEdQQ6b5w95
YkvIViLxblID0+SwfqQXuf4byhSsaegXzsPXO4bvdyxUB71EjGSRWtNajnhe2haWwxMV7L97K7BI
iIUmy4whtyagdJ7ggc+9yFP0gM0kur93D4VhgfMRMBjOfwQ2Y8RLGhWEDylPHpYLaIozU14qqrB+
jZhHo/VVAm8QtFNrDH91QV7AaQmxwTBrH5SCNNPlZR9m+qQg98Czkq0Ahz3uVvFrqWLhA4xaAP4+
kN9XiRIGPuopbFLqx9cEBlGH2puhfxjs3QWH29+qNrzFFLK/+Tz7sWYSGb6FX8RIGePgmqa/QCKY
t/dOuDQpkBMMFW2UCGioNTOehLN5Xae2N3CjsV6lGmO5ec+/GWXK5HCdPVoaJulMpHzbM5WRQWim
j2gbxlR7WJpc7icXdr6qYiTpLQQX+ceBGXLCWhIgsKU+wPlTYl4mfATuCF33FUXq32CiRcDI3weB
G2DjtM6XQmQ+fGzclF5IQuihjlFiG3/unCHmS1tHLW1scU9B7SgDPOEhM9cZD121pAw1Sd1MvleC
kanKCNDc1YnZEZLSgejjA3ZV2RH18Qs1hVYo1bDANva/fpHlCFF1mc6IIZVs08gywyE4BLTEe+mp
NAPFSOd3XoXuNCu8/sPQFjDUZPTeaT8MsHiRs9bITt2z2u8+iwOl95TUYBprsGtUXvNjjSCjf7Q0
FJFZCbZBoeWbQl6aAI+cX7OtVkDqHdPI3WM3vl0srmCkvYMAtEa3R/sEp9o28pkrGqsv6ThdhCrC
P4EBoR2vPce0Sx4V8HPQ+jtTGh7yPex0H+hKNhapj6pprPsMyxn33N0y7GEmlEqCJ4v/5kO0+Rel
zeucsXrN789hD5+0KPZMQCX4k8OOxwRL91tdTok/WHtAuildUGpvLrfdLfBTXwFxLVMKJpgOtH8S
hjZZKNlmnPXULIKJvNgz1Q/cxlmm3PYcEUAIlLpxC6Vstbwml1mo7m0Dxj7m5YnLOgkVcfzT6X9+
Y+TE/49IVQ4WVaAfE6P9vl/L1bfYcRks6+cx5RGFyAG52sLXmWcUIp6OKHME8M+/HwSk0KjUFfhk
DLY+U9kf43IzjBHO9eC71rupkB52VAUHlYaFjKqpupSScrsKDlc864IM6MJSXLTOxC/GzI9JSoYW
LCqZ2uvv3wReK+OaYQSKDCwwBj6ztbyWeU0rMZaopML+MU+d47zbRjCh6REpNnZhNos+/cJHlKQs
SFK3XkLk90oP5vvFoiRzyy0bUbcnvR5gUdYV6Icm2FrogBJBKURFLL98AKHX5kyaiwW+zPRrsAGl
rb4e9WGeDX5hFPesT/LgnKssekyNnpnX9Iuwsqd8zlTTbwU//D+Gn7q15CwmqY/uab0I0Yv200W+
NkDJxqXXGi8aVkHsF0co2ufrBaX9agk1VehUmR0QL7LzMw1E7PIth35SwMJqdvwpqH6qe80RRpd/
Yx2OZctdZJIjL4PKCB5BBFfZQrBOQBhe1NIR4fOEKnJU7+bVWlfymAk032jaWnTR5g0a8D4zFhSy
AZ5vof5f8Z09M/WPrbABHbNOA2Of/0lvjAoNerFr24ypRr/EQVmBe2d2qckk1Xdklva7m0LYXtuo
edeS7M8nhuBvAQ58qQf5taHpXQw+S6j97NjMxHQWel7K8Jx+4ZhQJxp4XMJB3uzrKpW0Gi38Hh8t
EIOOxy0H7TyhbPWpk4637msMyu1fERCx646/Ua0u4zwNO5XqbFf5wfKXinwAmFb/ynRshwZb6v6M
uN6naMEUOKFlJectn+8aYSZhKF8GVdKL2GGzVH5pFKjDN1HUHoDtmKeuiF5LW++E7nU04oGtyewZ
GXJLs5/86ay5hf++vuNOAouCu4uzdaSXYG3JhTH9Gdbhxb7BSeN4Ydk53pV6aQATJH0wSJUnW9CC
dRsjRKhgGmfzXq+VqRKG3fto2YiSCm/bssJtqizHCeeG+o+W+SgwwAUH/BtGUAlUFE2/uTJZdRtL
FRCv69Lp4Pyvs0M81C4JgYr4eV2wkyDJ+cJaSF2K+1CC75WPNSLUaRznNH1bRfP2EVxucTO3IS2o
ObR7zgN7Q52vbql0Hq0gDmQVU2/tKNo4Qg6M82MmiafX5RMctK0aUQfSyAPxv2J91dgeSJpgm0zF
EvD0PyvYOlrO9/kRpdvc+d2fwHXotwUeJGnNu8+b4PVIBMbZ2VX2dGAUEhwyh6y4sq6Htg9s6yNu
xB2WwVrQvIv+Nj54jklQz0Fxp1QpHeSclL+YzztuDHOQZtodp24DP9J+J+7Yexyv/OGxPkDxZ9+1
veQfzjMXGooPNCxCFmhwg+A24DtgtJfRZCDZGwmurK25SkoEoStC9LVq0oYYmsNyRgoRgVrEV91S
dhZHnI72Z76QU2XI1ENwOR64O5eYfReywb3K6vvWHeMpC7dm45orCFKAUeG4oVJmuNR2lCTr4oFl
SyPndUas6wpf2ktMk7uo1u+hsY/oa7JJQxLI5bTtaUYWAENTTlwAv9vSX/xHRQ36hxfWg26rXjaf
ykjMjz65adIKkYJ2FLj+OkEehYMI3r4tZI2bsiawECCc0jANfj/nEY+4LcyGhdkYythzPj8ZDXdL
tsf3gvZMCrN+7ehLyP6D7AYJdrPEwpGw3KWgcfiYeESN2Q8Vj9bei9g7ksu4UWj7e9oauR/ZYiBK
nQa64NVGsYwKisUPnkJVxz44Gv+UdbHPpkq1W0m9gx4MlCJv0H/Tj+xVrxQcLYkVkK020ecsnzD8
UuiP05szarTPruxaZCII7reKmQgYhF5A/GhtgNCdJryFdzLstZZgQUliA2OWve8lg2hTLXAsH4zI
Y0sAGiutvdgrNjsfjFJkb1dM9b0lnuSyG9paWHMla0EVRr0YrxPt1XD7/lSUdFIoKAOHxeUOH2/w
XSN6efnFiwlCLUlcjgmLz+ZxVRHIFChOALchsbReJb8HKy8Vz6tJS4oDES4RoH69CmTezghJIqrU
Rin2V6jsODtsUB1xF7jQs+LJvZXSyO6EDSAAzRCnG5H26CRZBayR3ifpbTpaZZZmN2xhsJYst4ni
Gx3SBVjgm3NM7mjKwEzVKX//VoYhfz2W7giRzmKDXM7YFlvkqluTuTzH0zvlXrdCLf6gRjSIXkTf
G/p6gJQ5vl1lLUNUggA1evdARW0fO36s9T5ACaITQ7FIuz7ZLXoiiveVVKyBbt2vgA71ci56Mt+E
fwl7zovD4P4nE5a7FUMTZxTP8GFIu8sck8w4FUrG/9IfN0/IqacW5JIz4r0qHvX4Q5f+0h3RyM7G
pDdHC6WBYeBGPMGBDJbjpHg8Y7YwSauWu9rS+BvIFUiXmGkFir+Zt9hHm9P4mkNSgb2ers4l9Jsc
LK+PtElsqm+j3sKIbmEb4OZWmpvgn20RTAoBm3fPAYiWYGsZTIGLO90x06olpjhtgfwUbyIMkBcs
1XIqVNv2Eq4Q8Q+hAKBDWWDjb53HY0K/5t7Q7zkGtcqesiPMqyW+QoFuL3IBoTbAJk8K4svvG+LT
KypNeJX+l01ZFGNgcPc86A8CHadNTv5wlbWxG3HJTphTlHVrGjT4GhEHgRv8F1RtzbjB5j8Ydn0F
Qkq9UKVn7px1IuqWZ6z0tSbCxIxdxKg91i3cjfyrlW5xujkktIS66zxO5FIxZJk6QzNwBHZbFN9F
K7LaM9aGCpcTwJh051NaqMWk+3OWQjQjp4NbgFokD2V6jQ5JAEo4sVFRUOWGtPQNqZShY6s9TAma
lumsVXnMDQwe9yFr5UrXsXbtYQWi8B9AeoJksSlrA0ubqTDouClbCtVVB58wXx2TQgnASyZ3QzEj
Jor9xO+lLE+A7gtHMlnm3AvT/r1Oe4/FFMmJpVoYGvkxxAuMlmF6bDsXe1abnEktcZO4dA454zu9
01AOvidVd1/gSh9zdKsFFs/b0tZ8iRNgSb+Mivf1j4C+/X+x57+nNaMHyEwmOHwGmqI1LuqGAL8y
yEDJv3T+EFOjFldAthh0VHSpeQ/UI8y3/dH8KLubltsmh5o9u51Ten8Hc51BLQwAH74KkV9OfpGN
e7iT4s+sZKTjGjq4y/PLWCh0ECgkcKYr0VpYzZu6dujnW+TFlGgfA+q3COwvSs3bRCqhxCoVCS7k
vkBRjGCB8aCRSHkcofd2dzFYk+DqEd+71AbHcTqwR+b1pfocKZomcyFC3Bv6IIC/M4ng5GwbtwlL
EO3kOryWHA89E351FnGUc4b865NR4au5wLz9eVhAYGxhcgLr/azETqS72n64cVxoIEo/KCmTgDeH
aHGpRAbVch86nN3oBCWJMY6lP3QlLqJYWxKTmKVfbn/5xDRMg+kWtgjuX6ezwaOgELZiqCtuBXT9
XrrZgVJ4JYPGj6UHTtR0YyjEIOKaG0p0lEdyDwaYxVb9UGN1E91xbjVn8Df5oHgacGqBbPHlEcJj
TRTKNadR8TU83hU4gvblm7nVxS6P6w7XGPGXYzARAgbZF/+EHHcr8B/Vv98O/CdBqEbEZqGzJWnm
ZAH+3V6EcSSB62vkPVB03yav0GtrhqWGaIHC/SEnlt/2jItwdmNtdm4kGbg9DSotpqWnMKhM0phh
m/oPEVQOyPP33AWpxe9QVCVwsCvKCV6xACbHXkE3VGkaEekvl5WxouHDJJb6GwGe32mgOl8oBXaT
H/OPnZdTTKMAC+QF5wC2eQk/zF9pjprBVB0uA7pdHaaugxI9SaKfWXPwClZZyAUUlv5phnC7jiBL
I3yzVety5DP+3vy/tsorzfxpufx/X7jstoStW7Q6AYtgmKWm2TGzxF6ByM/Fq1bV73aCP+A/9OC/
8wbAcXfm80uA+4BrOhxFA7R3fQS7YZ8A/uUoqBFW0B7VfPpPQVVfaz8oWnx2/EC4zPCC31ShGvTS
7h0s6eSBbY++UNzrRtZKW7lFuSkgFuYm7S0xoPDVL2qFWpn22/GRxb78RvneZI2mX+EbSv1WMqi3
S40a2h/4e3xczlzh142E9xjuWNqMvY9gkefH9H64ehqNQ5vo0RggZ4yn44Ox4QRPO1ibl4r/DnSW
k5tPs5pEWqrJ8owI9nI4uTWp4u/BpLu0uLBhR/b2IzDq90CE8VgSYTVScayIqIDTQcYmExx/XK3k
c641/fBPLv5hSjMm1XVzfqWxG41F4trbMp8LhaOnFBUMyPCSv1F+pxyKelrgJdZDRjWyoRQVmDEW
udsvuq2qTz4+f7OgGtfkJRLFL3bnCaxPhMsFOWXnEcWuB66X6+e2BvsT7R31vlnRAxw9XNrIje4l
ZmsDeHQHzd+2yAnRVkdjBGtGgGKmf7qB0l4VlQbPVqtxodBN8bsisk5Qd5wo4FRcnK17cj4t6PBf
oYkZFR4C2WIP8gGpx3AZ151UZY++is7vWsQXuEEW5uobVQNKtxDH0LagfE6b0o14EmIgzP8OQ0pJ
lFnf8iK27UHundJS3P61XbcDt1t/gJ6rQcSRCllwkYmiBEPYIpRHgPoQF4GRifvwCucSfSt7hoys
17WESHfwMi24Vg1XtqHqtshzpMZ6clA2cV58RdKi5gBnbNrxnsEtXYv5zs4cG6DyUmZVstmZoN6x
WtTpKFHnSFXLjD5dzkuGs/7kRNJEl/we9kbmd+agL35/xmq232ycnWmBa2grQHMQvyoSdd8R9gpD
PEnuHNrn0Tmjawk3grNQVItrmC9CF89VRt+BjPqpnZ7pG1m53gnT1nWpjIz3LR70chfEJR51hJsN
2Qoo97EW0GFKm4GHH8pTL4bxCNhcbqHsgKW8vJfRnD755N0sv5bO4tCElXe6du9oHCusgn+7laiq
cvh2b1o1SkhBw+IdvwV0sl+qjyqQ6/LFSJ2MWzL+iLL8cz8EFyYljARZztgz+OhhQ7Tbj82/+e5e
ZGD6XaDgPvIvP8DdeX4jYQ==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
