// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Fri Nov 19 20:43:43 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ wall_mem_sim_netlist.v
// Design      : wall_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "wall_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.2716 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "wall_mem.mem" *) 
  (* C_INIT_FILE_NAME = "wall_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18240)
`pragma protect data_block
xNgUBelQbITyoDB7DyWb4fpFe93t3FuraJ5j+XIEqx0CEdFvfZO76+Gwc0AVaLAMc77hHMxyRJ8e
4CVJ8gL7kMlGiCnf2jlqLlcSNPjEMZKDJA3h8UoKkbnst1odFcL0EJcH5+u/umVDMC1R2E2jQKNE
MnLn9Fa5yRz9AA3SRXA3HVSyvjgQGlQLbbO04oFOa5HfE7BYVLrbMiyCqd6aUVG90Ahxlah/wOWL
JPiy9wS3rwpO6hptuBMKBIY2a4kFjOmeKQ1ivolFXXc4D13+iSy0VpwPl/pK/5Vr5y3b3V7YoM00
CDlW9vkfECnMXxTjgxWSMiNDr1pCWXtxYytt8XdF0VRkQikM44Q0nQwsJiFDPu7yXmlr8t4XNPGJ
oUuA0aXvIPaFBq4SwrS+y6Z9D7x9UIaTbZnLgBw9E4BnhmM1vFGSwq5N0OC4flwhuUhK7MwCl9rZ
MjS3kSaPHIx8dVN+Yrl8Yr5ggqiEoEIibbLQTAdU1bNAWtrzyf8ScYZgVJW6+wZLwR5RAyfu7glF
0ex6aezD9czzrHebfI8WvtCnKVvzhA+7il8PP1gC7Gvh0lLLk19D+Rb5MCGReIp/olcv6o/bzZtK
h3h3bIzxUkdjYBqXC5oUNcnlIvh3TzIPN3zKRB/TAziMv2tHMmk1dgswEIUMMcxwmYEBj+qbrOJt
obX/Rua67ts1HS2o2H041oXl/8FWsR1ofvbeD2cgtdxkyMMIg2miGlclL0Lw7UQo3XcKmrFP8/jE
sI8T+PtNeH7/GE4pORQh67CYJX9J7t30vVdMYlgdmEFbYy9407yCoMk/h0QrxCeAqI8PNft8fC5T
BCRNWNwiN1rKZEXYjqbGrAT4EbKAI7Xfmn34yFhALNKcaWjFiZEWA9m/yOXO4Q4ju2EpzzCcI8/X
2fQg+8AQ8DZQ65N+1j60FvItpa4OANnk0cc/LDYz1Q6k6lhFSC8hyBbz7+hLtVOVWUDGaG5SX8Lx
Wqq4ktMBWn1Eg5dUhkL5b5MGNVuqPStBS5/z1s4YtkyGdv6g4xOIp4y2vOxsKlWbkCIeCmlgwzNe
MoYPDDQvavZElkxM15nm4MAjb9loddYqbttZotuavXWXQNA00EjCSXe85+C/zxpgVZ3l+oq6wqkX
FLM9k/arqVtaA3BiqJpfmhn0gL1kU/tC0z8bTSB/mUgGqJX5NM71RJLo+jOBHV/mGHny96zvvZVx
Huc4Z/BQo7Qra0ojtx+ceYj6WWDdxQ8Tssg4HiYoQn2ihHNP8A+hszrAcHnv/tVJtPrtPuReUO+x
6L/Ws2TJ+iOpC1SmVHeygPmrEle32Kc24r1gos2jJ+Iv9KnRQ4T4BAZFfVHvH4hH22Myl9nYVfWD
tcmMSwRCUzRksy5Ff2Z/hsTCxFgd/mdRb5QxJErs9Xi3o05tkjM/7RtfIFmGNhHZG3Vpn34qNqW0
QIXy4wfFp9v16h0Ygwn+puUJ0VyHpsdHnyCszOlwwDTa7g420k/W1PIAd8wT8wiwg08A8408RQKr
a1wq9VgfsuAg2vT0gCyAd4WdZjWyT0SvPTCfGBHOxK6+JDOCHgir6EhlKq7hZolcYkC6p9jvNA2u
Ccql7n+Fou8fEBXTHl93rCEifUoykugQnAZ93qH2htR01v6K4km4P3Qw3OvIJMbaIECcPpUWkME+
0v0peiIA35voL5YJhe2mUSx3KHo0Fu/oRMWUxEViGU1zXDRdlZ4txJTToNmo3M7Ws+2rgLrc/v+x
MEfD7mi3dCGh+jbiK1kkOevhCE8bpEPctfSD+2V/V48e8S7Qy2mZEgF+N3BrBDr1xiN2jp5p5Tht
3w2/vMu/7QZQ/cF1/lYKGyT1TQAvk64H2m5aPnskMrCQYmtpLMHCHq6Lx2sMJmrHervCwD38b9Rp
1vtemUxe3xQorZZni830+Vaek20zWcCYcI4NvXzGAhI53AdwZhYAN3ZmxMC/gibAtFb0tNjyVWyi
qU+A9D50aUQgt0K4t45b2FwqwMp2EgdHQpBiZJ8lfTpf791u5c/Sr6u6e9B7LsgujOsWTbnAEtId
6CaFmaifNNNXXk2z589Wem/UMH0b+0SCnDPT1xonKkiU+d9t3kCXIu8Lw735Bxxn7guCKu9iBwl0
UyPzTBS3gbAvb7sZTeVCwHuK+rGWi73A8+cSM473xY6T8I5LnCKmwFXxO40LxzMegiK0+EueKNyG
3Pfc8YEHTk+rzflPsqxl1FUD2KAwiElBmpkh3vq2H5xiCeITt35CIyjVHFKGzgogZxn4cUb2BaFa
UFBNc5vEnjaK/YCG6MCThAGlgzuW0imxdZVOMXUJnvu3uLCpvTzsqIULU6/J++Eg28ttb1A/zSUc
VqqHJeiihS6ZYOUvGi8bD9DOrMnDLuQ2xSadjFJUi5dMG4/R7cm/0zzoGHgLgw7aNHdTERTIwUD0
2+eGk3tYlc2uGT/4AJHVEnSPoFBRJO1mLXIVZObXtCPT88ojCK7PDZdSovoEQsYIp23dUmlZZ2ot
oDnU5mhsP7eFr4uVKdWIangntztYoff+lCgJQ9SGyrn1LVO2GRJMozjT5iM/sB8bSyUsqIHdQV2T
CkCzACKzmc5u10PpEIwNdqjHf0QUot5223xDGyhFk7mUs7FbJB9R/8zIj1FV+fQeajL6GTYz4S9J
chUyCd0qGbJmcEtwwpJa1ZaqCM0yxE5IUfV3otw1pL3u1K26IsfmUJonuCkICmAaJjKlxJ4akVJJ
RFxxT2IGBAfABlDq330lIdsPwlb7Z3so0lb8KXHd+tR7fDhkXrwnq22qRNE4kdJsDN6NXqTW7ujT
CbFjnT7tX+ugw/fqJAwZSNA0QfHpjgzkWularX4mPQJc1aXENbDWUtQPFU1SnR+mUSTpfl+UPNpP
JK7PnncFGitY7AF6HPFpxeSVbZ8F+4RQXIbmpryRQwmslnyBFkxONmQbSEwRXuAGCnYc2eo4fPgt
getyxHWT/T2N4FOMQZ8TbbrSTlCvOIjKp/eTveBD8QUqOGFKpt2l2A8ACtwhruMa39EHi4RMhdlh
0rRJw6ff+uJ/RhzVb4s5XmvG87k+af2FzXmYwT4GM1ZjtSs7sqFdLR00pp25+DAknvLh5o3ZCqxm
nOt7uBBvc5UR+0UvHl+e8dhR3HbaxnioFgHdKeC9m7vLn6WcECfOCOickBIk9PVWwyCHLm5fu8Cc
rUmQ9Ptc7HWZDUU4vjG4o6eVfXn4rZuI5426Aa1XYbadvjjVZKS0HM4fm2BsLuuzKyESOlsueguT
XYiYZMy8CJd1oEkm099KyWpe8FA5BG9yIm5UAMbmB0Xc0AC8G8N9PQkYI5Jo/E86GiE4PekocU5E
WDLvNkOdydASRexbKPD/Zw7VioX12A2/QLhn7ubwjjRrW5RU9VRsziG9PhGEs8sJ2HRLYglTo8Ca
HcDX81b681LCsRYNOXpPINrZX/rBlPcSTCyDOqdCBDpbqWZwuEIKGe+DvvgephnIsqRMCCNyIwgU
eglXgtmiml7UoaE5BwFB8/hfvXzpSPBRFT8lN6qcOEPl6jzH0Yb0pIzBP1syF9P9TPDBwYVCJYOe
1twezHiFe8Kf33fWT3OI+R5sgNd4001sRs4BcWBxGjNPc4WSp2Cqqr8s4vq8E5AGxDrmREfX++xH
7OaDYLL7cFV4WcilKCMXfcW9QJCwporMEdhnnMvGHtbxzuIYUH/TDA8IttV9Pqp9Cskdry5AdWgk
AStMYEoaVQo+V8N5JJV0IYNjGU8GNOui5+SfB6iO41RIr3Q3EE52P289zpDZkDClmCs1kADc2tfd
n1qw73+2zmpRQI0ZW9E6CPVibodmVCGNklxB9YoyosXLeQRrQiIHvlP+WYpDYklkmOMv3yblw0oR
bKZ7tFtIoIEYB7g4tHgVsoFnAWKcC9gsUd8560WunSSwl3bOwKU5MDb4GLU6V65kZnMJyOrLnVtG
P3IbnESqLjC2UFdB9P4+CLnbPn2UkXZph+P9XnyJBH9+ACygrXqTcytOLicgLOEm+++98ieQHOJV
g56knnrVIA8GHI/Q08QBaOpvJQStXuMc3C/OmD+o+5MJn6Z1PLKM3kj8xJpgg+4rrXuw4rvbhEhf
N4hoJnUL6qYkBML4ciYSL7QdGon8sW8dVh4/o7vIG2FB5CFUcomlZpTax74swp+0csZVjeW3JghU
GhqgOihE/vf09Uau+rrvQYw5+G1mHzUFUrovtuKSOvSGlk4dt3SympjZWXM57p6Sl5Tsa7umRfO9
CRVRUH3+H1bNJ58/8A/WLAb4n0bKbzQGqg3f+oUBbRfoxkxk08z1IWiczW3jaW1jjcotc0sAfEC9
TA5dgdkyBqi9xsSWkhQPFN+BWrBM1uDgOAkD80K4WlIaIOxO0w/oT3PE3fWhW+YqGyt1n86njvfA
N/VLdQqg9PV0zHJur9jhcgJNwj9ux93WbpC+KeDe5P2J2PSb05aCExfudSGgTbWe4i/MBVG8+g9U
xADr7tqd0QjyvJcPlZGNCVTZs0k7B1U7EdJcJ00I5nGU91rlKEBfjXyoOMGU3TZLC4OaVOqCyjOA
iEEVpw58viX4VIZeimX+Btt0fGhRQgMziALgFIWQfbNLBaVG9jV0kGYNVOLVxXEIbVlcvDYGNuWr
K8qEOq88G43j6nvlZKWZTYF4iBPTnk+NNQknGxmnnzpdi5lsaLRLhgelnzhsM9/8EnMk/Ny+ncat
Gch23xu58hLMfrNaeFrjX8qDfyS5r2p7X/h/1Qzmrc8SPDwdTbR+OMbN3/zb0MKRTxooBNT0L39R
A+3gi4VygDzG+qOjzDbJoi8ECjowe0864LqAb7jxY7MNam5KNOacjP9/D+ESAnT2ZVyJ31ubLWcm
M+gfA52SwIXPnnc2jDYWDf1HzU2YniIbdxe5Ht1jZVWQbljxxRIsCKMfmtTnlkwRDUpy4q6Qim6S
OIBQ4z7jPh8sE161e5Gy1R/RfI8j3Wur+78MSzIur7xi81N+o0uACaNxJ97XeeIPidaCZ5xT0QrT
auU0eMlvHgM+6DhFR2a3NSGE8JvHPUL2gBzN5xlaGdlt57ufJFqzqtC4Dq4yxE7qct855kleQUKQ
ATKdMPwiWY3xLsVbUBefbqbYFx7U5H2QOh644Jo561oQmyGq4AYnuytSyVHm4rDzwVxfP+Ql50Ua
PZEaM+utJoLEXfWs5iopnQ1OoRRQ+orrN/+BaPt0U3wH3ixtUoRn83JiG3uVJIoXNp56p/05jRsM
Erd8ZmimrerH/XcgUuR/1zcReMY0G6+HIeCQ0ah6zP0iQM9jz6HMN+vwtX/OQeWZCD1V9mDHCxZ7
Gyu09ftqoCA517m/1zS9DVNs4MgItSXAiEWm1+d6H5VzKZaxr9Su3Sn519zKvvq1ME7T4qAFLwmn
zBgCvTL/Zl2LliM57NAzXZ6UbfZPYz5ZYADi56uttIdpFumiQ1ABUQNvXh18D1ocrIqfosUsisWo
nD+w5ZTDGaDczFm5ZsGIH2dvLXMVgl5zEDlKevDW9culf3hGCkcoClzuYo5mWF+C3Rlm7HrsmjlJ
3QTB6zufuvV4whXjkN6ZrToJSr8tGGaxWToKRsKN6jG0XIhi3l1ua11mrqamilcuwPKRh6wpkt4b
T9fM6sOlmPasvvHUl0k688YZ4x9y3vrmPrUEk/yPhkGuUdMZFQB/dv5XrHuIre/JlgxwRz+KZZVg
bLBFJ4m3m2704XDdJW3oOVzAdKPDmkalbKAgjxNisYyqIhPbSR83XkqfopcBQyETQ19A7QXLwb5v
a1Ur3doIYxQyAU8lstlvuZ+t5YOLEGqdJEh3+u+nbFCr9zZZTKdxhoocCgra2Of2wVcUbxPjF89W
LJO8+GcwSdC+9acuG5DgqiuUkuEXj94NInagKoVrznCH1SVSA+lynvvJ9pFQ1oPDfSD63CWR1qNI
XEwP6XaFUVAA6yuurj8jetKJvyf+ssGJIcSzTlhZxey72J7mjQefG7EyVzrDbXCybVdfTmQCxzcZ
gmeVv+AKJwfHdtpxW2SXw7R4beRABgGaJK/FgRtR8rNKLbticgpMKbdYVSgzv/j25Deolaq91a04
KANfYRdzmXcW3JGqgcZWF+IT4xbrpHvk7B/DgRsBgYP003DI5twbWijBOIrpKXVmgaKTP46+soA5
iE3hafSRjs8kCQhkAOAYVhAmFebXQQlbTFZWavbeiClbm7c//Lz0/mDZqv5ySZWhnY2ViHCw7rb0
c7lkdMV78xai85LJhoKFpjwqNzGLvnIgnd54/4TBTjto0Tf2yzNajOLYEhSVV7GdyupFju1bhgFQ
vpvzIaxRf4F541MEM06SGzyWO+rZwQMugzoHNRsbrL9YVqKgsvcjY3zyHzb30NwIDhJmEIP5KhSp
OwJysiU2MTUdwQZ9Ww2mfFmBQD8cVTRIec9MrT1J/uhDV1l/2iv34LxC20lwBRMWcW/LUNhdEGve
DlfYKAvAbUgjI6DfnBfTha5B5wJgbaKTQoORq/dIblI8KyGPhv0LtL19lkcgxD3iYJEYmmhPOcHY
UgbQT6kiek+tOTUsq2CFt8Vc0u4UApoIiHnPchkJ69omH8MscVtXlY7vlhw04KK4G6EQPb52rM9o
DEbvVKUdzJ7bkNXW9RYsR1476NB3lUr6BWEc4+M27PL/VkBzul4AencwCCequH/b4m1Qi7kw8Jgj
Ztetw9HPFVG4jN6joZmALTZTJCklGhQoZlX/75TvShlA+lH0X/tsP52r7n2+LeCwCEhQY6NB8ZfZ
xUXBQWrAGg9xjeUABBANbz6AAvNmjS4eckOy2U0MJCsm8GAy5udMc6M4I3aCvtjV0wjX1xsCWjWH
SZGIlmlSaQqaiH0B0I4Tlrq4n1CuAoZFcBg+8UOvsm+aoh6v27yygTlQ8ZhBeb8TjnYK0oM4F3ll
jmqiTYtHG+B2KQflB9SKldZQSnpPnBlGH1sl0WoRpIGWLKH6jvplMzisH6oa7lYr04lUCR+cEwYN
GRFWVB+sJdquoxrIFQn49DwK2CcITh59DxAV4vKl8FEe9ANgrcizslx9uehVHg7sMTgDn8Ns31tC
VPR06s3gC9DLnVpKTjqGPxVtXfyzKS+wuwDDh+lPwI0H6iEpNL6jxzQiKrCM0dyPCRoY8oqfY1dw
bkf6x1Yl029oaAfMKztSNPda0zixoK3dM7t0KgH/yt81l578n20H2gDEwKINJdqXtsYNGJUHOgu6
iJ/5+6/xcDzSyf3pJT1j71Ld5d52EoeeV9q6XfNwwg+ABaYi2Yj8+b87tILzfD6nI08YwTVc50rY
xlkZTWuZmRQ88VpuYPpXR9yj1fixJYmwSiyqpIA5hqyK8rAzmpFNgwHHw5yHVOpokSo9WZMS2ivV
kg3E5UM0BuNgIoRSrByFjOal5ADfh1DJophOzaHcxdMowOsBK94vrZ2jHPS9GkPodI87IhaOI6TV
lqhgZ6bU3ThYMELnWIh2orZ+25z9oQusM+pr/X1Uo2vD+b971/KgpbQEzL5RZfC2d7Nel5zAGyeZ
f+zf8NYTJZCV4qRQZIqDMRvidgXeiZf6wLd0meHya2ZtW0aMzKR5bjZVdGyIAplRY8k4Q2bBCtjk
FaUCBfJftSnzbRBQzMxptW+adU1vJHOitcPNIULOhRulzp77P5hk6TXwhgJUYxVnVvHnFqNI74B0
U5b75VVsXDCLcyQDnqKv0NsemF0RndiQ/PwVr6s6njsrpuJhnFGccjRmOGMhXK6tGTVb5RlmclFJ
oSYq5qPDlfGFkLIyWAvBbQdlMb0bCAY3ZB8I3w1O61/i/9O8jqPX2ja0735Q4fkZDddpSV8cFnqz
SyjCfaJ40KItCFmMoD1yalSBtsgEgYCJbmLZ557p9hYqTynvioX9ycfR3OUrZPt7M2dUyYmPkbmP
Dl7WMsZ3ZeRbdeez5zxl5CwUNL8BTRCmpa5IAy7QYK+1NjVy3pagGO8riPaVAu2sBAZEfVqi0Z6f
sKUZqNqpicmHdJitrFEFM48XpX0l3ZXeHsAXW0rinMgQQMSaw2Ee9NF1Jzpevg2fxoxwOkDu02gy
dAWtizbd46OT9Lpdey+vDJgIjFvRhUFEQk3xH4kzm4B5e7MpzbZ+QNuL4lzq8yXw9phyvgad7K/F
jnPsi0TFNCqhzRNl5XxdYSPhpePjTZeuqZWXxHRW/HdGPxVQFimYOy9n4Nc1jhZKfIdZrcu+E060
lLU3coY2WLkQf7+CBdrk5OFcNudDNLDX1LviukTWmukxYcGU2mmY1MtXL23h+T5wuj20QsnbIw5E
r2mImyH8oCl04X/e/UWLvr09vO67KuAewHXqf3Z4hchnt0SVWZtjHYF2VwnCJu2TQ/mLg9wSDtrz
6ARU21lcSQQ4AE9k3qzQfJI0+qoYVVpSrC9z+Mnfk+5rPRlUuWNIh0JwTP8AvLvlaTieuwwDxwNH
fOrbjJvCaoWDJ7xbd2K0FHEESwmAu89NvaU9FomVqFGU8OuW/MKWsVxGQz48LWnViGzkAZK68xqT
biM4wVZRG8WCH+RkGjsMWxKalCW/cyQRb//IyJMl5ppdptndXQ18vGKmGE57f/NrHwL5m6mAdNbp
+uMg+4trIxnqVibGHfwlOKmxZ1rZ8vY/vOQn1lhnFFRLIHjMfx8DRUeCcswNTSeOXgAOiXt6GI5z
ongUoeBkDQuQ5KkbsNLccCGjPyJ6HWRsjCvK5eeerNkRbb/Ij+BuVHkuNjLwiudORUvvfLCSNg3l
3GfYE8p6g3+k+p+GUFNLPMD2d3rNYbr06frYNB/Qi4DxVUO1a/+zUOSUwxXQmXpOlnBB7H212fiw
pG2w+oZS5h90DLKuOfhc7lb2xUrLipWG/kh1vUzkMjRtac0Rgis+2n7+YRadBKfkm3dnKSK+bRTU
vy8iTfBsu65AM4jE9UzD2iqokgOHjuMRSQnK8QAWZulrpywP6O7m+hucnPIMs5kkJq65BUEowb1Z
PnaIYV5kq4AFM5CQlLzK2Tdn8jXOEyuxntLsPXKuYiazvymDypzG8GMEmyeIN0Ey03BY7CjI4nAO
1f6X94ydaISHovpL8xbqNtB4b2ImK5Jt1SW4eeX7DP7sDVMI6qYR6I8oiMCC2GtvXWGR1VCR4bzj
KCmLztTMPNncUsc7v5bi9NDLRlpy0SwXty4FB/yvKz3YgRtZThW1ALnGsiefCpIzCNlrxj2XA4aQ
BOIrqg5FGRl3chdreJiTqfOcwCHh85SmQ1k6style8kuDx2JINZmGaD7AU0BxiXlFLI1l/Wc5hjd
eT62yfhbmsz8+atQ8t2Lg/3dbRqr0KfRaC6yzoqm9kkTmGg3kdrB0E6U/CGPmXJtmLAkR/JV3LFd
1oPJqGe8mnxPRmNMEcjw98xboNZFWEtf8EMO+/PoRhdl6lmbw6ubt1YgwUSng4/IC/Plhq0gqiSU
eTBxHapm8Yl4D5CQMGxEtsj6k3UMzLKN4Y8bouoFlUUC6k282F0fWF6RYCKmRgNA+WqKuNt/M1zZ
PuRa+38dXyGlh7UARtaskmOML2UHlvaimgMtjKXXZ3LLfBVZ1+zSqbtRuA2ybp7oYFIHPCbMcDzq
JFZXkSakAzWWm/X/k5AdKYND4UFVoMcThGCgLE/iOCr+1xHCh1n5J94Uy0x+cDzcwsyaGQHvi2eE
Y/MsxowGdGACaRphwzNuSPlARtubnhc+rysYbcQTwtFSMbKPTTQI7rjRMIQZEpWNSyrrTqt28hM1
B6C8ECXCWCHppQXZf7QgBkUXvQWv16mm81fdSVNNeI2BLLcb7Spy5oBby+nI0gst+Y77xASiAU9D
R6Ry7/kClLR66n6wklHuMq2jzz2APYWqg7ezTc8zKz4GvKCGD6JEfe2EWOt5uZZH+C7IxUBaloZC
7mmF4YCJA1l4R9xdZIxi3/0jKS34SCi9nnvjmtA87Cb/3jil2iLKp7rYdtoRvxd/f2Ei/G6P6hje
eK00nWFi3DkTDNizyBzR3TMLA/SMbL4BD0QupMqZZu2wcw3Y+PQiFRHpYM2jmKXncXGBSBPSHMGp
nHI1+FeWhWuJz2i5EKqmY9RIJyH3pvahOrcYWhAT2HTs4yP2oAzqCy+xW80PgVdA7rSfYjmbvpv6
+fshRT3gS9hO4L88Guh/COV3Hhode1Fi2RKWJtNEUc7FtxGzYwN/DKWc32fFvEmC4c6atKn/RjGS
D+kAA9iqg90MCOFievq83QmJx5/h7gDM9u8kFq31/E/mn5cOkJdrmLPNjXYdpJN2cyobYw+pa2+T
c0rAhqHHipI4Nz+copsDRo74wA029cf+7aijtGOApsdls+NSKASd49U7jaQu2iTEUujRQJtgqPRS
Md9O8KOTEt37+SpLq1G2qQofCfkHQbLbjm+QIhAEz+xi1nGePpVhgd/2gZDKYcfGIhqfJnDlL6hP
4/v1PM15xSQwFgiWMO4Sk0LKusp6OpPsBirqZ4+NDgsT1+u9rHTMxwPaHnfUS40OHPQJaBwK8bsN
Cm7DTYvNjuxADob0kmkhcqlKUtaVVOl5bFsdRC8ExBU7HsMyxLEtkd5/GJKGVAwzUPoEBi6JtaLN
hax8T7EQ7S02PtaleuJhwCSPBY9gnDWlH3NMFh2V36X+QZTGfLx4b7p3UyU0S7W88hlf1+HZajGs
sZNh7anwiGx7IuOsf9kUu+k0W0f9oLxBNWuTDJbDvhzQBS+93auvTQwna2IsCjqwv4bAKK8HSGz9
eyt9qH+FqWX5N8BevnepTr4DcEXezr63ogdi8vy5kStE7M7yNAyNA3h0BkxNrRmG998LoHn8HQXT
WqqhF+9RsJtVJMIGf9FNAKJD3c2RiKhRpaEaBAE6SKp2SrzOSn1GVS8jrICT7ExksS0YEh6r1fYA
mu3k3CIesAnrm8zfeTVCSogev6cumYDZrwyxuTuhPDEuoPmKkn2jF1WJNPleKCupfz7FBKFH2uy3
+mUWXQcQzmDroxP+Yd04xCfw7QRi8Hs33TZUmEUw7AdbIY7xLkNonRfqZzMaBeL8nBb7ypqpMF7c
h4z4cLGTeDIB0GzZfV+O+AK7GV3QsuRhYVSVqRRgT5q0CGxEAGazNQn9atP0OcIPK1MnReBAV2qq
sL/8Xn+mcE5kceo1bUnutNCXilzeFf3wnrWef391YSVakLgVGpo4Tw4+7exMAAGDSp3Ca2MsNMOt
qbIkJhDr9AF1y/NJNThFo8+p4S4UycFpz8J46kbXCNC/EF5Ne1zdw3f33X+u7bNqMAba/ni5YtBU
N6P5HLYZQh4G/abSX0yBuqLLK3zx4RvnrVtJyD5XETfVCsGDc68qpuAySHetHuroFifce0wx9sz3
ju3eEVH9fqaBOR0CX66PnsQbrfT/6cnoGjU4/i/aZ0y6gLleZO4IugBSQ4Fxa1XjqCFmYuSpZ5V7
y+3DAk6v13a6k2lnz3Sol5X7qTcx53bIOcT4aY7XPncFzlQMCU0+7FH2K1pcYy1vrWNS16IS6eGu
2Qwyj2BCVwBQhumTuPOGkVV+QGRUgpUUMsufqzTegaQpLmP1Eoi1ymCBfayYg+D6eDqCcpX5hvNB
IEq2MKoikqRJLugcGnv2nJ2sdob6eCv+GTGazaCtTS71WVr4KRD27Y5UbkNHq8pV/v7VnREUGu0r
k7oxh+87b13OBlZsDKw2AM7WQUqiPNEZKxZ+bsKYN7c//RDPQs+wxPtzCFn2fJvYdM3TBJKc8Hxt
/l40VySBRJkY1FKckfxqhnJiozGevZva66Srn12zPOnTVAK1qR1CuRycfKh02uBWm84zCJO0dj2N
Qx/BlJQTNDxLN40NeODB0VVrt7RwPQdgKH16tRh0Dkxwr6s7v3D61SF+mQJdAvExjp/T8lircSFg
Bqf9XurihVcilUKW5wHgxjhWWlPQWK1tAvNIHHMoe66pHO+4IgGpLDYbfV3XHvHQ7htGGQM69RpL
SHhBzv/6RoTVfnWJsd7JkJUQ6s/VLnrP7Wa68p1ciyj2vDX0GKAo/J52QdtRTVKR1bsrhON0avqW
7z10abGJZLos1Z+lQVUl/gJqQjuweq5b555sGtMbJSP7XhuJGzDrnuqWez4j/8cankf23KrH+KW+
WVY+fDzs+mEQKiyUpmApkSn7mnIElGaLf+QItuMYDeTV0AVDbN1N6aug1aeURqc/9+Wu5lz3KoCP
8c8jJ0MG3AkgC3HdjistPs+WyRwnB51YCj69E4eTsWspWFeLd6aOcYNtm1A0JocfsxHkZ3JwoXgv
966XPTk7TN8Qoale54kciD7EeIXgW8SV4ezbfEGiC4gbvA95srtHLilGWSu+KGy/UcUSS5eZiFiV
oS+RNfb7m62VKi+RDxuV40phXucUvLDMLfaftaEeE37DJcXQGvaVkCJv2tkefyLr4UCkkp/LUoPq
N5ZsbcBmv00lHK8rkRFOY1mjxkavir6BZ87CzLNP8eQWdM2cRYiwBWHrLM9N5mvFtCHU6/kVaB+7
Lh1ohYj46TZnY5TckKqbrFvDi74pOFu9KNQ1kquiv98iKQBvjrt0HwQTk2jlHxwcWWYr6cOhVl0w
76i4DxRNMprKhC4sa/R3GZXems60xf/LfA6GyYsosFqjsmRcoPgbSPEjt++yZAsqBH6OQtJH5ggF
mQDWfJ5AXbo76d4VyZL/K0R0ZtntEw7GzHXuoQm2X2vY8FLSOAUfULGMgWNMk1gEpl2jS1xmG1V7
bYjntt8TsRwDhuuXSnhB9pjgSHCptItDbjumQre1o4hPLtB8ze36bjCWxqYxjNbvodl0ZXbG14Na
B+SN/AkkJd+PwGp7aOjaVPbPJCp77aBE+uu+e8675J6GC6TBq7j2PfVI09UqUvE359Evfv6QElSg
ou6le4j+lgC5GhGN+WqlzOKFdEfvUOL7++ZMejUDAU2wzYkWwb9l9R6sFbobgOufsBmTL9PR/Sbz
zOWV8ME41e/8BRB8RVuWmkXp4/XhZaA598309LrDLO4KFeSrJnSg2KR9TH6E01yt4RMeCrTWUtx3
tZ+635oaonWfY2SuXMrlsx0MyZOQGUAOMNnQMRASp0kM/t7cCjA8oA/goIpxnPUhX55Y2OAwyw2g
/33W1XjlgKgtaaI+V2f5tWRX39rcqYfe53FNz3dYrJVqfA0fb726k7+ZcAcP593gQAMybyeDspVZ
LHJgLK3IpAFZKzs5EiHJ2cBGiygycmVT1VNou3+sHpuP7HJb4cPjqbdGCaAkquC8uhaERmkIPP5G
YeA7bgNjU10l5AlUopH98o7Uww3bQ56CUSMpt5W3/sAVMkcqC5o84ZTZZSbBVh2fFtE+xfMTXFdo
kv2r30/XBx5B6q2jQnFjyfIxm4CMfYG1LpPgX3wO6Uf5iiEN4mnX0+mx3az8UCzhPmts4X9z4L7b
eMwOaSu3P/yrXLW4IXAl2yKr/lDHxjDp20YL/KpInh1Hgy3gEj9h7IVbMDsyT/UIsaQJpePnvBYx
TTygSGPpYAYNQZIQgP8FZC2zI3xNbi0QcEFRySQJ8aWSxkDGuiPKsVAtghnN7Wxgd1SMRrB2oNgh
XRzorWq7/odN0FcsQ8i3aqPtHIQ9/nmutzxNGirp0aGXIJHrNO2supIUtSiCBPyckEDiF/j8diFt
uItQeRIHffdv4da3e1FcT2uofFemOaHPHAUWySv6vYjXLRS7+VBU0s/chU0Wnd7atSBEe2y8NLSf
TLOdhzSYPHR1FrRl0lfQD/YHC//rdbj5suQSDigc85XRU5nt90sAhhKJ/0w7WoPSDvS0XHnh05Aa
rGxf2CreDXWiqP4nMqDFBQvrDBb4S75dDYmSVJdGjMzmLCY3kaIkJFC0dgbqZm6IwlU7dHxeHFIJ
/UnPdvkQHoCql7MziEqOqctI+OeLhn0+NkPODb49rfSU7H1abEU0JT+sgShUH85yneLXlNXBU9U2
siOljzahMeH3xy4pWXlUnmgx/h/poj6d0UtMKqWr/Oi28aKA+T6VbCx8tOQBNeVlDiJAAch8TNOf
34AEHTbY0nczTQMFjBkho8UNl+QfqGjHbnSqC87gL4CmBf8QcnkW+G7t9DAu8S6lRQs9AVbxWcjB
wmGVnFTGxckAyhW5/9/lmzjImelv5mcCBGmxyfY/5tWZVIk6s+extmAdxVR9+H3fButddglQ4Ds/
SiodNcuxLqTv6rpfzT5mWBA/fe03vcxHMqzEk6aouzViBUICFx+aa8vX3osFSSCstDxZ7rZRTPm9
U/DyTUb5PoCbEmb1Phcp4+pzSayOkwc0sMP/nOvU2zLcTvJqkDyiTtqpFyrlKYnv53vaTOgrGYSS
FLRi0QCPvyypt89KFID3OsMQ2VXul4K2Q451D6aUwbItmHeZV5qN83RuPb4zrA8wEmcxOl2SbSFe
1Bkzbg4AdPxFCnqBl3EzLN9fWcFVuq2NZ/WYBfFSrc4fia0e6qID5yx89wsLXjYnP1xpPMzpsVgA
q80tzs+5lrZWaFzg14ZwEIJK5BEc0B0JwhdXholow/Ji8k/rDhUygR4Cq5MmmOS8q7HQ6ePedHgU
Vys3u2Rx491cqAicm/szn26QmpbYi2+5xfZXo0pVl57j7jpH/UdeFsddCKcTalWC1V8vdEatM54s
FIkYXDMeMKfe9KDJa3TPVZKvTdUXWQkqdtEW+aBIEihoo10o+kbvLslrKFtrKR3dnAY9oYBXCQTs
qpeYI6BR0//1N9+cXXhkSkCluakwN18F0NrkFq7fVo6fCjAd8uL76xw9iXMhxtmGzCR09CMCrq8d
dXwbfNgpATU02SY+cbnm4osCn2pIqstLzfLroNCjoSfhGFBB7ExzNntkvubFCxWtzRHXkcQiU7NR
OtqZG28ZEQ6UvTA21ZF6oyOxv81tkt8fxOFx8239MYp8pfrQkKrnEhLra6p88m9vvISL/1KmgEAc
gpy4fONCgvNAjVgmPi2oYeIgpjCF9pXyTrWvqpKI5R0vzTw5TV+4v7g0wUWlRJUi7oE2awxSN+qn
tNQsbSY47NQNajblSg3EMIne4928mPGFt49pcjNoJm+BxFH5VmMPNXArHBKdg3VY5buGs1LVGSyZ
iVZsq4L6roIt4loRN9GQ/CLzcqDRbsP3CYsr4nImTN6YWoC627yQ3Ds74VJuLBWpBuZ7lCKTXmkq
jLf1wGl+AQkq1yN638g6bouA1/jS9ymw3+FGwID0+TZ+9k3w+TFwChj/mEeNbAoO84wSPXtPJFwd
BPmWn8UHZazhtkS0VboHgO3YNGT2TNlmg3nIqGxMo7NHOmhp/9lpQ/LvFNz8iIA7ORzEy8olOEHx
jOfky8vT9vClsdEkDBhidJpReiopvA7nB823WBaBY7VQUdWcjaT4Ypm2wscS9VIPlcdLMskCY5pr
w0ekeXee9PKN6HL7+iUwYN/U+2ktWCh9Ie1k+pKl1AbJl19+ZwOvvCXJy/nj6o4hzaUqB3QsNylq
twQhPXcsOSaEHl1zdDAvfnojuLaDPfqSIgPd+X0rak4yVvM+Ll23zIV28Ty4DH+E2U+vABee/fgv
TFRxSDB26sNS3VQy29OQtsR+FwWZIwp1P1oWiEReo7gUcJBfhR0/jLNc8cJVKO1xJkfLVoAooU5D
YLoK27aAgl6DgGm3mehTN/fiEY7otE+tmYLqy3bquJE+O5XU9XFeF3DvQrTStAt2et84cp1b1JQT
l4CPkcGqkPp6mfAvcgTMm1LpQf/I62gUbxX/8T9iRURoo1yGbcNTEkLloHrvxnGRRclujBY1U02L
htc/v5QrCXqzadzg4zhz4maHXru41wpoAKQ6UtI6uGZePldoVC3bls20ZbDrqAYibgz/o6zyiWNA
I8XyPuk5HFbbRtEHPhzPKjCoDLiIxWxe1pa1g6lrEAYH70NIJ7oB5REwYieIKRYm2U3sVQwIsrfD
YigHo3D3RpN1GTz3hNjMhqcydSExHiZWsuRg1NtTSa8CKldl0hWPLeaGksrxKmdHm5nhFIW8NPKG
QMOPZ8e4ApuvsFFOBPW54zbva0gttBHAoPyHoOSHepmCqTh3WPGylBGJahHuZwLhLNhJMVKn+rjl
pX2AKMV8OG8f6MV8DQz3UHoZAvYael+MYlwWtnZjXsvtC7D5YBVsLOsL2NY5V2W1uE8sxDE7aM9v
TNGRsMmZleRBpT1k0dWRhdzU85qRrVx8wrbMM/Gt/UqITaXlHBmWe8QXBIIZeKeKkGanc+2KbVll
xSgQJTNEW7VDNfH53mGQuCXVNNEbZai68ATxnGiuaPLPiZ+owxiVrkhx8PJsaFOueqdqViRB0cOI
3JfFsnnRLctXZlbm/Jnxyg+ydOvT5JgOMXWbJ8/vJIIixVlJZ171CVWh5gcVhz2Rz2t/E1Aq4Bik
3cWhzoj+rLXi5csZbDa913gYoOXpRV/e2aBx0nYuCguMMInJcDzC+AycF34rs4rZM46+e0Dwf0VH
d5S0jXYNCYjtjq29SpdcJLQg9M0FuEaZZt6jXlH41Fd6JgprHca8MmZEQdZEs558bk58q/kxmpNU
D+8tJECGLbN4/lVhVTyOGS8yahjuLn2vD8xs66dVq82ZU98wJ1WFAvdpHR7zI3mPZEfCIlw50mRO
7nqKRV0fdpZb6qHXMB8ZiFhcYyvsV/w4KZHeytWbEZj16+sQ7r3CQVp3GBVlsaEuCxSe2QAwSw8Q
UhFAOIygEkad1ck5VJD9IHUdY7XH9O1vnxOQMGAPdThjHZ/jx4a+gDbWvJVk9VO0kQmOBPGVHV5x
1XN7nmIqVAJRd104xx7hDVkPOWgb+WlX3DQ8ofwwdDU2u35hnvgq4EpB6DoaRkZryNcpi+TW/9i4
MbtGSDKGUlm/gh3YV3s/7gLHdJfDlP+KDjJwnRprKPZXXz6sIRl3lpBUwCIEliDy8wQKZfpzbclw
cEJ6HEDk+gICyuY0cc5uXD4sQ0qMcLAwQEKMazX91InCvTrh73qpjbSmjcXDcO3gk6erewMxX0YP
PcS5IqaxS5ypa1ArCHYggr1E3Mk08cvQ/vDnldt3VQoo5WoLzMksGEuhoKvn62ydTR4sHfahOZfu
nkSXcEwmPsxOde3HLlSEKmLCN+tsifIGSHSbUKmyEs9PlJFi/JyMMUTG6zDWn2IoaiVkV6loxfCR
0GXjTK+bOwFR83Lfxs5wtgeCSsbAw5duzpiisYlk1gstZ1Njmy0xFLa2PKl1bCeaLe71EPPICd/D
q+g/xEzwcnEO8pmRJk1KHQNn8Nm00uZ9r7+CYrHksKeARy77IqFnDlZwGs0lVggCkMKniv7yjOjz
DTS1+TbPy3EYirnjuK7Fr7hFxagORzptCIeuppgbnQomls8Seza18eMI4IJKVC+Y+ZWH//f8oBiu
YLPpdiMu1rnxGAf+D0D3ZCA9KzAbpp1KSgWgYmZF0cp+IcWl/ocXsfkrbjgXpNiFn1pfLfDLADTq
MTJEibs+asRb2tXehAuutXqZ7VbFUk5pK4uzqtz1VceX+RuoxAmpoTBVUHScvVSeGRKJMsoqyMUN
EK9x3rO19FYrjwN6GCMU/m5D2B+eae2BMUDLgZdbUz4lfvHm3YtRVzfgbVhMYBjosOMwxw1HiAUr
Br+YmOipzHK6jIndAMRFbvU0ZSJUFsVuKwJIjbXqm5lAls0s6GP/SLBDTaZA7Mos6mIPIPg7bbaZ
Z3cn0cf39LMoMzI+Tf3A3bL+VPqI6E+cttLCq5gAZlMV53IiN++Tp2+R8yu9BcYePQagjoJFDC93
L9SeU2JWHQZzD61xvB2dN9sggBcHo57QHhgpAh/PNfkvCy5/nXZF8jDb1zJ7MaYOdAiLquYCitQH
UYQ0JwYCcL99qUNpvTGdSZNWMhJ+jXoBa0AHZ4DkDozb6x68P/3VJBOQxBYaXjTtCUVsXQnviFVT
mdy+7bPe7/PIAuHqGNQ4sFbvb303G18Y9fOq+A0rAUazqEex2bBidlZDB0TIHqYGTlzyimDCsQm/
pUQ0x0xGbsVOI8c68+crroSviQiK/IUMWhdLp1PLVWDqP0c00m9u5Dvuu6N0CwoG85kAeTj9pHHY
KeHAiXRW1VhJf437BFVSGS4m+V2fNhiRdMO8lLHxI0xoivsejMrOqXAXPXJ4sbziXqu/5rj/7xhv
8ha0q9dsTWsmkAm777RQFdLb2H6gs1JPM4VRTj/R+5GlHX44yudDmhKeVcOn5b+3LyAcHaCBGRfG
WrIAXE+kRceISEX4vDQQRy9unebAZE0e2ozuVGR+OrEOm7plECYAtnM0xnOKH8VmZ1iYIQyL8xXD
QIRIU3mgsulw+S01Oe9xEO0uHVaf728N6xA+BFlB3lnzRqF/Q/nlEDK+ZdTG/JOeCA87WsjGq2Tr
ZZ9itdfetbKkXvUM+dwoPFNbFUQcI++jsbzW9N6sT+nvZjyqXwjZZxNRLKWDCl/Jzr8eHQBBK1CS
zN8jsYqcf13pyYBmm0ZIWY1bGIRHcO5GLy7ySTa+8YF9uta605rskvOY8xKnKcpr/vb5I3g3UU5f
DBJdRALggXlCwCGD1tuunrt098qpdg8CR8ztxivHwW8P+I1WWuzBaMDtTs5IGFkA+oPPBLExN3cK
AkhEVZA9Kv51tQKyNrkR1KDBWEqIFDgHt1WkjSsTfojW703xO5pOZRHi3wjBE4hbi1Lf6VdaVToh
0a8BwrNL3bS8eKuTMLOjTLFlrutJlFXqaLFQJujMzhyQGYRe0wTk8qXIWuHMV84K8j81W+UKvyQ/
bALYHn2VraGkQ5y1tw5IorSjfFDA8SQeqWeD8IMS3SIizICQzLl5FEvkcDwR46Q+yii9ob7t05ox
s73GaaXI2PkfqisKwkx/4u2LLMSAA41QSLVn5rhSIGIvqNlNdAHycddNvE7myTQrWs5RVkKzVcLJ
Fx8tQHyL/ruk8tbI2vNYwZfXVb8jSUeqYceNMnEBecK3QtqDmOY48RbGtwn9+sqhOeODWYdnOPj+
LXbIQEPfAWRfl6z6cSZhepI/I6b66+YiUtt98heMJpHc/mIJ8d1rsOxvjW69YFEd/bUzekhY9kv0
26vKtceJl2W2bwJrBpr0MaVlNdDiiFb7uXO5m4P5R46enfh2ZxlYkkh73EfQhSsjNALG6ByM0LAD
/mzGgwhJFgOmBeFEYd5DtwnGO9DbEWwGA3LeV6XlfzrmvSI3xbxlp+ciRceY6Ogj4GHNGbjoyWwB
986sEPyK+G2IgtVu5orNwN9v3OfU+88mq0OnEsNeio6U/BiiSuuUmY7yB4dfkgnt+6smwUfKTl5r
vDiC7JBozM0wJth5oNG3lt8eH5BLShlpbKuR6EGagNYMHmackFHouOUCCIEqNieM8AmPc1MFq5H7
17BPthpx+4NRlLdvI5N6tuXacXY5N3XNeCzaIOXJXdPG9XUGi6BasBW1+NegPwGaveTn1RXe3F+D
dl0unkzF3YOXR4voTFOhYDaiZCbErW45pa8VYMGES/Z9BVZzTtZ2iBxLvaSKeSdA+YsAkDv/xJIa
ioPd4ovymKqP5j0Ay6SSyQtnlFnmYVEDUv6+8haMurRncXdTHlLLDt/hOI6DEcCxUpJlvsp69nbv
59OQBGbmDvSAkQUKumppOZTzw7g2IoxWBc42tdTERdwBcebIrFy3H6d8Fnp+hOvuRfTBdS8v91Uk
hWQB+4TByNKB30O1dH3DxwxiRXDQWfvwrFm+RFlLVpM8e5coG8O7l9oGTPLOByQwG+QT471T9+me
N+yKxCqx1h0uS3p0UkxqmyfA0zIw84a6e45pgMvensuewsU91tOvjVMquNKLsquPNwn43nI5Jk/Y
FjKhp6blMPSqd6Z+rWgMiPpa9WVF88PiB03DMlrlSLQhc33bbi+OyUCYkPEF9AXTR8KJWJa/wgon
HvfPEOZuN3AVG4emR0h9F72am5Z6wesivRsxth4HD5yGx36ZJxgiNAt2x4g+KO8Gc0M/dmX9gOI9
x+0cgDhUsJj8srEySTHGaWUHC73GE8+viL3Iq/czr6eb/0fd+JDQg+nV38hZBgcjUq/xK2F1W7I6
B50gI8/Mgel8P7driEDgTyy+T1jdjy9nPwxfHEh+9quPt1DdydcokSG8RZrfzkN7A0RESZTQmyXn
Tliz42XNjryNX0GUCJ3sL5CIxxSQMmrIAsuoUjj+QWzzzARaKwXHiMBv52+yT7ar/trVm1QPROdU
twRDllRcAJZbEC+4/ZVouKmjHu49OJ/JxKIqMUekg0RWLyf11aGPDEE3qvi71CRRGKqoUDqQwC0G
jlbFcmdHB98vqWlz2uhgkh1iuOnva00j/4idWrRKLH4/4evKFiojQF/zv+1W2z/ncxzbeciBTwf2
IzWIdk8hznoTybJPzCBoZPHp9uawPtWhB3Tqv4YSFBr1icfaUAMTN7nwFzy1jXbPkxAcClKJpMAX
fac8tEPaMehUDlREQ0NWl9hynsmjjGJiLIydCbLyYQWW4k/t5lIsnldOD9YfzBOVUDPZD7CQqQc+
EtUC+FXm5kox/JcPROkc9dh3BNba9aJtjjsFYZ4033Ego6k2XpWD7lMCk6Z6dd+biF198aeoK6mG
N03K+NaBc4dC28FboEKeJGZEtTf5NwfC3t5YxApIgBUe61ucBfACUBmQyxx/czBpvierfAARgEfm
bLeSnULZgvWNdnpCgr1mntbV73Euqfya2gX0xFjTeQPsfs2WpbLNufZY2Kqe652O+SdjCM3272KJ
ZDlVgWpqBwoff5oJAtlXspPETSUFUplqJ4u8CE4i3nZCSUYP/FWuxVXiI2gsdk8JQ0AOakeVFtk1
6N9Nn4N3FxR7tT7ktkkh67OK5BJq1uuSKOwYPdYzhMZlDw+gdEGJtFqVQEcux5ejzaJ8EU/7C1vW
THqPygm8PBblDgbCcZhb52nL/wF4TI6hBNTUGfaIAoFb/+NxFXYns0wRi5pNv/VS2fmhEamMF6VD
BfkgvMbf9m9Xc9uVfqqGNdjxKqSfZAlCWdwsBOklU+c2bGgeL9qesHcbpU2Z/hGrtAjZAuwq7SSl
xyHVis9At5V+NPYYIol+L+9H7iSQpyYvHAKMG3M2bWnj2Rnj2VjxpOWdIbTVGD8t6wejev5qti/A
QwCEQcfHdJZjXpkrLxVsrCod6D8iWLg4DupQL02kvqtkmO2jMW5ZOPT5s/n80RVfT3ruayweSn0D
RiFZUhjf/EbHvMff57qmNFoOf6OGFfWvOPJVlvQsZUt1zzCNgjHirlFtUQ2c2HmpO3uIgez1yufc
rvWktcOgjbiQuQLl5r4c1sFMlST1UArF2bwts9P76wsdS5AoO5g0jFHICnWrufJb53ClU1GED3AR
U2w4uKlYrqIu1zgx3LdWcSWk9fMs6lWw3zxxl/LJ2A0wK2Jvmqu7Hbl+QsKCMbxHzwcXUpXFWVlT
AIhhQT3LwuUcX4rrDGIu1loz4iTAjsWScDLIAb2WvdkuQ0Ye+motEemeDV4T3S1TaxMrewxPQhiR
PTo1f9h5A9zcE8QavYgehpP8fqlt6AVgsC0yXEWYwMtvoctnla9YOgl5XKTwbFqTl/0l1pvoCIut
00DLqDaKzvChKI0ts515Ukt49ovhO9wQchjRKV8uzWQ7Bp08SuyCu382g8DfF1dTJFBPgTvJmi6B
A8srZV+NvBwZBz7y1TTkdiczxG837j1xWNcFKjDQeLEI+KmotSyd7M0RGczZ4vRjrCS0TWTRwhjl
oqHD4hrHlrsYybnDzaZKkKTG1wymonplTv4tZ30PnvaiVFPVfXIJyx6EiGDg1hRgz/ZhQXy232OM
9J6MScg/7130AOIYcEGiA7faxptCzjvLqI28jq0LD2j7xE6ijxBvMUUgHF2stNz+lhoJJ6SX/Tla
2bLpuljulKL9+8DqlIQQ70cub83KpOAhvmtCPZ9HdbHCoh//pwbUWmYZ5PR4U+yTw38O7b3j9M3R
HilRvVfY9yH16tkgek+bEbbSpGajl/Z+kF+c3apyEHzeN+CkwYs4IlQ1lKPkX+MGmSDydNvHRZ9C
ygx3bIIP/ifQJPbLzVwSN4OwP8SnSPX+NuLBHcosfcTnzbT6VPQADfv8SQtgTRNOBnO936/VZom6
3Uuv2RLqbKbPMTxZNOkSBPfHIktumBaHlXg2InlkWCBOxwbAb2dDSQEI/5RC9uAoiJG+ezESrTrX
MIT2VeXoBrsWg3x0LnX+9kmYSpnMJh3QHk0FGSL3qfOgHAyjvGT6LC+1krt0gL0ljmwCNb0rzEhI
iZkHEVb3n8H0pKr/CNovfs9PjrhY3+o6weEr7tNVhoKFFbIwRN+PTubDR+iYakyhT8fedf9WnqGK
EkPUa0EILecos7noSxbsp3LEpGhY/YzT5tns5TFkK48lbOew513V53GXwVKJLXLzz6oQtE8XbFod
ZD1JFSKZUCtWOfimWFUK40TvSKVukQFbkTFDnpBI1v7mzOw9K8UcWDirq5hLMRPFNOeZGKUMkH+Q
bFNYJG0MoUIKcy9oI+PNfsRrgEidr+kmm6Oz720YuNahNciQpxjh3GyCNkQ6ueCO/0gbjDTqBjOF
7JgYfXGP73JwPaVDS6ayhmWaPuLgS95gszWf7iU+vi/sTiQpBwmyIetY7FxszaOaJPfbKwb5hJKd
Z7QumjbXSBONlP9m716KeDoltQIbNQNWnFdpYXhumA7CZc0d7nA4Wor3gKEuUD9mpjuJEtwL6naL
pFRW7X9PoinLixX7r16YS9MFlHXTwt2J8YaqxePCQlujRSTTKfHlCUuWhk40SazWL7AOJrXVjWYp
X8sPnhtOz1ZTthskS5ZgAZYK8yYi4M4lNs/9Ce88Js9DaEPy0v+37ht08/Ps59rWoUZIga4uMDGs
6SJv1ZeUGOldSjnfmNb2hWpSs/ptl3+vE3/yCraVWxNyajEdOqI/2j6PUA4bqPYPyKF7V0GVsB8Z
8Z/blQyaR1KT8JJbMyD1C39vqHeOvg5kziyi8vbPIik61ButSnDsRGC92/3fgOVoq3gioTms+IAN
yaU7ix46GzrFB8dk42FUxLTjuFDzdf1XnS6ltkZbXU1x6snqqOhfyewclTFQLmWfR5srI3x/BnDQ
/mzzF1HrzIkjueW/c/DYF5+9S+XbCzZzAxAfq7Me15gJqwjXbt21r/+l9DAK6i1/Mi13ePJDeEkl
5IfyW0q2AtCmC9ihd4imgae6pXP+y48kyxIZloAn5LWsW49E4Y/izBMJiz2xMS7W2VkPBu+cRuTC
IBYBzVNZ0ArGm734EXbmLng9loX5IBwOIdILXZzwOQxjWOcpi8WlgjkPIMq5usi0JPc4QtrNd+pD
ranI5YK1bh1EMVazrHObrMjkIb1SbbdxyF6DLgONzDEvaOOI14uY9BQNxTfddyPmHsOrtO4jYiYC
ahXw6MPJxIEK3ZJQTZ11LiHCSO8fD3IT5A6eN5TlnS8/djNo6+lFSVTtB7EiiyIHWRBheQh6Gf8O
jwqIuztS2r2XlH2PMcVFn6vR+Bwo3/vV3Mj7XNk8QiAH6TX2K8UsyKDG5KbPJ4CpRifWKg+GVGaX
jZw/ZLfxlLsw8vBpVmTrTQu8+lQjAtCq9aWs9sdIEtnqM+tPULeBFcszNcUXeQc2dU2QjvoYUnVX
MepAoW2tbLdBVrzG9X1FhckoTBXDEHR+I8N8yTLRA7uem2ewGZuMRL+r+/FuGpdwQEIVJGR3dlru
2JNMqM2q/QkDFP8aLW2ev0AUCq9Gyr+JdZOL0pb657b9f0n3tBZUWMfpCrg2HrKeJZKDrtYg72p6
31ilxXYc4o9YL3oeJA54J/IM7i7gmi6nG/ox4PVPo4piPXeNoaiLVQecKzc+uz+B/vhkEGKj3DPk
wAUsS/28r27hBsKg3uJqopJe4BI5bObR0Wxp0+aLk550J9uWAI5IX3ZZ9qonkZz/v89yeXltk10/
5NKpKSbrYL4/DqeT/lIwWEsUW6TGUpC22Djo9l+rpMftAWzrJFvFWOYdwMC0b1GI0fZ3ixs1NZ+z
vUQOe3lSLAZ2WUmL1vskRsPYZA2VKdeSKOfCnxnmRf/FO85wmPHgypDBbAGUyB8/mDOZ6mBT4OqK
JcFICclzfo+6lC5KTopWklz2g/BeoGzn6Ri0TZtJlCrfPXgFwSpICRXYoAMr4zKMLBWxFV0n2yvw
tNbVeCLlLf3yrw6RNOhUKrjumwcJfSuruk4RtvrwI8nAK78Nl5atxntDzmpJb81R09/UgfrMCz+J
11hQBClYl8jPOP4st/icMuVX/T8THU2uQspe/icYzXwlNHc4khoMvBEbk/BO3HcYL+Q8ktETkLO7
C7t/ASLPujDCFfF9Mmqu++laSb+mdtdaSqYRVl2xJ4b3hhfmoCkNm7tMpvsVaCodpcL4YUr0ZQpI
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
