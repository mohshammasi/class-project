// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sun Nov 21 02:42:24 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ yoshi_right_mem_sim_netlist.v
// Design      : yoshi_right_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "yoshi_right_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [10:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [10:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [10:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [10:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "11" *) 
  (* C_ADDRB_WIDTH = "11" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "1" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.35235 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "yoshi_right_mem.mem" *) 
  (* C_INIT_FILE_NAME = "yoshi_right_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1344" *) 
  (* C_READ_DEPTH_B = "1344" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1344" *) 
  (* C_WRITE_DEPTH_B = "1344" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[10:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[10:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 26400)
`pragma protect data_block
AUaP938Qtf97JO4ixrEr+/foCI4UV5b89ZOUgJoXUgsVWu6YwNPwy612T9uewjPcppUFvGUNZcof
zTXhlQUmysfkSoAzFI3UZBT86UYuYHVYrl2goT040EN4M2Rj00yQEwgwYOKt3Bkaius6lxjzBlqk
aakiODLskeMZkkCPUCDNtVF07TUgu+KfqSNFaGaUiMHSJb38sWfHlMWkTUmaK8cxvbRaVV568BFb
E4zBrly/NGFGbFmFsE12gEqxSGMfPsvXkbzPaZPF9mg+owyHpcFhcHdwpJyCzQQrGLYe7/mr15Uz
MSnS3Yumbs9cpol9l8pxvWRVFjFccTr51V5owZLca3yWmgYkKeUjVP0U03ZyBALoDF4QIEO7u4+c
Pq4ch7cOgWeB15DQ2lTX9BdVrThYOod1HdulFiRwzAvqU7BniM7wFwJypzK8lNYUkE3SArAZL/xP
YsZfHrLz7Fc4auHHYyjesC+hJrE+rtUEC/XNyImxIgOTzuKfMwXzimADOo57J+22T69swf2Lpw5L
y9hKvnePHpsHIN1wHljj5czYPBnLd7gUQYE0rWATfXBcdP6Qjn5ACmLQ3+7lA5i820Fq7TJkDcJ3
JYfC9MPKUZNLZy2jF5SOOr8XSbil750a1ye9DO9tAKYhl/x6vB8hkYZuxeo0ODq2i8n0fd0Auyks
lbB0XI10+AczRVWZq1TnoNrp29ADUGRrEe0SY0DjwKkJfsRUgt/dAn/j7mOlXtje9ZSwcVcZIAEk
bURJLsjVi+UYJWWHQEXdJciWJEg5Gnr5esi8n7oDbvn3sv9u4vW2xsOJ1J/Okn5OZ+gKwgKXGfha
mIJIo1z9dtXHBCtfs6vo04RMwpVC4lQNh4u7QJG9hZmIN9fvp+Qf6PfuGqJzwElxz0pjwS10jZ2i
JaMnh4Tjx9KaKWPXQhyWWVvGoM5Jz2QmIs/1iJ43Gvuo+n/gSa3mAS/n9GzajK7HViph1tXYWDPY
96caFtR5atomfBaCguM9+8FWgIaz0nJyuD4BcFsWlJH3qndtuYNHIdcXkW88kfrJk/j20TSdNKDU
GQVM8sulfU2lEXVLKZUtsrs/2PhDicRs3SVtLgreyPcElNxSkInovBQYipH6X5A3yvbUT9NMfyGH
VA6zglbuP/9aQL0QkkH2OhkLAj+Rf+AGzDZvb4p+9dzNuytHOWT9gOWtoIyNLvZR22xHBUYCya90
N10GL/GyYLvRCrvOinm3dHQZ4BPRdYW4bs1T31D/aYo3xwWl9F/xY8b+Ia2y2Hdnt1cQ2hSVhg5B
of/efyrmud0P5Rz4wrGzh2iRm95SEFwpKbyOz39cmV/y1LWdLA9Bc7MdPCE6uBWapszOSb/eCSBC
KiIEdHNWBiCBZEKOioRNdFWeNug82e5AUji727ssisAJj/QANCqHrrxU7cYYyPFd0JyyZDMkwWyy
tl4dgVU+VMjqVlBU3RpaLlC34y7N+Pc23G6TtFj9oNwJNKnLehrGwP0OHkG2DyTjIUc4amjft2tP
xP4GGICdfyzkxi2C4ynp4kONdXU/O+wrRddXMjEBwCialSUfZPh4yYDPtarF8/Xzs9S4vlNdZV3P
7Kor20g45ebMMLgD/VV4KbBslhhwDrraFlqGx3EDa2/vPuDp3j2akfWJ+rml5ZRfGqI5tzVEkJbR
GvY0uAogTP+mQACaxGAV/e7Ig8ojCC8zQ1Frj50EmApyOevzM6IDYXTfSfrQ+xoThbEIr7p1wGrt
MY3yVQ4b1b0+IviES64UbzBj1tKDHAejDl4TEzZxU77zMG/6rCZek4CPlO6Ypf+/WkQ5UDJuULsF
VJRNHJ41JH3O2aJF8IZ22FnEjj+HGJK6Knf3L+kK8vcLgIIJNllGweHd41/4UfiSCM+ETjYmOgr6
VD6KJC7kroEBsnDyZJlbrvC9SnAFcLFxmqj/AfLKIfsQZDa46zSXF2ywvIcUQkL9/K30oqWjpD6l
Of8DmLmotscMPFFuZYM+quwqkruWyOlF+9Th22A8KIrgjOMh5ypQQYWm2j8JshFliSyvLeRIdSLX
ddihab8imMEYvN1rWoZK1Uyrkmk6nNdbuJjSIQKzc/WGZWE8SwLjgqPg3cl48XS9JYoM3k/txTFl
sEMZEaCACMBBUPnP1N5hV8Dy8S5FM0JEh4/CyilCGOhbj4UEQZmQhw+5GYn5WXQ+CohBFjgSBv8b
VuiqSDAA/FDeWjBFhVrwiwWDaUjB5XelCi1dK1DSQqnASUST0wH/yzoklDg/iCZdAD2dbqkZ5ebh
hoaH0JSXaDjgRo5Rwp6hR5HBuBTEvEANTNmtRGjD/eK3j2P8Taick2uy1gY6AHWjMRg4ay40jpCZ
58qXRpYTFAYmIzmzp1rsxa4HuIrUvTCheVkPa+LCMZnTxPh1x7bhP48zu/WQ7FfbCqeH/X7G3VQB
K/3ub5YdXvI9oGFEszrq38BWZdgOVdaYDUK618/v/tvDbdFttj0CbY6IWezi3oxde2qw29uYqiTk
3HmpxBoXnJO9LHNXGCPUlPcmhK/iCyuWlhIcN+TG5SQ2YdO2DyZYU+cLozGTgXhcs6t2k13norcH
/kgI616IANI1OWvtmOXkh+ovlUQSHGRWMoVmPOv+GKtlCmkI/aQnUTCO5i0tq+D4vv/hrU7tDGsN
7dLGBjmo6I0O2pBG5on8BdK1m9JqkjF3unsionLLNgPB725O/h+axmy4rp5m2MN1ZoUUPUSmXwWN
CJnuHvS9UhVtlqJcKWxoK60OVFfxjF1BJ9x2HAihfjsyqdWDv0T4ESFEZOovH0I5HGlBKaS/gicl
Lqr2bWjdgCwvYMAwEBqiJklPaAWmAkKoSqI3uSBxqmlLRJbab356W0AkGx7x8XyESmhAK0mgdlIe
H97i/GvYDNzKj+6PFaO3eBqcyvTDW/uT7ewNJiqyb4YdkXVjFJ0u7SWk3s3MhFEE8z3mpV0s06ps
aimEGLn3yIW69Kix2AVebE2uqOyKu3VL8VLdOZllaJReXUyvh+25T4ctvRrBlKdIRzQg3NAhp0J8
Z03gh3Ev1E5rxQwtkN/eP4aQ6+AtEqMEyF2EpMHVrhunuwN2giE+6IifJ2DD4o4oMX0dWGfs6uCO
FVVzYrb7/4Soz5Sg5cjueOYuIJRl5WrdTIaaogM9GHI+8pLSPOqF04vzrw/QVvTAc53LDol66Qiz
oxmSwry1Hn5NhojM2fuBIy/92uX0jLA9hc1FpGACaSydpI3wJl99cS/VeR9pf6j5bZDp3/V6s/xO
pbUWDqvN919kATWGBLruIFvitB1CKOsHUC7xtsRG+WGvVOKRqhekSi33kzM2wZZuopDLgIPklnuq
rtoGTeGdmP9eSPvbv5AUbZTvbxL/zvz9FCRsq1lQUPhVWb3mQkzmHYffI4D4Xdvvpnq81r5KVlHC
NCdP+sxaFuGe+MDHxeeTEJMZOCNkknm94we1miTsyq5ypA01xW5qygAseEjL+y+H0CFC0Oe/6Sbh
yNKPawEteMVVcmWPvgB451wImuehDhZRvt3eAi9KST8xqiptZYrvX8uTT6lTwzcXBakFNplKdz59
mne2OncPChZoXayPiR1hc8iUeAriFsPhquEisYOusxNF/8Lg8Hagf4ihyR3QajD5dfcXHXVgsVfF
0GZN3oBAlKIVPUmMNBw8ZbACIrArNkRPfAmy3COEqpldJ3GQR88vpVWkvn+nDxilSWr0kIZDSQRA
6n9tO/HlyQ7w4+Ci8fRDO9tSCEMdTleu/oOFrlEUaV+sk8r5d3lJ59zd/uP/AUqaOugHXB+Jlw0z
p/xPoKDjQYrFmfltx+L6Xe3UIkOISbydcfdyHItoxozWk2mWG6YHrLQhbG5Rxa/OW5LhaBL4l5Yk
r9WJF7Rkh3v0aRZG2LNQCXVeYgZ8rXyvLZCk6CE6AYlQkvqIiW8YKQmKr6wtFfe1XzKDMeZsrz+z
PEdx8my0GFXDdv4xc5oayIS86GJLdaoMZZ6pyUomNgTvnk37oYWm48/wQfs0jxiqdwAFGG0uA8KX
4VJV3XKot8A91ACCriQZT9+6zCkJyQn62vOkPbLIHubQjYFW05Ylf3PSs0aRs1pMOaTiHT7qjQ3z
9MYEJYG//nSpfjQRnE8D6f7Ana25BfiejqXbn8qbZQkv+hhXQqoRIsZ7dMaPOl4YqSGjyV+yLhgx
H0JexMK7Trxvb/JaQJc5sOz/04pSzlvq9Of3KrcMhR9BOb7ODKNjW4P6uvC9uo0hwubkTeNjxws+
g2X8JcV4OMQpxtC0eIdYVYN/mUn69gKnjTZXKMia/efZEv5tx4A93itXrrpDZMyNKG/UzRIGhQqe
m5YEnzwlL0Bdf938MhpfU0eQUtEIn9ZsNbyEm1DsbQVDWamI2ocLFdbLVOl/MUTEa1b4YogipJ4h
/IGZF2tIvGNEuobS4bfrK90WL6A4tRBOuaQyC+MjW1P1iai6ZbbakK96I+T3QKg2ssfgUy/92JTq
cHz1YGdzbGxPBQO94JIyRwA+7FIHOgDJr/ozN8n8DfZTwmvls7aWhpQLpoXn/ProLGMjjs/N3Otc
zMSvZbY1Or3sw8NWKYYccSa/5v3lx/SMj9/PP7vurBAMjLNEZC5G/tYwMXP4SGbM1h4NwWviaVbK
FvNpRvCUndkOIoqBEVyGcNlwM7UGQGZdSRN489wkBhC1pMJaK7JMOLHlLfbUQL/HzrURQeSLVPcA
8sg6UawBNIp/zl/ZZiaw9UA1bDKxvoeZDuMJ7tmFYMOw4QQ4IWzuJ/unELESKlfEdGzTWx4WC3md
iuK5+7qkIKep4fyEdUZfnzd31FW1FkLMonUIM6SkcnArmMtCXm3VCvhxKyJTwRfQFeZyDohOliWu
cJkN52v9jdH3N+y05eFKDkhJ7KrgCScKTH10cRghdblajoj7tQqUdwTnG0K6hsiPzwa165ywJpwe
9SQ6GYqVXd3TZK/+2oqmQnqlwe/ZsVrA4dT2sBq+ritv+bWH4DObZiwRanwWyXI9AnJwWMfodstb
uzpGeQm5tICYkt2EBGiZEEKlkUYcbOmTP7xfE08Ibb0HKGTqQqP6tD0OG1XUWpt2O6+P8vchDEoE
2WdfYr2UgdWJaiVxI5gSU1RFbhpdHnXfKm0HghnNH5J6TZIOEoQ6JIt/9sMlCfANqfzDN9uIdTVu
/0dV89bwHF3EttO4y1lsFjxdjacW0OcgnpFFqzmxu1P5Ri3sMdgfhMTDiy3zbGJqrrd3mbvnFR2u
KAeM2K4IYawtD0fhCQOGnbG+ynFIfcqR0fLlpVfCBQueIg4QjRGo9d9C+fvam14zN55YBwgWo85z
hwfmgVOIHDloNFszkMqc4xJ5yvNcS+63E6LzMuZqut2dJ+mAEqKUnAm1HL50XSSS+kIbX4xjhW0y
n1iguJ0+tadM36rfVYrcplXu/gl/b47cdAavT1j8BqCNNvBJKd2nX+BhR+0akik+DNZz1rBXoQe1
DpVnFxJATAJVfwAuLMGKY4Mj6O5mpPqrrIvYWPJnTzxRw38mUzcV8Fxw8iHXdcGV7u61KGb8uP8u
qGmSZVhk+3wyiPjfkZdbD1PaFPtzMLo4kzklAQiVq8qUqPTkEiCEeW8I9PSLwVYjV4HD7aj5wXCd
IZlzuKnSBbS/WpV8bShrG7e6PhrihAYAH5YVDYY7ORIU+UOsngxg1Lnt2NehhixU9/zkCgNkixAb
yh2zBmjyrSmGjA3bNmLmVHV9EYEdgM52U9xdL6LBv+I+n1DgMBx/srie5Y/e6TmM3vR8gW37dvmQ
1vfTJTF9fm7vcqD9hed9oHHyLSVJQQtXblnoXWJt/pIYZxfPMzfRBd3fkD4lf2yggraGurvLsBZf
3SipMEL/A+XEgTdQGMKhkTC0ZnGOX4sTJHUPbrYwJdwC833exfSAhpzKcQUoeGWeTasTaS06HHWn
HC/sjJztwL8amfRXFUZwijCxUUFAprC5QGE1lk51HcjrOtfFcp3bzDIGYQtoXhqLguVIsz6SYpOj
6Ugb0tlNYmKltSnl4wWp9HIXPVzoy6oaeIVDkf5iVGc4v0Dl1j/km8/IIVF9LUOrSga8SzfQSsmk
u32Rh54/noKP/THO+53trG9u7Ub7LR2TbYIj2DNlIkv6/c7B43/+SpcTojruZvYcm7sUMdfclwub
jBs3EwvWgMrDWCk/0S7811d6UuoFeYpyFHe5hUnpVLSoFz7t5MqYd9UaDUyiXhPw00gmn08gAgUc
MkRyb9Oh1/EkpdOE1mE9Zc05zMittOVORsxqJ2d65DF7z1/HrmO0eEGg1kYRSdkjSkbopIKzgQVO
4PhvDh7H89dKq29sYjOWNR+Lm8myNw8u6qAg8R3XaZL4FIAsGZdGtfkrPUSIgNccZqF1qyrdEXoG
HXu/yNmNzJvWf07EKqHWIA/S4R12oiCLaXeFy6Z5+0yrY9GVB87MVqNGXxHLTcoSysyieKcktpzf
V9ilimLBP6usfdl+2fj06v+JHj+ssR8+3oFNwT1wrA60kE9b4ECQbOuyAxXrqAS3gapVbcKqtVZC
VtM20j27YygKzlpYv8gUBEhpcADLd6TgLSXwtWyimvG7gIVEZ4YBZCHyVPRsP2nDB/0vpq7BswEz
yIvagpTQTdv524tXXOqAJgCRQebxxudj9H3iWj4jX8wGtTyKFYbtq6VzjlsNOsiM4GnOKLU07Gcd
j5/0dcW82SJkxe4p3td1zIZ0VJyyS7z1vwLpv3JcRqRxMdn+gZpsxLn1B/gil7hUfN9oXOztym/l
UIpOxLIsXTV5ZzK7fAtfBJRvZivwS77Js4BLVCIZaVaShap9Tl//V34He7yEl0IN0zvy8f0Cd13U
0506LoMhDf0w6d1e4wYT57JexZMtxNC9yhbtM1jADnou1CDS2kVZjQ438I9SZNhAcPvleaVy9yGe
/rmvoFfGNB31ea1BvRc+Xm2zwTVX2t4ijLgFeTYuJP4GWd3nlsJIeI3yF9RsF4qn9ST/0gosOXAI
erSsB0onI/J6RxG2Ae73bsGGfLtLFESr+lmhQC/+zmoivsSLJ+zppnSMa7RDRPBhM2OO3Cwck/Ol
EYuw0Y54oQQAIxXxQvENO9CQ1Zvin8SUiaFcbCFWaBrh0OGCZH4mZw6Xn4xNEonGzzvkWKhEtUCN
avqTslvsRAlRh+zvcXmIbL/i7c9/ZN92GyruMVnMpuW+BIW2ErdEK6JVvNe/GaaEeyaSvJ5BRUCs
kr8rLTguhKk9NttQXd/6urZjeUpOK/3co8PGs1pLlicDw2KxNT7B7wXyPs9yoZKKPXByd+lgQWex
myD3Ax5pz8ubYGdK6PqwXOIS/ltOcKd6YX8RD76lQVLhnPbxTeXd8fEKokTjqRYLjhnFG27jrS3i
tZ3xWD/VWpf8uk8KxrHap+nxEQfKwkDoTqX21PxzcfMapHe81EVHlfCaDkd9Tq6YeXEufRaEr3yf
mSLc5g9Q94iQZEzSpabtNvuwjnP6nLIHe1JNzsfizsq6VRkwal7i9UWJqHzNCmsxdIaFVrfxxxf6
/g6xnDyGVME3g5ldZqb6XfQ2tqu5ZPHK0ayKnzCaZ3ORXMAZS9eeBtLe7eSo7WIl9M/bCsZ1pw+Y
V+ATsNm2u16tbmX7KrWxCPfA75FWVQORiXTMys4PWP+NQZoiU8sVfuOcz281xWD2L66YwNgh0wWI
pCpPzzHvNDMuCpcQX9sIazgo/smH+ryi6HRTtFwY/srsg2YYa9oK4PH04+v1gvr3YN9ClK+BCP6t
jnausItieIPGV28TjsG3o4+TOkp3lfkm/FXud/RR/WcNawquXK2DFuH+EDiB5M+CUxPVFrWNHykq
duoxwOMXzjjG1mkzYgNFmVzHTWPEFAmohFxIyKaXieTqBohSyJU+EbWP4Db0FOmQODQnKXDo+1Mn
untQpNMcbv6h1U0UAs2EpZMe0gNJnJfAilVEejLrlqpUwA4pxDeQXd+tAuBqN92PPTWzJVR3Xw7C
HmcdONsHgC5je54iEJL3d47Igba6THx9whK2jLOMIs/DoaTQ/qqYcu188rlxs3dGd3GEn2lXA0lO
3cFYTYi5dxrorpvfwfVQDrxnbd86nw29ZQ8zVkB+9FZB8QpL3fZHq63+KuonySeZ3XHL+JV7nQns
vvEiy2Fu0awl5P0YpV1RG0eH/og4+kFekFnCElsNLjMCUhX8H8Izt49DIWaGO1xU+oeUlCO7S80/
bLkeslhxG5S6h50lVq7Kp+XienHFaSDluTWA3pyHakbSYC6kXfOMNdv1NbCsUWjnPPaJhvACOppp
EkirZFKjanQFg1f0ya6rGfIM0F/FLt+9jDZFeILKM+ZGxeJ+rvxv3W98DUaC7adMYdZ84GVhBuDl
SK2Fz32u3T8Qi/ZqR4xgsTnfOMZeTeoaR/cSrqG4bWq8QWACnsvDkwXGzi+6jfo0MbtO62BIigeb
UF5c3lD/CSDeCYgPFyxkN67tdvNiqOlQJhfiHq3q4E5sL/PDoiwjiHmnM1MJH8ElOd7zxRNmBEyE
Bc6S6zZPIu0CHXOx81N7CSgSDC4HeUZxq8OyKXsQI6D3e+fqzniwSbfE+NiZs5YNpEzcIEPZOz3N
lHFakYVvmVf3K7OCmyva6c1GAOGCWDmGrt5yH4qqsWQ9qx+SLLmZYA4FOn8GXZk7wJFVERyQj38y
uR7bG5Za3TIkb1eppRVC6vXcRAM+04Gr4jApyzgByf44yWRg2M81oSMBydZ8uprAIFvVHghXR4sJ
GbM+g80ndw/2Htz4SclmAY5Uw9UWdDXwZVJSURnPCYE/frBi4ORCgdvrgrJ8OV/ubBNsNHC62xYR
XoWoPxKlXVsopUDswhcgTcomFpSNJygdRVejsa2hCpxuqDES626A7oOVyMfaXiNqMPcND0tQDH9+
ju2oyUUuC29uwdGX1Jj4Y7N40VvnNv9T6crzeoOiZJEgAEDgf/MiXbJKXo5EWLjl7PuVOVlWD0ai
g17yPzfV6WqAAXoPsU40LFENX9s6+ZSIouDkRe1Vhf3QiFo+IMYDAIbwYYDNHx72JIBn5FgdB9jD
62+4z3DoBsaN73BTVnhSfrdopwLErC5wbwW78zMrzxaEG9w/0r+pfekhwTAJA1XSpnJtXQFwALZr
rtrKuL1dXH9tOAD+h6l8dT7u9liSYdvlM3cxftmion7baxIbWazA8/yjdR7qAmW4hPu9Vb9BuKa0
BO42qTFzz6fRUk2R5w/BCokJk1ZAvewRzMOcyu4PE6wu/i5qIkRbuH22VwHRhTbQNCrFw8GtKIiQ
5sNRVTX+IzCzPdjc5qikJrVjrZRrqkjmgwLhzfexC5CBXmxKCf5aubXyBEM0glGdlJ+W6+0fYwEQ
3viAA9gFX3rsoL8EgdzqaiJU1iD73RL4bUnsmvlFqGOWRkDimYAh/BG+m3OSQI+0ZNNmNKAoyaro
WCRsdx3qXCvFc+yCVhCs9NLFdYlZnbmgFdXRZe5YeXdUFejtVXuKKJz0WdG/ERt70M3Ujjy4aRR8
86RNf3yaoPpMATqyVsYwOulq6yoh9Lxw2+ddWmUZYOlBjwFWfmlRK5XPyztBYt07XLiZmA8ZkaWX
1EMcVi/IfZONI1v38N72PWKqx1owLnt7wmk4rHOh484G7aw/dVv5H6LIRnR+AdpAM4ABX/YZCBii
WqNFu+VxaDRewWU456R29N0vH+r5uH1DC3YX/fgg01u+SDHX0gpFtpyq8kNh09h/KNK9XSnAV4r2
z012Ww+iNQEKPRatgQgkJHo+U4114Ak4gIbXTw1NJDztnpTYXQgFlUcK+kuZBdTN+aayTuSs9Gou
UM2VvLXmm8uwq9Jg14LLnJc8sxNeGSLlyeWHPmT3QFAy9BnLp4dqa5HRZ4XmDJJltzjMgjftH0nd
6013iV0uXUy+uimpib9BuQSgJtrh4JbXElYf+yRQekc36tJdl/MJ4c1nG8RKtsy9mccnREoF0+zu
iIwHnsZE9tTfSh417J1zJzfMvYt6XTAMIMCMHFQGN/T7NDLidGU09L3epqou21UlfyjXoS6/WGeN
sTONLysxKHcIV8PEIvy9jHNKvunnsyAjj6Zt3n7Pw/5cfNsh0M46+3f7Edc5S9l4MgU5F5+XVYGc
8U9PKdi8oAkIZfyMgt4Y7U74ER22lcn0DVYU1m9aOYB7874XZoIur6bZ+f6TxemF5MRCem5DPGiC
PZrT3O0wmK6TVnruHxyGpzc+uVOf4Q7pR7QFGQB6e1+Z2H8MUdVMMrewPntgCs9eMHOGjicx6LF0
VFjIGm25+RYxF5/SMo7kkf0bTExqhvQepyYTejLDD+zT3VoUuPx32/gI03n1zcJ405Xtj+f8iEb5
qRJOirL7fLEFy5xCloD0LsGNOScAZZA+4hERDCM8TiFmUecbbdjOfJ7WFsrAh+VFRHOD3q7KrMh6
0BNi24oKT/dXys7m2UJ/mD5mNNSjcqS6uYMZNaLtZRZatXphireU/oK8UF7uTrV1bx9Nw2VfsCPC
lMkgBQL3kjn729ZnL5vAva06MmVONuLKr7PsD+zQNPT6Hg8e2QRPXEaMPpVW9nX9bYULqcCDaC+x
IZb1SEc6v9CiJWjY6rBZTxRH5MbZChMR1ivMN3eymWDUh/pB1iZV+nDeCYPJohZppYiLLfIzubK7
UPfwT3dyrd2exuRZmjWyIDKj6YQj65KHNb1f6r3odOYnxyeNS4C0zjz7FO5SWUWcnCAzumTFS6KJ
DdYuGVd4mZLBtYl87PFxUpNPlUbKKHu38zAeTh+IfGdaHMhEKMrpiOADoqu2pD/E9IGZc8Tu4oZB
M6uaGygjPPCV+ZflVy/OHdqHLIj1IN0mYuzqB3HpQNVCGbLX9WnzZg5Fs1IdWobm30On18hyKEvq
c8o2ev9ljwdKzypqQPAhFGzfJJaK5JJgV1BK1rWLHAmAVFONnG8QF7TUG/Fyf2LXmhJ9sreHaEiZ
+eyDY2knCqPPNM3+5J/wxw7Yhm1LaTB3JDpA+VesZj623583GVuKhnLbMJTyiLHPTFM9UTPwVQJc
iZdMryuLM/ya9fylFtRjjMdWoFVzaf1kJP39LYiRJ1SrB2UBgIOb8vUup2cRPVXiqdkVINffR5VI
O9Gk625vmZn1hAkPdh/kJcXQdEhq3cPJnlNaqtWSxW/jDzbdMobB9SRQ/Zw+1FmQcEQZN2OWzI3O
nV8ggSXI1mtsTj6PaEEuq3V7v6TW8UXyt07LN/R9BlQKRkKq38syW3/nL2Pkwbv3sgTJBVUVSTzv
GnfFWg0rIHNg+xA0FFQ/YS5g+wWyPp9imqqv9W+IEisQCLUfn2KkGnlIfWSlv3Ei7UDY5vkaURnf
c+wYnr1BSGr3hfdQB+QohvrEEkxwDtzJv0h2q+CVIb1Dm04ZfidnJMj6M6ys3ogIRGGlkKniXfXx
1pi9dSktH+Af+GMtOAsHnLGmyTC4KtdGdnDWSn3PBoTa/gPTzbRtrP/E71tdKGtGkGUSMkFfDF7e
0r1Zv3Z8hgo9LIG+vg2AyiwSiygSxZpV9XOGnPFzqrKwDU7/8/q5BBdozZk4ZwrG4hk48fmaOsj+
eS3UjBw0r+OPPnAjI4vTxjpTsM2Gnv/1wziGy64biYhEDCo4wCGvB8WOypWBjr11zO0Md4OQcwGa
06J9pjhSjqRRsVeF2LxgLJhH4xOnorQGa6XUvRSzC2krvmzj4gL5EIiLVbbxg014OZD/P9h5sn+H
7nQyosVBMsV6hSuM6MzirULKhf3/VQKym5MOZchag/jMRRRT4aWCwcRwhI2sQb+IdDkXvf7eTtaC
kdu9WF3g4QNay/rebQKaOb9Pnobs3gWK4Fu4KRyDPX9ozluES04VYWBhQylZ/M1BhGmXNouYTaWz
2vqJnSvxwQPb7YpQKM25pAGrtjO76cW9A+kx2Bi0vWdnGtPmf3WzdG27nTp3+1zB4xNdj0ohSf+l
h0lvN8AV+6vMumDaWGx1eeADGU1Zp8IANlxCtEsrLp6gsDsZlFYo8Nx94Edv2i8kHvMvuVTYK8ml
IyA7q6THs8+VrlTVVlXGf3NY+adJhah4ZeKwMn2eb8nodph14GZBkFDWTnLmCmabrgvY1Jnkdu/T
jLo3Nq+xmdBQd9JJjKTXjnLYee07XAH5r/K1tJu33R60hFTTjbYLAehCMZN/WKVav1q1io8Axu2t
uFMfu3ajFQU/V9sqGGx37lG71OMGclanNzj0MUCFKRhgF4RqrFCmsG/TKGEjrb3/qvhTO2lGtykp
jBLv+k9Ki+CLmS6F/NZZ4/hGb8DhS18z5uAYWeSiGcbitLuyViKFX2sXZtlHdbcKUPi3PzHrzCGY
e+/QLXXJ4lSBq2Hu8gesJZ2AIxvwfPTtWerM9DAcPOeCdQEAUc3eZvPm8vaQlkfuIz24xJP/wjda
t/e+A1i5XA85MRWEAc58/jIUPGgrOwYIyPeVz0tzxLdqp0iYSo5kpddoRLkhtlDK44bG3BqVIj5Q
688mpeSAWomAOE0YDJoiNT+P/j72kJWZlPERoLyjTNUZs07IsBYddszwqdvChF6+tzPaaHgeeQd0
CM+uzEC5lrhXXPcx/K2a7lx+LGatIi+vzt6ZW87Hj2bjPcnXqG8NuNM16Wfqe2VcVEjXK3ozA1/Q
vS8I6SPLSke5RtQrActsQRZ8doiv7iEvSxw/CzLdO9s5M0dZ7Do4rgbJIdiywMgbyoXUyyfALe1k
H/ho/BFojU9+cPF455+eRvEOTYd2glYLewlX1PbCsxkxcQw6J7juQNTf7dqotshjWyKToLsCUigL
P4u+TvpsPkxm7dmiDi1hHKehE9qbdgnWynIzeyY/Znrb291qu/pk4mtzTJZSB3aODXZo1iLWsrpf
TriAphXo42JLJj1XpNvLv1yweYyfPpM+qbfjSXIHZVAj8xN0sM1PRTnxJy0OqFP3+pl98FM+uRHv
rnSK3qLjQjbFRjYxbBd6XtTqRfk0Iw7XxeJy1yV8MiY4nglL/xvkESBLCEkk950/rvrIVsbNcAxk
5mu+OIqalQRVRjwCyQtVc49qKa6nw13ll4Rs5wxoHZs1YbhCF5jtRWdWNtn+CkXsts6Pq6plMKL9
5hvWPqr3h6DErrwoCsMdB9ivSjY7SjiuwcA3Rj1jCCESUBYkVpT8V2tqTSg0P1n/L3+o3w9ttdrZ
lVlDHpaJtZfq4V9uEu6L+A28HAzuZZr4lwk7BhOYnt5T0CZiAkBGTiakd+VzqtPVZoQG737RsSkb
hV+MoVKqFPx4QaVkLebA20xv6i1glxTY1o5+vZ+GwheB97o4Uy+S8zNuDWFRC5FXAYgBwDjHvI4I
2ujf36D9sQzYSTVMF0IZQGQf2sFANQ8MaDxI9CHGy0dRTMv2bTAhZTpput73yGALiPERi1EOSph3
HP1GoTk26n0mcILzBhty+boctWDs0GqNnp70sX4/c67VbDHPeuM9pIOb0hEMHXoxI7rzoR+qM9RP
u37yX9Nemvx44VcvrXV/mFLQ1pS3Ms8Ls9RstY7VQvPFjllmmEpBy/WkKvUr21xkxH55zuNeAE7f
AJo3dj/UuwGRmcSaerj36XjFru9vR8KH2L4IAkxZPn88I/vuQdfc5Xpeo5sqwSGOBY3SfQUeEiU9
p/SZq7CktdIvb1XAbDYwome+nULmPg+ew/9mQQTxPKArJittcDV9Tao2h4Qa90FlYPzAonEdIC3q
bnoHlATghbzdUTqxu9H8iLKDO6BIDFtMKA5ajlMESEkL5voio2/Pc9nK7QWsD1/o1NNXL/eabK/S
OOeW6I46gymvlIZzvz/s5YZWJiensNOPkQkcY6t6muMEzDegvchqSn4ndNhGVLLb/P/iAi6GnMTQ
NHld4zJMCFUZr+TQ6AtUDwKoYrZIafar/i/zjs3VmVwwQhU1fMvNZHYkW+AKI42sTRRwpcAvST1P
xZkvtQSyjEfB7crtpvZYOV0oCM8igRXvxGmRpDsZRfnffz7vnYlJPymde0FfsN+mWGHL1zjCWfeX
keyZ7yj0jvoEB5iJiQR9ZBzpgB7qJY+eeLrKiqJ40HSN+Q19Ur4qgebAIOeGdMDJT5e8bOqaXFDf
MkpSwzUHLp7cfn1YrxZ2BCkQX5rNgKnBQ842YmZj150ADDjKq6J1CoW7SYwDk8RscCAzJYQ5v4NH
KHo0aE96TTFacFy2NZP9Tz5QAmwEqhik7Wy8tYs1E6tHNItRA2ochrrwbSaahLfBzAEcp0i7l0c3
iSQmrlnl/tv3VSlCpcjKA/TYEXsfPPN54PFxN2371sfBu2kxyv32jFVcphTvNw+K0Ya/0Q+VaMWC
PhRNNrmsWrWJRtWV9yPJvb/ueOmCLXjm+Ef+EgLamgfJNbjK4YVmt3a625mI0224Fe9Dhh96Spps
NSXRJqkiIU3ImQMF/VMXPewHS70xDX1kOMiozEJBUc4MYrUNxQ3qCxwPZH/ihC1A9qR4lQ5zKLf0
RADvtLhqCTL9mMB92J1iVe+wmaRPLq9Lv6RUvB0WRx+cdq+WxeYhVYobjQoil86J1Apnlk1FgCgR
MfOy8n+oBXnJKs7rCjK16pY2GrtObKxUpYhgzZy825Z+Lbx6Rt6ouARC7Ald1vOBFOP2Ubx+I+pP
DnHc6WWBl499se5I0aHUoJsThz1j7CmCMUfo0dweQa8k28BnU0HzSznSEcbhARBPByJ7O/PVMqAq
xAOgBAZri/+2iu5zer2vQZ3UOpxmUKxK7wxYFHxZmMbtrdQL1LRNh2OnSQeVQkAD80J6kWzQ/ZKW
PexwF9X3e8HM2h/EX+0FIEK3mwdbVEhIEtd1Mtxw526b3hd4YFyjri3ZlZNe7g6HleFGpO4tPrM3
r+g5Tw26stw6IA+f7bRWTBKmRZaV3ofMBw8eTD6gMxhN87+/9DTsU+Uc1hkqdNerfi+27gdLzR4g
ZA8Q2nyogIT+P+3HAj6snFmbTwcobU2DzhOPM8p9eQbPqaCqeXoCOx9KZ+Pfxu9oEx3glFxjY5l5
Sw/XV5K0lHVNiPM8wZEb7HPMvkJIaqt3oNb5cnw3/4denZrxYIYcK8XeFBrRHqLlhqfjFcw0O09G
VeMAKFgIhg7CJWoMRs5HKFdB5IUQcEel6b6XxSrznffiFTQhfP5laGaaosAiUb2JKV3ltBN0803e
gEsxs7JTyV4s/bZP89+7q1dg8kpj/s8SMVOoW37qNzvHgoB12J5DdP3JEVRxk9Y9bIbTNXiJMDjV
hnPz+LGfUpP9VumxOwVlO3pcUWCa4BJDhisR+yg05bs8nN/1UpXVnqhVrnaQ1FK1I25Zy/tkweYM
zOvvNOjcxaP5aUtadFAjGfGymTdHLgaWBuIE9/JM5zvm53GvsoCB73Hu4XxS1Df8lHYlX0HReK9d
j6BBLRn09e0bVbQfaPpScP4dEczfwfMGrHu8dbbQ8yR2ilOj56OWmnBvKL+qq7OHyAcgZXp+IRJv
9YcBcEN2jN10VEFB7gKhcUCt4wVNOKaT9c1ubD6Uwapou7tvICik5ust8K4CsbWt5sTZHB11DN9N
WIginPm0tx/staqyFFJ83ckWAxNEnUWSY4YUyBwddkDpAPQl+Wt9VF1EbUc7NsRINz6Vpm3OCHD7
xf6b+G/uC4kOVYEx6lDhYoxlq4au7fyyJy+mf2JZJNm2ZEBHMydutH9bmQowDwzhuOG7VD/hsXtf
Ghzbi8ANjC6tAhDlnICoNi4yqOc/mfBlUuu/J1iKE5ZqfS3xXbfNHNFuQQwoEmVhtuOTxc0gYHVT
ZXnpbYtsBcg6+lrcbOfjo24qsicGi98Ia5Us+GkB3/e8Z0814igvimnF3S0jjz54OO/Qhey1fcYg
oS2PFGiJ27m8qaHaPuEUEsM0ZoyUNbYG+4hshOMS+g8TYArUNc/FmoNeRpXt4d0ySfPtiLKmlnge
KxtS6+O9HBXG0xrpZVvY1jvUZquQfqcmjS2COVlDVLndrxE9mKEltCnXXWTfKmXt8054tfZ8ofN1
5W90i38c6c92+ZDCsSRcKWpbXngr5E4ols6wMnapn3anyGBhTcL2hrBlJs+TjotCH7QD5RMJhY1+
Lvh5+G5gSc2P7lT4UD3Pn1cSAZScpFzcvxu0mfN2hJ2QHv0FxaqVtWqs0vfUZkYd6basSminb1Md
jtup+zRBAFqAT5N628YtCUKhMG+dgzCLvZml9Fg0Wu3RThubbyG1KvcOiODB4eP4ovZpUjJk+0KI
sAFPTVAuH6UFH+kUf7jyOA48HO7j/BPnnZYtGvVnUZDudubSnJgzFLJYVv3Y36Atd2ZyjfWu7RQ8
fCJt1UKt2BpvTBqSo7a6lvZ4tPFdDcjiE3fdy0geUx16Thlys2ZFK1Qpw1CRu3QeOTokOt8gBejZ
9UVsWzA9Dmcsyw1/pmQ0op2lBf7JkSH/mf2emKBfATDhd9CzsqkOWAnQKqL10yg0eq3F9xNTTwHN
OkOB9+kdr0TNAwVf/Ni4LZWdv/ok+atHes+1GWEbrqrYUCZ6q7+MfwYZztVQU3KbC2iirsyXBQws
q+i/GWSgbITsvevpWN3WwG6bICBqLGTG/bhvyYTh/8mQdfow0aXX+58JLqw88K7xaItnSpx6G6sT
UEH+OK9gtBhXsbcYPY+I9DpW7nEszOKh6DO/yu+QyJkHdxyiPKJSXU8hk6pRVyGPT2wY3zYJnk67
CP2KA3Cu6Ox53KgHy4HFDyZAQFnl1B7VMNPLpFYrfIhpOgQRTx1I0mWLxA/gXAivwdOHOWnsaFEx
mTK3AtqXCTRXDc/qszFekOF2p+8/o8cempHXPUdBLdT5kn8uhrlM0DxjjwjEoMfDx1uz+NO0ieIY
UVVSvl0iniYVWJqPqLpSAzOLyzHCwPjiESnrBCjLnEtxa/6oMC4SOZ6eHpCavGRY2HTUH0j2pOPP
l2f4/8qeBlx60Um2d+e0xkV1IQh+/q4oFGIg+gQ5V2aMeYwAvji/wf2JIMs1Bk/OdiaAss3K0E9r
wMzBrnP+i9gjYlcL+KupwOjlXktXUoI1nFySPrSVP7aUMSffJIxz9/3mtqkotvKnEslm1pY6LNY+
pYZN6i9mYD9qfC09vR6bjWtPj7smErHbwql6/a7YM09fJgKTuPHoztUPaGLREtYqhJ4sQab5h/Yn
FAcGf2Lpydd0Arl+Z4omMRK5AcHxqC7sHib/juKRsHaxDsaTYGQhfkZ1ujZf4JV7owG2vtGA6Hlp
/Y2QeoE36QbhFJ6ZL/Rx43F0KGXbuPf4sRtmN4MEai6voymB0Ee30dNLySoGUdZ3BThg/Wk1YeBc
TsXSnMqmeNJJkViEsgd4znfvyJji+BtlKQom3eCqP3xAPoD+8QjtKGycMRDdPYo0jbVLISP7y3A3
MAwyDU3FWLgYPB3wTVFpOaGS8nu3VMlsLDR+3nzOXJYt/i4/jgAwBVz+XbdyQcR40LL4A78Fpug1
vwAjAPWv4M3U20he1iJL6dYEzNrzcdTU+N1Q290Dqmnp57/JmzPVDEoj54kvSxjOlNNUcjKitakR
0evzwJVSx2duSYrEgNBsqKKVxnzRpcNkohjGkYqqkI6IKsV6dF7IloBGIAxyIq5KLs41KSk5IYJB
prUpe9o+auB4/bl55WcPwSeAX2GljOR4nxCftXlvCtSygBh7WRn3AGL/m8QPpspbrSZIFLIjpDU9
hQFePbk7bWh9axHN/WUui9jjqxCBk98Xdbc42gdjWBP/uc5/Fxz5LbOP2bD/gghkLzRgcIb9CVDo
qT991gX91Robl1/9j0G/27B90I3EUKYxiYQM3IIotYfC44RRfQg2gCMiAG3e0v1LZDWT/sjWgo9v
GF9yoO8ULUMOsASj2MF/s+gTvQXlgB4lTZzMDdBkjbjCym7sV3dDFuNHkBCSSdhyXWjrPWbI4y9d
Q0M99IiGn4Whi0jbpdYPOsdkIk+aw4Pc5B0Wvd3R/azFN+2CSKXBGQRC/AymNPw5bMkyylnXJ4BZ
pdtVKczL1yeyfKS2aU9zSx4XRjKrHsVk0oDosqcEy3x+l3Gal4ME5tqmiLpe5qOwweAU4OtOpBO5
1gWJLAh4AKFuPiogxuvIA/Wypx+XoZ4SxjW8G1l4XhyPXWo4P66o8pMgY3SXHdKet9L7pAQTPhOU
zdYyX5sKOtk/BXHdCyk7l+GTSjg1dxn3PmkhvTTN3mg69E77SDON3v10HSmE+LuGV4MT+aSJ0ETj
SGl8oKiUCJo1hh56YaJ/X5UoT/p+80RUb8CZbcA/sX1gWj4Mr0AwiC2acgQLwGYGq6lFF2AABUVv
5ppdQJ1b5EoKMADvme3kC/aEKIlyjQ4iKep7ol85heyXMYILRxPVMPef2Sm+D5+KRVAz+ED97L1M
s0rDAa1z8ErOqHaMtkjn7LW4qCzEJ8dMPB/MKjqZ1n79UV5BtAJ16pJTzbKSLDvXZJtbidxieA66
jKkscWQD6gRWjsGO7TSAyiJlfWCbmzDygzPjYNtrsTFDj3N0VAyrjKHZ0PcTxYrCFcVwNEr8/HE7
5rvQzR4AqDHu7IwYfncejZ40HeqXDyOFq05pjYFnk1dQjsPDS+rl+JysiPnWLAfrN/PeR2N4W8TM
Eh6B5igYyhOlVoyrBBNYTkUat96x2mmkJ37pLi76gH7uZSwSu2JAQxgY36h/PEdNXmw3a9UVZQfI
WNLTgmBrt0v+eHeufRaOogmQOLLn2H+pY0XvF7QwtRav6fZbWTfQSjsR9seiA4ZP4PhoMjQgq80y
1BL80RUT8FdfcYKt0bd/yaRBns4WNvNQrZ1nVczF6aGuBUXFqUo11VcW5RQKdwlII2xqmRyXylbB
t3Xpzk/URZ/zvrhFeIiB2eEcm4Cuz22Qpbt7zQUsONASL4TMBdESy0WqBnG62nXVrzZRtuc75Xqv
7iLLviL9aqd5zLxgIfuKcJzlomMR5XS/jUjziUC2YvctUYEI/o2iVrODmhLVQ+3pbbNkbEVTXacm
MFd2ywouKpeL9VP3I9IOKLmRhSVLKa7Dpv+7je5ossT3ynhsMLFRxPw+7WLPfH6+Zto9lg8IizHL
MePwQX0AUAIs8hsck9Bt1F5AzglNIoKt21gIfHXXThOTR0hU97c3DyLy8fm5jdS8ZmAghkKODGD0
77OzdBK5NLqZV3O++U3UOAg2XOGvWAjextuEGsFbFp3vn08XcnQ+bI/Iq/dbxnH3vCTnAPa6OWwm
/D4jaW+wWWonyMNXei4n/Kbbn8yS8BPYvX3M5+Pj3xygcB0/mJ1Q6zo4UEPovB0GimKhXoRV4xo1
ANVPDPEP55AfRGMPHDZkrndGvp9OUQ/s/7zmXQqaQE3ODzjH/BYcZWtoa55fJnzBVnKKdbbPvN70
qK8+aQrkNQ36SuwT8VcP4pN7q+419Z4iqRJP1zFEnk7rKTXYqwkBTNC5iSB5HfrsbS9yhCJX6BGP
E+cqiklKAvJSH+y4uzTAX5DUjh4jd2wWmEUUAMC46xaxkyRkGAkEQdhmyZyAAVR5+o68uYoLpth9
hoVyUtcBUPTeapyfmH58EUduBxu4RwANvOpsYS3DDki7nnKJv558ERA5yRiWnDIyHlRsJ/2ORiEJ
fW43m3g8+SIDLbMcLNik5ysK1tZh65Z3nTDfsJAXOZmOwQ+UOazFPakSgSBBQ+MA4ub9wBuYEcjJ
rHfMp/pBPZIXAyEYO0lUfEwTFdKk1jSzXeWIMMhiXrPzmN7Ytqh7KZEXwRSG3UC9v3QYs/XHOpSr
RLtKJPXe2R2Mir2z+ZqlhBOGKTQzGUF/i+FqIGpQO9fNfLTcIVj3KpYgMvpqnnk1mYx/Xj0W39ch
rH0x6oNWfNJyO1o7wCaeONMVagwDns+l8tM3Hk/k8xMl0KxymqjGl2DJ+qaOQFxnEvt+W2ksZv/S
+gwfEkqMNPNTlUgN6mjfCNJpBXMjx6GG54wCTGwOGBmrdm9Qlk+6Iz5COFtEOPRQ6BpUfhQ33Xum
GWqEpulPcl6Dy8YbEBuGyNtwd8/EYyFoBeaJNGjHgiJ2PFa/5Dzw3mGsDx5Q49PSeQcWbIuMcj1F
Mtk5DiWcyXN+aRIWBOtsHV8XBpEOymDgyMBeA7Oh/cGLMrzlPJumKcpHXtXl83uj7BuDHg/F2XMI
DSFwQcf/UItWjmk6TynxI8gSOozXJAO6AvtNS83gEmC8/uHU8Q4Lg6Ueu2dFG8rLkIc3wVaTx/pl
zX1Y5EKpNFWCcd2PChKp6SNlPXsb9zAdcG5Q58o5tyTZXO/Krae4Ajt9v7CZPTZ5UR3QhJ9/x1F6
zgjmwEub1VDscifbL817NDuUrKUEdtY0bYeJsu743r2J2uNOLcBEFLBvfZEHTqjYR1DvbmtcCL8K
OC/pzMyN0bAouG0XztDHh3HOrsxfAeXljxdKB9d4hVIffFjiluKw3gjaiahPgS9on+VkLH2DcErV
tB2zYvgDNOLuV8g6vk9nNWj6uKRSlN72k46KWh7GDznITvukf8PRcqA/4QSuuUOnarGo0YSg6hOy
GiqK/1rBOqY+q3AYmf8qKqtn5EccVYxsSGEj7rWYhzkBiSeiwhMmNnyxAxw0ylIu4Eqp9oaUVyQo
OigNQOWhnYSV/29nVyVM9rlhKYD+iJeNOFeBybUSTXLWDjIEI9VVA2YlP84kbIuWryUhJ9X9NO2S
XnH+ZdqR8AYjFjWYsshx5kcfylfUpCj0H9y7fGwPcv8saMRH12n5wa7jhYJwKMtkrpTt7Ha7uKSb
6onWniP+sePMIoLow4tIjzsTfT8TN+rNU692AAZHSB91fXTbZmw7rHnhk48fKWYsIscsy1Gx3aba
BbJN48POjuN1Mz5Hzvv3DSMhQ8VOFDAKD+7bblBOSTcIbPwqJeYojdy+9D2c5aiDU3Nq7oWXgVnK
Fz2NG21Mq8jEjhKxKPHGqaq8XA1UBq/OG2WN6l960ik6oT/Ul7hGWUTQGlN9rml/ohyXirhwzhRN
em1gvQjFrYFURb6wM8wz4xt3Pi/5fkhmD3j5EIvUgeoOcRvSApFpwgqFY8Er+p/6aNlsx9APFjka
aF2uf3oVL8KpEFPyywWyjG+s3nGNRR8+8cgUfsOy4yWRDyl5+x/RAs9toOit+kLY0lBRI7uOJpHk
ykMxs0KeDACxn3181ZfuX89ZMVtTMBT5qZ27pFAa3/lo4jnzPK+W5OP+6gVmcb+c+y1qo99NEM+z
NrIkk4hJSTmkX057uMkYv4LWBN7icKCgutOwt9GMssNn12wg+LhjHcXkBwpOWPOcke5eYAKmLNCL
ezEaxyj17te6BcnKfF9gMc/YrlDEqJypg+LHe6qmK7sBDEpTffpAHYAMkWVuUR9KKadAB2vX0rLo
ZXAk6gneqVSlVCK+EEXJmZ7flXDT5YnyVR4AFmT9/C1yrc9sPt1KuwdUG6T9BG06RnizCtLM2puw
0x/tJIZWGZyWo0iPde7Cjea0UFxAujLxp/HGsNJZjrTf0V5ZeToWA/TEEhtHfqaht9iB9oCUEjPk
lIMqUuXfsP+NHMVpnM8AbeX9Xa7bdT0kl56VAxAz601LJLPx15rPhkInfIfp/W+Y61ZZJCi7cJcn
Ka9PTJnqBPHsm7J0wTA81gHD/RlFEDepU9iPW5e62NVybh2k3PIhp/mem99YJ5mpsycJanrt5T5q
mzjz/VeUG6diqVsD1yJP0coZE/g+EM5XKRau7lqzh8ozyew7CRNvcXCsEdU7bXqGLlrUwPaAs7qv
PVopGx9Vc3cCrMWX/Qr/jXKngXhBRZ5M4Sx9/npuLDi7rvrXv5aTcJn3FcYCSAhwNWIfjwsS4YOB
khtR+vLjQvP9E6P2Vetp3DrMElX7maav1yzhw1D/QyWkl8Ug8MeE4geRFVxffdYPoD4eQluuPXhO
zd26XtwtKa9d2gFEaZ5oUKJaU7jHPjTxODDak4ML/Vmujca0Dj7za8jm3yg0wKMQ14xgcs1qPU4J
WV9r1KFk70uuZ4u9ZygVHh0b4cxJPtp1pZM68cHgKF5i2lekr7Gsl9kQzn6xOPTXvqaqyf5c4MXi
2z4Usofw9Rbrid2F00OGSyvkvCM7A9FvX2G3xHF5QDzcnY7YTPiy7HEmYJ+mqG4Ah9CeRKAd7MhG
HGGMLRZ7r6qcO11yWiQyzKnqmorLcpzrqNalNj+MAiS1pjUFTGjkIofvHgZw566Wze92RLifaRCg
3VFITDI+JTM9A0HMyLQ1PeONopPvao4OAuGXRWqZrgvO6oVLUsJVaRQ2uDgPjSUlqBhEfuQjvdAD
A7wcqtZDtvZq6y8GubPFMuF+dKlQj2dBOWwxi+Tnn3YosCuOBTHKUHU6A8YygI+Mdt8PMDFJ9lnL
hwzQoiVUpioLS1o3cmHvtKgL0Xv+tGAyuJpf4fvQRs3AgYp2jnjUttdhgPJRMYRwu8eFPDEpTa+o
8mJy0qoDdRbkcGsuz7COlXK7Iyw/Mp9571gKggGdf6VzGCHvcrJ4kv8uPbcfw1bklFS+OJniu9PJ
bA5a7131EZPUwQwMUiK39KkVHqDfZxtqUB8Ns+UkM0fswjsoXcZyhN+oDFvZ7vCch/EhVPEq6Fc9
Mlj+7kBpUUtDEYTB2CMDeMDz9ni1dmThgXTSDi16VjI2B9jv6Efc0V1KN6VJ/PMmFNuAz9d82xG8
NBpiLgObv4rPopbCH2Xpnwj8Ep49dv0+9C/GWZqlQWAJF1vC80yRWpqF0cmh4NmPyDuPAAVE2wkC
1+zi4rb4NG5Z05oseeHCrFxAVSW4uVsF9zGZ8N26t1UiwcugJfFO92OokAN60phI59m8q6L2He0b
eA0hn6tjBmfASWs4jJd++barwJ9tTSR8OZNCqqlNVrLsTSFKkYQuHzct6MPl6TQ+cpv12fu0h1Vn
7oXEtKGwoBUmxFe3lTkgxSO9t0fwTtFMgDXwpt/Sa3+vXmjfvFwIep/rAbNKRVTCj4hQL0aZ22b7
r7NihAl/Z8a0RJlhQxYe+NrkLNG2dX0tKz5Ma2bVln3+nTbsum07k5ZteQeKasa8c8Q8UWqwE+uB
Cq+Nn4Tr3MqtGPv2cJO/wmPDMT9sNYliRtRE78yeSP4uninEjEOu71PNG0/eOuXKlYnYq6sEj5ki
BcKdJAk4p+2qR2mwptoVFyEHk5S6Lo8g1Tq9XRQZWKwTmvT+mQbf7VZzmIqhPcRtSY8TIrSXA0MK
k1qIHd/ihXid2uvwZYJj1m0uL/f4dHrupgwa6ne7wtSlpo7+E2ecbdWjucjxgllBvNLQdInzjRaN
4xPKillv26yB/phTn5wbWTSHHRsAsx2C7ypRqO81R/gOM12oUjTNzVybGwqdf62ltVpcWpTl+qOJ
Hjt23GrTg/GYEoH+4yif7/kq04Y/ExQcreg7eGq0SZWUxciIKTnODSjOpQ4cxJhIwX9tkSF9hC7k
div3Fp7Oo7ZDT3L5bPbxb2W9PcJV3viFe4X8yMXmTFEFVwSh22rpQa1EosXdykuIOYF/J3ZukxUR
hP0fFYDlj0aQ6Jngrz+fuzUMTwqjryOWewCOxdHbfkJW0CCHo+RYCNjIIfaIdD6n5CQgLajHaNRL
fs1AumGiBGTYvsD9I3Yeig540GRzEDquHbHlMHATGA3U8Vvn8XkikJv1NwKZtUPRp9YCL7nvVLaK
zYZJRmZiAz4Fsh9dwajp1iK+5RNmsmql+9W0nm4hvNY2wRlhZofHEYrR8mnzHyRE6A2GJDREIgg2
XXby/0cxvCIPr26igPJ7p6Lw3iNHeKsoG2uqLiLsAGbcG8ntHbBbziKX6PUGQYjOISfOsFcDA9jG
IEC7+omS8HM42oclaWrkfk4CFXBLypTbMkO9DC+/NToYZspDVW1Kyay5e4BY5JS1QANnEicNre/L
C+7k5Y4HCoR6cOlaIr/hQu91w5beY998iczom0gjwhaYQIu1sM5V8clYOZrrq/skBSgxCV6QJ7rH
cHBvDCiSuULo5hNl7ngP27/d/iT53aqxH1Ha7tuyoI/tiJtiPe07Uj5cjOsZO1v2bYo2kQsY0KmR
DJT9t76bqRPE+eZOq+uJrA6SdWBVrTY7QeX3Y9SaYdVY3y8yoOfMNK067H30n15URyvZFGf3e4Mm
iuRollJdKYBAL6JMTAA8ygQ7JvlZANk9OwSOPEOJ4h+7YDsF5jSnJkwCWsURCxyGmLG7hQKL1jFD
W5LJ09OoyomME4H6Haly0cXUsVUTOsiXBMjyUGZYCNa08KSwaGGFFlOasNS1W6pceLRIIfyGvIiK
MhWR5ttthfLvL2IkAf0CuYVdjVQLzqrQVn7f1NBFPosdD8EaX4gWg+wsXuKAX47pgDqwFnpBjIOr
HFtraIugLUoRJNvHYMM+Rx6i3yqE61SULAij9K4J4XDm18XSiLREEqLRZh3rCDeQBa8o4a4b2g3u
W3pSI8H9WNerqdsgd3US54goC7EGrJbrV63p3Gye3pa1ojFWcJmd3Os1m9Q2VliiokMPjib31TH6
Ekte7aC0qjTPdtzXF6UZaz2KvDkBbwZ+Ye0h7AJAT/bIjwAuXONxVP/u6oXaBvTArSttXmah7iGC
8lrGULSnZlWe2SRhxB6ge1PnWgVOQSF4ii1CpswX5b4SaVs1xSclDwy26YzsIagOABzrCXeiAx3r
acWL4DbMALvFXjdOlNH3/AVjwtpQqCxjrZuJhMvE00yW3yayGPl9cqB6MuTivywJv4UURp21LvuZ
u6o6PEECrQ+5BVE0ehckxZ8CqZVKsw6IflRymCpVC3deFDMB2b7s0OC5PatiIdRHsTLsUaFF5yhS
O9c1twchT6JlyLtIODNgSK1+PzKvC3H/VyiuvbHL21LB1lHG01/o9M5OUYE10rJApm6UlQJLXb36
k/0Fqht/xMcnkCFvywZnlUW0iib34RWgRlnqYN/KquCKbaF8W024jTezmuwV15jogufHqpCc/kuV
xKV9SHGqzAVT+FPZFhuA1GmqasyaQGHczfHW8NXjw6LTnZYvuRJr8TyhqH0AjPnYnE2KH+cFD5so
QowT4xgbPcA+R1MqJQ1KO55vDbwv0SK2rr8YiYUs/7PRexFqdMHYBamIPdLBa7ymXJf5lHzJ9Uvi
8b9XMK5SlTAZy2rakr2TAl8lyoLIfvRLE02EzlLZ+2ydQ2pRMKid/KVy8+fkjOoZy4vauvG7PSrW
vzheEskU5bPksBRJk9lEUuGffVYuFDcJb7DT8jRtKiIdyJkA3iEDvPP40F3KISqouXGw8naXYpMj
EhC9rs/Tz6PrUnIX/l9I33pMlBDoaZfl98VL2O76CSgUfmL1+9/Ej8VGbycTJceoZv4cD9fsL0fk
d80AI7jQpVz797iB8X3K0jsdeLs69qmL42tbqRmVjNGxhlkfHIF7exIhNcX1MMBYEqyXPlvaQLmd
71BwicUVCmI3MdRQe4EnWujK2dj47CNqzz5kbOYrnUWYt6CvmQmbz4RVot1fsXAPFc0GI4qk81km
+cX7lCLzHlDFXmgqLfJm5ieZnaQMCT4MxkdoSIoBHm68oWtOrb6YunvOy9PbqrefIIOVE0Ekujo0
0sPVDepxY4gK+cgtqd58fbrMb4Jcb0K/bnRlt5pVHZN8eJJ3wlO4BO/8WwSK2tavfw92igG4S5SY
c5VrxRzgfGHXiq2x7r3KqGhKotEpZyzPy3bWvBa/ESZKhSuzu5pYFThjGXg2p8TUB/i6Mtf/GgL8
q11vzSWdoJWWHozb+zJ82ag8WPgJb1j8PrOK82zjK3+PNUMeb9SO2E/bLSdths5eUnabxNkRI1ll
LukXbQ/e5DwCYN8a8GS/yWahO/+QStE4YAMTgu1Hg+jj5NiypSPx4G/TvVc/p/ciDhf3FBSqaWqn
3TmP6lWeVAZ6AjVDNzuHQkZA8SNkl32+laPxchYYAmkslOmOigRfJxIMAcBbCzx453JZY8oa1igt
TX2So3A0cTwZ27euZTGfUkoYvoihf7/CpUVTLPnw38HiqxjnmVEaVzHT+RIV5bmy2WNX0MfrLPM+
KNePqFo6i3ckvbSjgU7pdV8sZutc2a6O0GS6E0+z4guBkKKNwWpspFkUxxE6u3GiT5Y62aInx16b
g43u/6jT4FAd8j2hHg3GGVIRMXKP1B2SqjeilmqRP9YaX9j+p/JlehxujUVJFAXMAVSxzV0OEUEI
tu/wjPsoh0jKuM4EYoMdp5ceCG3VuSQOMsMe+bS1+HosrULA222qL3PXrsZcdz0odkP4n+p+yRCm
cV03BjmjMjhp6AD8iVwiiYsnXy7cFi/49plwOLNQ1UV0iGIdYdj0yTWZxszNQdLg4t5EHF5N/Srs
WF73cfx+Qn7724VA/ZZaTErv9uj1TSJzBH9FL0W6EXAA03ejZAkxXMnlNjeBUZqyTzGSNL66X6ZM
/jvFk/zGxExZfeX7lM9aSvyorGTmkQw4iBNHdtkpdtdtW4P5ewpROk7pG4DCipxVzKGSoDwS8eNL
UCz2KynFyiceDWq+zK6uqk9lsDLXU00MnuqBAFXFYAIWzDvzoqn88mlyQRlr7/87b9L7d5LoDsDe
WngfYNYs94GB55XI2lTVK8hdm5ACJ+O2EVlotjaDyzEb9ZbBEXrD7JexwGt8KXBtKe7TG3rQSnc6
EtZGc6JQc1zNRkGO1pKM82hrgesDJk5mHK6CUL8RDrw8wFpsov4JJbm7Yg9/oXbcMQlhgWtoTD9E
poPGkl+1H8kF2jTBHE2NoVwEL/9ngLIioSAXIrx8+I2l/7zBJm2XvFomBA2Ot9/zYG6HLxAAugCy
aYe/HhtteQMhdqifoX4ldZX9n/nVso0TLOD4NoB3k+bzJNwHsRu7LS7kdsFGPzFgiNV85F9w6prp
tXOgyp0ZudsVnfIvQfABCjJx6GXEj2a4dQPJUXNNOo2j7/ArmoiooMklYJzzxC9eYHzcuPh3KyGc
o6SpeeAi/1Nl+BjFP7Y0cjb1ynvpx2BtO7uJarSP6JKkgZmEESiXEhPt//Wzw14POSf4Cn0Taj9z
/QfZcCHY+awEX07HSzeAZXVDnRqNwf68oD/ucHRKdYOZv2eg/wkT5vIRMgc3uFuWROgC7DbJ12eX
G9VW3VVHQAarKGIb0TlFCF8sPmu86bA2IQugFRCeqQz2puDA1rj2nHIqME0PbbjznDWPj3jVKBpb
0XM/6AaNeu+jM6mLriPXtuh8cZOAUDg/TfJ7q6TNF/9x3Ye2sBAOMjuQz3ibMG2VpONVc5RXzpTf
aA3c21Z0nWFbpn8vFgtu3zE7ZyD4AYIm2J8YHO697pYXtEc4gVhQQzJ35vaLKlofyvYQyHxmcJtP
X1YCO1RgXq9RMF11qKwFpmexuuQHkepDLsHJKu1bIqyZRNeMugwmy0ClSJT6Lpqelv3cjoUpEBYP
IOtioyzAJW31ooPWhvBYOgQTM7wtpixc4ex2wwg9H/lnkoaueddvSyqslh1AKrEivKVHuy5jJAGC
wHNORNxHuVM5LoYXFlAcJ3UPCw+Wfb1ZD85jcNmy5PuEtNyC9nd37edbBSB7CmSqjJT1JJrQath0
by0/uyoLznW7b1CiieE60HTVK0n4fCZc2cZWne0MEVy8JavCwINVRY2tQfivzBgOMgaxfGNQ1drQ
BXg2YLFXyQSTTkL7Kq7crupFXGEpytFVUOvDahKkDJMZbQv4oX+H7yFXgE7yeHxNypuWMD1Hfzc8
LF7g/F8yFsEeHndxnK38ftzK/qyysGewooDUF/cZHZjSnFYayIz73vc/GJsYL2+fysBHQc7KXbgh
dJhxScK0wsNbMfyHM8GfAieKmuS16xabUM7aClLpTUVR43+G6ERUrAt4Dsfd9xC23xPksA6cYmgd
dkXs7bkTx6b6fi4LAgybkSD14SKghkbS4LbwFBj3Pc4xAxEnb8V/2SYxYVlwvJ99LWLIZaPcG4uV
c+jw4Xbr6yOF+GJxhn5RWSP1LOwISwuyygbmITcLtG89z4+hljt+guO6fQgFanhrkgI1Yz16NZH4
Xl55CanrqeHp1k9F2muLFtaRc0nIrSRNeLQf9feBmNTkNSK5qbWd5kr/9QG0MUGPmKYPvnNJHjKG
qFTgB5NoG8HTN0zQjTu/Q0ekoUjHBS05uzl6J7BJtFtOmrRxNad1ICm9m4XdaKVVFwHa9mIVtFe2
GG3XY4ESoWrr4tHzKewY8IsFNSGLrpbdjbt3KGkGjl6dJMGpVAuA9qMuPBU4Cl21GQ5a4E6GIqr0
/ObnHyKgdI4S8c8w4eQGXnhwXWJBza0UOdYeBrldr8VLez1gYVRyHPt1SRkhaONZtDEq1ljmX4cT
l2dnklTNkmkYqyhmSrzlTTy14x+hj4h6xAF/zvfNJdi0ri0YGJdxtZwL02OF4bPsHXvV+dkhTox7
i+TNxMQ8f1SFuosfaJMPNUjzYL+UluxAg1Wq88llRUC5Lan6yiasaT6KPwOqygKPeoJIcuhLZ8Ee
3uz8LOC3MIHFmLtzImKu0itCvKMGDvIoaKRzR+bjaG4xqOukpACDdvEbRvnHtP4R6WtOyNlkgNNw
84s1puJLRFQMXL8BUMZ8iTkSJczhmQ226mzYEQmexj3K+xKaykiJ603jvYKCOOteEkESiF1M3e0/
NlNNr4GvpJVENL6ZOIepmGtwuUXpQNDFWe+U3QBJkdN+sQ6JliWPIr2859SXDfPr6SSGeF5mA7f+
QPxdGKt38NUAl1r2Mbp1b5mWFd4waZ7ktR1JwYx025Dlonugz9mf00xTNhCM1igHdU4rpOkhbJlw
kLAxl3hMIIUfeJ+Uv+Iyz2v/PIiwOOmMMRcLpQRhUUUdtBQcvSudAjUToGS768bM2eCCb9F7cwDe
23PclzWbpI5LC9pi2PhGO8WQXvj6Vsrl1pBe9HLbs2/V3a2GR2gwAVdW3izEUog6x2T25YeV0Vkw
bF+dgY3LtFiNFmLRV360NoJ354jY5OV9LbDEUEJTiilXVyf7E2zoIxTGpDGx33J+oKBzGNmWxBYw
WCmKeZ/UaChYVVojM0SZSRbPccize8+py1fA6QSQzCm9FjEYW/QiCzOYBbeSFb+5mNSirrcdqXez
lkFnHbCrxF+Jzl3cyy1l4SQc3FdAHl3swhm6jQy7tuDVeWM5PEhMLtrwzIGESe4i1RzTIyHeaWqD
1A044kZGT7kT99imjQ3+OMA7rcJb9VChRlf4qbwmjouwZuUaTvCHsGEVKuO137DKBLX/jYOdOi+d
9bRdDj+6oivVOqI4eQGPWlQD33s1RMveu6KOP+OZPXjeVZKwhVrh5NONAaBsA4yoK7VU2Fj/lITE
RLMJpIzvMXi49oVbckPugc/ZMYZuT5IMSaHT9eLVw1HZ2KOGOR2MpMJnAgpSX0tge4r0lSNt0NyU
4GRvYkepmEMk+BzVjAOxntUSSh5AKSqmDulxFP4jS2ZUePxt2pT8W5Yv4ZGdK5AKc9J29YfeXCUd
t4Lxm/WcliYAkuibHrZepXOE0hxVAFpL+qzxezNGWMiJqPPvVr3SvInIRWe18H8VaT/nplxoZlo4
i+sk3jt+OHYQl9t/ZTtMORvIC4oimdD2eK/91hHdZEcCkGhPOSEJlH+lhtnCEb9SPDK/rLzLx3FX
NBss3fet5nzXEVsTZ1IQ29/br8mOTVEXbjMwelF/YSZpl8vZMi2cOg9NtIxRjlrG4N2LqpTvf3+R
31nD00As6xsnnCLVYSTgR8UeObK3gr5hhzSM4hCnCCPpPHKu1O0v18NypH/M2fJB0hOFGXq48xmg
3rQW1JVJpeW0NGyALSiqgd5D8uGFbbxd5w/DFOwD4w/j/DqIRruUsXoSkAYAn7MpFEdBaN9/CC+1
W7OghetMNkzM6zBQb3CTwEGC1yrosmxSG11bW1WYySPJ6AaVMwSvzS1EYycCwsCshDHfudP8ik2U
Enj0X2NEqvwkAWkL0IeQcCc5HqJZDiJ8V3SUqq13KIXElxfNKBhszXwYSj/o93RwWf/gZjuWEhBq
0uE1zHl0RH1ojVHFNaCcQNrVTeBVePlbO8vNOE0StvRysL7LP5OUqA10eRi3xP4We7TUB3HpCD+n
77+eK4QJwS6EfGoJ+AujlwAX8p86PhPKXzZVbKggop5F31nM3z9dQXB3Pa1iZnB6WNdGFe2RKFNC
PuNd0CJL+apyaeCos2XapVJMTNtpk0ZOFeXwLsKUc+9+7aL+47C+stxJ3Juycy+yMhLT6EO0KTt5
CbD5yeQO/ECNTFI3e+NUVawv90LWuf9IHuMxRuDas+k6/n1brUstvUX832dWg+/5hGHMv5tQZPjV
UWk87nPpC+Jnvd4leDBcJ8pVCi6XWDp9RoDeVRValuq2/3lFQqUR2PKIfVi8MXxoB6p/lVnwbMU/
KGSWzi8pZ8f187zeVN8Ha5dBUbcQRIeg8qUBqo333MO2eU76jzEEcJ7zoh23/Cd1tY1PPYncCP6z
AMkWf0d4j8t0VeYQpiDeoV+9lp+c2mmuZJljJn2gcoTI50gQwyoOP1UCT50etUBD8TweQ3EJ5A/G
oylPDVs4kF1K7jKGDPEUCLkHFP9NEgwFDuyzSVmW3pJpJ+1YZ2nNfde6oJ7IUzupR5az1c6CQKvF
FkZrp30g43uG7ICs0zQrRoSg7nprR9HZnmE7vOBv6Vs9gjGAjzcCW99/VanE8KfHutgUt8Vx8Rwa
JJr136PJMPTNmkXzgfonjvXRKntVCBeQUAFzpprtuof7pidn2CTbMsjjRWezegfiwrDNcPsN5bSs
/g1scMB+ymDFodImuutI3Yebg3Wm0rKJ1IGgUN9kYFnRAvv3rOxFf7ytkCMuo+aLMfElrN/VfzTm
40Hbzi1ix2Ed1RfOQzJ51CxdcNQ2VwTjwyFTSxs92mZiRgB4LqDdX7AdcTz3kKcs01cFjx8jP/Go
AaD3Odew1l4k7tpQiK0x9cfT5B7vFXlZqGCzi3nbb7irldEKpT1jtatjhgUQsFgnsox4IrEZtAKq
4yq5XfygdxeK175iuFtZSOAPPAuQB5yHpmScaW+XbtIvxS1L1Yd+sUzBNK/Ae08JLIJR7+RUB1Hj
RPiAoB56/iPkPhhlJWHqrHdX9h28GiAQW76EouA8wORAr7XG3+G8wxuR1wIubYL/qujAFJ2/jjy0
U7TLBIt3NKjANfC6tpkywxn99fke+nQxaTqyI7ygbIOUCMRGgN33ZNihW8bEFntuR2F9gROMMXrP
QkWCko7XaejzWwZju2uIUrgO9OMNayHcvo9w1wDNhttgzcrfa33g/xf9rhcka/XgK835WVmyVD+2
HLO8/YnzWYxIGtEVirUN90EsI7/D8rEmsS/LrGnqbQ3PY/vwq3TCWUgEDO4QHEhlYkPAJ748Gdv8
3nDeyH/AXqSkGWC/NVBcK5uov/CxlS9giHIaPtiynD6yMhYytTj4l5WEM9YgUSl3F4cdF3w5CBFF
Fb/eaOVWeOkjI18izCwEwLB/8B4Xmp1u51XhaZTSKi9NAFnZ5XhWhEo0fRkn/U57ePaYIloCzxoM
NkcLkOV2rDSq+4Wgx2DD7MjQ65grLXG37dEaU5h0Qwh+10eanyC6icpkyLjpVHYMcFFUJ4LAJbhP
i4dSNKgIvjKlxiNT6/yMA/gQ6O9OA8MG8wKfyLZojAlRJR8EdbxBcbtyW89vzNhm7DZw0ElMOCVY
ZNqTGTv4v0BkEwndf7GwXuEi8tediHkFPgkC4uxytBrhWUDLeYX/P1u/zadPxhnskHWrzPBmAp5Y
/1CGlkwYZNIMXirBpSSQ1m7mW4p4RCO3FOnIf/22OUomp3OGKlY20Uy8U4woNMCmPDOyrYbagss2
dtJmbZbgUWe3sGHJ2/XfoFmAQmo2PCDlEGB5e69LD9TKuivJx5qEJO7k9ZyxANaZP1XIYZm8GLWp
YbufC6hwRbmqIiuk3mmBEYqVTbaoXd8NFRKrxyi1+jPDasBRfdZOPxe/7Imjnzclt/l5Jr5kA0JC
sMoGnq/Rsf2gEXr9lePIlx9BkSqEp33VB09TJ8hh5CgVAS1pmCgJbFaQEIxYAhOUMcb/5SXFESc5
w26tHfdGPLwWMLG2kQi2wujllTjvMjb6AygN42hZ7WWXsqyBu9/7zGMUawI3292FILpPDhnjA2qh
C+wy6ETAMSWJXpzIDTmcwYxT1w5xVJfQxK/ZqY1RVCZnh58z810Ms4rGwtpARN/B5ritKPgI6I8O
0nBK68fuF5yBfQ3wgABV/ds0CQcnBMdu6fz6gMsuXny2NT+96YM+LyxQk88D5X8JChUp9OEsvklk
3+EcVa7lkfU0oSoDOgQ7L9S1ACmnIieJOV+IOJPCpqP7xy9ddP3+BsdXwRHz+qtCMEkTU6y6rYjh
xYJboXBLh0PHMe0wcnuRX7JPdO9yCduExjeUaiFDKwwnylg5AIai/vAYtnCk349CdImUkRELy8K+
6KW0nwN67sIZNNIrbkrSEenw0jf147zZrceo1y4dlH636gcFE+kM8Xz6faJYKZ/BzGLNhh5cIb1A
M6ISTUos8ZiKh4SFyfhQpNrL2EsFoe7d/Xln8pT8c0pGTZxuJnlnWLo/MQQR0iY5VKo+eulcxd8X
0BRBd4Tw8fW1dVXT2zyg6JrXbFE5dNyOTcWydMnuQ38Ucl+opRB2Iw1gvYGsR2BZwTSPbgIj4fjN
RgBiCAYPubKrYavKH/LCbO3B2HMWdOh5yWU5WiEkdHwGoT4H9tEld55yUWuHOYyr3oowva5uIDbs
/N0Bo2XTcFonHeNhx4eEUzLT265F1lYS+fghjtVkl5DX6dtqW5Ea7ByEC7B5KwaFyfHUk01AcVqw
F/7wADdKM6hTbTNrSRBPJKXhMN4UkzCytTrmZoqpMR+oRud38t7PHAk+pOJLbhOWoSvV7i/mvMkq
sjHrQo5UJOQDFa6USKFWxyom8XzmQ47evAmpUZV/lY64wa3OTqDLLsyRfuNVBUElcqTnOy/oX8hO
hXdcmPFEBgtNVWw7ghxHeUDv6soYen4SN0tE1Uv6ibrV1Qpk2v8hhD8c/GO+8Uxf+z8LC/xIEc2X
/esn6bg7PEZyiL2OiY34ltx+53BsEmFgg8FtknMiIPlud8gxmQ8fA9heNmo57kBhhVpp64DWvBQ2
H11rgP7hC0PvizJ+m1IRJV7U9kvZWlvn48UQXe84DYUqaEPkqIcaw9czX0k4P10piXS9E5cStWIQ
9xbhDGMI9BwuXCQfdq6uLN6bM9SinE5VmwncwyIMtykWe0BA9HVpQCUlz0DHAR6txgAxeYXADDFd
X6O/xSo1H16BO7670fNcIdGL1HIqz+T9ePLAMwvcmbIRZVc8MFjR/ydZh+QeBhDk1O3JMdXKK2vr
oQzDeD+oWI9ZsGNKDTTlJlwBkuEm9GYeEtXlM/CZlJed09ym1QS3k4XHxhR9cSpd7i1d3SiRI7NE
0Jx0tpaUy9D7lopIo7HvD1lk1jKtZAvo9N+th2I3EuOX6/f7BOCikP0tYtCLBGGKd0L6XX/5JM77
JK4/RawxaFS/Be3GXSIbj3cxDseoHMHb6xBejxZ9a1xhgQV9qf3ldLWbQH6c1Urkm+412BrbXiKg
+Qgy3q3kSePuN+uQPKPnUrK+ZB+8Uhk2PwrZSxUHDCZoYgWcywWV/mKdY7YCqmd3ouo7a7yd5BOr
2ma2Ez7MI5ot/YArkqfxHgh8EA0XSBEY+hD/g9iFdFkd0fUHmKBuhmXb6JN9rnmGSc1i/Xm5RNUC
+UI/+S04lArV7T6ZnrOPkwDP8QbSvxcGDphsBEyos/tJdBXGevBC5Bk+I8R2oIEaBHTrr8fjWW3y
QWknUKvucLEO5cA8b6fS+K4c9+xXZbd/GRYgKJVISbKH9bE+77zP2ygJW5F2vxTD1513VVUT6TQ9
QZ2vWigUoDr/TpPBad4LUh9q5NroBi24vd7L6iRMeGd6o/vIdqYmy42UuXhWNYNr2pgE0C95opV2
5Pwx7H2SdBWzNvVsrz7soH6cfrxvYpV+g8usd2jc4d1GOscqRIIBF2Xfelm9AAvaUUWIirpnzzZs
9nOti0/CmqIjLmKPfA08Y+17QsOo/+HnD9bfYA5Buw8seLxdlzIRcfYM/1M4I95d2WjkqBlYS9TG
32/OjPDxpmOo/xVvkMeocyScDw6cVfEEkmZXnldqPKsko6xHt2rTUOpEi39H3K9SK6asQZqyKCrz
/uXzuNUzPDXse2zzyGx+hxfjch3e13AOgXhQIXPhYN4BsaIigHTsllzIKrz78HtavjDtkxKq7gEt
xoHSsKzvIvSLhbGhbRs3UtmfDsJbroRT4icm40IUXEK4NrtkMa/l11xtrOGt7fLkMp+RkdARKMzg
hGp2oQ58yiPtWyzf6nAOeelnhRnHnQ5COyyJrbeFL43Q+2uCLgvTlIS4wqHjwKMNM4+LOHp2LGjC
Az2GZqUuPnaFsD5a/Pqnf9U6oEN6sB/+hdkE1/4VMRHqi5Hpiywgew+f29tUBL3ne5bJNMZA9bt6
t07is+nrXHiiZOMYbzqYZ+CcodO5WyOJVakb2XuDOqp+qnMHYhiLte7kaFLQEstVc9oK0YSo8rbj
aRJ9BntLHG90J/mVbX3iTOanc4SHiQiXxNDYUs7LlopLFuKYXaC0up+7/Bm1VHa3axlvoZRmKakb
Fu6F+d/h81j4WaQlAbWz3VxgYxLR0CZp6kxuurmlF0I3TT1HgdVU4WXYtUWjLKWZjae0qpNpmdtf
fq0yDz6rliqylCcmqCPove22Ejb/G8fKZwaFWd34VHPwxA17kPAqJwqunjDxH1vogex/MFuYQjgK
Nc/tPFQvCft8iK1JNsa1XCBuo8D+zXk2/3dD/s8MNuHO/hXAIxs6Dae/G/0aFs26DtbRBh1u7t9/
5T/uSlvhr+z4xdIhgMeFlpDRbAYpzaydmzJFB9fiqkQeOqqsfVWVbghQMupZyYgoqC13A7bNQjQl
nlU9RnYzLcfxlZaN7W+841rjSrvbRtZRuYFhukY9WYfO29pkrt/ivkhxnXgDuMgmbUAjIBBqhXU3
s8HdlRVn0eGmixNn6tfsRSViIjw4qwRqGU9/1JQN7yqB1KeyufsgiDnvL4OqJS6W61pltQDvjC9q
5wk3eBFuAkQa7B0jzvMkYG9hhRfaD2OH0neFGmtOi+II/u4zpNkW/ZhUoEQCX4eeiT3Rzd1GT4pY
3B+wPO13wL/XcYhOaSzawq3NBu0Q0vNuCvx/SQfnFjUh9lS39u+HE1WWWHp0NUG2rwL64hucBGaF
UqebP+pNptVI5m2aJCxzq+YXNEYaJNwYlAbxjw5TG+Rt8NjFOcGh/spVarbyrtHXPjUIorVrAdt5
vNvwno2vuW0R
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
