// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sun Nov 21 00:46:22 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ egg_mem_sim_netlist.v
// Design      : egg_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "egg_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [10:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [10:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [10:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [10:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "11" *) 
  (* C_ADDRB_WIDTH = "11" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "1" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.35235 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "egg_mem.mem" *) 
  (* C_INIT_FILE_NAME = "egg_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1152" *) 
  (* C_READ_DEPTH_B = "1152" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1152" *) 
  (* C_WRITE_DEPTH_B = "1152" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[10:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[10:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 26384)
`pragma protect data_block
0BB78wGFpChqfh2dITQL3zeh7rpf+uDKBS8DZaNM1QJSRb5Ybnsv6+4TDI2ak5oYKUNhXL3XhwWU
kOzyIFZx/BMHdlPbh5THNx3Pj/zuFdIUgBtK8iDv+Noeo6HO4++MtKnPCI2R8fd8Sdi+QGt2phmP
KgUszbjQndnUpkkPlMwrIsflPBsn9Ivze0nwhh1CvmoUsMqDhGRT+ZKpFkz+o3H0tuEyLYUA7ByS
gTGMjLAsgsSt67yLCZHgD6TW4LhjiKqO4xiHgt1uZfYNmSONZShLb1yFCgprZtBvlEepMRBBYE/y
2j7S0w9OwsJmpxJ4I8DUc97jUCgzDwtZ48S5mW/tMnF39apkZMwj9HX7oXY4GqrFT6r0PeDVVmy9
mIKVLcn46J9HrS/8HH4rjv18n6W6c4ncfchWBzLbLdSyv4hjDHsxX11eI8J3AcgsrPiFd1ZilrvL
5bYCIna6ykhzyjOS3HK0YbNQNUH4sJxcN3ZO/z5l//7/hkgQ6RQWQEzwckVcGx+W3UHQ/AW/H2zz
CmccPyoso1sx5Wbfm68iXLq2ge4KQXnw8tT3kVcEPEr6uJIAXHTNAItbMSpwsgTUyY5Irx1KQJat
WwK1qaavOLC+Lk8KIL5mEs0Uy7FjBb13XbVc7ARHAw5ShGYo0vn1Yzh+YUfnucyTe17JtBx9x2Zv
0uQ0EMhnoFWEQ1Q2PwdCdj4nNQGHpqYuj2SrxZcCSpL8NSepFQoE7xxv9kmHo66ffnKZPg8xr39f
3qaLFhq1oPCKJzhKx4u1hZkdsHZvaJmt7a5wCMi8jt2VeL9lwBlXYmr2QNtJJg8oZl4Zvis/KKXV
Nt0Zs3/hixrMfZ6lNMuhJLk46ZSXeug977G3NIM/Hj0+ILh2LZoJ+dg5bt5NjeIlUqmiKJGqUK3Z
iXcN6Je19FJlM4sVur+PgwUBsESrIcgXBaqHv+pH3atnOvqWug0QBBZhQQbZ5z9baqCduDrNRBar
8QT7FC4NZUdxZI1c64lQrRQ+XN67v8JrlkhTGRSJaVUiWYjbUmSfjPR7lSpRopR08XrxqB63UheD
3dSXNmFmPVC+YE/o6lX6/RsP1icRnX3tsecD42w4WRHfcVzqwOhfUChd271d1YiIZj5JTBEOPE9w
uhhJWRZBkRARjqVbU5cgw0A9/v6LB+CaDxwmvjn/THgNL1u+c+ut9aRaLv4btpCtQ5A4ywineSIQ
aPevuBG7rTvysJi3teTUZtFx6tKct/Zmlsg+wCrcSdSRbzmd1ixSYd2glBweTID+grLCVvOYk8iG
NzYZh6GgW55OoNLEhQoqHrywKyXQRIFTyvEGXDWtfz8zwkVyfxUOArAoWJX8vX8XPZjWodCcJNen
9R+vHouovgKcRZI3ZqcPgEpp+iuGqzBYrV2m+hQuKhm5nr8bL/MrSjc8tIK1NulCYVU+K5l3Owwf
7z9vjYta5ryf3rGvBxx8o10p2dctoXaEDQHAnga8vjtjZoJPyM9E9MbzqL5Ehxl7q1zTlIveaBgp
5JoJWBmuKlwH7LLGMHLYo4owoXlBANFEVL5Vuk0FOeTLY5mAhrf5vaNK239QFHwg7O67CMObId4n
VkzCcTV1Y1Un3xkfxvTES7kdNS8jPvYNtMXJHbKFPOUSfYuG0AmHg6a/Xm/gl77obf2VH4gi+AT2
NBEzowZk46n65MskcGBG9g2qZ/8hjFEmAyjVECOM+HIicMt4r1ztCCwvjlyhAZ5gPNHNbPiIutmK
5uLP7l43hU1UQXCr3lujdmoek48bz97XwQECM6NiKG8SjKLmPNkDXrFLz8bRTJYCJmYvr/RQXJpe
0nLOUzi4KYBxLb9/FpHY8/xmbA/x4nQVC9gmsF/T93EQWMm0ruOopNzU+4O4mLySo9soURFRHBQ+
r+BqDIMYxVQYSdAnIysLHTVR2DGzgecxjB9WdaOnCACPG9PYF4xCh0nt3Fp2fcJknmNh2ZkSZkIz
1ObwVFx+4hPOr8KuvqDg9YUFyS2I4WJB/S7zTEA4YRmEAslMi2JFLTqg18HBfEfJJ7nV5jghuw3h
cq9gjqMt6Yzg5eQEb69iY1m9NBgVWmpGxA3ZbnuVNcvZ+J5dNym2s0uX0RYl9gquoS/kq8SZeYwm
QE0acpQeLWBTUi9XHGOlVO6CBg+uXycZCl4ItQNkvaPWrsg8YPGnzWU5JYyPrqR9vtAwyd1C7TgA
cuBOIQNrO2VMCC3YLmm4AgYpY+dAvsIoUlKn+YpLJEYmiBpRKaNYUgRe5AQYXfHs/m4s+me+f4u+
/QZBsZQ1EANV39xDnSt4Y/6obRIuRDFhWol74X1puQu2lpPVx1iLRTm6Iq9jGp2DI3l3WXJ7XzXJ
aru6jUc/ilvCCNeeSt80O/3XC2HK3zA9j7cMCencxnmAclbMG7qFG2gdNpsixOh2nMSj4X3Bq+C7
fJuSeZJculHzSx0qyRn2FBzsl1qr2IZakejp5kmC2arFRsH3CT/4p9vbJIckmpyy2lU+Vg0wT5SU
K5xS8kls0osFM3nS0GtsulONO/Jigo9WVYAmZSpNX4LUKgeeGcJT9C5K7JMz+q4eHIzapxwfg74Z
NwtCberIe7l/z1AmEuu30k8tmh9bwHuaLe+iVqImM35In0kU2OIUEXhgleJ3w/YfT9delflytapv
6MhoQa+tRgnH+x2F1KLjOv9VKHeDTUTh/0Mj+v/2lRff6dYwndSJSq0orwgM8SKVdAmUjwnnb+vs
Dacq+9Vu9b6Gur6l9t2r382h0FTc2TqY9OJkTMEMr5lEaITGdEDi5dO9TgwxHRDMfGeq+t36GV7T
2OZn6QXRUBKMBTjOU6Km4Ng7a6MscP8XCHjGMboEnAJY4B+j2oDNTTe6SQU8WKEKFinN96fKvExJ
WvPwYJDsAW4MtGsojuqOaDj+473MTVMSLtvckSdfd+tAHhoM/G+Is4z5jukvcw39z/WLaFPRf/cR
bRp7Rco0E/RS/yQVsUhl1f0UuBP7rJx6hroAaDJmJL/bc9HaAQtJFJH76mtSYCrAluXUBrRx9oeP
nvkuVskMnu/rpJeDCXV5rHGK0pPa6ItERmiz7saKzaSL7rU5N4T1vQDuT3WHtncfGaeBD5ueNyun
bnLH8sWqzhsQhvYhpbxiv3i5OQ6zt+H0PmJkhzxfhTz06o9uFcz1hZu9hgl4m6bTYrW4iYRr6LOv
4rVpBueL4cKTzi6Md2+fbVlTZ6zeDbYnLmm5yJWYCHp+WLn+LaRdF4qTF+dQmZft+76VEqovmfJd
g16vnHrkiRjrUGcTcC8LtOqHlpHcMI6ID2Y8sWFN+jnEx4vAWRpuWiaBPDyePHpzR/5RSlAO2uSf
PDHwUeb/BMzITMrUCFEHbnD2FjDH1lxDN4J0bw/t7JRTy9eba5UIQMnnvoGYakUh7qUd4qQe1Ja3
izW8H+tj71/d6n5Zm3BtJgFajITy5bY+P4SEe/grV2YpnbfNkN8hly2TuHEH0v2szUqolBrfMM2G
BuPM8HYar5KBUwyYCbWFKjl6DqCXU9titjElrFs0k3IAY7gsHEsiko2KtB9BVK+zg9/VmYo2+CDP
5ghs0MOICv7QrWeAmRjnG26cm8ETZqfWOp0931B6RDOiMWv4azdzl2H8ldXY5hJsCDxzEF/7vfwh
3Ck/vrWkI7jbeUQhSg0VKmcyClZ/fimo3BwfBNmiYvlod+XAeufmLOi5+XipuPqgQxIOgOhvDlRg
ByO+JzPVSXdGUcynZWl68GIakiW+rgvP1xxM9OON5H+g9PzjFISxEYZspRWUFazagf65MsdyahPc
UooX+i8fDdtE3yq5PtHzLzj/JNljuUwx4yUr3sL49v35RtQFL9IgO3bj/RWIwSFuwhDX2/KcOOOX
gv7ooXczyMpaqhtBuK/0XtuUZFLMfp2DcZVdCZ7rGGOS0KjdQJL5JxxAhCM1hBgnN2Ylbu8s7aTl
ROmGpjhffaIwCenzloNQ9LyaYSo2bTSe1f55BGfwQTOnFV+tyrWq/ph6+bUlOvbmBdt4DN7wR4cA
NAS5ae2CVgKYEE5Pf4DMh3faSR54ull05bsOz6wTM3MHBQXbOAZu8QPR/MYN3Zikzr1IaoaP0MG5
xt/Ki+jbfBOSSsCx6AvRLBZ+V0aSLqyxClkCtDBr64Clvy67AVs+bNCh/NZu9t7jVVLlfK4wLyLv
9QcNGjSffDPCqBPt3K74NFV7Ha8TgPK6MyaZvPjNncX5iqmYUNhk0RBa6J0QRmpK9e3UPlHkay9V
ERxm84LUvpni2qp/hNhwTP7VvS3cZwAi/PaPEUJOcR6yNAtuL/oA997QU9a3oX/m1QFvHrd0NsD8
mSs5iEHUsYN3T4PDJvLBuk7aHJ5HVi+FfGRsj9KfWwYE059zzdI5KJstVLZaTS65gZ2hNjJnEgmi
kGv4ud2eLIve8HEHEt1YqWwa5oIRcTNs6sTqxOHH0vrXWdM8EQFW5WSK+2xyeu9lgMj3ih80Vxye
D9BHh7NqK0XCYQKHJODEmHmcP27EeHo1/B/xZ5OrCUPCGdhyEhTKFt4x9x789BPAS/kMqESbEdv9
ebyjtuq8HW0M6mxPbkyBVX7xxN87G2tPeeB1jbdNUgVhL/0PyC0mkkm1DQlzNakk34Zh2vrSfBkt
UlNJUfDX/8fmV3NEfON9aRAsbcv/MKrTkcgTDqXgEjjHYgEhZSxFGI4fniXhxI4ig889x9YVw6hn
ZdiwgzVARE5yivWMN1RidO5m9aUIO77lTcn5XWcOuXddk7moMQW/KQDOpR39m/2mHZ39tjP3gi1P
8klbGbmdjf1Ss1LF0OaOXOXd2nezGbXzgG/KJsneWfk2lHdq/ruSdQcy1SP+ZJ9DGi7UNy+0PhGa
tVhEjbe7wK/So23OLA0e7KF19eok37LLSneGWenA5lrbvLJz6LCzXFYaddp5mCunfjEWcnpQU86B
i+Fc1FSRduStve1r9ENLAsjD2uud0yeAr2xreO7H8dLvx5mrQ7UGDXe6guBhPVAWGatcDDB660pZ
8JzJ1GKXJsAPRLMhYGm5r+ffPACs1XDE4flCZiRqZVofuRDI/1ZVACyt03ZOvfslpGXq6kOjF+m8
jw4uwKg3uvowg3df39zTQkd7QCl+OCZ/e2m41zM+0Wb6nffQUfUL50qBkMk3mIOj1bVHBi2zc5Hx
OpMAGVSYP8qXw1l8pK9XRbRAck0tiEFxAugusT97KNegmhNnwIffTT3HWjIvkgNniyuiGuY1Lr/m
O89AVoi1lU+reyZo49Z8/IvMeirUCA09eaQQHsX84BG6aaWCFF/D5jvgfaRiGrma7vyrcsWq6slI
ylY7xIMCRma93QCxkZxkK0yMCR3N/qYcR6Jc5ctQ9s6uxOVX0tbK34YqafnLJ4pE/STwl7kLzxYR
p95Aq3aDLGkw4OZEPktlUH6yyhunn+6iOIhcOCS7JHibKBKARIxp0giJiy7yU9YLJqh8nCzGqOm4
8fZ4V176PAoYhsN0YaMpF1PLKawwqqcW5fnqN9PCOh/C89DO26sbTft5aMl6ShrwovoYhhTY6R0H
BR7J0srLYkbfyLuPav+/GGnUjzXHTOS9Jpa00M7AWzyRRbCS1cJh0qds/zQfWOEmAljnwM/OXDEa
cOXoZaCr9UWaAu4PSwWus5Xj/rOdAnNf5UY/vKJ5XiRGI73Eh39PSpycEZE6c9Yogy3wmmAUc8kf
6EwnZmDGTqEvyGZOcE/KrcNHmBgFAZpmvpBs+FLPy3t10edkDzt58mhedAK1xrWIOedoK/+FSWeL
o21tQDyIsHYBI34u7YvNyBNIZDehTnIMORoBu/5+VF73GpYhTi2XwIwpxh10N+5NoHpcpAcyhyl7
9To4EYQQnDrQP43g3/1Da7IRG5FAp724Akt36cwUWAiYrOS/Y9AUTQQBCkKiaDII7tw3lLl7xR0T
uLasBR+XxV/4GVvvMXkMaenP3OWsTLE3fol6M9VoIrkVG27+LLaFdOz6EkZ9j9/AXPH6JgHVxNpX
n5Ov3Grj35Y9UKuEUTYO7ugca4lu6DeFtU3TqUU1jJKT7BCIADhj4kV3HzyfRPpzb9m3zMJnHNi6
PxIHuAyV5+i+J+Jng9O4JvWVLUSermFCwW+g3FpgV8o4w5PgLzDzkC9d+TIT4z+pmCU2bwDneAGH
eu5saFsRITopESEGjxCvoB1RfJpO0/msRDeje45SgT0S8bHHD3o8mL6SVV/qGcMuBSQSMUCV9vCC
rIHC7DFgcpayyMS52dUD1vz701U+ADRBmaPbkKTcVraA4/ajT3NgCWNgF7tNrak0XXZegamZUCyh
jYYJP+8sptalUvIcNW/JxrHly3Aed2MDP5f2yvGCcwYIOY+PI5Pe0qLBUipsDY7gf8MYMsLcDLBM
7qqN/yrzt1tcREuz0WDk4s/mqk3JzXEqMWiwDAoGjUvaNY3t+SlHH9G+9k9IYWU1/nLSCNFPKE+f
KNqik+rF7r6S8Ct1JovPY98uAJNc8cLppLUxUcThajW7GNkA7BoDkWy8s9S2Pg3tERwO6ol/VP3/
ufYblX+rf77b5a3X9ApNgVyF8Q2u1KLI/4xit9w0Klup0HzopX6SDgJg/BRNg2AzLB5akXjM22RM
v/y0tauxE9Q0YleymS9k4nwVxpLSVTF0WkmKf63XUlEK9VP1hjf5vYMIIs6HL0aQ+yNss+rKWlg8
5WsCDf9SPZ3TH0VhNSGfyHhG6ydlGmRBVlHU1mJDvjpJf+E5NmoGGTJ53BuYQYCRT8DAFkB3ZZ+n
Vx7y5WHaHUWs5Ks6dusCDXnc4+wsdKrfi5BuK4cQphWioLNcrO3K964DMzTH3LHO8G6Mi/CP0qLb
4vX1n6yHB4dAjlEQk5UiiepYF2ORnhrsefubWry8cKGzgly6lqzyB6wpjudjKqh6cVIuWds42yn8
1phH3xvbjgg0//h3m7HXRyt5Rths8fo8w9DipdSysDYHpsoPhubbNn5eBil9h28w28dPwFDwpLbu
WItvyi+CfEMM5/RSFlDRpcTQSGaKYXHzBmeYgM3xxe6Bfo8e4OhwwXAGhJExmVA9ASZ4Gw8SQRg7
3H8u2BP96KPxgQGx7gOBLhrYsBM3W7PmoGlyOi7u08QhsIiN8jPfr1sATHtTlfdYYUeWsE5IzccB
BiY9nnKlxLosdkSNVbSQ65CHvnQXpXQIQw8VHCRAYt+EaMUFTelaB+gwwppUlts2PbVp+ehAn28P
gDUWUPTQbGXpfoHLQ8AHMiZnBVXpDJ93ip+9p6a0KQF+YpY32va6cTh/PZp9ldLDvfThOcSaUmZm
fzzL0k5iSYleC9/65HyNDgJq/Bw2QQsJmt9rsiNLI/NyYzWOC/N6x3xVTgqhvfB2aC7QVTdX+YU+
S55/Wi8T81n9djNcnyEp1p6lHsTpeLjeCWOPj5o91sVPMfnQBT+DCgvlPaVBp8kmxJCVU50XP3Hd
Jc+uTwqK7mzoAcEQ6RCDcXx4hiyRc4/TY6upoM3abru8tLvNO/dQipuhIwupUa5whLM8xP1ZK/AO
IfaO7Y7nXqSDyZCH4C87lIvlWFl/uZ3m6VWYzrNX/uBThy7QgDSt2MjFrb86zgPefPT23kLAWMQf
wpiF2y80VbYbWOzeXMo04WxPwklNr/vOvqzHOljwT5hbRUTQSSPkeijW9McjbDVe2gf5+VMD1UU2
AYNq3C9Sa+pTv01UlvOLCN57Cs/IHS18Z5sy6XmWZLUEbraVSHaHIwYYK01qTzUn8RJrPFx5bKBf
BH1XbX55Zhr7sb8eqsGK94Any4YCaHUe57zq5WXiCmA5FSc4efjCgOhu4QQ+wg2s46lPZDK9/FgP
KE7wWXQFzdsWYgZ30Q6VOr5OMAo8Xe4Cw48uYIsM0fjpT4qYysMbUay3ip1cAOrXQj7/CkrRuR1G
nqwgnU5KiPBZ41JvO5xhr/G+h+nxRC6l4OG+SJiEuKqldXqdpeHpaOtOjI3XeU3+8GCap1iIFNt5
hFnvPthYzG8vQRw7aw/uQJWpMoP/KOKhXSiyvVV1GpfIUf3+sKaBOJFGK73cARuc4I/207aAgQ7K
VTED/VHvSSMzQ90tiEP5Qmc/Llv4pCyHlwe6yvhi0PJBsyzn+GP/13ndLaLflFjv3q2PT09nkmwH
1+rj2JPO14cnWDuNyRuR+rjig3ekpUE7ystOyKNxQZAiHUk31tTLiEg6Ddd5JsD5nMxQkr1MXthg
UbP97u6EdscW4U4IV6StvHRLpUF/MkrhfXxYBcjxgpNWLvgsdOpsxhPQ2VdKwfS4G4wMAmAnQVdk
IlTe8Co+VOeel9oFhn3zxRQA3gAXDp491GlYid/pqk/B+rwNsqAHm5vf89R7n0Ta6CYgJosD/gs9
mK+o6dORXxdNggzVbgIq3gU6RuYE8B83hOaPz0ttWQD+bF7p7++iu1LQYB4pSGKHkcWqSrMrf4iz
RSq5dtBNLt7pFHc1loa2XOHs7oxMNFSm9dhkLTzJdmkR6rFOkGCiYk0zwZ+JzIXbUouaOX1aStxh
hA7dYgOcsBEWMRjpdERSqprZk8Ni2gSGldg1Hqh3qJ95zdPI8KWGQ9C6VUvugyWIsnE/Xyf7/nVf
yB8fLcmf6sf6/l6e6e3wCf3Dkx6GIRiIWH2PhrgeW8zHgIKmj12Ktz2hifOfqW45G1q1luWl7243
uyQdgWD5gty3IgcheMtktmmMsBu9km7UNRT/B0ZaiMR876IJVONz3ERVHd3+OmqGRZMYzvuzP+dJ
3A5GjurKjjH34Aw6r/BHjKRiyWEXRURe8FrvGpJSzZoWx8/ZLHiR9ZwoS/gZAhMpjS1UuUO488wH
Yv1fWWhLgCYmxo4t4QZaMTs0z3RxwAmxsQQfF88jefChHYKVlf49UQ0DnulivqYSpG5qSQrpAJw2
k0Y6GXZ98ttTwZIWADFUftn+NffhoBA4Du74GoumOTZKFTdICawOZLi1s/f/mfODIYVJ9jvAckFU
icg/02SCsFuKEZ4qaG4sBGUnPSjNx1qkA3qyw8TO7/VpO2kxzXOf6Az/f+5JitYmdOLjbJIQqmBq
KAcXjUEu8GZzkGls2k1LsBDm62oUt/m6rj0L4JS0YiJvWm6GvwEN/QKq5jvY/mKueG1hu2sJElB6
HBoPjzARXQGPHJ0ICMZ7pSkmdVxjREgWL22HNf6Q0e5OObKJtKPcD/b0l6q0KTNkoEQmc92WThqo
YGUgsnITMy6MDykCJ1nclrMzLFpmSlunFnz3YIeUhGHQSvdUefNmscE9JNOGy5Yr0JlYGdGZGjcu
8wkkTvjv4niMfkr6rIK2i/rMpf3pnxjS1M5LhYSFxUyXyCQJvDdw/5+A8qECeldWlI9wzqfzxH7S
mZgkRmsT/I8B4eHfGzlwLPjeBz8KdM1Q5l31oOx3G6gm0A57dswQPbyFAD+J9tK+ck2oNPv4wIMC
uJnDe7UExMPhOHs06MlTDH1+vnYHX+0Pqo2+RmYuEJ8CfHo/d2H+yf8K8x8WLENEBsJtqP8KE0hF
S+bX9GS2B8GUPDT68FUfOWVv4hNdxdAdiE2X9+fAI/EbkiSmkNoWvfVgVm5g541EU+pPxdjxKVuN
y0unU9nlt7dAqxLY3Rw2JR4i6gU4JIzNY47roiz0UFCbJthtB8io0PGMdlmIlZU7FVSz6njhVy1G
lnrqWsuzmjkT16D4c7KYHLcltp/8r5Epcr5n43D4Oje10q0H8coz7zVMlf/5sLgn1AN9tGwXIpvK
Va120UPk+jxn/wWbWEaVQI+UwYE3pZqeg4i9FBd02aavHnKL0c3//Z/m4WX6f4+eD+JVY3ZetX90
PaqLaVPIFzEfJL6QUUSKHC2ds5V9V04olzAZ7/stQzPIItogUJmUjncJrQ+KOWBJk4fH5Z7O/uVa
EAXx+XH+oU0D2Od8hPOxzC9GvxhFpekyieuZGrc9fG28IVb7BQpi5X87BFd2jO6gOzm4CaikGRv0
2jA39vtMzWOUoiviZGX7Zl/isCMcVylEZCNPYQgcZ/mrN/xZH+sjuVlBY4Det5AaoQjSvkjQ+a24
kCVFS3QiMaPySQjpR1jQbYMWDhojU+oYpXiMgIStNoPDoURT8L/MAuuiK/YbFIE8r4OpqhTYZVBo
EF0xqA9VqCJrtgh1Mg5yPuHCIF98fVi6a5OPrcPayDcsyMlMWX3PHNCrMc5PMrQ4igiftuxvqb/4
0FZIfUAKI7lEbcWP6HCOWb2ec3ZvNroUBW4Hz6K1+xouIyZzWM6F+FTTxGJutsUWfz39zWn65Hi1
Tujq2eXjpgo9A9TEE/E+gfEX88iOIbRGxynIYEwmQehWxWg4oTkUaOz1EuiSMPYbpurf1OMT83DD
LprM3FqmTiKtBp9gy3smyxOafnCqYdjV9r7S3qj10kkjKr6or6EnCSy5niYr2eON1Uw6uz8IxuBy
/tFDDg3Anw00XgFw8ier4j4Az8645oQCmFHObYsbe5XnsE2DbWxZmL5WZMlm4tIYvPI55vESico9
PHfQFOCNkV/S7VKsAWWBiF611GeeMpU6mAaALB8cL5UBY/GYcGXvyyv9O2jTDuTn3Rp/7auuRokp
WpyupUbM5S8l+Ur3Yr3xvJLz8k6VFo8+j7KQ/iV2Fg/nnYXCFhq2/YS/Mwkgbgt3+WhwQuFLfHW8
Lq6WLwyymSPsv88jhrcqFvuRUXwPCQZEpbxyrfwJ8HvXkDwDTkMMxPTpG2zQ9+HXioGU/gStfJoX
GAfcBer65rJWhkiVGjywAVsuH6EGnuK8lb0K7Z/vp1awLQuPEJSuUYSJd/YW40Qdq2rBbVsVbsUN
0roFGvY3236iyaWr+7x8/peZmy6isET8WdFzV2rhMxmtqbxyPP/mxlgpYjkFh7mEqbYHkTxxWBmf
BxN87snTQo2sMotayNUIqRmx7MTGjrInM3kSeqgF+7LFvbxOMXwHN/dlkaJ6LrwwmY7vxCw3obOS
NkTToaAsW2VHLn6qzcFxfaPoIR2cW5XDAT2b/mJbTgAu1i27ZTlTDfvFC2jUhdLs09iHedIqMtht
fzYIKwKltw6GkfvdPdb2jIedYWlb4RzTU1iS95rKPo8dcmrChSAq3+kl4y7yRsCnrUrd19lQUL46
NMwRvLsTGsh07G7/o80Frun0nNYx/UbXXi//5FIvwplpQ5+22eue1VZ3I+ExIbCBZQPS7x+CF3ys
h6ZKGWOpu5CxSD5P0XN7HPQbBxyl8+C/RgjriLFfDJRh8YS8lC424tLeyROOQm9XiFzqg9o36Gc6
dFADXkSlGFl3JNcUoyLBSpYLhbWHLN8Zm/J4v2HbdgxHQJdWN9mYUUhJM9aF+10klUs+C2RpOgvr
qivE/08Tgbf+JGeknd6f1z6iSS+Z6siuIX6QIlMaB1ILahi6ALLqBAecr5HRj3WFgPl0VXiR+Yu/
+bDT/zxuaOgq3LD3sPltii8kaQYRCi4gJXeA/9dDO6DvILBSjEsaDBnGCg+8zYeUrBpYtWj8fvYS
4+BaOxlJzhtut8iHKv7MJaHqUK+k6UoAk2zQXpr3Oq3sps3S5dpq5uHWDm/NLGw1e8/EA8j6nZM7
2OeDqly03cCFzUP33hROp2C/CH9cQd2DmwZkYRohKv0Q/qKLDRQck/WtVjqX6wC2/cmqGUF9tt70
QnEUtLM7E8JI9+MjexBylChA53oRqI3f0fEyP8kckNmllJHLzmBUqwO4jXY1mWjnMdj5+a6zXLWw
Fh6rVX10/feA5ZEkzkUAWr6fHGcT2gGtVbdm056EscMhQXtrrVQaxyUerNkiWyCXTWbC2rijeefj
vKYk18Z62R/eDI9GUNiSdWuY74CxU2SrkhVBfUwGuuQd2RVJRsSd1BpL4WdDbdjEDUBSy5HOS7ah
5zSGSoo49xmed4T6otFBeH8ZOpraTr8ad1ti0PLM3U4TXyO+4+wB2CwDE4WKb49zbFxbH2i85kMT
hmLz+VVvJxo23VwYIfJsn9qKv9gBD++p7oeaxLDFWWhz7/VPGfzdI3YEjrLMP09Bow8ZwAnhWzVk
x2tdLxiY8q7DdH3nzxd5lwLvPiEjHzXqhmyMDNW3RrCgadJtQf67FFMybD7CRExxBECHMuEvBu2R
8Rfx+sVuwM9gAe0eVVu8/iSHfWJUKv3bkzGCCTk6q5QPEBSOYwGJj4UheapqUYCxboXDijEuLLGO
LdeUb05JzzK6NvYwGK+oCf45NRO/pWK6sbBdLRFIe6i17pXiUufcJgVxE8BIWpI2y2JkDWDVoEOY
3tdU3m/biP93fKfCCzuOsmBgQc/phQMP6jbUZKfUPYh9ytp/XnVHBlTk3Tg/L094PAzCN4GcgehV
mpPvPSTDq/43YjyBY1Mw6kDZ230BaeXfRzqu2uqGAWo3zFvS4BcR0oBy53CLKyR6tMNZQlOWIe0i
CpEf4dntRzTRf3vsWXAGExD3x11TX9qho9Ygqe2FzClGcbCSP4VYReYgBR+xYW109t7AaY3wlf4S
NfHv3dhRQuLHFtfINhj1GnuAHLFHLXls9uitvE9HoQhETpZlKBV2XXWaHrZ3yX0mqeJ58W/trZk0
qw4DIO7K07WKV3ppUH1qJxewjGt8kSCQfeOmNwmwcx9T2JPkd7EJ+qVPAljER4Sx62to0Rg935LG
AW2hgkYw705DDE7sUO8meoZ+SojWXSb+iBJSrpbP6k9NGwOpy8TTacQX2spV1okGfkyVdLSS/n0M
mJ//i+FmZPqGoYGtIqMrl6yNrciBOI4+Gx6zKebmwFVlnYdDmHzf4eARwOZyeLTAtB6SfMXCf9u0
jyhPyeRz5dzcNhpJ+AavU7FdOaqFREn4c1FJrTwGrbIHKqOyztBf28H8aMZPUyhQhcjlW+PEjRyt
pZrEWLsgI+vq/UjD3mhZSAL6dvJ7eEC6D3K8iN2nWmt4fyrrbe/KQxOVDQrL+0d2X0eCDXcQ78G6
Tpd8amwjbhT0+BqAAUoB5+jVlfe5mdb/blWOcbZHChs9BKpM4TU5X3mcnsr/IKIZDtXDDbWTRoXQ
R5qE7VsQdAzvjuIWL1dwYT0hDRsNdhCALl/LYmCRqqN2PIXW+yLIZxhBMzGRzdLcu4dnTya+AWM9
pP+dHy/Ym0y5m3wwCgYLvtvlRDt+cPTVnYszzhS+qa4cUr12kl8/R0ijZmzGBHNlODnIAyfn25Js
RQRr5W+aFe48rjEcTJE9MgowvYSaFxEz0Q+rxtPKauhTOb1U4H377zxzTHfTdJtGVzWE2Abg3ndr
qGw8HDzt1S/YcVqvOpr3YWDw8vruUO/PoVuiwBR4JNLukhNldtMpqhuGYTw+9w7ieg5JiViEipm8
jJkFG1v4DsJiudppGp0goNReSHjz8xRtiSiFkesay7vzIO48y5QOYmsNKNQpbg05u4Huadumjzjh
JAXfp480J33betoqKwb15zkjynQ1TGg6cj8Ird8zVWOz265IJ69L4QIwUxThs0Zk3X/yPIGhqpIV
dmH9ofRsy6ei/DJ1XtPV3lpdgmOQG1uhoD+e7HWBZBjKHipBcIIuic4OWpWNEdGmKUKWXYJTjtTq
ZKsLgmBJ+Cu2TQJ3n/dlaDatjNidYVJAjBbyAy+3kSdYFFRNnSSzIH9XGTsUzd1vnCzz3D9lXU47
WHE4tMwnpCg2kz7tZfOmI0/hJuNW3FnNozwsosDv2w+8vEj9RvK/HXf9e5AYDACadB+stbteLgVA
BLYyaAVBqDqLQ1fJSxB6BD2A7n5j1oJRDtUDXUswOHGQqzt7dB4Vgbe6se/RMrDwjNt+CI/4Q9r4
cZbYpToKjsF8IzHzdn1mCvY9mh+VdMICx4ougH80y66MjXHC+qLDkEFYms4AJOBhVmcH8/YexquZ
KMi8wK99gLqVvIQxX5oGyqMoPYQJ1QddFKe3fVcSZtkqcLdUxEV9LU0u/moqZbRWvVwHNG7b5D7x
3XIpdY0ubNTKCZrif/kXCJCPmJBDFc/VeUEqfjPDj9bw1F3BGsNWi+6o2ZaRkPrvL3evpAIdglT4
dGRvc5RfnDtaHYL/DVYGyhYXCvgFj4Gbj8QsEeohLA0odQ6EQl6o6gttbpGbiTSw+jjwkm7+dLBA
UobWRdsHgJb76IliKMNMv1wB6bKks+xkYaqva5CDha53gd0VC4bg2NepOYwS3izDEqtiFNdOlHl2
fdaChtOuJC89DeEgpHf4O/1Vr2a3JZG5jT91o4G/UuU2EregezOo2Z5yXXuMtcDJSj+rlR5f3+8S
MRB0VYaf3VXxyFHRpa/9MFH9I1bbfQIRDoiIg+o8i6D1D72yQQNgzCzqdxs10gKgYtHx5Qj+47yb
xNhKt4RKpk+DMPeaXBz0cassvbsMQdt6YPQqy1t40ZwB8Mw4Y8kRFYtWT7Z6eBq433DpKbcoFOnT
fM6CKUXGuOEKxOjdhbflygTHIx35jv7tybdJ1+ambP2/9UnhHj0cBJ23DMjwjfbS++n3kUl/RGa9
R4rAFBCxZ5eo6AIUCIYB1BOnAlHiigYVXyNBN7pr7/CaFOoZ7Su0nv5DdYsb9h0eJf27Sl4/1LX7
XY2ybTdQpKqh632rnQ7dOYb3XSejYKLfUdNUtMHn9vSucPkgCrqqUORE3HxrLVtPOs3ON4Unl8Ui
iMeCxQjS66slBz78Ut2wZ743p+bdaq62sj95PIxDHKi7Somj6nwrL54V1fr2OumB7AlxLPiFBE1T
G306v6eqgrKC+TLQO83X84ZLrXSDLwovorT6N5KW2kjj+xeor/Kz7xiikBr+7mRzVaOT7LVl0lHV
FjciVUKBqjmoq2ejyDsei9e5wsliDN+PtyLsYRlEUwJms4X1h+EAHJDMmAOBiTvirMeDiuC6uR99
oV3TFH8EdJRIgouaXgxABeWJRAOn6ugb/db+8qMjiRdvtRXSdN6ZkkT4Hfj06UdDGPUH38VHjykX
aEn4Rg1/Jc9MWr2xgiNwK7rUG8JUAy0uREm4+8hhOQoDTcZDBb19wGeKdbvHC5C7OPIR16OhqALz
t7lz0hLDyCVeNvsV940cIQGaHJK3GFgOVmYP+7z4LkrktOVI7Egsv6PMJyxu0MMblES5/9YA7Ccj
GNOHkADmj9MOEIoG9/f/MtaAGvebYWmb4YeKqP/XsPxIUfyxRT9C9G1NaN+vea9nKFQ+mO5b4iNk
wF4+BVlu3pYuwff11iIBqbzRu6rkBq2NDsc9X7rSNZKBRAK24qW60p0APRqE483ax23Iftr2yrj9
2NzwvYaW+pnaATkhVHncL3B9WqXlA6dJC6QFioWCjzSuSpXSXDRdA0p87SX1qTSKWb01vbzUjHsO
10mqDZ0D5gpdH6wNURGgTcZQv7HsKSb3Khxh55LlMahvwVcnzFDqadUVrdtOYRAh4MkkaCrfPVnT
5oQ57zwheld5KhasXYZlOzHGeJDCiLyGriYpBc0OGVGApYrpMKuaWS7Bmev58MJALnE9lo2pltYX
ZsuZjHHWqBiJClrtlGG1E1unv85uR5+WfeMM6YeNruMS+VV5A/ctk+T3eswWy5aK25GmUHo0Kbcs
Y7uwf3ELNMdpocs7P5AQ6wkLK1Lph5FAE6LQkScmULBPXC3PHraVeHW+kRjcQYLHPH7Bcfufr6pa
lWd0ycHlUwE3QHzBPaazZY+5raoMvQaJzL1LXRagEOF8PhvZ48XJrVVb/dpk6azzEQKLCdHvTb1c
ZNWOPrhnq5rVYPAkJQ+aU1PgTMAej/K8kn54QYmZvsdCJfb6oFzYh6fB8oHpr+jjU7uR39kxyqSq
XSLeclJM77cNdl+329Ouacvf2MuLvxnovC4NHD+W4kDSDRoHT4hBOaniZ9E8iL/CcJyQkM1NXORf
V8lPSoLP/fX+KDN/cLLpEFOSmYMq2Di78n1EL0WpoK2Q4+GqeNY0rKCMLuqXd1NFM01I4fDAqeoH
m4HIp8eh3/ulIF0Sr7slWUaCseRO3Am9NVzYBEdN1ZFJ6TGaK+491HqE+AsEENqaEKMO7w5Mjw+/
/pUfpOHTv06lnRGZkJObR1CRFpF7BhzRD5kyR1LuuV0all3fp/slow1RZL8sugpZwJG4zWd9IACi
wbOgGstmUoukPsDHZYShqxAffTcNvQ6+qSEA791MvtGZABBSZ9XNhoPXUXnQe4r7BMiiSXjpMRC2
lghyyHUMs8Gx8BJ5pac39RqqO7vQhSEMLopVTeZ9t5YVsSL4gXufFWz6YgIhdkDWC7qaowctFMjP
+wPrMIg+BeWTXHFXMc/M2QWH8OZHsUebzlL0FCIH1Flqzca9H2o5/GeEI+b26rHPUbzpCYMJrtci
Du5FsxbMFnG3v9giRZAFFO/J+lc9VMMpjVVKd+c4g1orQTPsVaPbjE+wmkaKJGGuS14dsnppUrNh
lDu/Yx1kHiaObW49g2wiUYj6e6yTr0mi10+SYlX3kDlXtV7kGbjD8NOJQuKwjZro12geAqpinUWs
RZ4AInDjCk20WxU4T9LKPAn8MFJG0jmiMCjofz5gpGVn8iAsMvLNIkml+2DGgRvbnRCrEoKJpwem
/YCIfYZxiXAXSNRmyI/+67DCohgATECUjutwwbnm9DvBYJmWZB5oZoK4Pt5Lu1HQbPIUmzPedEye
UzS3A9tNtcEJ9ceQEQwgf6xQGf6CYw7qpJAgW5rscTl4KF4/X7Xe9fD0FKKmgF/tqF6iFFWD/vV/
wwH10ClZQjFVP7C01Vhha0Phjbzr9dDCe96vYOGT/dVktc4cVfb4p+XLIu7byiWV5EYrofkoT7VH
UFXCqmzkBZvhcVXdRqjA8FjueT+z/ShMvAz6hnkxFpYhROGFayHYZx9yc3MNrI02mbeCOtmU1AQU
dTDoAW8mmG8JylGHvZh0GbgT3sbIr5FTzlxSd+OkqM1075DNVW6CY4epW4gV6FAmaVgdDbjYU/XW
sTh4AfvYTIxj7Dlp6txE1xE+Q8IoBJcQc7zaGcEfMhkavJ7jxNHHmrAwUzm0CTM0kG8Ze1ctkTu5
vOhhb7a8hdzwA3RJ2lkMxVJhSkaPZ2uRUCRzO7oT/QZB95VtGq7tul27ovJA0K0Oe012VkKBySaS
jefvRkV6kA+cIGiqv1jS1H83QngLrzJVFCSX7+wqyW+QOvBbwKAJc8XMrvZEX+53/8TYHoVkQkhe
J/Pq7RYPihjIes2G3o+7IgUNLmw5qc6ZR2wOsIErhE4a+FpaZtLE25I6DL6eejovlE4rr+G7tXAQ
pKEYMJjKUVt28kq5rerSBajaqEhRCVBD7EN4jkECvo0mzv7GP2Wl7xxWqongtBNOK+PQH57trnpt
Ot9Oon66q6HHYFJFiLTGAe61xsH5a8l67e3DndpcxF5SYuXYGIHAOZ6XLP09KjlYInQqLCbW0nES
3veZ6ELqC+1chwiz2Q+x3GGN23hoptscg5Dzl4iR2T9vLESWvNIXBs/SJ6WHQrePUPNccrCcqJjP
7F1q/vcAHl3FP19vuQLZUJii03F3fOZoYTezJOLQ5cZLddcCvXBVqODtu8WT01efnmaFzrnQsqZP
T1BMJ84meWFpVlFd5Cf9qXCojgvx8nW228ktzPH2yRXFS3x3SkT8EjxjK4XLXexHezPln4nycT/L
BSSBwD/r6ohG89DJDk6FlW24mdaBOIYhbcU+ArLAaHDgl89mdJrgiSplQv20FFMozBMuee+cItkW
90MJyTtjZrYg3Hwo0v7ugUnd/D2+JHtPcQ8OmEUvyTKB7lP3tCSX71+trstSN64xVBJ7htrv3+YL
+OtKVI18AYD5D87ZvnAIKCQ97kz1XCNFBEKqZS6eDFdS/Pz44YWwgpK+rtAlQd/2o4vZ5LutWk6Y
MaYTP230liOReD5vx23dffVAiW7o3b3H3oDc2H6eWmwkFOdE51ELuSAQ9AdE8D8V4RYt9WsENHnW
buXJqFgsDWIb+FwQGYnS8oeLL+E1AyibxLKe0lPG81TYQP2svyYBdneyLckuHFbC7Qj0p+qjAK03
BsB7ggmbTM2OyC2EKO+mSR+d9v2Tm001oQHr+swG913O9HAYZ1WDTYG2+qK+CDoDJJgYp/9LKF8D
+Mzyeye0tuxDwnp5ZEEO/Dq8I5vAKp9743sT4vf1dB4RaC+X8yxP7SmQoHZgbjvfu550N+FWyQk/
yV8Qh4ekWzSuC+9KN89MFc2vI1Yiavy2YPK4RDDiPNHdu6/XnvoLIR+lxG1zAEXITC7N7X6s6/zN
xEc+fIdo1x1K+BMhqkiBWf1qT9Vd1DTLi/xNcnchKwTdyTUec4FjMs0Cm6AnPztVBdaDFOLBQv1O
mhDnug0r1iT5Tg+z8zTY6F0TjhQvAwBQUsfclYFEZXPrUZo9Gu3rrPkInB00GWLXfdpxMT2z7VDg
ksejDXK/wCrtUH0E9q+/LvayBiIaExAGKaXFXURHt5MiAWMOukVSoVhxqFp2V0SnouI2U3wEdjj0
HhZQqA6g3+MjuTeZc/OxDuenvMwUsQ4q+v3KLHMlKpThRk+avDOzanbzZ8LZ657n9Zo+nz+r0MPQ
waUAfM/qoxv435BE+Sr5WtXnsQS2hXnNvH3DyrUGzWlYzzZFl6jCk8ukuQv4rcS8GUNbhwGtTuxE
GE9YuxBZJ8SjSk7bqwgjEvWY3sK8Rs4I4aabfYNPCFYXDJKhf6Xd/niudWoKJwW4fredROO3xcpx
Gdo3h7GRKuI9n304R1Wa8dFn/pigpGqjmyUUKc3NlbqFT7VizkrqRyHKfeaLLxP/XKfuy0y8nvRD
xT4hx51YHZ3w3BKoPCg+tQjGSlZdM/nPX3REIRykRxPZRn52ACo3VW3KbttCKBTNKjjkOnhk4lje
LgE28LxD8WW2+DIMcGLSPZjLXeXb5tsQOnTyMjXXCTsRusAe9z2QMWEfkqmWPZtnywVvQERHQqz4
3WaeeZsC0UVbBctNRK+UMK3tZDWpSjk8jV9rABJf3teaoe2sXU7epzg/SscWxrPH2BQ4a6ByJqsD
N3Q4naeJ4hUcOfizrZ8V6EEmGsqJ/FGbiNCHSVSsUXpeoOc1Wa5ujxZ2vOYXAKLw1j3ieMRn25LD
BSVcwInE3VAayULe0Lkamq82thvv9aHqafaTwPCPImXXcWVYlH7FDlPRvj+B0oe3cW0OTB3PlRiI
6DeEPW7EhLiEDQIAZI9VnukdaI/5U/qVmxPCW6T9Q09OXmAviHeOTVHmTcS+TzXWQ8NxVGYJK+69
RnmlO4TzUuZeehQBJgNak3qAr//lrso7xx8hSBOpI9IGiYZU6IHXULXRgznmbumyio0ssPij/5OM
fWV+h+IDHhAC/9MTtTD7J/C7OM3UvLr30GWwaOXQ3TzHjGm7woS64RyPcDpcjZ3Osofr5bxdvkPB
ZTTDviuwTHsZknGIEoaC9eLo6+2eYTSN+T6QxMSGGLMS/hALWjh33u5RUipAlvACnYogmL3Kl8bh
IApv2CYIC3g0xWeF5QdC+j6+4/B1KVtF6ZItUyOsJXwp+X23+T62Ha9R0+i9N9M9las8bsIO6f8a
6+YKvG6cmal2Nflqobalwk2Y0kLbhGwaWGW0kR+ZSOgSc/9QA4U1c+QeG/pkmGjueU/FoSLmAbpH
qja5ZfkJRkeAU+uzP8iYT5QfMoNiCsd0ntv/mRj1oVYgB8orm9m2Nxf7rGF3rD/ogIqfEDpWD+9/
D5v6PAkntQBzVn/84+ntSI7LHCHLiXrI4OxjLrxfPH+r2l6u75g1ked3tMmsfh4iCgede2I4b2HM
6+0R7IXmHk9E4CGLUu1UiJp7E2/Pez6zME85JA1VW2r+BXcFAC5Vs7N1VLWs0anId/+Rfv25VSU7
DUNp6q6+vL0fm9hz4GKMfzbpWxqHpWw4vCG1oTH8HC1IN1KNVi/mkFe3O+s1eX8WlUfVpNW1/gos
+JlZiUtChefq1gLfWtkbxkUglenfDKXZKCg68KtuDHlJEgRYaQ7aKSG/3k/ptykBg2qjOmjPHGok
P2QdqwkzqHNDwRLCH2Q7Soz1/Y2dLGJiYjWCfXhZLUgDS6zz6HJuLyt4dkFoGQ5rAaO8La7X48Ko
y/yiFG1hHjxasxdebJyARMSXigXVgRtMnMSaG12Vfsb+ysH6J8nGBcyrFWOFiaAeqi8Zf0mS+dme
TaHKL4nuBIpFhEJgkvRVuJUoYWAuX+ncS6MesebmhK0bFWvuj7coHypdBCVBx1mdMulfhpVzOKn4
kd5HgQC/AppS8wlx/1BiK3we8aOrWquRxUiOZjFP26jzrt8oPbUh/SO64xqakSbre5RwM6AA9Tst
kJScvbKQDmikKQZRCDjg6CFECxEeqQydYRvAUnlar8uzTC3Gcy2vaw2JNNhi3TVB3bcaltJmrPeL
BCdIL5AVlyXGvq/xKWYW8g1lrATUT9VXuTiR7HCIRc8DNlOeoEqew/2NeVgvw6IgHgSJN3SKcuVM
ymApNabqxec/I6lD89WgEUZgDAtWwNVbPm0Tdzq4WQZKNuf9Y2o3FwSxcrKGY5qMIptDNtyIlRGv
wBm7WMeyZ0gJDKuaH+ADaY8BQZt5ejTS2cmRbT1As8vcs0keXfIhUqchglinzC00HbeRtGS+EWM0
L7t1i2dPk66m6YlmnKelC7kDyLkIlFkRi2MlveWKW24zTd/O6L2PEJyES+C0XsbjPFx3neQcjG2u
h72D4pp00SxFgs5oUExDb2yOgdpd8I1g4XBlbURNG9frGc3pK3MomJGEYUwLb/FwtCrKnxXnNfbl
CZDNgBNAALgsjkLFWnR1lM2aDQ+BN4ceTrogT0coP0ajax0SA+bv9sAGty9Q61rZ6jOWBHGXSL5+
5Tk6MzvLUsttEGZAya04vcRIGnJJnL9FkY+pfmg8u9nTUfnUGbKCUPYX+84iMNQTlqahEtytOTq8
hm8jQvtObKu3aN2Di0fSBE524uyzkXrqHvNydqqu8DKO2+SEoV3pT6qqjtSPXdbHhiERjkRPgax1
AcLplnBeoDw01QQKT9NQEnW6kjrgWtx3ds18NJcR1DfclHCfVUpHAEgmfUKZSc/hwSnhQlNsFD6A
43WsDSqGBmFMoRUnuC6y3f1VC/sDIUuBEvrsCfYNSY2Ze9/eKQWDe4e/695YbnMb118XjwkBwO1F
Q/mpH7/rnDaUMEQ3Q7M34Y7KSYRfKuFWWVaKCo0wdMzBPJQUsaqvO15xXP6E9KtyzXTOFmpmIFAP
0IBTls4+XZWKZ148Qzw/BQH1AuD4YFvCJh8zoasULhBNWYZn91WUE3y8cUvvgOR7FcSRPNKXsVzw
aMHXPQC/xP8jHvGMkCQOLLliVJw+x1RRnxdGuIyvg29wvlC2908OUEem2iKNbjyzfg7RrJTvpboO
iZOHhJUPteLGkgQpr7f/QoI0GfRYZtbrJnDUHLfR9s9eaZmwg0uURUoH2raI+vMGbaD/jMIBLm5U
0dGREcuJBjF5rwvTgrSSI7HdchWT95lXld8itioP4CR8uVylhAeFIVgFBlKtjQS6BWHHchDt1WQm
waw8gYLnA/mmUPLVvT6luFG9iFG/ALTcbGAk0uzDQvCbPu2201qaUTjtnTZwQSv/PLETWAModaFL
H2bQWuJC4AQM/PH8ro2vYyCmwySJUJYbDGSDiZjOOHuvP6Qwo+txFzGanZk1ybKwNjFTaGCL49ut
J+PRpQTnTOtQXEq/3KVXF5c4qrjXOeZposABokH32dTvcM92d/3NF8SZEOdiomLQ9Az8+pawavlG
j4IM15kV4G1/CwxmDhmJYUym4RweCes168rLl7Yke9R0jOXq7w/qqPSHe0S/zbwJYFv7lFA18Bu4
kJ5RVgPWHVHtrOcvgFDg2gTwuw7IUgXU/1pBFYjCzZBb3bAM5SuNTCadDHWvybCk0C+Jy0ystcFj
t0TKUmxhxEA6SqNyppf5/FzCDEM0b4KENRX/2ytvbcIHMU9OnT4NtmStbFI9aQikj+GCipSMRs3J
CyvopP089oBmTAOO0cg2OK/fx0Ls/axVZuVHbzUwzBVduH4C9QLgAYi2mrpzO7HHaC/5SqfDyhJL
CSUbuqx58FZlcx9bgBUZXdUJTdIczYJnzP3AcDGxlOI6Dh/dCoDwFs//DTLHRYp+MJFPBGcC9rzO
oYKtsuIkqYA5xj2rJ5d1bNxKLa9J1O8i5sTUinUn+m9XY6gIWxz+QjZfcNB8QHfRd1dwzFsRwVFR
Nh1J5raPKmDMfSGYBZr78RVvP5KdKFKAsDpznpaHRLq3RVmlQ3RCBOwCY7lztgFw8xET72JBwso7
T6ZBCZ2sTpolaABpZool4Hbu+GJnxA/KhSuI/cV7rH1+6Rru+KQJTNZFfPecIzGxtL+ToO9D+0Gk
0XkrQsPfLKNyEyC3XnpFfKk98h2kcFnC8DveB1WgB7o9Np5Q87eKxadosINDg/7tYxR2G55fheeI
N39FI2dJ7ELuT+sPTATvQ3MIrX5OU4rllzJ42Avq/jsr2NevPn0CGopNJc40K/QHkE5fFjWWsGpW
ZfNI2H43GpU0RMIBETGP/wbzDKgfrV5uu/7ymEDKm9L1idqx9OTxSLDVayOjf/xVnrkYE5+PDJA0
Jgo0NmaNFpE797fNaC7Vx3uUg2dnupdZoN1NJbT5xl6ywiLZHtMnNt4SxL3uEUVtaozerlcpcG/2
8diOr5MWFD0r5Ap90znVYPSAJ5QkW+Zx4rviESlZr480OUrKwb/B1BYmSdYqA+nwKWFxlLdJuThs
QWRugnZEdURcbLcJM/Y1DeEpo4XL899HF7iVBP67DnI4kTterganglEC1WSPMcrV2qojP6P32xLb
kqx45AOZDeBJRJvf+nxavAzHqXkeWTIpZxdguMVnLwDUf682sHev4iSF0cHiFd/JckLAlBqoo95j
9jNoMSvwdn+9aq+YXdDKfSXLrmXIq0oVQsTtIbsOfKmyuzQzQnGAin7szNy8M8Z9+ZnA94G9r+sd
7np1i/Ukt90mR96K5/1yJWWYqNja7gskpSGzhVu/uJdw5EsY+hAE+PmZ0k8xrT2CEyxNEIHL3Wrq
jR2e0UGWlCHyr20S5G15lPCLyPbyxx5lhbDJ4KcIwV0MuLBkQL1+w5+gjmaZCCbkOLOwrvBe3Sv2
epi8IZxOGdtY+dAZIksm72rX412UUzIAeM2FIbgNCPsaVFiHGxm+9MHis/Vt8ZlJEdeyDGPP7cH7
SpGEHZ2qE5rVDySnGg5nZ0VfN6qHfzcFXFV/rmtIS1+QoPiDeIfHpDC7lIGL3oAdRFRJjF2hQHQk
98te6EtRVuIHs6cAVIUFpSAyKNgi/nC3fqjUSEsk8AjGlaLIWb22JmxogZVg4n0Hqi9jVTM3KiNu
iFVdZAbZ9v6nR6fxk0kYYcDIyZZj3Y9kuiREcB2bMsxzw5a/e9qgJcVbaF2akVWmHrf3QwaRdRnK
Jqmv4QlSGMXY6wZN8dmSqFbiph385ejICBdN0GWrYS30rL7Pt3LlY5zgimYy+EW5BVXZYAIkiLm/
ooJd6v/BMJ8bTn9GOZuZOeS76GayOC+FajKdL/UMz2qqmI8uo77wtFXIHGraNjOBBCE7OGS1+kHE
FNV5AhcAJI1qz3tytzOzDAubuib1XV+JwjAI9S+UUZdAyU/UQoEGITUgqZXwYR7QgsZ00IJMhz9F
b+OFupC2ZwGKxLyigRF0UR05oKuGL+lElM90s8JEDL0GoVM7Ek6xx2OA1OM3ijV9DgW0CGk3ERnL
BghAB/9nUMsbq6el3a9V5yJXjpuvjm/5ONi6C1Pz33KuCP18NqS/HMBxsupu+utu6QEopVN8ymsW
qjsoLqJJ4eecHLf2DziYGqC4yD58gahnSEw2VXHbY33rD7qxFlmR71kEG16dJCHY9JOzzbw8IgXU
dxK/36SVkUPO82ih9ea0c0t2Vp8TN6pVs/qGyMlNQfzzrAvVZkWV/QYpFiyXePauHpfB3jnIPHuC
pyH2bPNkCRGO4DvJ60oW2uz/3eQztts878lthEentjb84Sn3OiSVA3raAOgMVI6wHssXcxkfCAAi
1BI/CO2U6GRVJw+yrvqaBzLNZbKlp3eXkLm4C9nBOImgoPsACCR+0z6Yu/zHCEitwcegXAdMxL6X
2gARlcibhzXMQhu2e7vifY/ri5MqP5iybG3xLVbk0MLG4wRbxyZa6nrqXg5hjjZ+rNP0z8L2NmW0
fR9/lVkm5ZDaXR4liWVoejA/jChY2WtStrYhl3QLyc+nQWwTLTIFsYn0sT6kiQovZnuIEE6e2yJ6
wOevQtvzEQ7FwAbhSQSMM3vpGWXk2jgvf4rLwpLWs9dWIvLyTi4opAOr2uP1FLke6Ra0vXB8zqEs
KTLeN5hzP+QxStQd/lfDFZEagPjL35kYTnUHEjOa1PShlJBsv56D79D/Y7Z/t8WEpIQxzwPFXsoB
earTFFlZjlHdAxLO2FCHvijpvreYSkXaNTCq2KMZO8ayrPoLIApPlco1Zypo3efS4aWyQFebCK/e
BZNZmkDNFnIPYZ0hVkLtEw0VQ0Vnf9zCZdr0JZQOl+h40ZD3RdWgALhIKMlLwJ3zTxYXzAVQ1gN0
9XKxWoK0hfwCFzD26Vd55dLQri4iiSQDyPo7N6r5nEj5M7fqdQOiPAe/eO4UdDpQSvY0pGb2NHdw
W1/BzLCQmbfxbF0HNa90VrWY+XJqY1zUMDja2ssPnrRutfEl1wRmc4rb1EPNRHHi5bzx0r5VJ4Sd
FMYe30F0dRDzmoZtcRoc5+5qdoNGh66zvyNMUuSIzAHTbWGilxbgZYgNUfZTxQxQ8eFLu9qk0F6y
06iR66zMBQ5LxkSadXpq3GbNtPPCTvcFZKRhWJ5IZuyPRgGQAAF2sNGUNZJiswaPx+JRqtnSD1At
IZzPp+JyomKB+x+Q6BiRmeT2V/hJgBpeF7rVEOyERLukIL+cDa1suuc2KIYgXjomCO6Ko4YM+RB7
3enehFUfqDdBl9m2R+N1dAC/8dSlRxOIcJg5Hx7oTAvnII/3fR//shheNjTlxT7rYtx+Y1+dayou
+oE6qx7C94adOSByUJva6+miuv4xFbLqG+o6xg1Eq9BoNZGyBs8ryZDSGC9p4QaTo+KgPPYFh34p
6Vk7yKh+kODAGq0MqaOTzfVbvkeP9J6+OUwzuj1Nlv5qzGScnpkUgJvkdd5p0MtCr6BwsNLoRbz4
ViMNF9XTUQTNWnIS6P0XEa86pUzaA/KFg6s8N0KiWBxNBZR5GNDv/G6tTOJ2i6cswHeuXCKgJthw
ip6xEKl7u9wYCDimqvc8718GU71VhjnvSNZWE17AHDkM/lPtR1CIwoS1d7IhQQ1cSp1sBm2dcDH9
jMw5SibmTvmaDTB0wPmgm0WCVCKDxJ0k0YObeWCa7yIXlFKgQmBvsaDHOExLGTZio+ncv5YUq5Dy
UjRNYSWjpoK+u2/Rcry6O3dwnJw7rQ5xmnE3O/ir/gx82GPCD+s3RXCxeReNyyn1sh6UoyEPvat2
1VhGQJglj1PYKF20S2yn1O7RX0R2xdJohleaVTZHdwAGyIENmbXgthV5Oj8x7W57iuCou9XAQ386
H7E/DrjCbAtS2ubzhkFHaDXPtc6iWnj/6EHyW9DY9R3ugzb7cmiCDWvwM/5affHNqugMONoRGsOc
jcK9jd2R/+hXQ/hQPeSfQErEJe5kqBh223Tq7ftVakDAPP/Wt5aVw2P/fvXQM6/cDiujixpQjC4/
BR+06E7gi0IoyU5aH5cJnt7fcWhLYx2LRjucP1yD4/gaRqE4IpIZhqmQ3TtJ/veaPoX5mOy4Cb+U
BN9XeIpiC3CfdwOtqjfik7MEX2qRiNhJY12Pic5oaMm4JAudy49R0LxKt9sSXY1bQIxEZ8ZG8Dme
Ty5QHOJC2aCjjCx5rUD6Pupp1uv55LC/EqlrHZwbDp9fKrXOIVd+qvIEpoIy3olmgm7p00YQmdE9
Y7ysaGKLHLkXkaO4eRL7aGy6z0vjyMVLrHMKlZ9k518qkqj534+m87weL4vYNbQfJh7We3dcZm4g
nmeLERx7YsRcY4VdPB6S+QfKbPsLACHuERDEUfh+dmcuGqoogRculQuh2mAn5IzurM4RMsB4kw5J
KCBCr3aCFhab93jMUOfS2UG5ejUuIz+Z8UQeGfwyVFnYTpgMnZSHmD0wIElh+2NIvSBRnk3XEU4L
4DAuG+c07BzYmya2YmRAEA1e6bBsyzCybZEYyOSOT6es/Wvr0QpSpF3bT3iy4eZtRE58wNkvCv9M
SOEWBH8kjgSLFct1cZPSbnZ1J6aVm0TWVwv3LVqDw6Qgjgj/0gJbTFSw8ZxpXh1cGU6h9o8G5boP
/lBLrl0lkgFIN3iOaCzMLMkEdzNSnTCskcz2EPubW/TDORG1JNFJTr7f52VF/TcjnFZ3QpRoWLFX
eYje1UTl5+ZVGtSpRQLpYHSM4wmlzzl831VXTo1aPEPKdSFKQVyuBqaqFdjKh3suNXOmhk2NrgkE
jxZvQkmiL5uVjAYG577CtYrs3WXb55UXd7zANeax+k0bj5N4/XfHff66LmCUNtb7qMfez6M6ubhX
G9+Y62SLnNu729KsBmcemCP+8rTkjm8/yg3dngQfUluH/Qn7jkciTTFN0R1tvt2d/R7JOJtN0fkO
T4E+jA3ChMpUpnwK89KU0B6zaM+nGT0F2EIUXS0QU79JUd/q37Mp3r95IhALW9SLhs6wlMblYBrc
V8aaG/0+Q79eHaKG1EgHGoaNfQGwtcAwZX7at2hpAdyI9TJkI5B639QXbizkuYLbugkBNGO9RATY
SC23jjYi7bG+m8meWQA/kzhyQy0m7Ev4sB7rKbV92zZ+1GRVJMiNK6ZJVPCb3QgYx/vbc96qdyro
ErLmRwpWcXngqHtM4jU17JGaFadwycl3vRpo1zXn+fu6R7YBXIqhmUc6ZDGhJ+C2bFu3l6hDTdOA
tawmtDlpm5s9x8MTG6vJAbC3HWG0l0Ae4PHvAJgvT9OAZZmKOjcmLYGfns+gWozP084eZyInq5dL
of/B1A2n7vsS3qRbJFMh+PgkXhZdX2p+pWYOEbinFPK86lM8epQnI+/7Rc8fw+GDz0oQjr919urL
vVowFfwqknIDubz69/IU7BzkyyIcbwan/+vUNk9eJ7tsworp1CuopivOv1qjfTKqS/SP/FA/AWFD
oiONGgkU8zV0aqRl8PCTMiuAp+BzwW9w4I21RKcpC017CJVkSnWhnRN1cEPe2m4sRDVC+IpsTn1x
lubHDD+zwUJSqjxt5dJWdPCeog/XyOxAzc9kJYxt6DhJCFfyD7PuHP+ZuZ7iORrYYjxvVlRg1WUD
Z8bvdkudvs6JtWBIUm3u5RYczpxMHTOAiang5HUzocAkRJ165jFD1hyC7PUBp4G7WOFTjHcP+4lX
JqXe81oiAWaQpz/2Xd3wme2xDdRO0c/CW5YOnhhAaegQf2Ys5p1JzYBtcQ22fxK2SgU5WqUGxRJA
oBKfCUOb0IvMcTEVHTkSkEUvIyEG4YOQMJmjCbhzQjbwYzJ3oUxesI5upX4cvPYDKIyPPl94+WhD
ENVtbs/J3AdmciQTTke+w7bDuRw+G1noaMV0ZFdsGklOA3SqBNd17be4guf17q6oe8UxZhf0Bslz
CA+DbJ2IsEWDwzk0EHN7BSu6Epg2pgYyMk9Wr0JNBPCc3yAqj8MF8o0kKsWStlqPVyI41YWuIBLX
5fnmD4602VTCMjV3lNJMxw8/RWGsDknVg3GVQxIh5fwgFBOxMGsUGR+GLXHOoESwtJkPBJAvbBmW
SU+XLqBdbhc+sMO/IShNhfZZmkFzkG5exXj3+oQw/ZU2Sp49guHx3Mho4zvMHvS/qaB8mwTHHbNV
iau86XmdIaLSn6QCnXSP1wr4cGVO0ALAg+Nt4qE9iP+gXzaonSZKjFppg9cHRy3Te+koAlY0pkhS
+eHkRrBokU3iKAk71ID2KOk9Io+QukwlJo7fKIqLznV372ngAnbPdLql4JHvgD2x1bKD+01+mJa4
2kjgWdLqVAXmGviSa/xTWJBtt1fUTESRxy0ydzMVkqqJAeQbTq2O18iWkUGgsTQqKjz7A5Pwlq07
vKBzpKn34t7x373+APKOlVh8PmHhReYGLplsBDsV8gW6NnH1yuIoEYxGyEce2tlfkhYEJnaIFAWg
3AknQ1tNvTP+ouGERk6jS3jfouUvfa6ioghVF3htf+5QHhklHkJSZnLgbayeu5VBgxD3lR6DfJFq
hRcqz6OcX+52kvrX1Qzu9+l7vnY2N4r3Wr1vWEXRJs+W616y/CyGlJPEvbJfioSPVQdTQMOSJInp
LeGtkrwsJkMNTPuZWNuv1XTjHM2huZ8IsyGZXHvkC8nOTPlPZ3fj62XFsVRUD1JPIRhuCA7KFY8A
mK9hHAOoyLOSINvAgxMNT0q94YcLnH0vmvXYcKCA7nEOlxnwTWNDEALMuxZ92QGThFDm6XWfi1CE
FGYZwd1qPvTKSCHDVAd8BPkkamoxRdslQecnzE0ooV9W+q1bCJjWIk6STNqcDOukawYzsSdCqd9s
QU/g7o/Czxt1Y7jZgKWUvTEQwCVCo5ekrtpmYovf7eHrjJUNcpT27Rw+YMSbqZgAxiom2WnRFRxs
0scnohuteaowq5gCN4HMhQSdNUBgGWKmqdVCcjMOmhZl4WVvk2zZflmd7iMXnim0Fta4uIfCiytP
k/xgVa+UtzpM85+QcQ1mgzNcPjEJ6Vfpwhscf1k4roHGmpV98EZV3efm8W+j0VJFGdqNJVfskMYo
9TDwd86zPoVS3/Bdb4JWUmWYrZNoXN/JgHvNWd2GVfljD9MPeij5herDTdzDBuNBfBAJq57QpJwB
+5e1imh09FKpGb3aqkiX7keTD6eyTvGT55G/GMPsJpOL0knWTbhtaWcHBT2H6NfOK+P662+AoZq2
Y1ewpdjU6CgnvALp55Ngi6FKsIoxGOf/5/N4U3/Q65bBO0GLzxwSJ+DQp7BEDm5KLNeoPpOjdg7N
KS7hDw+JvFR+/D4snNNNiXZbSUyjOMVHiVMrzanfmENRqMIGiJ7/ngTRu3+nvHZ08xEiepyCoEZM
hUYzm7T66A1UUTJ38IR0n6+5REqheSO2xgdrzNRUJLzN9onhtEi2pbJ+PYTTwKpd7PqIAFM0Ge72
nflfEmFUCU6rCe4MrHWGjPmUkykuCtRKWHnO6uyxtYgrmrfFSguv6Y4OuTa6DT9b0wkxDkrrljE0
H+M5DMYJTjJN2/GT4lPm9dOiYyJ6VuP8/lr/n3eGxMn7T1BLYvI3oPpKWOtDHCgdejREP3GrffO3
O136dwTcZXPbUS2IK0h48ANlXiCEuPkj1GHQ1Ri4Cskvo7tGLwGSybRVH0Wk3658REnEc1beqSA0
x3HAiLE9loMX26sqr/R94bugTNqKXzYtHiR7YkHTr8ZAc7P8w7c4a2b+63aVwJcYG6sBoxF406w5
rmWTyMgzq936gDuzp6G87ufJe+xmPcJEff9tR6G9QXSrtsABFmImqT4g7n8t80P4X01EEYJqEnSS
vQIgvw5u3VYYUDmwVNZDqA8ufil0yLHaMkkHYG0aNVzSmQ1Z/8t5Jsl9ZEBYVs1v1iNqQQ5X3+xX
oBVtRjSMKhRrCYAzGrSSxK0yRqwfOHYTbYWpu7SlHwu8PA5pmYtHFSDqRg4EcDpQIjFcxIWxyE7r
LxBwdRUcr64ZEKnB4Hd7cIFOsX2nczcdmKclzpIHWCVr2U0hdcpYwbXbpoX30XiLEFUVXYCuM3Ix
f/FS2HJGjH0yOHVqMv/6lMCeHKSFUESGjekfKB/o3qQS/VhiqMNer7Vyol8gp7Hguq3sHhHb0jGA
QhTFZ97fMlR0tRcXYUvPaM+ZjmoT2utcOoB1rE6XGC7qoUH0yj654M4ttPmXvQV8PEFKfJr89oMz
TL/M/9qQRWR04hWIDvXyvRbmaO9HsNaOOh0TSGC9tchFFmLpv638ZpqtnebxnLgsxTYdAzcm4LDv
QHrVviw9a4LQpy8j6TgIrPbzm/ed21KBRBMN8MaEL+gUUokiHxVAkRFomN3cGUC/KfI/MjXNCDLB
Zi2nv9Pt68NYT18tc2JyQA6gNNZimKX8vtLx8Xac4grGGogQoQPZ2bnvxP/mNdJNBQ+PKccmPm/u
Q+vY23AIt4qkJig+vWjvmvAtCYWh791G2cVJ8GRCo193UIiIpOJcZOwuxUIQSLWjuKu5ZOUj7cs8
HsI3oJGlPE6SIjhMOMyKX9MkyW8yRmt1oTuQcYpXIh7UMk0QxC6ICr1HR67E+88f1WtWuv2PKpec
7fiv5F+PT+YF+XXWC+kPscI61Y4nPxaFlaxaHWPvTlb9itk5+jxEiOQHy8ybsTKtPjeJDply6aiQ
xeKZWyCQ5WV7nXoOOzF8y6jbmdCJ8DH6bRyHdrXzYqpsIERUlBmltnPBUlGehz92TIIoZhZPE1Y3
JP+rR/7yKLuT5wZHU+btUxSy4giWjOkQVLpqpgya+v7mWtqpuY6cRTOUrD6tmdFEiv4GeyuZEKjJ
KxDqe/tioFJb2R06l0ELNtozILt3eZy4mG3aYX7YQLRzKkpvLQKWTTP4/033U1QYiaNyU8/+mYYk
T1/zz4yXmWkq6qIkYres+28r3obtlXkvxueLH01mzzhfJlj+d4/1kGmMzuyk4YNiGDpu+HhYSu6L
fx4KijnzVIEfXB/Y1LEiOcfdjDafs28GtXi9km9bElIMvDPp/9Thhz1GvO11+W8Ha2BBnAAU1fQa
B6XeDDNYrg2SY4rij1NfLShVZ0AqR8hL7ctFUSObtnaIPfj40Fmk8BYuHlsmBpmteMLNnHwzESsH
IA6sFfMgoPLNMGC2OqAUu+4fLnSG7g1ZwkntO8y5mkW8pX3h7YJMZg4C/RRXwBHWeF7ENF+ywxiL
dOl8ksMv2dYHIOaDKkOF2p1V7UeZLrup0qvQo7ScOTLP5184kn3EI+Y6QN1pqXRJ1g2FNRNaPqHj
UnXUbZZbwc2ywKZIFlesKtIypiVwv01QizH+cKt9EiMAxox+7S6otaSM0WcS59zsCdJBoAuX8/gW
Ok2dgDb63QZxM4x0j8JR0BsmPeuvwZilmj8sxIcmCb+IhjYf6xTNYfXiOjl4VqWidnV6VYTBuJ0c
WDFJcX5A4PrR0hYtKZe2/2dIpi1TuDaM4yFvZfiqnwvyTi4gDoKANGFAB7CD8B66trG8WYN0nsPG
1kr88N4DNg/g0x67bIL/YI4Gk9mGvXS0O8I7lqu9vYPx9bqLRt30sUDVtBiTygYCURrXyG22Tkn9
u8GOxjQP8V5zHm1TqoKWMBeoP9KOsznD+7serU8bidn9ff554PHZ2tJGb+qYd6pGq5rWeJtgvxPO
GLuunChqSVVkCCMLKJJeSx11+ZulvKKBsjvzWe/WQGy+o9Rv3Y8qVlTnlV/1VwmMCHZVSP2mtriE
xIpScJpos6C+HHhkIi6e1ziWMTC5MdNd3NmgJi4d1ffUJicTXnm2YJdgk/q648qfcqIpGjFzrCOM
yN7dJJR6B8Tu2MrPpKNvckc6AcB87DltYpb21VhVVIVM9hGay9/DFW2sdTMyCNZy2ccostanYG/n
24DZxXg3N7LC0f9OVtFgbECZRcJEiG0YA4DOCvmogmlUa2RN6NpjgZzvx/qQg3Jzsl+bI8JCKBeT
4A+f3M1naJB4lAa6HAZQfHQKW2vQg96DkRxVhiWux1WVRR+FxkkQTG0EjhfdOszvEXbum5frSMS2
Rk8LySMlMjaBZe8H3+igtCi2whw6X08cLAanIdzUA8Cx6Lz4BgQqBg+3nbTii04kIGLHBUpufHYy
t2IbHS2MctrMUdfS0zN4Rz6yvV7GEkXTAI/d32eGVhfAXchBhmT/Q1jtuArfQHqdDZDip3rlOfBw
rMhgw4Nf/NUZUXrUB1TtD+RrLfjYPTL6xBFX/t28WkQO3wTtqfpMpFYwYhUhseRqLAH/kow/mjWw
5DJPD8kX53kYl2j76tDK/U0+vzY7a5+6GtVzPCWB2mY9lM4MjSIZQ9oZsu01chQuBTFD1FZkviq/
ypfoiPyDVeC8I6iwGm/69ydN8Thfpwk6rTj8+3+zC3exxGAekEjGpdEeGaht1Nf4OFSCdqR63Bqb
jO6Em+Z0bjrEqT+adAoy33miXXRS7v6ad9om5ogpVCcoc7aOU5sk5llGK/t2Yi3EtX7uAtJ+LwEg
w0kBCbRQGAgbkeBBC3ZvS5nahMQFTh6tDhV3e4O+zHhJFxp2bYhFTILk6fEWZ59KZ3hP2dKitvZy
UWcP99klvKCe/yQcdOlkseN3Z5kDx9+10ohwuSNLfUqN6SBiAq9hC1wMhlJo1rLnpQiD8tnmCtAl
1eIbYuBtmdhT5JV96kFLbxHCnjt5Rmm1DHTUus/MJ3Ri8Ag+xftiac4fkGvS5a5jpf8VdRA6lSgb
J9Gfe5IX2slVpHOiI6S4oyr/8/M661v0OdhND00J1NLGkbP4cAmbvH1pr9jXQ5K68bEbMcbWD9ij
JE89JsZFJyJsuwA38DgyF+vnevrNQKX2Ma5TXvnetraJYNlLlAfzovCP2tVbNn/6hLvftnrAHcME
8I5xJqas3RNe8a5rvrwnkSCVcvGdVHA0kK6Jo94D0oPLU5sHgu4tOQRo/A5c/9XszZcfVGMzC9tT
KASSpDpB5RzFOVZHXXxn3oW+aPQCZW8i/dK8E87vEWyuxRPP8EQBbDU3PXW5704xy9Tp/bPxj+L/
Lo7Js6qxmk1BddfcUfgbVhJ+ObbSjLIskXVrf7mYAvIxwmVos737ZvnG/y9e7Ug5xkSPJnoxNJ8k
v8W1QuxUXN1nEnDIPmKz88DER2tlJiSMSDGMFaZhOoSNi+Awx6cPWpFEfDgOrtWOFCHrZ01AvTCo
ogaxnydUe9kkj1mmWx2xING4J4KLb5NLnTWAyQuHzxrZQdcEkvxUJXAYZ4HOxap7gt4XFbHZci26
QkHm0GnuGkbjB92R/zpOIu/JATuMf8NPKTb92LzYtm8B6Ktrm3uqqysVrcHbjLwHybiHOvyBI5Jx
MbwauamSIjGZcoNX9l/sH1dZGbOTU8MhMDnjnnKFkVXfgYUzPzXzhFWj/JVjz/wJs7nFPTaqmHSP
9nzRbWS7mm7o0oH7ibbQJpb3H0gmEviCaRvIMRwf/fpzfOHIwAGfAyDgrO/L7+hwZcOSNchtCOov
0LYTHRYsDZokrapkSIby2bOOwE2u+j9F2ptxHhMKQQsoa0uTzAcpcv88l7sa0IoCVJNMJe4Vwtr5
XHLMcGXwdAvAL58jcaHOQy9nS3VAI67PhQaCjJRJ5v87KZDP/AJ7VY4RqU9DYqWXqFyFhRy0A9eF
mX8w2Rw02CX+BMeuFFYQrfUzPJL2V+6LNJOm5pMlDrQHAMhSsVhNXNvTF90Sr1n20YKYSq2RqvVx
HX7bzbBjOqVtr01wD5JpPOq1T2tcIEZp3SytpubO0aof20mJDe1BRnJSAl0FYmnaDBQ4Uh1R8lEx
wVlp+wgkr9a6beXATIipBcerMf0tQIvTUxx1LB4EmpjXdjOEiS/2wlPGu9nl1ZwJF01nrwdtxNz2
ZUKJmN7v9Y0BG4msOLT57b75suQjBAQEju62FK4tekPvLUVMY2t0CiO2wmyJG6p5nxkG+1+N0hO+
fVtqRZH7Ijn66hb0XVCNHhJcH2HmZYkn5gCTHoil8vlrfKdsG64I5JoGtR3bciHiJ8P9mqLauInP
RW6Jm3TPXVdbwmpmXqWFwjvEaRehfIUgBQj1g2YLXb0acquV2+Y08h5Ce0rNcQZWmdhLVK4E8xvX
hzYUcdehgogox+6bCg+xRyfWmnzUJHD0L2MMEL/JouyFCXjJpqUDHUKxgVe9HMdllk0l/6S6dRF9
FRfXzlVGGWhbtJtNs78IhKqhpwbVM752IJhMCnMc8NzxnGd47/FwFkeZuWvfVvEYvr+Of4dzbNLj
IhLQH6E0meWcKY2SudBoyPd9wJagb4YRQBz0M4waaaLrIFYJZqIJsS2Otdq7kUY1E6reT8FdEED8
0Y1M3X+aTexyxLEJJbfKAIXmilMB46lp1VBG30R1VKV/FuuE4kq1r4CxhRTZ7gEqX5g+RrXqpGk7
RzOCKPgGlz/aeLbv/GmUt6riehjeGqMh7IAx6z7CyhNRtHhDO1cFkxQOcsp2imzrK28nZGK2VpSN
qmJPP2lUHRrfzQBAhEpibOdNi0P+4ITJf2FM4wdMmsYyukXUNBjmyGjGO5qv/5TAeOf26EauRQ0S
DXvqIr/UfavquNU+ccu/topOC1Yojso5vJtH7LdBSvGkl4gmXyevqhHTu/dwQxJYU1tGR5lHri9m
3J07jzJ1GaEw7KtnyniAjaYQqnMhRXUf7IFfHdfMPPLavEoUGOpg7ABrOBVu569KXJTjaHzBk+qc
5J7hTMk5mxTENxpeOMdQ+fliWLa5cFOlyU1eLFzAWOfZhK1HQLKi73yzv1QXQcdZtuN+cDQIwcTr
4lP4N95SYX+XcmzOGpxokRx6R6jHygPrffqSDufvd6izyw1VaOD4LYGNZVZGuXF/I0m4CCqOFCnu
tVGDYcZu9usa35Kg80/ymnCeNe8MRvvOt2qg6R/JRlx9d+6XNE81C/kVN3xZsvNz9bD2l+2Mxfke
lfA9Kg6RWEHr0XcreyVUwW7j3zW+nzZFG+4N82bMmO095jLXOCZgcORD3DqFETmjwVIbCJ7llimT
MNwTZN1izq8jXANVzj8Nk97avZasone4UKBjOuJnIPB5hbRjzLg/b93Sn8jy/gGXNklVi7OpiKW5
eZEGV7fBQjWbW+8eg0Vxl/A+6ckr+7tKj2grxi3sLVrJBZNgmcoRbg05WWH7+fTmrEBaPE7MxIXc
RLCguboLRgYbwWCw1vgyQq+zO+NIMsugAsk1C3wY1wVULFfYTvFxDkeGpisLx4ouNgJo7mDX2twz
FqlszVMLk4cX1MREeilbsKOmwDgy+KyIzM6ulMhklDwEJ7vXkiPjRoxbQ1JDiD3BuJzN1wyYjdt+
E+YqcVdvG3rnFY2zsqyn9grVuAI/2vy8wM8xNzo2ikZAj+tdGmBE8yB6iQIuG03u93Y5EDJcyHc8
w58LLEnw5iBe9pNJpL52aTeB6ASVb4ELDXR8VVjCwed8IH474z1wpKn0zRxB6+b+qSyojbeMhejF
wFxolV9+wHVnjVUYvPolAYLLOImC6YmLQAsiKdtz9OI7z9PIcxbmBPSCJ6+DvhG+p89S1ZrnLuBh
Z5J+HMDUcz1KbPks9fihAksJPfyYNJpg6GmY5XSi2ewp8lWGH58hr+uBQJvIY/sXj48=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
