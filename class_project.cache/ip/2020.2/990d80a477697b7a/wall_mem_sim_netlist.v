// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Fri Nov 19 22:43:21 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ wall_mem_sim_netlist.v
// Design      : wall_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "wall_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [7:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [7:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [7:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [7:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.1884 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "wall_mem.mem" *) 
  (* C_INIT_FILE_NAME = "wall_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "8" *) 
  (* C_READ_WIDTH_B = "8" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "8" *) 
  (* C_WRITE_WIDTH_B = "8" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[7:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[7:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18400)
`pragma protect data_block
Sven0QF3wcgF7Oc+uMYLagYm6l4eGUP8BrOP/hN32lyKikDNHbgGiYDzdQF3FKJgOU8qYjXWGPM8
qzuxhLK4fTPrralN2a3H3IhvJ9HRJpt8xPPUtYyWikWrM7T6/f3tJ/g9IJm+4CNUzXHQ2p3PjJG4
05s8D5Dlb9t8teyflz20kbr8x4REkVU3uEatri2idhmQuRMhhnYcyM8Pl/1OeVIK4TFN3kZ1frcS
xj2m91il2zi+zesDmxaYqUfokVC08KkMtPuYQJJpCGgu/9i9E/eaNknFp0TN0uMn66TNvhctSlnW
EHT/7gmAJrpdTHo/K5AEwnUYtAvIfpPJ7rNVK5FtjtAtrz6tetilmElpPKTT8RxmjlZfTdnYvhCw
wVQS+vmYhmVzsFRXLk4oQb7O3tNYpcLYK/75BUKojhnbQ5IDcb1+sLC1WhdtWF8I6NT1OVqNlVZh
ogWYiIaCj3Tt2fmI7NW9jqJ4YCeq73BlrBDva3yRFrAcaUiamZuxgorqp99SZOxdNwlPXlgPf70r
RJEmX02VgAw8xuO4sr9/3Lu0bWCoA92nwbAvOrX8xIOVOYZ1Nymefu8fnizbg44yslWLhjlefhJn
P2iUF/nPukF77YWapQ/e9sZkPkWoqfm+usRS7nXeX0uJ2dFr88zj5jNn8xB5Av6ypZ9CmHMhtmHo
KwfyG2wmHb7/Az/5gZU2g8nvDL1z+L/MqX2O5Ads6f4wrVQmN9hbjh4yr5IlMRzQR5GHfsDlhU+H
Pqp2Ch2OY1/mUp6gvCE8blYiyf+tDrFOeM97T98v7YzY7KuJRimt+Cw1pnKUKSWwVtCOlCHPQBkZ
7GZZCYw34ioyH2zR186Vd6FrB2TaSaPzfeks2iyKUHVrJvrcKdjpRxitFTyOjC9N+9oMuNaGzf8W
UbFBTjrF7vHjD3zswT1L5zMQvlircPctiguR76CRAXGfrRXWhDbWXfSrpYCdUd8WCNUqk4bS72f0
m/d99CMnFYgnvJbIqp15n1mSKeEOYfE3BWCM5mvgoSZ8P4XIQWV0RVzYcY5vpWfh3etrNzAAzffb
MGt9MgTnLU8P8E2VqR2DuigLBPjGWGg8p+en5IcrsfDf+/lEpil1l7Zy9wJ0SW3K2/flb1FAiReA
XJkeHCpl4r0s6HortWfKYIWuKOyu31OOVhEKHOfmwjj5hL5rKkg0QUnm4uC3YzVVOMkI1ZkeB/0C
ECqDJs5hncyGKtGsbmMEXiF4q6278kavhy2Xtg+P6eWS0Pnaez0rottHzITlbYXtJRkH0KXzu1FE
NCWx88jVF93z7g6s8cILW69q0uN3cTQBwOYKZIv6/4OuTaURCzbUKn4E9RCmRXUP8W4XSWPkW1N8
J1ISwtKDGwIfZ2aRpst2ebf8msvyR34/EjN4RvmZd7YsV5XmPgUtSfjyvf8gnI4Yc2t7UO3TGUTy
NhhCyapbiTYbPQk4Uj6Q6REk5ma6J4TVQrufwiiXS99JAQXR74YKXb1MFc5WVsOtcHiFbHFAIYzX
X6Z6g31J6FYocvzoko/xef5b+2NGNv29S0EBBgGMEeP1igw/rC3euZsplFOTWj9Qer/kfdo3blCX
K8t+Um+ZgpnGFWS0G6Qn/XB3BzMaYcP2qW2564vvqXOQcMYfn9gZQ/ZJAX5TbTtsB30w3hBSSOkx
XZ4Zdw8SPpDy4KbmOlBVqSHVuvoXnAw5gsakIu5IFIoSdH8gtU3vWCUxcithwKN2OMrXZhsaQ3CZ
ZbdzJ/T87d3+Ei78ZqkLdb9yMju9nWDo2Dhpnn3h6tfghcbsgcEGzdBK7fUYtHpWjXBihbuq4au1
iQ4XX29wM1C/kvGX2jodBXasKR1rkA0nOw7+iBmS4qCxUJIh71Vuqtgd/Ikt59tpLu+OtFGpn3gk
yZti0w9PcjGRE6oyZUNVlk5Lro7AHHE8/ufqUWbnWRgOGiVZ8sSY7MN50XSzkQ0dSfdeDFUYRngI
0IfHgfIu9Ey2IRtU/WALU9X4qiazQ9q1feVQYaFPZ1VibolcHk+EacVMAp9S8uUdOWb3gvxblTuq
EYK1/2rz31KRVDELkAsR4njcmPxx3Px6/bl6hApwvE9+EdL8jHfIyz+Dnwv9iR3uaaYsFeSqR0Ah
7fnsAxaGGib/4q90AXBJAaSMh6TdBVPx2DNmrzydhydb4J09hPYFEFzaoPWLL9amPwnkdEz8qdud
G1pHXN4psmIgd4/fY8k94NNf0CeKKsdethQ4M6+23w2PxQFuaVGJs4yOdJBb0CdBuZR+BLAXM9Mo
Nkwgf7ZlWb2WHkC/4XqQFRIvsCZeeCPaYMfVhqk7QI0hdRVoqNMEvWXQIQkBtXIOGJ2mysnZIazJ
vpl2vOxMKGC/wN/X28dgRFySY7aNMmm9ip1kp+dSZEy853S39uGald/y+QYbQdo19pT/E8JN4UsT
wkMh5aX2X/+iTdMic+hsI7iygtqXGXgZipLehd5dByDYsW3C0UBUDDr0VvvjZQIscjp8YStuAlC6
/5eT291R4PiTLlkcqVTsoug1CMcOs+LQnmR/R8DfrTb8om7GGdTmaQ4GOMw3Y9i4eZ5MR2ECAnlc
OgTGM2uVqJO7z4HQvQLszyqFmbUnDAEm6gII8mBsY6c7kNLkhlvLCoGwstyA+qbmMEFn3emGPBrP
+rGDLRJ/7/QTOMshs35zqIl+HpFjG1279iq3Q3Whtv4l2J1/6JnREz+lSQdV19YnHG9d0kSKafcL
3z8BkQpVpKjSNDCKvfMJ5zgmYi/PW8pzUbJ2VDHshKsRgOdTUetNP/l2ZL/qBdF67TsgytC9qJLG
USruDx/Sz6U7xm8xRk24qP4CuPtaqygQ40fnrDyGSoMgQYQlHt1RNjHU+2zHk6/PaOz8CYk1d8n4
fZwp2CcjUvrdFRCT9iKO/M7urdXdJ042ApD4qhRUHTHdOZRCCN1EGKTjuxl7cMVE6Dqc09c8Byo5
KdlshfeOsuS8giViHxpFBvOPr5PMhs520H+A3gP+0ilif8qCZ8AIWQwW//VPNnFVT7DUdhkuLm0e
dTmQLvJJ8eQCs2x9dYSNZ5VWZNDgPsW8GjeGNMz1wBMpLANMgwXPek4DOxcP8TOiCpjpMDJyA75H
dfDSnTvA5qXdMBg9govNXuL53pNPGVIs1kMjhIapYU+enm98hXrjbiDNdMeDoMM4cEBWMrc0fSc0
A5ZjZYmLfdP5WVh7n9xlYpkGif0zzEoE+1g1eOG77ZIKnaMRCZ8GMP3RBlTz+XfJ5D40lzp6jFYF
OPhVfMvSXi15vl0lHVa2uIYWKhp6VA3Z7/A8foX1gv9l2GLdphReEZkM2ML2Uslk2JRHDWSBI4Bj
lH5rOw+epz80SHh8DEEvyhpa8WoyQqVcLMZRzdYN74RXnSKV5/KBf84hYWuzl1YuuiPWZ0K+ZgyQ
D/aboaKxCCtgVAe8Ky02NiAiLr2bZOArqwinn89QjAg1hLhoaE1UYo0pnoYmt7Pp9+nlytb6vcZN
ogNcetwj+fLqqLHKIX5GgC6G/qwgu6/ywFRDj7rbrfR8V/OfVuNQy7cacOijXmFbpgCOxU3xhf8D
Yolazp+ix6vDcp5qbl2lBOPsS53vOVFPHizOrwhxgQsSMVAR0/NbSJtv83grkOR2k5KzCakLTWFP
C2lOjUO6nN3dwoHgWx+cGYYR9fadSk5/P/DDloUGrv4IQ0nO8DZaILnnrvYv0QMIk9uMlWkeg17N
6JbhUj44J6emUmD7tWYFvgacZGvNMt3Q4pq7cPhwFe312kj88wmePw4DoYG9yTWquiBAR2WPZTJY
0mR92POrTPtJPb85aFGmGeI0s6knQHIL9XPqquBj/WR8Oivkx2oYra5WBmHrE76AE/YTSM+wVQzd
XGVljXDXpW7YvyfkPCrTMSdbzj7NzfL0G2KkqwdwO8J4BcebA2QtSzer/65vfim1rz4r9wXEGhrd
2MVLpe4F7sRLNv1a6gbJiAoXCbGeeD+vCqhx99JKuKNHkczoKUfhJiYVOI6Hw4oyNENUTZsPCHce
C0LQTn1ahO75l1phAX9uwHSxnnWkVp3n+8YYq1mGDdS7dKzayI66GcARuMTFmnXFBzyYnTgI/Yth
j0aneFMGi55KL+0tEiM0o5OEwesqbsyQh5oBnJMTeys2Er/S+x8Uw0UZHBlCw7+1eIPksj/jN44G
clJVhZU8LAEz8KzWvE4aJo9I6NDwnuW9juycoq8QGc3OIQGgakEklzLx9MjaTTt6EkbfoC2UDMp9
Ju5lu2e041TeCr0ecogHwjXpxYEpPE04eo2WfAez06ICSgp6JDjsxSFDKk4dl94h8CreMHiD+Oie
U5YALgNHcOjsE/kT9GMCcGBJaISpglMKlywRhrNMJq4t+jHwTuWpi4gp08uflvptDJTBghsABuwD
XyDMkeKFM14n0nFDXUxiKPpYZZFs8N5eWyg0BXl7QRnet9V3aKAWibfIBLihfooid3MuLlxmnm7c
X01ThX3D72s90m2e3Wgb5Ia4wLo2eP4C94w0gGJhaEI6ZI3KbL0Sj/RV7FgjkGMTorfi+1/3ZjEE
0sLAesSZ+Au5lP4Bv1JDPR0B1UnWmZc3CGZykBM2nFkj22pQgJ9K1gh5Wxe8SyEE9IcvQ/yqkr3x
FhijgzyTn0Fwb7024/ajeTt7ArO8oZ0ZxIjV8jnEVXk3QK74CWsGEYEs5+kGSBAxKhhcImXNCaDs
w0tvlF5c6HrcKEKyzz3DG3Gx3daBBfiZonEaD+cR1LYGLo+uqfwxtVrAHmH0iIWLUIWUvQ2G/wHL
m3ixgTWYYsL0FwdvXAg6dIq24abOZny6h8C4Ae+ldwAcX0uYKfl7zv/qEr+TEMvpc3EHbHiPPYPU
dkNHw1uolqYYTiLRBe3ucYcZFvfZsLV9YjuWJ/0bmZGAQ0ccxWEET5lMxhqaBE/w7Z0mjPWSXLS6
Vy2oRimIi1Fz2ekkfyp0xU0D8djSRdd0uTZttnYgLCMP7dD1QdnQRAB8OyY3DoPARLwTejrTHKOb
8Mrr4L4rD9gy/p9R9oaroie/5bj57oCzVnBJGMH8onfLGwNIEq1ckEOM54bHE3s3e1WogjNLuyJJ
jHXn3bZtzKvyipYrNOwbm2ChxCXfCkhQbAHBLwOxyiTWMPhMXyBKQFWIhDK1yYWIKIETc0BQhJ9G
DrFGHDDQoKJ9ndsMm9FLr/GCxOuGWfN3Se7vf5Cj9vCMdvxP0AzGBIUPLClja6ShfN6pGrEG90An
Cj1+Milbv3p6g8kNBKv/Nkw3ZEwna6VP8Ci/kURLBq6pCSZr8dAy03A5i2Un66E34TPhfxcuAMgF
9I9BXi9DGI3xHreuP3fNZSCmgIPFZ4msOJOLlQEjt1cQgp6g73CyKg2d6quoPzr+h3/8Cs5rHd8M
ilTI2cGRoZeFA2P4CqhbaSrM37XvZ5NYfoJn2KyrO052d/g+Iva7KCGhCaCwWdl9gFfZWP6DN7uo
CJI28zK1GJ1H0nAagykfYhKwSrs1S/toOTKl1tP4fZYrXpCTQMUpAR+UZv4Ug75q674OBAVq8/Bk
A2ccS1OEYtk8rWvs3t3KkEu9deG2leKN0sMFwYxi2U6lOzDFTlnKEW9bzo2mYKE0rhNGAhbslfbJ
cyHxHgsphAAZXZ5bCPyts4aFVxa8p19GWZ2S8CBx+DvODL+cUTLQQAnjE4ctkNQs9OTGGVqDuP0A
AwjzYt/Y8UMjPim8XtwCZ8Lgs94tYxFyd8LqKUUBe5oL3eoRne4yI+xcI1NWn8WAwBFSR7l7kMDH
sm4K4BUEhdspqcBmiRyiYgneZ6P3ZRygn+NWpEKpcm2D+g9xDoVoMO61QhpBashxqjVMk2DwZhJc
WafjPEc3PrGqsh/MrsDgEOL4fUm2CTutNK6kAZj/WVLjIWW9VabP7PdwxfyIBG3MCRxK9aC905nY
9Tzot4O0eZpIr8ufiopR44IwZB/uOvvW0djbbO1drYVF9SvhNdk0YwBsCybH4Cyux+MWD9SFOeD5
SRLUVfdMydfubImMuSoOMl5INlrlR35usCalFHLtPeBHmLreqbNtaPonJEWhFLsWgumyfNk0lPPL
jCOhKEJyRZFULIiTOxFw6vPVLB4OuZV9CwkfIEwyfI4MQHB4VXxvR+96EMClBHPw3ruph0LTXQH7
oYBHGKqow0InKbpz1OQUZK9FpQthSffFaHm/tftKx9f7rnLO8slvhfSG4g39qa/62E8T4T3JGTWX
WZq5xQgJkVeqms80vc7CFIRfbLXsaRhtz5rxjXpX5NdBMnTea0anMhZDI0FWJ51oos4NjG/ajbB2
dIs6v35N5I+IxR6gKGOgs9W0LRyBt6j+10VlH+40dSpek3RFIiH4bSMBYGPTbrg4ev6/f/N7P+A1
nympvPcNUbdcPId3n7AELTtNwrlEV6N7BsJi1UJj9NGAJ7ISM3XajfC+i2vI7uEtLq7XudRbkFXS
FAfoy2saJ9H6+WiU7uTGZHC3jeLHXoFtws7F9vCY2IVm4RsgKeBuvO4z9VpctlqUuovXNorJ9jRD
3InTcbSdxHFPEEolVo8N2T6yWXwmQumNtqEaJ3kyKfU/FP/Sd5IGNQVFECyKhlmALha9lzKJRj78
VA1/DMkA/vxPt2AFs99cP64HfFFyTj5s/q6eDsKZ0PZIZ/1ADgcmkOYqNJDa8QeETWq3d9Y+lb9y
B3v7G2pZvoNu3ZzH86dfIcgHQC4D6WNHF/8zrctoKvaB615t3dXtXpdN6zj5SZZPde3WUlpujorN
iZfdpefUjyBOtLdKZw8juaqOVvhtsNFT4Qhx19gOlxO4LZ1ZF86JOOULiAxo/qTKODLfkXjUf+jf
LkYTC20JEYtSCnNo27D7gFv5DT3vEpPZtU08AgtaGBok71s6aCHkYyq40i6h13AkixVaZUqBt25f
uavSriMmjAWcx8ugTL04i3PPdL94YsPnZ+DESN0o1BAO5C6DSyUkkwgP5XsKeLpEBTuZ0JneFOBd
CWFOyjXA/nj1fxNMH27F7Yiqe9x+sJJCBVG5atn+yFZVgYSnhRiri+nWaB/7ahbv7F9z34nifBWE
pqutoSP9v6+PELw8f69D3voZn3x+ZYAWmSmd3ivEZRK/JvBhlpfVxrkcX5detixWLrI82LSjheBX
Lm4D95/YEc/b3gC2lDfbaoHSNzW1N0BcNGghfAla6KDrWrlcmK3YkqIqKwH/p58ydO0yhCpcxGqs
RNmRXmyMrJQ9XNhh5GA1vZkUOrEWZBszQqQzv1HVUahuqHxupRcpkHbtl4gOD1aZ5JiPRRgivb7d
Rn3NHsO66Ku9GU8nilWiUiiAv1rVgzorqHea380QTMgd/yFzneas0OUN/m4jB7RZ+AFtq/gri2IR
5iYMwpQJ4mnGYIuMB2jyxNCnjJ9M7/boCZB2O96ksh5zUptEz6YrlUUH4Atp/dY/cZ1tJZVCBKhZ
x2Ay0Odb6Yd8YlX9rAsxp91gbz3PVG6JfDkfAIjus9NU4AzzW3qBnpnJsuVwa2EszE7HyP+Ykt3R
PNLxc+FkZTni9/gKese9RyYtZKlqN+KP69GitwCtENjs8j/YzHIkKgSWlvP0Wg4LqOqxq0Z/CqIm
reeSYYDfZMKxM9BZcf6AL+ueQwu4pDeoSfFPQpDKUC1rwIszbEdiYhMcpD3CqerlI1dqQeEhzPDY
ZLVGbxcGRdUMqdJUTZsqokUrK8gr3Uubkg2YI+jr9STLKC8x7tqFhfvRM3I66GPFPoIlJqkqKMUd
UvJqTHHHIEM/aqqiHU8RcVDC2QTco9lQVPGibYbJJBbSr3+TSPxGwe9m34BRec4tgrR3WfxMlx2T
HsWYVnHdigR2Kift0E+2rMJ32JdabSZQ/na2Q1NwPUo1RE8L3ekYix634C+6kD8147KTMCCLgsxK
7v26HQtWBg5HUoWuJSP3Hd1fmuV8caamej35MzQogZYuz1hLEqLe8EVqDMY1WLDDkpiZDrBNbLih
ciymZX7fSfU3Xha/NNMmdGUlpRKqBI4ti2VDac1hSqLVZ/QglEwVL2rjS6+E6Yvn4s6c0Zr0f40f
D67byh8dPMGTTrs677cjsEdrs9znmuN3IKvpSMzcELQVTG9Sv5bagZ8+IqcXhTbg6eRGuKMuVYw3
maPSaev578EsWm7Srh6u1p6Y5r2Jzq2PEJOYP2ykILa8eLKND4Y+B+AgQF1ajwssRLERGlPOjazY
hyAaFvOy2qoRdlGHQhQYRotplUQZ8wOB6j2LlaANi02Mnll/mJY6GkniclfujlxJK9MHP81u3k9k
9IxQvjBcVpO1VgGLVV60pc+/iup7QIV2uF+4LBjk0PBtuk7q+B/7jwAygveW5lwD8SIvYU5fenRf
enH/GbMvN7G/FmHLp9kndW05VwTuW6CmOTF7PkZgBCTnsVEGoyDms+n2c1iyg7QeuAPfqALu+dQ/
hkJ2HF3z7/VJjocyCts/VGWTMpmeTNHY0BBWWmaBne8GEdIlsAh4CcOGbz54BgoZ5d9gBdtDQ/Cy
f8k+csBV0uR3qsayUE9Nmezlhf06QX7Ka72nWMH+s5R4Mv2HkbbqzNpMCDlxhvUB2JNG1rMdHiHu
L6O1tlGZ56vrPZOAXrkr13DqI0JH4ALBxxxxNp35d9rtRRKs3hIOwmFE2r5gyLqUMQlGBT5TTgH/
PvAkkVN2fhuX65MWckxSlWDvBkzfv2p+DWJwLXfyKZ8i1Rvtcnjn3Hhqv27SnqS4ZPDT4SQsHlTs
JQXJNuTiIl8TeXw+OeITQek1cwKxARzSAtQhgByeBF4sUTuxSQSrms2LPXRJeqdJpnpSvTyM+1CP
Sy5h5dM1J1nZkbnMtdHti57vg2xi+Vz2HtIPoUkJO3G5PSYm1YlLxSQJqseq4Cfjw0Y67b87WURd
h6ax3fPtWbh41iKilXUSUDguxQvVU9AcDgHOXKF4GA8wzFTf3XdRvq5VQiuZ3YQm+/L6YDENN6Tg
AKady+NZqXvYAXCWPqyoCm/ZKl1DZN/p9DC+kYOROPo1YL2k0SuzpttZ7Q5pS9OmnVqyxAK3bF5/
AS7GdMJRl8WvlHqx8d4N0HDCmpYqPTmCv9rLua9YhS8pOLVuGDG0THJDy6ymKh010GIyl42cB6km
+Fnb/FA0ahoTYMQ1hfukehTlGAfutGh6QyAKW1S/yG6ev7rQES5HQh4TFfknMvz9VLb7SijCyXAs
3e7Zmens3MHft3R2+tmtxu4+AwmfwuYehAhGEMbFIlFGGR49apEfxC5uQ6xA8foXBcFYbYE7KmtI
Ej6zpqYu3pQYi/1NDaX4Qw84HEscpMJkgNZbG55H2p7qHriduneVo5QdvKEBPMIEb4kVfLL1+1eR
4+gHtqoNZVShehowzdqsNsrHBIuzBJdZ/b5fYTHqXJDfEGaa1fRot1fqxRn7AG86GyWeVB8IfV+0
9vfNE2kAH41t3EFzDFI1cna6BpWfxfSKiAj8GAaAXOHjRB2xNvHMCr9ywzmTsG7ULSfndTi4p29p
ExFWRegOgfqicP4fqzdPNkFWnxanC/Vkvti1g5UDbigL9/Z5QAL+BPF5yv56ne6tRK7OT4J2iUgg
XdyjMa8yVuTgfEhI5Pq84PtUBj7F2U37s4TR8Tha5cq7HCC9uReHZzZxjv334EonfdOj4w2vpU+c
rNbFHudpot++mk0FnNqFpyb0bLajLURjs3+fZS7Cyvg7yvgqepYOCGLuY7dTKRqdgquCZPEDm2pa
eRW2+Kqax/yLYSKNrHC2WCg0e1x7BiCNZ5fa9v+dTmU61EVRjkhH0qlW065xiEZO+pJshtB3mAtW
0lvAZSeLwinFUjQyLZ+s9we/3/KniwFBgPUzBDgGvwg97ZVN8VnMl9GFz617tyDfYy961I2ezzVe
Wj/dvpXgZynN4JHlvUF2meEP7BxLqyJnmg1KRo+SQS7/tgae1vQUq+jHbbD6ZDIUNm53vQv3LjaP
lwvdoHQc2eNMWrN9VBrsp6p38gUzkb3SlzkfaKbL6V8iTMCHaAX2Pb/elNXEoA51X8r3dJike7MF
mpw2n5fI3CnP9VlK3Qzw+FyA/jSW9azL6oRBtc8fxRuowzQKwXlxRTTsSnHmXPdYHs5kjCrUurVE
XMAyEHi7kg7UnwMN/S2yBlYBLpqq18lRnMOrYSDatqvSNqcKq6qcusE/xlZKHxLThsAeYdG6Ilga
fnGQSn9lIF5Wwy/7OIylcn7BJKYM68FQcBMHEc0ZGx/pJqBZhioQ6trSp1zs7EUcKWDtxcqW0Gwk
rK6uVzRAi73/eIZPP4sGtDDbATwnBZjCb9VaTry1gYisni6LALSRf817pYstF9zL2yN3ZYfWry5G
jd1Dk9NkWs0S4Vl0JaouQ/ls/uktaDSVG9C4nRl0i9Z1QZ9gOLfispdvCsGINaZIl4wFtMddfwuT
6PK2TbgedqDHRP+7E6Hkkjy40BDevranET5lrhNdJ/FMuh7X4oSn+cP+qEqF4ktPx/eN9hJUlHDO
y2e0j75UKkH6NNq1MnlNi0wdQCvdae1yHp2RxMQ8vQAioMd02MGr9krqEoDnxbr59Gs/GvD8fqY2
VrbGuRsdBv5kUoI7V3szsxJoYdsjLgmvScmnMCzGUs/hQLDWLu+3kzFEkBsLhaminMBrB2Q8e3NI
4pHSzDB5DvHm5uWRv8BqqpqsHlTkDTU0Sa0+mbL4/eP2YZ4F00kUF2SpgdSYQdgf81idB6DBfEs2
74uzvnZ5oJfwK6wBDWxR7KW1HxS/ySoavirtj0y3OlSt/T5bgzkKis7H0xyiLtqMLPvQ1cZM3Rdo
STRnj8T6jgAGmsJ1Tr4R+BectP3/sdRlKGv3q8j/1ycrZWNJlV7I3/5kM0cFXfKzSPdf8yWtCR4P
jtToEslE8ZDQtGHyo6o7H+4+zkPklnN2Bmi7A6N92rxeGKJP2yh9KhlKj/LLehEJ2OBBKKhnd4k6
EWEMEFtMphlKSj6tH06zBaEK1LRyHlPz51fIBYn9s+kzIxsaRAIbj9AliUROHz8hWgc5BT0qDc4V
lnrQ0iF3HY8ARJosaU2BEYls1OP2r7fXJeP1iWsY5yLuTIJN+lGrBE4N+GwajKV1s0fh8ZEND6CE
vs67rRCLMvBZorDO6qkMljkTdadYWzj+EBqEVmcxVJXPAxQzbh4tdW1kxg3DqnVwkv619pbwrfdV
WER8NDY4sFPSEl2Vh+LjVXmnvvU3SAFPJCszMxLXstu+Dz2bW1yssyIJEWSK5U5igv8LyDqG+yJ4
Iqe+4pHfqooiArbn47ZK6RWGXsFV3siphca3HnQ5AckUQnFjuIJPyh/7qDUiaJAbIE1w71AnEKHn
UTj2CMVFuLqKohlte6v+u1fDqs4TIij1M2GehZtlhCAqDE4UZiAkMW5KY9UB/fLRvV+njuCBHZKr
/lE5tviXNwBzNYspuytjWCFZMvVS1BZlJ+yjQnQ8vOQhnhg/mwwZfsfWDoW7R6769Cli8ykQE/6r
pBhZ5PgvrU4Dy8hm4ze6/BIz89EOV5xejtZjtB7yNdZOWHa11ZocxMFBfJEniIn/HywR5LBP16VL
KmnzT8KL59qb9UyBA8M2UaOGWsjWH0DohFUbQn+hZx1+zasTQDQE3J9ya0MvDNT1ZozFo3BeSXxR
gG/u1/5XrcIyTL0JoTh/Ic9d3DFBBpn6ENPeLedrmSJFE2yMy0/Of7XwRHkzFLmjO7LT7w3EKq46
UAQdxYzNVmllTErKLReFEpXHflW1OVvBdZj4ut3rdFNXkd/PnkpsuVuzGMPEVxW48HIssiFJfZX0
F9qmntIktvKrTXmjxPw5lSm42O145CBpljhAVWbJd7QI7sklr2vFGrpbE3SQ9bi8r2VfRY5Blmcz
I5SPDLX7rYN6QKnuOCW29jgWkRLLJ87MnvSh5tBDVr3BReM6oXq8aCA6Kzw3v9ECf0dtTE8IVvPq
zyEga6XRrZkeAQAQs4LSCgSaaNw61vpEr8OFehVEtCeE22cXk7kpVv8U5HT+qPbWs50FeDjKQqzs
wqrZpj0iaaZJutxqcxJL8LpWpGj3BJRBGFx2CvoLgsj5ry0ef0Yib6dwxazxzH5GdPPa7mugJtYD
ouUHQIjFPLpPS3yKQBKVJsncuUs6xV3p3CJyTH6LjFUw80K7kFvJRYxhnZhaX5aiKMqaTQonvz5l
1A7AgWZGW0+BP3lV+f3YSz9iiM/PtO3fwl0uIiiDm58pTFhRuMvBJoXUsOdoDtHLncNAFWXc3oUh
kbB62eQ044INihYm/ljhaGMfKbvuMzH3PhUwf8ycjaGjzTQ2Ex9cyRChzN9rTKquEWiBPltcYEYA
++ZXkrsVpOBXDUwmiylzzeV6qeRNcxEOLzioIyeIk7CxubiH0eGYMbb/eEY9HeBL6Z0UPgG70NQx
bXxzXLHHJzfPC53l0vWxuBh5S3UNzbFcQBTOLbeuddYnPHrl6kWDnlwTUIHoXPf1xPe2y5b4DYMC
l0P7qa/O2PupcMK8u+K6S2FCVd2/C0CkXS4SC9l7lp3gokuoGJVrStejBPcbM+Dx/z7JL5wo5BJy
gDf9FIZbcCl8ElT/zoDrCCVnx9a7GKW5u1eILy2pEFm+Qn2dCr0UqPquld/K1njEu+5NyDBRUVHQ
UlzEGgnZND0qYQbDOviWpCWnD/AKWCNqmaV7ocfA6P1Dwb05/0/muMJyroLB/Tz8RzVMnNJf0lOS
dqiXwhSkpcUcard2XHgJsQ0iHYfurfItTj9WrQ6k/5Inzd8rZ9+WNHhUvzIGdxahbLpVrT2YFXW2
29xnONqVZIQtqQYOkil9Fr71tyPEd48oz3oBsXjatfKcnF/KKQHi1o9owqCT/gZrDKeTFrnwHK6U
zUEYldBl5OSHJTrlJJF3Q8dS3Inkx3FowBQo7YsagKADeJr7NOEvaWyrdB0sUDaONzYeyFOV60re
ai8e5kmjoAY4uqZFUNcAOMUdW3wr/gB6qvkTihfxeXvrTTD83GyhlqvvtyOwiAQDyygszuyDauhj
lYzwIPvfVG2Cub3YJdxK+IiDvAOcHVVnPSqHYoFEUwVxdQT7LTO1TWYT2aUndjNSIlT35IcYmfnU
S1Gqkr8HtLcfx8asaJ+chfwGr11/jxqioI5aCkrNtreHWSCfS6nC7BnZ6gsLR+aYvbGqGa+Rmbny
udNJeJG/tUujIHLHxsxrmSeze8AaGQKL0qWPY2y9IzP+vJ3Hb4bppapGSgphca1MU+xMaUR6CGzp
ozo/rhqc5bZDae6Rf84W0kKQNEy92Nmn1BUkmepBUpUvzCsv+ddOnPwnNKVREWoy8DQqoHCg3IoJ
paXLhPOx5v18eS2snk37Q06hlxMw7Mr3X3OnHbl+ZCBTcKXscvFB/21G/fr6rXXKqUE+nrnVg/fj
tIlENQbYb9IKnFBT8gCjwhsP71q5QkuCXio1zw+djwvSQcnVkNa+74iAyxX/GqvlMTEm2C5Cu8Eo
RqVtCrISM4uEGfvuYReMGqDtgX4PjU3YpTxLccrKh09KzJmBzc8mxu+Lc3E60olwBtdxVft1mnnZ
ZNCDcRcVcwuVKHSb3QvU3cn/2kGc8fK3BkIRbdEX9tHM3vOkLAooE4E0K5hgeG+FLbMXpVUQZDgb
9OEBEFpJn+duIfsRiKRyjf4zV6XYpijO0orhiN7J8xRKiZeeq6w5ip05g2YBDndMsCvOdCAEUrn5
sE7fx8/5t/dF0eNWVHezkXjqL61HmqCnnys07TbHYnwBq2Krz1YJJGBnJdsXrscOVl7CcoBme20X
UBDbQ0HuOEX9o5aLNXnvXyxKu6+4oToe1iKFJhgshr95xZvZpRa8GR5DzaP046V+cNZ7JL1oD6OP
od+kJXAqpWymGMUZAC41OEhM1WygPYVh0NBX8/b36HFVNVuMKYDXpOsZIsz49226FIgBSrTLwWQU
Qce9hYaH1fZaGXVNYAaf1GCbABwHzn/I1yv7569nWHxsxK3Co0B7gccHks3C+8/cxndxR7a2x0ot
8ga4Q8bzz2uUWi/1y6e9uP3+g1BPF84Gv1CUwkA8lfuvIidLw7CmNMjfTgTsJVj41gg+V+YJ93ZE
twD3SexoWgo/+wx+6JBPrqiDTzpc0weGb2Hprv3NHlMJQ6PNBr1HMb03TQvAXILoWePdH4r/ftPk
8Cgydgxz6CBJkwtwt0qOIk1H1uq0FNhpbjjpuQ5H5T/rRGJRVM9nZ3r5NPFCDwc4CAk64PRSMPPX
KgiBgBQxV5LLHrndjmrwUUstBFIHgYKM1KW4+QBTOEkBNz3TKxwPqzJNobYR73FHdM1ptgWgpawT
IFy/pO7oSiEOJTp4WlRc0qq+Yx7xr8AsxnOlO2wC+LSRaafFwc/GJxzf62fVZ3h6UrDANh0iQPhz
VEUuvT3eC9qfrCjvf7pRyDNfmvY4ISLf8wR7mpvK87YZWb7WdcmL6W28tmz5NS0zuT4s8/YMck+L
AbLHpIh3yohTD8hsatBrLq66vD8g0ow7lI4OX7SE/gS/739ATx34y7WtvUBGQQj1xRp6vNESdOI2
GzUWuqTj2ruHdullFSXBTySlTKp+KQHqylTuu1YTCI6+sXa2FndKsu5fyU/e+m1pJKkXUxWuEnG2
hSIrxCIxhjeOyxCnjIWK7vtyN9LwCoSTr6DI7OA2Fgqgy2jdxrSqcAxa5nkV3ARMgIMS5Kkrteep
99wClk4UrqstBn8t07hsCdfdxZYiSmrb+LcKAuB1om1tMXzd+3YCIRPrEV3BYL9SenVzvXe8DatY
/XuF/lDLTa3zl8Cacyumjloeye7//sLmdcTq7akim1XI4FwQ1hT9X+LOA0UtmlCI0JsQmkos2j9Z
alOl7W3gj6Z+OnQzmQ76yzLapSzO/VoyVgkcP93MPhocCN4Qej+82Kf1Qz6yh5JwxK44jIgu0R2C
rEyPPGX6zKEQxa7O9yRQsZa2cx/0nw1Z5/VErenomyVH12eq2clB2079sasBd2XvNv/MXxPMaZI2
N1HWgzpkrWwy34Wia5RBW9ftNthW3CWgDoG6iHaWN5ZQ0dGBZAx7pk1WN9VvhaZ2uUtfnuTH8n/m
/518nZd1AodSFKpEy1/wCcweVNaTtt5nNbS4WTDag6prRGFWkxNf5H3g1oY7UnlHNwQWi8a8Pkrx
9hVyxZjKX2wUXaGPr+aBAg9ybEqG3+jKu+ygamVfJZfMaItPeVmCEpIEbLu8aqLGPt/PdJiTGwCX
MvnPiFtK4iSjWMjmcYjToGblOmbf2WsXqEwrJYX3CbJSU2kEkEChRmQ5LCetxlegfBOlSZQZBtLX
wFmJ1/xUlqZr9sO/7NEh290FZrxpfOF63UgeK7LE7BmL43/+cporQusrYf/736jqa/Qa6t8d21g1
kZj+YPC0L921angecxTGGlZ0F5C87Co0vvntuWTEUb2gC00JNnKaYTqBU+NJt8gTm7XFFME+lgwe
4d8LNavw5gJs630/fbfg0hXx+Uz3RaezYbEqAzfD5S+i2eST04JN+EpMakCGaKok8B0TLT1iSaPT
sVsxgTr/XGBc5IP0LmGxA1o7MHpnbAtO0SrWOrsRtEkW4OxNeF9Zg0I2Is5yXEd8f9Vkgjpc22jj
qnPyGqidmnZJU+9iHjPyh4iECro07tRBIPXtjfhGEraeWNoiFflTvmD+ZCeXSoC/RO4O8pkZaWcQ
lGp8q6ZvGrEsgqKYN4mVW8nZOoiTlZ5yvCdOYQCRWuOC0TiQufRAA+QUSyx7Dh/yPU/e0XdA7krx
9sqe6LX78iWoT0gN9i6PgXFwNLmhaROuUwraqR4yFr1ShFLvUPsxOGr+0bz59KbEGtuVQNA3XNzG
MGIlHHK2pnPe1xmFvPs7SPdU9wjClAnHtgPgB1shB+EjCjDvt+03YWvXc/lLcxE/LUMZjMOxUk9k
88lsXxccatKH/DZNvvxDYwxSyV0ymKh0+kd29ZH147Vf6yV62IE2OmYIasncsdhz3Bfb84xmwPLV
JOYCvPXntcqV7a+VbdZVumnIqtzdNZY/bnAdxUIoEq/4WtDXrOTdESX5i9JoOKYVmheIyH5h/uTu
V/lPc6ZpDrJmWy0r4Put6DFnEkud/j5mN6AqgWWDYQDFS0PPXCXWjXyGeJ2q7RTrGqLtRTj7ypcM
+XvzDpeKR6GwbgfiymazJDJ/AmIF5kbLSaC/EyXDnCMKgq1VpwkCymfbInxwtfaRATzsnDn5VZZn
7tV6lVem78jYSb8Tu7MvFmTu8JNtR0XE/8XPw2wk+YqmumvxQ1rGG6z3xjpuuhjBS/ZzsC62RW0g
jWMlpwxlvIafRq/ApF+Pxc7JXEZ7ZxvaiJnzoXzqSu+zET2u5knrMUgMV06SO4bSWPrysOxdCQOQ
4cYUlgGai2L0Ou1VYYxL4rNwmkIK2+T4tvZ0CrnPVc4y9Ol4UMvd61dcwit10P4P7gb9KxMoFFB6
14G4aeNVLhFZakGLs325agKPXn+foiFqZhxdwouImtCnkXbTlnZ/69Z9cHPBjeDja3uYMS8Rc0Xt
S9i+Auj53uCD39p0aQWGoKNlQ9zbojpmzziDv/h3BQ8E45zoZOGZcpw2g7BrCWawkzH31YbMUFO4
ENKNGDeut/2vPuIY3fU06fMiE4yNDdulxAS7YBGSnfjbgFv23XOglx+Gg+TMqQELKNqQKSi//xtm
so+aWi5z38AalybfuZH+1YtGc+rurV39x+eSn5GERovu6zGYOFI5281G4VQuoOIgYIQ1FAo+dyFz
TXeCNgTZmA7kCDMoTJvtvJaErAAVL6qJPDgHORPxMcr0g4f0vfCamC9mIbKc41Rm8e2Z6KyCsPK8
MWqomVg0zkjB2gA5ZgXvYA1aLNAFmnoJVkqAJuryQcKBpJG9RYXCGj+gGvdkLEdxn4gJ79IxSjhY
3KGdoBhhRpOD1PMi+ezbQiUadQkpJUFlyONbqBZ/eH+N86V0GF4ToLSWG36zU27XzJC1X4ujvng6
mTXt2ow+vPnfEZNqMIbRnHW6Ko+ppM0JJIbiwU7cc4Wi14UQrc+FRj1sklVb3sG0PViIbmwXFkXR
CaPq+BlfwGlMeRMh4rH31oF4KB1aMnUyAlLIAmp3AYWuCYcgP0Faie98k024p1ESreLrwyDluGrh
8erwoO7Pv/ntDYPqCM2gIvtTv1OdIenLuA0thB7V+64vGw9UPbTKVVp6cGFtAb+4GX7z5E22kWa5
jSHan2lFa1tAj1n/WJqlWP5vrVZie3benuWNPdJSB+hKDSxWi/VcuohWxeiLRrVYsYWiNl+w7iFY
sZFFlE5l3sFKF/mwT33/+ZkcbzpIvk0w2xw+cZx6l6hxsbGTL+HSIB3IfN9kEeSwGjD5yNCxo2Le
ZzCHsNkKv7rqsIxjSMDD6wSyYWqbaLsUM63cyWVNVIhyQRLHv+bC6xe4GBhINn18K5G4DtWQHuM+
ikS4gPCRCg+kt3HAsrKHCFt12ziutxncsVcW9oEpEgtfjAhPn9y0pqCUaAZngEi3DKQw7jMHzNtn
Le37IzLhjqWF5yc1sLmcMaiht4Rd3CQXQuzi3+PcaLgtbSmXj++ukfJvMKHbW5scfDYVFOqlsf4K
T39D6fNHuDsFENjVM53EgjJlXGkd4laBFNj08KtsAtyg5P+GOAyYrQX9O75HATe8bcQ5z0eMkc4o
IAji78o3geLUfwL40RL5gmonB9iLZzXNpIE50bWc3PYWvPqzGBpMuTjsE/lADUPIW3F2bBHvG4D+
9SzzoKEdBoCtA9E5FXfojLA0ZUqSk2rnumaNNae+b3Y2MTrquC2oMpvSPIMXX7F0oY2r5D3xmUaq
lJuXMgWOk1hcwF5Fv7G+oMVr8hiTU/DjHidMFOL1RWHs9cZhhchT9WzQtuHtDxmlEg9Qbx5797NB
GnyMU8XrTCHZH/bfidBc6Pc6amFsh+bsnTuof27QaExC4Af9TjimD1SvYZTjLRm65Z4/3Ty7zhWU
q4tYreb3RSxVaMFhZ8Mg7J8LhXUl4WAXpqi67mYENPTn2FMFr4kbpsqc3EU88QEarktQygpNkBxZ
orsouixivHnEEZKO5Y/RE+cDufqsC4Unwhmtv7+Vt+QDWdcHT/2vPNfKpzBU8jtbBntcc1XJvQRC
9DEOwfRYmD7aDi544SuOKP7CTMwGXCSN764Lu6vJfTmaNa5bNTxLENA/sS1UgmcZp2cYxq/R1a9B
4oi/IOmuwHiTj7RrJKuC0zvyLaathj+MaRq8z1L3bJwpADNm5Vw/CMdIJ4PuhXrl07liUSDEayWu
dpkA4wXEuue1+bm0fBQIgMRrDOuQz1+D5/SbQNPUFslpPBCqSjkQjbI2W/FDG9xo3Saij6p82hub
fBA7KDtOARYo7eHuX1Zx88S+4N34Od8RztbxdGIEPmJKBoMTeiPWUz7BZ9pstDs+FTPMrz9WwXpk
diOZELKyUSQJxS9EltMU77P1bMl7ilLeSayyylR620BORZLInoy19ZfttU7Q/e+YoA0S6S1CX96A
7PkuHz+/Wi7sQCpBCq17Dl5daFDwpeetKvtiIqrhBz/pbVxKYmKMxWjlFbqmTxrXYjuIQchCKBwv
pdcLETTbS08gxHRLwW08w5tuZP9+OUT0gERJI/cvecRg3QguNVkx3tVzBr1m3oNkGqAcwHQZgufZ
3cG6KHHbrydVyUO3+Jcl02ubgsSROBVI1LJzLxpw2UVn2PoUJXrO05s+ldtDqfERWmDMzv1+UkM0
CFlJHERI+sB8vX04DSmjKL+lVLV4FBqxKPwGXJ4+2ri0nnhEHHBYt0k582KmlotQtRP7qkvsLE2d
WQGLuwJqLXw5uzR/dxgBghX4BPSr0fQr0oK48zO3r9+Lyy93aljlrUOPC94htEGe5H9+gA23VPSx
G+QKgLjKE7Xa1EP46oLkNjRXSdrH/hUupCjKsBJs9+ziKMQwVETZicNLVyYVp0tH4Nc1rcWYHis2
p9Po0yEltooFPG73KJBHin2E5iuYM/gyIV1PiEvCwhQZGWhMog7/xyp8QYLXiHbTfRH7N9v/L05s
7BaN60JqlLhsuopJkX+V5EANqBIJP3J8FllFmS+FxUIUezb6x8Bzo4Je20sBlhNXnMeDKLohunsK
bfwCjj75wyRHSubCKx2P1lmL9pc7EDKM7cH2WZM2tzKrtu8qYudJxPxnBHDvvZb7ny9TS9tk2xGy
LO7gF7wwvSrw6PFZmahDX+2l6T1h2dS8cRed7fa6+c2LVThlAQWSIHXPfGCc47OIaXhyFSV9ZIxH
o/AzgL13XP0C1jCQhENDXSXv9cwr14n/Goje7Tik6WcQ3Z2Xj8uKdweOWj6tZn9j2oMWBm+ctHtP
e605RXrwmzdKcg58H9+SMZUuAwBJwcvKI/WlSBc8kR4NiV3u7rKjiXXiyFPFSIAVMW3ey/5+LPT8
/xH+FOfZxqor2i4kR33KQOessDifvJh1sQFxLR7nHGGAYNkHIf+uD3XF27yqchCgMuRrXAbcJ2IG
GDM0J5Fpt/IN0DnhYV/Wa2bu4KEfWYNKg9c6cvJvJhqgKUT2WCKfdyFgM7UIMZ1fxOai7Gfy/WlF
OIgtphYReEi1+WhODjcDkH27anmYuSpByrLb9rjtC6DC4vIwnj3oCLfFXuD4mcBDacNtH+79sR5y
CMQoqfa1a8Gy+miuxK5RtMHi+El/SRzXLf3/JhY26SeltE8QlLezcOLdaNCclI6nltB67FZc1GVU
9lAWgkeKROoKh90wWYEcCh1myM8i5S5uez1ygBtYHZAyGqmDqHASLPooAejnO/4+p2Mc4VI9psmU
zD3Mye6jdp5IV6MQhfthL5BBM8IqJBbvxzUBPfxwUqhEAeJsVIIS1XIIjzFVK67ig2EE/webJbet
xCigqAfCLNId2NvU76j1ivxkMdZixNcUtF39a9MQg2+1OCkMF6+gNkuvbpW7Dx4bNQIavh8jwIS+
zexbj3VvSa519KkYnEKLYpDRjryQdnDl5yHcQHWjyRCv6NFVlFccdL59wVqj09G1uHqVjoMLzWa+
FcLZ1XpC+ZfODEFT2ch6FkbCEdjEHhfrtfhH5Wb6cHRu//X7HPVFc59XGEvZMIpZAp6V8f9Y6z7I
M8bBbA4VQK7h9LlXpCFTKSalLCISIkfWsjwXzCCBFZTRoJgL/6lwj8HIzmGDbaMJSiTniBx0pmjk
AbQR7FSzAHOut1DweRUCuml0Aumuf6LKCyFbw0/xlm5tuUhDrsC6+/BVuKnmE3W07d3aQRX2l7Ru
MjzjxZOSGCnqHydZZljFdW17DN1h0s2ofgj3rvsjtCHdKLrdOCfL8bmLtuSTbnCxVylOXLjITSxr
KrUiKu+36gfeZ4BBe9VWeZWwVaXAigsSHAc6nKqhbLW2SlPVhKvk+logjECZklTfk+++M95iDlNE
CVxcrdjaXxPSRG1oObsYMjZqGl65AXr8n1GDOf7D6dPZrhHiV9QZksDz6mk42bcYdLq5fLlrufhs
+Q6m8xu4TdYBb/IDk8R9Bkwy1i7POY3BESNlGOmc+rBGj7y7f6+F7gqDqWC9M24qOYn62QcY5jpL
xB+Y/oJgURWuCxREVUHmaVi7pqrCQdPR3jz46h2Ujipuy82VFqDbUMc9l0ypzO0AXQMVKatF55k1
+hWZ2ayF/fj9JVgbYo/414RuUVDdFHtqZkHSGXkkd2B7ttJmsPCS+tlV3M+By++Ztx+hn9tcTgiR
qTbwqnC5PLGdKqoz5wbctdbrfI2ob/fNRGJXcgp8mS3qdEurgyl38JYQsKMuh/nb8jBZWBwpwRRH
pO4JdTI4OL1D6rBg84Gxncg+PwvrHBY9BHswwjduuAAB8PLs8Dk3C9Zf6wA+GvXK8g97iD5Zf4X1
iTKJyhyU0UuaMCfSum1japcTt3ycyp/EvhXCSfRd5zIwcph6qZpbQch8+ah4bE3aosS8/bf0QYtw
b1AN9E2mvq00AqkFj2rI8Jj60ln/NZtw896EjPWPcYLWDTOYRbuDfhT2i8tvYQ/6ssrpws9Uzf+Y
GHaVM/9zccA2ugw5KCeryTjfnuN0SBhi9sxYPYerB5RHjLd9X/FOFc652dbL9mooHwEsAOumbV3Q
SKhFEhGjaMp3dx3jqFlLL9PPxJ5m9zJdficVx3qDj1DqzEsKM85PD5ORpWKn2xoqJD9bqys0AKiI
Hu8gr34iOFq2foKZIm0yfQ4EA2K8NNKphEoibvaMtCN7UHMNLPmTidx1MPCMyT6sSN5QK5DCDfaz
HKMTVRclpf1j8vc+DDLI7NXc6lB1OdEs6t5hvd5TXqDq8zRipzObsbkTOFOKeqMQ18QnpHnPWbKT
bB+1jK44UxQlkiV2bKw/e44rYSxTI6HwJlJsTcKKkCqXJxdQ/yf+lN0m336URo+qsxk+7v//kkOs
+QAPNtyHzFObDl/KO2bnH7zbB+kVVbsm2ewTJ5hkyLMr5A2YqNELCNGsliXB9T64FysyvBledvTD
hJUgaN5n2Yj3ROok5NT/7pgyySbb5kmvYf5fAnRgwiFguK7+rlxZd9zfwLVIxWwd8OINlT3xWklp
r7/TZBoa7XI5gP1vjBOrhIOh76VrQec9AVFfB7+8t0HWafu3HydiCtEenm3cpaeqke0RrWt3q60d
CaqpvzljRkZg99+bvdmyWlQ27vGi99Pq5FNpK1nvBm09np1Tq649fNax5Ldiqnx2DkZDW7jhtwhP
Wp3bO7dnhs83ItgwGenZnAgOG2jHWExFhsT4efQT1sQOq3lFWUZTQMENtnRp/1aWAXQ4uTh6kuvP
ghHRNTYsslFnnzhX4bZ/ovF8c6mnDoBTb2uxOAJAnTggvQKMkHvLX9WYZ065aNgwxK5UC3lwGPqg
85LlWk56+tVe7WETbV3lnX15dY+xsKgbYQxLJ6s2CNQZV4TG4U+BMOQ3OubLIowTz8kLzCo5ku+M
Wg+WUp9xjRXwQzrgcIMm1QcaMCeHbFN2mbwTpNKwIEk1qhGK6RgITZKCC2axoVfK0jPEA6pqiZ+4
i5Jzq/71XSeyFUVT+8xANhXl+vwUHoJPXzrG4Wctidx0geuoaLMuQG95i8RQlBWC7k9+p5HVTDx5
a8mJXM6/ly/IgNBKyP1Lsha98+zap8xneuF1nBjKpKjxmUJueVQJRFqoG1avnHhqmNcuT92klSm0
1cuSkJI9rCd5ei9wswLmy8hXyFhHDnGJvWzKng6uwMahxkQe/q9bqPAt5nhia7rQ7uoLdNgZYZox
+y6kPu7uhHyphe90DiJgB+gTFrop91DtdDqwEYHQmwaWRXiGBhfCtjUq4ABZPCm3WDzBC4iG6ear
UURstOIu2qgSj4RiDTnwChXCqPTXu2LVM8Q2BDrNe3VOn/3bjq1E2cI8/2MP+rDSt7BK5U60jPoO
D1ZQtFCSVzfnCgNmQ45Srkbg5Nd/RtAjKmNVTHflMJfg9EN/PjB9ID1jgZEWAoiL4P+7dW/8PMGH
GlNLZ+/r9ZWrL77tUSsReAxxR7eKobr6l881gsJsYWjL1HdfaOIC0pc6uJ/Y4ds3CDpbvNu0dW0V
FN6EWoYEkDoaK3IcUv4ExKhUG3S9WJKn+0AsO0o8lI0eI2S/ydB516hX3ZmZdunoWnnBCoi+x1Jo
SPye2JCAXtUNE92QeR18023b6f0/hAcL0OM0dwB1UsfG+zADLns7OAL6WhMQ9AFOCpOwc63nReI9
D8JHYgif7bIOBFRqE/C9DIXbthUyiRqLtBO8d6+f+X2RV22KforcAy1HO+Sk3zRdhM7ztjPCHeay
CFTxjq4WSADh7YlLKgEtNd6GrJEdCXqeAbwSlkNb1SPM8tJImUElk48cKqpo2eP1TtoJfEJ6vZD1
F/co8LIpJM7Z4FFx/74l3LSWeKonmDBsaCrIcbERKF9xLZ16EJ7THXRWpFemDv6aLx5yiM/+Xi+J
FjnMDXTtWX52/Fa+xKgWYtaacMIHSQS4blaJfXxi1kVamtBuEjHFzmvP3+5C5jr74LyO5ohxSDEc
gTmoJbgk8ednprFcl1u45qKcQP0YGiicUYzZoXIpg3y4SaVeN+zgB4QH0NJgnzs7HUgHuLuh83fX
tJSIrYuB8iaEvHKoFtVhxH3dfxzVvyKPsDh16MOZgngvwZ8SIFqk/UhzMsa6xA/hDqjVxh/PXOZ/
QbrvHhsWpxrDftesE7x6dNN6u9NboWAd6x1E4Vaa2zAPtiZn72WARGbOqARBa7vrZpeLPmMz81sY
sDfsb5jZdVqet6MtRo1iV31TkTUk//rCCrPkjcwK9k/qKkd2fcoeIoH82b4RLuyNuJ3JoB+nDMi0
liDQzGLPE7p4TAz/F+1GlWNMeEp+18KcBOC1HPyujC7R0iZRDGuU/RBhBF6JDwslxohvToh6pXLQ
awJwNHmqRmi7avknfUjmKeOxyZfWk9XJhFyk9K/oXa+FIWrxLt7ofOBuA4h780b5g0qSJ+heceWO
KiAH/n/EAEJxXbpf2ZQZWEHHlQvj2plNw/ar8qYhYhmEE8deUmMxdLKr6DnLUYSL2wCjj3wTz7b6
OzZgw6jbtsn+O4UGjR8okhRle51ks1MGqMujrc8t3FlCTct5ulfoKeNWJQYRhToKDUDDp/ra4Ad3
Rx4InQvtH7/2pomiIC1GOlsEnW9BDcRORMr+y+lB7YYqnT96q8wXW2yDAko4qp+nTFL4+7oOpy0W
+iV6lU/6z/3KTvOeTFOaeOsT2eBLsR2+4xzXax/oUg3RoZjrck/R/HHH6p18lIXNoBjuo91sjWq8
k1B40yyr37cpS0KtB78jI0RkjajzauJX9Nffi5UQV99xd90RFepv/YAcBJJnzwYe4EgFAdfvG0/F
KhRwO8RnLxgbl2BRCd3NHi84spo14IvRFjxj3dM2yjps9frNCfN+F4bXnQWpX9Zx611LEIDtqJ4P
Bcaoq1E7yumKG/yFIN6/DDQ/P/6klVB3sT3GPJ+8uYuCF2tldfEQPS/ysgP7T1uBwgl1WqgRz9ao
+l+vzRj6P99CZfNxIDeYLZM2pvknox/2hQ69//9QqYLEvqvvaSJyO0Zw4feq0QMSjUmblekOWfWA
2u5TEPs5nriQ1qUVXVAVP0lrGJpJN3h3oZqJTPwV8D67RfEFk7G6+iCYLwYh/Msqq04iGfQN6JVm
5NwWe+ItZ99FVbPUwsbWT/kM9WzOU9fpxbxOmXV0r/yxhXrdK33JwB6Sms74Z57E/zXyr4YmCg9/
XQ80+M4OK2GMCST/vAl9fW5QAywPJGIrMU6tcdpO1WN1B2t/57jnaHdSnLuaVhLbK5mTl6Q6QIbV
vzEKH4OPmHSr8H0O1svLxhedJWpixX/eZ/oUenYP6zErKlVzoHz4TqGJr7Ki+7ksMiADqIQqLWPf
1oPdLTUd66NAizEACnd+MwT4OjMot6+57aqGJuioIWxTYck0zQZ4xJTXTCgl+MEWgFdmRL3OujzS
Mo+E3rvcU2RpcWoC+8wjeb0/XoHWyEe6hX9EuC3xCG8pk3HCrDMhT5JmkJh31kpblC5vHyJBGiWq
mURa1Pn64s8+4kFfZoc1XPS9tUp7hRPXoXvNZ2iitf96Y+XY7aYC5DlHwVeZgw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
