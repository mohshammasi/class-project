// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sat Nov 20 01:28:38 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ border_mem_sim_netlist.v
// Design      : border_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "border_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.5432 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "border_mem.mem" *) 
  (* C_INIT_FILE_NAME = "border_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18240)
`pragma protect data_block
qJQ4S8o+2FyxRjbCuJciCvB/uW3lhZMgIWhJdAkJd4mdK0apVK3nJET1cuQU9zR8Rc+6+PZBNLNA
vs9bAM9G1eOSRkyCU91z4e4UpqH/2rPA6qomGAfIFwb3viK+tnHshkvrgR/PaYUCjp1/HAoK+KJT
7lh6/nyIYy8vdXnBWnon7WmI0UQjR5VoNf5sdBoX85+Yfc66jTuW9zMGQvTC6RXYH1wFfuJ2/KNi
WV07DeghVzR05ZPn55WO+qpiX1DpRS7P464k32dNJcgsFg+8PpnF/hl/SzFqc2mKMrM8xa1N5uyT
v04DiPTdq3aTMKIU1WZU1Z7gudkPorK6adI3Q6l/LRQV/MtYLUC0j+Xwpwbfi/2wXIJSC+7Rmg+g
SmKDv9srKPBeMhDb7r9yR7feQsmw7ayJi3v1SdD6H56g48YV1bgCUSCmCTn+p0nU1RYPtW2asDBP
KGY+h7VFQ/iZfSxU5UdLXmUvxkflqisuRz/TfM0m4gmqdV8QbeoVAZLiZhj57OUa5vpHazyRHwTf
SMbI4HWctGEAvwoIa/0exyzsvMETfQDSAIFylO78wqJq2G5vBfHocCQEWuF/+eKh2isadUxOv5e9
A2XswfeC1tZyUT2HfiFiWuFg45qAhAj0en4funzvFe2Hjuvz3fg4EUotQnn3ggLplLVFGh49+H93
uUOeKoUTlRRKc6VC7pnjtIvFXDjYYEyycDlkiNi/Sz25Crmxmtazsc1kiRvi4ddqNL39SzqD2rDt
4KlAH2ZEKuOUkEMKufy/p9DNVYsnYg5TLzF1PKabYezSy0nvCYcQfSuy2u2F75mWVp+ISy9rwSrr
jnffejbh53FIaUneboEsWC8KhrN61e46WUNiInQn4bsGqBJsw/dy3y+sy7PnQuLfS+2H7H0tQ0Dn
Kge+zMfsRDv1ginyxb3hN5vFdDYqzAwmEHs4VCfZ70wAJ9EzAlvpnrrCNbxb7P9Blqul4IZSWZWY
Yi6oFG1CYx5RAFePQFZ6gu8InJA2dNk0j0QGHr8XITHiq7czdE7A8jZG9VMBvsdGgkIK/QY+9UfK
d3o08ap90JR4i8sEjmMHj7D3uZnVh0mSE92AdjyNh4HoyzhC2PlonorBIZvawu21c5flIJ/B76Mw
UWT00LJIbluFZTNlRzC2VIoSwLl7lilfitxtK/ajFBEBDVQkpy5FP1lI/hdLw7HiEyYCwmZcSfz0
Pe2WSLAs3PY7Z75DaIiUSKYTr2uhpwE0GQSgl5lbqFIx78XEfmi5JWPYgpR4mU7RQB6+99Fu9lgV
9MV4HiTjePUyYLPs2goVOPv9v/zZ/EPq0dFnp1HhoVKRo3LBGIjRCKkSxfcYYcIL2E5Qj5C0XNQx
5NPs6lZb8VUPLHEO/os6QvSeL+9LZ/BMmeUyVw8Sy1+0TP6Q+ndXMbbY7C1jwsuy/kL7jEVXi17X
zFtWgQhZ5/QjyLfBQ3i6zgYFG9sblm7rD0qacL5kis6gAwL0p2briRAHpdXaZMEikHmKt7M9CnSe
+JG04UkocNtozOmg/dYMtSsdtWVlJNF5hQFoISZ6kZB1ZUWap+7A1/nDafNxJ1pcnI88lAzX2cc5
LyUkqY9YP//YwNXgNchgsBRncJsSeHcJckKPuc/BXLAgvDRfXDby3HIeFKVDs5sFfsjx+nz6vXT/
XABQogks5cIiyISfWRqpfI9KO0pgnmtm2qOBNuIRih5+eiz6VUfRq9RE3su0mV2s8BtYH71pSC5T
kWaT6s3PH+gMlvDjmLxQ3m2qm9KoIS/CYp9AeMj9SU6zekbuJYi9Cwd9lcJlK2jzWm5mTUl+Gtyq
5flklMiKNFBr9Sxe/DpTjjLvLsxmrebbeyDyPNR3SikeKGv3C+mxZC4rtx44LS/+tX+1QS9Wzuti
woyvIr1dtZMk80nM9/4bbP2QieMmQ51RDdof0kjbU90c2ptrZ1dIM1sefnEDHy2yWOrYsjEhqnFF
XxGrrxxdsYTpVXHAyhyoB+yblFp9VKyItu9i4FmpIivibDnOT67bJr/Sqg2G+bQ5uZNZMpLr9N91
w64kJmjEQaHmbBomzJNyIdApLbfFyaZSIYL5f98FBumWpEJwWBqIY2e6ZhhOVLOoi7plg3wOWtCU
gxr3IlmBUKkCTc9mFkLB3ypoNvjI80mjHx+blkk1end/mCoU5zg++joZM3q6Yyqp+PE8Ulr+xvD8
XWsNH8fts8h5T/KRV3OvtgtHyNim+IBML0b7lsIO/yIkgeLVVI9UMRmKPwNd7dTcJAlHDmA5i3LM
y6bphgbvBzC92SaytBws+lkkC3bNcOpSpFRQt+I56mir0Fs0dJ16sn8M+pBBejceGxDGQfq9+wzn
RGwTgko48OY+isC/XYSukJdV/pGRABSvoE/RK34NHZcOCtNES7SlvxBquOl37T6Pz4KVq+HPdLo7
5GLjZtMsZolHxed1+IxOh1KtKuXqM1b1iBVBrGUqvWSoDG4neXFqHuDUOV9I/UoEz26T2Fjg/1Vm
AIdpH5PPorFipuUnTtXEwh4KhW1/vW1VAlJLh9aKCaoSxVGLefr5/qhZlEOnsSWXQqVZgcPkYKyk
UeMY/RdggEnjqcWIFBvP05ZSfduriK5cpiuEq2H9rUBltyRTxMGXfwxJ88H2H2j8ZetcIDrQlIN2
1bx3N2Sb0j5yHiYttzVk0OKHIvCbgqatyFqKLHgn7pydxH5MIWdYB7mJV6DW9Mq79rpM3KLeuUHC
7Cg4jSjcLRoGjB2ndvxZuARTC0PyWlkO3Q6wT9weBEF9RBW1PAIlErWH44Q/4GWtNAcD4rcOXEwL
iYMzlyMoCS9velX4T960NBONdXiKa5VDGq/JBsr7JU9fuVTG+Lb6D6nhIzTQEJiWBcccbC5V2NCb
aYg/rzf0EQ+7Mt88CalJW/r9NSsZ3QdkuSECvHbWMJ0SWQlzrxWpe6ESi0gfrQa+1Ou6p/1lNLYg
4d+NjicbLATK9s7Q4D2W1pu/HGt6Hsjkvdzcar/NdIU1sK7n7hSHaXPQrUkbuS7JwPuOMpzKM0lD
z9Yl2uzxEFVi9EGBtup/2Q6lfTAV7ZPabf6fV486mbO/tiw0eHI5iW6vhvF6LBZAGrab8tgImxKS
hPftxty6shPnyfJxemdYEDRaQAY5mOH1ueqREj5IQbY2CHa6NsMC7aDNHEA3jd6755F8GIWxsJps
KSodNS9XvViRdsyeVWlPpVfEAtZs88zqUcn2W0xOF8KEKDG/ttSZetY1gA0B7RwH6mvz3vogm9GH
+3/47C8Z9RtYkc9WgnrS7fr8/NAzfKq7Gl6apvKJGo+opgJW4iQCjCJmawwE897TwlFUnt+HdUd9
hHEmXicwK2h/48Gfl4HcQ2yZC8bv/Cc4LayuiYtp9pOQEI94MvoLdX0AD069XSYOPC9c4q+k9qJH
x+ACXDiGmiS42ObmvhA5M/ZF9swoO3KqYuuB489j6VbHDiOzawDjKCMSspcOuS5OUSfDJ4nYm7bo
Q5aB1EW/wQk33qmfuGsoQdexiAaKFc9xSKNNpVvMd5XTsEsnRaVoVQuqxdUGQ7PEDyFnW4U6/lUJ
ny71g5OCK3HFrjkFVS/mCkVsQumauCwBsnS4yKngWA342b+TOry0ZvjtJCB3KUMuT5M8lakfVmEr
k3lJKkOO9iU5joXbj5CCNjFBLYmKwhlM++UoTvzu+OpvoTvtinimpoB529WJuBBhVevekBY3Y2AD
FTjvCQOnytXfdtGp7aIsl01k4aP7IK+S45gxeTcWivUG+GL4OxHwhZpW00Fzx9q44RZYuPF45nCY
JvH3FMV33Xk91leBhfP38SbAaPM3Ld/1EIV3sFKgFdtC1dFAKgNXpm9DRaSHdDy7XohiwAcD78dy
wKb6YzKk6GNPRxEkIcIqX7evCR/JS3ehJ/OFBIFox63OsAggBjS2GbomxhJwrTPIraXT+3zjgw4I
tMlNPO56S870n8OCrTzsHoIyLUgpHJ1ydqKKOIU2E++eVknPG+FbdqrR2rhSUqRZaH1wO7zKr7k1
nu5/C3rfmE4nxbAnVV/f7a7SCUOps3VWhFLZQT7Rg1tp7imbMpXSc2s0Ze/7Duw02gl3vDGHSoAk
2TGgbyuD+Iw8KK/x3riVYa8YgBc3dD2v0cFOqkwsUnVKlqdzav0FIb82yn2VJz4jB/9dm5GYgmc7
qWHHdTHgkD5wmb4tBy36q8kRXXllCvlTcXWDM+jKS3PFhko/5A+ZnyQBpmqCf3Y2ICeK/aFvQ7gX
y6w4OJjTsL5zL8TFzWEehFDonwX7exg5BFH3Yedm9YNmXQnGG6CDMmfRPGT4cweqrKAY1/heV+8a
1vcp2om5SdYWGAF+PU1ZI62s5NiQDskQg+umlWoaV7+IE2gSE54/MFN8eyqLU5in8TG4oA7tmHFv
AuqeN13tBuVmE1uKMk3/r0cAVDjh3x9EMtjImp95nxFKtaAhpCwhMzNUDUSCJ/O3AxkxpN4aPAiw
taKWQOUlTGmUutGghUlblZ+qwQmKI1DtYKBfQE3OahAE4OU4VGjPUi0e4POoFpGCe02DZ/qDlGbT
jPUumUNhGf1eFKXUBdQANxFa4QLcIgXJXrCHN74dB2KTjgrFd5ix8ftzCKj/G+Hj776cq+45kwob
rgfXygTa3VhKPpQtXGkZTJQtTCyw/vjOdPv2Tvg1aBdRD+D6UEGgfPaCMCeTTiGUzR7sY51/LWNy
H/mUmwwKWn7fMFKUuvHI1eqGLPeA7dZcsTBMYU+I94v7kdqPbEZWnBQ1tl8NzjfsKjK7KEExuPY1
FFSOV0b4TyJir8A2CVl8ouxac0lzoR3yzRNLxwJvFREwEoixKWIYTvMgxiRSD+x+8AKSa6rMaHGf
tlNuUn0wRyHgjOfOCz8zT+2L0HdUbneL+bw0yXUZg6uWai+zZWa2JOJk9kUgiAJummSG4TnqTrsP
Njs8AcX0FqcYqqQSYgS++/S3xdyfFWNrg9I74X9osDV0FijnYAbqXB+8LE/8u4xIFktNoIe0kfQN
m46QDRxCA1WmuvEp9lUhtautdMmdHNx4j3r4FSxGwxrD44Y86n5l4gG8qmwjMzmNFZRFdhfMATrY
SwPjwnUvpROLuJq7zfKYWUFSXYYEgetXCFZh3lHfXmqaC+rKhDUeG+D9PFVNorBOc618IhOE4pek
ZM/h2mEayHcIFCoEvZbyvBtDePkxxLdqets4nSr6FwwJtwIOPnRfuJNmyE7o59NURvYEcFq747Yy
6YiHBpj2KW+946cp0BbA5ynCx4vD3QhSorZJVzKmLe9SOUlK21mYRsoqYxbJ8i0KeYNOHffUHgsi
gmAAty0GypSMJ9J2XYbNlCDggiP4ug58HA/Wqgdd0jeQlozCxVNeP1nUttpAJ+lmG1VjybSZrTDY
y5cbvWc/GKof2gekg5ZGaqa0LgXhoLRgw8bYHo2+jHZG422+3QcGNf82GR1dR7NAInBf2FrTLsHo
6C7UGs2UPVT7pdmp5Vmti86kdJvAdFeRNv9JOUKxv/tKOzYnInEL4YEpmy9AMqzu6J6kRG+CwFXR
hlezz3ZZcS8sA/wMKAqCjwVTPBUuMrreWXZipD5RuDtEL7TK7Q3gJaxr61QY5Ft9R/19AV+Cpwit
yzvayGaw2dZILrHQG7Gbx7e229xKqxqr8L8eCOudgLPqt+fY5dJRTeFqO6I24qGSQKIaxg7ShD5Y
ohrrO9IGOR1d27HSWNOeqZrg0zEG+6Mi/TUovC1XkcS3yIvxt+jdlWsK3mdliqDn9ztMrzChreYu
IttVIj/CAHoms6+H6vl0+2siKvCtZHw6azef70FMVy2+Lk8mvcWu7+MXs8f3jkL3SMqcFYzEWQGX
dPkz+QZOc63a+Hc7zWy06HLfD3orAeHKin0N6EQuuuuqtJim8tV6Mi+2+MhsFNHQ+3EK81oo48FF
wz7lt6bGbxPUI4XaDQUtX8RFAnf0ogb9nwz/UDenDHqDzdECsqpzMGNsSk/mwJAaVSTJ9/IRbkEY
EQbIzyzb6AE6j2JQR+0AYnjg9WJ+bmKyOGQ4e0rlMUBXwv0BUjFwCeNaUztliqGZB0ay0LafF56c
E6cva7XUALqHDYcH/pklk7tj2kJrTI2FVENy/U5X30A9zAt5ishRHraWDk8RQtdeBHzyab7ym4s+
33Pl4gBjI6+S6E6sDPRnnj1IBIB39Z1bCZ4aQPzfEJiYMy5wv54+jmpx09/sdED2QTrnEbXdwG8n
/XCAu9VVbYT+JEbPlzwNrNtEuoiYh+X0FEb2nsP2AUCsUw3yAQj3rx27KNzIySrSnPAD0+ToJQpw
/KOXLvTr5uTHQobCgTyHLVZ+fA5K6r+tBKACh/CTPcdBqXGUATJzUURs58KA6yJHWxFS841SXbj2
fyX69cakaZYrogyNH70yCsMQfXcp1X1NjTdVVx+WJE9XtLCDMvqPTnJR/QvUJnAP1s1sWULVYv5Q
zEAjxF8Aj9UqMW+5kcImKC+cwmZpyJomsioI4+OPnnqGrFpKtaUu7h0dfXt0KP+VKxq6Smbtzk6x
PXtNONm/YXinfjPOMzUv0ZQv8f9cIaKqDMoAquswqkZQ07LFcPjO16hFns6SrDd+gXLgqHa6OcL6
vgcuQ6ioFM/ZqRIFDUhx2bxCefi16PqrFGVbE0XDWKO2EA3amZcYzhitomjv6IUygDHuaUT7OWv0
etZQpff62WNvlks3uLtvjEdEuB7VrScHm2PPA9tdU54XWGQYgRUr7EGRLnb0Gxk/RXq8Le+eQ0M4
ouuNgMPvAXHQwUXfHvxfGGEMzPcYd1UhdA9jD4uld5WPZKZiewlXnqCY/eULZKClbysygccTqpE1
WTFsR/0r3/W6c1fzG0/QlWBZTtb9LP8VzbflO81UmwM+d0uX/8H7MsCAPTH6ct9qQJdXanjDKCVO
mNV8/L1jHQZc40f5a3k7cmuKskeD7eI/znpSZXDUbX+ojGzRDteShlUo4TTMBB2h4j6BzoTDNkI9
b6fFjKZ9sjuw0eO9P1x3FhhrlA4UKmsTYt0IuHY0hpA75ctAv7eBgam/hbcLfT40X68CusBAAens
LC9AysLIG6IaR/hiFqnt7DluDlu1TWrdR5eerUDCvANTkgbAWscKlbxjCrYqcrRKV3JXKrvQj88U
/SBrpRoH9hVaumkOFM854R3i83PUGVs2dIZlSlKMlIT/5lhNB6E/vUENONcssyISwAk2XNVY6tsY
Undg6WN7gFBI5thAdjdbg71rpCj2WJLol+t4lOZyd5BnZMx99HdnTLaWO6vj3jEMOd01fbM+C5l6
PKPamV9Elgd2TAgAUx+7XEQLAyhltBRnVOd8Gxz4ItmGhe82vZnDS/mvRyhYCcQeQAcSu0cu/1I3
205eTHcpVVyMec8m3VLmBYSvru++RBOChzBDbJ2oZVzX1FxTdMZeXniiY7WCxTyEcTSfQnCzr0qi
YxqfClRIBGLfkIpq6r7A/GVYSp7bjgLJz8gzQqFUrsa3aIhOX0d1P4bXkrT+j78gwjWrCAeNMejb
XwncwJPimA/Zo+klPQP/FN0LsInvb1sPgscrZAw8sLmgURNStYLVuf1gFaLWn+/CVPKP31gKGCdk
rvXqgwmyyzqMdpsfv2OunS9gRqp5JLDKCCxzTa13gFd4BtCEgjc9yJAeGUaVwt++r+tChMcqzHLC
8A+iQvrSYKZyimssJZwIS0a+PEbfAtnZIrSJ2bElhC6FLo09Wibu0C5GzrkXXAOvy4PoXCvnSw0t
wlQpSRt1b+Qc+++NEgLQnMHc0YHCriTeY4cldBzWrdH+redAKbmpyig4sezQWCN+W90sauYvrv4v
Fy/b/SquQn8XFR5OkAq89DBDPlY/LpV6NQqKUBGrVrON61g3kY43hQheGVQTRDxDy+ywZm8ch+b3
om2aQnuBtpS2TxlUsww773gpczJEBco/L3hCRkBgQMtCjbmFLjjpgjw88YGIDG/+9YqkwmbhK4bo
u+6cAz5GVg+RUO5sFiyeGzh6xLRECoHRK8aKb+JIrzVn570G77XOKJpKTCW506pmUQxKv8I+hzGC
U57hFrwRb6OBGEdJCB5+jWQwClrMG1Qa//upU0eX1NJ1WOYiHBpeHz4jUzlT1oa2h/HZu0OSvx3T
55sPQRWmUlmd0HK4nBQJyYd8h3zb1ExJVQ3qgvTmrCvFSFAd/cjNGiN5+R6WJYjUHohqxUqGQTaN
WnxRnZSGKeftD9m+QVLupgKqkaZKHvqUGdCHrxMkth9zPGnOoTX7AGDJorI+cJTJbuiACgG3OXpH
s4QauHjRwNKtC472Pw3P7HO0iomKTxZR3YKZQY1DfelGQDGYAcMndan8hLV6L5E8WG7OzUNDh2kK
mb2Qw0n4RD9oHdvAxHZfdPjTbkWgX5KImtwjz60t2MrNEblJEMVfIYck0LhIiGjQZb5kRecN0FAM
sueyDzzJY7syPnzkkgOHpG+DM5289MSlcW6o9/FFroIwIdz+/Ja8Ac79Z3tEechlgFPrfE593L5r
BwGzlvqnOCIUtCGTqzgzx7Yd3NqtJXO1AaNKu0dkklqTknV4RXEdMNo4IJY1g6du/JQ/G3NN77dV
k1gFn6VYsEXwsU3AOaGE7+5jftOiSVrNT415oaB+LcVquCXFFTet3wwr2VtywzhJdPgCDa4molSq
GpOYKqudoDzDMGOsv4ilfZNrXFx1jA4ChXbbmdLOWJzqUnz+aR835sl1A6l8zXejil09lXDhIqIW
sWtc0nISzDXTU80exEocJOjR+wUjF7rLOflX6/eituJZA+4MZVhrnx4kwnJ56i/Db8moRBraRYfC
f/+N0bTg+HaS4wUoDy4RzVCqQmVjRCII8l2OcKmPvn0/yjJK6reuAL2kCNfKbE51HKmhL4/zZgzl
pJQ/qxU7TTHDPkJBJgwIqGsdeUCtGTClzZ0Xe40yZkkrQ8LxMNHa34Bt8vUVHACpr7z91hR138X8
R7MGNnLWdB2ADyydBjsgaqIVrbv8frbZd24b252y2WOYB8L3X7Cg52xeVdgU8RbEZ0usw+BM+Qgo
4y7V9tQrqlJ4V8qCLrcLB7ecYkhF+ORAxG5CBicLfRp22PW1g3EC5e0xO56VkcK4LZgn/ftcdI4Y
FfRW9k0FxhtTRBxY1E8i7y3p9zj82dfJQyLQ+YCzTTUn554K7d7/uBX2r714w34CULjIKb9yFvVB
f7K6A8bq5fD9orVCz+k9TQdHJWO8bvpOvn+9rRpMcQgPAQYxJ7rk0YLBGjWYj/TBaEWcjaFnpBov
4H1G5/aQcRv/+05aBnniuNXzzc0Cx8AD6Q/4DK8KkKUXCDR3OdH/oOMKc2bBAKlKpE8xuA0CfdG+
r1v2iSMgBCRF+r+X/jZuJAjGo3IRdsxU8fRdfWe5j3wgcxLe74i99wxSSFX3RzYZD2zEPlSfNIA8
eeSK9QSIiladRxMaN27aO0npPTig0TWRAxxKIvhQTqXi6d1V2dFw3R7T0/GmM4HDkdEFJcZG8tQ2
mHT7JBbXatGRD3efBVOXMDv0BO0Av6pyNxoRaDUz0DBrhdTUgppe80qz07wAGEWOANzOOP8yDDgb
nwjAgYazVNcAhN866KyPqCs/r3QZ+TSooD4jYUf4bfdFKwQyFpqBBLO3xo1tGAKvLoajq2QRXYRr
JLB7m86abFgKio6cF+je6xGvLXbl3VtVluWhcbNQgAcblHGQ4QmRiEgY+k6nZbHGn2kQesbuhZ8a
zIkLek3W+H4jACUcQt3hQp8qWUCYfzy9CjPdOphZmu895nvs/9xEzXK/HfnsK3QZ90hlq6ZmH+zp
zI4YwYY5v7GEjB9TGbgy2HikomX1a0czJqkMtGlsQFB6UVwPptNk/yzxrLlEnMHRUqN7+frGbM4s
aa1fnrfb2cqHpQgX3yHNUnMWqZQVy0yIptW7wWaetVmkbFLErDWGb8/LxDrKkYH72QH0CYS1kusW
LrGM+iAl8bxv6TLXf2+BfLfRhLkryPnCt3vAeokN4q5ubgGugTEREL882pBGy1TVrw7y556rqnqn
2X6q9zZa2pa1gpZnQROCaoXdKaU3cvX6j5dQ9I38coBjfR6h7qrMrbAGbKQO2n1sU1qw7WLQee/j
GHZa88wRtOjIoIK35R+Ie23qYob9otSDqWyNUPLy3mZ8LvFtriy+9ghWnZx+bBkgIAJMBG8zwddC
SRJ0Olmm/XDPlceIKQKSCMBx4DYE1x46oxMd98tXRjNRtO83HduuzWF5wbTOi7DZvtJeAgf9M0BB
zvphdhd1ng547ilATMrmLaEiyQlHq2smPX6xF7/M2dSQhAcTiewF8wmhQEIS5YFs2JyDMyxqqmyM
hvyK+npg7HWxG/6q4mqDZr0KOOz5AllyiHEP05xIV1uwIDQ2aSPY6YULAG9k4bdBNiy/s1NV4K5O
//A+3+8dB3RmtoZJ0qnjWGhmseywcm3C7SINJZfjmQ3XG8YbOJIqegWDEdR7SL8HICcT68ShDTxY
N4uuO0R+OJDwm0UHjCmecQulPybQoJ1EAIBVb2mds40rkTKitVrdjsStdyYH/qzHdrdmC5f4Glvz
8UpXkjpADHTe410Uy+e2Jc/QxV2PLXsfLwiq28XUgcw4s6zo+x30z3r3jT9Vad07ee9HZnJlNH2W
EN63a6HQabYZEMMMClfzgyvtXuEv+0NhiyKhUg/SRgarlpHnZ+01xcLzzn6F1xdFfZWyaYjltc/g
XrkPrfyNakD8Ph/EfnTYxrEnyGQecVZKsebISX3TaV4w6rvHkxkh7sk/xvHJfZYkQOtOzlASDgO8
JIu12omL9GBRP/cOs9rD98XJqPkYBSPe7QhjDN00T45Wz9gUk943BnFry0tEHbAthz/sGAq2D7n3
gaMlesSjmxt+BPBdTJj6Zj/XelTRK4Y6c5Fi95vj6bG+Lqp0XJnNlS6Pc3WbHDk7O4Mw7nwghPJm
yFtvbLJ7TVrj+lCgRaN6xChIyVZd31qBaMYjHP2rDWEVVSb7MRZrfcwndJ+z1kGu7e0hc24w/sRz
4Qod9/1UxFPkw1YTrdC6UTGIyBatMAszBOB9OeIJswZDHDunxD/98x2CILPEmH/bVmdmrGQB0o0L
PfQAw9sco7cG7lr+t2YMH0mCsr1W+YiWNmnTjy4LBbWnKulnk7X4kdvh7bgmfxeXMKM3F7gmaMeV
gknEuUsIFondUPeQwjSj+AA5tV5SD7fqe4WklIr3bZq9gvgxurNSlquA9lgBpgedhUzQtTFyGwW7
i1WhvxU2u/BK/zbqzx/fBN5szEYkkfdM04AwXMWsMJKUMBZErPAFQKfiu24eo+PYn6V/NPae3ekH
8kVfAMPolJbIamcLxbytrrFNE0HNTVTwLk8SpDG0ry/B0924t7FBNXdp99wqG2boGLIQcRbIALBI
M9rm5Rt73HDaoZLWkq5RoQqNtl8Zyx5A4KoAZiTvnQSXl49i02KAsVEfnnUldenZGwKPHZuEMfVD
7BqoxYHz4lcihgpDvWStQrmuFIpNqbW3dqeGMhaAOJ9bsbhkgm2gf40n4tm70mixrC+5FWnH3icV
215lntQjikVqVIUjt23t8Y2gb4ZC6KYDUFpvYKC/mFV4pklOMe+jpzhJeJTAaOhtGfxgD0HgsgEZ
eYGS5dBrrpjWH5Yy+7Mpoe9c+K5u/+QWcWIVP7D1qPDj1yMpd5q9qZALmR6ufdjvqtUokOK0Y+3R
v1qINg64Z7NT9uIcfAN1QxRwgi4BMLDqFAXYb5kQd9p1fhtCP6l7042urqgyJPUAEDdHBcrIiues
TYfll1Tg7w3jav901TpNRbJzJmkdFO17dPYhCbteQ219Lo8DLnF8jUWfD/UwHYl90/BHUzzwrHaJ
8vqMtiE7FMBFLqG1mXlsROFlOQ9ibSPU1GeNVzhERJz7oVL3sW/2l5U5LfTqpsIYh3tjyEB/b9x7
ke8RtNikfXEUspfbspVzuC1xo4WiEZYsSNqeOxfJSZT9+mQIUPraQHaoPB4yjjrtecf8vw/PaPN+
r+mn1eK5Ji6Y5TmZBeMX58+OlOHVyO+seZrKfP96mOdCDh+tG98Rw5UoQFLoyF9/COX28iZrcamr
nRbnoieaedmvAzDL8HOnTnTR+womsYKmYKeEDybWLBdAy6wkAGIQeh5RfQ5E0yCHs8IsivDVCame
5wOAioU/RmjbaQhyJh1SvyRYpY4zCPQYXcCg05l7aSC08Awymn22FTDRcgj1BL4HFOLO9tGNRcnK
KswYKsNe8AfgqSzm0SmvP5Lzzt1pc3ysIBEW/vK6exmHfzpcWVJkpBDcP4UQFiWiBFMKSgv0gpac
SUwuBs9NhKJmEYDKVhS+Wn9FUjUuCHn/1r85AsIc5ZlaB5lHB670hZFxJltAjVVdQan1bi82Z7fh
qGap9SKbdOL1RIuQh6prXOQI2yx8VANLbpg6F/ZEgDISW0d+c5CLfkmR4z19reoGyQGPU8ufkb4d
nc8e39g5VY1woiKXdBi8lsAwkr97YO0W63cR3JKiCTr7z2NZ1kpvbenP1d7yDsG3d4znKAISLRS5
nNXxLE9g1JCGc7r12ddNT7erdi2NXPYNmSfBfZnpZ8W4Q5M0/6O1G6M0QBjY9ea7tFL68VXiJO6A
h9/nA4N9/6b0iUOm2zA9lVW6uQv8TLr+WsohLrewlxqaTWCns0Gz7CPGEmm4TIBMJQ/zpw6MWA9S
sV2B2HntfqpC4kteW/510+rmEwfEOjdIsTbX77ElJaj1Caf08y9NEXJbaR4PdciBKW0Y1irQl0o6
xncoS8yfvm93oIOQM4rQ9iv7/KnGDt4Yo5iFP1+Wqi7xKGkdoTzLJXshAs/mgKDvN8AqYkU+slO1
QmtAKWzqhjs2O+s2+glBZPDo82HhdHBB1f2mqvSIRNGluh1Vd3Hs4FUZUEW6pT5bZOoks0lfVRkl
IV2cfDpbadIHb1/1LiaxxbOaYEcmv4GPrZo9C/FPr9Fhu3Dafa09HYeq5eRDrAAw+Axpob1wT0b0
HquaTG2wjRxqTYDKfmZO9lSYkx4FljU0H/3O2FZNPJXDPd+oTe8OVGAPJD2TgYdgZOOtvoZ4hnIR
OJcQV5opUQ/EsWQszhNo5SnAqQlhFVYe0befXH6SYqwNqKbz68aZ/9DwIti5OYF6rR2Ll3gas2vq
LkcEMognN0uWRL5is7bm/2DT6Hll7CxKhU9KTAEbyXX6hSg9cI0ltXWp1+wbIWL8j8sHSRth2hPt
p6v2xFNx0XMA7tF81fr0HrYA6Y/4bqfY5Kbxtz1Jgo3OKfXeMZ9yIbjMvogUvuFq98cJmyiJTs0P
QobfkpATYJBlEfC+NK2mUb+3xJhjDuvoyysjFRD21BZDa+yddLa0/djVjsDDFRg5cO/m8TIc1Zhy
dQ7naj9EvYH15u9qbB37mkSdwv6nCXyrT1z2k2di6q//z6R0lkX7jHROk/cQO/zJqzMuui/KoJZW
9UyFHUTtMOACgaSwPotTWmzzRiCztYy5lT07Sin4paL+0r5G7bD1qbicke8QBBO/ktErpL6ywTzi
IHqIpTO6QnxHqnyz8T6JHrlivISy0udZrVuUAhK2Nchm70mSISPB8Nd8CZEaEIzsAhe8zSaVPWmb
aYXM81Awx7xt3aBHOh04/uji32scFNttTys80SDJGs0zftTDm4yPABL4GyATLp3SyKbJhZDyf5Kh
e9GFtbjB3Nefd8Td9hjqbY5c3Pd3IImcfdrBhZE0xPb1ZZhLcw3/SIPHOkg2iOvOfSzHBBYXMKoI
CDgmHei0cOx4slPNVSJugg0BUHjXXsD92lQ/nMWz0vldaYZnlt1C5QxeO/MWmTTlpCY76IhOgOm+
rMdaL8ZLsVDjzh9XlauSIW7Jhwwk6j5zO5GPEYhHu0Zrc+OGd73m3Q5OSR5tA9JNfzv0yjLq/6O9
ggNLD6ITwUMC5TT3837bfrYUTJtnObJen1glrMumZmxJma1hJC4QfbxHL8TKPyf3RzyFHtfogl5e
T+PRiavug+EGXQ9GFNaq+ZBmMgKK4JEEyBTo2iBxnvVo0dkk1nSF7rH6e2yCuJhxKBtj9xNMcWlP
1r1Yau6/IGjy2BujLI/KNLCYfEB7aS+QdcpEnhnlJwGxdtcBeLyXqUMsyLVW7czeigNI+IyeqSLT
U4+KB9eaY3e5ht6KJf8BgVViELO7W+qGA34VTm8CVmEn54YZFqwghcF41BvkJ22ou+m3H/zY3beE
fZk8IFrFvGd87bqA5SGDNV8lH3vCMoCtCJBxlow3zW6sxPI7s8aTeoRLZIoPZw4UnL82LzC/OdA7
B1Tro3chG/yR7WvNC5q6EspXz3P0AjotBUoaAB7q0REXrbnw5axSZ09WkJazHqQ7r8/Ht496vYNW
+cMH5mlvAXQR/Nvwsp0UjqPcJ1l1JorIofgPuRR8J+kyQbGnGOsw7V9tPTlumyZDl2nwBSd1udk8
MZz68lJNAsQzDNNwr3TkrrQIAuDtBDsbTSDh14W5PcjxN2hpRDVM7rN5CvN2lb6JaSmxQ00nEtVd
WedZ9s56+/GwSgPDcDVwIV/F7X2jYO/T+XNYLLpHjz9cmU7jEOETlE5NbzkbKOVJqkBhTseWB1b9
mZTx4MQUTmckTc3L6p+WksqS8AlgSEjg1AlO0JsoWghOpJzRfWJetWaD+N0yDyJtzqqDijhpu3LC
4K82x8WGoMh5W52TN9Xu81F7f7QhDjXWdQGRvccL6w10c9is24JC60Ej7qV3f/+QRXRQ3XqsdI61
kqHmsI5W+BXhmu7/VO7rE8uzKnVwTsT1hgr+OSuRk3skyoVLjluV+LlrzWvL9wAHYNzZff5EifG8
qraqdeVHedudKlbSqCv0VM16Mfa5bMWOSy0eJ716L76vybkpeWTHrOLmFLvCrX6FHhL+MzdEMaiB
3osGCe7yyA2N4lTZ6HxqjqX+dlOOPZg23MjHRAP6UcxeFCLxgT1H50xZ5ndI5xHIZb5E3ldosaSM
n26BkM7bwzQ5PF982mLcNzBxEwv/tIUFF8PesdtIbTu3y1laON0NKWY9XTKffaN+RqMEh6aOcfzo
W25HEWCiEK0RokQOgEVY6jXL3ey8wDSba/2F9X8OscmU10KtPFe5l1rTmVl21Wro0SMaNFYNIW/j
qxw15MH3ZsHVgjrVvRfj8uu3WSIaTwyn9fPpcycKuPjOPglD90oppnh6dC5b2A8SDeXhbSLfvoJH
kK6tNKeC5Ue0htz37MTEi+6NkkL2NnQNzHzvD4ujr9ccbsgNLKhy4cHgww8eTSpP35CJDwQndYf9
SRJ4d3RYb+Dj3qHh3q0oggLGvkMKhGd1Ael4qTHj46Q2DLmLomME8AOZcj/UnAy82DI+9Ar9lwmS
LgOKSGRyv18SVq16yy64Qpf3BQ+4xH5gNxmAEEIqwn0/kMvMALScGY7ruH6+afChaRJ+7tFtNrBz
pB4akXjgp+acMODPMfH7tHbSoOCf5GJ/gSfFPGS9MB6JTdeh+4HBy/We1sq1lzv7hfayvUA5hLSq
9jqoKBg6zk9rbUU3+Cvmv6qCvTkXr5ow3DFXK8UY2hoi+2QRxJihcLBcjfChUgvdlYLimF5ppYzt
ZmhOcoAh9TGRWf03YDEG+9YEPGnHOGkzwrbdTOamrPXDIR/+XSBawADKCVQe0hM9nZqDqOxgvp+z
6BS3a9mIOSkfy9OWiQYr+sgSxlBa2r8+h79VySQ02LbGHpnyysCGgCI2uJsBcXQs0Wy2K/b8dnl0
i5ARelJCe/cuqDgFOszrLdygOujMPv4Z194zf9EPYfmlkXY21Ee/nlN9QRsacLAs32FptJkctewR
aE35b8J7dUysygvUi/XiM7LDk8rE6c9SE32FhNWFFQyT4nbEkp0kUEqWEeYcURFtiFqQ3w9ADN9c
K4DqCVp+PxTCbPRK0AUptRRGXn4VRlT6h+aRhzeDQEr16k3HkYe2LIoYz2tebtoO4khr8MfNqUSf
QZw7Kj+10l/eE5IwPxNfIjuuAZPrGACQU3CKlEicXbnzkae5fGVLYnlEsUjOBfLl+FyRDhQDoVpc
t+Xw7BJBSliYhfuwwb9KkZ9hgL+d6Wq18FhHGeeheufSD7dEJ2vgnR6ZOLLT5Zjyu87Heo3d5cGR
S3Ams5uSxqpcKOZM1jVo+NqkfSX9QzVgiUvcu7xieJZokpOyRKPqAMDeKodBCXgaUUJJFm5jkOTd
+VzeArbScSapmrMTLkEQhRgRvluAayYaM4F2e8tEwgfv32/wR0KYseO3yX/IQwuDN/rPk5rNEtUM
z/ua966H6/ouncneL03ztuVFQ4Z8H92w6eoss4b/poMnprF9k7Rj49WCn5clyvdVkjw5SkjNlbPO
qtht6P0d8JdUtPv0g4AG97Z4sT7EnPJE3/PtMljhR5OnN4FHJHQq87L9yJAfR7tdY7kICHVp+IDf
YF0mVm6fZOuwntdOQbsNLp2961oc/Q6HNddNEimD48oXWYYnF9mNmQhKnFumiWkaWfcvZ/r1cEmf
CudrVUO5sV7ucmoUVuMt3jHKOlgDQmHHV7VnZ59iWQ/Vmi4Utu9ughLvOmIYAp7Bu5CaMSjaoeVi
MADiX3taihbqyyVKpqdYPrnqNmbRRXDMsbdtzCXqVwRX4xtXcNC9LImDm9DMHOnUYM+72+Iz8Y3z
hwFc/xAh9x2SUmeVF2c0x/CT5q2xyEWUOuaNTAMyCaF2KD6QREsB3FJV/tu2lt3mkGkAHuxNKu4S
SY0WY+yxVYTflyiEfQzDpX6jKCAnPWPfTiMWhsenRasMuRf5jQHV9DBF12Rtcsp9xyQhPlKmtrg2
5WD+cRc1dJxYK9fMxPaGC8kes0i6ZbrHPV/BJO+aXnVepnOMuZ1f6mRAHzPQd9E+35aiuL8pTQv4
zThgjijarYCJHlvy/MQ0hgD0mdV9X54z69tYrgMwevKxU4aFWdyb8FejSdxU81zIrZDjGVs+3SLr
y21glDLkEM/UglfknxW8FpadmBFf7oS2B06UQD0A3Va+/46sZJU8dG+JbNo9rln9Jn9mEvuriddM
QWJqoW9Iz2igzMdDsBpB+pz9bRiEJEEtXXA0S0gZpw5TUaJYQfVv+iR756RfPjKHC7SyFvj36iHk
Q0X8oplTD6G1t/3ntPn+p2uaUy1crt3iAqNdJrBfZDqyNNRAdvpNbDb1ta4ADrYCU70vznrAazXR
OS29uf/pGyrPHsP6tvOh6yCgJR1PgN0VcCUBQJbnIj/rsVhdpLiFVoOPNnPqQA2z5ZkH3d/vfL10
YxB1nWgxBqWmvaPtHHORGCJHw2Izc/CQHYxRDBOSTZXMBVq/SxG3Dupw1W0dXcmwFj4tqe+ORNY8
a6U8rtSGxNEBBruzEIclvhm0yLII5PPUPPKvgTEg/lxxTU6s7fQB/l/iREQUz3iyeRhp0/07FRh3
vPVka8yi24YcLjpL8nsKBYaxbQTA33eTE3wFwhoP2l1iuyEmbRvq4vtuw/bGyDVruG8W6Qub8xhy
wLVglZq6tz344lpT9SlJSUfjgusfPSlL5u+r4XJfBhGoIp4ynF6W4gf/0eBnN05xaFyEk6LXKBFe
cOE2FNMeeoh+3lrgwCoGZhgiVBwB5bk2eL3HGyI7NwVjyN61q6EXTb1fQL674CzKvlEOMUvjns94
o1HCLTN1oOfaW/bED9Y2ZpXGzXA07k2Z+f9hzgQhibVWkH5H4acJyJwSH8mvvQcOt+n02AKYz//3
iuWbT3351c2d17XCZbTD+YnXQe8jzx/ISgTdqtQFiLXoDbuXWr8eUX8FRM8um79IN7Wu0NJZvxL6
mTf83vDq8CEXtg7bAGugedRMtpKas07qZ2Ra8RB4lkzI0vjn5s1PqyuZeQE1q6g7WzfFdYjLQv9h
c3cpU+/Bk40quCIO7oTuAhryPd2IQeFiqjs8qQ2le8qJ1SwaXWChAw4OjgNyugtUj+5CLVbWEt/l
pRmVEKx7/Jj8INR/Eamnr/iQ5QGd4v5QT9CHHSRe0nCCVZlZoVizNPGwnqUDY5BoFztxhJMs/V6Q
fc1vI2Gry4t9vxZ7Mnqtioj43Imwg39GyWTEHaCFIqGNmAi2kdwpnjyI9aMaW2HEWB5DVJg29o9Z
5lRy0fNWBSLDbPt4s8I38Aacl0wmiEwa6ysJz4MR/JYnInQ9oUTQgjAW4xVwZ2JaCUSSmCYFRj8M
C9GH2XiWC1XPH/TECN/3UL77eJONXzxebbTBPni4ZYrHp16A46oqsqjpXM+6ye2DZSBi9quUMIfY
tQuhe655bSbCDcp4wGLHET4QlqafkrSOKamW2QXVIicw/X5x/o5KLshIqiYQf+1vD3Q54u+TWHv/
CC+FfA6g+Gn20sQdrbeTuRFiyyBPBOgVcZqPQ+/SJeYNVdVFjdrjRPFehkDcj+QAlbj8SbjG/MK0
NR2M6YOmTikykzYQJZhqM/WV+R22ysZk5Gb6lwX7fcQzCOwto0O3qhtmfFQBxbi3s2WSWMRqu0XB
jCrqZmrHXvENQ2jCFc/W4QG79UKVwD6Kh34oYmwaabqgKDCEaSuuLdWdbFWtaulqqjw5sakMZcb9
useZzAlVmtLwdtNV8bskQ8zCem0rL+XdhlO2lrVuV/c3kf+EXzqVL04PFw6rubvNHz+m9/zQnexG
cem8NeeE5jXTctp2oRskxBwJMYalCOLU37arX0e3wFNf2OIFQzdqtnsWIc1yGgVBBe5T/yO3PCzJ
3ciLRAg6ihSPCkWtS26GNBoEQ8BtUH+jNk+j4rkbm4v9gwqMk7S1t+/quzPB/JFSbeXTDr2EKIVW
07dY8yKhfgyS09e2/a2pS0Xjd48J/+OlrkQE79LRlFChO6TfWL4K9zdkM1FIHhxRDIpTf8qMOWB6
auDTgQeekui2W58h00EkLigBwA60Csk1Pn837Dqowa26rxhD+oYkowElHN3fuza68UK0IUo9RnQJ
IDu+pBZ6Bnishp9opIUEgdvpxG0+ePvqPUh94xY1jFasceRIYGihDnBjL7K/z+5HJ3VrAaH1KWXn
rOK7VF9tDxF7UmIkeUaQLsSi7TSXmgj90jVlcTEe5DUT/VxwzMIbUs4THFVwz1mphzeY1p8Vzrc+
t6uda0Qatgu4GU7FaFyCrWaoy5SqgtHWd/bPGjx6nQrT0tarDxUxZ+O8C2KcozVqOMQ1/sk8+TNr
vlIhF+sNJ/iHB2CHQ7L9SnRUD21JUPmUiOgIvtuPoSXpNgVx+tvHel7tjy86BGLEwJ4yd0ctADwA
CjjDNqehZ3kC+EiRapWGc5r2dodKn0PpehDQWsnTPssHBIvGLioWubViZ3YcIyxlwXfufIEDJJIo
Fx11t8OWykhDqbR/X7c+ouHPqF5OVZHzXt3el2E2tEkhnOTl4IK8jSORa4Mp8dui9g2XPslQtaj6
8I/C2uqjtJLn0s/Ky65pjRic2k0m9pemy5fhlqpMw4TU8X/u59vLz9PKsE5iL/r2OA4T4dcdrSgK
WVmHXhJ5SKUKQ36Sueoevmi87TRykwzfnopYxpcYUFK4MjKK5N8VQ8bOeO0AprRBB81Rl2JBZDOe
Ej7ZzdRNv8sDhY+1MAU2lWopGwjpJdNMkjEFR5kVVASzUU3ZIOFZT3t0kJmiSocLKTVVj6IiuzjK
un9e0gZaM0d4TY4BN1oOsrQuHJz795NWot5/3g7f1h7llUzuobtTeKyYKHGNHvGC7toDjE7mLWGd
0bppDuYRe9I25Vc36UES3UEIxNyLsG9qIEzqyl+UpZlmjj3MbwC6ol1LDRw1OfP3Reugwxo3b+vu
E84hLJ4K35YE/DdRkpuCC3LwTfsIUYy6d/ET7CCKoXnd98QgWUQa5tZDguZTs3c4piHSnMPh7SJb
8eH9z6iyE++mNjmK/cERbjRM8Bxy1vXCb9pVvtbMYcJkVQS4k1oOTjbRPWyHnWHLghqA3/BOGed1
w3jqAhO/XhWrIe5yn/3oHuGU/QzvkDjGl99x5Zvv1cvAxCSa1lMCXYWUijXhkApvG7CF2tgb+LSW
kP900VoWApu7ywjTv31vMHKUlbsYu7jfKu5cy9ghyJGIalDZJpmRIdFqIC93OejnVosf0ybX9TaM
8QOB0SsZYo+rje0zkHXvx6ZMbK5C10raHvBTgjwSvSoHm7bqMdsXHimGL7smI5StQNH2vPUcEaLa
uZlge4ODae8DRwDh3TMAhTILQOwdMO/lig7U3pU6BxndwbB6XANaAy4tQtDGYfnwxnW5dAVS/NNM
5WbkqtuuaHfSS6mpNkOweBYLqOFw0XukxKoSQihqOqmo0vPz6anRwbZ8bH7XB4J296/JikVcAdjN
w+5kog9PvUn5J80arvU4eQ0zkVqtyQKGXO1s91OD2Td7ojSR+SPIQUr1mViDvOn4CTlJPCb+jxn1
H9hhhuzjk9vyhBnM+etRMgFB1qZW3Ls1yibve57jxJS9NpPQEKqS3xE8aoNJGyxLV9MDZ6GKlzOR
lBbz7EXDvajv46SjACvSaFV7GwzAhwV2pFLphNgH/pwfafdIrlQCMhwgVvmhLGpj0YeEtdPq7Nx0
mnRPeCkzHDNn7PQFf0NckijyfOakmvrZX9iN05Sh/M3s7wW/62c5aGpj/EMzlzmlDUYu4TSdTPAB
nNKWI7IL8473GGQ4OJXP1O46vp7jQwYx6V7kCfC5j/cf5NcG++uxLXAC/1JkZrSi6tD7Z9231D2r
6jGFOb/lEV3JmgXle1nNqPqzPbtdLcgtkgVB0N6oE+Vp/K4hb9Whk60g5gRB5T3g6o6IHS0GelkE
G3TbjXdv/HZ3bOzzjdPpcRjlC2JZgN1V+bc88XcKbjXcxIb7f/+9mUKgTYuEKKY/+wjTD77psblg
KzIEK2mwVF5zVwjZwzkT1hRKkX3cC1/I4aG3D1qniCzfBfi3DWOrn4ApH7nX7AUQDOwBPrO86AjS
wf5jNiMjUr9hUZo4u/1zDAfeZQ9YD2wlSMr2B6mHS4S3Rap9EiGiU4WHtMGS5kkHDu5cc52y4Nf3
CJZWGSJmr3ye5U3HeWw898LhCzIP/RT8ALiZjB1JdRGl8RFsmxrcSTQLdRJQkZ0Ry78238av+dB+
iT6rt9vSYc88a7TOegeAujlc/7wKzOzwRjQRo1R16TgNCEZXfL0CdMCyeJ3T2H//YPx19ziLByt5
43i13uUmmlK0pU+IXyl3/zzmcuRGMxADiXrvYF7itjtPh0OPw2h+5Wo/nFLSZpEvYMxvrVRGluUh
pcjdRAisYCJRn+6fzr2hE6jNi9AlBcHAGiZwNOdNUKG0l5VEgyTyy0zty8fftgz1C7o2UPx+9Z+8
TwmpZOXIkNItgs4ZWCoMGdM39+E5Z6FyGX0WqO3BivEFh3e3BOqcbcga/JzjXGB4k3G8aalqPhy4
6aQuoURSaum/YN08355D+RGf+e0ztJU5buhOcI+CeHyFoOTM3w4hRhjxYugLGeHbAlM+jWdS3mdC
edR8O4tfSsbG9U7XaT2NmHg6GQ8MUV/oKHfbX0JStuOgDHt692Pk7gmmpoVUa1y1W5CLOzul5ToO
QMFcXQUt6pfhfsN8JReo/nOzdyPmEpgvv546hDMviiH9UhR4x1rVl8Jl0bsMlRGs8nCS1mxcByR9
stz+33ka1oFcUxfPXoJDqHKhqqwLWK0NZhi0GBex/1635TcFudOqiPRFaWD/e4bkSB/byPKRcRzO
AaLzsISUHl3aoB0jFbbnuKs9ec12bRX6eKWJNTEZP/T72QN5BvcsiCHteKZuO8hs/TtUN8FwFCIk
vJSPWA80YUNaREcZjTzvx6rRNv/rx837foDP22A45341TnSxdtaJET1o1zd01dm1/Y/FhXVfBVEw
QcRV1nWJoanV4QlCnnO1+tT4LfDp5zgv3QAd0If3oejvFGMseOu2N7W8wj3DpjCWkCXD68602wBr
kwV0LNMhU+pech8MixmgjRB7eC+AgMUt5VX2CpfmpY/FpQDNXAU4oj5q2EC4xXODZMDpuWQ1NfiB
wZuZxh353r7Gy74fB4HoadYbcqZasi1Q6gCFv23YZmS+K3RYHufE1f1u5gvtVs7RDDz8o0LvlpzN
Cno4zjnE/Msw93Nw2PcqytUPI3LFZp2WL92B5P5LSd+VZfIwovqVvZQIc5w2tQ2ZS2E2yJs/R1nL
Zh4XpKOMx2oL39sVng1LNeiHwg40h9R12czHxROIXt22CgDqdBaBamLshCIa4aB3Kz/R5HhHdenW
oHuGde+tvozD4HgrTfzxvrSpFQpFbHefZ7kuRBfXpv+YU7Bk8oKYygLJLAnm/bsqjir9jnVqAUx/
BbmKPl9VRZClz+Atgqd9xtlVEyj0cDGfRRc0d1j/igfhKcCw22Qs4YzXeO+SuRLcNNEEtGJf4aK0
SCp2df0VsIENs+dRIrAquNa813b26z45qoi7/qc28TJvNR96Q068reAiSvwxw8If8GSWoDlgsQ5C
IIcKNqn1INR/hyf7TeIvr5MEfkcc1prb+842LUWWqfxw++D2cKPUlKvBxu9PVKGs/h0sOa3NHADm
IsORzUfmmTLVtRAq0QqH7BvEryehTdORueMlbc4CoVXGYo+hu0c22mBBSOXKga+QvmHx8JNleC3f
CkxThDPmu6SmqQA61AGkJhktjNg4qOBdZU1I/ghvX1DksPo0pfKHVAD6eVWGQ+Pjc06/SNegEFqn
NoieXUIVVa8YzkK1RsQbCsf7J56xVMDgkwi7/WWFdxSwNU+ksJ/yplv4Z5mw2EskXNsGCn/pSJSI
R9gmS3ztUm8GQL8db85ktAIROPpkAxhA1JSTLvk47q4NKA5qgFC8uM5zRE/fZzxZmaD33fW+o6Cm
Di65itJ4RaAqOfSBxZLaHR/oxoX2PYHfz1lPrPaAuqxSic0NwOSW9CL6NZHjZOwe4DsiaL6wyzl3
6ZxGD6+KcV4lHhK8kMEKxEQ7yp96wO/7Hv/bY8GivpCL82a5875f8ysnG4sadyYjJcj+7hYDGksx
W8kfhJEOVEsyDLwAsd340RCqo3yIIMKO+2cjN+rr5vxt2uJXjytb+1YkND/M8OC7DrZN1Odniw2P
iiwrmIQOIFqlPRv52vo4M75+mbUakuca/pWCTAcIKE43+ckksmUm0h98R7UY6eChXND42KwluEn4
CViG4C0IpgRkTdmtdV+L4m9XzU4KR9Yctcr06RHzaq9d3YxihJigods05CeElSfS8yC0LSK5/Sea
agbt7uzdIR7krQRFdHXKNk5npZk42qOALOAH8RNDVAlR8kGpC+FxzWBQdMH26DFxYvwQYwkKpTJZ
023WnZePMY9YGqbvvBD57zbNjp8YuouChm4Mc8IGQ7yGBF5vdtsrbNpPTMAxq2iuBs+AJ0bMTjJu
ggdTxRd/mrvtyTs/lpdw5fw2aaHZqYUrRZDVKs4/n8T5foGP2srQWVY4BYGrOn7q6142b7kyZz4e
Q5sRtP7lbpLrfTu0U7DCIMvFN05CFeHALmjsXVhpsnm/yM+zFdvFQcvdJfLBUjLhodQhRGaa8AjX
bibXwIfVwIblVQbM+4vTuMfOuqH+RxOba2AnpIesDuG5EUIf0zvGLsxz8j+TcJwy7npIvrxk/MEc
/DekQqbTArlGC7npJ/bK2NhnQJioCfpV5KNDkVJA2vKNpE/yW25AqBCdb6W97OxPciE1KlwNR/+0
P1bgZPAVjyRiVB8wxU/hVRtFm1f5v6+ruMKVEIMEIOToS2VZALjDvnKJoY4Nch4HRNGKPRm0z9Pj
0m1ghtqOM596Qo5rRT+yLezft/so7S43nholWJVV6ppNtnvJaDmDX/xNbJ/QFtxa8wpDdWnEzxRf
dgeb0tIfk82RGih8yAlVJq91AXTCjRjY9t4tbI5Ja2TD8Q0uLq915A8k8yVhUY6fbWsVomjRj+qU
dgB2yGOsB2BaC9UMNhl1dXgtQ4FBelbAJH8O2LJQIc1uXIb2F//29ObIB68pOcBM7IDsGZPobIkn
nMs++S+qCNiNKPbb1UPQk1ob+ieVz1qDwwAhnQcQhWPpt6RLc0yfVIuanCbNSs1mqG51t16h4uRc
pE7tNcsYHklYY9UDcISjdY3HsCz1yE42HWevwuT4oGqGduyT8fwfLWhuLnqnRe226eFfyfhNzgaU
H5na1LB7pa6/nmqXBzluKEpozfma54YgaYuZNoOuEX2C87aQuBezNIIlooRqOlHFcx8k9h1UV8qu
Y3J9eIY0elV7z6a4rm1Ef6WlxdER7x+kkSGIFjFeD7U5MH+ukl4cn+1+SjwYUX6r63I3MybOyzyj
Qc80UMcSzLTywL+orDHwoFC7Z0GeXEUrCqnD4277EZoUfDieOvuy95SymqwNECEWA1ICCDANjLlf
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
