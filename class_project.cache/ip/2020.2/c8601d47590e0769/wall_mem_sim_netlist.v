// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Fri Nov 19 23:18:29 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ wall_mem_sim_netlist.v
// Design      : wall_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "wall_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    ena,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA EN" *) input ena;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [11:0]douta;
  wire ena;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.2716 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "1" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "wall_mem.mem" *) 
  (* C_INIT_FILE_NAME = "wall_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(ena),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18512)
`pragma protect data_block
tmM7DwCBgpsDqOQtst9yHFmwsWlkSpJcg9Td1ELENtOUhO7+BDvJ4sUHBaKDeT0xw0tIdzUcqE9E
axOTsj6Tm677/LjZD0CeeaQZVV4FRA7iadM+UWzdSvdADsi2ZD1lpYjoWlr+62gAILICMNe3VhS+
2kZk7TcAnsaGSbpDC8Abj/E3r8c6dF5jdCHJh6l7laNo2rJoIeNJfrvnuo79kovGswU5R6KuIOHc
QsqFJr+S+QdlWvyvF/HPL1fQzMaP+qeIBM4LGwDIS4McaIuIjWXMQ7lU0f0s0XDOU6kBQVR4+bFT
mkulkRRYEveRMl4EQxNMW/nbcYH+PJfoqFDzFcl8hSTF6ocoebBmh7gS76u+xedBS8oOjY+3xr05
XN6cl4u6SY1xkmsi9d4JQNfR8cELBzJWqZeW3pnrrrpD5ZFu/scySinYXMFtzGkO+bBjcP/fYzuF
upzFUTtDojBWnZerkZtnQkdcKUlVioxjEEUu9sb46kwIjJUmEa5OlcqWLa0uPs5DtVXayBgD4wsD
BnuTvPI1CAFELjQYAuIIjk3Wszm/wCwNg+dpZL9pk7JZFTZUi8svgVguoKUPwhOxsJQJ9TU4qqD+
nT00gEyZqx6Qb0WCS78MbrAZN2U+Ivo8fWzInwrWLyLRs1qums7nx8OCc2/Z7nc0SesVaJcFOe4M
D7fmkV79Rir6Z7tP1NKHkdti1PTE+CUILPVpDrC6Q0mhjbipf3T/V9sspUumBmfDf1QjvCbwk0h9
L2wOFkblMFFLKMyYS3//hxr3wYXrcUaC/YIPyM/sY0ooQeiya9/W4S5zPRe20bW6wvhu4Ox71/jY
BcKlWw6LTA7O+zCVayRx7LRMltycOB0EFS8WAD8iQ1toZRDN9I20bPZSWNxLo23B/YH+yL1awDcm
3jZ3rq+RZVsFIiinbzjONOyzACGpGoXWJjBEIHBD/rI/ocIDmF2PfA+mObm7XTXo4z9oTBchKBbC
w8sTy2szu9sAwfVB2awamXMMqRa7IXSNORE5KV/CeGylLAl9DsuB7tFJby42jTV1E5+gurJKCqP2
auGzE0nMDOE8ycmdZZOL4Cy261UTf+hJAzVHcbiq+Gd1tNqcwJJKisv7kww5A0r3Xxf+cmytL5li
3pzuNrW23auXKKSec3PuUBwsEIjttnaS+TCazbyEO6NqwV9TldhayxN6eC0yMVLGSR4Pe+wzpmMk
cIWS2lMEFKkjD8jc8ieAtpWCs/llBGR8pnKkO4tC0KWq/53R9j/OZVSob33Bnss82Gty1LhEmuDs
+0M4bYBuWwDv9spzdHHJLPe0lPksl0b+Xdinf1AmtK3GYUb1CIzwza3mMXCORR5w41D9qeZfECg9
z0ET1e0Mu9xOMaWVxMqaBUw+0TUMcqJeF/MzgHoLu+vq/0GQYiFGfmqraw5JqOxgCtoAegLueNRN
q9oRXHlOh+MdDLbrlVNofFF/Kqv+ju/9Y6St3x1YN0m+VJImWdxow5XWJ0tKQ8PDVx7iKfaa0GGe
P72zrgRKweNPIYI89ilwRIfeCBRmDYWjTJSrC0srY4FDJWpxpdWpkb7w16e0BfOVr4cI9agGpDJi
qgXktXgv8Nz192I9E6S3rSYkg09+smtbrfHDdKODLkRQ6UoIFqLFyi1exhiwo0iYGh/1y6yOCwso
tlx2pIIPxwUlom2gDm09DJfMb+IVoM6eGGFbFN+GhFcRzr3THfwiGMdH4EOBt25pDOMTTIjN3VtJ
l7wZrv4kMzGrcL0wMqI7pwgQC9jNV/1jigMepQLyJTWkpOe3N3HLm7t8DBK1Q8vMtyE6wL1cAUzL
O50uXqWsJYirZrLFMaypXAjDLYzw6bNX766StC/U3no8ON6g+RloQAkIEyUZDcgzjs+O6+/OiUcd
t+L0MScG11TdvURizhIAnpqdWFv8FE8qz6L6Qs31hgKe6Bu5uLR2aHEdquGtiN1b6PYvMG4c0ae+
D7yFM7lZ57xzhOZO13yzw96A6CY10YGZv9PgOUgLzv3Hvg70ndPIC2gqkXlOHfJYuQyPKDytMR/H
upd1dLRx6jejNkLJB5YLw1Ddw2v6i3Esbb8bxEGWn/nTrcDYfsFUYlyVxAr7TDwdn8Hf1m6cOgcG
9NTXtzs4uCWmhnJCNhM4XnsO/kcpjCsOXZNmkwxiVtP/xjeDQyaTB0ETcpcBBJ+/d1KtW2Me+aA7
UE+IuloZZGY5nNmwbVIXfTAPpTXHoEdXxX/uzhihzbpOC+Dk/7rI2fIGBR5H/Yvx+bfjIKL7fAp7
RcLox6Ia3+U5zM03DO662EwGITqLRwsEb+5jnYlG7fBY+pEA2FrFzw0myqSZm6tfwwYF8Qwt3nYx
5t0x1yy2EaFMATS69H3j28LGsD9rHCb6prYJXmkklhhs/P7K/svkEYturVr+cuY2awW/r7/yYvkD
4h6gRrsV2ZRoeNe4SFUMZJ2yM2Me17ADwZsun8ElHLRZy6DbpZOCwk/OF7k0ZdIF6mNHXQ1nIe3e
vZVrRKeSHin6ZtYtoYamT+bRo3Uh74ms2QCOiefK3WfuyPtoXn7wdsTlVrFma82WK8vM21HPA6TO
RqW69sGOKQ1fHQo8uRbDXPd1PZ2LIl7XVtibeWLPihSxs6/I2vW5nnpZ4rtZAfjIfaFOKkOxzir5
RKy0oMiyf9IW2MCzzZYRmJ8CeyvayD1S6buBjsODVFa4CAN0Q1YbXtUmX++gG0rDoHkP2eADWsYc
Yn45xT+VrkjzpnUjPmLN1PtTX+Pg91UnbgpGJ+NJIlHGnTb6DInUr/whILsg4E2tI8QeOw6Evp+V
wuySaaUxa3S7zH4CLZy/br/HSuwFTFkGsM0Ka/BOTGCgSKuCN96AQxOvmTddb0/0TuUuQacysTO7
so1Wbtahk6xSZS3v5Kxkiq9jZcOO6AMTHE+PtPeBmxboFWO8mTVxXEUHhT4Um/AvuZnGEehNsAAV
AmQVfzOOw6sOCCoSwxDQk6FwXPBeQ59kbF9iXQPWKgxAY8DMshKFxNtHDFw1kE3mgSobTl7IyY9n
Z6zEhLKLkFxTTpB/xOFyJchRNaSgsqPJaopX+OPdEibESNCSGDqmhf90yQcNuYLpu0xQg6I2t7tu
By8TusfgsRJRJAwsvdh1htxaCiauea8ImNeNgjCOUvLMxg13EXgm8ixZpR4ukt3eJL30tTSL/vUb
PB0Yyrv3MfGQXVPE234wmKgtdvpJRLVKToK7WQ26p7oeYS588VlKhAsvUAlMst9c7j48MpKrLmmF
XP+g2A0yaivym5UXfCcn1Pmg1+oT6koXd0yhh6bszqg0BD2l3YI32QYCit9fbTZCppvExuM6PhAR
3U1Rr4WzNq8Me1r/ffNSPrAPUUGti00zuRZ5CXqk7dHjBaN1tium6Wy0j9QTsL9FKL2DuX6fClsP
VluNdx7T9f4OV0ZbpV3C23EqmlP23F3rC1zfLCAbTuYOJFLkNlZLGdw1RSFWkaf7bd41iOYAkRG1
F2RzX3MG/qlBWvCuSYZi7VCrpmIhTsoXmkrIFBnCybsrHrTGMJq8WjisxKzs/6E0fJnJ8Cx4MakD
5BIQ2BEg8l9UhIwuaK++qvpZQATumBc/8G6L950J+ZtFQmKSAbI7pavY/embfnyPmCbYAXRSBYdN
3CG4watzQYVOhZRzZQUInwesYlV07d2XZYpeH2M5UTFRBnkH8WWbxnSzbRFY4ai/y3646/H7ffmj
U7oWXMNXiLVMvYUydpS1pst7dN2pheJpeGPaNFCROwCMx9czlczza+ZDWPDawFkMX8WPgw6So/vd
hwLUZ+at7h1Sm5RK1CK7/AnUMFHDnLXmuuDws+8c/LCFSxaFD9BO1aD3+0XSJ4yYX8m1dAJuUyND
e1fNjjO69iWJADohmPDSfuiKe6WmfyYOwzFo6/HUEjatLnn0CUzVU6onI1PBJzNReuyjiuT6c+HO
+JerCIiB10QTd58ilMheEACKqDPfw9kAJrAK2CuZV3FdS8LhHashZjRY3541pErYsorM3fIoFXya
0NYSeKdWTSSSoXmQbjLRJhCcX5Ns8vGrz3SFSSbDjxrg/tX6ZtuKy4aBsGvVzujXMXErbEEcDW2u
iLP43zZjx51ZRYHkfFixINcj/SUtVnYnWOcfZRU2nxFMlUib5ytu3LILdCG5/ogI8TjAyzlkoaIi
2Ww2P7y1NIOeumzsc9R3mWl8Ypm6wZZPtMya2yhvIIcUXlSS/C08Xh4gDmzOjnbM/PtCAkiHY/1S
0+PzDml6WAVtPO2bcsBY5xvKB0hbnZizDpFbE6Utc3FJ6yX/G5x3btmmWD98XJsTOOQDr3ULyKxu
YVuATsSkp6a1vvbY/GI3VsGGR5RjOitEA9rXCBVf6MqdotVmmEZIw0gmk5BLLjoB9TlaJoG2vXqs
CLAh3lAyGiQZZtz0MFt80v/Y+483HmOHR8y5FffhjaI0nnNzxRr1Q2sVMz20mLnOTcHcDi0kdBtG
WOczTlPLrEIA2qFjbKPvOwidZ+TRayG34ueVYFl6p2j7WkGeSD2ZoZ3xd+R2MRRYqyynE1IVz8y3
hRqpp1avHkFkf6d6RmhspL4UZmzFsoCd04yVrPp3CLZ8kqYIDHajzZ9508aXy1DFG+0xsaksR6Jt
asN39JJbGLX770C320n6fMTifFhCHqQ1CEbqPVbRLu97cKk1c8AYUet1c1p3XBpjAbznYrVcaUjh
JCuFu9i+Hq2QaJBrpJLojx8AGSCbyCrrmGCZ6855FbkKhCqI6peCY0HrAT59TADl9m/ZN+khR6eS
lKRRjwCKFfPgJmVJfM8MWmmzzygfPU5MR7HGB1Enpd46z3KxXzJutto6WHDktQqmgxwj+CIInSIm
vdIliimRwB9wJdV34Azrg86NNdLMMVYJBywJCDbhrLC7y/mtKjWJ7tlJHIg25FUXO6PULxBgW9Fx
rphwuIPBHX+NyhVhu+wae2W9ybtOv8v1aRnRmfawU5EVsrR6sfK0TdPaC15SYVO1IJKq8XFRW6OX
hOyKlqvWgbYXmBSaobzGcm2rZPEcNnGSgivtFMPC+earYqzg/X3c4Z12S/mzZJ24MxJfoVC0H9HB
/CcNStHxLb/tMqhJ/d7B89GfJ23UpU0nGeqJp+DcXRxpxQfXDV6BStKVcEJS8lOWGO+ZQJvnvjC5
h1yzsl2eYBjbQ7fHWvrMRJg1ASEmbnGxd6svgV0ZMTJXiPEq0ugGTRRyduW8ETkWG4rEbgz5n+z4
1+8CwOlAKSIRCk2Pch5oRTltL+MzDDvpH0//mfqLH8Gym6opJ9dDrU0jW1YAH4FloG6icG7tYLhW
qHcPU9CFB9zjVJeedB33tYtjVvu0wEWdNWcw5muVzxpJJ7oEpo+ykclzJxDeQ040+Z4FGu2PQUMr
0/ojcozywib64X0kw+KIM0mYo4h8waljLNYBDV0SRvCRrKBHATr5+LqCqbg9MT33gNYm8ymer2Vn
VliNDyKbUwUJDyDcixsKEW8oMNwAAmMTr3d+38tDaV3bUhudIOXzRKGmuyOd1s/o0h7XJODOySRL
CrJ2LVWfNTyeDCphaBtZJvk/uJ4cxwbuIiJZom4PIOhlTnVk1siyqjcjQIPp2iMTMcPu1FPReruK
ZVFlkAIoimWDLsp0ufYdRWT1i3FgKqlBNCv+cYGcQcrsGFcJHx8kLAaDhhsId2Ad1A383cDdVuvX
1+0CMfne/sMynY7qi8/4rdb9ivT/7VZ69GasN3bLZWCHoDz6Pd3+IkEaaWZoLO46fl0qJ9xx+Mau
2pNF+TcsGhJ073A75nqgWotWA6P9vOcX7VmQUMYUKw4OyGOvmA5zoz+ndihAUAfKUQFY+gngoq3p
67zihVUFI0W/h0cDCn9ZPGT2NfeRzAlSaiPqJctgjMjEYk2ULHHdMHQZ+SyUhbY5MFgYC9q2Sug5
uQyh7vvKzAaTkOjSjKGDoXgMThkzILaFbDiaH6NIKOuA0+x2uSHz4gH+EcoqniD3Jn0EinK+lQjA
mLfXaq7Lu6QZLsImyFD0PFmCXP7JGOMVFsue1SE1ThWU0YX0zaXo8LCtLwnIlLDOdfLNLYvD7DaU
S8YGQQFqNkvX2cOqVJEidosooc2T8jR+3B6F7k5msrQ+N2xfD2JGKkcw4c4nyFBrx7j1fL/FTn1z
IoOMmL1BEgzFQrpNnb4heqjCESexB+62/QOcxkLl//meeh73y2LWXFsf9YLwBBpfm3PujZaz0K7x
ZavzptSQlui0LSbzHI3ZmTMv7vfdvVxx0Jx7FoZ0cKESeow/0emJqRNPVWNc9H3XpsfYn7/X08VV
tTKc4qeyBelzo9v0DyxyoNl82aFyUkDvaMzRKSnKJd4/99xShCHaOa7sDgL3c60KdBieRDhdA4cd
ehdLKm4R1mfc48ny75XbSnTiPfexw4CLYsgaWUBtM3eOBV+kG8anQ/4MlLMLOgPe8APFw/kbn28k
UV2M8eNIhbW6SxIwjo6d1LyagYctJuNPrbuhdR70hq8kstkTcFiwORA22+QqmpMAyiQDSYygR3wf
MbbM1OvG9FTRA+1yhksScO5zLRUPL+Tg15RCRgcd/Jk62Tr6E28bdGZ8BDDJiGGI3oqQRTxwElE9
qb2BKEm+sxl4HEDjM429JSZvWAAO+XfFxow9V/mWafXp42PdDdnHviATBI6GNqn57vlwd8MwHIlu
gdeGeHKmpY8TaBsVW3jm9a1PPA2i45eiHK2D+V87LXwdlXrwrs6Nw62avtrR/5b2I2qSFCEuf3Mb
kpDumFqyY1cQ2h0NdICudHnPm9S6RkG3TCvTXI/7kBTstfQorSJgPF3ZCmudx47lcPl05jO9RUWP
D3JbN5jl4VrqV0ZAWyu5rEF8FHppfA+y6fQ77L10hjytcQAZbZX5T4kfVG+z9Lmxg3u0mkV8Iqxw
zS2sFPdtTl7+ba2m5aX3DZwvkmKTvKew09f5TbZTpWvN4Vc4eSuaEz+Y5c5VskiRPKi2iKi6qBgp
73z2v2/U2prCO688NOH+vwbeVI9JELxD+8aCM1bWU/9zFdAW7jAy1bPZKo+lqKg+alil4GLhjaIo
AyrcPLRy11yJczwdGs6oCmA4M4eWyp4T7Dki/jspj8b+hr27H3B43lJ/lt2FqZGu1PTa0Ggw+7VI
DL4eEcC3U4UYHSLdNfGcZVMj6X7GIoztdnjrECtoSDqSZ9m8oBDymJmMlx4S7W0RL5UW5W9Bfi8+
UHV6sDJFXJxOMWCPGSKjypAn3R9K/nLFVSI2o0TOjBfH2zwwlKJp/+0GK8ijbb2LECgsk/VDYg8w
cBN1EWjkb9QW4VnBL6DS+kFY6hnpjw+b1tVdHK1zThhSDqxhII75AIo0KiYvu1+3SrAnnBHzMEs1
kfkENGsY5SD2i2CA71PNWh5IS5BACo0Dsqgql7W9pi3hAz28LHuPH8fqlGpaj+YRFLuLTm0p/hzH
eduahk14YYEwM+DwpeBldwKrTRUGkeM/2ouUknAqaCyCoqQ8Bbrn2VBfwVxfHWiFtMdKBuBenvPE
POVyXaF2JOAXzX5N6ARfVqysG/AbKSBGzzhqvFjKCxnpXIdSCBWHi1HW/BhK8OfsS8vraYTurNWO
unpotehvt8Rd9m1ab8GT4MV8alMdGwKzYDHSDS0C1Pd4Jq63y24ucVyq1B8Izt/5/VmxPgEIZU3i
QBjW23t+5MtY4y/IxqGyJZxn0wOUhZgcA73Wqd5utb3ZIvaGzx2bYZbK9wBx1rPWN6y4BXMEVxt2
2HeJj3oCvxbxGBhRFAHtuqqmixNrCKBH8IwFEUimeORB4IOfq8Sv879tfGJ4avu9YidbatEmYi5g
DFUTO6n4OBpHnIPUcz/mgYlgIuy/Gknm1LgesB6ZNiBYxEysoowYS/MwIDgBeTCi1hohgwoB4GKp
wwL/2eBgRt+iLlxcDJCdfu0foDYHHvgYCKGDDoEVk5VMTRM9gNggIXs45+8TxHg/FaQuQXdMcy3r
gvH5YJyvJosAi/4gwT0jRYMq6xj2oSbfXjdHKkdIuA6i+8LK7G24ZdDki5A/Ko1TSSzDLA5lfLC3
DnpBEMEFpiu06dED9sWNObalc8wod3CmFaSiD9/85WXdUGcjhy5ClEey0nOR62FVbHVvgbHEc7Qn
A5zD3fMXyFEs83LVeSYxK5ZmUcus+Z/NbzqmF4lRjwKrFYop1IfMOPh5C8GS2wHTbHn5aQv2NObd
ZG1k+l7aL/lu2H0RQjb9rJtX8TQObB/d7myQdCLJsLIOWqf7sHYG4uMiRGWwz3Ybc0+9fkEEOa/B
t29bO3FSyP291vBL4VVvFnpmlPywgQ74SoWIxBCO+pjLaL+EhLEdsl01f54h300d5s7m6miT9ozj
wqhFia0BIvYVKuHLheQvB9+CBntFJEq3spZJ/jA1Eg07/Ui8CyqUPpD2m4UlB7Fd/WYVt3LEMTtb
D19z/U0is+RSe/Jk4zUZYPqxQR+iG/3UAwIIpN3kbPaNDkO5FL+tlmqXBsaHRol9uF8ZX4mJZ0hp
q1w6Nw914G97Ul4Lh04RxBmx+fesAg5vp8A0r1yfIcTcG8AiAEkYrJmkrKqxZ786vhSJWUt3TPyx
gi4HWV+4LmQlppUs8eiQGL9G7AQ2iJIsXtagZ3ealkrXP1SgHoIx2ff71eplIwim9aiJtzeQhjZb
fQrm1LdDaevfv76vZsjEf2kzaNgrjURFmrlFq74ExlTQZ584WCtfsmcELHWH5/9b1XRyBCGjx08w
Oof3Dc2f3W326PS92bIwg6xQYUm3FN+vwJk+dkQBBiZO14t7YpjEVyE0Kw+Ov5MtKKGeD3aWsoVq
7T860H8ZiMYpK+pbi3J6Q2PHnu/FJy/DrpHgZpZcZdTtPgJUs8oRUSxVPzQXyV5kbb2uZ7BIbY/c
NkUpcTWOSdkHgC+QaFrrNTC0pPV5FmWRInine0kVCgXSMKiTQg7iL9FOHKmw6akn9TSSRbBhbIVW
NH/d1Ff0OESp3mSfcZZQZDUNW87/dEplnuPdUvlPUfGLFqY3Pm+jNKmGsb8DNb0ZGhYOvOdFetk7
ZfXZgIDDonBh9kSNS0Pm9FqSZiEeygBsIcVRsXXv1/2/3rfpK5XXbSuJniyNmDDYIOCSZv10Kr7y
PdvRjVnJf9hiq3c3+UtYgdalq53SJvLJptlhh90nD4m4MBqP0kp5TsTqHQ3mbyKtwLC7totKZMP6
ECW0XZSaDLZSZQOrfN99Ia68fnF3Wbz5J0oMrcBFpPCPQI8l25YqQEFtrOSNF/De3rMop7SZEaVV
/zHDI2PQqspjfny509yo8sOrpLKRy7beHag8wWiS1F/8ltRv9OCb5sl4tT1RKRUBIp7AcJHjHqix
Ocg4YhVW/sFbcYMuqrD3d2BzQsQWD5MHIoY+EHf1l6VAqU1VI3binMpMl/qYU044DFi8ZibEzqXG
Ly1LARnL1zxcrDx1uRWje+D2eBTOvLbwyQdcQ5HFwxc/nFw+fRJyksxxCWXmJPLBfgS5/MEFYG5U
UTq+vdMa0YsHVBOzFi4L95xFcE3pF9OFcr1/eLhOGzewv+90hdPQf5zuvOE175f64h21MQQHxSS+
yZV3jdxiYB7qPpoNdDLNWuIyogpbASf4cXi9XIxNdV104wv9S5e0TIied3otoiW6SiQiy8OQCbmg
GaoHbPr6qPVV9HxFlHYek3py2hEWR4I3H/uVzWOcyD+t3T+2WtF0H28y3R7Imdot9CvRD3GqSgAh
gBc/80otBvbMSqn9SDGY8MzuKpoppadFd7EzodOLS4WdiH0zRqqbgVvqmzdP5WF94pzOc5Js/FgP
1nNGldixjnd3qWeEDbx51Dp7ghseyNEHlTQ5E12SRicOsTHk9g1e6Kej5s+nOYI4fJipwAM7TsBj
h3+0HnnNh7zaWTpadmC6dSHeAJtsoEONncOinT/y87xeiQS2nt7z9x7bAWvrtAcA7T5G2hODG9dK
Z8yIRLTfmOJFUUM6T/km6tIZ/+e+4frSSxyoW9WKMgLGDc0+rRNbsEFTECXPPRNTTsmg9zBFrdrk
T4WRs+j35+UiMvfsQTcxZSigifD92hnouhvCcWBDansjeg0lW6qAc746Jmo0sOUs8K9dkiBAMHis
mNGXhCivcf3qPa0Zst27ar2yT1Zs5xoJ/gLvhoowsdowqBVy0pTvlgnrtICQTE+23RBLzYZMbiVg
pzeNEoDAsV8cXXdKZNr9tPc3kJdl98z9CuDUR5Y6Sd0jTx4rZCrccy0ug+VwnYRRcQfPFz8LqWGJ
IdgYLjWCElCz06LZAdBDmFxG1QDzRZOfWxw2ZVj0lCNkKcrVJYzbJefZtIZJadBiVo4eHCAPYOxU
mgNPyoh2OLWbvxL3IP2QZ/y74CDFb/4w488Rp73+kS913Vr+lOJBus5AKbzaaR5w643TAR09fY9r
vX1wBVqflRWe6OPWBpFSZa7CyUDjnbn5CnY/ru5OfW8FucvpSR3X2Fp0B3f0k9kfa8z/pOtjRW9J
ve+xJeWCIuioGbMSEQ6Zd2V1ymKl935Ol5IlkZ43JO7EGHsIj34INd3rBpmWy/OW9DI1gSIWchRn
qGbwSY6ZIzFNgIr5Ch4+qKJqvHz0T5gtV4Q29jKQ1r2YZmnhqjTPXrbNh5uqd3ScC7izd5iAq/Es
4kgzb3Va162dEVOr6Jlz9WNermAV+agnKsflL2guGmA/wacC3CD7GZEdLbP3ICzM87m+RhpgdoFc
6h6fExPZdGB12emo0aEfV2vrqHStmXDQb5lcO9o2mGVatNESleLYvJRzZ2iQcAEbf6qIdPQMgGT4
I39dT0oxbXWcUx7JARvDNk1aDVoUMjCj30MwXFqJk7KeggC5ua2NL134/dpO9qabpdCbo/8FV6PU
gLXQ478ZMmrBQ/ycM2uLf9HFke+3YYl7o+HimDg9Bc99KmKejkg8fO2I1kp8qLW9KSuZeZ8/wC7G
16fwpSZeBWSBUK5DZFX+eq29K+RcWWLjzzccjFNq3HkQsP8mlPAfO9KogvEaiyh13e9q6Xx9yO6m
oZts6RrjauaOZzU92aAjdmXmqscIs3I9kGdjr1dX2XF8wCPZWY14xJtB9K8iPQG2eOsFda0Rh5mK
OfwWMsXH9D0ba112FsC8WTQZEkOiycn0JcKVHdemG28fUCQ1CW/DKXmCdmDtQJ+OMf9bAGy21Yel
zaCH9GM1rNKXcDBQY0dwppQm3tyuxB0L3lI3NCgaw8J1wwC30KJB//qKsN7s7gsoek6HmbvHwq/C
DKPhBwSPKQ+BknFFM/MAy9qFFrsBX2ycDzwZHMtmf66Ho5ZKfyLVIsxDLWvvge1sKUSxOmTOT9CI
HSz9x0MIIjvkbu9y+fJtUfgWjYySgcaiNymuUoTnnCchelJTMu9iViA66cGZKpmgKueCegnTdiG/
+sG6Jb57+bObdKQDlx5d2+OgpqWJAGkA9pV2bPlyWTWeP6HigiBMXYkYvS0XGUfsOoCMfQj64vJU
kFC+XJ6qXpNjMvdUpox2SjS0/QzoWwitvecBUDGSebq6O02zeTRfOCFbrJwcU4MIUZE86zylmpKL
/yEEzjxGtPbxtTeuKfdfe3zB6UANUcpVTRoOBBBa+8ixhcmvuXftmR3wBmdpDKJz5QOVFWnr71VY
fqnfP1ke/dD8pyK8z5McHmV7Gmi/HLZN0xLLcPs3B982j6vwATWjIbqMitnR5MEDdDslEMd46GTa
fej7hBUAsTY/2+Cb1xfDjAlGTL+arzemqcjJfPH1MttNJxJru55aU6hy/TACS2+f8vshtYH6M6Nz
F8yURoTSTomKUJINNFMepiwsZFY2znjXnjwYzMrdXSayuXW42lnyskY9c+tsSsUx2BdCg3XFPXqW
RNtu4DNKSS+UaMSzNysTmEDzSY8z7oCAhleA5M1eVADp/vCWmaB9VwL+FawCHjvyKbAv3RYLmkM8
Wf6bOADzBQEImZhbTh7vOLfGzM7pOYDV8svTbM3VBZOXCg1GR5WVYzEonBkDjLVIpzmHXeUozjfe
fxkXos6uQc7SHzpJ50U6xUvmC6+5Jfp1hpwXJo+O6nuk+Pu5jIOayemRn9Xc0auQ1VHOII2ALCxX
Fe6ckBaScC3VXPL2Y2t3RLNL7V5zcgZlrJ7C8eO3Rr8W5DLNx+ZMI533hZFdwbo3qP8Ig1svIead
Xz9UyVfer4y+YZ2iNSRRdSQKEA7iZUFsPLId7+WxeyEm7bO8Uln2RTN5FXw7uMu+PEskrqaC0Wrc
TDkMv5OIJcDlV+vJ9z8YO776JXlONEXhRUMnM9AO4nUYdG3Jlt/tmDAbNtxsW6dzNH9UjX3g0uB0
R2F/RK9SKTN4cfjjjl0PY+/VZCEgqa0Qr2wAWdiaqiHB8+4v2EPlWJ8X5FmBJir7c8XUUIa2mbPE
p0rV3aAYmHDUObbnAHOaYcbJhRp/cdYdWQVd0FdRiLbyPaFf6iJoqUw6g726ubQv7Mx2EsnjIhs3
T1CHCi2IXV14WKx5qAR1/AqE6Q5B78Aot3mN5KEUUsmhPvcDdsec7OjAZNzi+1WHGfqVzYFKAX1V
zOXtS6TzJUkCb/He3sU8OVIc9a1g1I2IJjRoHGNQDIyMW07jS/+rYMwdZWG/yq78b6wnuBs6gaMD
48vN7y6mU4Z8SxJAZXuD+TG2LCZfCz56tTdjbpqqKRq6gaA9t0drEsU52lSDwr+Y4SwEOhXx78Et
Ykj+06jhjVkMF2XDgbPPZippZPMoOZi40LIvSxdGbBx4kGCksWJyNMMCkxTeIh2KdtxnOIsTGWz9
G6DRhySsuwkQL9JDQ8dDD7V2DH0R5C6TT0mYzeeH+S08HfEJbzQunHZ+F21/xjVKyHEuiPQsvb5I
dHFu2YvvtM/d7blq+JVmC59lvdQAcMbaGgso0vfTDQK6SkW0tIjiQQ7s25i9wTU/RW+SdFxC2ANl
ekMW34VLd4EM9IHKvxMIbAFqWxbKAN16rey9JJPC4Uc+xppSe2XeRfqludFJ1g/GyLmFAfDQT/C5
FehHFxGvlL4CRmIxE+qzdXAL46lM+HDj/PB4+0sQfEu5Lf0jYUrZvyl7gNb39ydRek/N2pAAceIh
Zfe0OkVPd1rcXFGopUya+4xKmX/m0a+ilicqM7f7iSLX2vJVbXnKZ3McLP/29jvsNItibjDsUB2D
uRJ06j4b/kDoFWd6HDo8zxoSFM+IuplxIlyYD548OzVD9M/X6bVOZpRDzyy1b42b7iBBhZi7kXcb
SLl+ryo7su5B2dT7lIRXf1bYeE90Irogk62bGxsPYo8NxwFcbMvCIyLgIeyrKV+Oq3rGxcu+J6OQ
Ucr4esgL+ZUjJLdgDJYZGrRDRw8olYKo8xKcCTh/9LPdYlJYVtlOkYkAkAcIeWHWQHPO4OvDsxkS
8wSPwxRlEQo+Y9BNzfBpiSQm+KrGReOFgeZ4V2GxHDlRZvpray4kx2Hkz2CTtJSInYl24qoFDXS8
ATPnn2Zoyy+FmToWIRLs502CY0tn8PWlcqwli7iYqGjRU7quTYxrM2UfR21X6ISVODStFVYlBqig
7cwB3VisH9pQEcVqMJlYH+KSskbPgXpHM9P4mwV11Sc5aUBVZE3o4Jc7uZ0zdgbcHs+uLKcopTqe
RRPB500EYK3pqkY/aynBBFWKM7JQ1dUrpF0nh1QIdrt6DE9QGAnjiKOfo95an//BNpWLytlO23oy
RzxOb+o8qxR8evQxYvsoL6mUqMsWo5H+6X6IX3UT+krmc38z6CIDEwxRhfOiAEQ91eRe5/CNYPeL
oBs48nekHFxCyv5wAtiKzacq6cBHAkoOpLcfWgZHX/b4IQn0Wo3N++STdeavRCo4qaLwYeAWVyEN
UoPcJf4WgGc8vjglWv/ZHlLGxSgJheE9NjVChtrVUaA6+BFF+N92AUz/BZ8fErbDNgS9OJCyTV6A
bWiDfTDC+kCFhia+i0ti7s0EFBUDW3CELaKhvR6Tb1/s+ZA29Wor4j7Vfz3Z5Z6PsuDKfibFoeaM
w+ZU7lzlt1h8GEotdopSkM0afXuCLS6Z3jkG6uxS0a/LcV0yMgB+zSFfBtQbxpIBZgd4gR7Yw/RX
s0gtxHn/T4Kded81FJTym4I1qjjlIfk2+5oo/hRGGyOv1CwdJmbTjQitkfQDdTddnzAawEvB/xCc
UjgwO/XbxsNfR3KSBvQDEffpkTYL6+3ErC4rQfyITxczQWzb7lI4K0VNZa4kTV2fwZAPBzcUbrqz
+a4lkR3FAYKv8LBUyq+xio4dIf3Nhzb5rLXAXm2US0qiDlVJOTxViND5bvdDKqw3/u2UKA/XlnZO
XA+2SwHnptlZT+5wR/mU37qJWBHpujz3YFtpv+phGkkwJQJRnM+LOlxdOKWHGARZY9OeVXo4pD/X
cTlUx+bVBV+s/PoxRWMh2z7mzE9cjcAGglsK/EK3/OBiSUf7auL7pmAaKfAuuezVaAVm2rQoPmYy
lZ/ZoySps/8+qirNJxdYBJAIKR3g4nNxvYrIgLPwNNHhYaCsAotQgC7fBIA1tfGeTQTgvi31h9mf
LthBVl/1anxgin2wr5rxh7LBlywn5CeLXRjVxyfw9Fwtddsl2qr6SqOJ0egVUR+4D+xO2smlN95Q
2IFbVOgXbJArJltwqodkOnGvcOrxTPIdoJvSOprMRb3QiIweezTkG4jqirC+XmAaMLft22ygpcO8
g4XNtiGQVuR6lAOr9muxZrqBUKbNRCaBgKzF35yk7kD9vSrx3GfGKMbzAkdqzRIpWa0qoC2Q2YON
QvrLEUihdhPeNJQSVKalQ1OVboNMqdWsOUeEm+5zdF6q8gMJBME9zeWOAa12k2s8zI0lFL57gFs+
HawIfCduXM1mOATLiltjJX3xPobbQ9qw1vGRXFUHVWPSoihqOJppFf+2sFAJoqAEFIQnqqfB2f4t
Hw8fMiv9BRA8R+GJ75LmugWMcb2ksEBoWEVjVzzbGnIJ4hpK6V/Ge+f3WH5L269AqV5fEaNJrXTw
/fwfuzQB+b8I3bjPAFDhA4OYETvWcXe/oOyMQAK47n+roxCfKtbIMCm7xByztAb8xwxYbBlXP1bo
vMTyvwW+iaNRcSul4mk2vYlVozs0Uk0pUHDL8Z2PM75bY+iIGPhRliuKlw2COonPpRHBzdeaSBwp
XPZ1TRnWMN4xU6M1ou+4FDPWz4FkzvdyI7kmZd81TRFeS0mywcd9embz+1sgx78yarp05MbO0t6e
7xFopWFmb6fYFeeaIDVsyxzCLHxe4WmE5AJt2GVUQrBXaTCUDFWYxhahd9vbNee0ZoKo7BOw+vSj
IA+PQ9EZH+niqVqKkPDo5slj3XXhIsKtxDqEgcQJSqlawBfcmMvn01u7Bnm3CKWjak9V8GY3+GCl
IJrYy0vun9twPGeXYv35+o1DDdg2d/8Ua6jm3zrL3MSyF4nBpHTttCeHLOe0wZO+tkdjbRcQT77N
1Ep07C63HFAwplKQamus6pS0maCBblda1uMCByjl816qBDsVKpBt1XED5VX9kAGzlDFmdPy7fiKk
tBJe/iH2B9NNFRhbo1LNQGhooz3FQ6kA0oqnWYQSgF6Nbl89DDS52ekffQKeQ0Cj5LnBgKkKYIMU
ijxpFDtTY1ohoouUk2pp3u+E22qWKUQTMJYzHSh1T5ShLSEGlOLsSwhz1kaLhCb8t9/PqfLtsyJT
3RTl/4rblAJNg0zR6Dfu5gJMMdeO6EkdBn4TRmLVcIqDlAnpKnDVMcAfFxRWa35VNNav1W6hrL2t
0V83qNPZntxCiFQ/zA96M8QSjdTE8xEL1UXCzNX/3IwilOX+UwfEKuAfCNgtDmIyi/YOCoy8pkHg
kmcBl7fe1JKBPj5aFvZmHOxR64n4XcjcVbj+yut6UWM6X+UjUATBAshb9URG5Y4nbeC2ATM7ilML
nRPxqEn2ADssxpdnIqTOrffvVZZTWvf+05aICnR2w/RcR0O6BnQ5xBiow5KI9+C9JatGCLk7oD87
NJWV4AbGmhdQOZJ4dlcWnLh4/KARrsDFgVXFm71oxocxclp7OZ/Dr3AUeuSIE04KJjSE5t4X2XhK
xDnyt/bDAQeoEHVpYPZpnagTrkJ8oJcOFaSy8DugeAlOLf9SQmUfPLi97OJU7EK55kCT/MccRz/S
N/HOXyizD0k45AoAwcsXBEWiTrho37F0vvyN0SlK9SkwpRVy4dJyWNs9gPCM0fBy9Dhe72121+/i
1rBjMkDp2zmCbJRX6pkPK4k+/9K5XrP+jxGwwxfQKvDOe/gFjl2S5LnQwGO/DpFIPINiMFhg6VCn
i6rVxRVTEVP+F3P5BlhGJN0OyoH3sNP7/nbYGQQSceQnPJbnIuITyQLq4a2lE5oWo5l/OsKvTBuO
NBLytukqza8f7Pr3KdER56ii4+6IYnEE4DA87VadRF8Ux8DycbWUkY0AOiRBFNDWmD9ldX+dHiA4
Qua3suic1zaPF2vAQg6nwZRbBwbtUnN1GUyMzrFUerBOX7kUzJTejPMxo6C5XwAiVqTrIEwcXiwp
Iat6hPerD1aSmmkzIoIk2N2tAlbESBXPgBhmhbm/Y581Yk1LkYcEwOJQu5Zx8i8ixwXSrcU5E0vF
KIh0njgg3u/250+2CCt+8hpZYOQoZDDxtdlAFnhyC//Etu9d/pClvIIuQRaQleKlILh564tKIq9k
gMFv7DX0b841glE9dBtip76+YJwRJa9ZGelaWURs0g5CpeRdyoPVjITA8b+pSkSZCs3L7aIz22Uk
ORsKW/jpq6DqFxlVVDdxfBKQGmn2EojEjBYjVTFj1Sk4/jSgTPlkGi1M1OZdBNfuU3uzuOjbjZuu
NSVh60IKsUIUqMpUY3H9RSnR+buInPfjDtwpp/eAMbm7O6jupvmv5uMfuk+zQcFfc6m4OkWWuFZc
CcnByO6hzcv9fYVfJjWecTOyeQ9FFLW4cqfkgBKP0nHo9+iqDXWdOp1hZD6tD8toy/UxH8aerEwf
bxUGX/RW1B0dxkSwG0QGsIDnMCLLc54oRcjnPV39fGTC/jpsK/yYjoCeUe+Mv52QxNBErhIXZ6hL
WNjyhXV7ipMIX+pUkrux85njmDZSxZ42/zbkqTNnMEvijqIgVO5TDZheEU4j+Qul81PaYdN2cczH
tHoyLgJpWXPfrhrH8+9bgg1jlMkrvi5UNENJ2OQ1k9GDmEmMdp/4EOr5X2+kc3TuDIXUQDimjELJ
JFBOm/dpxI6Lamxw61o6ZM7pNiwFzcByOK8KoXJNvVu/Br68V9wJf7WGk809MOvRbUyE3sByWdLa
te6UHdYd5I4iAKDXLeG7/nbHkw40gSy2yEXnf7TaIigjz6BsElV1BNVj0cxMKD7CrcFa0rmRo2DH
wDXLzVJf3y7aIqkBU6J+Fxt3srs3s0tVkIj4ZajrOH7T4o03Va1LoSJ9IHkOI1CkNiYAg2ndnym2
fol3ScSE0zOMmWEV/zZekJM0YoHKfm1aK8uCx9G7R31IEbuELA+6ysFmadxO0p128pFANFcb853S
1Cubxo34XtAWrWOvcVDcV06sxVQQiCsuLU7T44c6nvLnX2hJ30pQDnnbZ1DHnVvaDVNsvI2Ofhi3
Q+szgJYQPwBMtk3UeXzBNYwvqQdy3cO7v/Zsq8MJr8LaPhXsvqXBWuXozgNW3JzvEXsJYHa7AFuq
g0hdE/fqP62W1xc6029d+JKMaXO+xtRCsUGcm9IuFzHrzx7EVTPZDG96g3XLRn/EcQQ3NfPJNlmd
D7g4jhs3pXqGoDPm7ga+BnwtQ1+acuDDK8wMPsyPKnCzBKcoIeOoCNbGzqxa1ujU2qxZoAbxTjq0
J9W2tDkwJAkwdHnntarCq24N86ovBwhD33FHcyU7StSgRk7ODIfUluNI3QLh6PwUdRgFWj227nve
VaD+OMq7tyydoBRyy/uBh3uHkI6EBgLrsqaBxN1lCZZFLA17y+IVjUSCgX/5c3lRJ32W2JuD9LlV
PSpLigguFze2chSMVrzrE5iIJx8EPde82PR87WqiNvm0GmgZvFJK5geQJWkr46DfbN/9iM3VxwH2
zDiT4K58Oqx44x2RoZCfnaYwu3Fgg2P4svkDd5/lt0iK2ziTeu5H73YPJuZd4bJ2Thmm+z4ssJRU
RfSQcEAd5PUAHB9bGcUiDwYp8/QyzWCnTxHR3YvCVSvhTILFeUwzHe8CfAXoHPISlbB4G6ymE+Go
1NBxvLaVLPFVeyiKVgfWmCjwj4f963erE7ulgubh9CFbWe9pKqiRTItupQ+NvjkSTq1ob17fMNVb
J6VKY2F4YHOBECWxHqilcF7n+nbzhAB0VQFN3KmgmMaGtY2jD7AjI1WWOrvPuNCgYLYKWsSlWAPG
Gxdqepl4l6O8p3a4fltqWs2dHwFJc4NCpic7ZfjjyBsIs6v48RMrUGNtdxpSvI8BqPo57j5nfksg
YpFMwW6c12fiPwmPtOKVr3+KdZIfUVeN2/lbD9Rw0ESZNkhLAHy2ow2yEaeTXdlaKdjwKtsdEzc2
A/o3Owy1jnTSkq/t4zTkSbnTvTCrd2/OdsRIBeqiNfE2vI6H/P72ZhhFnIB/uEOlPrmChoMgxLHR
aMmEQuMkCzbSoPYWMPNE022nCAI2qWhC0LdcQOCxD4apM74V2Bv6f1xXgl/uIcJXnIY58z8P3CR1
xOozSaTqO7AAkRyXs0cy0vJQN57/pX6/p4sR5vItlxZ+8vIlLzC4VTN+kncAHlR3m5/Ykeu9HHtJ
Dt6iNEumwrPMj+vYD3/+ZzRPwJaiyoPI0McBG7hWQRd8kgGLUmWsJ2A2hdAWPmQqE1kWY+JEtwfV
m+a7z2ThqHdA+lyccirWCbMOCzPCmPaCjefyhQMx91oph98dcGeZaTBSmVs8nw6qAdIJH/i/CF7i
pP3/WEjton6ea2iOaSa3N9y7HAG/I+nMm1OK+U5Qlko/mvGXCYsIrjn60klymlcLzBIPHCRjAvGO
ZDOV9/iWnm4blSStbEohF2cOs1A1t4K8Av6pQ2frWb1M9TxEvLu60rdWpC+az+FlqzvN6zjAGIOh
z+onHSt0NuwS5S9b6Q4W/gl0fIDjGMc2wA2FaSwIuuszOCXvImgdCcabYbX4E2qkBD8Sddhd6N2H
nzIiYtQTR8eKTO/xA6i1vLd9aRZRvQj3cgQsDwigErFY5jfG9jGRnHh6tIm+Zm1TBgHFMJpeabaJ
FZP90TguJtz6HgThNM1L3PUuVdYW3E2h0NucHzbX2T3AxS6Tv3SOhIqzsUpBmW71iFGzV/zKsANO
bGoDkhEoWlUt19GHOumf6Du+GoLbqQ2Oqi35aDSTQ0XdtI0Y4VDFtAeg1kxu+0FL4EJfwvsmOrom
W/qGrqD5mVDHxFVEPvD3Jaksncos9sJ99897xvGkRchDVk3PYzScwph9maVtp+RAZXJjVrwJvtN9
nH4bMGnkWMDcyvVMnay4czvmFjh/U3ARvu6dRuFRocUrq46Sr2/dxLSRqBcBEJu6xbtuYUyyRNvL
OKqNjlSxFOumS9aEJrit7V0F4xngpJGhlwPzQ8QWZnj5uCL4mF43aR5NOq4mL+QQ3fzgab7z5wR9
efvMZqC5HFufjh6tjQhDVAlkmvP3jlgYhocYB9OYbBvMHPlrFYxQHShyP6gGDJJILS2pjc5Dv4kf
rH+jM+CFgO6NL0H4G6ea2IScoWpm0FqoRx0DafbOKqHtOQMli7zdwJDTtJn7zpl+phf4v+vRUHzg
0Q3CFYm0xGPamnwAP0wvq1vpBJeTlgZgZ+PGBMUSrZrPN2JlQUDS13BRxLc2KJ7YqG09HLdkab8I
WMVztIN/it/1XNA7b1B/8/xepaSvW/DlC+tkCUuArth5Rb8YBpyoWlq+U5V3WvechJQn7bDrzBYA
UxVyR9f48HQSqj+y9Z7gjXIAn0Dj32Xr1RAW1ENFoha+nKg5GG8QYpjQWWgi2FUcJPTbRwvr4/Bu
4yzEbzIjiBBKPISD8ZaAbVUhcHTSIqOtWwzCrLocp524GPavWtpRzhAkjCeguWbEFUq655R4izyx
PHg3DyZmjx6KhwX2zE6MZJNxw3YKJCJTfJg0on1ZY97FweenUMHQqx+33CLEs03rzceSM29N95Ie
OGVOcu7xcBkFEbgIp+FWslFXF+2PQV10abi3dlFVgxd4PKtHn007/3cy9W6Cfxsr2l5hcRbLI5Z8
u7KbcF6CNmmUgNPRMro5sX8v2lE4LdLP3WXeVJA3tRUbzK+1W6G95MTifAaOJqcBv0yzYQxfVm9S
/8TPtJWsdei+dvZcIVE0ShmzIBZQpDmaqWpdfmyes6d0jEStth4RwOZyoKq7hYv+NrwqzUZi4d+W
paBwSgK7xnCmtDIO/TmaNpf0gBVMpRVQgqwDedZrIXTC4QvRUiq+MfyvzFNxmZhCMmtfdCJGOMB2
3q5QLcyXLHOhZgiSk5V0KOw0dlbvjffCURcs1g9QZ2NSpuL2pinZ93+J33hJgm8EP8iEUbUMX/hO
0q4Ta3xVKGUECuz8kobidDObCKh/EKQJGR1d7gEHoIKWDobPn7W7z/Um2YX79kG0N23WFvi5IFQV
lRrlnnMrHdLOiagKlVnT2TNgcHppk568BiL3aEv2dc9PTUA927wMhh6ePxEOeCGJ2WLCKcIT9IuM
4ZT8sGX4xpmRXjVFIFjj7So+sEMSJPaxLnHAtOYHqhrLiqO7XKQZx2wZ0k5BVLGzTo9xhBwKngLQ
s4ySU2UCd52mnaOacIeeVq7ntMx+hN75ZMVdTzUwidfXANXE5dhB0PSbvJ+cJfkjhgCbMGfm3JG2
/MuNGp+joWC5QZiReqHCbJVVT3KdeMK6UsLojsagSSZZzQr09Gjf7COYYHeva3fRd540nJDI7dr2
wVQD3b3nAmMHpqEE9RCg5B9Sjb5tSNVIqRb9uaAOvyuu3IfmTmWby56lWrr/RmDmIyItBboXmYcG
iYkqJpSdJwh4+UkOTZ8VDj66xFIaIomHtfe6eg4l/hQZ2jWc3y/pqR9n/1ok7s3iV/j8odUmFF2W
4gJ3kgyn1lA9Xn0YuHEcvp0eTvXrZOiC/Jr4c4GTMQq4qSLUl7I+nsX1BHms5aL9pNoHuSwd/SHa
U84IJa6RHx0xHm4CF1W8t/+ofSDorhqQjUqZanYqANHmJOVaOC67mV8K1OKnGQuHTrjhD2q96kN9
AW8LOFSON5x7FUr/STXMDvZDGH8YBntnrJsXu+Ii5cFn46mmxH1Gkd85eSV8b3XQ3+/r9LSDBBv8
W1eBvbP30LLDgRk6TK5J85ApU/H2Lt9YCek3uMQglpNY/xIFqSLFBRmiJD/8vQxkgxp5Sw9PoKKj
aaQ2ZVD2yNR+ZoGt0AyLeB6uXdctZzFRz0Z/CQHB5G0L4JtnRM14ohgp7Mn/ZmSXwjU8kWx9Rt1J
T2w9JxfuE8I4dhxeajgYNAg88EuckL89PfqmpFxM7oOzNyrrgX/PmvWhTmg4Nmvu2/+SB1ksg2gl
HM5wjS7u9zQx6t+a5oJ1y4vKadpbbA2mDc7Cv09c/HvxCYcA5B+Aw26dj+R/weJQpXdfsqxWJuuv
PUEsoMRbaP4ESTCqP803GyB+E+22ZofhgJlc5hOKda19ikhcj4myA/OLd81+jL3iTxMeBMIOGtXW
isxm6CTLwXBlPEh9l2wSSttimSgZyuPoM30wsUNs+uu8656fZIi36jzj+Kij2LC3GbuGX51qxq6R
QuysVFsuvQo57D/SXfkr24imFkCO57rclF0OTSrXlErgi1Z/MNeHx73satZVHnxhiK07X7hMyZ5b
JixQk8VezM17r0igeEWNvNkwLFpdTjf/FbGBRgTYvqElvp7Rb88nBs+Us7FwKRra7NhsyjJddk7v
IFRfQpUU6ZF94uLWkhBcGHxz6wVHA87pPJoEuWeQHvBe/oWDU54Fp7hso7Os4TsPBIQNiRMg2dZM
Mt5Ug8U7yddKDrogTzK4QM+u92mrgAZ/TBXmXiWQy209Cwzf+IzrG3RSIsT6PGCPCtinEuvONn6D
RcHlMEdII52Z9Rmsq6lt4KNDA7cEV77lOpbyta91OlvPO02nIbJiBJtIAfdYNfGz5fpDOpzabbfC
mEyQmKM9o5z4RtPuxiWPnNz+eQkDGikprZvrDVAjoig5IVENUBFiwRSiERgkrxpPDDbtV9R8lJzq
vnBfWVKm16/7rxNJcvRf41FEXnpFA8dIS2dwXQG/apk4QgID+3HTJrde5A8wYxskjeEiTt3GKWKD
Op0afYbZkgM8A9OmioB4Qa3FN7/yfVAGX4s5VFm8a9X5DOZEiKHSlyO2wuVDfQZB7COIsHTHvQpu
lY0jia8x5ApLgR452HHraZP3b/2/vUkGzm4VIwE6aP3rWryuoky4aVjrboI+/d8lZjPQVFjJRxDI
BcPXcCOQ/vC/r5is/hogrsLFR1ZnszJ5KbWKwMTlkmoUE/doLo3IYe2WEmGugwmXzbpeiDwTFX4R
5OTFsGBkdkRXt5b2RWBC94SQ50H9dQN6cCtk33241SnsG5fJgQ/k6ik9HyvqvVkNJBWJ6STO2FIQ
MjE/hfz3g7E3BYTCzSDW391fZTaKVSpYjatv8oahYQqZJ0zTWIj7OkYl84rL5KU652I0qAqBMdbq
nQ87xsO42m2XBIjuw33+sfkMFGRObz5Rg0FmaoLDOXa/NcCmSHwK/brQnQ7UU/S17xYOZADxuHSd
lRxzXVmayrLwJMyfkJU13cgPuiPz/a3UQCUEc4UMMSODKZrWL0puGB+fHzqIaA9Ndd+Mg/FQ6dAO
0CwlBahMbU+m+zB/bQhWRRSnH86ohuR5v/+qznjZQvn13ngSEfM1QD6wW+zkX/iFr3HPtp606iI4
ufg+w5jtpaPrYUDpVLt5/linHuU1cNXbObJsHL2b25yLTRh6jMknPBLBgTDoMBVaqzTc/0C9weU9
bg9aVI7wtoYgku4km2Wyqx5WikX6qSnK2aZfpXT8Jl8klbtZfDHVPKe5p2PG09KMdUz43OQ+B7CD
7oTSEZNB90A54WUp6I+Q4tstsglV8xiJnI+P+OBVpM3+NvKGgAknpXW72a9EEhzgEOtjrixBeVzl
OwQ3S2TiEgTxFMwVyPs59HAOi+/3fqDQN7y9ftqCAmIKLmVaZ2UoOKnEYpD4O4RsA9e8aPkWcYGl
QsJaF9bURFu/xsS3fXe6cNgDEdx90eTjA4fJdgE2Nsx/ovnLQ4Yo4BnlN2K8ldFWUKaFgjVL5Fm0
2Jwlw+48nE1V39w4+XQb5xDxaSnjDha/52izrHNHaYlxWVHRVbNiGzpYW3Cwby9cudCWc8lr4RJt
gZa4/kzuiaQ3u7a1Nr/FL5zNU7El53Iow48yo8dTeFRwgX7K6yHQmuw43wa0bn1/vBCeKgYxVp2R
l9PtFMhB81BokVlB2RnxSjTTSNHBUa4i1FkJgsrGTSB+jHM3brS7lbBnzxS9GgIkCLByz6j2slHC
1sDaiZH4euZvBTatxHZ9A3dQmuEX/jGu4rXhjWMnnU1JFMnUCXSJO4I8xcTBUWRW7Xd5a5y1L6BZ
DteQMkQnGbqBroLCFdQvDJfLbRqF8hMFsjAN0yhqnEv5hpVLRjzi3Ibge8RnhbjnjZVEDu7wZHSR
K6/uaedsDFBYQF+jdDWf9Zl8r45/okBGn6Phx6V5KwaX5f+mHDrKvdA9YUpcJ2tJazIYGdvip7ZX
iLz2N+TtSszEso3nmO4ONSU5BhJ9naIoIaDmGdxxwQPaoVCI62nZaFvWl0Km9+mOSMCq128ppqCE
87mVfkEVtWq8qdzOAIk+HJsbTYE7XhmxA+B/pckJfErBlzM2AN24irC54mlKNMrnOZ9/J7K3qJbW
t+VRU2sjUWFzpFpSlPtcc0Ft7nyMA02X+bCponI4ULqyusgEnrpgD2zrdMwO3s901D8i+JPTKnxM
3qLNYXN/c2YJHHq9gxWkCgtQ66QmeOScGED3ZUZ6QGyUQP73qhyWLx600u7BGs4Ayp4DeNgiUkdy
e0mISoIEVbPMODGxSu16vVA+XdcUrc8ZjilVAxtTZvr2wZ5B54nZQVWFG561hYWYypapsm0iQEqN
sAtHiX9EN4E9mWFzDD852WzzkPP6CTgLn1BSPdqFGAEaNLP43vjolJFzf1wIosz6DVp1hKgDfTmm
2EI+v5s+kJjd6myF7zOY2vDS1RhYQohE1RLI8P8CEcRpXltEfEgUopqY6x/qkD8acKS/auvFRhnM
8xrD0o9tKjau4AuXUTUkAnrcuJ4h/RRdLevM5CSX2R7NLIe+aEPdcUsO4M/7BzwaeVRiP8Jc+PVT
RK7gFyZ+P7bcX9dLWCoS3H1IUBc45AbX15LzjNwPyRpZwEdGm3IH9GxCvi22fRrmuBOHDKOWngbQ
CuS6VIaOGs6siN6JfqD6Dmp3NYSuqf5arFhZoTx0pGx6t9DplmH0rf7sfDRFGPgEKM0BYtqE4EfH
jF08FTueTvFRAN6bsEP+lN00c3j4MxSjRb0zxFPBC+Q/Q1I32MBhwIbZqw00Gbdlf0LJhjX+Fk9T
AQmv/pTKFZLsXDP0z5MW1TCrki7D5rkDQ5nLjqGj9Qjmttg0Tjxn4a+k6zour+teLPQZyN3llIEW
PFaf3EV7kLEI+/kE4KGVsJ1e/b0UDlnWoK8JbctSdsshvZPhwGJjnL4lNQdLrdhpgL4HdUrWUtMb
c7ohVgRfwhpqvUiJI0NBix1ttYxI6dWwC0R5mczlutH0kuYjnpm+mrEBV6Y=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
