// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sat Nov 20 23:13:32 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ platforms_mem_sim_netlist.v
// Design      : platforms_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "platforms_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.2716 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "platforms_mem.mem" *) 
  (* C_INIT_FILE_NAME = "platforms_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18256)
`pragma protect data_block
t4ByPhExRDx25TOdMp7Tsc6m/4p76UHWpBbujDgn4rQCkx8O31WQVcH935NHQ1hlcnfAsTb8wZB1
bmM3znF0A3UzBC3cPIwLd5djUDSr/LejWNpFHzYUv+7NgZ7dUbbFcpt4aMWcyPHbUzvdOW9QqpzC
jDBPn7qfvv2Y60BaPOFKVF2iMMptlSOFzzxtP7i8lZf7neKxHyZmu1fY7/Xdp5bIkmxCv0QmumOB
g6ttBqp0Xa4IaBj+9gkLBPBMKBHk1bYo617tUwSID077qwAfDVOrMFLWNmHSGAE5BStwZ6ZnIUoo
Cx0zO6RVIQPTtUyl1qMFl74grhdrKtaq3cp9NPzOBXCqV0jNaqh7esdVkCWjVGMDISiGPy8AvrJl
LWVQKNovJbfFplZUT1SVQUocDAUL8yKFAB5d+SQWSA45vBzCPbiaz7EE3HWtchA/N7dWHv/DWf9f
jL1dHU9F1D6CE9Sal52OcFfzVm45rIkunRx/tq9G1NaQanbOmBNQf+RVkeN3ELx4LiI6KIQKcilV
zF5QHezJQvk8KBH045NLH28EPZlorFjqmQXadgYQQvi7VIycqNeEXkJTYUSuWD1IMT/vZ5iLQysO
r7LjDfyYLbvr1tQ6uMKb0rjvDcsPIbOEhzQtyJ5BS9PxComx5S8GN22YKByEZwk5qCbafAM7hrj7
3cbLznPZh6jXhxb9cbsTkdwCLA2G8KQQnm9OUEKa6r4fNz6vM9Dh3YxBJ4VIhR3Ssw1puh4Uky4d
mRKQKfKL8gEaJ2KduXgbRp0EI2zBSOePzGCoUdDiin2IhT6v+BTNDl0e7I/j6vppNUBqFGX5b++7
UTKVeFF378eq7FTP0oXJ/MRdYEmG0hEcDGCfjRsr2r/DJT+CrzzrAIatkaFGRJ9bshyYfU//GLmD
VwhOb7UqGNwyLgAqJ1K2tuuA0NKz4IyLcXaOiEoQ4niKdXuWsHMvqyMXQSJ6UXi/uV5dLY7TcPKj
4Cw40jSBTYrhk9r4erZl4s7DlGBDjLw21JCy5zmf5SqKAa/Erlugl6INc6kSis6MxNNFChipEjct
TUNsMKk0DjR29ZDEHf9ALhFy3scJrbRiQQIGNU3cuEgDJw8iArhJUWxfdnLpYMmqqdN2sSv9UGbS
xaMClSS75qZD2xQBw2EIyydvQwtXh0SjAF2otu4bxy6CIyhQqqbuw63WtJ6nlrcBdNICcKzpH1X6
xFNA5dyFkW8Zpg3in3EU3bUYPWTF8fcKKlj5jOgVqLglqbgg78eWYGAN1Hvu09mS/J2eW4ex5mQY
8DuKzNMTqjDIsZ6eDpv04A6AEZcO2aeI6MRt7l6EXA/pt9YgfqGh4iMnAs4VOwiNTW1TdIQ1NMFf
urjaeas4W9jkEI1/hMCnLFIapyNqNQ4dMeT+Oo3crKG/ZDTo4BhMX9UWAecKKrkZ2yWhVCmLoJt+
EldfLMt2jNr9+j8KrvWIH+tx1D8/+Ptzo4FUL4UrNNrzV/qwDCUxaArG40qC4CATSSDtmf2cgZaR
8+xByFE/lqxrOTZvXuJdmsGFAwWKd5qR/vAy6rz5viNP04HFuMa1Q5dil/m11+WeMs4NE20IokMN
WXCagwT8vgNwUQ9VRBOGrSn9YQg+IiLF4/EcksSPqVZY7IpTOLORbP1Q0zmbT7riEimHp8/sqdJX
p259Oo3banwYITGmEnI0k0ruFDBxnGzRZH/661dtIIS8hABbFNDMPZZNg5fCNpn5352IJsnwm62D
3Hb2sGTMmjvdo311Eol4c7/jfRbMWuQs8Inp8/++lOvBOWGwLlnLdR6aYXB6RUWQLAEy0sHjeXn/
zqzvrMUSDqzZG48FwPxDm5oWf/pCU5ZC31fsZP1dGcC3URBEELCVpVDKsgjWM2uM9iBpZ4MsKhye
KbNEej5i8q6a/62FrgYIZ/DuzEs0+FlDMM0ifMJnHmlUuXkt3WkH0cQTPKPOexsgkDC2P/ywgc/n
HQ4GaJaKfBJwvRzWBo/+uXUTgDPczQPRy0dXYHLB/BUA8DjMP+AVtiA2AX/0qdOjGbDL5DZEs9Ii
t/Rc+QAD0K00icUi8hguSwZ6ZkCazzsuOF2I6y9atKbRcijEfrIlTasl2edNYyKZRMPid7DhA3kK
4NpGcNVWvtOHpT12qfqUAiwaTxZB1CLvj3E50DWAxVihkvRlFvOget2SHDicdgrsUo2TaxlyRVcA
4l5Hro/wrEz4lljB+iih1SjmT5MbX3YOkXIPXpBJ6E0ItmYoGoQ3OPLIBAqbEg6eRNDOS5C6a/CT
VmTwpuFVGwdn+/7PjNlvwIBDr///XvXMSaJW9+5PWjKGjFpuvTZO9OXfKsxgb4p+Fiq2covVLsjb
c25y/JJ6mM/Z79fAuDuwlaovY+SK5s1P5abk2pNWzjlE5ABDHslBc7EZeNi3e9k7Xzo/aLVt1FUM
3bQFCtHJe8w4Z+dHbEu4x14tN3600YnPVUhIF0mGK/Qc3Xs9rn6Iicrh/UTGTIGipSO7KONC7XnX
eq4VFSlAZTUa4cAVVNpq/1XqUk4ynTG2PF1EeBW+j6VuThNKmV2kcpZAoELBItDOWD0w9WVfGZd4
3PO4Tqm8sE/q5UT2vCDpVmN3+SYbOd5KQ0ZepRCeFjfm8mqd4o+Pes6/WzLF3kLXXo+X7CTyERT7
BvlrfY/vcciYA4/qrE+/JrLJ1CT4AXA+W1Phx3Kd01NvJlFddAgWXyPUIA18CWXWM4hVKaLuX4Iv
1htgiYeLL8E25DJPZ91C0S7/1/tOpwxruZP5tePfdu5F5fWYhLrMqgufOwiDD/J+fGos2fGFp4Q6
y8XwKq0gtCpzY4ea42Hbrn985kbDpficp2ouFbIhv3GLglrFz/9qCI5L+x1RbU+rDdZTwn9zENYm
/iBiJfhqMa/aOb2isEwRL9wB1G0qobdQPr33bXpuvzzo2MVuTBUy88KvOW8vRgc+egZANC14aKIS
ljfvJ6iROYNBadbDbLSVRXZLwFab/Jux3ZU8ys9AfnV7m0J1k9jreTILe9PGHLSrARwBgfPQOoOr
/ur/ufBXZoO+rNV96cVpZFgf/lJvYhaaFGpypoE9PwHp7fVvvKKNheOXxxtjXFC4jooUFBnGOhKU
y4yUfD11rwYCvrv0O9V8QK+MGvzaYoALJPSMi9DRk4gT1gTnIa9CPi0/90MUbVVxHkuhQQXVQxGx
NhpE132YppKbZhOqwvYeG8z+Dw5orYSk80a+9+ftLGE1rQVtKDV43bZ8K/OZbSWK3y2a4jUbVhMM
HsIAjeDAxXJkms5FbzdIkFG0JMcgWxHiEL+SbbbrwYKFHXLQU75Q4Sgwz+Cm7Jegjl17Zz12NqWe
l5W7Fu4Ossz8UTp9XHtLofDUo0Sl33fnAIhAUXoF+uIzQu7T2Jyik42EzCxrZkUI+vrJH7ai7mnJ
KNCjgiartKL0TdbqJs7VCg4zPim9tRXzFHfJ1V71zNcryR46OY1hzDTxrwDqsub+gX3n2fbDbf9s
aryxWOHhsRyqJXbIEZj/w5RD7VaxAw+XVR9qckJZtTiJlKNsnAiyOUzrRZPfvmkDMvxWMlZDc6ot
0R0F7jmKUq8uigWzheD6gOx9QTwgurfz15WH02ktC858twt9ZYDUqpjqCgUWAkp/Aojvuw4qJs/+
+jtvl1rcqAkErfBf3whd8CczxKKc2kAGWJgqZ++COx0UKBNQZwh+VxoL8tVZ9FHsMj2aUpuMVtFM
dDmfSwpSNSczc5OFtCa73ySY3OEV79NqCex7SZBmSU7Tx7uqXDEjgcQeZbLlxQx1KI49sjpKCFGA
zLFkZ0Qdk+JdMjAHuB6joFGk4kfYwCyOOeOVCbdY7kqUreLx0VBO8J47GjZCmPLHBj1iPsEf0pRs
AFq+AuEKcNyyrRHSeyWf52q0lq2BHKLbCkRQcwt5+kCHHuWsArWLcS+KxD3+iE4KbPiy3HOIpXPJ
sgIy6F+W5pi8U0zGAMvTJtjZ0Rn2TTAmfBhUAWTf/xI9puwCNJAMvKkgTo0RC4prCdcvDtSp4rxi
kLdBO2dbSSGy/Nu5HM0BCUeqpKJ07q9hn3DQVitZ6yZWxVvmJ+DTI76raMNzuuxS5ZrJFSYKL+sK
+E0vNkLsEYE7ir3CCgGYYG3pL5ArLDX77PhEYlyBWkVmIoDO53xqH5Gr0NVE3o61aZhwu0uDhaZZ
wJKesykOve8p0DGjx4Oi11mEiRI1zl+8db3bNXhCwmKppoDbUcHVZ7b2d3VXBEnVXKldid8D9eQx
5c6hc/CIIk7vqnm2cpnc6NAXbK/LQLP8W1xS0RToWkET4QIOuARgUgaVzPBPohJzNINNIIoa08Sd
FYh/1B+kc1HYgtQ6eQQlHi2KdEz+Z+tiH7e+qZlDHXBx7g2JCgNMNrcouhtVEzWDXAj93T7F4M+i
iG65Hurx4f6H/y31hmnjPP7QXGN3bcLKbfYi/AfDlU8ROrkp/HQN8RRsmPg5LElogiQjCfnRh8if
BGmKY6ZujuLXWmRYV6KGhhL+Ag22CQfkhLJl7wzcDDHS9vPQdq+sL+JpYKjTfOsosvpCefUhRwPd
+6cRKQYNXY/c/vZa0YaB/uyjHNxkg2Dt4NOskJAZgoTT3fasA3NpSw/yaAL0hyBqvr0gXhN1MIdL
3o5Xy1XTUKauWLS06VNikD9nyEwUB6SSLxS+3BKTmAq7GlAB3M7wf3emWmIKOqeLDRFIPWx1VxuK
YmGzhti6MLvE/wnaUloLy2agdccPgX8kYFrY1LSlUUr3S7XOFQdOXFF3LDaENYHBevQpHFAK3ZXx
a/QIJnT1UVEXMNBtj5ud1NO8Ju7jaUKpMAui0/cERkTbDRKcOEBGMiyeyeYUqO5N4D8s51n9x5m1
0aBn9s+VnrKeH8CTSJKm67AMT4CW1BcWbipLXlYmfxe3Vf8/m4ssIgnfJrasHYkkSwgLLzKxa6d3
Kb4w2NBTHBYOrFfk06aSs1XKVpH/ehtqR71Xese1pD9st1MDsY4AnfOJGCJji4hEdl4P5ZX6z1Q4
TxUx4WMRwL6O+eFOCm3APxnNF0cKlSXKsm4HcG75DWsb0O/Kbiwm/ZEZBiEzOLV2ohqZjPNZIIQx
+ZT5f+EuZ75XGukczPiEmXPT1BNs5RdL1T97jmZx42zNjpx7Pusqtx68ne9FCFtc1by0955R7M10
8Z5GZc/WEgWONuUom79bDSqT0z8WDcdG4cVw22hV4HW+ELIM6AKn8+xlNB0bmEKf1vvH4m+kvkvq
SVO6UdJa4OCji5RM5AW1MgT5uL24KhCx/PGiZLzbHf/l9U7C8cUP+v9EwP/gMMXmGUj8lsll+TXM
G2GIg8VVwE6aLTPz7H3iNllA0hgl080vrVGa4BVytK1YIH5ZLLVpnKpyQyfCBTJ7sj7VA/m8X912
/CwVXsCjajj2ovVaxRPPZl2fvGdqsqq4vHb2nkY0PISHnMYORWHe6UrRElRKlOPGSkGgydFjakrm
W7/oYnsfo7FBD+nt0+pyXvvT7Rj3CDXFRNputLUhdivJHq6OdhGF25SspMyyUuy1cW9e63TfVguN
iXOiBTMiaTa0YD01HH+zu+AUZBvR9LreMW1U+3tGQgGtxbW7cKsCujVGqbKmeqlBexSN7akoajXO
dkTWt3KuNSBNz6nHBge0bjCyvaZefYlp1VUVIj1ay2V+OH65baeTw4BJDgNu5l+psUhBXmZZu8DP
PIfqqoIyxTLnP2Tw0O4VZHD5KGFtG2pD4unggYIekgoJMTpPOEjdcVFrIhsg4ZbHUCnh5aq1WVje
52aFTgjp2eOVOOVFkdHEciooOsUnqXfrF6nmfhFqCb3ok/ulEJHuUappUzvYLsteqT7pWA748xfZ
QwVGDQ/LAPpA2E9q59EtPGDS5VEO8uRsnTMoIORivVptcpDyF85hYUlEMgV9UjvSNq2eFaCh67q6
1fhlQxVFWXjafJejR9KmvAUKH42vhNvSvh+wnX9Nbh+cnP16NMfKTpjFLVbizHianLIEUNXW2JlO
OwIMBEJNNHlftmUkRnaxYrng5VomCrYhpoEDgPXxKwePNPLsxmmTuI2IM7khxx/eqz6y32rYNlzs
JNMGzj3ivTyR+kNwRKE0wh01xL25kylIBG+IkyyQB23BqqsD9BwmaJB1qkNUlkQ7XKz1Gf/j/Iqy
wbxfvhAWekVsdtctADiKflF2Ix3Qhu7HXmY4ZtS1eqgnHu4epr/LAsiEMTBG19MmJiyiASxLxtD/
eHB4ly9j3B7P1WpJu6facDdDtsVLPMUMN0vFczhn7JYEFKKQqlD4OE88JYELPeEsmmh2qXy5sZJw
6NPoiM/z/dr1mKF9rcTlpyDfhTWdvOPIGvO0FUUzJ+VdHufkwfb9BJVR+PL9U7aPtFO+IAWHPo5l
7xtWbsftiODFgx8uDcEtlIMgZ5LULQV4p5zxaeddOOYc4cJNGiPX9NmK3Z0vATheorhyyofJUWx0
jHgvHZPbtGz2zUQhqyjihvwvm/h5GiCW/VI3QnRY4E+R7HJ6dfzB+m6UT4g+5rkFC9pXQ4iFtdqG
weApD4MzpEaOUpRAzKMqwSKUrpc9EJiUyj1YmCSkFdGMEP0Gg5aNno5DdMkXxPoe0uit+b/Wtp0c
NwknMuxXJlC2d9GlXxhYotMLgGO/C6JXMSpZhy7zBXtNjG7TwrYoTcMtMtCL3Yl/7dAQAyYWYvod
95YZxXegiZcVXE6HjsYgnPBKEHv4bxCHY5JZ/oo9up+GrZmfBpVM5DWzItz+5m6GztFNU/+5YDWR
KJWsVOZaSg1KsvyOH3SPfximCx6bV2EO6MlQ+bviS4mpiO4+t3SmNHaETuvBATRYGx1/ENNdjqbM
c5wjRAErnGBDPnQ4HuQaHcnbSpPkqOB38XuQw6ohuOmzH3i7SqWIE0Oy11NDgCo9MuGsHhWGR65Y
R+9TCf0vPGE0e2eUGfdp78VXmn9g4afqSzKbYdi50vK3rHKmJhc5y8Qu2AfTPt7vkDmIse+oTNpE
K4Ykff/5yFSKrIIg9zTXdzLv/Mf+ajR6jF1FSF6Hm65CHJyWtuA2yl1S2tGYY9jmB+u2w2MEQ1Km
DUNgTfxmKlf+T/wziuj9RsiHgeCM9PFPwyiQSjftpZxOMOc6WB3IPsAhyPUE6o0mbL4bwwNItzMw
Y0sR4eo+o48QpOMi5Yq2s61jjhMi/ySRqNDZ6RgJqhbmbZs4PNiSIDUmv4ZoxtFltsPMVk+KHylD
2hyBaWNH4nb3rQ7m4xnhKggsbioRpPys8GemVCDdUwo6TKLQtQQila9RLojNBhX5k4V0eEXPz2Tn
2NwmfouVlOUh7xEO/9ve6UYVHq9SIiibBhX0a+/O0TA/rrE7UPsfe2pzzr2x9i7eVfOCVH8HXSHR
8pK/vjNFIRvrppSpUuH4nUxBYeqAhQiWD/6d7cTF9Sx0FnQ+txmaMKZqrw99HC6F+Fi57z581R0j
kjVEzf2aAmGsGhwgk5qvIU7RN5cRE6Ex0yVU260fhQ/uUUePxWTQqf/ENZHbjhrzs6vgEIgT+0lO
PhQl9Yt4lcU7winJh5v32MQ3lSH1OUab7gK2GY/gSCjxfEbQsYDaYbrPjrxFi0VzaQrVlvH/1lwe
5eKIQ1olY1i0DC9b7NhncMp/of1NfZx9G0ukh/ugMIobctn/CiXbCwlkDpl2OlTVM8rkQeaGrQul
0KcJgNz/UT9RvqkJ6WeOq6yjG0u/3lt/152NJJUL4yxGlCuLfaUsfp1moCgg589Fm187p4B3z3OF
1URd9HdrjrgByY3DCVBLKrgBQBp7lb2ZZ9Pn+LEZVmdbeBkPua4YGySIKAsc9JUIxMQKadtACCvV
K5J85a4N6/UKANemqq2YMhhpsYCIgmAtT/Pn3RdaD2BBpZrFCEgaBU6U3/r5lOKwtkQX9yYxB4Ge
iyasw/V58EG4jVDrOha1du9Y/MJig8mKJtDYf8X6r/0qBeI4h11h1Cj3dq38nSymvkXHXKMvGcxi
/i4WadFeJWTkwFnN/FlS+MfbSw1Qcxe5tr8L4+lyZ4m1T1pOjvZ6h7IAQGMiCNv9gWDYM2THmTlU
r11zGITn4z2YkqFWx6UDU5VSBzuTnhdWdP2qFRaDlvmxBbO+RxrJdTNe2KfIh8ucyRnPZ6X38z+P
E10UzRMO88rbnwLRhoMQQmSvYJdPyPkyGfwB3296FPcAWVVhcOV/JSTl7kB4956uASGCfRPY//jo
SpfDjhCwbrvWDV2q4fOCrfxxJcFju3LaVXyqIemwpTI/nj6bt/ERsJ9I0K+gW9jNTNg2G0r1XKEK
ET8umDxhXGCjzS3FjSVzpQQX8GuQ04rT34Kk/hSdWItupcsZmh+CONHAYyUlYcyZUA8Es3tNaDRw
jhHuN59ScDhvRh1n+kJVW++dpaQgepz1gbyXjfJZ74YhaNY+7I/KZ2zuSl2Lp/XXYgA2qSVGu0zm
R1oFmcGiLseHVbm6IAB3G5LEzvLwdDAaBhFEiN7uT8TQU9DtHJjMHV2tHKins2ev9zu6hkl8O56G
xl1bgHwkP0wLb07cHVge0HunL4UaOwiVpz4cpGi2he744LiT7LRmpVV/Zb1LfSKrAD2nEFRl0vmn
FwTd0UA3G+2aKso8UoiPNzwilJPpumb6+lEiDNl4CKSe9/PxmDk2278trhr7s/+TRNAe9wOdZg0j
GWN//wJ3YRWx3Nxn0uHUmqvaXZKYpuSuVI/YMDy/R8wU5v7rMDM7GOd+n41BpQBJzRMUYED1WOGH
L+PWTO4siQ0XpSpXhH4bF+nOSi0mlugxw17Sh2NW11Up+m+3DlOUqA5ovrHDBFH4QIBhGRNWXxr8
8O4TdxvNhozcPmVteY+VJG95fCOVdCbWQcRsvWr0IHgn06yq7X4ItRd9ZndPhzYzSkWxcWj/MCqa
3q/M6qbH6gWlovI/AbgR2HLSKH05fwtFYDLn961XogeaGHmf5S+7lAatDYloDEk84YRZBi7UUD5l
0CDx2np3gmOv+vs/G2Fd4+tuFCTCASUCK9uyr+DviOGYFR0QLTwUVpwwPw3Q1YOLA2+VdkQXj7xx
d53yFPoiUrwN8cD4METpTrilH2Uloj9yH4s0uGGU2Afgixwq00SRKcfO8RFVEv/DczvA6oQStyv0
HY1JWoVWiV0jJ3KV0qfiZcNm/C8yCYcNO+hRgm8MHvdnKJOM1NZ5I2K4/z5WijUxw0NLnwBEApys
2vRbRIy2ToQs/tOqph62E1FSB8XxWue7NxC7ffK152bL+mYThtcApqLJsHgzTgpt+a5Ip7rELf4m
PqJeEkMv0WE3jKfEaneno5Yzp0mh24FGuLdDE6POCovWjIj1xrQrJCKdQmUxmn+GtFy9qXuh76cn
59JV+0RCDx77ntS3Kt+U2NvhwuIV3tEKap6UdZq0oYKxsYkmgtbO+4fIDR9DwOQgHs+Nlq8g1MAW
AGPrG2hqWKjKe9IYL23JeBa2SVjexOMUsdYLCOZjo2J1u9lEKa3MEVh8Kk+tu5s0uyUbWr1jMyJa
LWA6FGTxHGuL91mu4hOjdvYdURNjYQ19Swnzi1kHbQ5mCVWOsEGejDMowX0oHTzRPiu5gEA2QCFC
CCc9iUTrFa6cnYnqyZb0qCYbMOITZn6vN469XriTL399OPHCZs7ha+WCXasBaAYTMEId+WbeHGhH
9H3PTvwWjFG4gW2C0DCMjFsgl6P8I9/I2GUhA5W+ruM3kluv2HM3sWuT3ugSDoWARUVW5QUHh2ow
AR5igFrzQTqAH5Rx8A6uLvBFB2TGMyPDdGFDM2KDhcdJUs29SB7SLiCgOa9jCVjMbN2U78cODied
EGgIdYCGFR7Xy84iQ51LthCrynI0l3e95tyEWNdQfZdAFDUxWptuOVPR5MB/jbisZUJZLl/SU4HN
ESU+IIQAaC/TEol2w1IP8yisMWKSUBr1Erfe2a6wyOJsBBh7sCGudtUYclKK84pPLRzSk1uPXKSE
vg6VS/KzdUnriFnFGj2U/+0q3rLgOcgls5Dmy+l0qsj1XNtPnX7+6quTit59Jw4u//UkQ1HQYcia
wGQMxzIVwbfaloGsz3SOZsGTB2N5hHK1JVq6IlrRLyYyjFlxf6+zlvb4SwrKPj4jk7cbl9GAFpbB
7B1E8h0kXhx+hyFDvW5B+Ye5PBeFEKQzgGpkgrsSUH5pgFZ/IpZ1fpymrrDHa60YiDQumbexoKpj
P2MrheZ5eNtegmET5B4LS9y0g4UPzaN+i1sYfRbCt9SclZgUghaiHEXZcAdTEDNyOBYSZpuy5KBq
efXK4jNoYhYM+U1Ojw3Ynzn3bQicjJlFYSNfnaMKJsAo6Cnd7HprEZc/tcnj1X6ORwxFLFInYy4h
OS938N1gqyLOpqbD8/JojlQxnd/Qst4pOLIVqOy2qipXlguHyZLX2jQWGaieUNt31Ka93kkqbj6i
0xTuUv/wWo90cgSc8PVRtDd/R+GX30bCwwxZrVGrfPRCUVfS24l6L45cabehHbUgGfcQkuvUN37x
7s71UnCSdrxfr2trjI288yOr9vwtExnNPPcAeh5b/YL2HvsHwRinGLVq3wz6J/AdPyKHiPtQnVfC
YLob/bLrxiuBEARKuPUzKP1MpvSsB4QjRJjx1u+N3zbUsLm4vJQrvmmDofQ/cyN9Xo/6Kn2njzUw
6LrIF8UxovBwO2KhwNkvGfJ08lq3bMX6JYXG8hFHc3Uq/wjgxWnEZ0ub8TxpJOMC+od3bqjwE5na
xEuYO3u+8bQIsjB8aKu7BrEwCLm/rZHExFyvEes25XZIzpuclWMz86jiIZ5jFFDgdTgIj+qoCks0
Sj3Sy639n4Zjv9sBaTiSGvvu9DSYiWlb6eZYt9wX4B7RdgSOLSC1gcnaJdqryH81081qvl4XbCH8
Mv9GP/2OGx9QfBUxtJ79/OToq78E/FtaO+4TrimfuydHCwgdVFLC+fbTjOJl6171X/m2LphbUjjj
iGS67Lsc6EplA7Yo3MlEx6f2GA/cHWb3Ld2PCDBU9oqdYDRE8s9C/sfVIgSS0eAY71alN+fADVGS
aUuzbcVx62GxkrXpSzem74JnNSaG2Ek99cc/J6p/qHriksFRps5w9DgG2TmogbWygyJPUMLc0sur
RrYPX82DQoSNVHI4bwSBhr8oVpSa+LRRE9xcUTC8uzNK5A3lkeDBnx+qwS76CcoWR2h1bhUw7D4J
ECUUB1HX/a52+TYwiIE+moJDyCm/baMmnrW2y3wIiVIn3eb3j4bhXpTxLJw7dHcA5YDHOCIed2j1
oBbm8I6mFyROpQUf+VrFc+jvoU4yxZ/sghueUK/AOzDH0/pFRcnEqlghsTRYSuUQuE4jRdAyxFmN
71w9gxi9EWiNxmMhxPyp+lV3uKcugW50pqBvat5gml0ZhULMxirp5OydM+GSObgH2Mk+pu0jbYXj
bkWBVhyQOLlVgjimIfzaeXOak+mrzU+wnwxhLtNQjMVnvUxkySCWaiNZ/CqsZ3RWPH4toCJeNv/j
jJUIM8kG4uvc2RWdy5kGgVLJaZFzUUWH2NIKLQa05e45EnoEx9sFdYE6jPyVL+C8+90xkugRpwWd
lDBK9F/Y4gJVSDGw+bTCbKIsu+YrQ+5MTvXa6DKrcSQEdeS8tseCVrLHZHfBQG0D00hexGATaxSX
bD2sR8rZel5pSKT3EDV/fYkKHhig6+rypp5OzgdnfHpbV1KHMeKDJil8isS50aovvmGwEIXE4AYR
c3ZSsjDdfr+ING1sHMAuh55Mk7JNCMceFBYwHkt40F8WsITYbTcCGhVTQVmAKsJxNlKc0Yud7z01
OswAiR4+rXIrxtBlKUF89SOPRjysgM/NWNnQNQbxwHeVxI2WKJRogLb63h5gZrOg6iB3MMdTt/d4
TOOpKUVXxX71a9pWiQqjd7XpTbsgLJ8Vqj0wkf9nUZKJjd7OsUjCh3ZEXJ3jg0VdZbBm8zjeQ0Yd
NO2mERg7SN56JnqFhk5QfmYI8t+wc5sdCch4sGS0SYY1l+7FbC0bwHxiK+RZXyByCQS1XubHMsME
EA2xdJqB3V+PnC1J6BeqIEx7YEk8i6gndEBzJgsvE9csjSEKnXNz4ta/33Lnk1Rzygq+2KLgRoxC
QwX3ktHPUKJVuj+076qexVVuDqk0D/MZ1R0qu9eFEvSpJ8ifSKjS7uuzZrwS5x99d6HdE38wOS8U
AIMjRfu8DHva6hQnfHXkSSlqvqmEJphF0o7YVk41tNMR6kIyCxILDkowA2eqYYZg4OVk4AGQUE37
JClhpq3YvME5HYvg3/kZx6+Eh0/RqNvR+ux/XvlWkp79gLwZ60p8UFVIkYosYMvrVqBx5nam5ate
jJ7kMSZ0xVhYt8t+/Py1IObvmbT98mHCNoN6SRoeV5nAGRMMrrdYrqGhQYM5BjHC5t5xSvegWCD0
TZPGHCseaEG+azuIPfLqoRSeY60ciQ/iDH6QpOwjLO8pyWdOjMfQrBX/bgFm8C/ntRwSXfGdtJui
C9nrA9EkUDjD/+ps1MJSMuZihjZUolynnit9nLWcPPGyYfD4YgjhYRiIICTLaJX9l1nnekkCNTmD
DyDKw7YBXPfSfhhq97oyXNJeNN9c/dY7WFOcWKzHC1lD9MJGb/zL5L1riEx8RlwVN4WmljM0PEXK
vv9nEQzsB0R4YihV3/yDbfOEW0zNgwhUiRm/LykGziH5SfgEphZAWx4i4ksXyb3wp9L1eheot9WU
m7E9Ab+MNb+beZcufUbQJN9KzTQBF4vMRetfeH1jft5a8A541VvJGbU7dmWelsY4Hmt9iQb4IZv4
YcR8vqbPd/QY4hVCunD+L0CMhhz4PRowaXc/LmU/ilicmJm/FEfV6UJyFfnjcWnbq3dSy8XsjwK/
oXm5mYSZyMnunbtSUZ8bCuYWNItPoOWCukUZTSm3Mr+Hh/Rq2IoPZPd+cVq6GnrrBQT5e3+gTqzz
LeIO6AnIzTjKSMfY11Avmd2OU2KUwoIVFv6Vw/Old2hoipxfxPxrJmVqTSgc5meJXL8/H33P1gay
BOyTV76wxJN3Cgzq8tmSCTrEWs+RP3653BSZdr+ICheawfBg3zBywtL/XpyLtPwNSpMeX+VmEcQ0
+KNCR9GSg9VCN93H4/OSln5hYqq44Wu7ppkzpQjOxvyogaqJs8VnhbAHllNpcxdee00rWbUzUXl/
jDfOy16aWJ9qYb1KD2d29laLw97OptYrYcM41Fo+zWCVCBoeFSGnK3CQ2tU5GK8j8Z+jrF3SWZNU
FUqIZzvy+tQ0ZrfA+tso4gJEmuvyhT2dLsUUqDFVDmYCRs/wdClCFbg/JjT4gegn0/ZB6yIl/uTc
vyXbjZrr9bYYKrMaQqmIvhnThLzXigO075C4BlzJIliPgkT04mroB2oDtGkKSnF0D4NTrjE+rbtI
6CMFkSGqDBXAHXATqw5359LydavAaUAda707e4ngCw56YFVm8Gboqrz++oLN/OwgZ/1P2yNpH66p
lEaP7e1Shy02UHoF905R2jXqIbn8cVhK2g83phBvSCSjrMYNTmZb8+jsGgjL5F1JB5HmRKgbNvbx
ha96BaYidCpGawsdXKY78UKbEI7NZTB9TioPXhxuPwO27bvGYlte0o5Y+RNfA0A5Ffq7FR9N2VAk
vtV2+l4FWWmda86h0FR7wMSQ+rYn6lZInXz7lsVQxKAqagg4+cg8Rm7gtkkm038/Yw5h6xcc2Uof
gYP8K7/pCewLxDwH0y3fq/JAY6Ybzw8rYmEkKJnKkWcUcHmpRB8QY81Z5Gor/VpoBCC8vH/wfQ8+
2jju/QWgpoJG74ZXGlp9vigk4bk4JMsShGgW4bEQPTSLO5J+1GJNY9u7+UZWG83VQhRDyr3ZyVzx
5wF0UelpLD5RjVlk78Q0cKVDzwN4h3jHxJI9E7BGboKuEHQBroPKA9tqxk2F7M8Y87vEaCdQ3LXq
btwir3lxgIeOAsJ4YlSEf0ibARPJrfAZVC75WtLBp4JDFLxNnr3K6jPeKtpXAJY02nxfjL71e7sZ
o7uZsBMaoV/QZr9mTlWIaJFpIazBRWWQ6qbSm1J/MAOsjHL86TtFAn21vOpCcSeXJAU0JEs+Lqic
ZVKkdaPP7hdGJA54JiROffHv4n9waUVgRMYRaD+7FxfMv29K1WVWgHBAXy1A0Dus077bm2blgUEL
MJO0fq5zP4Amrf8Qy8gqhZjMnbyWmRXMJRGGOmX0420JaomDHRUu9S3864VLd1y1ik7aQPstKoFj
0EvNAIiogt/jtwrBnfqKmq0tBhh+pz4pVycuatBsLlG/hENfkCb1W4IW7D+4wVRbM7qTv85fvWCv
jjDY1MQ/bJiZrY3hMPn9Z1+dU1WMs1R3YiVi0l2cEyGl8DPTrLNhkddrA+2RKWHVSF1N6Jx6wqPT
nho4JPhH/bdm2/lgheCN9ER2eHPQwQsDd2yLUH/STpH/eU5BGLcSGQDMAYrVuhQwex+Jrn48Et0u
DqXKeXE4lunCHnjhJorQx/BTzMwAzN4hr3Pk9YC0EVOtwI02uOpEYXQQvpmB/n8wI9UWM2zWy3dE
X9rnqonXHlRUTmzrW5fSuPe8Z4fVUVwLM8sZ3cgWN3DoERcHByrHtfr5fdiZTSDF5FgzeqsCF19H
s61k6SRttLI9aN8T5ig3dA8Pb13qpxW1DUw8wEfqHhpQgvA0cGv2wK3maUQOJT5xauwHsovUAlVe
wc8E2QCrVwObEozEXXrpj7ExI22GH3bSi5ig+xFu6L1p/FSS5wlROW8zB9SDixKtMubMjW9krolI
InFBZIg+5YiDUIkI0JLeKa6xe0hIzCfDXCt1USYcbPBRzjGmWAPXOHUtHocAzbs16oPIyE9dg37H
EKE8npf9ZVRxu2L87amSf2C1GsQTyQO5TQ7xBCZGI7J/OLI8s3t5chUDZg8y04dcF3EwQtAGPLQa
nS8zr20cjCQSwanLF7iugPM/OMqo7SYWTriyWMq/vOAIWHhWQvR/S+EZRMZfOcg8CK27G7OQd2SL
P7RSRokzsKB693m+nCQT6g8u5QcyxskHEUwaYy0wril8VOOB7odJNvjiVXTMKRwM2IqvuBG1aMo0
GJKs92Ym2IB6GdtG5e5/vcSsoPf5nLFCIv1zJqZIDbmPQEQzDaBGoJ3YE++9oMaDwhEu1Ig8I75A
LhQ+gCZNIRvQoNoz8oYTIsCIJr2r4//0M2DMYOXkuQjrr7m9/qj1gxjqc1XoPwnB87QZ4seOESUJ
47LhJfRLs1E5e+Tzjpvu+byt7/DmpRz+8iaLvnODZymCE1EGm+Ra/vvAanOAN+FlSDvSw17TNNHa
nALokXxmSqCEZ3yJX2YJwkXipKFxklJaUKqQok007UK5Z4WnKcrpzBZhFD/4w6DNe8aQVhOGtVaB
Yda5t0mL+lkBstzRJe3tab01dQUltTrQvE1AxZ+dXqDLQjh80/EEtbXWKE/Px9JbILrPfY8g045N
Rin7rknOo36kIGHIyUqiEs35fw+XQdUehsHv1hAoQNPdhG2JuwHNA305oxKh3iy30bKZyYBCGByO
5MTIsIGKDGX4meTFVBAk4qaJ9ErMFMG3AiKy/66Wk4u5kTlDCKzHt9r+2teQxUpK1MWFhk/+v8Xi
uroe2AEn5zIo00lc5rWh2Y/61YC8ZfQnaQkMeua/X+n/KA6903tJIHv4Hdf46s0nekb5MURCsyqz
Ujl0p8p37QpBNkf3v4n/hRX6b2JUA6kXFudHJgbTyHPmFJgbN9PtGuDX5pAjbn0Nr5ipgjuJp15J
K/5q4wPXvXUjkPUTnD2LU6AlJdib8eq6BoRjirIxFqZul/FNKly8TtYx7Gd/SFSsy1fcjqovIpzX
YuIF5LFbKYctfzBg5FhG4RDorf9P8hwVp1B300REBlEQ8xOveqUHV0UoerNkUX9ppU1Qw1srN6ji
tOt+94KBkYV/NvNLAnwxvWcsvtCzaqpmeZ4pQDGQioj0EnqpPOT7SpHdqAk06ETCzUTJBKgznOQF
nrZOvdTD2yzn9/wf0zskjgPdlSm0z1rJWn3LLtzFMCt09IMOdDb7If9cNLeKwAqCXm89qbqgp8ZN
VK8zHS2gCh0xX/7f7VILazsCpI6Q/FQgv1ANfM9G3P5Ox2XpjqUntUhBwxwCOTLTLkeBjVfKxxHI
D5Fdk+hKfK705KEbJSUlqQod/ypdHFrLEh0wkVn5qlL1VV5WdyYd81AGQfMqYwHEpD4tnwvYsIRB
FG1CJMemXP+H1UPi4otRg00M1GoOf6+/5thZmCDRHUxp9t/D4AtftE7tx9Z5tmyI2CNMFnuju+F2
pdEFd8f6tNQ9Ej1cckcDurI0uoYAQJ7MLmkTec+XeyZYX7WL/yWM+eSE2jnIAt1K46dGn8Og6uzQ
BZByNQY5EeQN53WPlhSDVkD28LCP26SBgsnTdq6ioJPUoSdj/dBJLAxnTo/66T+lQGkJ7EN0amx6
XHuTWqzpV8pfxUr5BfSdmCvFz5dy1/5rl8JzgKxJT82wCLxDH7Y+qriPma1EDx7dJ1UYl2zS8KlB
A2/3DRHLMweKQrTbNi27YnQ/b5+b0v2+S/1FfoT8Co0PcluBaVduVSZdAnaVbGjnvgRkasE/EmqP
WJyDi/39ZFLNklqv1SzPRzNHIVJkAp1ZmuFDWSMZecxg5X0boKqPYtltP06KSv4ccSL0JVfhH5te
mQaR9bcm+DpFC1VM7nj/kYLb52TeI+NEXc6nRjpH151Kll9yU5N6d0iOtSef1NnziY1hmhyyti1Q
pvTKFwS7AMIvLuj6RG6IAEABOHy1qSCA2feCX9RsuclaOUrgZCOc8thmODsrXjvghnEuZv8XXVDl
Atd+yHhPMlgFbZSeiiemzf6gNM9PP/rdTVUJavtZe2+kjxsGRz4/h7G7iMn+tiOBmx7XRitjQ7cV
BIiUKckyH4C2C/roYzEF5/yNbETxVa5+keciRQ0gkTa7uBKsrrob7tQOcfZVHtIeMZTGFePphXWF
LUzEJBpF7lfg1aBmK/Bl/7tvaRjRTR+tTIFQtaWupCAQLQJxKwgd7aYhdIHwd5zJtpnMnnQeRekm
uHZlR09kaR9AZ7xNrXmPJBFVbq3To8GI4UgrEF/JNmJ2+UJXobLSnCVyfBVowwmyVu7gA0RysVnQ
CifNH3DMmq0hpNjKxpYBHc/9w6Ct+yisQhyX4pGFpb301Haq4g+eE7Ve/tp/GpCzvwE0sccoyM2x
yafu9kXWGllMBC+eRJv+lCteCLHWtlLDuA6QXw3ZMv5KSiLy/VI/PsItSOydlcGDQFx2fkdOIb3c
299QJpWuo1YAY914TTIWXrfBGyZG/AKt5LEkBphf120JN2xPuHXXeZwfpkp+rBliw+G88aRmA1nG
wbk4ZrrYE0gVkd2vFaqt7yqw4ABFgW1e6MrLNuTYdodEQgEETRuSNjfB17/g92PoDNXMiTQEGp4D
bSRxU+PryYSWfSN+MdihoBi+Fjw9ME0RAi/cnsjmMBy97604UsmORv02vF7dQU5AaOro+fkIfj9k
iaEIMDWpR4f0ZRd2ZXv7FYpnVKdjzV23XL9jU/blVdXAp3JKkfoFR09fm9vOb+NqqraBQWjHOzte
AKvAr6BHRDwSmOc9HD7Htdb1CZbURq74MTMaeA+kfdC+aFdrcvl/HBfQbmRCaCzblMSBGSC0fW7b
wbYSveGHXPqZUBi37j6PPxOlChon9crzUbxvVZJ3edvc1khVfHyy1rIPFW+x3Z3K5XSh6emLyoWR
rs/Hwmj36wjzoyEZJHoQ2kOCZChsQsgP8vAG+rccTIGFUmey8JYg9YHa6xUXWxG9qmaHeTvoJ1Dq
AfedcLb5hXnckWKnunWVUwhlPGTEWaC0wy+68OaUU/0vyVfRGfKae1WN4K7wZaD4yQbk1X2QMlDP
fI6mvYUn39Yi2MiwQ4R6jSofcRttL3Svv0pXw8B1AzV9GOkvz1naHoQuvSdGAgwWgkK49xwoNcmi
0cfGCAyCve39brRJemTHdh/wSEAn19q+FRQyOEEnWBswmQEQiSixkgoMKQPs8U8of/hsHWfAGKNr
8MZ/G+yvaO1n/UTMzK1zPfzKQ+Z5bIVL98VtETQjQFFoKrGpWuR5XeMa7tnRAZirJXOXE6+e3EVT
niM0ITflsyRDK0aMjLYkSW7a7DnKM6+wDQs0KPXrgP7OZ1rx0IS/+JdSdRNplL9g/dSBPoFlGygX
2uLNmmcFNS3OY3Nbw0yaQs/nliGQ/UxfvvsKwgroXtwFW67yxTwV7/1GxFL/tqT0Slt0QCiGbVKF
ZNNbEqwNvVAX0Gzi2zvJTIuI0pEc5zoRGn0yY7gKSmD7MA7A5lR01wKuP5TFlqlR+qEbf8pJQRet
VpsfNVmd4NOxzZnghjGMj3wtGPt2ld6glxKyqO/83VUHhCBIIyZnWRUXJR94QdYLNaorVH/twFIa
72XndGYptAPGx6K+32nIbh+0uUbcmMRqorwWxUk9yeyYIRtzUPkIbd8NcByXsgA3VB9aj3IRZcDT
2TPSC31Tcs2LubNwAFCALTZNe+PBKugKHsloX6L1h8NnRX5kXXwA+7Gl9CddIvYhfdQ4+GlTlT3N
v20ZvtKdSvCNY2OxlTxK8KZzMYuCYjqbqQEmJ7sJwY1zZOR+/NfF07Gl7IVwTfB7VBsVoy27hwnm
oKUukitXVpIiUmNa2niUbwaziagessRUbPwZGQKyco3+iNlVz6aMiY2CDlRm8gYzhquEl4FmdNw/
sru56vCptZUR5G0p+QFsRCxgsNcziS0N4YBuDOckwTeNGUV5yyMo6dYgZnpQHV2PwAThFSPjKzuS
dlGUW3rq+KRzlHbsNGqnYxcZ0mwVwMXvj50HVf701OtGjy9eYNWJzgWpkQBvxLs0yJlc8+KfKGKc
j9z24+FsrV8Ut6fNxiuVkctpOTqMsps24+jg1qv2OGCv5eaMaMiDkKJF67MTvLeQLnoA090XxVlX
DUHh1SAth+YPFP8N1hg5DWv70NQ+HTi+g43DBJmMcLCPkJ0trB3n8S4mHBbio8FXDzJVuVBzIbmC
lHEBuzFrWyglmea1/eKi5Oe30dZIUOTl1uC7Yi4ieneb+6+tNWSIiI7t/UPPRbBnCAeec7f9V4vn
zSuy32ivbRRwjrIUGFihOd0Ni0h66rweCqTYrHjyJsB+SJUSnQkZmufgDg83cvK7EZ5Fxh53ZmLP
XZfMb9dSwbpJxsHrVX12TnEXBXQdiU3ItNop7xrgRUKafHQjc5NBxCX+SGcftLWjSQrZeyc1BiRB
8ueldM2/vju0qLLBXuEG94wODr0I6jMfE8Qlr6+Aro6iKgMiD4Z+Z2imHtYZOvnkBT8dDPXq+B9i
RCdMpyEM9vLP0asqIS6bzmzDSMzzMQqTwdv6ht+ZT2F0U2bYjljst4liwBqfhnPoabYIFTrBrk+j
N+USCjSqDZuV9VwqKrXbsVosGULmneMR3xAtixTyGwHxai28iFxrugXom9JsmomCJ7UFEhMiu6lB
BcSKpc+TfckqLTegmeTzCHpFV42BqOQ0zVZeN7IyeL7lAzAGQtBxKZ75JSKJ5ohGnqz9MeJRjTw8
M2ACjVC9GMwMwnlf1GLKluAHvGcuN6fzRBzG5uKyMx2dz4UXO+p80H1Wdzdm6H3uitMEM5o2CbaA
4h8IwDYDrye/HSaUgX1PAwVyIchESwi+SNGAVADsPdiadOixu6arlkqpe41SPwX3iF8Zlr/XVo4J
nI9003R4Z2PsgcsmsQz5i/RIvwHVCZNtQnSk1qjMf26UAKQef3aahxDb6S2Hm/YKUGusrl4gT+OK
Z4pj3pTWooxIsUV7m2w42uCf6zMAenn9vSry4bmXH+4iIVgkgB8hXoH4tO1aSv6C5NNHDKx/5V31
jlEIvSHRy6sNf7VGMnmxhT8xAbksYrWrFX/DfhARTrYS+axBLJJ03M83sfMcCCM65vK3i5C8nYHX
yAIdYjDb1p07kEY2ZrpnbS0EwcQuxwpo7rPu4xp6MY503afSEP7/RhLvRITE6mx5rKfPZDfUpneY
Rj+5Xjy8ge50dmmtd842sY4queJm20Ud89sUCgJJiyw3uPbj6Bf+HR012Bajz0CNd6wE6cY+h/VM
8zFITn2f3/Irl6W5wgw134+WaR1dNKfBunrgBEiCDpUogHQIoksjvCRkgc+XFn1Gf3316t4aWk/v
xH9009ZflqkLFqPm8RQy/huDlH/yTEDjgjLVkMWc3W3WVSIKpJb916zlBTMuTx0fgOX+5qxOVv3I
RmWYZ+chXZorw2C5MkMZ9h7LcPwX9+HY7o/2AuJ77ULrSStehTZpEYYeUMv85RFpXwoIMdz5azn3
5GP4WvXYi8DU6gjOWt4QCYklmjx+pspcUWPZjN5WXEFG4NFZk0H3B/XEW8EBpiu069YA7O9tog8s
Msx3c19ET+M3tfmpX8YIdNbPFlMuLVCU/Y7p9gBx473OSc5xV7NSi1ktP2CiDuF+YYxSv4OVEr/6
8iLqy9fkFjaONvTRhf/7BhW9Iou5gZ0S5L3fV8pHXoiE4xIkopqi1C21otVzv4DDlGLIH7K2ltfJ
0DUcRO/yS++EjjpHVGoKVhjM5QCk9VkJhmRLL2NztqPTT2BJxgE2C0x2vu25h5HJHFNgPY8jYGCG
MXtVhSYBZVPW/N1+Cn3LYMx7yHBGgaxE1TJUeQbj8ZY7zqUpn2gprArrsbNntaDlHL46nkJ0Wzri
surq8bQVn2IQrMMNUvqGiK/zRScjJkFMo/oubWsfhhKGeNG++D4ntBKA56zPHLNgei0bZSZkbo2J
GC6SEQJjn+HKPNexZRsNkTA/QCiBiHNFfmjpJ5YRwaOEHXz1oA67jZyrw3bkNp8PCDwJpd8BDzp8
JXU0szShAB8CDAczJAsCX47dUlaGGc9FJrmC6+a9Luad8Ii+easH/5U83kQSfsNr2n9VUJuUezzL
BNuRCuhEC/BQRBqrDM3CLnLEiuEaR6vjzZ91ArjF98ghES5ozLJ2Mu3sV3UM8GMFEbU8y2yIeS7B
aG1zkckGKVC+nKNtBLpp690qP2IoGDykKyV75zq/nGW0x4wYVrn9jRMvcFPojPFxO4zfqVvlNPhX
/LXm+1VkZ3r0QPTucGQvAo3Y8gqlnutXBGF7R8Fx9ixNPZc4+9vbNaCXo6FPPGJZm8xXB62Irisc
TBQJmv1LIkfckCTKtl7O0BhIUX9ghhYaGPCjdoMUg59pGDXH0Omc50gBM02myA3RJi9z35/q3TOv
a4bn918WIBf9rXBL7ZmKDXdSW+6bLkY+OLHcTMDLjdBrXS22o1+E/NzjUpjt76CO4xIvBNg8BA/c
+rhsn5s4YwLBlNCC7JkzsdfizqIuuv6bs5Z0TzDVYiCuQ8qsDuf7EYREIc0nejsMWARH/IcwJNue
WTGDWWqNlvjtB4RTyylo+CfMVuvxgyikQPDWZDcq3uA48QKPKKZBUKk9nkEKtlBs8NbwxnK3ju6Y
6stGZCYNqXvsGvURTJj6y44WdgMwoegH9GXaPY4TY8gg+Tsa2O+uhMeGsUxl1TgvxCxg9a6Ydf/h
1SoFcfPdqn06YT8fXmYg63+x4gDh4vDcKfV9dLqyyd2Wns4MxoFGQ9Q5AAz3G+seIPikfwgdeqxT
WGdFk04l6xKuA6yTsKzWiOFSd3XRrN4HdKpDHqPh/Y6xQ7oNQIUPt5x4hryb8xF+VoFb+6Ai9KRa
+1No/kXWpSfvSebCZcItf6cqE68KaNEft0GiU3UUX+Bo1K8E9j9Duq29z6ZkZMVA00LD7fVhFwbE
HQ2/7sGU/YjYlVhcRsL6KrIAMXxRzvFeEvaUmzyqucOreEraeKWwwkKr3EAsfRrJzLN1dQpidUQH
cuumSy2jgDb5LfZIdsCTijFbilCELeLRCbXdf2CpAaPl+NJAhK2twH0+veDlqnXf8ufap2WK1Kho
dXoHm10lU228f0MuiEtdA+Hsxgh8YXjMb2yfLiUrMpPQXVz+0eenCXL/hUUt23UI5ArtFLs/JJ7w
VjOUEQJE4GzxuGqZd/bm0kjj1BvcSVBGp2Igw3gkaXznp9tgfPue2ECkFTAVXmBjSOgWOsuCrSt2
Qh8L3XSFSo8tAmKwLUo6uWo1w9pxFRjBWRM4gAmqJS8+u7lL+JXIm6/ovVzgkpNKrwuUITejp4Be
ZQiJJChKV3rsjIK8NjD+b1UsdAI2acvJi4kaXPhI0wzYOijUu9A5ac2umdC2cbSGgfU95F+4x/ec
hMjV48xnmXNV+xgOYFbGjkqcTC2nWTuHnTax5CQL6c0kBqGwWDEwZ9w6BPUgN3L5a7EWOF+2Yjco
r666G1Vi2sU9xsIx8di8Mc1t7TYBuN5qxyvdcRKzYmpzk70wbvBK2E+J3sdyJalHGCCPuioVqwT7
sKvOlZjInCB1i2bfmLIkLtKLdWX0OJjdPaRkb0PXnm9oC04zzihTQoEm7BVV3DWmInI9UIrHz+F8
yVunjILN7rnFZ0zYaUO3OnDU7XD4G4xYGNA3FC8IqtTYm0fbgCwnsxOb16/qCR+8r/OcNMY6fP4L
pu8ZJZOLvSzPfsX5qlSrRfoZzHpgyVqp0T9Iibd72r4sEF0NB2VrlRevXB3KNuIknFNl80bVOSR5
sjIyKtJUHAh8WLGkrQdKCd9eIi1Bsu7+R13Ei6OTz0gx1z3HVDweahx/mE7dlaG2go0KscfHsPnU
qwMJXubxS1JrC+kubx2MwzIXgoWio78rFNbuHqMIN+nfgHxIWrcY0goL1bmcJxvKg4489OQ4GroK
hoq3zh5vpCFajBwrJ9YDMi/NOjaSm8NYDqM5BCm9ptI5Q/zxQL2w2JvV0UOmAh9ltTRYHisPs3xW
+kBGtETeNih0zWmBbaDY+G4fuvpZECmVpifMMRjE/xu5ALSIOWWA2nkhsELfSGbrkojRuJnnaKqc
8rS2jfSCbfvC33XPKw4OOlYmIUW8O2Ub/J/njcK3cmHWMoCuUVOlN/xp8cj/wSUNKwlH0wVoK4UA
kh2fsJR1avYZla7dZKLvD4ifsVvPXEVPehw/DfUjjCCr8F9wwNY6rNUN0j1JJgvIwiXzGAbOR7hv
vHNEwhKK34dDWDRDRiLpymCnAM2BhrVqVIsHeuImbnDk/R7l6KeHketEPk6Yr0t7L35gVwu468T2
JZNqTndHF2QrMqOJrMVyvWPCHzF5xsSrbhTQMqrEYb8dNr3Sb8yZiVFXcaF0GVJIO3SjK0l4RTyl
C3Dlpc0d48nFC8+8LY7WnsDVa3tBB+sQSHWQWdj9Oo1kW0jOLOwJ9lPQouW08fB/T+8nd648XR5b
UHSCFvayFYkFQJ0HkkhirCbdZ/MD/IJGGgAtcW7OyroHRqpAcYAORJFvWnagBjHFuHXNhknfQeC/
MPy8esvufnDf2KTTFX7ZmhFqiFammiYVK6ehCluRAYXaZSeYwkZioWwP/ao9zX4bBlae9/OKVvLr
CF78VfWg5bwMeN1RDtLPLL56Lb4+rErezt64jrB+whn2mTuv9vh8FUajURNEbp/Gp5uqFG2iD/vr
b2Wd+DS054CmWfQTM4QEUmAOowzu5shiwuTCzunyFFZWdYc34vWh4jzwmRa8RrUHaICGTKUY8b+A
Zi5txNMshkPSQsphS093SXoXTV7dJjvvvBYrcdcV1lIVeS1CeQQdfnYWvVUFfQV8kL6pjknBkrg5
ZT2ouBOcc0nJHRI/d9SVtwCfg9CqztCJCSP/ziXTcuum2/QoqI4dYmn3xM0cRhGQgm+4OVLMBYQj
3FdaNn1OJeFrRjm2mXSQAYd6s29ZcSWyjJQUyhQta4Q3Y9heZVp/hbnKBVSwYG1qyUcSAWaoocMq
mwwTgufMrQljlJeicifDCP+BUyk5rPl1lY35UxbToihCCCFaxLAzc0R3oKh1DhDn29MDyCSH+Y0X
PLBL+gxI+RszvzU78kwo4s9acZt/GYYIbs2AF+II0VvE3GhW/hSN0TtBiUWKtCuo+bGrXfjEPP+t
fReyq6aYsgbpkjUWLHTnjE8/JowvyDIglwtj3sF6KjNyWJDS5Fr5TXr3q9EHvVrYHhX22y6cFn80
+UmWhiiDQvMC0HWdrN7oZcp3uYskqSVIaCT+43Ue1gr7oXD0XcUpB77ubeeeg5U1CuD4oKjYdic1
pOpqo1heIMOJJ5dVZSrSjhFW4wQ9kU+DX2sIFpUEuIu5v+OM8CDYLA/Prg8gsqQZ9E2iQ1nBs28j
9TbNUuMk304NmH78CelG3a8YQNcmuo8JLi6sqWOy0DvAhl68qe4pMNdEVpHV0dIiNCM47DFfXj2s
dFTEz7ySW2QkAcV+LgQudcOx1br/8KcG6k59HBItPvDZVet3seUTw6D63ypXWmHSGClsx2WB7b+b
JOpldDIx5Ue22wcT1OGrnQ==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
