// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Mon Nov 15 00:38:08 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ tile_mem_sim_netlist.v
// Design      : tile_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "tile_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.2716 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "tile_mem.mem" *) 
  (* C_INIT_FILE_NAME = "tile_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18240)
`pragma protect data_block
cYEpsuSNkpY0753E8QVfDi7HvAs9udEB9TFkaU+cb+/xgtClJN/Zgc0tle/HQCtij+aiaVXKArjW
VQcpQZiyrBCM+TWVYIC9t3aVEGgKdoil523eTfZUID1OGJ2nmFyzEXUOqGrzlA2nJ+McmETzSWkB
WeyuVnLyiMIbR/dSZTSuMR/4b22iDLF5bAfjGNcW/ufq8jOiU1YK0Y9460vXzvtF6gt3UvI64lQt
r54sMzPtSHISwmu1LQfnyP4C3wBQ2N6Xlpkd/whs42W4IhK+0XZ8QOrwsML9sbrTElUGqDn6YvLs
Z+OCLueLBt+ZOVydZPm/tqRsyONp1t4pUFKCP++Wgd1ob4gEh8QMzmYno0cToUON2UvcKZ4VwMOv
mLAEoTd8LW5qkhdOJQITVNiTHYuaeZLQtzipr+1tFlbcPICZRCTcb2hroPOP7KqTyJ5OJMNrGOwJ
krdIVsPgeDX2XSB3e7CS9E9uKcrM3giMaDTumEB0LsSc1xpYC8yyI4DoyDtCoYvaVpFOvF+z4eR5
i/bsXBOqK40AiMib5yv+QNvKO2C7aief226orYzk9wSdEJJE2ETtJCaHrGStxXU3xmaZXBBp1kd1
zTp8ZHEfDhyOHpATLprmij/UrdxI0ZZKPIwm0ZgW5WdoeOPP8tIK3HXcBchWTlU8am6bPn84dyP1
hWaeEBZt5f6cQ7W7paT5lRXqa9XNdVqqC2KeByVIpozeZacTZXyFFKiLGOCYcAnPLah8UyO0LPNz
LCvYGstJ1IGn4F13YOOVqqnx5jvagcW+pv8UvUZirMHd/m3Mlvi92jAJlAmTjQKyCKWQVC0NRgIv
vrMSsf2AP8phbjk2ji60PNJmrSFiRi0bIFZgRCUovWRiXSyowCjzmz7z8IE9nvWRJvOjt0q+ATzI
puuc1TsQ8eUjU75AwhhwkdHWPQrL8vc+ygg3DynETZSeUbXV+sGhylj7ZFg1PYnBFEmXyBZa3qFS
Cki3Cz/evOnMvfXncOWhEGi12oosvK6CD+vTkKeLPcGykKJiudkntf3IrImGcoLijkjQ4qHapqLx
R/bcxVjShnz/FxcpgD/oXjvVwQodqI/yKSZdajfuycmn8CuKM/Iwi8Xi92BnwTBSOWdLcdr/6vXC
enOc19JFSqvmXIoQu5tKji1c/RkTyoOEDMMmGjNBLRlFccuvINhCJqdZAM7DHDiPRredhj72ce8l
BFtx7YddLhUBnSTs9J4TwWTgz2tpKj66RAPYdvDh0WznPPLhBPJMivkfN77xQIcwhoo+KcDv2c8a
clR768jLn8Il5oWUWfgJEI0+EyCcEvcYX/XfZ0TdqdGzx/I2At0ajKnvYlp+j8WhkatnY+jBw2i2
0aaFE95ym67Wh7TpCWk0zMI22x21/oxfXtmwIAqA9fE1OH1Bs0HN9CfjRhuO+KKstcJ+1nPA7LRR
VytNi9P4GK3kC7YpIBVDAamkNKPlaSYNZJK8mHvHYUPCPsNLz+/VjBjsi/SFw0A2MP0CQULDPvOU
0MaqKDWrZ/wiq4NoVi1UcDhbe7qgLpNggHL3hHqgv5EDK++AJt7kiDwIKsZdz17+1M7zcb7WmOUU
8g8rmhObCFJKYLHS+jL/NdYy4U4m0AbzEigPIVvSDzWZC4HrFQOZMsjnfEUexSKRw66o0LXudd0z
bk37sH650B9DZYQl8nltBgWMPQGWzkQ9IfF0/Pdmx7spLeWavuVdAw2ZQq9ZeXLeuXm70qXsSfXW
nVynzRyZRNuYJifWEqdi42JUQG5dMdhQkpllzzrEjLIKA5hI3FO4fuRP/v3Fd6qR0FPb3ndON+7t
iRHXaoLEolqSL9evt/zSWwCNQRVQuhlQXjiPduuRnW+JvL1Sfod9eNSMShf/8gV/LECAVfrKzzYR
7uQB3afc8m0hZDLkC4KrE0IrBuSjY4wSqvSkZHxj6XWJf1kXdd6CSmFADrantcrGgPUwUnugQaTi
RxWVBu2mpTGO5IMy1VhyACgiydueO8L+ter6GNnxKY93NMHu83gduXe/4xiSdfaVK6NIzAcPS4+8
rm5HgcIKGwc2fxbdIVEWd7G/qMlQIaFoR+Se1WHksCmlaU3jviXmuthwwr215AECLODELc12uNp0
nnunYbubumadRrIapzA5qR4x97MYIYGoy+XhB3FQdlubYdVR42bBND2hlucKBsaMIIY3LrGeGbE+
TYph7hokFzCpsI/UeWoPrlzd+ykroeSEOLQv5hv4qFxkj4lARIT3lXxnK1pXzZsXcU4c5GhnjmqO
ooDGJ9313p+jYGSyEF/0ld/CfYb5DvcOPA/dvXqwZm26bK3ZHsdcs0OCtqyXxT7IhdM/yeccj6Qs
2lTNybsgY+MGs8CRA2Zp+n7pYHl2uGGOTN80VnOx4dCAqZJOxKW8Ib0P4NWYz9SeHKwqNVM2Tv3n
vxnozpi9tvHTCMDA+kmAm3Zmk5gxeOg5rFoKNjMURBtzpR/Ayp59Nvl0uJtBFDHLe8EuXihnuIcL
rnRc+M46hIM2d/x/0jj6rB0c0ZD331wsEujbQeuqaVW6Z5u/NGqzWV+f/QPoAnS8oU2duHL70lIC
Z2ZFj0W9fM4y7FargjJqjjzeMAO6jD3XxYxDP9gtCbmOZpVvm84J/IOXZemYn57cCgyLo4PJJuJg
7hoTL6GtmV5Zn/HYXbzRDlPqL42uCq6WNI8j/Cfr7oB9sAcHcIIz7xhfi6DOtAfxcynwNe14W7Th
iUce4IOdtWaiE/rgEWE7VC3qjQSQc95VYqF/5Ch2QB4fZvEEON1EC+8gp3qm97T2PBzr88lGuRCV
47dLg5GoL3h7BRLjrhShFvhewn91V5ApiEDimK2M0Rm57kXEvzYouT0ekMITY7ojJfqVMaRIhAw7
dg2UHSF/0s4Y+sOj7W4XNEWZjLjlvF9KUYgPWbps0g2yD/mq8DKoXKwAWowpNu7+Ibza0ii6tb4b
IrN2tvh7PCCESpC3lkNGsaC+KBQN/URJq+dgB2QE2cXjDlaAxzF5t7Kyjw6a8dc/7mmrImPrFnK8
wvlT7fi5mgOjE9i0qU/jG4VLWL09vJ2OW8R7NbA8nfJDEr6A2yJrKXSGecF7EjXhx5j9cw//1uJD
f0qdq4o9+a2A+/NdKhTIU766GQ+QwJATZI1dV6PXQ4aRVWo7BsMzOVcmwQe/dVRAqK9DzMV+la/K
lBg6mQt5CIDXkGl2smmemaR4wBOURWbvnspNQxfBKlU1Um08gFn0tLdST7GNZn/HID3bqLdVEqGK
7X3PlxALE7LtLADg44QwHfMCHuTvS+XSlPD+DoI1ndRQi8mGWF4yYTFvvgCkh1hbPFzz6bklJyyw
oHb3TO5mVn9q1I4OZl+HvsWFs4Udx0B9KWOGfWDC7UTwL3fkiyNKFIV2UPKFqD00fZOhjHDbNV0i
k4kiu1QnrhFrCyig+CnrKndT6G7cCBo2O4hX3YMRHjg4kFyYWqUxHjj19k/NVYoQc3OBcrB7hJML
DShlqJoLWqiRlcJ2CqhQs1gYjWK8EtaAoKOGY8WD8qszMI94Vp9rDs/Zf5LdID3LlDyyjr7d1zr5
uRKYhBvtMn7nF0acLUx7KfGNzaWAosHSE7aFxu+Qg/1VsuRL5smZ6hCMaDoaM0V60a7amWop0qeM
EUyNBNZ94ok+w93fKnTHQUSJ2XBG68acrk73j8ZoHMOSx7ZI+mxuC/qfInDdfnDr7oPHatNH4N22
sHL7rMd4wSjciYfd7tcoYy3Ng+MGheXJQ3E233VRLpMg28OEsPY+GLA5yF/9Syntafk81EkSlfVk
aGVaPqbJ7F04wmdYDMMG//C64UuCOh9YT/adsm2uFP7rDi4Abz30FiM5YucdKNp9Vh3EKwUKj75j
9Rjy7vrKCnCGv5RWtXXLLqQ3eNdIRi+Uk2xROi8y8bOPPH5PxNGFt0AxJ/tmw6hae3PO3YXBpcxB
SqQi/tC0Y7+8TGdKSFW/UHwtBq4lSfn79IQwTLjajCEPHr6Lq3WE2CPZqtvcXf2iUwePnu8FRwjy
XGTYpxDiuouTRsDDTL9gn46Di3EU8z1hROX3W6RH8h8OTzoG7JnfAG8cMjy4Ra7FxZ6baoIyvuaY
k9orw46yCnLAMK+HqXS0AbBTQCCDNKG3nO5OPE19vAoYqxBvkx1aKQIjyZlTbfEEaWYsran/bP6E
C6IdSLAF0SJhNfpSGUXAt/hhJXVcyHWfzAU4l5ogwX9hFXWLN6UJrwlYoBgNrx4QnSNhR2zqCfaG
ctZyWRoYqdsBQjEPG6R2DLg194k4T7jT21SVjKLYEMI7vq8WWN2vCo719rv1a/xLKDMhiZqzslxx
7mN9IzTMsb7X6cM6aCd61wuGzm6Bu47lEPbhrgE7PtJHoEjb+BkHKVjIiWptXYPGMqQsKQNnutOD
Q4KfYWGu5h4f/NV7Jbq3+StM+XkLUr9WXqiMNb2Gc9i/FK6Z06apMYZfXxK/5fsGlbpvjX5OQhPy
QWS46QG0LB26v1+z0hbHDzM3hNwkeA/M3wfYUltQAbFeeUGMKlc7/snFU2xfoC2BuXV76Wk/9HhE
ctBX/IkM30rayhgrywsNfzieXTU1hgZv+/WA6+1wv9BpqeGWiRM75ONy56uOhnfzsV5HK/6cwLFh
BsL0XzXarZKuLviuFBBrJdWMRKQxOoJ0cRr/SIRnUG+pbwY9qcFG40mgNR8RyWLOG/0X+DiuPLyn
e33u4TlBCdriPrteFwPdtgMBrj/Pyym8wwykiwyXNiCpcAwpmWR1bGS7lQfnYuIfhEj8seaoOmlk
zkrlTzBUIDo1OCfTJAAfUWQ9T+tujUSNSoCRq763F0S30nEpm2NMw3XREUpHGDxuiSYgJtmA20PE
aTtY0uNS6szA3VGMnLzQyVPj9XWSk3wnRDGmAauohbiy0ddBElifFoU4/2pACd3wIhSho7Y06eiY
e4MSaOOVjX8FRcob43vkRH/mB60s66DRJxviL4Rn3YDuny9d9yB/QkaHzWwSH220NcK4B0CZ12LX
WLg0SCTS7+geklefgIj1B4hXl3TPQ8rCkzwo/ffjDYQ9nM98gyOv9NdnUUk4p9DTtt1eVh4BrByz
99daz7v6dWb1tz9zFnFEY/3HBUTITWf/b4LUjbA6TmClNf/SqmlJaRVqo+L2VPGYfpPxUDhLcSXL
skfVOQZZ1XUWahr6MtUGjY5VbN/uoY7yzAB7DAiEqNMx3KS3rWbewT6s4EPt8Q9zZ8dq4SKgoif+
/TSTBtu+dhM28BB0qzQbWH00VTFtPLnYSW5lcgX3iOI1S1vGmvYVfbcu45P9EjygxjYXiXzuDMhU
PHXyt6u+8PZpp3Rg7DPYXkNdlrbwtRIK9ZTQbD3gOcWqAEkPAZOdms1MsSBhTxkZwygt0DeWe9rE
kAI41EDRy7cv2M8zAJDElCHwjkwwzsQxczawukrU/URMyiiJLfSYaZz79N8TWQgKqQMinXgdf+RU
sQrufS6szSrXmGSMFZwx6h+vj0e683C/jjmHW5Kc478zQTkxHERdhfYEQ8/wSdQwGDnDF6uEV7i6
kad+tnTp3icQtYikcY5LF2ws2iFEifywy0WhTOgXxov0RpMU6e/Fnu4Zh840svvduiLcTx9/R9ae
6QdLqsrYmQid4uZ+L+QCW5Djzbjnq8GOsoHELCZMGx7dZ6Rv57XVqCSPcJzYktKUqJPoX+9YCmpL
TwHbcWA/m615Pfq9NUtihqwpLlJD/N6pFcMG2494xvvkpC1gk8gaPIuhjyQssFHHDo2ZAusahfSV
mkAknfUTixBqJa6KnSsHuZhR2gho+nwH2GNA9IKD9rvoB40baGRd7lpsEfgR3bRwhfeM3fmZRso9
i4INUhQloz6O63DeJXk0W5pcoz5NBji9+foe2S3vdX74SArK0X8n7lEF8G69xBa8UFIng06vhWSE
ARkFgOHbx7I3e/f3HxfTZpH+5GcMbeSOmUxzjrOGSNncUhy/y0Mr2n8cwdOkStXrsv8SWYPw68HJ
q98tFkouQTtA83izU53MWgAiMHBTHGJHMglI+lSzr/jZdLqD7RUMru0tGOnO7s1POsHuzgiCG4Jw
fQaCMK6/AMcmRqIALNGyJ4OZDUtVvlI5+kL0NYYg9slUToMq5SzqchKoHmZv9I053Q3Nxo5BaXN5
RthoEX+RNIUjHD9Pah7UdAUpEopCIjiKiRWNjWq0f7X3OMBjcA65yS3vzg1sfkl/GOjPYHR9Yq4S
UyZ+CBo7lE9/fG3YSR/Jv5X9GIppA1kJr1DcAQ7b5kZIPxAkuIwFnywZLouVtZNGrLN3eQdrISAt
fYhfrA3QrDfXQzybfzQ1BcnEkr8qkzacYZ6claDuvRVrBf6NbHcj7d8BoI/TfarxtuEnBlud9X9R
9P7rdACKNjA7YzqG5mjvvcPvnZPNSVRrz5lqDhv7VXvFwWdDqshCkITEk/7ufDlCxAnz5CODmYAo
41SmTXNm6dK0g98zqIFngwt7e1zAxmx+vTMdh5LPjF+LT74vAvfFovjRlOMrUcNZF1u5r9zWFyyv
Af4e4MhPya22SVynqtFQsAl5zaXFn8ZfUqdTruXNoIQQCFkzrvokFpNehV+aTk4fzfToPMrynw97
8a8VoHRdoZIHE8QmgU0JjxKCMjGeRRWxSN5Y4ouv2/h984vLW2CFT28uxbSPFlq7iIcFQpGCRS1v
19tWwSWdmEMjmOFNz1B6imglvmzPqZqNytTMFF431/TqbKGPHofM3BPmqjuIL9uZh5oNP9YWtsp9
H3fRpDLXxt6BqDjB7KHc46dlzGU3htUHOI2uZFQruoG2zbEyNLohfp9IiuVpxKmNVwW+RmPpi6vB
w4tjTw7Dp1ekJvPYmTsHW9YIZhIuZT8hlsyjyQz69v9Ajjy1Kd2jfE2oKcw6pOciowzWAvoKG/J7
Iq5VMh4F2+wAHWNIyB/HltPVZ94rnOZPrtEjuiATCTqPWvwWJpbjdB3UdfcDApWi2TN6mYeXuPm/
AWl/SdKE2s1pdfJ+40WNd75w0j9OvyF68vFtozZwFd9lYg95RjgT+2xyNTwOsVoT+RUf1TKSrOEe
mKSdpIiz8YXTndx+dC1P3+PS0TOuCXGhDnpc16ncqhVxFHIjfoZS0sKPPwx44sx9m0FFmkzsIxIC
faKnbrACCtpl++xYzoB0R0PebLNloW4EK/W+j9qLQ9oFHiclIu7O4053qOjNiEJcINp/mA9dYLO2
LNFGTKCxDbb88GIVNqRa48HJSkSFtsxs/UYWfzy3PYWLZc2Z4cNjX+mVRXH95avN4YTxGniMqWGG
+9fC6jy0oYHEUD6voF95scftjUMFtoA8YXJT1XTtwqbdKMtU2/b/H+ULzbzTbQQQTKxTep2gIAnv
FMpMChUr1JrQGsLbkGS63J5HsVcxs4H+NrWQLvKY7dG91CJNPkqJjzQnqVJzWLU/j43NfmDDMyyy
8ZYCpGmFKtmXJrBX6uQoSQ3RGu/jaxBaqEK97h3JQeXXJqMkscaL5KGuCmXgEKjx9C7FLboeydS0
wU/DidVIIW/Img/hJ0u2Mn9zZLnhc3pC8KDBAS1lKysbFiHB0qv9dZDYTyK/4NnqJaimAwKM3Rv8
is18BreL5OcF4uA0lnXTfV1e+jg/ixHOONEEjMecLZwGCSXYhV5NG6wIcEXFAnGTVKt1Du/uxK1f
d9KoDKDZE6XLYuSfCdLehBnVD5GjrhV6Lx0Re/j/TXvXXcXDcfxJjyVFEE2L0LRvsVLeoApcno9U
alCzf3ucc2rkqgC6nJK17nxem8dAvCtekIO3GtlUTPd0/opJvqHpeCR/2LiuznVhq4v+1Sg4Z3wA
GaP5lKxog77mVZsZYqUSv3Rn4jjyGeNJcHO95kPTj1uz8JQZVsYwZnjUPaYzuURZq3G8qkkDvGyt
pYGq/fZDmVOFBs3cqhffL6qw2tRcTfuCg/IuMqbEgO4vF6CbE8GVD8nPKTRF7gD6nRaeny9T/JGn
TbmldAxajnZqoSU35j/MzUFTDvUX2NYhrKU2CN5Q/kXmYpn0O+59Dl31QCwXXXmetc9oBieifDNQ
4siEXNtKpCCZ/Y9OmZfEbzuic5D2uLNwZhDXZUBrvOjZiOhV/hx2iKC3XSfpcEe777NyINbc5+wT
ZLldxjsnrnseDHT7fAl8YzR+IwCtjDzUo0NlpPzFk5ZeM7rI7Dxkmtk7+uhEqj9OOl3MqcfTEwgr
9JPcrySMPhi6ZykdS2GkKDyXmk0t7De7CEsnHIP9STyLxv9sIdbO0M4iaXw1K2itrj5flpyL6FlH
GxGfv5xtUJ6B098yeZeMNmJKtY7aH1y3bS0K3ZgoDQv5ruG8qmja4Lct2jszPuy7QzHbrx9wAdBg
Rwe+HGKGre6OVUB+vI5RI4MAY+NFfE7R/lS6A4+QsgEIxgmg10UMgXnOX7fNZux9YBzA1GvKPbIh
/lhaZpce3v3EEAuBDQC8PQyDcdb6ypVgEYmCw3Di7d4yousTSwx0SRAGJ2M9DN5XChMl/7uRMJCz
QLA8sWQzjKWejtO3GwlOi6ZerwOOaxyFGzhtwELBRb2qtFNSlMBrRiOPRr19HkVX/ko6+fZMS/qj
shZvsqjU1rUvPs5RnJD9sZabXutfO4rEgDikjzLtEDcjmPCwhag7sx8ev2X+gkq9aF3aiyWCACGq
yrlse06RXMxyMjz7ztO1cQRpGEvIwaB3EcqUGo9NcdXadg5p2MiZ0UxvQdBnv6EpDjIjZWQE5WDb
t5O6LNksL9eyE6HJJtPAOMXNu70ACcAYBsCtmeJeVVoaY+w60+F9irMc2ntR7BfPLllaLIKU1S4c
eGj11tDu3ZhyxWIcS5Ih6izh6M7HYoiDe2FDsR48KTB8EqrLex4OLcXq875wu+9r7U0CtnBT1gpC
JECZNT0WPj6hCJiLj2+PQk89IiynbvLjVwSsCt55OMXHfpAaSkfhp3VmaJr/QPsYrd7+UROptNfw
3OS0bAflHuAlLnZFWTxQfm2++cIpe55KcgT3ElX8kRQk8b0ci6RlFDkKsK9ZvYctzY0B9QucbK3P
xwYEgOotDtzWIm06Q7lYQWzkAJ6ZtnHLlvmh9OzqHjtXjaju/lLeKRl8E1bXt97AjOFg0HSFxd5u
2rFFpUsGLlwbRNsjUmCgHuFOQJkyzZkcA2mZ2W0J8JyigewSxxt6F0SPysZvpyy0EQm7wbPgNpsm
Mw9medCzY1j0cnEX6u/R3xtDcjfMzRAIgSET3aIEaibGL3lStIMWG2m+jH8clmCuSNNToLy2UbCo
EB9H3pyX1LEWyBJNERHF8oxh1hCQfGcRi40PushuB59UvyluqVTV/FO2NUfAFSxIR61KYwV/pr5e
yxAe7pQ8FQmFQ+OnObU8w9FamS48VRuoGyyYh/AhxD6+TU2l/8z15/ZJvPjivHOytPt+A+ufXego
v6vtubC1FknGcYRyqaPh0Jps6aqg7GS4kPwCrwa0RgsCza+i7eMYUejNWQhp5KHV2Yy9i/WoT2vz
zGU5FJj1Q4ywnsBSI9jyTWDvbhihvgDBn4oW+pmhXpCX3cfWwHsHQG7Z12Ksr4x9YWqSnaksz9GZ
fYrcCv3og3177Ld0kr/xj9hQLTKufxGCosiKpliNU3vsWBz0p10oRO2SC6bfRIP4SEF7NGhRe8pA
OAvrJ6fvnTIfD+T7YGtzWHhQ6T6nl/MQwR11VTMn/7jJnFiYENQ/04QDIuu1ywaBimr3HM7HFyf1
rK1n6wSwbU+0z54tuXlZy/JcR7ibKzEjorWGYfl+kAfFoKKwDOkFGWe4YpmSigV71vck7QGRppSG
N9BobMaAX+TGhm3mEkyhQmdPxXQrqsFGFE4dLZxUZsrI7X9efFMRoq2Tm9jhXJaFSTOwa+uVOeFZ
Y73wDiFoGGqb3yeLYnTjQmqU0G15Xq064HIVU1KtkeMlwwQiFUFXEeQVyXzcNXpBi+WTaw4ihwlp
i4s+Fa+RQlHKQLZV8HGFJcQ2s9Bio78cuICQxVjCs1ISuhIGhRem1s3dv+eBwQ1QQYjg6HbukXCo
umWqEzIzy3o0kNXghP5pSkQTmcIJoJuUAjOtDbZTJ5qtRwoHdHxJBxR9VrMpGQJqVn9WlnzXY1th
uFGKk903CcO+hgyKGiZWFAbcDRhCEFjUIpoKR0UFBUqdCQbc0goWRM9h47y6pKON07wNLzEaKu/F
iazV5/apc3/GKUJhpv1satd6l8/0vSlscQ8RBbr12qTmIjyppD0VVretAE1CDtc38/cCEVADSMZH
DlzDZEmnlVL1glZZEM4ZCqNArIwCGVD/mDxp6wNDyhV5x3xKJatlCNGpFooTTz1pzZKzhnZfxu9D
ByFJZ34z0wlHbftQWbYAyvdxXqr89whlWbgvA68TgubZh7VxDjVDTSVE7paAZrlEDjm2TKT6RckJ
g/L6gDpo1o7LzppO/KeXkutJEkpbczw0cAjekx1lKguTIT8D4dTt+g59h+2zQdlojKsEQDA3F/Sj
8i0gggXG0yw9C607+YZuJWVVgaX0JilWaes24+5e7RlK+R2z7sSscF0yCsBZoYuNjngM4OQhsWd7
klwKsQHtS4bsmA3fvVHhpfqQmpFY+Jfx/DujBJKsnhKZlIn/EHcYbcQ3p+ofp0i3DNJ34j3otLV/
9+cz0vvh7S17qgVU+YzgHxsYoIzOg0IuWqc2Scub8Sp0ygNoSwiRp/sLusLvqdneifWlYhD4A75r
iiFT4uxPJfpGZz7C9HGXjfT+itcSn289LhsMJTOQfqg6StzJkMzrrwaV4N9rJVfGAwXTfpq7l+kj
XRZZqFB4rBeW3pX3GmcHZSamI6n1CmTNrduABHmpDw2CcrATfGeEK5ILFKRenLlQDDjasrwCWrR2
z2Htep59Xc3n11gt8TcG5//YCjXv8+8BvhTNQ1TiMbKscKUvc6osRyVOIFJVUHydQKpyrXQUNBy5
rOeu1PK28ZWWLh8Vc+gXMRGQgIGgXAOI/IlId8fVyLhFoDX86jtuO0RhGxP5SMYX8OcaZtU/eDY9
6CBK+eJi5fIW2WzVC97wiB+0FRNSmfa8txnY5YS+lKsOvMD23nLqqFKOvUHIoYApgy0qjEKSGC+3
PBH8HaJlK405alcsQ7I8/h/8Z//FtcWrmjmk+KA9zxS2AZMAQkHBERoyVANEzEfD7zc1EeoTUKxb
XV45VdrclA7UuVXftIXcIwt4xuTl0sLxbXjmwCVy1MIvOCTF2KndqYOg3vD3r6tu7M2hiP/3v7Zh
Zg+GE4ILgIEwHXtmOSjtxBtdb1l4ppezvAf4aoaSsLfw8MpBkLi2meZMxZLaBIKXXFd0+dPaG0kK
bAcBQhxN3aQD4b58SwS0chkGxewVHWKdyWmtL79ms6PbWK6zbmKFPTEkzc6G6dq8pO6zg6CeiXHS
pYdQ8C/Pma6oJbRrAjAcosL9PbHovkLWmEHXHZrTX6cvyBEJkurhe3Qj8UKOCO7U8lqArse/2efx
iUX2MJYKRdXGwxEIN4YfOkTYU1ZsG5zqj5lwvKNpdoJTJFINdM7yRq6g80D0w+Hd74DrobNaHCPW
KgZTsSRsmSW9OxZSR0Bb25wZtm6nJ+EdFlYf1PVZnzoVHNnGhoCjvnS0jOj6eY6FBnKhqrgD6peM
qJ+qRjOK5I1c4ylAX6xxeRkkxG4Mq/rC4Tg+qXCfwkpDPf8FQvPnNhltrl+PNDrsPakyTiQgpsIM
mtKJWoQdHiUArZvCZwLlOnQhxQH7iKPVNXf2ZJ+amY7YEof66j562VqEh6YgC9D7PqmEfmSceMO6
ZIW5rLlo5fxw0sQuMdhkYmBvCHBEA6fxqSi5pYIkU2GWwFjNTzSXZ6dX6DwiSpRXm7aMEE9x2Ofh
vsCWOZ/Sd6pnUyweam9tNe7edTSEiOTPGoOEseoOQU2EqI1rLxclFVAA8MiMrbwHpBAcaCIAl2RJ
IKMGqraC8VlGHFRKG3O+cj6BT7Tf39oSf/n8XVNlOXK/dotVEqdKNFWV39YSv3tPiNryUtG7E52s
r63FbPz6Lx5kyTxmWP1eWmgEU7DpkottdMmujo7DZUHdjXyvaAmgwG6balMGfAWP0Wi/Ut0ycoD/
PuCrZqQuGPrpxlrL5KznPdlVPp/Ta+CFsWsOZNPj1aSR8SqApu5rc8akqCpX83iHojKq3a1yC+/R
0CSyqkndSMvhmHbwZKj2a8+GDHqkFe94CcrmaMFNpnTh1MNpOHenTbB7OGZgKapYy9ldCmZNdGIg
jYiBHigphRG89e+fv7C63PKnhuV40xuXZayqpui1/zDhwiUw0QJ7KTXt2lvc1/Gi8Vqmd4hHRepb
et6hiPxcYwWOAdAdo3+cRJcl2k+5dUz8804VOdjwUhNgPUi6ySUd7T//ibvEjaYDvv7AHsK/P+18
kIACRzlPc+6Ps8SWzwOExJcHVRsYISkCRQ2rPoFK8Us+lEI8YmEsf7dORvdC+AiY8zrDEWxpl19e
RA2PWCiMJteCcHDzGKCq1Pr/heJUVmt7oJnE888k8DvLC3xadmB5n2hb5+dEq2lETSUodlLg9IfY
u1lDdAapYxd24fZKUMg+xYVwhXQqki9dJ/S1ibyazhMjjTQC/JAvYjd0EzyXb7WIsZfhFjpqeIGM
0KG7Ep3kDgI4Q9UYdTcb8Z8//f1WIZJ5xJkMfmd44VrJ0mLBswDjzN4N1gsfVQ9YypXzQQBZ2D7W
/U5ZJAMnUEzTGfjbhwCOt6QuOgalcdxwbGMDzzVtAWtc3g5gA+vmNpVsnEpYHrmF3Mp6NAlOnguV
BGe8s4fOz4p+UNM6S7Xgh9ZuGxswuYs2iX8LV3zHqtH5HTMBvxQaGOMXepXTaIsmKvtERPXe0arI
ZvXVEwEwMP0FUyRkhJkjuYRIfhmfwt4CYSo3EWKSAcAKTw1QXm4wu3K7OvLCbnwDTStYseTODY5j
avel3T/GH9qdGIgqbQSi31OzKQkaxp3dN0NTlLiDiZ7cy6S3LAS8JBYylv1b/ibLEjPJ9lmSKVp4
yJp6rgQyZ2mvMkvzQ7MmWybFemQ9juvBFnnxdVwp98Ztp3mn5fBf0hMQsI+W8rTdrvIg8pbqUMXM
TbFv1yPD3nIGYSMW+6lFczp3J4+kFlujBRO2Tq9hm+g/3hKkZiJBW0liyiLWkdhgMRv3FyKtWzkR
51MtcIAJmaCbyuw5rf7Kaacwwi+wYXMLAsgb3bc/QpF2WWLUBysOVULpYKqHYL4yAKU9VmLGDgY6
TpXwifdI7g5ip5YQVwzPXwWwOlNwkoZIP13OBpcZvVFa9oUQqnxcssvoeRR2n+nwB+U9nuPCypyi
iB0VogItX2T8PtIDtPMZagSxXW4TW5v45f2+mgc0eA/woeg3Fhdo0CcJdJv84eRSzwYR1DxwmOZ9
jwc3LaGPX3ju5z3+q+3Zp0gaEq4ui9RYhBmcI9RBoCFaY49gJ/bRfEkrCy0UVYGuVcMOK4zvfCQt
SjOTf1ZB/85eFV6U36uIsGk2IPtOn3KRJzkT2Hh6J4iJAzJxFf2BqHiJgldL+ayFp5hIqHT1dwfD
e1M1KM2Hf5BisYOfY4tDnqQ8r+oBC0OfzNskj6D34uATgCqQOh10PS5/WRhCVZy86WSfAYu936zD
9vuybhbzZZe1jAiE/2sgNAbq0hrFoFwEUblHOZyyU9V+dwySax1R3t5jU41nw4ujhwdjYftXNlhF
n0w10F02RfFyWxQY6rBPi7UEUJmvzpT1mmHijMwNsjxz0yhsZPCjk49Hnn93lLECKJSLh81EbVyn
+jMnoFF+ZorIW2YR7Cb1KkDz1zXA0t8qmrTH2CIgZ5zzCwMF+87liAzOWJlTrHutnx80mz1L3ZS/
5dOckVg+eZcmpJXi9MBt/4FESrCK0AhJ9Inu4Tss3BnIO8qBSw55EXkRUT96BB2yF1C3VQyc66Os
V8KO1/iCAwLgRf2xHdIxgfy7ZhD2kcuoN2OfjsZQqo+CNKQcLnAnioilrNB/BefP5YhU46hit/fl
MxnW9io+KMWfmGB0d4gVzBdta3uzGC3X2vXPLBDArCT1QK6ouA+fZEP7aChUGZTr8yOHs4TPQtP4
gwnXmeQz+eFcAI+cKn4BpwNNmav7i+sYOl/LH5iWN7JOD17lzcqF1GHy0A4UAapukjtWTnTh9Ezk
wL8HZR4LzBJKkAQw6FZNKfcSk+Sj3UV2wbUBrQ8ztWAA2bS/B9h4KAAS9xyQ1woT6vKfPp16zMwc
VqHSbFeodLNN3myJCBtEBNHRykJz+1gBYkHbwZw0UwSEo+wxcsVj1C6xrPsgG8B6+HbfAbFWzovc
6PWLjExnBUT3t5xdYRGSjzI1ftJG9fZqWcRvwFADowGcBcY4CsdTzfkcY+rCrb0JegsHWFUORHTJ
8Vpr1aV7FDKTRtcHqTOpYrn+96B/xv6pgCRCmWwoqbDDfrPCP1ORS7YtuvXeL4lHP++V9ZEU6FFF
eSqIERsgIaZ3dqmGbqIqpaiuT26bExZ9em5boMsPFd1jpINDbMGprBgDOGzwoauEhC1Nad4madWZ
YC6trZSwH6zJIfuk5VzTfBvToE+o8uzsAxNBMFXfwQi4lft8atJxDNstNbveprQrUTIelKcUtDFH
9IKES6Y0urNVXJ+RqtgQNU2QmHiB8I+hJ0hyZrlv8Y2R5BaSN5zQZW1LsvW0CtLWf6t8DOrJYqKL
Odv4ymLNe/E7N1Xa6yIfbmJ8KC51flT6Pry8Lf1MmxSyT2T3i1wI0W32AaGu08YlHpg+LjYtz3LD
bOHH1oWTfAFaQUj3M0OXXd95YNr9e24jt30niSZgoCy+LpAs5oWMkD6vBFokyRLEiOzfdG246+21
0dIv3upoSAZIJ22JetrwzwVKHkgQwgdnufrS16Y+J9hZlCx0UEyw/m3PYvRr+4KaCyQdnrERAQm3
EKaJYPqlzpY60o3DTbl1PfLrW5TlvJgKLehFpBp3+nKytNBDdFcv2IwbqrvYtZQ8DxoV15XNSGrz
zr41M9qF4QKNkQXmtBZlbThMUE8tBq1mtxXGWHgQpQpMaqbIeybqsJk6jKGA4MaBG6xIUwvDPN+3
PfhoaUYeBEnM7AOLxF/aLFRiye635M+hcFOwYXEL3D9ztz/laXfvyLdZKGsyAVBJabi6JuEcxofr
YozFRHC1YW+13Q+aC7lsQTrGwZxhsF7t6hdYiwmEHa7O2VReVKCkBPL8TXCR2okP2p+wdDzTU9VD
6SIkcX/hH26IRYTGLSrxVfwrS80SMH6ifSG/B14e3ox+Nl4kEJ+3f5/KEybFslFhlNwnJEK9JRXN
eqpH319Zwv1NqNFRJo2+CdaCsitE1OdLFmQhknbM82/GgILdVdGnp7yNg8GvudHX52MsD+FAbJ5n
B5FueXByI9RNyURABhRNq0Qk8055BEM7E243M+5L/ANV0/88vaDQTfbEwrWGR243v9AZSLmvdmZF
DymYNtE7Qj3WjFvU5jaQlX+8DRJO0Zqk7HNmJ3MvjyZHE75bM0lwMAjDNUhbQu3fAs10djfk10+g
+xlNLWnJvlek7RUPGcXI+FsuyWwnDjVuC2z09lrwxnUoNBLfAQ4ar2URyYBMDPLgyU72rT6Ke7zs
jbz2ZdCZ4pd9i5L1oy67jSRTJaSObbvSijRznASspnxW35yz32wsKpNGtM/U/QKCAS1QHJbwkIWf
CFOQpcpSpTmOO6IBo2jgwwX+t6tuQwWuQIudxa4HmlINfjQALG2eVDbLfqdsgJixRnFN9PuGusiu
GjtzmHW3N6xZ4RreHfF2KRgtYDl9yJODYkkNAc7aU4ApXDfhb6eZQ73dBIKvRz0Cd/jOVGBvZKbV
lwnuJF7WgRGVTteaPFaZXGPlgzJpmqFj6T4txRr3I/ksmhJdtDmubayc6QaiDaNd4kNRxCGuKljN
a8yUU9/zjed9N3ZSgg6Mmx/5boM+x03Ami/NOH6PmSNJMuiLzWkDt0tiFr0lRmlzOYerQNURfzfm
bbL/QeCc9+FkF8ch5dB6wL0L7QAowWVXi50Mw7Zu7EVxJ41xwkGO4WJtQevakqUYySKOcwOYLyH7
X/KPc1WFRtiGzTHRAJNXK25k9smLBjMoMznFaE6FGTMOEkioIjASybqai04ALCV6evQX8hxvoNGO
YWVOyGt0zrNwaSkSVcOT9+cnMjd9wjFzzxhbpoIxZ9T1GQC1AmnubkpUzL691taxoAUu/ECUsYcb
Quxw+Mmgd+9lDd3t7NRoPwN5Tl5wZSrdI9bV/0qlveFGupJrGWZ9RoG48Pns6g6SDf8lAHQwZh9u
5Jkood9hp+7qdhY5M1TDAeaEUar3Ob4cj7iIWyAOx8LqBZ0ZI250cBa6XsD/ltrPlOAZIPvjNFCT
F5guK/hsYN4kbWEOnQFlud6Y6aGB5afIwwY9/QFhx73JAzqZZ8LVeU15UmNQhtQpWgdg8QTOnqfR
SfLY1MOOmqhmd2dr8h6nCEDzbUW/hY3sz286PdbxEvZnSgtCH1DVRtQyQ90sxohWbNP7tGCosSDB
eMSe3bjGGTWzfM5bx5tEY7DxWnMLm6n1v2cKZD1XEl0dbKjRG+MEfmFthTdf2o+GGkN5GPeYc8nD
3e8lXqTtEb693ChQfIEMiO6YuWaJvqPvEhV2uqkiSBlhc1qgiYC9u8lfie4xXgar27lHeQRbzX4a
MucYkmbQT+G3YpX53qj4pnU0HwmFVrH77dZYHVnkHtZrGAtU+5cybbaMc2LalBgjHB5WtU9LTpoD
dysNGt8/iQRnm+dUSbt26qtOzVFgw50PoTqkvL5HSVUESSEazOq+9NbpGw1dRYZ5yECDWCAP4IXL
/TrqGpNu5si4EPndfHy5QbAqwQIo36ipEcx+PHU6iSaZtzgoOV2ljE4l/MOKsEBdQ0clPjykurET
9yWeUUPJzyWUBZpV4f35ASG+8yiZl0pSI5jkuYzyopUII3V9s+PSPLbQWhh5VwLwoY7yEiUAAPDN
7b3vjVneKr+0Pt0bOxMex2HYDIgLjSMBRh6QQomSN0eaq9/rL9SX0FXoUtKk2ogdulRoTa4ntWM/
+GSlSMCEbKItmzc4i3dVtHaZFDlFznapHJtQUfeAWuEfVuDpJaJR2h4cu42Yfz+jRzzY+kEebFNI
Q35sJ8eYjg6GGf3gwLOVmv6b+W5eU/KECk+nUYrjSO3ScYlR2wKLNn7xqq9uJrAq/2CETRVxF4EH
rBqunw+vgT34JKSFEMyC9qStlv5schD/ghwAwKbPnz43P7tO6a8ymX4q72u5oGpmMhYdaebfTxWI
+9xXQzNHuCzKEZX/gwV5pywjHswi+eH04hNoTNoWKjfNX+d8Vapbof5WNYiXFIBO6JS6xTqrsf/4
Ghf4t0q6fvQ+cDBn6HXM5BIPI1J64jOPXUl34mCtlda4wRrTe/dsGId2eRupGdgXTvb/KgAZHH7X
ZNMVkBfM6f3wZ8auBVr9vPf0hn+LNVcT1iIm7EyXjgRI8oEze2MHd9ZRzD6WeKPC7iTGCB28d59i
EVABZKxBd1dc4LD24MN1TfwY3zS8/iVADbCuVFmXx9A/JXBEWmpt3lLDN76JMm+w4op7izcB5h9t
L/TB+QOGFDpn7FspnLHaRXuRLpqNvapbRGmRfXjXGE32QHgnSsdD50t9YKvRuBYPcxnwqKK6s6jk
8gBcD6Lsvl/NoaTdUSRuK0rLjx0wWACYnKMPJg1VHvxZUNxk0jWQVMP2hvv6HfmYTbc0nZ4CsXl3
fVrfxSo7JANM7NyI0+zG5I+EdqXKD5lnzO1bp673uQKcAmwd6Ewo+nEqiNRsUWaezNI5zFkfpuqS
qtXUFAe+zjduLdXCkARSaWQ4c/qB87lCvI9Da8SVerSY+oUD4S1iy1GG0MlbQWEoWs3ulI+CiSwh
17nxZGku5D6CB6Cn+MbRghbv8I3ZSvBm1EO+Lmw+ahJ4OvAqeyhjnL7cSyPd5MMOAH/cqzEJjaPh
YwNzgol/OHcX98k9NyJE+oa9rk3lHMTiKpNEkFlh15IWG/Npj2jTdZbpaOLhThZpHbw7UlWmaatj
u4N8YUZM7yhTz7yJPFw0RT9Y5istWltcHrKU2DHV4odr2XCYpuxGM6PMSIfBlLxlT4LMMvZitypx
PlzD1SVRLB3+NJSP8vfht35i7fkGUawp90xshMu0Z/K1kjLhzD8VD1GIsiMBQUjPMQLanVXQfesK
Spp5De8ggtHVWOCbnz7pwaDXfWSeQ3pIMFGxvPj8gIqtP5B3UJ7H6xWhwzTzqnIhwquynG4hHTqB
Hq4HoUJmcPSfOF4shpTC4kvT2FmLZ6lgLpl9ntFD/cMBDFr8UsspdqMW5UNFZg726asZcqtNNDX1
97POVOZbQGYr/ITGARXsdwdy/rqoxq+1LvDmtW4arSdr0Yx/pEFZiGwhpvAxzikrsnu2Ua1fvSFm
YXDGPiV47tieqaRTn34wEjFIdtfqP+Y15PbooqEfDqwqeOJkEyi+VdMCGkCD4INR4gSq24vFX87j
LS2LpteXKeZUfzOLTnnDamrgx9Unb9aCyStVsbJxr7qB+p4hSoy66ecQD7/mUbIUKZ5JYUlVeKIH
ZEnCeo3bdBznEU+KcOU+ojTNd7E0twLXP5neJzJht0ta6/8nUZYy0f2bzzLgHKAyS8EqgiW1UZ9o
9OFMhrqoMVfbXEQiEN6YcUf1n2YjwkzEo1yFfQUIu1kDRq1eRtNbh1H3Cf6/ffKnOTLtPwcSvmqX
R6e6Et2pFvcnrRWxH2PdfXSu8Uxh2e5OM5Ycf2clZ2cDQj/0sQLdYSDio48tuDTAPBujMej3XRSQ
PmHJ5XIwGk9U9lJNRMGvGNoMaNKHK0LndsSSku8dSo3UOjH1PfTfhSU/oTRnP2VbD1hQWmunfXmj
3aiEWkFwaGePk1YDEsBT1TrM1aHNlmuRXKbSbkvZDpHQfh9a1BlYZJsS3R/2oqZKNrSQ47jlnrZO
s2RV1hqI57M8UoXrfR/s4zk8kdjLy23EIRNeNdLJEVA/c7nRdg/e5y+3nUEAGdupSboKJUH6Se6R
RKlvNcDomlus8isfdvTiYu01XJ5K4WWgqXnxLTh20ux0x3bZVtaoledSNnX2nknAL0tZV23QROvF
1HUsUKTZcPe8myrHM6AnxLqA2UKX/NJEbkbLChrmOES2zyS6Vr5qtc2FI/yKRdlAVgtoLm7MWJJ8
Fp8/ewSyD8fy3nTzh2k5VGKKGnmMCrwsL+W1rSVFJe/HcONdhRopWX0LXBJb/XKwMcxkA5eXCGcf
GdxFQc81wFftbxd8gtSFPY/iBQtmhe0IOqpZ4vZBxbNp4iWnql95buLZj1kuwhG+qiF8hORp1Ocz
84Pf14w3hTp5CxiP8DidVy9Lrv2t4gfFRlxO7Fpok3by8vMOO/7mCTNFwUYfQGWBua6DSuFZ+3Xt
P5Vusea1gPwzrMLN3IsBCmapyutYO9u2VWS2Y9zKEyGAxH6GoOzTyyP89PQM1R93+a7AkBIjzppk
R9CdM33rSBs02Zn00qJ9Q/K/HEJ/X1eFe+/L/GKoMyDNDEjbCzY/04f4kOpV8k9L4q7B6IbKUEKX
rJpt/CwX1o8+fiRijhz2RQnrpW+ruCKsD1ye/QF9LyRxetVAxh9qy+yT3UXdDUS/j/M370iEQWqJ
n0x938Gf/PrmMS8i6nYp2/QDXqAq1YV0CsCyELHOHbXPfiXnAp5oz+4qc64uOFRXb1oN5Ijr37Ps
CS0cnP7jbAEV0Y7Vl0KEZ76GEq619VZdVxQzrPbZSEpIIqUEj8sY4y6A66T05kLIcKnD2jfOoQlR
GoBVz3G5BFIrQadWaMVkwi0kDqYm957Qs3LugnNcoTnTVXvibbBOpRG2sVEVC6y/XVKFaKNaoHXB
H5J2dkihb48fWPUfrW5PEEhs/wQXEEFaOwco3cuP+NSVLwWTTfqY71zRHEDDDaMzz3B0Txs7PkGY
iHPFYIvo2NtISahNcmgJbF3Wn1HELH37cAbSCsrxDDb8FQYnpyAs9ltuT6cOfJFmrSHYtb6wchhW
6MFxd10tLrosx79bn0GdskL2K6KYX0tY3ziXGbYds4IPZ0KGdXcfn9Ld8NKcPOzd5oC4J7bHAmFI
tJD/fsYZvyNrd19tmO3RqaS5QEbOgzy+XB2thicYxtWam/KMSgMNAjYwSmzXIlHLJ5oOSaz7jUap
K5tk2zDT1nu/GvfKQdPba7toQfNDMrF34YFN6oLMutl8QMU7k3fh92xPzeXhY4G57cXaNMD4oxlK
2Nk0cY7/Mz10Y81W8Nn92LSYnWXFfsltixZ1K7tzC0tlWCzfnuLobWEny3lwNx1x8qqZcaSJmY6X
9Uw6Wyt6Rbuu2JuQdSjcIJ5rIHa6ep5KCO2lxhyOERMwm4ZUNIi41EJO/QLx5VazA57Q6T2Sy051
Cf87Kn+KsXOQLEwsnImaU5W1nWDfbIIaguE0lopwMdYFb33xG1jwTyYwdsMFtXD8mCPx0zD9YTr/
KdJazrsG86JkRlgP0KTlxIEYrkI3ypRd1hS/fbGkR5SNzCYgO1ZZmKYbhUMiRfD4+ftDF5SkOwFr
aZfzmWYS8fo8EJGYBCesKanDXEI2xVtci2a1GspDjiwkJyH9WG3t2AzSRjn/tmOrrZSpox0v1y21
SWJYHOz5J6D6VHblADsubz9czINM0EyVEUqCQQ0ME1Wk3M6rz7/LEw4MiiV6Mzzc+DmvAlhNNLqY
qdh0uembiWtqlYokVXpzM3qVURfms84CALxV+Ll7z02gc6aWv+bE4TEBKqEAW9ENRP7zTnTwlpos
Djw2WhTqW+K+yY4pQ6ORAB4tn5NFq0lEAjc3PIhLSnHMdIXLvYvnebyTqbZECTvsLBnf6Q923icd
fu89EeBunT1KaW23GJJ6Tr+CqhjDbZnpI5akqxfUarlcMQQY0+Toxx9dtdXpwA1QnNEFc5U72q1Q
XfhyitXq4DuG3r+TRMtW3g+VMOtNMtPz7++kNwiPjP/qt9nuHlKC0A4uBaMHnmhTIvV3+dWMl9iJ
WtjeuLP8eLvXZiT9yZdFRPEgKqOJGJ7F7mYMSKJ9fW55tl0WLT836G0xwvRumKEwTqefTKvpnrG8
GejhQLPv2+bKG2MbI4USdBYEy2JSWsPBy/qJS6Ct5YmjGdgNlfTacE1RMtpOCct1oq84L7a22XPr
ICx4qqXMPL6mzM8tGjycuPGBKOo/+xX1OvcVkKHDfyUc744Uk4nOlnOlKuvmZKYkU49rL1WWggWo
r9QLNaO6/ucr7U8f1AbpcX6OtReZuBFgrXz07Oyevg5bRCaujusZZ/eKIHjFBNajU7oolUD8Gfcj
z+T+yHkHmPlfC6l2mrfYFufvMP+kYOh2UF3M2V2RQfoGNW87JNacGb1/4MI4KqaEvA3LFBJzmkIg
O9iQHhEzV+AoSCvjWS9uWaXlUAcz2y9cr9HDErfocfllIBpscbitqqdedYrxHdJZQ+AQX7GArlUH
eo6ryNpLFAB9NoRnxJdOcGWavseUc2KP6gOOn+8ZSEu8DO0sm+unSbJM1rzg9gQVqizNF2XRDUuw
yngsN9a0lp5ei+KKxuLbwBKRTa9B59oCeTtisjLKBpgmlkv9H+A5gCtFpGB9iTXohkA0GAt7+H+l
871lCbuGMup3W1qe8RWb0jr6xmSSa+BJmAbiCr5T7ppn2zc7ndgpROCDA1hvAkohXrePD95womK0
whXLmpqFxnmCknWtfqVImjTjBASE8CbH5LlVdKUBercMefUVzSPW82eRDU+nmcqXoE4HOhgLgRU8
P1QlSiH9wC+2G9v9BENnrL7sHJTlnS4GfJlaDxhUC73A0KngbYGDkq0idH322ZicrV19WCgDrcQa
ACv9xsyt1/kPX5/8C6oqO7chPOc+4fmm/KfcP7DmdZG4WeYg+Kpyc38lqiOYLp6GeEy0A21TDRYY
+oGFCpZ2+53Ucq0We43iEOaGppcP4Y3aMe704bzT1nyzTN0nMeRQDJLV9ZRhzEJmy9d8KRtggnOy
yeVHNEVv1TMCMUTSbg4LcANx3GtCjzPiIoZYPKCLYY7alNkjMnloH3KEVLmmlGtvi0dUk0ebjry2
k/kUScOnqtgaK8qXISQIX7sm8XiUR8btiREDF0dLBtjaI5Ga+NYlTsXSJHeeiKAzQmJgoFrT9/mc
kifbuCcPBdWwECqaz2tD8zkHedL0RcFDOBbAaHFLwN6+cNlxbWl9rm7nE4Ee1BC/uUC0JW+dj1cy
WB+ccYOinHKdcYQMcjR4b/7fJAknLUgYeZfiDhmElVDg6JLuqI3EVJHXLGRvc0h/pGvlyTO+xp79
Iw6C+HsqK/MKK6JxY7YmxdmLYsEg6K5ATSEs+sdd08+wM5pCYvz4Tm10GbtRk17vBCTiwMw6GSkz
Gs1+vPUkQhbxW5fd+5orqba88XKahz5e2ToyLnp626mrkOaVMIt0ASBumGzPw1w0o4+UwzAC6sgZ
qKGdSWq5xQWXZXyJImcGuzwgi5P2c3sF+8RmWUHmz0ZtGLen9bIUW/Upj5oz4B1TLsepHzB5CABW
0qa+73+IfjNXHdqbCEAesGgHmj4TLXm/jAdLIyqRuFqJjjj4nQ95c0/+2JcyV9ED2E5DC+j6sHOl
QohYU0UD+ueYsZ3Q3vxoXXR+lc7QTxZ63JRIy6Y9efiszHoLJ8obgag78IQX9n36qrXRgBuOXC7d
t2p+QK3LvscIry7QbBBE/uYYVKpZsfesfA+YZsgcZgpF2sMeexkif5mrDbwiD+7gjmK5cZD/8LsL
h0jbZ0zLGURYZIwfeCBntGqxhFJqyoo8T1e1pTaSb+6g81GWUKeJ4PMUWyzWGoiTsd4gudcEJhpu
3kzZiTnBL6nCE2gjTQZeUYsj7ef8dR6Sb4884KPCV6fWk70NA84h3oeCAMhvPi7LXveKSUCChmxr
oTI+6+QBwNqGAfZECJTCIUaMYyWevMq0eh0YrVZWIKgIeTTf9m3jXLkJEJJolWFOeeQu4hVZ4vJB
hR4F75/68feK+3Bq8erBIe8dBrvEmrdlbwyaMnCJugJFYma43Xi488INlk2benZG7Qg/YpLXPaZH
U92ztPvKyqEkA3tK8z8LDo9HLJEmHAnSoPzZVPkuEzkiBdATC8xAUm9nG1ZeTE9I59WYJDeCmoUt
3CK65caQNdwet8yGsN+99aSqhOVXfZnJsAeWCXDrbLwuuqhTtt7SNKQl/9ZPQJ7EyB8yqXNSc39E
NlFJErTtIomH5p+ixVwtjgHjxEEocRXAaKKX7QUWWqolWLmXThimIKFNSi9IBqs2hMeYAsbuhjUF
gzsjRJMOCldgtRlH4cZxmpeYsE1iif3XTO/Vf9JdnSu1hEzHPB6lGcWM+QAJNCQg/MOi2CGUAnlb
QV+CUhIWv4vgwczD2BouT8a9FFJu9uf7dU/glax0xtGzWXNTSkO3ZuR0yoOyG+HTaoO68YCA8cha
I0PgLSlAA1XQ7D8M3Ofh527+V9vcx3qof773bxJ+cEBD0qsFQQ7h13+IjZdTdfQTsMsxSVOIhJgI
QnPpIdGF1pTKCXJszsq+NUDItz9Ek0yA+pgGJozgbCvA6FgCqqwCTOHd4shrok1aHVl/uo8WaJ+l
MRVmMD2sAOkGBBW1FXh+R1t3ywX6FG3H5d7+Sw3XWNDOamXBeSvYsSz6dcQ3OAdkzU6k5jgCrw3Q
eWZSeQxBdvdwFbevhPkdkVU6GYxFhMaW9T6SqwpYHsullgs0iBq/oJk96zO4HuThT/uHJCUnyDTt
6oVelIt38hSRYB5EMrli2krMK8j/7Tyo7Is4dCrp/g761G5qzMnIJKn8LHiolOhDTmxN4/0IxCeS
8mJwxjMlt6T+YGv4XxzB0VXmHYrbRmu0Xi32EdRLnfzconppM8q+W/4NVCmAbeJzA/QmMp8Gpptl
dutGBmbGmJ+F7dHId4HfdJwiuktIpQ27uKTxKbaykFwypbX9h+GpfJdwp1bjIPGA//IgGGBeG3N0
71lgPE6nFAxaByuinIHwqA708i9g2bAZ2Vov7nj4BpRDVuIFxEX5pgBnryIMmyG2vTpdymYszGTS
MM0VVDFoBXQY09dL4Zf2nGt+eEXoYV08t59xb4qd/uLyz8Mm1h50MYOUebtnbF5hdSSx+7x//2bm
yXuZNWTipBWvehLEmdcE25XxGS6v7eeAzfCLk7qnqDEz1An6BfkmyTY88Ke60xHT0UOc6N6IvY/K
zvTh8r1Wtj4Cq4C2Rtm0k/hOEDNKH1qLdW29gt/m52xQM6sD9NVr+wW+I5uYjXaZ+HMKgG8GR1M6
8zQQ8+/za2c07w38nivjrSv976iLcmXqz4Ru1E5FOYcOQylHShC6ZfVXuP5TsysJHBKehZOkjEQQ
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
