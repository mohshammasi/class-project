// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Fri Nov 19 20:43:43 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top blk_mem_gen_1 -prefix
//               blk_mem_gen_1_ wall_mem_sim_netlist.v
// Design      : wall_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "wall_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module blk_mem_gen_1
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.2716 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "wall_mem.mem" *) 
  (* C_INIT_FILE_NAME = "wall_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  blk_mem_gen_1_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 17936)
`pragma protect data_block
THfc0aJwshUpxHeWVuP8xKvVkWdxdFIYBOsDfJPwjv7jE5eKQyJH1xz+HCEIgV+dWN6ALNk+PYz4
o8ONn1WOw/Sl0/9W0TjSBKdoBfcmy/MYhDYl/GRq4QRu8KhQwIRPzxlBgMLyqvaEIT9C2i5zQB3Q
Q8h51iqd25IJGhCBRC4zG7aJLhLRb+5xy+iwjvFaE3tRyX2hQRGQKlE/dAVh6b2dwgrUMOCdbgc2
LMlfGQO4qBqQhDvo6UtLfT+AEMNDUN7j1UG4VlhL1twOhgoplPej5NCZO+k+tGUAAh2znQM6h58P
cZ0axYXw6H7jLp6Usx0FdWGjbiL8sDd7MD7nJqt2Zxti3KZhQnGNo89K+RJDd9lzDJDOeb8dDJ0T
t8UHAcfyRz+g7Rrb2IgukPTrzFsUiWzs8vNWnXoNDirAeVW0n5safaAPCESk+gHNw+cle2GNM9fT
MVP0HUWGjzaTDnII632vq4dbmaylNkUDnZLzq4qd6yXJY0+GZJsgFQm9ny5yZ4u5wHaB8u3HRcfS
CE3lFgxlElVKaka2Da+dZbv6l/O/fCjZN0J10DXTtN/hpnNyPvJiZhk9Bs/WpkUE3F2M+UZja/hB
IZ8O5sTV4ilI4HiI8eHIlFywUltjR9q6VfhDuhwW1XkauBpjwLn1+DbpJ6lCxFOtvZ42vZdn/IkS
GB/8y1AelrbhFNx9I7oPVQKRvwkeNeALflGCZZGfcUtvDHEmM+ijNxI2eM/0Yt+MroDEvJb3zrak
7htra7/4fQpJrTR4csmtgj72S7Uh2q8Nq+Ntfx53E7FX+NwIfEv+ZAgJwSSdkIIWk/p3l3v1tALi
MgIeMI7wBAZT1tVKdlfCWBC7YQQ9bHglxy48YD39REiuta9NgPAuP8b/9x11xbXPznv2aehyqD2M
WLJg8UDst7RV1fAbxLjEc2x+tN4XBrACyZqlLhQkp+0yqbOkj5u8k0LUU2B+uc7RB4lf5BrISPtT
wdj5PRdu0XMcR8/M57BsJzsMFD1//BJFisped1ejBbnMlkVQG+vs5xs9h2runGQtErEG/k9OQffU
uROcutF9ESi8maiB1h45NeS2EnS2u+yOmneeOu96Y3SquTRISn+DbG5P2a083CskEIa9V56rYzdd
PZvLpfjeae816A+XfnekeAr4IKZbUfBJnRmo7+PZTYzaeX3EzwJpnny92BxI7I46IxmcdaxmvqHC
B91kM8EOSWHCB3J1a7/X8BhzmP6XjN2+m5EoZhKE/Yk1tlZYLTdfdgSpmpv0SPK3MI+dlTYxGDjN
aXG/UJNOOV1yjL2DZlvZPzyjUL0OvDa/2mPMWGtBCcyWAVA47L5LH/Wlo12DWYK1ShVciUjEAE2i
9w5/jpllRnJThOhL4TWFfN3gD9UtJx/Iuk2Ijp1RJaQKuT+WIYr1HC8ZUxkhxmllTqLz4SIiv7rS
MwmABtwtjg5aUCS3sKI50ITHV6b12WA4a5f8OCaxyoYfxnALHYPTGWxB1ryTG7GT0wnlsTe4s9ja
PEDGtpVnwfgUjcljlvpM3eOwYUGwqnnKHPnnbOHQf/5/bjXM6D/nf6WtVCjkeXI7tAnphB1xF2oS
VbvKaX56fvTF75yOSX4qA0fSb9dP73okBUNPFVUZeIzL6mT9dagQQGz2YSZlYBtK8H5f7GKuOZsf
uw7H6W0Oxlc1kP6gnzccwqFVpzOe0h1z2Iz1FET4ex9KsR9fFpBTy2AoV6OfyBkQ+d6bS/uvaIGr
3PgugF4ns8XjnV7Do/vxTFkSeLJDO6DDHTCPA7HCcDzSJtnpoqr4zUIS62KDuiW7Z50UDdM5vriG
p2MkmtvshxKTmpROgnQKAIWDwwsOVoCKcjdUJCeRuwwqyD3e72iPfrRSVJbHd34D88gWqIe8e7yN
5fhs1LbXqV4RcZXt4VPFqQRPk8D1cwdvMKhrr00KyLHoSEbEjOMyEd8XCIrGOdeEJxkUTY8ouipt
aLHkiRMC/zb+eG96XqNJGkc+9+LWO+CZNLiJ2MzbHCtYzitAbgdCZQWtgT03BV02i6kG+iYYbK1Q
TrdVi3lai5l/9O6ygN2e4FTgVl9DbIHYbixOznwRAQZTfokmSGnuYtxPf94sFbGGxHpmo9JV6kaq
p9wsuIrPsfZQ99yK8lo1AZ2uc1zkrt1RHZFRj+aIMnltLkdVRl834uamCexHSbZ214nsLA03eHmf
Xx9Rh9g4QaOc98cO+lBqVmZ4JiLihPE4UCYy8uFCUgbxpY+5KAZkM6tgK9IpQ8c/bIpjwyrvilhx
uXlgwVGjKciqc9W7wDvN6HQNN7jLHI4J5DYyBSk9MS3JR1LAn/eV/clCFzsvvU6VKamcP6/rM0Lg
k0BE26D6JrczKFoWJmlgK9dvlFSGK/10ljqF2C4TaHuFQcvb2BU6ibSDZ+632DWHOxdt90A9BHzF
1r/Xb+tS9p0v3NryXG4VqkFWsMYeyTQz2+N7VxwgisaqGvbdZ0ujiOjO1xszXYRNGP3Ig9Pou1ls
32bvUbmxnL6a6e2mDhIs8C53gBz7zdIcx/V5/iA9vHNBz4td90fws/b7JbQ/IvC+2BZ9sXRPQfBN
0j5j14k1/FybzRTIAZB0ZXvX12sM/HxIevsnJhBfy7I0n243jy0xNfDWOGDvwU53vMcr0pZqUWye
N0bCFge+14NYCRe/nGa6pD9nYrYJe31GelmXpbROF5vXzMW4Jh4i9oLp3MSmkkcnTnO/Z6tuuGlN
XfWQa3CUvYOr2+vFpSoaBzq1PaxJp2rl+kJwvJoLNqUXXXKIh9rHFw7hjq1qysQXK80JS2nqZU84
STeYb38dLV7Y8AIc5wT9QmtGBRNlAaPzd/Yl47gIio7fiJHVFUPjJUDUEsJMm/I6sbjBH8jR01eF
fmxK9Aa9ZMknUGgJ/3nKi1P2Ib2wWOIqRQSn/Ohe2aKic4w/CK0ibJQT1csalvEpvLWgrd1DlE8Q
6VqA6qRrmGUrzfBH1dSrjcYTNBgVWpPnJw+/cQO/MBpoYC+md3M6dox3ZwSi6gPi5h7bR+lcii3O
4qLkLnv4dCL7uUUPKBl2lyDSqYQ0i8ERyyp6I2Ia4V+4cV9P5huy9PLl6Fhlmtb86V0TSRCIFUJh
JYTJ3akvU+E4aSkfy2y8+eAaAknv4Eim558vaDX7DSbxo/fQVRFDhTtllcbTft5nCoaPstg7ol+8
oAHEkZNNj+dRSFsBi5LBY8xWpxlKkkIrdu6v6RCgNyyjOjjMD56CtKxy48aym/l8LPhkYzD9UWC4
I0SJ78rzkLjLgfaXeJjwsFSELTNQ+fAfOU6C1/kVQdFiukMjZaPAQ2HmGQJpO+VBafOvPZ5gfcEd
YUxCtOn/Wp1eowLERux88tzFtAMESXUrtfgXleTh/bCGnenOG6vEqLVKGvAz5VU+NGxbKWsMXHnn
iHh81zEKCeNI0qRj/fXj9yXi2gzOoLND1kMY0ZGQRRiYCbu4BeyEOJvxJLWeJPdlpcqSsaHUv2Gz
qVujZBL5DSJhXEJenmqITzLM7OYtfbNhbw1x9zN1EnlfdCMDDsiXWgGgxNTjTMFjz6JDwgtYc/TH
k5IHxxyt1H6hZ1FYaiSIA9n0yeD4MtSRIZUgEvvTEIr0PJ+iuDyI4YsZ7u8+cZq/mhr25q2fnAbY
q7T9hlLiqqYnzJ68Sn+wUcmQ5ZQGK2c60I0uA93Lq8jOUcZ873zIJIv5Ce+4I3rn54jPqb/ow4op
cEfTJXEyYVCktJQ4EbNyj78y1Wqxod7OMl8KjTnBMi1n5ifdBQ5zfZ/leS+XOswXRk4tcMN3UhDg
KFGpm6j3Gg3Nu7wLHy5sMbR1cF4Vppz1IL7WA6jZ4NQ+9pBDIBygivPdIe2pyevf+c90IN+2DrMf
LzX9lF+BYKjrSOWs2co3TqHl+Buc99F+20kSHRjUNVI+CSm2r4FmGoo4SwTUM74Kyljl5LYZvRyF
lGyuJTfq/tpBm8cZg58vv1DyGFkHIUO6odrXgh5oJI/Inljfa0cLngY5FpG4lPxqCFLHYrejjsKd
+AxTeypFyEUo7fHkYgrVCymL7+aWxjCVPwNO718hRvh+yQ3BGs4o2DvKIHxlTW5naBN+h5KRMl3k
jZP2Q2qcgLSEumbFLMNfnmgU6+LK6L6qVIlHT9sqD3/4+aeJdK2cZFle5VBG33vhcLxEZjWmkO/c
7NP54P7nRqraD2Sz/bqmp4ZzQ2atvmBYrr362CkOkxz0Oh6XdPJ69dl3efi7XYq4bLousNcXwgTf
zWHLJWzBnSwIS1fZ266CFvHYev81j6FVDTvOveN7QMNxWaya/e786ZXVhlfH12dUJmLW7puHpoJY
eU8oMURjxAKky6SFSbdgij899KDopyWt5YxSa+2zE6LwswsfYYEEXCdpSj6Gl5zLQTtDGKd2wA30
7ucwjDdk5gkqtmd71EpIn40QRADX2nL4dp1ksNbANVsGtjufp6/gGbP5XSM/dB0r9UI55qtXtsEt
CjKE3P6H9iupTf7GyNyD36/gBeEeOAn1OfvMQQJvKvSE4JIcU8NGuBTipHMsGXgYonhb0yx4nBuO
bF6qdSiZ6B0GSrcThW6eJbN/fMARgRBg5SqQzSl4XXKUMvKbXAa37/F++vVSrMAP4VJzvf/7C/rR
DuRtHCMsFtQszyEZZPvrvxlwAbonBTrMUU1TPxNilkCdWZT6zGJ1KuHVxdvsB0NiCw0IzJjRPKgm
XKfirmgbjsFVmBPCIgcow0PiP2Ff8ky16tjUTPI+HgnUcWGV9Z+V1VzLOuonQ0nVQ+SI+HDgfQL3
1gIfa31n1OVsMORxN6JFI0jxrtI6z/C2GGsfCLq+Xe3gjOyALdKODn6YSze33sALDpFBeYHKd8aQ
6jeQhupxjExWkmpAsisnqvMsjFd+8x9rAMyU6UWxvBqU7Yri1z9FtkKWiHWAT07wpvlPw1ZdXXRm
zN3J+eEhFiOFkWwnk3DjSoi2NIBCfUhjp5HztGhSC4P7zJbIwU1fvmhaNpf3px+l9dt2XIUShaFX
/5/TGaySGt9boTb/k3ngRNBPBeAFuiGU/5sr8uhIrLWcrwnhJTfaCSeNnQ12/kc2YnhCcp3GM+hd
Rov8zLhO32TDbT6hbRWH+md9UHoIj9eYdEJxJkHV3H1k6EjQ2wSZpxs2/69SdKRI0Zhqbdrx6W58
akJMG99kikEDpp3Ruonhv08cKWfsGJIUY61+4bXUFLqCuUdlGZesWgqIqm6RdLMkhPRyg6DvU9Ge
IH68KtDVWOPF6HbGX+R0m0uWnR65snrwtn7D3TC7OoXuuO73bI1je2rrCRy1ZgHLrsGmYeADItGu
0RuSBxw8kq8O4iqIcqLuoq9immIF2T0KGxBUFFGQoTRcZ5T8rXADtRsZpUde2acCUtvsnIpIADrU
01dxy83EWJDhI2U/H5LNSo6yH6l4uMG6EInSBf2ELkps8phk3MIG+gvCOTR3saqe21Eavp1onpqj
2NnmVGTbZDalZxixDC4s5WqLlpgLVBMkMYrzafte8sp8cy+gKssNU74Uj6MorRt3igkUMNV09ICx
FBj++6hxpLhq4vRcgZviTU+yIJv8iN2mb7L9ui1ZmEvuuOu7RTfmJDH6ydJnzsYLZFYF5IfFyhX0
F5H9d7DiXXmwEYDOE34ah9T9dRobLAhVLEGtl/FQZAMrBBf+tEXRnLKOfZum+7SobQSMb/Qp++Ek
908Sv/uI1m6Nxh8m+GX6sfWwnhyhrlb7al3m5o12ArTeP3P5rBULTHw945ldUF6eep5dlxreYitw
75rGFN33Mtp7Ks3wcm0ditYlkVPBjhw0D/Gr2RM1HiK8gB6bFJbAfvpbQd4GnGQmQJTTtG6dhCn9
1fU8THvAefdthdahrFXsoCo3svnzwHnQhjV4i5C4N+o7VffEN2rCAHmxAl2W63y5ImDlY7jbBQUG
q0hYr3yk4VSf+zupRvrOah1P5vq4BrJIIdRGR1HxpfqB52Ebgyx8t47BoSR+Ego3vWmjaPnanFLg
INSUg3uHp84gPs9WcCX2MZU1pG5vnpJrCFlHmjNazji3G9IahDZPTvb79WNIorS/li8rioNXfAET
hho/fxMzeG/RIEgdiujyz+do91MLksFxPLDPAzJ6GuXFNdJC1raOIS3+I9XT7lFcqRN+KMdPTCMq
GKIP4oW6ghay72rZETbOCy7BK2arR/3CVDtasi+neEHn/IzK6iFp9z/C3RpOTavZGXkvMm/XuQob
En/by37QJAtG4mPR7s4ywyROar11zYf9unqbfWG1xwPBWyaEZEpePxkoJsfldureL+V739PkHXz+
s2i8nKaTT6KsieDFNGtrRnEv2KdRsRFgGG22woCeLcKfZrc/uNhfQEOyj0aC0ANAKmT7uYr0wmNh
ltDlixt3pYqAIezgat+IfKgyuGPwyj22A0Gaql6KwqUobT7ESBkkN9o1bCc3Ub082TvSh/J4bKIL
RD5icZaHOOKURiOCUP37KBU4YkufuGGbBKW3neGKxiVuyGsAxdNXy94VkPxObilEqaKq3MbGLB4g
ENlnHVT8xCsdzaXl6Tp7528+eL1aBYqv9KGHv/uEQ37H8qomovIq29jqW5rnVGVDbV3OkS4mk4WX
ZUZIG4VtC/Lz8bXLI5lTzRmkr+DKqgozrY8lCmjaxCQwhm6jhoj4J2Miq//8GoFY4VYN1OMqLXaO
6zsJGqnzomToqtCXxjZ0Kp88dGoRNyYvHVFNTjLdjE5ikh0QCbJg6oZ2ky2kFL9CKpRn4EiFZxEn
m5cct9kbtoOhIyeL0YhA3pSo9OmuA3JNVdeZtiY5RfRJlx5kHXBDPynoagalrjfT+0eixJUdVYCg
Tuw6B0c5HxokqGXILZZDUM3JRXiX0FE0Ck26MX8bJOgjPMPFoUU/VGbrMkfrStIvDMuG+YhQ+D8F
Q2SQfhIh6sasXXb0UnYOJol1itg/fqZ9FM8o2LKnU3gQuTB0IkfQOBYy3W7GoatKmH/FvUNFUVPg
MBOhfDYCq8QB9E0ivCurvXJWs/cmF3V4bNbrAmwbaIK9QwyzHve+l+GGAv8R4VdFb4PAEXMJvFlR
NYm5XEDf+YMLxmkZuBmW1trDWjVLkJpXuH11w/Qmn9Vk6vCdR3lsRvaH6U4hYGjIglZ7W+sDr2oj
dJJxbCGzrA3RXd42fJjGY+oOqI4e4+ijn5Ct0WUVjBNY1dWT0bJ1OEWF920cOKpfPxDazLg4Se66
Ev9GVkHxaFVMAj+1tbiCO8IBaivMC3bWw67pgITLru86TjOwBTn6fofFjzXiMQnjvJMB0Gy8bIZo
BJALOcXPPEqPNYXn2LQc9JWCUTkksBEjUr1S83/1B8CWCvo3W6QWszY/s3gvoUiz9Ul8lrxSWh/P
sumMPwE04kO+ymxxvW3kugho8ZpY5ZOngL8CiObyQYgg9F0uvT1y17/K2obu+reLWHvkXdP55jqD
ZuDZewHaoevBhv0BzhvzaD2od7+Gyfl4kDh24b2eHlKNCWi+KgVOLsyjauiNOc3udUcVYONMN5Sa
FhlVY7mJP4v3rsd+DbjUrgrZmQFvMiA7rylla3RdBTx43FwYdeCWg3kAl+zzx1acmPgzx/fGcrIH
ZdJ6s9k6yymyDk8Q6aMdSOHThT2jM8HLVSKT95A/etugzSSPDEXpHQFQoaRrDNH3D4YvMHn+wGuo
dnB+9kBOOgr2cUaPZt1FCxHqc+XnnuSJnDpy6yWMF2M3IflIit6Vvxx/Zru7PTJMlagx8m8Khn03
kk0iltaLZJR6AFiRodt7Mrz01UCYZQgKQn/Ctm4AqluK1WaUqQle/g77EcaymRpD2GwImLIXNZ6y
O/smxOYr6E7dpuERZWJ41nkIsS5a5YDHxFVFtaRYK54UEPodwiFjJ01xb23puAeI3qdJ/TLFHvzh
8r1Iy9A+cNZYzO7dFcrzvFb44iQk2Ta1hqzWz8Zu6kyMBHwjDpnAw0NgAicTrBe8KRQFdMu0VP9H
4FEiBgWsOKnpWkUjUQvXxLTp8BxWZaJQR9XhSx2QwfiX1uwAqMqHMdXkkEv7DLmk80gEnTHI2YeS
yMSxb+k2VukQu2sCy4zXByehb60bFONUB5jXwH6h+7KD5kS+C08zpkU4zLe5m6ogBVPP4fQY7Sh1
87+t1QqW6pDGeectNCYD1cSPIZczLxqp3uU+9YrprEMXhjFdAEUdSfXA0Tpqf57wJei3mT2F98ol
7/2i1lnQQwfmJwdLAEDsKpB8dpBn3IIiY9+zvM/SDAjB/cYR/9iJ3u6TNlIe/2FAeoM9aGm7h00J
GxkwP+EAM2XnKVmdZ7CIj0f4c774YOovByx6YbWDle3u+l/Mm0y7ixesX3DI1SJCcZJKd23CL5yu
epaHlCUO5GMNxDYXnQTadKR4iQ7ByErdcArXjfwrJSG4RwVJ/56XK4unfGL/kuG9c0IBCxrRRPM7
RCtRiN5/p3/Qy7Sx5vFnft0q/MRakQCJCZxOxe0x6QDB2sIg4a4yIPCnWi+XRFfq7BEXNNlllxeM
8ApY857ivMxnPWNNGfEZTMGKJJuef+5ZU49vI8JpPr+uZIYq1Di1UK1QXN9NJcCMlJSWowUvASFX
Jvhmw59yIfG8TaXYPKH3lmbwu+8nERxx5qrdw0NAGQmlzpxJj5sFZMIN0683E2HuxROe+6hiFiAu
Rvdhf9vWs6Z1dkPb1cOvexLSE3j9w7KrkAV7uWjXbnnkpII46+Mh711chozUxDinpJdhyj5d1YSU
PubvLzqZkVsb60tOCAzdVE6xlZcs55wdUbu+DDhgOgqT/zaeqXrlHGB5CK/iBW4wpQON4MbTr4Vp
Et4a2i+GD2LQrF/HahPCam8uIEMolCgAyY36TpNHe2RDR4SaGyzYB2wCoeUg31pSzWmhUO/M/oT1
fV43PvukOQEzqvIL3DjWl01AWG6zqpryz6bXqYhvqTjhozX9rejYwhl65V86fVscwTf0uMJ5iA+e
tpWJJHPrgde3pgFfSWKGCKUNWU8cp/hR9EHdWXTFA5sxz+qIBEyYZ974yfq8mVzcurAbi1qtcB0N
R3att+dIDv0YhqtRV3nTtAibtkempjVQl0UvpF7xUbMdchbCcA+LT8lpiJxvNDbwBC3zOvewrBmB
xUkHkTbeO20xyol23jPTnflB6zVgWoe7g+UO5IfmkDDE/PtSiaKQTTeYj1MER7if8Hz6CHJL18z+
zngVnpwcs2k7HO25a+YCJoWB8XiUz5VYdxgg9hmUc6c0NpK6s1EMB0OdVWpuMIOc0SLS0QtUy81Q
DyPCaPDjqoWiCRh1Zq1vzKvC9hywTSvwyQvGQNlf8465Ef/fYRgdHk7xShC8BPpaFi6LyjrvH5QK
EKI0GyY0c+qBBP/G6ikxg7zZwLH2z6AQp7IXpOqN4y1uyeevt6wCsPYirBrdWvtt6Bk0JiAH4R7R
HonjEf/qivq1JHVBzlREDB248blH2wNmFdI99GzQjONMciVtV4+Tx7Oh3Rj6j2rHtaE60ezKb/Ef
38is3Km+2SYmK11VjOVjOqKMzEFRBZ2/yPokA5Eid/Rs28zW38IQOB9csFUMKW9e9UhUgF1gQguM
JXfheigDfqL+KU+idF1Y5VMCUP12VnUCVWLkNqCjO506wrF1c+c19MJqmG6Kj3dMAfp/XvccCP6n
mTHJl3F/Am+60hcwfVwTcHC7ekcr3YqElviAIDc4t2EdeRffbc36hJ8Ppwg+z5tXv32l6FIkCu4l
2mFDYX2+o/nm8Mhx7m4/5tWCxwer0RGjZVW4UZ4F49Tv1bICk9cpMg6xfCh0QuaEkFfakLURgzUM
J/C4DbJSKt7f5ROmJNRDHq8PzlllTHtDs9T18vfa9BGigahKgJ2alWpg9PvmqSQUTItoaCH7v10f
0SD/Ys00Sx6GEjkycqn0KPcvMKsfnc8tLK/IUGQGjuzXOvbCBE16XGcC5VEXHF31zgaBEc7+2Um2
wESyQHNQJd9dqb4CFyWR2Lyiw/cYJxY21h41kkTPqnRvNzlZDZ9+tIk6jOHNYYzaN3piAai1QS4I
OSgdA7hWcqB3oLSrdoRr3Mx01IeZcDkfLdns2Bf/7RTdAJZxaw5qDoBwDT7x6kMSG3YKgaXDvI0Q
AzH+EUOKjk9C/UHLPojCbDEVDTJtAQ7tftJjYOVXirj/TqVeiEn2t9cuOMT99YYoc33WtZzOimTK
PZM8pYlpLRZujlSjAb9jWYEFH+tMtl0QtwzCFmQrGO2jQ5CRIjmTuADYqW+kk1ATeF6gAKg7h3di
x3vqzWxt3BYfOqCqFgf3cQD75RUX06oaXXLccrDaPxUTyZntS/btVNL82XtL2Gazs6K5wqAy66dW
bQTAE0+FWSuyCUaBhaxpLV43nyJrUBIjO796/OX20QCM66QhCnvIqwBZfVDTgYXYnFx2MaKnByMS
hDrPHqFnIRKwGrxBpg+9Zl8EWAc8P4fMpOeqIhUIqXw+pfS08YEssTLI/MLCxjOMBCZQlagFhGZa
DvLFsczXAsLWeVXk0WKuCknAkglV6spDg716TZUaemYsZEI6Ls5qJOyv100mQZcgPqXsAGvtZqF/
84/9ePA+yoXxgdNFXQCfJnQqlzXCS2GIJcsXTmV5mCPWgGuSW8em9swgoj1u8AraXyam8oNE4NlG
x70OJy5ZEy8SfRTCJt+qqhfruxMGaYat62gxw8JWf3SJNCKJhhN+1A7UiwhiahIFjwxqmtZMpMpU
EZRCqmmVHmUchfy0EccNlqhfYiJDDKZMGdsd9vGIRBECPKg2M4/P+yAKebyUrwBJso6T6U+egV9u
opxgxmca2gpgWxp6Cz+FazBBmL2uLEarkjg8WU3ZDeAxsusmxSfeNpuj6Svo08dsJGw+RFEcghB5
dEW2w5uhqwHhfZLw6x/bfoyLB84zdbBYzy5bz/58cAX3bcAkz3KghzojkfrggPa7aKLCkxtfnzW9
+n/l+Y1K2adHCs1qkVlOvCxsSCv7DzjantznyzDFhI5NT7WIKlkooKLbQqF0QOZ1ikB574queKWs
Wt9+F6Tm+lv14sg7XS7+snOUt/Ql8LDp24TUqOvTOm4jz6O6gth+Ev0gKAcMW2ibs/AtxVT0Ek2i
mHy5ANfOEKig9B2EzF6TLwfw6oS+OXdcPMuXZ1V8TgaLLhTwHqv/ngxPU9gUOzridmo4rBbO9Lwv
3V2wTrKX8jSOnUXfevpOAsOgzHwv0G89Boc1N6h9yFNmWC0E39Eod0NLGtcq2+0aOAgiCAO36w4r
W3ZnwS3JrGQHMolp/rm5xf2jssE4+1g7A1IvlGQX/I0C7HzPkSNNKPDsk0FRouxh5QPaNTee5Csy
MsBEKmcySHwcBcAhbsGa+6LE7s1Zt47KDwemWCGwWn9NnEV975qL1DiA7biwRozcWJbwoWziFYbU
AVa/sZVxwiS+sYvWOQ+toxgL8sWhaAGaeIMz+Dgt59QdsKUyjSp+UrCmM8ZPNxiLbMBjXbN3pMqf
zyAw0sa8Z3DbjsyuA4NMKXUjZ36AgWxYamOYvdHAEBF6tqnJX8wlI8tC8JbbHU01qN5DfmvTB2YC
lJlsT5m8oMbP1EpCi4eqO1bYveNNn/vqk91WUxUQ8HD2okZ7Sa71r12zP3X82O2xNfne/ha9V2xi
2TOIRDXaZzzOiBxWJ+E+1Yc37a+Ru+8k6TOKdK65WygRoyL8PIwxeGn5pseqJODhQsiOd66xkPNY
q18eyAmUjv1OoDCN0j6AX3STgWveJqMl+O01jf9B16oZphREaWh/v0DaTaFf+GhNyXDLkT5dbO37
epbiHiSa5qxgcKJv9WG+tCRFc5+opmCtbE2ZKTd/S5iA5MTAOzDxyqqkZ1objWJRY/0dQK4y6gig
TZPE317U73EMHiF2DkodDUlWTy++SF6dGu/a+e4yC1J0TZVTkFwWF30EaxJQEEq6O+T5EftW1JoW
150lASgZ8exhLGpA9FiT2vZroh/nVHrIdicEpXivxEQYsKC0A3q6/fC+dul6UjugjDzJFHHWfGqQ
+vAtBjuUeGZlK2eFbmjkmazW48+n50CELNMkmU6t22p+ltNGZBFPP5Mm7kp/CSQpzy0Hpha27m3O
Im1mMPHSxBtPtDR7q4E6TYnmSHvQH7RUPsvJNMODQtwnSkosCgf/Mn3E8mCxswbu+mFrLVXg5vrl
H2Uv6fcUyN8qxHF5osYX0XTVE8dDtZcwNrlCkp97lWx76CyUO4PRP35A79RqNgYPzAIzYGYfn3gT
34GrxuQfyduS/d4uTR56DPWyXUzQo1lcS/qL6io1XXUhywzwsweOoO5rkZ8pZ9m59bDRCt6su+e0
risav5TTi89K+k/vfEWwRo4/F5fPT1Cqt95zvyj8p2lpgq7Lvp+5wKLgLjdOViw6ihUcpPs9yUW3
7Po9gfzRzap9L7M7dwHPjlXfAJGGh7hNN6V99JzEQEjTi5h77Um/NdamMAjw89f4iNYAzkS6rojq
19wdBvtFNq7S3bhVopeAMqnZYPOLM5x1/94fgYJiWgpYIIPbRrHmRy1yyfA6rIO6+EYgw76yAn8f
qCFIjp40c7KlpijbiI4JGCL77XPQWpB1Aiz6Ub8qkJg0ctb5r7DwaKg7K6mWFsdlzbXyAvV7rZ3n
LmSqJGeHs4S3LMLWIoxGtdK2C4N1L83lGiyU95y1HwYnh/dmPPqMO8lpzieCXkxYNqXLyCo8FS4x
Hkh9G8ggy4bw4qAIevmnoF1JINVaA6C3+ic7HGvhwUdRRBg9g6ioD5HY8x2DCqXZyLVtW8lK27up
3fZoyq8Jog9kbymRjxIzl6YxwmISuMcGm2vTgc/rKLNmAXg7528nF/oPFOY3VARI3qvbNEFjr6Uw
k7vH3MbcttGIgaBUxQTkGvcGE/fLDtHPGEefjSztg0mU6f0mRK0OcflbNmhEiDQp2c+E1RFgn427
j20xv9W9tLn0bjm/RTLaCJzEhup04VUNgAh49oq5mul3VbXol0tKVL8ByaovYiKtPCdyMwBfQKCe
KAGwWfpQw5jDPLPe6pr5Zb0o3Jxne6gDxioJsl/3jlgx4hqzgoepu2dDcIw4GMekZn/C43VTGVMA
Suur6aWHbA67sKbluOQzz0ijFwlGau2Sb36BcWHYwsy0Jka3MXctyACjlTjdOfeJHjkvEHUZuntM
zXYhNGaYMVh5pdu1qGETdmJxRvRbEM+sd+RFuznLwGnqB9T6wi3uaqkOKY6MAh5ZNX4FuOQgkjDK
Kah+/EZaCoEUs09vWDLHkafabCwxl/zGAP9xaf9GLXVMdBgVHRTJ+5K866LmPBdKF7S/gdHLNK9O
W8Z8fzFr8lCbZBgUwBMcFUyHNBpMtOrlwTjqU5WLGU4Yt9a/g/qzoxi4yXGX/TjMMODo/+8o0DmZ
vYE1CjNy0Phu55MgT2NhRVCMCbXYpc/t6gxbQGODR46HWE+u6Sd+pjZ5dVmf5QhWkdrIhSq6/8GA
fhMFNnFxzhf9nKCtlRkk6Z0EpOTRXmgO3L7fiyURAD1nXI/O2NGNwCpBZ5iDVZn23IMGYnbZgnk3
VLgqASNccuA83V7hsRYDBsiVQPuSSH8Z6DZejChm79x4+MHhGBT6AvWxVTtwaawqCiRpAMQiejXd
UcMkapBtfItJ7/MsGZqp5hvXHw8pFo151/alxDkafuB0tuD5k7/IfpzYKKSgyhuGUEammucg6cPx
k5hVbyLu7EhjngFPYQqJyKcQG8QNQdSzOGyK/CNsFxLzjSbMvTqe0TcAwB+Rhbnyr5FA4Nk3JFig
tJ32ysWTWYH/TIXVcV6KjLaY37RZNgnE89ulHQn8+ashjk/+OlaY2G3xf1RPQQ04F+LW0HWBWP/B
pWLeYe/vI2VzzXM1UJJK9VCc1ZoVRZODYWDujdcE1QlA6Hrs5gIITmO6nxNPkogzef+MJQfQY0kr
nj1cL2peIBbP118qhaIE9KCoCLs7OyPgwfT587EcGXiAN7+uQbiaBzq56ySE/Y9yFjQ0+0SoNqio
+w37vCZAVie081MswPO4YDm0FNbzj5cdnQZHLgyQqWpKJc+HzS1W3ZSrKulyERTvZSGdJIaEY7hD
cBi1/abmtjoqWudGE+9rkWyyjqE8/BDkM+3v8m4EUByWhfQ+rLnMDbuDlFFUs34HHM9e7Dxj/xio
d1Q47T8QR2ClsUvXmc2Ye6Y8eyleRdXIDCTlAnmdThEPIrelVXtq3ugSwApR/+u076OaBnapc7eP
M0GQRrd0NJJKqF9yz+utZimIgBAvLS4wvkfWpcWrN9meM94W6v/doa99wst295AtVnSQCvkKnQnU
/Ffr9LAEPGx08cbXoYiwr4uYlrv2wyPnqmAzaguBUSgW0gfi8H7K4Gnnc8x99V1f4XsBaCHJ7952
WVToDuq3EL0OZnXWCSIedGNz2pTp1a4e7222wpF6UPLNig44A1fsGUB5PLrc6/WqAmRN50D8mgOO
Rq0cheEi1FDD2Dmga2LUFCzQoRRDTOsjzyj/sI3J/gabnxCsMRxbvLoLOay4xwhw9wRiwRvcqf4J
5ShucWRFHMk7CC9IO1dOVcybbssh6Rd9O9YZtivGpX3EwM89dg+/L+mZA51CGRKzOYtlQXG6EYaq
RKow3N1AIPuYam/j9NYIZ2GHi3RgN6aLI0POfNb+LGNWlZxO55t/OrA8zQ2pyLhXG13qFsEB1UjW
dWNm8Ti7tt8Ms4jfmTF9MIv7UBQHuFVl7fIN0YdO2bIewvmeC0bnRmCHmTA6EdJS8fjnTi84f2qj
Jq1Ra+Vc4besnkF1vGF09+mDhQsm3kHT4iUI8NS8k6sF6U0kVatpPh/qrvpbiI08d3REikETVHDK
XxfKCwlkpptNxxgXtotrZ7Sw2W3KuVTaMQPf69zEA6DcWmmjLXPABZSWkLHZWutGgPYadEZmjQOo
iCoN83yXjonXmPxwRk4aQym0DNz5qD6kZDymy4o6rJwpLDC8IH2z+BGTwR0lytoxwP6aMuWOUXHR
r5ioyt4XOQF05G+zAQ10NSEE1vpqbdybQqNIu+zNd9vtDNAu6i+unwSZqusJpSLTnFa9Iywl3CwW
1nlH9ch1zT0Yu3hARFgqYfsbMl/HPcJl54GQ8EyGZNDUXAN8KscixVzQ7h9v3/TlEszlekcttAv1
cFeqLu7PoOTSHwOaMxwQKiqiIfgUpBfzTnMvbPXrtbMX99nHZhNX/QpMpe95aQXkgLuB0WbbiiTy
uAoa71OxKwcWXr1HS24WGLk43YQgTbcx6FP01V/BPD7x0lqBkcIPz+DcRadt/Vc26b3QIeETL3Q2
NRHSpNoX+b+jZMZBCsrUmQESBey/u5n7qdiHVcDU651w45t/dvL1q4jimt/I19GR6v/pXOsXOkMQ
zDoc4RMBPw9oDfkweknWrVcKTBOx4LSgbJkUmYWIQ71gJWspnqSj4uTggz++f1rQEiez5Ns+ULiE
ASTsRZbO+Y3zoGHY+psoJJyinvZZbYImlzY3MxbzBR9gXTZp7RXk9qW1wMNPPpSCGeurNFpfmCJV
fsVzZUfQX/aPJBWRjHIZ5l+TReZIboFkE6LmO6wvqm1t+XOnBuU0jTPXvG1rGqbJGSiR/jJUU6ku
a6xgIxm5zq0ElCMhcZaukZbvEayYYAYsZeLy8FW8IHroZ2WnbBLHxfSoiNj41vt7z+75A/8FWNip
OPeLGByDbEp+jt62AMWl9mP14muwDAC2SM2gGnS3bHnFH8NDT536W5v0lGK1z6KkFy59w3TSHwtn
k1eLqXXCnLF7pmPv2X1mlPkCrvwkh5jzdUOc5dnQTiP5MretmiJKeO6N6yzDHd1m4nsKNkt1Ru8J
UxQbm9z0KECs767fH3oXbZS0HtLT0PHjtIlRxX6FL1K0SVWLSnFNlDEfFj9EUaPcWRWNXSO7P+3o
QTs1W4p/vEKBDcjD35QnrUJC4b8u6E0BhJGTx1QHVyRIqpyIEzg1B7p+KNnSnekEZfO13Ho36q0r
j+YtMRwnD23TkVYfBw2AK01AmFm1/FOmTG60+h8BnUCx3f1wy2rB1MZUQQ0KDIy8qFYRP8nv/yEm
Ktk7plEJGdMe12cPvoEobZUGudh2T/VujcwmXi4koKXt57EJP+0TAKJTQeoVrZmMB0OQK9QC5OGp
tdwHm+zTbKQSgMR3/qP3Yqu/ysrG7l3ezZn/ggjeOeSOUOHYmxGSg//CQs7TA6Zwe3m0/CmL6S2s
WVO4nA6LHh2xMkn04F7WbK88weffGAZyTcyeUkLHT801sSyXc9CqzD9eg61qr55wCEWmUPZoztAX
Thch1V+ZX5dh45GMH83TGnUcYvZReqRDDOPI2zXP4KrwbVnQVFr8bxK3sjYAByCeq8U4QqKkgZhQ
0257J+2d+/Dxo0s5dEEu0ghw+dAcVvboM8ilcMZKlGFNqGfq1WwMh607lxkkBkPOXUQqVBACy4LW
8HnuY1vY/TC/IfSEmk8yuaNOkAJDHowUNxqG5C3LLQkz/nv1Tu0V0dgZAlQ2C5n9LLrh1pu7S1QQ
gNMPVS8D3YGvhN2GOEPbQ+Kcl2rbBnRyCng17IM9yfHDHRg4iYeNmL786X8HDLEVA6keGWTd4ecv
GaJrOn29itD0RsjIp8omZn9uikip2veEt1Yuw6HV2AMM8ybvlY2Uubg79cQLMse6RAac72z4Iyjs
5OtZWXN8ZH2PHyV0UD6hlildgrKedid1bs1GvdExl8KR6QbFvl09ZM2tRYxs8CXC628wtuaTuQCW
buPcG2WGFyCOXDDo9HUBA2yKzOTeI+J2vGaNnQwLovDMAcpfe5GR4Up94ZfZzZI+rjEA3fJj3HJe
cXK1GyHBZ7ryr2pWhGfThg0ZB6yra0wu5IkRIH5N7TezTyCcD45ALIXBthZfMM5Hvd682ftSZa5H
wwMsytfpC9RKPJBu+aF9wH4M0VQKSDQS9YnYLsPWzTCYpoMtKOQEx3CTD0VIrinTHN1Jx66gXamw
+is03TNQmI/L3iFlujEZ2MfRlylgXS9lu2p+1+WqnM69KnTrMRbeqVCMGL9l4sXyJYbl8CO2eqOZ
6THkkv/ppEsRJlzxXxia36a4nr06BPpzXEdp+8lhW4D+oTiNd3/N4LDVBUtXzswn3Sc7jZ+8L0kN
nUl/0oPDkDYh91RF2UvBzy2m4Br7UAxqhDS9Kk3bJz+OlsJHtYQwIL4evmRVIRYvWbMKPHGMV/s7
7O0Os6W887V7KFUL58YaEaBpFfOz4gRh7PfN2fpWdYJdpxoXMDPMVczg8LmA0MvBic5daEEn9jLO
ly0twitqdz/13ptBv8clC45+NlrvisoNy+npU/4X4I7h2Ajo9422Jb3d7ZOwvBW7vnR1NUCTu00L
p3+7NZaaioJJOqf+M31bjCleAW38Q7EwBpBgMXHJIc438aYab6jn53jYtfqPWLBlFPw6BARLolmZ
06VPBjgtNt7yg8mNfNshdjW1G/ekH+lWpnF4nssjQvZhabOUtwLLttxzipve5Ez2Xi69jFaP27Rl
dNponTa6oT4wpLZKgyXiyR2Os1Bl6jcr3cmuSqCDVqYdF2sk9oL6yV8wt3cUMqBnjpw99wkfLHWE
+RN6zunZVfcSf9u0kbO4eAKh2+1odkJynqT58CwtuYyC0g9XeQbrQDcOA8S6FQb4bA4/4FI4+gbH
4iwejJfgdTvoaSoH5aMMmTOyFpFumMFkN0d48xkl7AXdC1yS3k+DNlp8ZPQoYGQIr8PXAkU0YJoq
Fv/PBB5alQ8eedRV0Zh9ROLHLIAcyGmOO44i5zRJTULKMuv9nfKVLJvSE7oNcucllGN7BICQzHsG
5+SdSSrJOHHYTdUYyKW9bAq5fMOqUiMweALxrpdKhMtiaH8/LPzbwHEnz4vKIsBIaKKWXTzqOAUz
3GJMqwote0VuEdNn4zYFjk94iixRjIrJrTBHUvalVrPqR29y7PQXh0eAR1sef+Zg6z3sU/fB6MWh
QhDQrRmYdLhQyF75iz9qWSHa1EIRWqFtqRjcqCv0+Kx2B7JAqLPRvkLo3kRg3tOU7I+9XFc8h8Fb
VBOTr10EtJ5Bzq8DlPmbhMkNGMqEtbYBiXIz+h47i3za5S6kQ5+lucgbf3Mcw27AtoizVxngEmhe
wieKUWIQ78onznq/Ug19TmHYjM17GyCMhsd9LChdIR3NonxRutMuswKb5UwW7iz6AJi2v11aXFiM
H3tGlc9Zv51/HxDiyCz5APlb5cUuIb59sOWEbGEihfqG3sXdB8QoBaZdM/nxwxNl/MqBOKcEf7zi
oVm0Mdb2n9lvXVkI4fv7E/gmMNv/URxBZoP0zQ7F/CCX8bN/UYHngT99EZ4IhdoSs7ya3bcNVWqY
LbZGjvjidJx8gmUmJlgOTmDlCYfP8aNVjNV66lo6X/7b3vJswMTROjAeOgSgd1xHndOZMYmJQPjL
WaxpzFzVeT1ir+WWy9S/wcmGbMQALFpyzgRcX4SYqBD05US05sCF/M8nnDLncexlfPniTRswpqtq
bPjm4+PBIKMwDR0fM4ye6QceX6DmWMn9S4m0q+Fk9TguMX3siWQoWP3lR81jFqyEgEtd8qEOtB5W
artTGArJM+BXYdc1A/pVm5PZF7fUU/DvYkULeWDNtda/Yahc69IIhz+b4Y97ZcjJw38IDydYGsz+
1zwLXp03TLEdFgTU3Q5ArxW80mdQhkN/MD2JhV1EByUsipP5dNWM/ywVNNfWG8So57H6CV1WIYlL
9lfCYQrNAUKXIMZAPZkWgAeotHPcQOwrhSjhEcXGS1rOgOWW3HkV5fFNWRjHaKzPkCrvgPWfXI4O
iPEKheJ1Fk3B6YUpkUl5gZ9EzS3gSqwF6tKqFBagasM7zycD4rd2Z/ONOijzVUiNw3qW3yYrNQof
Dy/pZAQIKgiqEDh4kGf65z1jm2L4X9FU7SA0aAR6x5B4JcuJcKgIwnHgGZEDrtplISoIyunwBOPs
7LRH9anS8wLECmNQs+D5Nm09vS1+KLM+OOHIuQRZcwzC5ICliHKClfE1mn+VGw2skj9YJMqTp96c
ZQqLulvCujStgeSZ7CQVnAyOCx8VLVmNb2Tf4ljJrNFOV8gNG7Bq/uuCa3LyQhv6FJEBH4xYKiP0
/Z2dLaRue+doTBd+brIiivUXcuNdRpqoQqFYy8C/HJ/eOcjW0swZCwZi0D2Chf6a/hzqKJ6mD55v
UVSMw8POAhQJ2Dz7u6FwcEy1HEjtpU+sr3QTHKQDCU2VxSJ8+8Do+tY6oxjCmH/QTqzaYPErdIFk
qPhIdKKylMUWsRsHK6qgDA6M4Weocyzb5gDb/oht1ERQriI/ToKa1VWT464cYh0v+rwfXFABAW29
ESmbNRZJnwbtKwo+wxiKbwTRNYzFwNGLWTuZwpkyaaztux1FNv7+XW14oa6oGYiLVhL8JBQGljwy
mfT20WC1Z9u7WJ7A5InhvK353T0NZr7o8WJ55fB/92JFt2Ivjy6GxnDib00SrwaKz5kM9as2t1MP
lBusMRTnFv9z1AncFNdTOID7ejHO4369UpeGYxYksFIXjAi0i75FhrT3/E69q0CX5ncHOItfSW4x
c+jARgHa3WFe85FquqToVXfswH/l4HMUHz5WhjTYvRjYm0WQynfdDkvTccyjxQpANkuraz/JbsV2
3Q+T0t3opNiuH/5EoRHT52J8xGNmODtcNewscvZnIoZTTfjuGJ0isgDG1dWoOdqam/g4AoxT9wM/
VVpw49LIoafByiQD5D3TemQ2G609Ck2+zzikYLdcEJUzSdfiV3EgSF+Ei8O5AICa1sRTAfuMlyUU
ndEHyFhzVHNGVBQJBvo0dgBnhi9P6CrSwZZvMSW8/QkOsoX5dW69DQuFHOQOGNfyNI4pgYt6YUAz
sZyhV/7nTYy33eclyZvn+p7FB9aOIlyIuCXsu+2X/Pepqj8xK/d1B8nUGrh0iuQQbPpXG+hz/pey
wpA5O6++4m94zclrB9jseuyjxlK27n0u398xeVW7D+fqlPsRa9w382Wdj5LyqqB86b7BuSrduaAU
1/fvaO59v0mYFgJfqhivwBOST4DmAooxcL+/QV5LQbgwVCdmP7ry4+PO1U4cp4IQ+n/I7e9aqvaI
8jxXV9qBJPMwMc/n6UMV38BQq5H48v6sS/u9+2xpFAw6d4Pho8G3C5w5Zf/dbvUYI0tWfbL9OYJq
iwNtJjoRi1MK6UunJBaCr5doU1k0sNmXvtpsbZbUMwzf9Ow8s9ZlY8PVU7HCiLVL0/+XulsUVQDT
WaZVsCOnP5w243nUC1bgsSn4zJicdi5Wkt/kRwqplzMTIulY3X7EGMZbubUiDipTjtYg4kG+ZiTA
8m7AyghSBpXEXfBccDDYx0qzHXMw5k5MzKHdyLyAXFiMs/DFd11+b7+zNR6nDAWtoshKEOA3Q4yw
Av4AbrBflZncSd0XjGXB5wAKIdVJwTaiN/oYI3MYbH7tGI9l5fulZ3+wHGtW0C3rEXANaSCwaX5W
MPSpN3/cOpfVi32AWu1jhzmQWVivJBjOdzv5O0X+S98+B5VgYI/4gV3GGMyuZaimGN6EbbxBxoQ8
ZG5zedMaX+g7axF4L+q4jGVLyGXoWRKb0ZV3QODa24EzkJC2KA9KPcZbq9lwTF+gtM7QFoaXwYgi
rKpds0d85tnLJeiXxSc7ZN83euulFP28JQRYxHXJq9mpjhgYFXRQdo8d3402EbumplLe+6CI5MkU
lhn2EABcHuei++1Ht7SaZs3BTJSBazEuJ6yHNsaeTsvJ+FH3Yj8todnOlC6N1mhl2TPC4IiNirOe
XfFxS+o13EA96pYst2KviE49DsqZ93AVPhvLdJT/YPmTNkItsb+Eb/Ctn6u/AlqBJwJj78ApaKui
xxD/QS6MU8pkJPfIH8vw9IAu/LAXh7LC3278vR/xNd2DKr628G7XbLVORs+/8CM6hkRDa6jMcqdt
NKxhJw91cK9iP+woNedmq4XWvmvOYA3RRHslDzEaaNnGxe7bLNkRMGgeIv656tUriehu7y7Wycs/
ljSJiF3qJnqLJvW7s9OIDMgoYdzsZTeM07sLacqYqR54Weouj6teD5cKC3TEwueUGa15C6cTa1M9
QoSo14VeL7ywJOP1CLYhi/ApL2NPaXLKA+G8meDORs+hYaclhHaaGm1Lk4MmwQA4BZDM8gKKcNHh
OEr28VCuT8jrfe+wwiGwE/wKDKopZFv+Wj72DAYfELQhoaeftAJRq1IoS7oVxAzIzBKovL6sJCpW
iNTj3QfCuncDbe8KwXge//VYofsYNnSnk3AxNvd4jGG/sALhPgh/M2pwZYMVWM2BGy/065EqctbJ
dJKaOSPD3n50E9rTfrBc2NyQLgYcJeWGl/YDNj908lZSgIWVpN1ZAukQ9nQjoNlFTaLQhrcXa3i3
jvGx4JfbkSpo+gib34wVWr6ze9cFGUw+jhc9C+SPojk1YQ01WmhFHxgqiEHdElQyRaFBdgt+IsjR
RBELBYEFSPdCLtv6iIEdS/tT0ThUJbONL1gH7yBsTQ2wg0N86WCg5LEtUGk3Ik0IIkqRSPSuq3/1
X4INoLIH0ZeLBaCgBbEmTCD1xeGa1BNKl7RPL9H+mrZg+j23iwMJUUu/ZLYVujOQjVspxuAreUPn
WYxEKMFD0i87EzxpBAhn7He46whud+YGaITqEvNV12q+L5qMkss9L9/LvFH27AiX3Wp7Jcyc2n1N
YfbYXFqBSS3UpvucynFBn+xw7xcr/cs+U0GAEC9T8l6zrYxDnZN2ymLijDqohKgP2c9/dPc6TX0i
AI3iEioxFGOPcuAuPwDFAkMRuk/ktlDPtcTliL2Vtu+h7nFlSANJdnreQ6L8gKiXDeEHyjnDPnHD
qCDsSYRkOzv/+A8sjjVg4M+zs0A9KPsZqu3czmxEvo4I/k5NnbI0Jj5LPscdR1sxIy5doMJlNqmY
uUPEcvLgAUhdTCYL4rUUbmqi3WrbFPBvhzt6MmCRjGVMRDhVhYFWHvEq0p2wshhhWwxEARgvufoR
56DkNjDg9k/ktHkvc96x0mDITCXgBOMXect7DVg1eh6nhpii7GZ36YvJhfjXjoz+RJVgkiMZZZ2N
cAV/9gQLle/QJiR3U9Gdwi01tN74jv50PVrb1CbyBKIE8LMbbRlgvJIGikLvby/G5SO2yW9EhkEM
Z9sbHA7f3db8fOrd5BbF1iGdzQDTd28OapBFweor3ba7BtNOlS1YhNabKtr2baX+W+UJpojNHyIH
3gRBp1IXw4EarECjap1oKw9mT3geJRcSsc9g4nMguaYmaQ6eNvMQR6bgJZVVb5irz+9vW3/iMiCK
WP/1S+Y4RwKHEobQoZVilxid0SwWiBw9V1uLwR4FPX2CvyKqhsHfLgUJKql6K+vL7Ty9m9WBntTb
0055OuGsMOUeGeEUEkDN5ZVq8OUyhX/g6s7CFJd44+hv4eDJ5yF9lYtOFsG9y8BPJSj7Q7RpJfIA
3OUCwOEF5Vn+Ldkfzh16VnnEJi12Dbl+L5GZZSGtZUDajc46dnxQAIbSm7AZ6WLNH9dJobETo7dN
gC47wByW1Df+WHwF3Ki3JmHVUIPKevKukRgUV4cqOvICBBdJfcSCkZQHsmo1hEPqZsBOWnrIq4vQ
cWjysDcG/Pi4oZrEBp/gCVqt3Akgiq9nXIH6xV8lnYjNjL7btePkBvaqJy6owVxk/7EnhsvhmOyh
O9EfjadtyZCLHwI88OmKh29mhe2s55PpzwkKqwzq99sy7JeCPUH0qCPoJwT95tAW4y8dhiy8Ni2s
0QSnE8zFKawQsi56eDaQ+zY/xHLFzde5XBn4zm2I5UuFYR3RC0p78S5i2r4KjxesTVEOoF82DGj+
M7aEOuKKVF0MpaMdlbkFFxLc2EHfOp3p4gn0Qxs4rluD8iFICIG6vhsM5Tl+iA65eWd2btfPnfoM
LT2DZocOMApjfo1knypi116tm7vSpHsdNZhZbxHoFv3laBrmosdV75ILV4tHRYOHVi4I2Jm4JaLJ
EHuLN5Qzq0jMklGCgQsJdV1rBcfG7Pg+LbJFU7zoIDSjTEqzGWkaKZ9Bo0Uv+kvXPD4uTWXyP+yl
O1p3eNQjXHksu3BQS4/NejWzeiFJE0TlpgvpVm412J7CBbpVXlV6Rpuox/gZRa4kV+zGf9GCic6z
e5xY8FIcJz1ggwT0uGm7RBIT0Zq9GEqMGEjLMGvMmsNZ0A4NRxtnMmnpEGq4t6fNalQ+VzZCp1At
HmIhdcgI1ZiNQdAXhqNW2aRcQGElfIYGcTxE3Ak1Ol6WtB8hoaBIFkaeGq3k8/d7iSl3otb4QBzQ
7HuSjg+p6jYeZnNOWnY0qci5hL2gQaYkjOF2UItel2DHHNZcfA+qEW/zQL6jn/CNqPatJx7qQ81o
WVG1PCSXkaOoMnFJFzcaMq/43PEOLuZdrpobvHsGHH1fcdg9ussryG38jeVeY9z52hroxUL92ZVE
YZoDYhb4iHVBnmZKRQ0NAJsPdlfkhj+nwyIWlVys8nbMjzjQYeVibrBGmomLYPSccmwTi5B/6L35
B8JbFNaJwL0GjlPvxDE3b3Uq2ht31Acssz34KInRVeh5kklNs5rvlUkix6zgD2etd/9XWnUZlmO/
Og3QhCkLiz7HHOTZvDmPjewD2n5rzJUcKduW1xWUFCmYKUJgFibJdaKRIFZHny4As3FmgVueNMQ4
5ZVcOgLE+U5lySbO5rWs2WDGeVH+lHhjpx4RYrvIapYSkxW7LJj8zTYAwR6ayhXVgJI8ThMg/99U
+d5tnQxQYGJitJrCfZiKCqGrLHyLWHBbi49+qhPUfrxuNMf1myuye9IxDy4bhwac5+n//sPIzQ38
7ZHVACq5VK+W65HE7jgWKM2iGEcsqH7cdatPqy5IXgi1abOj+YybH+heHm4h8BxtSjfkjQkmkOIx
Ro2KPK9OG5gzvUmLfEQmg9ZqY4c+ZOx03L10CGHWmDDIU6MpKlw=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
