// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sun Nov 21 00:46:23 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/alshma0b/Desktop/class_project/class_project.gen/sources_1/ip/egg_mem/egg_mem_sim_netlist.v
// Design      : egg_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "egg_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module egg_mem
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [10:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [10:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [10:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [10:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "11" *) 
  (* C_ADDRB_WIDTH = "11" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "1" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.35235 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "egg_mem.mem" *) 
  (* C_INIT_FILE_NAME = "egg_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1152" *) 
  (* C_READ_DEPTH_B = "1152" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1152" *) 
  (* C_WRITE_DEPTH_B = "1152" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  egg_mem_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[10:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[10:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 26288)
`pragma protect data_block
rl9en4LIVh/hrNk3R5boDzLE5hKj8jq6bHIp6PtyF05+FYe55Qblaeew9dtYWKO905cDnkSR4gLf
NjkXjZX6J6MWHDsd7mBsLNKEbOWpVTrJzkW1plzK3be0P8qRO6zyYDFCPmap2amTdvyDJkpQWYTT
bTrAjNWG93UPbMbqgHdi4w4cqw3ripL4Ev7miRMsnGY8kG+nj5tgQYyYqm1pWEXO+OvF6jLJOn1j
CeOWiIEyU5rAHXKzKYmBwE/nbiBm/aMA7Lon0mr9IiBsRypsYAvNm6sp1JBPhtUdUEsAMLE1u0SL
WtJoccRN8fT0h4lsZj1r+VjWcYBx21jtwLx0/U5GspWARqtz9kduTzTSdFxe6a3d36+lttyvNGIJ
zz3kyMDtOOXKr5AKgVWKCvcGNJSZ4ZrDovOB4XFCAn1ZO2ol10pcE++q0xZQfHHXJZxEIeSiMWwM
pp0G7NLNjDrC2+ek/x9PJjVaXNk+BHPzI/OfbEDCQA3wO3swefDf3TGukq/qN8tpX9CVXc3L5TIw
9ZDQ8AWck6YKFURTxVuaAkJL7+qa2QleTmEAERkAiTWJQWOoS4PmVZtVa5HAxSEt+XC86VmFjaLc
8kU1d8F7c59N+VBfqcPRMQclPoRp1G2Z80Y5qOuuMen4yY8Jj7PzfaJH6vZYMDhpXb+WPutaQbRi
/OEzOTb1u70HRa9lO7djI86crKCcdmx+YvwKBDoLKshZ/YPjVukcHtax9Czq3kU6SqJWjeXIUA4g
dghmzqCXCWG+cLWVSEVYPYb0Ocw9dcxGmISeq7/3CY8PBholKR08YoRfrgmzkdFiFkwclTlns+Pv
ha+Y9k4y6k+/ctciOyHT8OpzJuz4duOfJIyrb+7wiHuIfqdj4cL85PLUaKzN4mPY6AK2VdO8GFvC
XlGE1Y9uyAOEM0Pje07Z2oKkzQL2ETYZ/o7xhwRmDCp1aZ4B5nLnfFE04O+8ExfmezRtegovOlLY
FOKmwlyuRiqmWRtZ2Tt5hnQgzEayfe42QcH9mnBiRaspVFxmxaBFHwJfX9dl4Eap9snTMxeBCSrL
O0t3YpSXrR+0eto6Oly5swBLCnwNqvULKtE/tVWiHCFqk4S5gXOU66k+SxjWt3M/NatGaH6BzQO/
u5WUoKacHmqvknxpfWtfTdjIS46uaNGvf4sLFz81QSdDDD+nr97L8TgvOVv2GkukUCQw3oS+njMD
t5Ltgpy6W1Us/ZHBBYfgHnPdDWvwXlsBGXOgzDKbbCGsmYuFBXPPUAHfxcg5VQ/Mr9pbzegStGho
VqNq8EpueItrzlxFxxj8/o+EHVma5RJXPOrTCfF0ms+Kv70BlLIAMXHWpeXaV0zzN7/sdf3qU2lV
2IQOtI8/DGssyHeLABwXc6aHc+FWmdtMe4w/qgQIfPyu4z23mQ3/nGUeTrhX+Kbfr2VSAEz/s9Cj
3KJtpnmUOEIUKyFC1FiqDqqBDf5C6Jsu9WALgT4o6LuyuAO3Ck0LTg/TXAyG53rx0yMwleeNsJGl
n5E4ziDcVOWO588duZfKBsOw+46an4gtkkWInKG97ToXcU3mIsivx3OYVe7yCgLVE9jkO16d7rl5
5WgK4Sub/orDxKWonAWHIUrrch//XwQTe6Cm23LjnvTb5guSUuN/FUO8kKN1gyCHp07Pxf7fFeyZ
hGaZZ7kuFhlhY1WPjTST/FJufOsM/nCU3bnYd9f+5Bq5SFSRAPeJ4EiYOPeObd3mxcjk86v4xSpZ
s+r8TdAVbEp67WzpzI0sRSY6ZlMUYcu4906lc4+Oih7/uhoottXy+9PcWQ0umckp5LeYuNOBrH2l
5tlAzvLJl3k4RxpKMYbZszNGi6AN36QgZkUcfomTHVcv3a+k6l5UZvcCle/NVmk1QmoH8Kl1ABUF
0rRUEwLLt+MbBQpmxahXOdaPz2nF9K6s7ZZffXAGenxEM6JW2gJ1bDr/07+CaZFyjtFojkZzLfDh
kZV/L2Hj8hgxFyjLknoRva/j0KRoMJp2koPA0dE7Q1oYfQAaxv/dJkleBPvgd2Ku3F6hEodx34Dg
PzF/fee0xyXC5bHUTSJI8Rw+nYImaK/PqoEZ6OZla4CCEthn+FFcnFwViocJuqPyJgzgzvrewh/s
670SIa1dEsRNHjcyaDlXXNrMiksFpD81/eUkio3tJ+Hx8XAWBQNxU1UH0XzLinutn2pA4BvsUbJ0
pOT8RBa9PB548UYO3sPxWy9t1wRz9Ss57VqUDdrFJ78tlvto+H4FNGjaFLjuXCrYuskRC7i9DhzW
K8+cEAGZ8W8HfhNdnGXXaNOoY8xyE/oxndyFBXNHizKj8n9GdKC06iruJh96ld3hETnVpXw46R84
UEEQDlZJejJ7vtq1LKvIrmdtcB52bAY2UWdMQ6bOpwexZBcLTcq3Oj04fccAGYOXCwViFer5JmZz
uoGJ5jqd5kBykdX0+4JJPMojYt35/+X2D/9STVynKlYSVYZfTFHs9eTvQZqynesba7E9NDukNwQd
YFhzUkrLGPYRv+1mNMJTf6/PLTNnPCYZ0SPcG+WO8hXwxYbsvxs4XKtyWBtv0aPMp8D93SWtu1ml
DcFDLjWOv/a+X82mYDIJTltZlHphfYRrZYxhWEuz/IjHSRazn9VtgmPDbTCAiERT27XtunfXh8Dk
9FN0kbBLxw2KibmjYbWc+4LaCV6U6ewj7U/oGeGRXvhTAs6+6l1aknCMuLruRV20kqt4DisdzIcV
m8PeG88LiMKs3y5R0y3N64HqztrCJDvj445xAZBJvCosK0UDKYu0fULN3+WanUoBAQYo8fW4/52+
QW4r73R3+vOOTNfq8/mvTcOnpvmbfHNQ6buPtu+P0Mg2swOVQ1mCaF/K9dbGzHN/U5B3ooYdFKTt
0XYjucMS4GH8Ci2V27sB9VH3oO6ptcQ8/0ohjEaVXlVtRvIwrW5efjC245eWHHP6yuAOAMdaRNc6
B5RoI9HrRqaQ7/6sHN25JLJJrRWTn9J7rPUsSnvcTscqHAmskAgUsgzl/NV1Xf/R9y4FJN1f0K9b
P4mLTtVlugM0IvtmU1gtgJZXWldG7AHykc3Yi79kdJbAFOkPB+qCZPjstD3eG8LmKk5LrahqTvvG
ABuJ9dyOYmkQM6nVkz9fBsoVmYx/eRdGgowHKTUw3c8fGY2GJTahiglcHso9uZsBp5hVrEGAv9u/
tIC28MAnuPKhuX0G/530ng5XZsPjc8g8/wHu59/QohqJQ1aw6V6KPAIy40/JvqHfvsJwLwDAib2c
gDGa5GXS/JMTl/iqTAG1v3gVY+nDaofj5GOKOo+kocdiCDjpZmAMCmQgX6XvQ0JRcGeGivT7fNx4
1nNBsHMvj5zYhLZ7Z622WeiCACFhJT3R4h9faGphS2xVe1KKU2mLpZbiE7cZpTY7hS2t8BUNt8bG
AoLobXPU+H1m6JT+CQxfZDe3sRhcRwkUw+uExR4a3IpDyfpL5/RONJSw1273sWms8KMCUJRy+jCd
aVgIAjfS++9I66pxtmU1qzFUFLcBETeYm63PfyiXLXJemXi8sC8vVA6nOEe2xNswGkU1L9/KDr9b
OejfDOzui2htDprWfhunXnP10US/M+b8CQrUVMOvdzTS6NlkwAuSfJOAdTcXEDd66wm3e8vSTwRr
8zeYIbhfQT60GIat7VwxvUpM410Dm5CcrhvpYeVMLB/XKggVErpNHTYR4DWW3c6qNH0sbhi59Tcd
fBzv89/6JGT7azDLRLlAPwdsD1SarICgdVndxUtxRmIlEuszSb7ZpmqujhlPg0KVAQzuqRRaPX0L
s09KS6fK/o0e7EiLeP+tg9P/5/DsEAdKR+0H9faMebHwzkeqo0lrYieU+SJjQIccCZgJDeCkpq1h
AwzB5hcsrEoMI5GTWt59p4zOeBEqtrhAJrMjDukYYk9RVPeynjwHXoCsjQI/clX8Tm8d/p8UU+pz
9j/EE4duWTia9m2+d5Max1EqgzHvshU75Vj3CeVDHa7ZC7HXqSebebTP01M31U7+w1sebrolTZWL
rIG8P96egR9SWG2ew+yCWpajWNP931IaA01jLYC8emys1G6Sel7BPsBB1hIxcYsSVaJYgolW3oAp
FMA5xWKRto16m/ARK0NXvw5Q+wjasczCagKVYAqT2am+dPhUjZo7V8SopHRRem5Gg8I3S/O3vKiQ
T10slyIYoC3ylppm4kgnqrtb2was7qdNszFu3bYgzmGaAR+DM+9CpLrUNeD5VsSyMdK0hiY7RweP
mgLoCACCMu3JuyeJpFhkaDDqwkyM1GRAZIrO1cR/2ZuNs6WHgrFIoqty7M++CHjM38yVir1geWrj
Bh8+TB2qE3bnZkxoFWZYBHENdeTkKmWv1VMS+CT1IlSpli1APm/in1xDGfqulw+mpo4s2nA8zPsz
PXDrlRcxXtZLxJYSHJFIcFTReVBOpShqerQi2Lfic0cxXtCPcpYd+uqWaP4caVkHosNfTZZonUXS
5WkFqAwifnlnhO+ey4CSELaareKPlJNHN57C9h8mgXtmkXBgrM5nBdLU5pX8GzrnQ3Q4DpLGOp8e
tu+YDbcpxt40vOcBrjpmAjCtvWlAyz+dKzo16BTl6QSolJ0vvyf3eVsk3FV2C59f+aKaRMtw6s9Y
TNEfqkBdq2usqodi4xnQ6XYUjo9/koAFOeYXMrd/SV1EciV6GHh+Uv+WP2UT2R2OC1ivkm50nn53
XjzeeZyh8N7b35QZ4Cxq2bO6LQusM6jQP022Z6kvS8X9Z2xA5huZ28Sk0ZSPRYfkh4PG7ahl19yY
ZL7pO2weHjf4vEW6MZtr4ZR9+YH4HWNBkYTgtTAozdFmESkurKKZVGsmcdK1eWiD8yhwptllCTNK
lwZDnJvqYgCwrB3kVuUCm62x0+rve58tcp3xEfnq5ErqkosPsbPQUrIBoUU3MIKsWTx9dARByZLR
vJbUGDt/R1Kx1tWPsxhny/grxLuygup8n2fhKrENVz76RRMOjPw+GuYTzqKDG/3rEhfPIlvJvHDN
v6d9vvL/pkpJvTGhIeTtKEoNt94M1kbavQVdI+Yk9CN0b3hWFaFyMhAVKZyvw2X2ZPKtSSoyd6gg
wiIXKj04SgaYqiTjZ0akzMNAi5qN5vmuDzfgZItwc8rBqUf0TRc5L+byXMB99oy7K1X8eJiOmkui
44SetpoIvtb24N8aROKp5lCsmk10CGXQlus8tFHz+wZDoCiHVTlGM0sUCj+e71SgxbR2Y7ZIGpsw
94lKj96dEKcVviPH8se7/6htHvjpz83pWvBihv2nPqvRRyEFTlLHeZitp0YWkupDlM7zDJj+YrCj
eFIKwjF/uSLu+UB15Rw6lNl9DxiIfFeu3gYBATwQ9g4DvFkwdF/x+G9g/jIexcfHprnOtO7v8C71
N1q1PYvdqKV5EvOj3fUcxQIt0MtTfOciaNq7M7VpdCe2UQtkCtkEtiYOMdYf9atdoZHn/yyipl9E
g/1jO67nBxZZriYdwK2FPGw1BVN8TxrMtQqR1NsdXIlLsYvT4fOLCHnurQE/jFYNWfPqQ8md9tQt
5kGFaxnF/nEh+L9O0Pc+maQjn5ggTRzxcyPaSBPFgXdwa1QuaTEpBxaXIwlGYAVNtwD3MPCA8MSu
qwlgLSUKEc0hmSDNpAoqM+nuK57UIJJrUSZoGztk0i6xQViyR3AH2LgxvPj8Tu+sGHeX4wAZMqaW
039sZXtHyd/p/nIU1x/tX8nTSnwG4WkfMNbmKnziIwzaOeR60P3YU2C//2WxJ6AO3VH9UZ0zOG6z
kFrdI5ZcmTh+m8+YgEexLSagiJ5OqJBZyZGyNfpeqYkzd07rj71kxXS/19B1oq2pvKlVkBVegljm
IeSm5YA/UX0DWSw/2wHNn5i0RH5GzYPrVvcTlJljpEBBEUaI62ROTPFASPSTZ/uRK7q6vrAnmte1
gzy9Xfbxo5z+o3rdFjF2pEngt1lXUMD0k8CZ6Oj1rrVahr0DwXrtV1huQRIiyHunxz+ZMIpXRoV9
DCRJJmKTt8PRHZF9ZUd7nkpDzfHBv9b3xBlAVc8QvXx/9XWuEMgLUHHMx9JqHyhKblblgPIsS6WS
yDFltW+t/fyJ+57yPM41xkEmISa1bVQCB8IIZNmQpWYg9975taDrmOV1Q24LAbHmtaG1Wypp0skq
IDTzAIgl2f/LhB0Mir+HCDnkyeZMMf7idmL5h7YEMCkILXV+rgCDdPneprhRUReYc4ch0EsvnZDa
Oh4iR5hHALwLYPx1t8dez1kGvvDBmb021+y+l9sAj1ffiQYeNBn5HBNU/Yi/RZAwKShux458Vo9V
4O8WD1bHMssawkwEq3SuBeQFM4Ms78P5LPKR9aMl5KsrG7sk20TbP23rfouAHOKQ3sOifcAQDSLa
OVifbrZVlhUkkaq+oqIn6Oe0y8bsKq+AXkis127VsEk/KELUg7dgQ7oa8yQV1/8TPTIg520sr4FV
6i36RLDqFPAsTa9fD3n2EneJpNLXlkh8LuykyqKCYavwMv0YTqVTWpuiFqJBhQ4Eov2IgDrZwzAq
9M3Z30TGjzHu2ds6/hVnNvys25qfRWJ2qqfsbqb+UPIAmyTyMVwJ645nRB/k8jYXNJN8SdYxi00O
Yj5VTsddhXGrlJ81kiwVeIsLZ1RDVhd6oZj+0y74GOZ9lDio3B4Wz7bi7dqSY29cUcXyjG1gn9sV
rHhQHNg1kmaLs7xZYW1wfOkizposQ/nfPh0uaW/A1v1dMLkeYJ1mSZJixunkGHX3qyXT/QHH10gZ
FNN4JnKM3DfqlA+zTW7EBHE+xG/Xjkp8933YT5+pal2b1DNTITZ1Yfn4Mnoma7PNy4Q3cd695sZq
gL7v0VS7MZcbKbU8UmgMZxIUg67JtRgMqchrL0eEjwQ8pRfWzcV8xty8zmQI4Qg8bkJO44uoFFpf
EYDE18P+W5ses4IulD91CDrUr3W8JNCq8gjB5I6Cq9ulMJzr56dY3wKTkkRbWgkQre41P8EhK35r
4A2Tvb08EQYjv/jsaTX/KYUq/hS6SnWgrXwj68ah6Pnwdxc6UAGZ+gNOJa2Mn/kMXo3tl8tPpW3C
u5qWRZaok42uBE7BT1tmnMSyrpOwZWtK9kU+n9YTHK8UFJidf62oqBJLgZyoLaAZpx30YZLCNLUL
8TI4xoXlIp9T9MSRKaXxVj6jJPGRy5e1ikG157qsxux3NdnucPK5QEanjXzH/iEBPWqGo/kLVtT9
O+unfhSOmrN1+8DQEwCtFX9gSaZx1o7sgjgQsqKFm0gI4dT3+RJfhjHiH7B+odQArvq9ZnAlSyFs
1ARi9AzbJRsWP0rtu6KFg+ChLgQT1Z4OLgxviU+CPR9HZKXxnWQfR0c2q797Gryy+82IVl9/JpL/
VZFME9vm6K95WQSzGlqMV+A8DHfV9Ns/YZb1mG0/JyhAWcRKm1VRFNM7bwhCGxoNhyzAssSV9Bj1
VW1gNObaRibSs9jNyazDwZaEYW40YCCTJDoeTIUTnZVSi26uICt/YimJ3S9dgvLosqDoT9jIvfs5
TPpWUnBZ7G43jlSiQzuiMR53mZRQmfg3QFHuok/w6AbqpRYFH0Jpz1ksVAw8l9ltgAP6t5Zdng5Y
lPlnm3nGACjAa8IXEx8OOUcUYHrwyt5c1IWd1hJsEJVmlTA5lKEY3lLOJVAo4SzC2ZkcVE/TMsXY
jargDWIfnvSsyWaGAZpskKgibGOZMC6DqSbyZgQU0UQaMbqyoPJ+GQJT23s305ho0U7C6XHJC7Iq
uU5IgX5ijSB2HjUPA9gcwQbDGkg4u2PAb1o3BKM3J9n4s7575MZbaU7dmXiwabh4lPQOw6/CcVO1
n8pgC2NsAGEmCWbwNEcvRZvYtonm4LEO4BYPpA2o9MG587IOIOXshNyZQ7g3Y+b0bCtQqYUVKvql
fQz7JcFgTxtB9R3p+rfYdVkxqnAXkhMYismcFvzubZjTB2+DpTrqvvhlcO6Kjn7M3eLf/A7JKcRU
dpaPyvzE8E9ZbYFbuySrgoDiUAVSXSygZjfIh3Yb/EA5jVastBvInoFTVQAM1xvlvA/e4h298G5q
bzOpDSdiGRyXLl89QT9GwVIU47g3lOFQ6CJOGDCaQOoNAcK/2Bd5I5x2g/o5iHRV2vZukuGGe4N2
FRkgrMWELnOveId4E0f5FXmlsMlH+TZcU3vbA9C7cPFKDQO2mT3Fla+EDcrg79DW88t8tf+4kxrn
eAQxMOIN5gmQS7pDzySBDd9cLOttEa8LsgWmcojYGYeBro43B7iLBZbUeXAeBvrYjZaC5P0uWcKI
gkOhPgh8luBJHA+n/KQVZv1OhwFvvkQwog8cFE10eDkeE2ZpGtxfbTsvmevBpOzd8PyDK7quQBrX
NN/dA/yFWGk2FDi+FWCrcUoU9ZzSPXZFuaTRjlrj7HdOi0CmAXPAmOfAgxf3TXXGFMkrP86j7TD3
ahYP0h2NWEne6gj4Mb0Wuhw/QqRlYZThNJEYOTEW/1jDzqV5QctJ0gVDm/9HM5/9sovSXKAjiW8T
ArxObYvH9pC5mFrhAurYquOAU6ctQTzCrk5GKw+8xgDYUUPNymDLk90AObXiNLoaM41B/kENtZwL
b1WjZ6+9jGFCVgTSBVSXKxpzmjGFy1eicHY85TO5T0Qt4f6RpIkOZjMQ+U7d4OKNyn1C1//3cakZ
tZy4V1gnklN3NLF4tATFul8MHWimub+vw8A3lGjHA4OxPIxjjeRkbB7UlVUezzOU5hInKnBFAQ73
3jwLfpRQ0ONUNvnP/UQhf7ZT/l1nPtgjeoWllJU5lpMCXQPv9k86EdfaOZtX7l04eiEWJY8xhe6x
Zo1ixelsbUX/VBvbGGemwU888Mdi8hBprR56aJFTcfn55RyZGKcRsrAsDrM++TnwBGvsxf3/LLF1
q+SiYFSA5MQsmvvGDopFuhRz2bt4GJKnUo5bl461NuTu1HGJbzyxXOrjxFESFnCIbwyf5ZigBRYw
7CEWkcxtGbHpxNhT/Qa9KzmU3yJR+r2fRX+5T3W9cI8C0yzLY+gK5RK9zFzSYpyAW+Uan53QqV7z
EFf7wLENfXGm6khlLOgufEIVaa2+eHCjXfpPGV5h1DWDUXZYbb6CtQDwE4BrEbv8sNBlqRhYT6p1
cGhh9I4E7FKoxXs31svzyrFJuL2Z5fYNyihSelww8R3z187pkRdRnSzi1G1wdbF3LOdJd0xrqAYJ
P8BubJ1+596lKS0zeO0C+V8ZwG++7fAaT9tMLTuzVGVEtdco7xOrvoyAcBLUZxqwXLovgnRf0ZWU
DHN/aUQBwsrwfXprgxyyrXjVELyqnQB19Z0U3/DcQsuk10tzSQjhbaQfV0U7EUbFcTXtDCOEhLgL
VXlMlW+etr3p/3/kh4IBHSBlVyS2AMv4dK5vIw/AOpBQ0xVI9wZ17OMckpZpzioFKHY3qkFnVWx2
lkzPtjxRR/sp6II3uWaGIg+P9JtOI6TA+DBF1HMJgTDmy0LKo+m6SeV9FAEU22KbGarJJsuTkGAY
bYif+ptkoIMO93c20kBzLGmcm+OKD9QYpg9VMzHEjiQhxvW0PWZx+W0IFXck489wZa4SWhtmd3d6
ZMDTW6HtOwqi4FjMSkfoa9tdxnZ+AlDPdjjiZs4gIJFbXThFf3d3xHSL6KQW4h3Eb8H/KD2IhYHH
niAteHlWUHdOFTRDwyKVzxg45VBDVqEtpCaUWESZLEeWrX4nFhQYy9tWHHS2peqrmEFgM8xaTJnT
rLZRPLxXBLw75U2e06g0DhHkPIHkNR437vEjOY7qMgdo50sDUKAWazMlbsa46rFSQM1QsefoCo8w
jtpHFQC08jMma/8d1K/v3JBvwjV3LvAvOWDymhYz775RDdHT94AAdxBH4TX+uPVGXo5TCZLUkoXI
aWfRIhdgdZ0KQCcuoD7zRY/Jnm3igWiCkp9nF7C6lzAdxWUI1EdGIQ4ozCH8lTZCjdbI3DUsV+GU
PBIfCddqKKA5UA9pV+TWk0S1lXXaQcYXThqolgxcVs3zv+Z5qrZ/zAVFRGSGxtiztdew7LBflEMr
3TjFvsWWHuqLrGq62ERwmN5+G2WyTAcFY8ne7eXk0vszO5SdvjcnkWFMUPAkpEGQvMzCflDagXF9
HXrBzS8VIo27EI4EHc3KimGZdS3uGsfpSzgpQfY5hUYOLyIqgHZEAjy7w+0+BtNQR9eM4koBpLUY
J3fUR/mpWUxXBGP0tfnz6Zu0VqnSY9qhbitH31pN4EmTjVKswdS7gJNOoyTSO6wEw1xRMNM6/MdY
XWsMO7LhDKjv9M823y93c0uFbnEOEore1BALmve/34B+wtJDwYUUPSdAv1q4PAgvRmvKkSzlw8gA
n7Do8ebFpsBkg1bbBFXngfc5CwU2WFb5MTlZ2wr9LikV2tgi8jLduv7xmwcDp0jhuYA+phardCGd
QfH8SWhUQV7NXioecj1MBdayT/n7ibmfttMx6XMDbcUJaugdHTxKzoIAwH5GFfjWb2Q59IVBsekT
VseTwYvqWbd41CSPhgK9a2EYZ03UD5fjH8RHgJcN8W95dCc8z55YbSQITzE+tzGvYwgB7oanAkO+
URoFgKLNSmjGMfxxCiDGJnvX7wKjOopSrHFj88TGqubkxnqmspllQdtd1E0P3HomHOaCsoUGEkUI
NDjk5E6ddudqXBShdPWmRugn69cOEDq1uwy64T6ovo+6DfCN8bvzG1krgrReWl+xCKlX1AI/CG2q
cSmio+Dz+zCKoaZbTMbtqrPMQ4XdrroDJVwqsK1atsCl7ORuGuCGZNvb+vUfEBPCOxpPTDOT9E2s
mcs0iY+pTGqtjjX6HFZTcOl5+zsCf75OqcjmRM8H7axnWJhlEMM2B7oeH+GhinKD2haTUVUdbBUx
0e4g3ydGfANUJlxUIXwhYqOPj9cwBS/LrtjDQN3oXJ6yWnQZg4aeijXwEAmbY5c5lozbs6eZoSqR
gpEoTq2XGzxi5J9jNeQctCezr6DF79XIq8lnoYs9pEluaHeu2vKakzrenTXjbqWWKnjon5kbl0zY
sTnCihlCqFIIM4Dn7E6V07x4Ue9xVQyASGfupRjozB7VzacQNTTG3qTijYj9ZhYJMGb5wN0BqRDn
46+RDCs5gB7vZ1xuRvmtWh4hubYukAoVHYjcRUKymPy4JZuqlbqheDE2VadVTgo91HzgVS6iv7Q6
hK7sROQHpunboJNb/pS7DsfCOFWtwmA8JrChB8Iosho2ovZeEsJfTGWXWl3Y99B+X+3+jEVfaVm/
ST3xzMFzro1K3kSAcZ+J6fBJg1YW0pM2jKz2ijzrznZ5LZtIRdfHIq7ar1/LZP25jzmdiDfvWIPZ
6xJH8bNdJXUeeZ7WN8uAbj9kZ0WRc8lt+h+IE0lZSfCqWlBdNnqC8uM+5GLpSLjt2KbnW9uyxVcn
mCY11fXA2yYxiHazdFYTKiOabe5yfqIQvtsHvNoaFW0gCRpKRpJHNMSaRwopAZ3K2ubo68bYYdHk
JCUgWbGQmRJOav4X9cUiyc4vfg3o+wbLOyopNc1BHYJfPM9oDDuoKshN8L5r3Slx+ouaaqyZSQUt
cpPZ7NBEHT7s5x4gSoqeJ9ATwCQvTPNlqE4oTpIizNi08vrivE/lnxi2VaXfUU0IQ0r+1tG5rFTa
C6cq1GE1U+Kz8tWFZeY8YVnd1elJMLJcXBsr+8VUcw0bydcufLFzpPEtFxzq8t+FcrT1TXGPm5XF
5pbwDI7p18xn0xPYoZ5AA9XynPxSdfIHWUjcNNzM3/FLrIjprxx5sLnoG/yy67NPhEyXzkvrLCZU
RMZdss2YBpDopcGRdDQZF312hewOPg6bRlq69tcqklvJL9xqPApPfTJReGvV1TCBenkpyQeuT4Tc
1P/2PkxxjGPKTbOVHk2WLmO0ke7K6l/i0j+xm0D42if1SqZzlBFyqi6/dWqmM0dHX4w1hi8H5BB4
0ccGbaXWucL08ZohcDoFq6WNSOO0sGNGxLx36/ytJZsKTS+QhVfZ9Qww7E54TIsHQvpvnNzdmXpM
+ChNLZKDdTvVsmFbSRV3aQ/E5TcjOwtzewu1pKCeXyBVlogpSaIRkkh3UQuajbv/8gUMsvnlB0UU
Hkq1XiR1JdMz4gox8vqHiBBoGEMBafJyiTGH/k885YiTvOvItcfdDOK4ok/Sii4HjuhX+kijPlK3
BD6/jI6zkWwR/d5AAwYlnnoutDbc3FdbZvwMJJuCxK+8LcoCFuuN8FDXTXzTewFvcyuhLOo/nFx5
TFoIvRO19PoiLHkO12JHAHBlMappG7+VzOBfIZ1e0sSEyw+gACdiZeZttrC8H45bDhVlf0nLC1uM
vN2tkP9xSTX3ZhFHlJnQ3lWbF1GsNBjX/y0eJ3WZl3pjXvA3l7PLIiRNzef8nwj5EUPiZHc/Lh/X
kA7ZgYWOcwz/gPp5pdUwKVu+cA6ldQrefq6tSxOnaFIg5JbvIEhItlbmQZTYbo3wG+moN+9fD9Hm
JD9BhXncxvXmsmHZ/S+4cBTODKwKpQ4ICoCU/hU7lyY4YTSxgmaAp0MKzwqPjzwIXRwuw4MBxDy8
YRlKAExk+HScPI1bolvtxjVSvPEDZj+bsoGvjfyM43qlG5ttBddAjWj8Ny4pOJdt9SnFT4V68jyz
EaWm/hGAFuQ3xgnuFp++OjWi9t55GMjE7fGe8zGvEIgQQOsoNEnqg0XV8/1JAe61Nw7UXekwv3D/
iQ9/EA5oidUQ/27yHHAfcr55kzfqspfWn6fyDRSUz5dErrBfRHimAWEUpKe9U4NwEj+VSvos7/DC
9DxXGLJdMy2U6VhDLtS6iaOUlw4wxC/heTyd5ZP3RsWDtIbl+BmtCIAFOeMAn/kkXWJ8fJp91xB/
mZqqudzq5M4w8tDBoMpWEuxgRoBZiKbR3OR5nxk35OqoBnGjSYseXn7JPenhtND2T/QGrCvstQby
d+bj+ke1Mj7sm7w6LWg4N3uJe19I/usTGE/lyicPfT/ffufeJlthYQ5YfceetBOV6pAiZHNltUiI
FXqs/n79LOzSAlaoyhgjocBu22nQdh14iw9KAYPWXlC8nt4K4EYAOxiTEpqgPadQcF35SsGEIglq
uniIw+WhSQ4P+0GLCBRYjkqiLZfmiXq5Ik77jiwZqea+YL2MY/mibwsACvDpC9bjvhsw9rAvPmze
OIHgWgjiIQ9Vj+x5hEuy26sictUb6eTjLXW6/ail2WVhsoI8DcWli/zKSw7oSUl2vxnStlL3e23n
lwSM9Yizn+BiDTkONqVVV6bQsHLiCwsVC4PeQonYyXFQCEKhcPTRhplM50b+VHYRanLp0DRgHdqF
BnnCqeNv01wwlkSvsH+PpOAhv9yBjttGlG/ICYaeB4Sa0Am/mzsOpFMAPBGunmjD1vaUVJX9NG+S
Sexi9nCezulF91bfP2btIgUUYBXhQyCpQ3hNC/h9ko3XEna3XQZDV4QkSW3azhGmkfHXXoH2gskU
XlzQSgXGQVpaojaIXa0SrxE+P9QD6WbfvrKugpNY8LNebSglWkHZB+uTY04lSrYQ7P/GY6AHnFdX
ITem0yxbtV0qb5aj8ehysUwGmeH64kWV2c30l5ozF8v/NH24E0hnBxsC+n6Nd26AoTaedVkb7cEQ
g9zN72JLVK0zEdBiSmCAmEIOH92NWiGtf4QL2SPtHZOC03y8LbiMYURTCW2X3zszkuyKbm40/nxF
pLOqVgyvzrYW5A8c4uvR/6gD5+pVTR07tcrN+ExOgTBU5mQnxjgjWGezm3xjlsPc5EL6eSPw/uBL
oi9qJQds1E50GoxApKgFDRygajYSjwhZj5276G8v/GMLCuNx+7J/sAuB6O3Fc4f/FRGQViddMdV+
XzUi+JCk0tq2uWUWX1D3L3IZECDb4OljhY9LEg5SYlqozttGBx2DZfUSffgcUa56DT3AxeAYf87U
iXpZBsCeFCcoiE4kZba4DwXuhm07+ISecmJUGwjiSZKXCFLW5QMwkWtgAtkiNDuXGUkn63y6pVRG
oKLxVM0qNbO8V6yAp3wSFBjzpkzmjjzaw9p2CfPLjhOdLt3VSSk6IkhF7jA1KxiuKvXD6Jl7wiVS
xOBtR9kxCd75QLlKsEHNTS7cy0uk/BDxQECmhb19b1TCM68f3xY28B/z2m6jWQXce6RzCBoO18Jp
aRSLUdvuWhXGHIVLj4qH2O3msVezYob8rOc0Zqs//5RnEppdZmDd1/2N3OD5ar6q+nG9BU12VmY0
Bjp7edgdsa668CUY5FoCAHEqHtALnh6SR+DNG7kQHuke3bJjia7wjAuF4B8g5RKQ4tNNKcQfpvlv
XKv6P+6i7jKoytLgoHXP6IKy4oi/vRqXZWsqvHyIFCO2AbL2omEDiDCuEBWmKrbv1X0VIZMg3noO
Gs0FiSXUvyCh9NELi/i6DkE6ZhOFjMFMRps9f6za/1uA1znhe+1SRBytS/qEKZoMiGzxsBG0zf8r
PDShQV3/mCFmifHJqhfHbvtnxv+aa96JfU/t9PMl66P77sm5IZ6tAjTO7JK8ZQQUWqXg3YiD9eV1
iY7+4+d8rd/rM776zN9afzIvJ9fbD6sCbHvnOwKnw4msP4H+lerCx3iVXbkxk21TTKJGdIr+aZW0
tHEGswkwlS1j4X9Ql7ZSrXEKQoJq5jFKJ+Q3q8omgjoytz1RtnuC61ZhSXoW8/oynlQHqyrLdU7H
Lyk/ev4T4EjBH6/u559bIIismK3AwGe8q5UwM7BvRCJAV4xaDiMMMowMQXgAhGz/UY/ftMTWtmkC
PUZ2sJW+aVB8Ilqau15LX9moBBNxGpOstTelj/30zGoQ4L9YhtbUUwuUdYlDdSJLsvZpDY8cDd73
PQWEUcbivKpDaJUR7ILZQNbIXQoe1zFxl9RhOaqgDmX3Zx2uSlYtEBQd6Flcvf6mZJ3arguI4uU3
JvXXH/XFBUUBBRH1oDTdqslZ1qOppC4pnI+X81JQb+dWYSz2rfSnYLPVfRxba/z+uqDxO12mr8/C
jvHd/GTvoB/2mbi0h3o9F4p3WtATQuIkRDF8BT/ZCKRNt7YiBEsT5fEETvR9O/U/aEnkTZCkiCFu
a7B31UeR2W9gTkl0gwPpNTaRY8jgwBkWP9nPsd7TwqIflABwpLf2vdeX7fUPGb7eLxSjvt6VoNRC
gcRYquXp/UWLEtWOWsrABaD5bOU/CvvWctMy8XlCSAYnDois2Cex7gnl9ztodqY7RBGlLCJTg9+G
hsgNP7dN4LQ4HxSwUhwNJi2KaO6EiJinhN/fnTW5AhJEWvDTp+U8m1C1OIDzQLD6zatnrUhiisG2
wVpxTLtqlQOxxXrZNg6iI58D6btN6k2sjypjwN4q9bcj7SLzZXej/BrffiS2hT3DxqGqrvG5ShpF
sXdNqF9y1KVUcCX+P6HXvPClTOehhQTmEw2lHXGbGxUrNHelWibn27YRhFeBOdOGDYSZFrTe52+W
v0CjZfON8CQGXkxvtJ+WEn0Dy6tTlL7jpM8ij3vjT07RRNdVqy3lSnMTUFFwZHeGD2qdHn61AujP
Z7Cm2w9VkNN7yYwqeLgX9W3JqSQpNb5pyPJUGLvzifEw4YwvgCqG7vWkxGV5zdcJW8lJ5GUasLd2
+sLhcIYgZDwjMjI0/2JX+jZcy6Sq1sRDJ6sV6ArMBca4q9gZUpxHChtIeRvOg++kw+NlcJlC6Hbs
GNNYLTemBw/ActlBehrKpUj2P1c1RPOywyclvPRF8mcRblfp8e4vkN3wwnc7UKFIcSo6RD5x9CXn
aXF8Huct2DUVM6Jf8R1HFwv4SUUHj9yySTIxeeEuo/Sn7QvaAZuGgr/KWM7b9v2hJHEaRgRjyD3r
r3vpKyKilGA7MkyaHL00MPxnvlGkgYcDguRF9dIZulxwMOCFTrdoqHwubIletoUniBhwZfg9VXSA
SLlxIHPLF8HQAG4GFeETzqtqwVkFunmJ2SIYKj5Wrr30dsgO8uh6XOandAVgZ1lmg7VRGT8vBV+e
+eA5PIfu/uUTuGgUNNMbRXIr+PT/GldXCC8kZT0tyr2DbKxH5ou9pitT5Txg1qsurb8txczxuarY
E0Vw13mfiUXLCpXlhPdg/34ehPYY/XI3rhAJr7HJmK7grlkhxub5g08EZ8vijhMS2X7OS8u9aotY
xgvY+jywRq6Cv2MKBRCvIRFvcvTsmPnfUCqsB5uu0dKUXT5k/d5LH+e9HSrv/XAx5wx222zvAOdx
NG7d581IDMDrqfxcCqPSp7JeKxg5JmAfGi4diWKX5HmxqRysoBGnlXCtgrzs9O9/pShtFAxIXc/A
DcEMAI7vGRNEiL9UxiyQIy7M0c2sqYuNRZMy8z5Fj8RmHP7JuyKfG0UQ8ewJhk3xgJUDLt3iSod4
bYnt9O4daztuFtyZ8WZu+BQG8Utz83JhGfSJnrTIW6qI+1r/r+vMVjwXrA3iFFfO8FfWqoqhuIRy
mzLPYkzh/7h7cMWj8SG/4jgLGhXbcuyXIF0ZPO5Csqfnt8jpafgioxk6zWL94/P/XEg3a2WaVWRM
FQ+/qV18/ljxe8i+QTFWyBFe1dXn6Zn9VmE0viwAXdDtj0lbqYnMeedjlEuZImCIyO1JNCEIw3pe
Tmcp1QmY5zkRtK4P4yBNK0RbWz7oS7rqeGy4hFF9XrMDO9Rgpsu0i6ers4TfMYeRL8ig5943XACh
U9fvFVte9aQbQLnMEMxI2vQpovI31L/Vp00CY+jwZn4ag5NoqT06KTMO8GP9WH1y0pLaSlUPnh3B
MdJN42oU7kh7U5SkN9Lik5MaqDT6WMWU65gwTjN4YRLca+Zt6Qqoy4Z/8VfRg3aiuEJNU+Y+AntU
QHqyWMKMWc8BYaU9p+Xzl3ocvf7JCKIw8fYKSDE3c/d6uFIQZe8pv+wrb78fiPvEHnH2D00PrXWW
21pcpu4XrNqRmZdd9NaUVs2XZ36Xtd5+huR6x2kC8WzK1PhJ6yYryO0XyhDNhKWJVdeD4+bUmnrV
wofk0xH/aWxnF3wQ+Lxfx62iNS04h0UVqtN9yo8xC/hzygqFuTBwIgdJxoK6d5AyalsdP9/1ctSr
wbeoPr9Sf5yJUvXT4ID0suaMm8vcBaKNEvTyyS/VKgLsIvp27KptD/NeldhrTRsMFi3b/CoLbyXP
iBgEbBVpLz09dh7t9uEX8or7jzIB5FUvRo88PcgwmF7Urbh5/hwLT2zxnseFPj8mCbROh1oVs7bo
LKmxcxC+p9TKe2QaIhgvy897lJjSuefk+8rU2J8e357UBbBpbDa7frtmEI+Uby14+kFn5s2SRlhn
Bf2PdxJyUjm2YPN5ml5h522pPHSfMXMrfXRr4ySxQAWS8MtfEYH+AN/oLWILLhJOdYHXi8h67xSk
DSgTlGMHu5nIY8PsRigVIok/ECeTECq9v9bpm0Cdo2Lb0Z3PECXFwcfe1G4/q/akemdntwalix4g
8ZHp7WOpnhZfjX0Aj5VvCru/7DizmktdMGY4qPpZrKDRT5Ox9Fe2tnqt0j3OTGJI+MZFSJY+4V0x
ymx2ZM9xyrvzWslJjvSqBYGD/lyMtHozQyAMFwHlYfY8ho2IUmAJWsMI2R0Agswq5IUeukI2bTtL
shV3crhwlIgp3rjOwBnRrpwRatNWoVjRr9joR8pWabnD+2MF5n5oB4O2qBCisN9qa62F0QV7NRJc
oy9nTudT9fM2eGUrHTtabvwAv91HFiq9gb/ls3CoIq736R2DuqQ7hebLtRBGgH5f5fxrxp4njnM3
M8qzvJOyMCKKvJd21ZvTj4pM5TVOhUzBC1ofmZurh+xUj4wQplH6w+24Qbp9jKF089+bx3XF2N2H
7tCHFS0N+Axnda9S23AWg9ISwl8lsVOvAp8khaQnGinL5SuYqrBlau+VJ8XxTlMOceCd0MYmd2Dp
JMGTil+nCx5bEMeCqM7FlZ5M7PbaakBz5JMBmQyTYl/g1RJY56eaPe+UhmAvqd9leHGc0YUE5dOv
Cn+44Wti5iBTRBEqflvsHut28ezhMMRWM/o7HocGQJh1hJ8/2DPXnh5lxOqohIbUCLdINNzDTOmc
sYDBqFo8KSc+hvsUWbhxX+idtjGR47RN2P6s+RWHMSpqMzWLbFmf5BhkV29JCRDjbVRl6Y2LjqS5
qZLxASyvDhIVSFIEXS+FnHJXqsCxjBuFyPw+9ytA/up5lVYLxEw9CH2W4pSiE0SUD2TAvtgRa2Bo
gUn2DBHJvxzRO86qsI/E6RhZr/a8v+feQ+0D6t4CKkqeNymC8lXgimUyR/wF+bYdhYMNZ4g1UGUx
nrM3QurI7FpbAG8uYNTT/SNAYBOqH4umEJIzfeDVZcYCcit2p7FETAcysrjDcG3tT2NcV6PsBKEY
EwDliXdgO2XWIKtWideDzbdHq1HZcbCKEWdR1VzgIvVb+sB+UIBDw8KTD+H8U2/l0VYL2egaEnM/
C3++oSKDajZC8FIe4UUJnqTpFO31D7iw0Cw1gnHyBAWx4d7+2EhMHCWc3TwFr4kK+67pi03EjBLZ
MdmqIKHbezowpsjktTgB/WYBXS/ynld/l4bYsXLm9Mz4oJV3SbnarB0SY/gdwlUnTE9OfxBZKbgU
aluzLKh+rTzwPfWTVjFpuMgnlTX05mkI9AHVDLdULNzlEbNluQteg95YwgXv0c1uu8kARw43BJFK
j4Fj71o2nTT24VI5cnpIMV+urj9hP8jSoYrxoBMUZgMDWI73LvTr8UClowhVJhTnuFhEwTTPB4tx
uietSJal9y30A02wxcJJ1rIXZO44qnV0w3RuEX0FuCkT96bRvv842avlVJWmUEuJBgcgpKtZbARo
QDnM8VJDPG9cBz2GBDZ7o93cs4N5IQS+++G2yX2Dm6uFEvu39nFyTEcxOsl7YLvo4PUgKlushAHh
sPFVEWBTfZhsVLupgf82UFguGrC3PIDXgS7W+Bo/OXreBNPnBaD06BRGzQIdy1PAJiUCs8sOuhgr
QxvzU3H1kj7DbUO00Thz7KX6GbrCBfGFaBpet34DOmmNQrnJ/u9KMICWRH7hg0ZK8lMKQAk12S7e
CJ/yy7DxUyy+/XWq7EOmZU0eHpvVJ9YsfJn9ZrBlM3MGDiX14s+Y26Fa4FypNsD5EGdcaceFZECC
LOmze82IGKX5WaJZ2bvpYGxdp1dL/PBtZiVx8GQvm6h21W8T0I+xhsForq6ftZ11fyp5wqG6yDBF
kQ0W8eRQNgZafW4r/pftgKvqBVy/CnKGgKQxfsp/qGsX8CbOH0Frvxpn/S/ZFjRlz50IE6F58KA4
TxGOuWah/EiKcDm6AWfLIf7f1WVi9op5ivykpk8ZYOLiar7tbyCJzElDoxDfkaYkrPvvSYqDItrU
5//cnNtuUSBMePaTiWXCFeTjNFbxNu6M/NbyvLQYZ0bmqUovLQZs7aoaqJToocyLbkAo/afLzJQP
q0Vns7XoSQtNP8dq0oRaNnmVG/iTlNwB1CRRYzSYe1SdvOigBFatHStJIMwzq7WlJALgYJqHPr0H
eeADKOQhOHZb0SN37QR7l1MMAqXjbq4nWShJmPon093D3n0sE8Q9MGi6/mbL4iDOI3/K93cGdFjS
KKKqlvFswPKNlorR+bLjm1ImnmFSPyP+w+WOmZdUJaAD+iPahVWf1a1fGdZTdxE/PtnPUIUF4JZ0
wbGh2d1DNRGG4CM38L7RPTMkGNDoBk1XwpzGCYFYjLIbTKpDjF8SQq5I/wlg296iBzZsNem9wVXH
LF0vBRIv/cv2NTGSVlAcmIlz6qhIvhmQUr5E62G96UKkAljGIgJqgHsizptcl9E6gPs7hqdCmDYD
nA5JPeUA2fes+KasMEJtiF4mlZ6R7fF68LDzv3pkLIh1j6T4Khv9szfhxvddQN1R5dELmHyx54OW
eCTo65TcgSbgJbSxsz173RN6Rje1l6QUhYtc8EbqxN49tKM/lvWzXzlXoxBT616IDyp0n7lreD7+
iOwajtPR2azPkhxFYSeATvDM0HMfBnMKiiv2bzFjYPvdVF3eTok2FAiN/ompdrdv3Ygkidto6ge7
/CQklDt73eaGobR3V3br8SlZlyrnXn0zcChHHSC/pjjQXdWz0Lie7K6V7wVQmr71QxqVUj4WoUvC
BNbWkElHw3fzZMeF/f8eIPGEXcnGnsUlSHeHw81S3ENF8qRjYgHToOufPAvCDX9P+TYF275Q3aew
cliO8K6QeHeAPLxIAV28dfUuidKx7EeiTTQYnVB72Y77SH4xfw/vaGYAjAu83/Ytu9a09XJUg/U1
XuFp78JUOATgjJb5A9kMn7iD5G44p1yjpHK1ALk5PqPqCR5uK/IBtBG+bZc53vG4rGjdgov4f/T0
OaO+J+43045ut9ghmlM7ICnVQjtqVZQPr0G6PWDdiBQnG1T1kMn4aj5THns+wlw05lkFKzZr7fJ4
XTKbTgtZPzDmH6mTQW7B2kYujqG9J08+Bi00hn9/9d2Q7Jk4tXZ/ZL9Zab7V3W047db2tjO2fUNe
nZTjLfLW3jFDuEJDWH9hiYMk9FEWys4ES70hv70pAWHhDIgiqeQFJgXuw34sMqL7lMkRVE7KnIv6
4aMfeO9TVe6/NIx+IInGhAEW8cKHAQqRqWb5oWskNk2/TCt8Nvrnts3e4PmHz8zuVUg6AKjIAsKa
9GFDyUC9mjQ7wSKPk36Fu9AEuSQVnypyC+5z7gEKqGhSFhEAp92RXyluwylJLXcRgec1ZGTmH/J9
vz0hpt11qkcpi/55QDIvQ6/aM4/QcXVr1Q8Ut4wE7HR5Bvccdr/8KrTigrmcJ3vOF7phqjMBXIcH
ehMkteSwYNuiOabp0VT1cD9/BUajt4G/2DKTtOFsoLBRU2pBjJAFqa9qwhbVUTeRyB1YkzUjidQP
jtHl9M6sdxTVJjQsYNf2R4n+qYZpuA1DWzdN59ilDQ4kRTNGRe1Qd+orKev972mdNkLnGRKXhohI
zP1VoztO9QvenACFHP+I3LYelAqUYZaeOS0IJg/WFf/kYHoKj8v8L5FQFGn+w+W8z+E5pF0BAMNY
3i/PiNXDNpsOLoFCXAKZn7apQlWF4Ec1N9g1aZePPq4e8sjESDAgswNwKn/c0zQIr0wUXhyZMKx/
xP+rCdMkmBh8qAj4D8Yap9ST3pIqal2QXBlxWarz4kw4AEs0YEE7aBsM1FilpKiDJQyw0cOdlFug
lGkV6LvN18LnMiV4mPCFDXdxdUz/2KVeaeaDhMjkqpe4EGZ2xGLEASOmaT0G8nJUI1XJ3pA5LFdx
9raOy+tuYziMAhqeqr7eCzRAx+K9ZFxaUaddJdJ8hLQvRFgQEOsqQE3CKKchmGGml8JYiWWlW/56
SOFud1fyt5wrw00gIRfhyt06l6kj9hp75nM3dGkhLiXTL+fuStajg4yKOazrPMQxxaD4m39Ex5i+
KsczsdRfpzf0k51tTZc73lusCyN7vwc6MCAFeN+XnX3lnDc242FP4pYFALtvl1I0zJASXcKlD0zb
BgB+2Mww0Dh3FUJUy+0W0CDjeJKTn2pQ9bXLP6sFREvw1EG/cy3QoVCNQMfRNm4sUKUUX+6/DE4P
D7VNHgJApe5sIxoJMT4w/7Xn6Lebk0IJSB5UIw2ft6bAR80z6hD8G/RmwvPJ9OswFFNGjFKobpSt
LGxVgHYxHn6HoMtwk+yO5xq3ZyAHs3MmpFYRy05CKyDyU0Phbt7MKRVqBRa21UWN98PoP16uLQ9z
7T8GKa81zJkkm4IkNX7oEHR+9RssdZQpQUmM01azJd7ui9rOWP4nGuTaT2Qb00/bTf32zB4fQA6E
3XsAyb2dededP22uGiQj3was7jVdLnd4HiDINOEfJwoUVuiIwD2R4ss1pLvlrTPZWjnMMnpsdUHm
UVOwY9e6UXkgsMSlqIbKKj1a9glC99+aiMAAn4/H8c9mupZebQKT80S0RUujTyZkYGlAhEosoyXP
heeSktIK0A1A+ElII/7GUShLKXrA03NPJxu9Ru7Ie5ELMQ0zaKW/SDO4pJx863x4/SSqSN+cSOF0
YPR74/i3Kzo+FTyfpF84EcMFdZvkwdMIvLZJOtUjo5xmdy4vREbBzXGJIkKbPfgXx/yQ2FSDxf7e
yLmmzsDiZS9oZum0pIL8LuKUMd/Dl0hfW72xu3hp42FBBHxst/YyesWmCE3nFUEEOqvwsX3oB1iM
4K1fxSPghW3mPmx6uMsY3eLryetURpPG+ed3pGj9d5eSFc7ponER1PPFxMDDLy7V5qzvNRwXpHsY
deAZ5isqszoG6FXfymO75NejuCgIzgKXZlJ3PsIvXQhNb2QgNE7kZ+CJp/ucG+3PYd1TIlSfSzGu
BKDhGlcxLMndVNCNnEZuUMEJGa5Hbso5im7dqLU0FoGyD/CGJaIRD89WuSL+3xqdutOEk9OYjcx6
S8SCBKGssXnijKGDIOtvFRqAlsJ+LgWykDWjUwbD0xExagXHc82DXAl3bO70HtlaoOz4vSUO0xWl
q2f2R12nZD+pUfMWZ+tQKCHw+sudNeQZv2XydIkLHFmL/5oHtz1swb+NZQ9vtodRp1FqlutduIAU
djU04jsmuLsQNd9kDgUphXzgmm5PqPVgcJwvNYZn1GEAbtLs0LoL7NhiRhTGExUbVUsYlfp2TN06
ElhVKp1Hrd0/gNHdU9Udu0pZBjIuFyOgPwawFGttIyh+xx6yfCdmeniQrnXdEX8CjDvRfC25ieQk
CYLSQlNmyWAwhP8X6yG9Nl+LWVHhQ5RfDe0EkyAQon1PbFgH/E4rNlIMnwFWq+Tf6V6hc9tAbXHe
8seYUbOHSh3N6T2O7MZVJAnrllDlOS2QWVbyrcgo/JBAVW4YMx5rqKmPQNqOTtFmmWrGyAHERJJ4
u91roFAyK4JHioYR8olA73zfXKOMmXFwLhaxoT2NsS/+WjefQbsdzDQEQ+h//2cU6Qarsr1vUtT8
Lib3q5a1UjdB+1qATW/rVp2JBWNzmpPn0hdUfQb9RKZKkMYLytQXuT8Eo1mb2m6qR6JShK8ZehZM
QXHvuzTvqr/+/WssqWl82yf+s2dJlrTG1zQ/MfswF/AcusyyeMg0NmsZxu6bmhjRwx/ShmepM/vC
6887YXq5RN0frM4XkgzTl3DUtAOjavsjUsZt3r+7WNrLVt3okN8SyVrgq6P5m1kFQu1mHZoyedMj
rQMj/TmlkcbjYCie5pVriqvMjoA7ykPIfMUXFOcjTIhEgC1IoRAl6U+wLcVUUhbZR1TEdWeJn82g
GvDt5z/XPT307yYpbb5EtaUdmPqR9QwIUmgpxTQFtRGEmR0MVsA/zPUDDGp3D1AJ4uFdQniIzvDk
7v3akROun8tnJHr0Gwt3sc44fPHRoQ57zOepEuaJ9H/zIpH5pUUDy1KdW4jT2s3L2fcsaUXsvdBt
tMeJhAr8C90YkyvMG/dKH4WIiML3m5tA32A3wZ960uaWj1uD0gqAeFOeLZUqEIhD2wz4TiIUhfWN
JtpIcoK7IPLPdrcW4INxQIyeJ4JceEGYuHU0p/DnjYpSJ06lKU3k7VTxnpVOmT/hHjf+lXZBQFKf
zVW8VgpzXLo+Di2fLRDr/jVkBvBptM58I5QaALPtLE8J4euuArsLavJ3Fw7zp8IgLBUAXhkyMldy
UeXUR7SqbJYydRz21ax70O6aIeulwGKAhNX0BDWryKm0hOLPBJhFC5RDAsU7upxuPRQ+a+Lrl0Dk
AKKGUIUkIoNB6PWXcAasUfgGzT5yaucsxl+NxBtA6qAiDDr1BzOIYKYrYhKVqCdzL4vTTZfQLHDH
oIWm/DYVHTdR6s0KcjQ0tvKVL7qEPQrwQytvIxHDX354jfCBzE9hez3gNCW2Poo8e6N5ossvJ612
77f7F/fOmi+P435Ym/p8CFVHkW1jZN2tkjcbqRdhRVyDPfg6pla/SfvLZGyUoOFsUvz7udgRqfF0
78j/fX9Gw7x6I5p8suE50ZjCmYunh1srlATcDAJ6mc2ypFp575HBENhX0z2k87q0Q8GB4ZrmrCvk
NosiljRV5jg5SMrMohlbDFqLX6h6kUHGA+qNWYGwIMbts6aKMWd3FOmIy/S+lja4dOcc/Ndv9kjU
1z/TxtOr8C1c0tjcG7QBzkwVSXwk5SBh1rWEY/JuEJnvORprHJy/N9flY21l+CcONnnZzMVhIb6m
rcGBzXdXr7OiGkNlvLCF/HAUy9qgWqiC9oV0hUMtcRIAWhhwxFTmfp3126Lyge7IEOtUTXxNYgUi
w8IoDhiHJdeD1nG6y2gWV/g4pYaIvQkpXU+Ucghc70h4iFB2+05E9GLCXErZUNAZq2vcjYJ0Jlkg
RxNuZhmI8DLZi4lQqnp/f621tE74FGzBgrJljvtC3Qh1uueuUHHbSDSwEJvI2EAjiMtMe9MB4ygd
zOQfaIfdLQyIp1lGrFHT3N+robL1PvCo5Vs6hlN7zAIyRzPnoA5DFxxZEEfWeDKu2TyhKZg7fxKk
5hK6MgjCxbNQ0rbMTlP5h0JwbjjKTooqhXL/glt/ngZrh7J+mktaB8jv4Cmm+MjD9zbGZDCrRo0x
ZHJxD7qQkwJoKB7WW+JeL1MoC79ONZ73BkvhsI4vSB44sOazKS02fUq2Oe2tKb4aif8qoZfA0xV2
JSddGVKxYnAoMDJvkeeYZAC1ae9VLuqqKLn/483StN0raNI1ILehZR3DoRfmwi45mByqfPGBN13h
ysZdsEPGP7qCPeTS1MLIN+pVYd2SkDNK9fFzkqizuk118ukAmbUZNFKImDQaZIo7TkwGy+EpZ+T3
hGujhc4Xcr6PHYwLOunD0dKWQHu7J9abmwbK8fdzIdNcGsOkawTxoRC1ArIyk8XPyK+L35RO7bVk
O+nm4IEA2SbI78CTM46Xy6gA5X38IYqyRuwBxuEX2GGmGrczEAw9cKXMPo2vAjRXDXu4q8i9h+rH
1zxQnuX66DDECkZbseLujHM+sUwHaskGCF3RY8pho6MzXSn6E2SKbBQWBeiVjHSpFljDciyFDRBb
Cx6Zt49NNIKpwvYkoYrHejo52TCRppd7tvjdIz3RyhuoQ852okpGogB83Vqx7wConTV8kn0XE4ug
qQEmqEngN7o+HcsfG/yD1CIXcTUai2U1QJqDSk1mfcyispYUw8NOJTv3jB8m9Grh7bnVqMJNvxrf
NRTDniwBWwuJz2Ru8TX9t4Q0qb2k/onS/8U54nWfdOM08jZXjmCjYB7mJ7BxhbRQ4frRAqGWNd2z
tgES0cVN+tiD0ycW6ce7cwREPvFRpfbeavvCS+LswS1gxWXPqXqOzjIzRwrUgYR6PZIvQLVmpGny
y49335i4OJCNsCFUonGLCGTPySyVzNS9j1zTL9QazjbIAPnSGTOBropklClz4LJuWiY2DNdJ5lvD
CnBMyxAArTn7xMMAHbRC6QgEAJJaJzSJItcYMOxj+6uXrZ2aVLZdMpKd1S4eQA9NssgeyHZFtDuX
jFGwn0SWzXrD6y2OL1L75V+GbKP1jbzEDAbWVS+BctX6HT2IrlI9f4EoZKTPWaYp1JXfLj++812o
bIciTUsOQkq/K8JPUFG5JfvAOVrDWPcUc1QDb55fv4cCHPUj/k4SKd4i8BusfDGNVzV7oloB5ttk
9ijjINy37Jf0cVIu+eQdKyS62YX41Lh+FXiG47G7iljC7OjXfujso4iNICa/YRYZ8bxLnLDyBpMW
wsJsHFIrv//veM4oAiiz5bEhMT/RN6FGOKKVwp+CjrriBJ6vusA29uzn3NZL0D9Z64O8R6kDd18y
jWwgx+x6g4iETf+XE4rMpNa6TPB2K0ppLXCd6wZ5vhlV4Ekx3tdCyI46C+XCKKGUFNtqC0V7W6a6
MJUU7d3/zvEcaDM4F9q4sLEDldKA4yiLcgUVJ7AZbvP31ezTJfj8sdvMtwV3H8mini6hIiLINY0B
BLucJHho94xMYcklJMS23uIjY50ngYFs1Kmjs3vt4nBRyRSOpv/rQoOJql3wbpG3VN+qgtoY+1Lb
J0UXeLcet94MIlRtfIbvHf6bKAN+ypfq2ECxK9zfYMrZzMfMofL5bnHW6/QjiLrVOzbH1Opa4sVh
N4thpazhJe+DIKCK6dNTBnk246Q6YE8itgMmEklocokQQKWdTEo2aQUWy30vkkVF3bkVL7gb9VFa
5qgZ8YW9joUIto0veG6GXdlDu0njP5HLRKLMp6hFAHeNuEBlCngUugh0+hpAMtBNX61W+yj/gIQL
iqZ6lQSXlY3DadlYrTpiCJ3H9EE8NUsVnuArwFBYq28QMsp9b0kTmGtODqIBLPEF4n0gp1I8Ogl9
YQ3XApJLduAd2WLIhyYOefsMe1DiyiTKWRm2W7/6eAFUkVyorBuwzoO9PmkE4V+dSiDTusEi/5g+
Kv3VbAeE/Nm+Sf+7amuRdFsVRPGpCKjXqmkDD/Zl/LD/8iGDNdfXLQ87hvU72FBI2LC/YjcEP1Lj
/qXd63UHq3w2wOxe6kFjINGFFZ/XtNEsl8eo2JwB3m/xcQO5NTnSeULe4VCdawebd82YzwQifhpQ
2sPaXE4UYlEjDmSuFIdujnZlKlXc/fkV2f+56xp/eYINhEahVJ9lfqChtPTQySXmQ8pnv3oJSXVn
BImOEgxLoHuvZJywRBhGKhwtjo8c/53Kj3NAQxpBjLH2UwJoBxxuu8t2/6dMMqoGUtko6/WV0QG3
rjBWBp8CSAlJvsLnRBhchKT2HA5ttXvtwzSbBtbSwIojlQwHa2xZx3/RGjCKSrZmynS/dwy3EiTa
h0rePfSozaEZcCSiwO1NJjABj5a4WzZOfv+/lqZAXTFVHehVl9VHTUMu3aqkM2xsYcwQ7TE0Lw57
6OcsvSmGWSukUALIDNc9Lwpede8tEkiCuPYL8yn49ylgA+usYuExQDc/NjHad63Ikj+xooXk/8U8
SwH98tVxlQ5RkYkoMuk/tL2UsNzwTlLzGvjd+MkLdIDgKnvtrvRajunhB5kU1jz5fuMw7sY+S2ym
FSWDBRX02/aTYvOXt2+oxaQ4vH9O9+3tzy9T0amzpey2pT6sTEEJTNbdOwuWuAfsA860CU2REU+T
ajsP66/CbBWnnWfBp7W5qTA9pY0nwTdN/FhZCfMqx88yzb5ugbQLhZiYcf2yNgvWL0PZhzXxAmLs
bf1lWrwx4QEdVlugB8lIiPhKL4obzFZKlwk0c2FQPxG3TIUMGDwtmeU0cz3TssdAUH3Fa9WrFNW5
b5CHc8Qeu08aZfy+D0DCFBLnnltOY+Wp+RLIYNKlQ4UhDM4i0gI2KmkuCwicUBQgv4xqSaWduzvc
N6QRujcI1OCTDyxnbOSRAld9qmWeicCK4DtUu7OIqJ44Idk20LeLdx+iImY4dWqOUgz9P+xOQn1i
lkgzeIjdU2CUbdgce1JQhJ+z/x04edLGX3cPsHp0v4BptS4RENdCJlpsITaP54UuGzukTmkyRSDx
xm+JUP7TZQNYxTeqcmCVW74bLXWEVKDZVkXFrijcAuM3LnfPR5TBIkfC8VcY8vvrFSE1lamxF8s7
X3HVJT8tbpYzXyAF3aRZypkkNIZZ3+IOuuQKWlo/BnYAFJQvYz7jY6pZ4ZFdJnZPdgr1KKkeATDV
JsVzyyqK/rJ45U+3+I5Iv2J9ZXTKgLV1KBXiPcwetE6doNdEzvGWBgvOE2zvW0nHP4yMrjrCiKyL
upyZABNdEqroZi2zNBwp9+0aPNno5t+JyJ99j1RyKzztZr5ZcbRwAl56KquJIJDDclKFZzQE5fB4
+kLSpFprFBAalAUSGTXuxw1wiS1Pm9tAuFT/f4CA/Hkma4XvXQmdXZGsozT9SgcT26/Oltjojw1d
KNtGHn8HyNrVjI8v76n6i+yoVjoXtJvGPcJy++wZa8KT8ZZS3VboNTyR9B75SkWeik7YT9a0y2SA
XRLtoYRVmZ8Do4N3VASXD/LGnKGxy8+oaNrIfki1CRxK/0wE1YKp5IVvyxf5YQVDRBuMERV/EfRZ
1mwWpr/3FR/+4RRMaTvhsQFfV+2SLL2hf8xW0VjHpURHk13KoZJbFO13CzGI5Fl3rqL7NeaAHAXd
3CvDlyBX7N28TYDPr75OG1ttMArktwjf9aTjVTNJ0YqE+XJKRbbrqJiE54EYKXELFS6I1+lp3aCr
EO82xYI3K3yVgwiJa9jc6PSNHjENJPAwV8pH5tiPF/oHFPHmqp4dHpp0f3LM+qmWBPeQLFDvpB+t
3z8MAuBWEuRaoXNvQJpDG7/3vlOaMgXGQ5LBJlB4WShe+SqyrrMDIkn7VbqiZkNEgegSrlK5njpD
SEkftxRDSQZrtLyUju7UQNtsIsuUfJtlYbU2EWG+dF0JOYxm//llSkFvDxmcR35WtODaSJVMU8bd
QjW05TdTVuyu4SSrrtdzp53SEHb1uARfFGgzxzRWGjZDKnvGR1sXxEAhgRcUTMqsv2sKwynCgQYP
pQ5rpHyroyK3AU35radgRG4X5YyTaY9lwL/AFb28RP7koGUKJPqujGy/RoL7sJ+9Q41hb9wuxf6o
H/F9yKm5nDNdDDaCqFohmcINMorCBgaA1rirbcCRYEBLEMHOGPtqyhBH+CLHM/58fwbXi6TMACUO
gwwrsA2CuF4YA/jlScv5P8AZmbUoTmUsUHSQO2ZHUG8saGzOVL6MZFpKKkYnlCkz2hY8if//3v8F
SQSqGCFJLBDeiJ9UzJZ6nwVZaVUpw3LDIEycSbBM2bALevdCuqezXq2CnItHd3nmu+kzG1XoEXLG
rSP9E8+iRkmX6+VmMqI/vKuenVpqoOlJaiGyA1WGwWO9uMF8isJgUQ/341yfZbrIuNtyDxS09oYA
HJectlZVHrsLN8fGVrqnMbkc+346AMzMVm9yYcs1PXctSNbVdaR0YzAQvKfgZpo1oYFsfIRc6uue
VuhZ3lT8zjGOgKt57z1PVhs+Zf++W+dvGPf7BtzqjRIBYpRFo56PKRRefeGB4lJahV17T6dRe1Hh
ve+b9s8VtTm+WADPzmdSZCBfJwdEOZY8yWeTaMbYUt9yvdbrbFqK2MWrmmfnRL5W+WyPA5VEcun6
AM/TSppM21toPxc3vKn72Vn8mq7u2R3wj0mAkQCrOMNL5YxLW71wbmzkYkciA6BXtjnw6pfzM44G
DMu0yuBtGv+hq7we6LktwLvgQszUOLZn4jFG87jow5IoNOVx3FtPYeWdikZExmTmXLioEQ8pifuP
N5u3hAdyG0HQrvgJW9REq1xfFzQStuWwkytB2SmnZ1rB3ScotCPHYXlWLKfr/KBAx/k0xYkjd30z
T5tcSEhBkNttwYweZBTMLlVl3BeBlDvp9dGqbeeob589GH2n10A2dWqos/eNh3ZVV7vW9hg5NNHr
/MhlATbxKCmtvUP+PmFyeEebZclemy4v71QVODi/gqRWisx/crK31EdiyvvOxe8Urvv3ODPbKquP
ElzZR7mIgpVqFbl019Zh00UtifCVXXVRF2iq2/pprlsb39FpPdFfh3nIHd834NDMSSPWnpGoQRy4
8RJqkXIajJmjUPZas+v5SdXBYwZObEfc9U1zf81BAnyZ+TGHVJUw9pxF6k3fIzzxDCt/HbUExzvR
IyiFvK4ETM0u/2cv+JtheXgg8+deOkWuZozhDQHOybJzZgxd3fVaBDAsY0iLekqcHRcXEMxmFy1b
zn7QXc6xd4Yj3eAAF0wqFqqEvDeWNvykRqxhZ0rYlVR1FSExsoDHVlhisPfDvjyfRvnSFrTHZ1zn
vh2cQ9Rmi5s3m92ZEl8U+iLfyWbHjP2hq0GF/WBp/IenyUWcedkc7+KgG8xEGR0+DzkXtpjSvjQT
zcvngMYLgpcsgjgTND8MWxkmZXy1c88aNvfy4FqCdSJQxUW4XO4GIfHLuCGfuboTuRQbfx0vEneO
MZ3frswXZ/7NI4Wcaem9jcFQRO/CKdhu1xpA2p+J7yqU01kmiCQ1++fozdLrIkaP724GU+kxXA7b
GS8nNrw1BTG5gVEBzt6f/oRxze82mwt7YNF3+f4oXabrb3uQuHKLI7pK0s9mRZAEwA7So3dnDCsm
veJ2ibV3WIuvWg6qsIl9c6yBMYQlhbfr9NBIjJsNDZs69VCUKvYxrCqMEJBUT9Nuj7FJJRxSIDcQ
lOQEoqTjWTQsH0FeNqWm743MKK+Q3ss4H7lqRDHlDClF7sWt2c4/97mJWuIyVybve4kJTjRnjBq6
KxD8lpySr6Lb9FTphmc+MX/Y82w5J/YcVe3oaFCDIAfSQ0dieKMQ1R7LZSNboxEWFp2pETWf6z6o
XhqCLltn48a1DMzjY4ma6jDMsngHFKdWnWGxL3kAO7mf53dZIYZnG9NCYgJRFjXyRGfB7/2JDbrU
KP3gswAd+cSJqmJ249f5e/jO2O7fRHN5qJIh/7+AllwGnHuTJkLn9nR/DufshgVAm8x6rJWHBZK9
QW+ckmWd/1SYGIe/yhylWod5CppwxR8LCnRvv2Wd4ncIxRpxU3ldIZ6/7s9ctY1mOb/yemAsNnSk
SkmeGvKxrw/CfoC910ApqVJJ8P4G6+qj7Bwvb9t9wzjZRNHQA8zQORcRFb8be4C4kZP5VCKZ1D4/
DczcogZUA1OprVDuN59n5PGdaqja1QdeMLnn3BaSIeZ3vc9gBXZypaAazL5TbYCDuhqWAdgZU1QV
ledw5uPatKfFx1zSbYRhkTbH5U+YDilb3EcEWptYhgCU2+5eYQM4ZC1gjFakL4KO4vfN8AZ+9GY2
r8cSsQzB+YW+MvkBVuGhJM4ElBpiJlRwZJETruCShwKAB07EZs8BfZvqOans0tkqYwnUt/aY17sM
bAX/ojV7iX9OX2IdCEDbBr4cqbqwWwkJs546pIsZ8sN/KJvGQ/ZKulgiOf8MKAACIxc7b6w+zpU0
eNHSMXQK2IlXWqkmiiHvGKraP6JD2LvC9RS+qZuTGpj5QvJ6DFdhnCLkYR1Ac2TJ72h/kXzKVW2m
wNzZ9+OfAiGHEKn8f56/dHXOqp13OjyF2m94SGPGKib+YkHjMJnpB2IMLNFy9E27NyMxQGnD9QOn
eyD5/dXskPuPXqfYj6spJDFnhMa8L66YuuC43fN0jThxYYEN5EU+78RFRPoGnAUm5p3FfKz/BPQS
lbBQuBG69SH0EqCWl/2onnMTk1IIPrlAnYadjRbxpFpd6hF0uuohjr5q4ow7CVmWM89F0jZPGBwq
Yu6d1/WlgQkf3h4cAoY/pGdrxebNH+P4cVabiq63evKIHu0qzEmVf0syFvL2QQ4fAFWSg2B70Tdn
kFRkfYVgyjtxO67TRPci6khPdpCGZuGmO00CwK7xtJpONgGIyjA071lv+Nz9f2eBVh5ZFNhUaY6V
J42kMj7r45nXxN9wvDJ1Qv/sazL5i3c4V2Cv0NVGJuk2776fz5jJzZ8396PKfZ5vfwIkGUATC5po
5pfCG6HT3+rCatzJ8T2JU25zuGxlDzwLaP7HHMVDii/VWs3gV/CIcDd7IxxE8BjljaLH5A+rte0D
QZGiz01hIBHf+9hWrbafXNJUPhQekeWND2wwHBR7Li/DgAlQ6ZvKjakXWlifpFyqvT5NwgWdN6hw
chXBCIQVGS8gQte3hawWMbguJt+uZPz19Eb3TbHfZJ2HwfZcJMQXD5LTTEHcnFJd13qAFu0ZlBwr
5FXKdimwohKstoNeVqbVe/V2FraoixgNC8LasWAowRI9PtQQwrEsVyHQChU6Ik489aLPPia2vIpO
KinvKh3G7T3tuaKGJ0n4DGte/0mwp2sF4DubRVzM5zJQP97n9hCHLHo9VD3yKVIceJtW2tMEEvzl
AvR2nJaxoBWGcZklVFQRooZaFVB2wmvVil4oFdn+9eUptl5a+8hV0HYDenMheeQbYGE0aIfWGiT7
/It/XyOsvFJAwWBOMPZL7jfZcvyp0QBw9SFCsiR4+lVSRsH5XudUo8umFB+kBgOipAlZOnZnvq1h
HPHRVFUF136xBvy8kCger3+sVsMLm8nB2OuPyUvUBHspuukMl/62LvqCkC9B4toTKl1oXMUZn1uH
54cKrpUDOyHQIDpD5qg7lQITVhZw0/8q/0VCZi8YH7lCl8gBKvLTo93hISibxbO+G7gNIZNirykz
TND5w9QISUiRfTzWkbovnGvdy/0vijsgKQEltQ02hAPvXWuSEjbjSBpqiP6mycpZxXsMyg34mpMy
aUkReXbmNPnG/k/p4t/gFa3grcLINDa48IU3zQKDF/+VyDtSjso4FLmG7+evnfrVbfBNW1BWp4/E
Z649vgpFXDvWEO6AphPb8+6j1cs9pE7iOxUmdPt5F2oKYjaRxPXseWoT26XG7w9eI+42rXTC+o82
ZXcVVDUu8hZXYEAs6sZ3M63p+2e1ahxsZ3AJuWLTOvph+MTLqRYD3YoP+nRRRElhULWfpzSz3OoV
v34Few3dMq9Lol2oYSsaqP4LvFhLsSZTfBhy12VcSCmmE2c9/1fKnTzt2LEOf3f47TOXqM+Mvp3g
4ueSwWFV5I2f1y+kf/pSOVODoNOPo9Z7+jtMGu9WrZZn10dW22G1txHPnVQTm62SaWOFH2bZj3MT
K7sYDAv3EGKuj6Go9G/ln6iA1CaTSv8p9JzRc/79UMNzfJNc2dMbihnhnkBdNaQMjlrRUME1tOBt
1f7npgXLSEVdwDaYuZeFdcbvLFpd18a3RWRiP6C102FWP1w3ZWh2Vz3eCci+bJnI+Ry1Q+gmmt5b
XsEVPWHsTzx3Jeq+0eoxViHvAZLVKL2o2JXHZ2DdCJO3miVeflOrWMJgUKczvV1vDvJG8HkCe8AD
h0fP2ZKxVPSSOmvP4lTSopushPjrLGHrnG7n6c83HCGEWvX/63NMOJK880w9iD0iCSI9HleRrX/+
mM9jc75dyHXku7c8WbB91XquIa0zDlR3waR6T6Yh8DJFvHhYmIYA4t7chB46T0Bsb+vWP3c4P1qS
PeaqS7YAQjOXwSIMoCP78JDA6b29LttVYY73chrHsgx3Gnw9Ou9ByE6Y7M+DLcczoIjtmw0J9LtE
ppsovGIPBIhC6Q8XyqGeiuQvGwxKRhzc97ieXJLjUhkMG3V+vDj5UHnVtEnvA2vFr55sqk3le/Pw
b+PobCt/924HtF+lXqbqsl+osBCCLbar+DwISBqcPmGN+SuzfLR/hBBEomeSWZHGYlQ/2HdWlsm/
UlIY6imEBtFE2uOu7VowfgAdObRsaD51Xs28aNbgzxc09QZgPwFM6qMxPK1rJKj8g7RSTst4lVp5
quMGy6vb8QAm9/F214z3Nkuvj+aT3GD17yRrwbRTAQU7KLbAERzsrGeSB8kgz1UlgAf3V4NG8Nhv
LrymPsZLE4anbufqFPGhEadswwik/WXUjWYtrPz4ZNmoDjsiqHloHDtCSE5PMyDZPSVAAwVy7NSp
dXuqN2ByteHlYOMcBmjjJMLOzG4O2XCUtIf+NKt4eJKSckxm+JFkt2Ktu01nGD2rOQw1QCvsIV3V
HIp/d8Opa6VxuRulRoQZJouteO4/x58ozCln/tCgTA4ZNoHbAulPafOQF5GLAe729d5lEfk6XkUJ
xoBXNRK91Kqxt3KwbUi+WNBcnlZMPXjjD3petIl99vAWMgbVivk0nqoSPLGczDULjCE/ASr03jld
/91gb8LtK3qVb4zBEQLnB9sgHVTBpmLJOLYLql/0DeZ9BOxIS09v+lNV3d5gE1LQ9ORs9HR1TGGq
/9NMr7BnbmuaQANxm/U8qdnpcgQbxKi9ClnViTyf4Akpyn835rM4vD0E6UZwRqJUxIZlckIzKAhJ
ztOIRhekGpmN4gkxCb2ZXaYWX1ywHlrLLFSdsOhmCEpLzw9H0yWICMOeZ/rOh6x7o57YYkepwn9Y
ozPVcP/aVJ/2fshCemfogkvbN51yqcGb3XBcM/28dD6lD5VH3hvqB8PO7jAh5uorMeLlS/sErhPO
A55YigY/jab77XrmbsciBidK29TeUxlnc2iY//sFQdCCFqtJynDmr+RtlXROjo5fQejz1rsorKA0
May2fxZj86AMzKTxvsAtvZWFu3vOFq4yJFIiz6rHFnOR/vjM4mOiWhV0+1Zbt9YPYFG4wuUQwn3L
BE10lZe8XMHhDDVvjSDU4zbe806igpBOZ8ZuvPF55fcGcehqYaAYy/IO/VpobSp0ZUat5Q7E/2Ht
btUnhNh/odR59Eymavmku0qH1ZnOzU8reTNi4ulAkEoxyMtv7UFYSH3djKKryb14aIUKgRAygrMI
3JhffbNB1F6684bfYk05dmof8RPDI1rQtfzAeTyLxnxLoVmdOLJXsrc7zBY7uAf0aP5gR9h3PNcu
5WCrOTdQtgbYnQdHb6p4GZ+ogrPbUAk0+BaIEQbZ1eIWCxkHhaTKOqzGej0/E0EdKyvi2XRVk5Q2
MIkNusMjmwh3RtW8fL6ykezGpV1ZevvhHxA3wpgokfx6y8I6PzRmBcFxb1H84Bb31S2bwkX+8/BN
764oGqNg2Fmn4sT4k+46nwZeU6i2ycKpHDOvkhMq2b4OiTnAro7+9BEKrGmDaAJoO7LYC91dvCwx
qzdfdL3snH+tuCKijiE1p2MXKOUIkiY5pr3PmOOwwOW2ApIqAyxZ3VJjtprqqzypX37tq7oY6No+
ulM/zCexURM4Vgm6u9rep2d3YZPooWPg/uL0cJQ6cwfJNAOnv98OeZHu5dLiT9wmD8OTbXDmyU75
xRU8ui/sA5xARi8wCeZy0r9Fsa305iLZjisgieH99E+CA6SChbs1dMcRD7uINYt2+DVeuOiLSuyl
pfjhIyX83bY+fhgdYki76peAnGhbWroktkV7R87FMF2z2hLOkqys9xet0WQwrbjrZrUMqUVzQzoI
rDErxI8NoWbIKRR1n35d41hk0bK+f40mz3zatj2wGpNtowzMa1YGeNc4DPJyfFainPnGU55L9w7D
CX5RefJZJWGgzJxSb5j1pDXHqe+1JsCvx6xxq9z5k24Ki4nj/4A6KpYzSISqcr1ZhFr7Tl/GT+ZX
YflX92FqzGv6C78XqaWHQ8kecWBvycE3LF+FiPgD1C5KWwM+BeUEC7bmHV+nldvaCzG3c6D7GDIF
ODKRO8DKaApKV7cbUlVR68R4fkCBi9Y7cV0/CuEuTebIW0JLgUFNecVmloMpsu6Kr0N3OUrZEVcN
bSNV6WIcgElld6U=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
