// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sun Nov 21 01:42:06 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/alshma0b/Desktop/class_project/class_project.gen/sources_1/ip/ghost_left_mem/ghost_left_mem_sim_netlist.v
// Design      : ghost_left_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "ghost_left_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module ghost_left_mem
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.2716 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "ghost_left_mem.mem" *) 
  (* C_INIT_FILE_NAME = "ghost_left_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  ghost_left_mem_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18240)
`pragma protect data_block
bOsSO2QM+AfebXbULCihroS88a8KXx66IKaFsj2P9FIPqCsWzCjtafXuI7ZgJSHjNxA+YHh2941x
LNJLHJfoHJIh79FGU23qAJgfAqUEdlvoaXQ+4VFCtuHXVn0TSz/hrzu/5JiousFFR4+668g/QBeV
SG1p7mAjC98c724k+Ft8TqR0gGKwm4i1JE6Y0ZI/qGEOn0qnsILc5ckgQQQzOwA8Zo+3G2OoaTGQ
X88nozPKls62oV8N3lK95zFMhTvAmEMxtgAGorECeBuncF1R0j4cvEqjbch66g6qekWG+ge0LhK6
CYEfm7YhvBDEYzY3VkpComBIYCdKxfK34gOWbRht7bmwONdBbnZWdwaGmpCp2+saGbFh5wkpmtfo
kh/Vvfu3f/0S0RXlu2zRp2HKPWBskKpT0pVx7WQ5bl2uCjyk+JXMCzHwgxBHLNbDW31QOIp+mjbX
VISIo8k5xeOF+QtD/wPVzUPFwwnphTceqBIJwddSqa0//I2LpQJfC9dhlml18iohIPb3UsgVapju
nqA1xqrNGVnlikHc/cG9Gc2DLw59XrhfaXUX4WuVNmTHw0YVoS6uohmaJkrEQwUz2RvA8UT1kqTZ
nTFLf/qTdcnG4NL1mTcE/2Zm0QPtOtMjGJZUOIVWNHGCHjzQnLi7eA19HHx0Aoc1zi0lW05eP1VY
x1lbqnTCnGyWNq/cjhEZNH2Qm+ZKj9NiKq6ACvEI7OIi3LEvmaoJRVGqcw31xeD1V13xPaC1K/RF
qxv+fowO619SpgE1CI343NqT6GsswPas2NC3G/GR6hWI3ZYlKkmt4BuQ6UpTlX/wZGI9y3VNjuc3
bN3Uiu53CjslsXKeTZykEAU2oVPUTN6RLk5ImFFHJKv8cM8RJ07JoWbztZ/14XQQvGSevKqTp3zq
v1e04FQJiOeO68Pm3eUhFPGGJSbqx8cXg1+n2nRbnYPnHjexC00cvxSyBdcxgPznn4PMxZn4+6o+
EmoRGdVzhhjm2lfJ1S4WL6SiPNXuGkw4AfhaWD11R2NH+1QiY1/rrtvQ47HW0KEazBgicCjAd69+
dgUb7cZVcLoJ9TsJcBbQzPElsZs5sA7IyxH1z+VgMd1zHO6Go756on9eteRAgKf7Ed9VI5hpK77c
8jwJM53h5bNberAQ3STEyUOjYTrAB+6wnzljFQpL32fHyRAdOoIjWQak6zOApq42MeR/FKcBVuq7
GKWeDfaIDV6QBQhpNxxc3xRmJ2EsOZyl9ngzmQ9P9OGZWxN1Vj7ij7z9iGQVsRr7+w7GZzMkhYwm
qwLW2cI2Hmeb5dG0PDyH27TUfvzVrLobHhXI0bQ7QBfTtM7GPRIM+BOd1/xKTPONlD/66RSE/vEa
VkvpZr2JJUr7/3O0n4scSwwKPuhucbPkJ4jRobQPnpdtSXcnNkeE0PIyJ2YmS9apN+ePXakIxvle
1cEFEN2OVO1ZfHE4YFJaMJwfxoxnNpW78zLXqOvl8ZYj9hPnIMVMeyIL+JPSUhI/0V5hCJvwCtTd
Ifpvv65okUZVLlsxpiXFQLGZKKLe4j6vGShNDPCqZ7ADd/J5tMC6z1iok6S1hsEUBC32FH6A5Lmi
oWQbIlr1+mzl2+jsSVwk/IEqjN2yL34gMfckvbRzumwodDSOC1J9Y0RiJULyfNkPEGGZHrMvmsEx
vJ29vIFzSEOi6oi5E9nMb7/DjmCCaGfjfJzZLO7rpuknS25mGbkoQFl3KcwYC2pDfcHzda1eJZbm
QcReSPARlPc+ENNDVAwLkKAkb9KAccYn+R/Zs8wwRjBKyeMhNALEbinmOP7uRdS/OrliVDTSdTYn
ZNNQBVcb7fZ4LeQ2FrqbpyeIT/JNptcjDyW8R9mm847g71UIs+sB9RptvEccNdDz259dDDJV3Cua
vBe0baL3OYqlM7ISDrS2J+2oKDb/HLk+d1EYu2CeOPFIGiYeS/pzlhdUL+ah6dfPUbyNdbuX7cWu
iNcAEKNbdp910Y0oLcJ4/oUR2e6sKzwy0Nyjjd1VC3wnRZTGDCdZIJZbutDtNOcI6pEOWFXaO/ND
9GDcLp1RtrJmL23Wpd7W1zJxZjYnnaa18eIF9to0EMpByj/8EyLnnGVgN0aKaPElLY52ZzW13QiC
6Q87uKQgTrIXSN0Rgvw3srXwZRjqWrykMWIAIh6IAmrWBwFnNMTmF+S04Yq908ePCQNrPAd3Jq7h
5nTp5LAeNeEot6PMD1SPA9HrL8NBclgRUbM4qPQ4QTAZxMhRezW7gxTgvLmC8Qn8Q/gM0ryiKL3p
KTOpWcgEiMwvk5DjFmofszOpIkXm/kd2ormm4bOaWOTTmkvEElrdMPwZHMCyShqoxbhfKtX+kcCe
1GszPfGKgulli4Ad9PlTi6GLRY9BiKUgu55CGB4PNjtiLqsl9x32u0RwiN/9cmHfMIWhVo22/heN
xEt1yNvrgewCusXFLoTtki8RafO+49N6LOdGnayTdrmXrSrm/eqIwb9069ww/08Kh1/zZ7SDZHnQ
iYctShjup4Ac/ixrnNIqIPY8wWrZXgwY/FWNnuqlux6PoAQGJ0hoUYLVVcSftMindcvGUWv9s1rg
OP6jkj2Pr9ViJ04u/YK/kWaiGRmOB3qinq0/vVFkqsP2eF8JkTrLWDS5iba1IiPWp9o/tf26itUL
Bkuzns2yrdqPpMmhL489+t1iLi+YSiOQNFLt6oFJxZ0BA99fr0YUntbsgt+Hj8UoCn1+OJJkKFVC
CHmrTkPMJF6j9PKi+WfrBIacNtaUj0ZFlRYrgFYOuS9yQkIfAlR8kijMtTJmdhG/zQ1ko+5kdynK
OVY3R0xCZZ7GU8B6pgimFcr9PhXTp76zZXVR5hk/Qw6xAsvTPJH58WlD/RaCX2kwoedObEBFPSCH
KoH++N9hEqgl0s4xKeG8ZFnotgz90ia2z8KjrvJ/ubKCQRLnHVQTfKv3pVQyGc0Hr3V3nwBCdp2d
LLX8DOWtt5IjRCWBsAb5YkiuEvBTWATxtvywBEnXnKSsPQglhlOJEPJfEaVbBB7l7UkD9m5ASNjm
VOXSTiMCRfKRbthk6uu8hhl0LKmmF6B2pGp7znP7NW2apvkutzl1ncyacQzQgmU20sPEtBYafyOf
GB93Z4mXSR4idrVYxEBBFwNGdt2CaVWToRkrE1AfiAxTkZIcA1InANWx22T/qJKdXiUB1PUCtzJD
44xdHe11aLcZAsHBsKBZD4nZlWWW3lTLtMP0RePzDGGcThCPZ5bpXmCqpu8bdIRuZ2kc9rJHeoNm
vpO0bYCkHidJ6tpCuNU+TkpHhz7hZGQFdeIOgaqjrdd/JkaSF2slKewN7IcGhyQLDoeeOOBMQ4pD
KQ8aOzXaMJU35M3KBYqeu4T46YdPMIbQw4HRmMYZwIl55u9m/SfFjV0IVIKueKR1zcDF9LoFEJpf
3t2rqoCzBGWLIrZInW12UXjnvNvjt27EW3Ccs9cBj0BYw+SotDyXJEQx8nefjpHG17rkiotJWIlt
IaKMm/+dsEVLuNp4YZYfI4KLb9nomDyBS/xNI8t/Y7GYgh/rnm3fvPoBJT/8GDlSmVsJh0zOaeVV
TlwGL5bDKv5p3hHoXLgPaBGWNQaSSxFk3QJVEnR32GwYNthYI/GTixQvoDnebyZVgV4vA/YyGilm
JBHXSH6MxqWnDnE5SMSDD8mxf3LRInlrR0LtUNQ4mOGmFmzXLT0O1ufuyax1fM3izlp4Hhvaajxw
V4wjYjLwQCgU2yvVyOYZcVtKCuja5ack2O3HpYRIC9WCNrIfIVocPlZF+XU4/MNd8cPLhsmUulbL
Y1Bi6J7CANJP4swUIPztIIWpzfgnvdFiAAD9u6oyCdQnyL8DELgdoTgVLHpoYn3+3Dro4jvfHxRa
WejAXcDXGGuzXOL59yBMrKj8EcGLclEhrIQQM66cESayuhyKvLOIoSaHU9dk+6YHhIXQ0VU/iNeu
ah6mifSOwvBFVIRArcNn9aym52dx4cJ09JsGkQWlxy6OSavrjiC5D9kQour2KuKGi8nI1eszNVl6
L6Ol38VtgKE+BdNxnbJwumg16mBmWOQiyrD+rDq84wlWnhccO5Tusi4eQmFrQ2Pi102KitbfxvlA
A8p4bfwQy3NDNsFGfDy0BPbepJiTQdyNgtMgHtIc6U/xAnw2pRZSrEx4e+wdRTczD12Q82oOaRaJ
bTUkW+j7nPfJ0ghhO231PSc3IsaUX6ZqP96LZ3cgSQkfpN3WFY8Mf04hzEDseEP7Q3pi72Q+ruG3
agoJ20V9XFmm0AQLRgTzxHvvVXm/RHD75BGRoksH/ScvBqtqfl+4VGEwNl62ngyAY+TUOBYSvOa8
S6XUKs3xbnbsxum7WCmxQILuR8kFA4OyJ1EwyW7vJEmDMKTGq1vrqX/QWLQtA+JmLUHTWi7o+eNg
9RUsaRfCxB/nYnCXYJW92lWs0vRE8tOd3DbHwQTSVuwMl9H/gxsYqEpW8hSPS8ByDbbYt4wnNyPy
04olD+DV4d9qACJPvPcGNbUcuakY1ZS+HLvW/IMjjtIwA7wj95v+rqFUBgP6ZxQoYheY4c+3xEq/
li8FnVk/XAH1MDsIW+pSNuxvcb2Ga/sWbsRXYqRjij5iMOhwrId5zNf7iV+w7lxjEIFwzlncdsMx
k9p1Lk/7KVjlSTSuIGli+Ortz5jV9dmMbqg74Zxuy4rxq14uS7Om3BcjoyC9AH3lNDUQ4i87OJ4j
vr2RTUqYn5GHS023Kt8rtvElqrXyO9FI792qPdVOvwTrqFUOZRQekyYx2e3C1jdeky8M1w2CY/XA
ocRCJO1XeTMJXc8e10iwWzrDP/gi1ym1pzkYa5b9NEK5tZxre8Jb0QBnoZWrbqe7qpPD7VwZUZJn
pdwqUOQ9pNZOuhZfN4qrUgQwdXMEFZKhcY1H/hy3QTtpIeLxNFFtIClPsuFLTdON1E32fStE7LNZ
Klk37E/otAbx5SEyXUZfZVGYOje7jdxYOE1+eZNcF0Ptpu7bF7GynNn1OIKDxkPzje5OgFoBw0on
rdgGmeW23BRMmFqQDn3JVH/n86iGsV1uW3fFxCy5y2b3KLC/OFymAgBRawtQjFLXr8B9pmebS20y
fHIqpqFvsumjGkaSddnx6nCYCYkuYnD697xKMCdGPyv1IRuOtol7seI5YlWD6QZWKELJQUtKmc+t
HRqjjcul4T5dZUD4X0HrW/RYbeO1asScKbhdb3RGUDq21nOIHu8yndtxl7pAa3vGky9MjOF6l/8d
Z1/gfUwir6QilYx0qvco4Vo4LYURDKvEhGMxZNfWZDjTF8urGxp2RBruXcYivFVadsKILR6Cd9NO
LO1RDg66vUvautMT5PzqOBOTyc4Nz2bPjQIXLX7EEtwMBLfEd0JIq/qBdb5bBTCCBJEB1NqAxGwj
2I5VNRq7JBoooQnNPI2LsnXZhoGNjNBHpndH8tweuK8+PjV+k4meUpctj8NQ4nA28Hh/mjMaGLU5
zL65ZculoTSfv/SKzFM7PXGPFTLIpDA7nkaDgIXAX0NOLASq8JEqW5Z88iifgZt3xjZ2EijxgaQN
vHo5aPDto6mMB6y6n3lD62qx0Lp+IAXFgqfkuqI4EW3QOPuj/hxCYlvsG8HJ81CyNOWryhZzjER0
ogsgGfK+h0IkjXF4TLXKgfA8rBhpFwNdmurHtWwj5Din6X7XXBBu/ZuB7K40deEmkI2UjtnNQ0uy
Y82BzWJS5DAfp9QUfcNbNIPZEdIR3Iw8GzrEKDYlcJYOqVXAUfq5Ig5VoneezUXBQ00svOytsm5N
eaZD603Tn+5UUifkrP+GSc2nbI5YxMKrv5YS1PHRfRb0M8LnP4sXfNPdsaghvpnnQoqP0XZMWcb3
YLiOD8/Yn4WVC9gBiMT/zjj/YK5MtGiSBodMc26rq9OzfhKeKqcZuB2yAQod/4JmDt8gB1ohQ5vu
5Wng3Y3E6Bii/TLXmhFX4QZuK4wpP9KQCJBD63fz3f7Zl/qszhu+rKfRn9kE72Wq3r2EROqhP3w9
Hfrss0r8kiTQpPkBtJRz5mZlH5to4+TWy0Cx/20xP2ug/Rlfl6Rj1iy1GqjuBURjgAbDWAeAji77
0RS/u47F4tzWU+PKs0GntWQjI//QMY+gr5luKQUf3ykvTOktUPAOFkj8EFURWtiWkiCbOo7PMX6M
mJDc02jeZmOyxOyWXLfGebEqlrdqb2MMhl6j6qDjW+fjhiDXCyba0ejue3kvRhgz5AqiVLIQCjLp
FgnrbkR2WpdsUxy4X9EM/KOxpxqzkkhrJNQFQPE8J2gW8235qlg7BnfxP2DCbhKBJsS88d5NMCDA
BDN/WeeepWKuAxthGnvdfNUwmxYW+OTwlUAn6vtXTcZBUn+eumtKmrMvbX20tQnmx75zakFAKial
p6aLT26nqaLO6QuOC8K7ITXQ6/LhfKNQvo5MvBhcUDFZKON+id1Tis/7TWl2SHFD3qVQGdc7y47P
tE9i7kXu1+U3HSJH42GQocx3s5xp9EXK4TJfSa3pElCnJJxoyeRmrsd8kHH/WO7swD8a96PV5vmN
vBZ3HbFZRJWW/eGa0kHNNI4Y883UaN0SLivJV+gFfnPqCKRmWk2nOxxH88miGD8Wb+OJoQZiRUuq
wxK9Ns3AZbjorsuX78H3DwDEu0ZpRy63M0Y/a4crBEbpKKV7wNWjREaL9vHMYfm29dyn6qXnc3Bt
3qwaoc8o+uCz1FyoDx5Hy6VqDkRnOgr9Tn5bzQ3HdlWIQoiE21MdzOJTY8hOHzTxC5P7A6eMSvFP
4OoLWEzihvcALBbGx7tMEYKWmPsDzQADu547T/mBTY1U+8P74YiPLEEnAcD/fhuzw3cJ5HpJj9My
kz8Pu0aLhkW39Gn0vdhO9o5P08/cH8ICEQakc3YflSa6mlANHwo8z1SvnQbXUGXj7HO7xYLw7IT0
3ySYBX3eVnSrzFkm434vQvafNntilfpgcQqV0YDe6Ox2fc7MQYIcpVmL5EB67Pq/Vzbwohy/SbOG
gazwcnYv0zQ3J7syK6UjX3nwaABN1g4RILYkGklrISyQ4YiQONuDWhW5sgkOjJiY2c3/ajkkKaxr
wCMxFUZlXbPWqzgIsbwL62pRJblarWxHDef5MEAe2lQBnp3bMYuRZ4zqG+KtKUYkXE2ltHG/sDEW
Dg2+uf1gKAPg6ceGXlGPH4kNrxGl72cAX31XOgIrEZYgho+GqanF5qeonKJeyX57nbpiTwmUY5Cb
NhRkyUK7IPHlm8mvh3OglImcKKSEhiOgidYsJBP4igKxzoIS8Fa6H9GzXzdIIQD71Pq/bCn2gj64
DEctPj3OyiQ6fa9roLijXuQCqEth6Dfq3AgXKED1HjuqGiulJo8BpQhDbdFPui25Cq1BiGdhLk9a
O490YnUbGXfCRFrzeG9H1sGdgsvrRHqvLRVudwYVf+dUP7Mr3buUeApAi0IAWkt/dPxOJy7WyVtl
+hIX23hGJwRQJTnNkLLvhhQgNaTQFcsw0x1fSS8u0x9wQoJZ+BHbVl4EpxMp3NHkl/9hgng1RlES
kpQBpn0VzzdY9Q0clUauo0hwUBkCcyxvQrAY8aeZlzc8UzFf55OdahBDsbp2Q/hPQ7Ic+LZujFlj
hY1RfjQButM0BZFrkkBVi3jGd/2hv/zhj5ajR2dLgz2I+yL7jhOXEjGUfnm6cPShak4oLrMnKtjj
B+0kI3XB9qk+/+w5uNL9+/SAMywLnMP9KgKcCtKxLv9XgzyDYIrQvyK+uZqnvhQBmYwf5O9zSJGA
YiJzYU/2s35YuXhh95r8kvAJL+hHc6dF91q1rZSr5bwtuDV4JIwL1WzXgtR6FlLlHiXuchwD0gyU
L74WgJagxLwyVgpDXTFpqmTUR3K24jSLhOXwx64q27K+OjTQE4B6NAgvbTt1JJV0KS0/hOBJn7+a
hJanvxPA1JBLKf2iJDjUPWiW8TZBRIVZLwNPe3HEGi7LKZoDcBQ8iMPhVwcrl/JfONKiVjeHYBDN
Psiud2szQXXbuC1awWcWe5AWopTMwGx1vlNpHLZ5HyvcO5hNxF+gmiQ+mheHzY5VL2uYitTBJLir
Zuxsd1vuNGKF05FavNUsbHsPn4M54MHiG9vJ/VrsfB/y1RABzHbcVate5dvhopyVa3cVB/knymQ/
neeCDeXRmVNt5lD1Ec/UxMC48SOCSoV0uMELm/Tl/JTqBciiVlJQ1lRnXWz5qF+0my1qX1sYh909
JZ/gL+3aP6bHsy/cicty5u8TzaBCUDp7Y8wMqGpQp/Ty02phxMN+PVvH48NfpkK820dSyPsQdmaL
ww3RwxKvzQLuX6Yw29iv+1YrzejWHcnPvWUIN5N9Zshbl0uhaRCL0qKsRpqC0G6/Ib5pODVC8bks
Dik1OErD9mhaa/Tsq2W0ENuH8vo/X2NzhWZF8iMuN/n41D9LiUa0fef0qc/URr5JHdEKhNNGVkd3
+wR3zZZW9ClxhUAqI+uvwqIBzdewFqOP1+Y7HwKRWaojvG9mVRC4AVNjFUMu5oK9EB9Kc7Y2KYjU
c/2MjYbfuXWHCIU+0X6BBqjHbpo5o2To6i5OIVYKTq2P3H1YNm32rt1BZiTvJI8drRZrrdKtOFWP
QDEfSUbJBpCpOuv1XNV1YvIa6YDxkUyXy/h2ZuGL9UHhylWZl7gExgp9c2ei9cl/gKwpaU51o98z
sWlkf362w+lCHWy3zZdBGrWR2bh7O2+4rglkHBLe97pPr+1gFkLCtEQyw1Cf6amo8RWlvId6UmP6
sQwBF445LHqz919CfVGga7kvCDKL4LTOvHdUDzvPITBi97iScbjebnZoJCGYL6BXoZ96P6/Q2fYH
ImCmKC7bivnbUP7273NXVVclV/nZIMFsLFxh0V1GNFSAjFqqzE7juzUfOxdfI+2fCeiJEqQbj99T
WyzEWM+Lncgyt7hrFoT5+/RWFwyLkdf5IAnhb9YSrHj4s/NrS+MDvUGbufSgZgbJ5f5OK++iPmp3
wYSOeAYKoUk091kN7vDgR/CMAKBU1ZFg8b9JuSASSu/B03VVoJSnRreem6wwR4vfHRKtUpw8yIzQ
5LIOGMR4F/wIki9KT4sDGFxr28rv9F0x3RYtnIcbiVlmxu+kmBi+7+BiXucBm8WCRb45pT7Ww1ey
zi24ms86ohGGWY+5CR8aNBSptmYSuHhJKiTLoHT09NdR3ShjeKHnmiVpWNFF2E8Eq8DYmig/xSV3
yea0Ozkr2v61bNntXIt1YRpn+w9Q3BaLR/BDZ3e/poFnhh1daDDbwudpUikCZpxGkU+W6CnW7PC3
1TDyUmfyOPfUja/HruAWdWKDsQaMhVspj6i5uqzIwufnOeRnYI9lTiJCeK4kJ/5NOYYaxf5kw5yc
Y7CR9HuTT3V4ksbxfT0xQW5E9qf5eEN9o1rt34cXaTDGlnbFD3kJBsCzrWzF8wFNRZn8lciXgrRs
RL92+bLmrerfT+9es6o6L/NekAOg0bOrvxDcvqazg7a3FvU7KcSgx8vVTS7nqNhLy7QFUcQA7R3C
VSqc52ZwQbZa+NRY1G9EIuM4/2m6fGQBTVzKcwHputH6ZkcsF75Uh1Kz9kf1SSarKeRv3JYqDMBn
je8RQyW5gJRpBHeAPm2GVZH34zN2qkrc/L70WZZPoos3eUGamlp1w9WN85Jqf+yuXcELUykt7jeE
lPuBp4EVodxYqmd4PNlwzXAu0xmPqhKch4ZThUMXCvE1DzgdIPBhZuXX9vXQBDuBucmVbKyRsfPJ
7bMwCx/vyyL/uTLvFUGMJJ3k0KP3CxfOl6IAOlUNjiGfAAkD33G3Xu6csNNbC0AdGsWbZvWEz5v0
3eGBw/aXv72OPfANyYINcEFZV/ZI+zfp0FzwZqQ0QV1HcvmzjXIpUEIOoIanTNpOPfMb8IWF3B8U
ZYghUgwT5Rb2atFrHSuAt+uodu+SfBtYZJT5zWOcJ999+WeiPLf1wY8fBP+dbrasGOcvVKdkJWnk
SbQOcCAix4jg7cTQ8aR6X1haR32oE7U+/R4yU5W5CG5bjkZJB7sF0Mi1EsjVWCXK6gKLZNx3woyf
Ldp1T9l29uEPvaCygznFMIu4XNadFvyI1l5jZoFNoIyHECCtII+KOAriAtPUfKf5GdhDJ4OjQCyd
aadm3SPKplZzsJPa1WBDLDkzG+MgUVpVlW0qsZBCUPvIFdBhd45tr74biWnZlT3DmxRIAjGEII2G
1eL87Mi5NcNbi/B0wcV4vav1KWa45WWletyXjCXUHWydluL6PaKej2u06ZU5KsT3SHTYSxIPvECG
/EUB3ixm6KVAQoqo+OjbNx3xdqfv3G5zWAla+l8GFECk+oV+1bfz8Od3Xvs+szl564WSsYTjb/xE
Od45qXs3i3a36d/gQCNaBoOzi4vUzFmaqxfye19wsgyoO/iEOd6dIFtFBlMitMq5GYhlaSTYJIy7
5OEPE0iKYZOCzzQkA3+hrFXwPOVLsuvJpVh56D0jQC/q3olZfI6xqy4Q1ZmyeKmgP3ICFdtzL7UV
IWu8LxhTZhjAe2B17BZTCfAxw1o8TljpC8VgmKq2vSVOE+Q4YGgl9Fq/h1fmBhYFKTNqM1ieZBx2
DLYHfnbRmRT/O1D3NMW5qIDxtgASMYg4gAeKw+3gjDTwkB+HFG0+aghHgxuKBXbmXDzLylkpBWHz
mu0HxwOuSkAmitA1eDJRCpSUCDYMvxDpL6nzRV62Xk8+u7RwqIL6zUbat+oo0/FGUlbxFlR/Ckv6
0hLqaU7tMGHJB+jiEc9V+UMyU1zY86CKphO329efJJOu0Nvk9ZkNVsVOJAIgNJ48buVS/dvai4PI
PjnLkizHXMBsXgKDmrz9wrt0Th/GkNK60MwhptjUUiDIvkwaNXG8g1Ob9YiL9dR39jRJvB8Vx6Uv
HC9SZDWC53DBFGMD5IZq0tTkFWCLHnDTptxZtU3J6hjTG7adPkvn8BJNWJxTh+AGmoDXwWE6wk6B
urloprp1SY0f6U6vEAiMcHfpEHKcyFlzA8RrQ91qQhLFZd4PXFR6u1/+6hF2vb9uzhPHcyGF8uxB
OMOTZ/qF6b5mesgvAfb89+VUe8ZX4FVwgz/axaGU2GtBbuCAAgUFRfDyJBL8tH1WnyVTgmi8hKs8
c/L8pUG7bJFCxXcK4W80loiY/zVgBTUudgIQWeSRi5chTb0YT6DjQdf6cEFIq1MTyQ4Mr8TDjJVM
hc2G2MYQRCKIiWsot5DOgwegEOlRb9txmgvX4tu1c5iQG1UywePxXZ/r1YR0xRwE3k6IOA+nz21d
QiNKKLc14Y4UZuBqEvdbm20Vkt1qmmKFJ5S/ie28hhUIxhMY7T8LWAo/H2j2yhkLQ8GWPXrBif5H
SFceIa5niUMphPeJgFdb4jpRRAkBhqeJM6nCzS67/UxPJAF72MrdJsm2LI6cyNwAUg+6N67x/iza
Z4mHrncb56IjfBa6EmgtVDAOD+j4yoSH6vzt7KXxOcP1e0PTrz+yEBohg7SJq+Cy8Blu3MhZgAlc
yhoY2Biezsao2PzUe0RTWq0mt80dKYaNbMdHS4g7IAz2HWpnriy4lp8U2eSHRbDIISp7beG1/egW
VKBUKFyVFktd7/AH2tdxs+yOxS2HuHCuJOqKMxsYiU0daTV0okyl0ujmfIS40hr61Y5xaBN6BBCd
9jRdgcorwtZsPGmd4vf5Vbbl+/UirRZFdDMMwKNLYWfqQaLYw62sn/T9AHvsf5jxvAKBJuu0ugYL
E6zCafOBf6ygMEetOnVywlgl5PKKJDhf/L3lCWPqRVMZ/GnNBLvtIxQwI3h7Yl0n9hIdgbp56KBC
2B8Vc5u/MjEmcXhblDMa95A28G350VWCbWGP5E+lIi9fUSnxtbymEI5L3lV+xQTtp43hxvyonzdy
u56BImDxX/qqnQMm9K18gD0oSXCOj18dV8OSY9g/TPf123s17A07b5dyDHi85ZrdT7JNHNWV/FxG
QYPkIqGn8YMxA3uFqGe9FsgGbLJXokMEYmn4rx5aXiSpPFkscIlek3BbI0Ep7TqvDSzwO7CVhNG5
bmTn6wnDF8viEA/ny6qSTA4Ut+1Ddta4bTCJUFB6J53FAuthzlmjvmhqgJkGxoeYfnj10l6adidE
d0PCzTpP+r37GaxUZ3xL/kCSsXDy5I+W1GkEz9fm8wRN0cdw8hbN/ToTaEjeWYyVjiZprRRiZTUg
zzbNhtanTWfFzh74qQ7wjjy8Qm1eh5UCuq1LIfls5C9yN1eYlFItjmRyVqgxO1zI72rVtLTYQGp7
G+urDCs47LtJjLgcWJ2dcU+63HvL4eoalf/QWMKEw0FMNEP6ITmPRzyrFTAokf7AQLIK3K1XWnSZ
vbgYX8W5KA5V3c/IEHrXhNEGUDs7jCft+TtJVdKlmiAm3ZnDEksXRMRbKzCYK1I6f+AmRQV+JPxb
8CaShWGJ+ocZ+xLLlhhMRBjGGtDQ1RlhDjnv5neCPeKnzCnXhLOpZVrJX6rAbYX5ONFAc8COpXbN
qVLKb0UfqzZktc+BcX2BpZ+/DwLbB9na9jID7pU3EmXmisHpiJI5jqe7qW0T45bUgHWyN18GiL3o
wK3r3y6prjl6FnL2ZMq0LnDvyhl1YJeLNYesTRP/GwhM6su0HncZ7uWEKevUwOEGbGzEuDSorW+S
BS1U5DIZv0PfgewXb4wXOlCSkQhWFomb2i7PYZJ3ig7HS3ju6Nhmtx52YX98TCzUYPxqwESKJscq
aae5YKOItY9MLynV9odxLDq4H4K9t5uKb1L7ENDsv2cWzXD1JQSnRW7S4fSKeNz5nWAp7NqX7dhs
70Xg0HgU18vF85ElY6b2FMQwxhCf5mQhcIForG3nuFZwOBR+1d2NNDE9mYCsnd8wI+CtUMnic2QP
E8+zhMsE+C7RTjUr2wHdYrDcoYroWYjJRXeXd7FEkfo9hHPyNCgerN+bDkYdT7JEmFrCabK7Txz8
lnuCsAEoatk0Yxld+/tIfGA9tGQ6/e3np3sa1h4Y4dUplkg89pLrnoR9bVKBszkMaYGsKGb6Dq0g
5QwYn9beyTSd+g1j9i9Cpn7kojtMtlRPpyaaaBeHxOwu9hJMbV0Uj02qoC9Y2FPdJW1k5/gSXE7I
n9qZX8ZQ3kqT8Qid+sYmsrY8tEPn9lU+QxPRvhhmPtWT0vrU5MleHXxIXld+vAQs34kMkNtdfaW0
CMfCCG4DZELojNOCaLfjvZ2O2rfFjloOsmTafbrqAIg2TxK0ZPSwU+kCSOmvbWFK5sp1Eiqihx8T
yyQCdiQgG1Kegm/WM/1WrQ24cyzRb9C5lLIvnHpJdXkwxhMF9K/3dF0nHAEdtdjhFKqzKth+G1IL
/Q6Kv3PmHFLDeEkfQsYd/cLMHzddedK1ZyOiXNbr9iJkYvQ7nL2jnHdArSWAzZ9rxQv8egRR60bf
j2GyZ0doV7F9qEM4KARZ8tWEl2UE7aeWqcmqq7ED5PC1srUWcjl2diNo0hiH6ZPScQz6DxE4dFiv
D+3AntU4bhFTYgiqWF66sqh6nDJYx+KBEoyQAC3krxN9YX35Sy3tBv2xhMXMtf09npMhZz5X+YHr
uj41oGdQARoXgf6zcO8KftThp3VHAqy+1GHLOxrbtc1ojAZrx2jI1d140yJ7CLPSazD6Rh+eYfNz
7OokhPf4l5sWZdkHjcEjX8HE92BIJr6PGYG8JcNoh+PZup/mjUTJeht02bAfgksOLo4eI17eGCyE
smaId7rsAD8Izk2vR7qMl+U8zDhq0kyl7m42vWWBkQDsUkeG/sT/SyHM/bJeQdVevOJLMq76+aXX
Bp2uhmCMwYnMjtKDMZL/wCN4rOLC1TrpJ9GI4HSzx4mDIMtxs0S4gmhWui17vOFIr6YVR+nmwOem
V6+SsuONDxNX7i8nygdtdbDJVuncDzexP2x2UpGRkA1VyCLN9UGL3R1pCBEn7BPHqw+5DKHIiHn3
WhteLqzyjftxMOGS0N951TADpCGMXqvfpOK+XmKb/9akLcEkHIU7I+66HODIgVK6AgJza/sA4Enk
VuSQUA5aUlt5zWJa74zbcbUeFnfj6uz4vuDgZhIhP3PpMGX7rNpOhsjTqU2M1Jp1f6ohkrS65B1L
OmUdF1jWJXmJuhVi8k4WG6clijZH5/qOePwvyGp370Ebiq0Wy5zR45ngcBD4kWaJhJX0W5CU2phF
t3hh7cTHD8dm3XvLfy9sX7MMdH4mV1M3eCPY/kWMPW/c/67h3nmQ6GdI/+XsPxVHFldq6qWday7W
dVhyIYVL1bnSrpLA2HPAyELQ/SSev9cU8480G1dNXKY5QqCrGZ4lLq7Zp6akNVy5dwA6WO8tHbvg
PSIhHj3gTJIUCGQ/vo3a/MUj7pcR6P+o0Upk75w/dFuCngjPJpB8k7oDA0uL0wIyL+CwhMSBBkhY
tWIFgAFNQNH4/yFfBZOGbL9w7R+gq1dBny66rOtR4rmmQuTeYAkJ2KJqvLiK6TfKbBFDCf8SgaTw
EHbKAPw9G4Q17BeB4A/CGHfBBed4bEKZrFxN0vlhFL2cvb/5H9CVArAJNFVrPSvoQUOf/lwK2lA3
1h0vKHE8e9/h5ZV4tDSsXxFmcMf4NPhAcpVgMifgdgB5BzzWYEuOEPKV19Y49iTjnQK3h7byBl4C
KFFDksSO2HrmXLvoq2LldRc2iYXW2fcv5kaddvqZHtugv4gsa7bQv9Jf6OoGe1l5OuJZHLmdbdlE
YJVjWza1OhnSHDVH2W70Mc8ve/0dc9hT18tFAreszCdJ32iNZ/u6TFYFby0JzJS0sagK7ehvYP4O
AKnpejeLF/+QbdWQdTVlqpTcpFMkqvXsLzlpBZnKZoYuD6Arn1cgyTSW3GfDk0ZqX0pKI1ftvt3E
PP/ERWRj1cyc2ZVdOaL+3dgMXifAXZ6ZMMygQVlpSUg2iDoxqGbFMzvBpm+a1CZvLmVdiTzLJfi8
GgZjVgdB89lsQZHVUim6SZ2bojt0Bex5lqMzj4+Bi+xloRpZxoDP+RxVlFAs4Mc16hcZtIF18DfO
RPJevp9nEP/9d2k8igQ4gOI43xiTNhJCU5iDU8TTHDcEZyKBucruf50pRwBVLr7zDhsSLLCbNPjl
5HLAymL3OG6uh2f0AdBr+wrIslljWe3o4OOXC1Wi5eHL9ntlvPP+yb7Val+HSXb0HO8EPNJoB3Pb
Rnt5liz4mP/VR0lTEI60EwKkawsxJD185bSClMPV4lay2DJoeTfdVo6lQE8aJK5UhEqPkZvIQ8Cq
AnZ8oJp+KkyYTJDoKXrYxyrTUiCjppfcvqlysnry3i8a7vreFj/VPpa3dwm4t6dRI9ykhHFG8du8
EgwY7A9voXPAcuUEvik8RCu6XaEoKUCv9ZLxMTw5Wx5iOxAMxIWIRDgORb9pFPEM11ju1DOYgpcU
2lDGrOBhoMt6yumj23X1CJLQLzFfnygmEkIRJmua4HLxyosvWAqUodtRFBKPjUm1dzrbd0OiECe4
r/y1IpgOylZAp2NR3lBYUhOoq7ztWs9N3YOHSEMVfVv8Rb2sftX3gOZQMCntlc3a4mmx1mSX3six
9xAud3u1Wql1P25mzQ5NJFnKsotYoZ0fTEEbb/QagURGiz/HGHdVtNFVqG4DfeGZgcF7SgkR8trC
g9JelwC8f6juZaErAepKikjr6PP7v8XnwFtqUR95Ni5iKWiALAMe86F09YdAkB8cj4fgcVWY7zyM
aB2JnSkjMRiol3qzOilByIYswNawrwta+elGl+Ki6CGVAmQqJXqoNIoRDG9T+i+oJpBi7IEzFLdX
UgJ3kaKw+PDhaKDEgHl7s8FIycjbwTEDZvCIRlkGGw8SekfgGZdwbk5x/DKxgs5FeqpMoa1f3lLr
gT2scYTgeZeO1q47V05aXb6Mn71EnJWTOlFMv2yCC9YTaRtIdY0GgLqW9BQ7i4XhNS+DIjPfRUpH
Ylcm70PujkFZmtxs99G/cRpo33vUBG3sN4kJYQ7MgVVHyDdJq8oOcQJ3dSAQwq49f3NsmZe3ns0t
PG6rSO+HZ4qMniFGEojhudHIrSTLkYBJHSWPI2JCaTUUosNkIGgkkk9K1gKTKnLPwyDcTCYnae3b
uh1Ub3ot3rhpQdYpkUWL6XV2J2rslyCsuEakZC2/QPe/HDCL2GxwRcxGq9Puygs+AoZfQt6PAewq
Fz1iPW5N165qPjhBdry/uL/pfvp5tO1cIIBhf5/LNVWl5TA04cDZT3OyAnVsdBbqQb+ZGpMT0dOo
HiFusVq3zIJZQUdk/RS5+90mIRPypqacyQSuziCZL0hnwd8z2R4/Z6PNM8AoNHPVYJhoFsPHwjrG
tDzC4u5IWUTQRxDnLJbMNPuMhkEH1wRsK92kF62L+u2Wl9iyY3ucZyatOrY+tSNkVxTBbtNiVBHL
yYORwBeJFWvSZ3h4hanu6RbRb4m1lRn4Nvbxn/PI5FHlgkaXP6k4fc59jQI/f+8K8IPj92HnZUej
aVRSxgR7yuJkXdnwt2CKxBFykOjIVOYtDTAoaqepjsO38wVZsDxx6iGIx5590YglALD4V5HmpGYT
5w5FoJJu7xRRgpsXGC4RHo/BwkkkCf/EltWFAmOgwCFMmrm0V7sCypdodtreVxMhWyXh/Pvd4yVb
NjS+b1nu1/1pQ6phu7m7Fa2mdnOdd8aUbDBfFO1PYOQjQCPoO0PVEqqNxBBid/JrZd781HSsH6Im
Hfxb603IBYZNqPy0XVitfH9hYtNlWnY2rxF8K/p61LuAeOKvbbpF8orIQ8JPxYNwCOz/AWql6ggs
pygwlRd6Jy8A8XWlKm+BmxNr1yH0OEJ3s+8xm/mj9JkUt6Ai24FbGu+xTJZtStrqThJ/AISCeN22
cC8c7myxZG5Lg0CVZmJZlou/tmjlyeATocvsJIC54P83q1IM4BVfGUFe8o/ftlaLQKENuYMEcxkO
2ucHzG1v4iec/sMBsLjkGONa2DfwiG4esgL97DqJruuIx4dli8SqvsfZT1jANvGlxZ9WWIIcx4jL
zsq9gYrauLQ22s5iUXS01LjttkUrsvy2r9ezxA93/pj0cjnw/2yzfEEwrJjWSiDxc+ldhM+A6KGk
305TzQIy7iDyLM00jrvpEp2Wl6opxUMHL5mW04A150PhV4zXBQTaTRUaR2rmgWR5j730ByIaLkLL
1zw2aTQ7IOK13oc8TKhfrN6YzCGGyNjV2rGN4obbJE6kdZmaOAacPNzxK68DgHYNUgDrtX0J5Ij5
e+oWXjg4/cEVeic7WMeFaWiYJTEhJefOEmdTmsb1FH4HHrO6dqvDa9WdWx+OHUbPS5iyr6KclYUl
HP5KHwMb8TV3awKvZ86Ct3EYbr4IjGzCsEqzG2r7PTjdZS+6rqFj7079T93WSc6qoqhURGMerr7m
lxYvDK3eD7DcTZ+Cw6niuyn0Tr/VrAi0uABkg29asH9WyxSnlJikDhEERgGMSaa0NVoEQDcCOl1y
JxUgf65Bni0nGIDYUiX/jJiiMUTMcmXx5jq9U99RgVPZpbWdkXPDAMWijmd0dGcUTIUXAhRDSaCz
i3dpVfm25mnyhAPDr7HVyE5tGw2lwaZOph97F8mgGRwy9j2fP0mWhinA/FVpCYCneVsXrLaYvy72
MJvsU/KO8iLg/uys26LEf6tgUNq3dynU+f1QX1wmphflJJ3Sr2rwgdCMNe+v0h9Kv28Xtf+b5FWS
4JGl3Hph9T9HXmY1Pe4hZac8hxr6y6lM8OWdpvxXXABFn4hRSIoa+mhDABmq9JEfv01vUD7lgYau
ditDhU0Md2cc/JY5i7EtQYfhT+v7Uv2dU4SISAK6x1kUdFN7VNfr6Ll1zoe350PBCI/ENcJlkBdj
/HhHra8x3y09nnoMJkxERrcixdFtXcsXP1TWbmC0lQVlRpue7GjdA80SvDNqyNF3coqqUEllfs1B
cxDsLLx4Ko3asIW/dzsQY2oL4rT+YVby0yxTweMI/yIzQo63++CCST6Gm8Wl5E08jg7llnFnxa9N
ULvPYN5EMjOEaPuQ3osiuIwYdtLLstHiFl+90FEYKn3ZOQlihQL8YRtpg6+EDT7Nun6keTG+wY6+
NWeyh7HS/wkJU1RAYvSzgzPpQ11seu1os6nj0w+4NVClXhpajcUHLx3Ii/HV1NzNL0ev9DHMqbT1
Klx8thbdDn/rsSeicGfJA9+mpJ12/+bAh61QSMHNamRpmo1RWWTLRLbB2wJvbP+hPwFbWaHyC0Pm
mmmjaZk9+cXy1UscIjCtZggkSgf9oqdi1OOrTEh7UBRonCSfVVhdjYyyy52zFcEJr/1CZkvTKJkS
bgfLalenSwRgCMSVOykgehpIlLsKVkCDESdbmckOY4B6kI+0vNCk9BJiBLgfzK5EXkLbVc/iQ5ks
l2k2MDZqv5PFxMgJUnsVksbQGyA4Dh/f3FrpF5PnvIdTcVMlADuQ8aIzAiO0y/6GOE6aLDU1K84q
dYUm+NRQS89Oh2ojb1EZwn1zdtgEY0eYI6okmGP99LReS0DS1YKTmci/gQmw2ruw6O0AXQLXvAZC
08NcyO14UBW0xZPGyzCCXNCfjLZYwZrhwvD72lQdx6oeXixSKDzdPlEZMyuw9SfvbiD0VntEIUdD
j58lpV9K+ee8ccra6Yn9Z4cc2nN6kbF56e8k+ABQ0BmtKQ2FIGSmEQchXVL1IBK7vY19urwiEawH
1PXce4gmYxjLo1IWjDlJ+jMb4QaY4HVoucDiHFVN3Aorc1iPn38mK2TZ7+RGHRGs2qKos/W2Ara4
revc7Q+eeGJBYYHZtckrDRbKuRVb1gbIGdaAILsI/q+ubnTo0cyPX2eJ4pP6X5xWqz1W7I+p7NPO
fQwksssDvmV0UfRhTBZdbUDqnuOTEWhycA2ULKa1KT4CmkmReg48D7sw5YPqgZfjag0ZSQ+P7Loa
yNnfqbL1t9DI47ImXIwpq4ohFJm9isxyxYpKtxGizJ8hFCqZ338hcCIlWLckmz9k6/hZQ8sC2bfN
NCAC8aVnvBp5fjZlip/PeAMwOwlEN84w34Z7PgKkoI4flRNMcxv0JRz7fu4/UjFyqrKS3i5X8pkV
9NpS92zhCyh5Q4uHwn66xo3UQRsL0xyaSVw1Zl/UvgvoEjcKosMgq7AGaLDOGEw4DdKvwefQoC/w
UCgJS8t0E4urbfuaLY/j9P7jYIauy0Lxm9ILh3FovaOE7niRPalRFp5/WZO7G1HucJTDdHJF7oYQ
nTawWc3KddlS1P8ibKhlMTK9USFOUowgEnxhpm0FpU8Y++2txjNplh10uro+LZZrZJ4G2PrdrpC0
zL8i0DKzIIg+BxH1ygC3whfDnQq3sLbhM4Hc2ihnhf9W+GxEmG5sdfvZQ7/tuiebd3CD1hfHVmXI
W8VEAYzegAfHqvWoQS/PFdGtNHp20SSFqpkhzVqvtjx+eqffWKpfC2bydYp8I7hPT0IqRxpgCt+W
yvc4YjxJ+5LML0X/JLvbJFynnSKzdEG+zmEb1cYlbzS78Cy6JwiLUPHEPPF4d3CgKhIgiv5m2u08
SqaVdU1e9xxKHjppgoDlXpXV/70/4uPkBme4p79vI0skWSyQ2j0OFljOdOJ+8Q67WmPFLnJ1HtGS
m/qCCLA7j41vxLanNaXot0WLg0poSb6TFpTXsZ/DJ0vPu7Ky/+wPF9iSq42ArN4/kZRAT0PpCbhZ
rNBMkhWERkx43MkMzntgdtDpZzkoYfA5yRouWgY/yRrudWMcWbtGrzb0MdHA68pqZP1QjqrQVeLB
5Ra6hEsAsLqVHcaOsH5iCY+usZ7PY54Bszf2vU6rAtwQ9TLUTiEoNmeIsy7notM3JZx91mZujKtB
JNYktbSTueiPECUUDmzMD44dXACa/pGC2LA6KhBMwqhK0o/qnE/HZy9sH+FHeUTgAG19tK8e8dZg
ZWzMgUK4S5skApsP3D0eETmNwLaLlVYwQ+MEDCncz2G0GXIdxVCgMnnsSZ0xed/NY6uj5RFXSygJ
sL0n83dogTu4tc8NWHSKqXuJkVmRrPxGKT4H1zjaRMxy+C3zSsJia61KlI0jT7xfPrlq7Pjn71x3
8kX29bUzDFIZJRMMO4yT+1VOb4wLmpa01EKWgMeplzGlnA8m10NHxINdfrmY+Cb0SqyOF/7TvQzr
/83QP3aQdz3DtWCNKSoJEm2XZIIBF3+9jwUnoJ3bIN0ehpb4v60tPs6zAVzSHZ1A4R8brHQWBnTk
Z9hb2TbuAjHKWJ+NCuVyO1Dq6tItor7XZQJCcCfUpLS8dw1jLpd6oqtIv1BYfxjN0SNeZ63ZvZuu
Fz5EHBEsDWy19KfE8crHCVEr4VHkUyRXx7x5FvpvltE+pFziQlYsJ4iARCu7Zx9bZc5FJ0ABbMq2
gaWWhskaRvfJn0Hi0I9F6v2wBMPp92pyjWzU7P+ruR5qZhHdOcmWRCFQR//20S1CWyrKCcOvTCjY
H/vEfiRN+gODTCMcki3zTGItAe1cvIkMLmP6du5HNuJcGMOVHKV0tUZZrP42KdZcau0l2g+2sTGd
6bA+hmYabR1OGwFNdt6gPN10viVMveeyRlZahfX0/xWU1ejVmIE7WrhEOUpj4DkYr2WynEzfGBGS
J658BMIhQc2dH/bBbK57G7ioOk58RbgPz/I/9lg6XI59dLVlc9Gc5B3K9mEx/vXPEHSkIt53RKQc
KDMYaSnRPajNwopG4O4fErwKapm8kJVzEzdahZ3tPYQeG7b2SjItcbmz0efjaaRg/9IBQIxx0rhY
9lcOqaZYQbfJQxs9HWmUH5z5wFbPd62mO+3wFJaRNGuCT4ULuvppOzt1jwHywFXuge0v+i++L13T
gYjB84zYUCpFGJpW6JOHyKT4oDE0mPH6ZliRSK4qLveBdwbMyuMqF2FLTL01a0qmZvSrkmcn3+M4
Qzf6jTkHVb6CIDoX3bZ2N25u73BIo/hhG8ZbXrSRx0IeMnwXqyty4Ra+Kscl1TsainQZYJXdGPd+
ccWZHx72digvI9FOMb6cq3OA/bHFu2HdZ/THBzQeblPVNcsNNL9HqqLt9X/UfL6dWDWceMEarFr4
JAGOhQuOxFp4CIP2ZBckMO/iIiCDKMSqUS+W501IBooUPqEiSscfH5BDokHlJJ9fNR9xjMoR/LAA
wNrc9zmEtjvQfE3LmuFy43FopWp5BC6mCRuwXpFzzPdKOiYVXjmyOA4fpaFha0NtAVT57yrfw8l4
hmzdLtVsdXC7h7z1wy6gcRgUa+6m1dNZktmyuxvrRGSgwjt8Gp1yO3q+WylgAQQp/lktt9ZPrKBg
IBhv/yCFeKhZaEnBp8gq7A0zGVoBHpsAC2ZGNCBut8+rx+KXanGPrHtetOiacZtQTcOmXrYH/w/W
SL6ya0pnKJk9uELYMpiClVLSZ2FhskUMcVBmPSXi+irhYmYWe/bHvCLy632ir5m+PtosRf1Q7KTS
QD+Faam+hR1xDgyANDpj4iorl7hlsqqZ0h3cTYPCh30gfuBe37Y5mWXXy1HTPISBiPdTOwciWpn3
AOQFDm3EEGR8pd1sFui8CSLkcrO5t+9n5BQThzOWWfFfA7i+IrtM6hm+hAFiU+ifYhMcVWSSOQzk
SAdH404Vum8uJ7C+L2Ege2ihEfKK77W+h4bVxaGFAfryj0s/VVc0Jl87HtsaG9Y3T3Lm/dgfdOMW
KzIcaQbjYTskTJEvU84S/stpp5F700nvF7IXvFaEXQbmbfIhx+T+9f1T8rmEa1DIhEISYM955Uj2
nU3uoefXDAXQ5bP9vfV2f0BW2LkxvGUSZg0EcEM2imx0p6tdyuQt/g4cz9G30Xy1uKKMDRdCxBt5
2qJqtwb3jagZAJy2ducbsZo3AfbbdTuuZDXKBT+togOWd/VN5cFvAnBaqQJUOlHqhtDrmRzgt1DF
35oehii9kQYZX+DBJ7VuO8j4hH15yDWBDwiPfqXkgiCBv5VJh19fHZdCkobtlyra4acmrE1bU384
xWkXF8jNdm5iPdJQML/XVaFmE7XxyNy837n5OH27ai2MDvQwFHKfL5iq5J4Vzjhi4Ep+aedbJ/BN
BnpQDpguINsELE8Ww6eSUN7c1OPCr5y7D6K7p7FNwD2bXRRmGWoWCdzIRvCTD2K5YXAe1l7UwMvn
q9T7Am4pRt0q6Ym5q1YrstpAaO4EhPIHWKL5CR15qf1woVdfPwXwZ2vEtPBBGtLQIFQIOsSTTFIh
G3X/lKMz2QUlDCHjVGY9Kh+o5m367jgQNJzWhWg63j8zepXxijGXJdNxXsmLeWwiLad+ZcZzWgrO
5gDbwmZCNL8bViLxdOAbVdMm3n+5n9xOEkc4916MazVYLs6LobImHZDmO+/FbsZ7tSOYbwwN2O6Q
//NaF6bMrtmuwpgkLbm0Qtmzk1+W4mYMXV34Md2R+AmR+lVGYTOVyaXl56YeCDXtYv9HC64U4yZ+
wmH36x+orHtEfpXODBSn5mOTpgPbnKtzjXUsMSPzzCmzs/t48Y5ey/zXooy/bggI9sUs8v0H1qXn
iuGKLgOXF09eYxxfUp5kAvnzAz+i6uikyElJX7pDMoOhKbZcpjrSfH1d9CVdbhDl0dudxqP3S++E
f3fzWT7OUgFokmG4RDXW6fSnueut/xLLPq/Bcz0p/cLd8lasbt76REO704wcynPeVvVOHlY7Yzkq
0Lo4qlyZhrTfiNLvhOmGIkQAI12/Jcb0teQqsCJ7wQF1vIrFi5Oxb3uvH4w8fmhkLVHw8zBmE6kH
kv6Q67DGFGdYABDcPVhDPLpK0AzByvdTms8Jpzu6Grgc7mEPrrFRTifVNgF2trkOdSjVrNlcgR6T
xOXsF25vDZTak3lT1oxEWaMpHBZrfMcY28AkuEzzUszmKIT/4PATajNOdk+Q9v3MKVucd85nWpWi
/JjBuGTHGnDz4+v1gH552qQxUHbNjhdzRMKaAYF1YDkhPpGds7sRt29Ir3jVKoSrBHss3QBPL3i9
BlzTvnVMtcPTYb8It+IIwudBBbWASAnmCt3K+uR52mzfTLLALxVrzIv5Dmez4VuobzNWwM8g0t+v
g5Poiy6t1etntCnmpMmwgXN4F1kVPSLoxxrdKkFW2wr6Hbg1k7gS1W3Lqh+H/77R7bxQ4moBOSPL
tecf7i9iuGWd0TWWna3IiixsbKQY3+6aJMRN1a7XdcGnILshFlAjoViShRF7U4av4A7KfjK0PYJB
y07HBuqRJlg1fS7nfhBGX52RH48HyJP3/HUjMzwrgGz1Z7uAB45QkBJ/2xC4sovtojHjTgTl+m3u
AjZ43B6qSPhPxabwj5IJANOcyFynJJ5jhwk8JQCx5lIQSv9I79BPyqapCrnOvYQylFORzG7GuLRm
1/RIXoGCPvqY/gLPKGaZHVy1l0Dc+VMIq7vvKxO98ZiBk5ycbdRcLk0I094ZnfArVK9X5AVj1xZX
hRkK6pO4N2lLWdMsXwmArvaspdgNIxzJe13jn7oikrCDagD9hoX5s9Z1Tqkw3dVIjqEdaji7uMKw
DXXQ87Fz3opnOYiadCClxwoCyv2GzvikIt46XDeB6EszD/r45e50X+8dXuMSfCY4JajBwWfnh9O1
nKOHlnUtWbUyUBc31d9AHYzNR0RZ1rfXwBS7Ixa8Ix2nRr6zOhAvN6chCFS/oRxQpey5ylOBwP59
8tvzWxAHPoPQfPCLYFaX7ySa4W0xzJ3PXCuD0EdgjghWEpFuzqqcRhg5ix5Y2cCAG6MQG9LFqTj9
zlKIP6xbnBOCb5UJcpiAnP5v8ZWiIh2U0fmDQCsHPYhdRECL9ltSXqdrOJtycfwKou+TXW8OxuTh
Hqo9OrXX7QqRQyza5Dh1RlilmNdVjpLWZgixZP1EDSPr2u0DSfMIz17gFUa2l/h15dSHIqWMmMnl
QIblf0o9ZsOxQXVRHZRkOamqXYLqT3FcJ/E5L3LnE/gJbGHwZzgVrHq2G+yYLhNcIfnWOJpjB3eB
tBAHEcJ6DpXrgiWRUDwdQPx0+A8c9ILCO47U0XH0h7REec+Cut67b81lLZrqoFB5H+JEmKQCac0o
VKQBlPqimL+BjGEVBa8cC5KYjH/JQa6gGT9LfIYVe53RUr9iXAZpAv6xHad+9rN/J8mY8PTnwMft
U6s4iYBR7WRtcBIUlIKMNd3IdxdRHzQ+AxNbqocp08DS/72oJ7C0j1rwUd8+mD8zzIGDagSHGU5R
HWZc3z64YOr8bmrqRwiiWwgCsRyO3VZxTXWD/U1VbpwJUrLAxUJHvUq8BrFarG8BtZIjQAb90lMS
/23AeGlr7zfmexS54j4oFaDNXPoNK86Q8o6eZKgzw8XimolH6jDdIRP19kRX4autRUZZsuiCRe+m
jFaXiZrs5NNLrTUqFWSfbRUcgAw8Vvsp5krdJok3rqVU09Luq9mMf8ijma4YBD8w5gQJZWWNHbGm
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
