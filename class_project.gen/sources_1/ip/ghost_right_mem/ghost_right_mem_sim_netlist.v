// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sun Nov 21 01:42:48 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/alshma0b/Desktop/class_project/class_project.gen/sources_1/ip/ghost_right_mem/ghost_right_mem_sim_netlist.v
// Design      : ghost_right_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "ghost_right_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module ghost_right_mem
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.2716 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "ghost_right_mem.mem" *) 
  (* C_INIT_FILE_NAME = "ghost_right_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  ghost_right_mem_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18256)
`pragma protect data_block
W+ynRxjuQZ4x5ky2nx86OumtU4CxDXOm6aQJuNgIIyiVn0EXG0ORlo5BpaioZsKBbxo2dCj7QZUb
lK29ASrj0SdLNzE5XS2NMZ8wH/q/1WQsTNQwL9/Pa8brcTNihnpoRdyAg6Pqw6sFZv2CwjFspw1h
tcIeqrOdhNqObjNwKmXyktw50uJxR5X131vH9LqKgDzHsG/VQGZKeasf01oB9AmjEgECJ3stKxle
X5MscQiSBrR6//RaLNfAx+g7Z9sWS0l8YfSDWMEr7cRXjjkokVxkrCS/BD/McvfDATShyS42kpq/
9sSR0OwD/iKID8n7gsjHNl/pKZmb8oDYpKUUSn46xEYAonVyjD11I0RB73HGB9KTL0GCdNhi/F1Q
x8K/PUuuP1WiuRMF6EOih1+FaRSAESa3na/Zce86/x8YM+lgCghFfKsupxM7XDQGl/kZJ3Twev6t
Zew8Aw6py9W8sNojkCaYAWsUHo1XH8OBlaTfaUp5layVkoptgNcCKZ9Nypg71WCaOEuYF2Xx3CSj
aOLB9fNSG4iIrVDA5DrKbGeDcaQHgaGyx+3tEh2HegMIUGACn1htWooBCpX0HGvh8tiQanH1fqxp
pB3vTMiJnk+VrVqLeRD4e7xlYOv7FZln2j8ChCddnaTGvOBS0N1jRWzwvLx5WAHTQZtySSKE8RnN
+3gAE4xL3rG4cxM0OODFyfJ7zKRXs+BgjeqcH7O8SWXVlR99JpsKExdVa6A4sKdoKsG4v6k1IcGB
svozAIgOWsAfdQLzr1+ifHMirdmqXb13JUiGZdBz58QvVwE2K1Fay1NO/tL6ROTu3na/Sq64QQmT
gKJwphRgfE0nbPGn6UNfBCQOWTmVyAk/Wrl30JfRSxgeZylnk3DdlAOuGJeMd1RqWoZDk1+iW0ii
4Am6z3aN/6lwMpj1Byl1fk3PDw8MFz2/wFqt0rivCoX17kHbHiN/SOf0em7nEVXr9kGaOW9y2xPy
RP7PrKtT2YH9wJKWSMj08gYbVHon7aRpeE3ZTRB8pTPxrbpoBwn5N5elt3FCeGxvyxAUhkVXhWo3
vXBx2hSOLCcpw+LSdi5+T1TPcHVs+G4LKDnRri6kEw1Px+/HzEhb64xVnkx2MPZvXgyluPG1SvPf
DAnW1+xyIv6hWJ17KS7vH0YTcy/McXGUATjIn4A5Kc+hcoPOS8k1YEdll+Q1sy8QEl3HHpGEydZs
vXf4qn2nrFFQsAJeMKwIWHS/n7wGNNp3qGUlEVIv2YRtfqZG2UDGrDJcTfbl6wwT+UQ6YoxMTjLs
A13zMxp88C+Pk8O0ZmI32Gx2xy5kN9YcEAW2ZB2MnBm6o+/uWJ0J03ImsCPQq0BzFlXRqvIKri0Q
r1z8GpTznIDwTNt7t31jHhLMtNGKB/WFNUx+gaMv1KV+aHYiSpIbEEJe1uQ4HgQkv+6xx3c2qAAZ
6BpQZonFFMEAMmW6N8HY+qvCneDO1DnqE2o2Vdwa/quxUUmLkGxCk5pXJ1I6u6HMaU8Y2oiM/qi0
nvbIHgtfuPZdw0rfK2bXf57RGzvVw28ej1E7rwtmWwvtb0pSHVxWfgwmFCTJFtmeMuY0J5eW46CE
L1elX3YoQKnhmlW1nkNLjQT3m/UbeGMNijuJjZ7LBJoIyKgHaGT9XUdrY62uJqEnxW56wsnmfhLu
ujb2p1UohtybZGERJ24ml9iDCJdH9eym50vaVlCHIM/yI71obzio04HVB4VwebGNE/k6iLRDBQZ8
v27j8Zq7nHgtJHRuWthXEjJgQoZdWOrOtbJw5rwfiNiIdImzl++SwofC01YllUSVgjyjDHmfqMJr
I48RgQ2as6zj8klVdw2uQ9oCzJb5a/ENdZfG4wRdeSuB6xXdN1ZpTEPkjTwOriT/X2+JQtnlR7RC
q0woH0uuzdJG08TH28f/OE90VW6CwdJ25q6w/MO02ylPFf3qEVvBFzfKcrQ8cFwdlW/5MRIeFWua
y9mx0boj4KG0YjqBaKhHxddoutoBNmiosRZ+THqGAjKkPwBDZgitr7K7QOxTW1hB84Su735XHn98
7pMwDEITZ1TQ9HqZrtAr8LDVqg1EL72/+NeyfnF57hMX4SajAKQYpxPjqil+kOByoADn2v0VJZ88
VLnCIo3O7u9q9vR4hXa0dHMqhJVgICDcmLwIQyO1CiqMX5pYFxTKpMqjg/xHoAzYzIhXOczecDpq
D4mc8B9bfTy3gGTnRvRCGsQo43d7wsZUm+5s+ZuJwvO2SjKnnKc/1+uheRgXPI1lb5nzdyspYl6s
VJLZqurr2Hd+Ju8eq5sjG471EPn2d4WdwIH9luxTu9hWl1iqCriLnwB5bPyw4ZBmRxsPLbXBc8bc
itttepHsk/zLvf2EfY61qrbPJP259NVluWTt+EiPKaiBR8Dg5HgZEKXJg8KLrCc7fGkIMFnQDfNm
A/nmGkBeHAEfGfTqyRK4/Y2uCNL1Geht95IJsAubL6+l8ivBVlyui8f9yBcAUzPm5+AyQ/anSCrC
ZeV0EOXQ7gsIlCf9qVtxwJ7b1CGG65Icm5Vh+ysre5/cbFtB4aOKghIBSpKZZev3ZCnd8cRJqwMj
VScgsfx8elZesY5VtTW36JuU9IdzXpxXaHdTS3Lf+Cd0tuW9B+0iQ4yQYgUffJrUYD7nBfcngjGw
FCfeS2blh5PIqU+REcfFtm5ia8WRWQ0TWcHmsaxNtlcUYysK2kd9HwPPcg4XvmXMVruQgxSSiPw4
pUYDZJxhmr6+FbrIYeVa8rC/O4AXsLJnaaB+NkTIJR9cIM3TS5ARexh3hpgU3eYBXM9HgL8OQn13
Pd/3Q4geVmtGMqY8jh57ctefJpq7REfptGy0Kg7uUtd52pBKZe97zRM66pbZqtPCjwYypWM1Sz+P
AAKPS35dTc1BpxPLOOy/mFDVOcm7L71NBMpA5HJzgB5C4pBf7e02KlbSPBaiJ2opS511AFbEG+PP
SdYYg9RHmNSf1d3zqUJHEue+jXBqs49Vg1XtBtSDui0zHKcc2qC+hEAQA/tlw9FQx1DIXnR/fzZH
b20pl8FZwdRltStsFWQxL8hJexiqriqFlky5Al3Yt6x2uLgPzmoOBxUJtGKaEeITEHmYgsVE0c/o
LZbM586VHcn7cUSkiRap8ZGIzlTGlyyaJHrnF88qJWcsVMH/pgcbMHrmvN8KgTeter8QVJMThKOa
sXZ4IeXQ+RgW7owJybCM87YTHg0p9dlRe3i9pXe032fCAJ7gurOk2lHngJOKXM43jnf92+yMrTF9
HLV+mzkfAY6o7CHAJKW4/5QJV+RbYL9/BdWoqd1f49+GilbZEEasyeEaYa+fDQAokA9+4WtRDATe
b8dEhgUQChDC7gaLknO87xZYXngmQqebVUK1IJoLqE9rvYAZwUdw53EPwREMgne2EeDj0aEUQcq5
T1p81lXCCsCxiIHep26DIOwOV3uNC/sm6SqIatuo9uOHjey3Z6wFkwLWRGX8bpvo63/qM50Bn6gT
e5E+1f6XyrxEReATU1PfhPK0tySdvXNWY00Fib52LCvif4NxxkvexANtvqVFWRSxXOxOD2HRK5vM
wUH730oajmSmEaOr7tCRs+EzaKXtdg6OA/PDowxKinUwu8a6NybHsUnlgkHeQsacFGgBN5c/TQ08
XDuqN4wK+lM4JywhgUipiCHh27yvtADRuNemymRj8PCCkTuUjtmiWQWt8pHApwBl7gOqzLYpXnCh
0aPAbqe13yCcuIvV6MCLdvSzIFpBq/plpvudGXrUFdVFmCYppX8U3ThbUkJ1eBlvTGtpairZ7X8c
bAoW6XIif528O2vL82kT0YiRPpnW0Ch7W8IObOcFtcwdqKs/JBZMa7pf0uvYK7SGArKY6SDN1MfP
8ZKVvDERlEB0CyjW5FO9yGDRUqILahlcIOaYNmT2pP7lTogllxSyrldOKdx7ukYF/WVZ+pHip4Ct
z/MFSEoNkbUqJBg9tyEjZuv8TbuEXIyTm+ACyUXosDt4UtNkYfMbzUsr9fDYh21TZLuopgDuM3f/
jlq+KLHBfYK8HrHEk/p2DmIGjmyCwq7hgujSoPLUXW6agetIwV9+PZr9KiKW7cJ1vx89/PLy0shV
WYJMgn2GH4ZdRYCe7nW3jw7nGb6IbKoPXklLb0KunR7r7bNGLQutx1wF8FZOhaGlHYUdiOPjRVhj
ieTWwF7hcHZJXjsedY6gWfH7m+r40wzGQwjo2rVMqLpibwxkb/Y01XiEfJ4Ec2ZiN7Y7BzvaPOk2
g6tbUO8t5k8RXWUIsZLfVh45yxORMMAVmxXS7+/vMEbL/Fvtcr4RmZrF5fBRCQgEvvkaC9kbJMEj
OsOpRX06gMbi4nspYdQGE10YUTBpE9JWqtbTuOKmS10UiWKDvxfNQnOCIKSzem6VIsTY62qu3MFn
mgrBGtoEEZhyUVt992zBCbpxQ5K0ex9B7qw3sOQDLRsGvLq4Ing1gvomiK2SzBV5UG2zYzk0HyRQ
EcQanvZol/iI4WTaG9ZQPGEojNs/XNOkE9uyVTLlgYWk//MXRYKkYGSuFrU9wxdZIWfQM/uZ91Fv
//1rAP/xU0BK7aXj5f5/Z+BpsVK+66Knji0J1k4evytWmuPQDIejfWmk25xaGHarAM6NUH6kBdA2
HXkpWo2wLXUEmmKH7KRcdv9332mefkRSw6db/3BdERbnAQY7XVQEgkEQJurMzAowacAuVTRekgY7
nTa68jIz8zzo/Em4/DmZS5dj4NPy+/p8/v4Xb//bf7zP6MrbtiTz1E4CaFkChjD1NIIRmh2pF7Y7
kgoMMfZuCh6wn9swssf1JRqAk4bwzQNMQgreSkD5j8UlM1oTl62eVpO4D6hc+hVagMLIJTAbuJMh
Yq99g+QN/ejcg8e4hMlX+FgUXBtTIHybIT/Uligql8p1AS57idctpAz8w6hhC3qjYGht7RiLG7l+
0l1VINhXsDOILscBDsgWN3E1nyS3zPBSuL9EVn8eKiE7D5b6wPd3Y9PiZuBIVYMb3Bn9k3bNS+es
6DXFV8hH0n7WNXHJsqa42iscnRDRJr0syHeflL2jKqPT8xR9JorsZQ0g02MJ/sOQ6hUjzOKGNNGR
ntDhOx2ATikCMjXUyaRkbCXaboVPUhyxCF0u655wcpUtG94wHtBj59yRO7jnl5W4OTKi0xJBbOen
o/5yMB/GlGNQJtiRURywXW0B7+a3Zo0qBUFqwHfRSv9xC6P5nBcehwUnzRyuqTchD0CTdF7EbhN1
K5ZcCl/pUiRwfp2KRQrdrabr4hjmYtgZD3LvBc6CJ7pqeo3GLj0mPubUUl3SD//o0TLcwDCPS0/U
dQnzy4sgQ9piKeWwMFyGgcC/LrobBqfU8+rH+5cGhARlku7jI50U4rQOfy5emQWhLuY1Zq6WStOW
lGVTSsjtpyJoOyM7zq4ka9qFx6OpVQr2axcqpwR9BZcyezcwDPZbKn7H0DLgTY+GwU6Sio4T2Abo
WGy5Ru1foYrAdOq213huPq5XPg9Fs80C49sX2ptauSEiSl5CinOMjjJixZvjgLL5h20AfmomfbzO
VFonxU2NZisMmycAhCF07kzzhyYsJd686/UoCUuVNGZ9C0M/CNMRkH2J0BhqBeh4HpjAJjrZCrP7
ZsBXT9w/7w6Tlr1/Z8XGU8Ahr+eavVkRVyX8VjmN2xmlhlZYUNMDOKiAg7m7sT4KNLhkvF4bYn2D
K6fpeCrYU59mM9xdUiE2VGw+dG5/CCt3mHZtif7EutzS1Ev/1mWYiZq/pS0/CztDS7qO7q9W6lHV
q8GCSK+E/IQMszfa5TQ1Ty+9AX8nstzcCJIEEZucaWp1oCu/AZXdV3Y1T7jXZF7CL1BkAW8egIFo
6P7lXLPRdL+/ntKWe4Bnbm1soma8Ibj6yseVRh9hKdsYAOhjMypaFzWPcp/JoA+ixf+/fLqH9J+5
SXwNqvNmDR/1gO7eYcW4qaKWFbIqKHFAIouYv604dSDrGBsbK7+Z9hICQXPpE8Hq26ikDA6y8BYe
BImb6rkSRBVtI1sTKe+Xx5fhv/jKobzpVYrIIAIIB+uxZFjxMVzsyBikXE5nmqofLQE1cNu4rkme
C+7fs9T3EednQdKBFT/IlNxBuyWsYXIMCngW6JkG/Qduj0OBNwqZZzUJoYFlahq7FpNAipBwQS7r
Xkr9u1rd8aixLvvVRdHRiwywyqu4+KpBGtaHehF74KaiTagVXJac9U8474f9Pak5NrkHDijERvPn
OtnA5DAFWvQKCnQ1Vyvq2o1RUKayjnjUAisvO5AhZEupH7IEqON63t7jlnPXFSQboXoHhROLePdA
QzGQJdke+lzgSByyES2RDaKKlYsQJJd4LXB2MgVxP7IkB7PFmqoyO/96xTcLvXtD7HfCR4GDSuQk
Q8i9K2UgK9gdNwgva2Hw7XV7OX6bPIrYPsHUfsMAqVwodbKk66oBFLSawkd1DkE9HeuFk96rf2ko
wtu2uO64uFgT06PK3q25l9MVHYN/X9X4GXsRMvE3e6q41o5iKe755qde2B2Jr6KuMTjJmbujyaQs
PWVDquIxZhnF0fw4zfb2uv8i/L1OeL7wjelu+cBzfL4VtO0gKiq8b0xZdOtouw6ArffVlz2dEgB2
x0oXlaOpziDaV8Pp589mM03al4nP/EhZ8YCkd6244OO26D2p/hd6kDmKsHUCDv022SH2DOep/3HJ
TpVWAwC3AE+mGG55Dn/C/fWjuhnB8QbwK6UfITBcOrPQIU69IGAy2YHyMOl3Newt/LfBapEKr4tb
D4jKGTjMZZZ/xtzg6YT2zhWp2YyXdY8HKwxBaVMEBNS8XCNJVdf5C6jV4Or3uvHAxUgFT08ZnCez
TrR/zzBczOXs4YmYNquOyA8fmjX5o84OgXwYHaQpOOH+Diy6wt718MX3j4vNT8PCqvZpE8snacd/
b/gmqUA/Z9UYNpa3kQmo4mixkdPp1i/egRWvHnpoJjDuviWj0CzLLl1O92JKUdBTzbQ/7rt8CPEe
nMDJ1/RVCF278+GAIIEcPc3vWKX3urNyM6gEIOtl4e/9naOJtCjFOC51vOICwA0BJgc9kkbJO0c1
52n9P2ifZ9s26vqnH0aDHhlQ+PNrzg/zmKS9CWB1HpLDJHpT7Hjh34DkixrokoC+wk+NWB0EXAwO
+tGXJtddBlyGqkTJt8XqTgP+XktMW4+N7RkZxJpFN+5Zb6cqyCIoOgnapJQgvB+/ljf2GZrDKw9f
CNYvHsL+Tik0kBkqD5dOr1nyQ8xKDOfshdx87GIC9pr38dHMRIf+180JtbBKtGexvqCSB4tVtIt4
uoqOHhMEgNSIxqBJxOCDyS1f3xUIyc+c2zPZgksEUC+mZqX1hI/pFhkxKvaC8BM/e01fItpoh+A7
4s+HuLrP87G2yfNBgchQnS5knAUZYmyyeWYU2O9OKZOAHo6w7GYEq0Wd9cSNC+q+1uAFQ6fwgstF
mlPKMGfKAX6pep5pGpIfLT3BJsTTF8R07/qsraBdcLDLu7Z+hLXj0QAqd9mg7dQ10PLaNyRf9nKD
9JWx2oVfyBcr2r5q+YtuTrR/ttv3wV46+Uudg0A1Nz6WtAdlWB5DvcvwD7qXlbl78YFCk5NtiwFb
Ffna/omyUpIAd4suyhopmTdD8G/2/Zw3Rv31wCD3kvQr/gSVBVibUn+f4AbBVdRMBQUXg3PD5S0e
eT7gIL+hBoqEG4Hfa++H/DfbMN0c+2eCApClMRtQbzwOKFzN4eJfUrYYcZlESuIOM9P3XynbQyc0
Z0Sr+OVNNkQck+ez7CHM9JX7jxDsYlt2C1+NIubl2LSPMFzwOqwLVse7QG04EntyrMjpsT4jta0G
EHBlor+eFYDC1IDIrFPJRrvUelxKSjSvaJFNF536zel6nR4yYEHBlCkA4KNv1PSApAwZ3cYw5eH3
vfP0F4LBbIYuRsSsMmxC7qSI2oLoweEeyc3xgH8mRCIYhRlDbpWR/dIYLL4QAhRvxYwG7iWHQtsQ
lfdVAtXdJ6RSyG2BHfvaJLhyKUrKaHx7lQM+FtK+Ip7LsAUnAsOXH1/ru+yDMSiB6m49MdzVMrB7
NgpBtxUKy7HXfuLdyeVLSc9Bdyx/EKW3Uvoli+wYpmADAgyw21pG5VseNDUus0Kb8Q4YXB3W1NEa
SHxJ58BJWxigeVW58/SMvuLjgsL9VWMt/5l5dyn9WQOdozoqLfWAHttt59/FM9XWdvq9v8jCn25A
6SHYacmVQRLZuAgZ+rCEp8F5U19erfuQnkBiFXDmujkn8CjDrE8onrIgT3EhUd1uJ95vUauvOUhW
0T8fP/IVZUl4Rviti822ZQhXLd5JT0yV1g1G++wet+s8VpTn+FULHs4orleY0OEJR1deWRnyivmr
MFlQ6NncJ3QArTyAlM6tMme96EdL5WWKRYppwWSsbg2T4fUNKzmnBvcRJPQI8LI9qDvpbcGrji1X
23j34A+HtMBoJgE2EhI0BILcn1lHUsl2UwQNm4bNCRGcK9mx6nh7cd1BIHzKBkD1H0OCALWRxmYB
BdE13uLBYwPPVPeWeUc8jIog3wATfoUHOK4aZ3ewORYd6WWeTG7cHI26mTUVoCIoSd6NnjXd+r6h
Hc/zFgWYAk2MbQ0ZT2mQ5gLB2xT5g7V3pVDKVSMziuOsD2aCZQtMau6GCXeyApIpXcysJxKFrgnk
3L5nX6D7WQqipORgwE09h3mCDWCihhGhteEUAbFP3y3lc4ax8BuZgS0m/2GEKyQHg5ZjdmNEle3N
309esINvRgcAXTMhitkFewnW6W4cP2CdjPHVNOvAR1YTVMS7WgkhOnN1WKt3jvRQMsG0risJ9PXc
KJGYaJvKA9zqfqQD/AlxQHF5w0tyhrzcm/7LU7mpg+p4y0BRxLFKqEgZiEfY0UYn3lVRv3ZoPYh3
ZWXfffH/SF3+A8moKt7JPP3jTXmy1/8qLblFvX5FdKcEmWh2yVhIwSN9vBYzcoGMwS2nL1PkJjJr
SLiTa1axBrL0kAz2g8rk/AHVrUftrxDluOeg1d47RZc2BhdtGghKkQ6sZan1s6KMhqR99T9yJKVV
43MljsZfuPoBIClvn7KzJQv0KjHNQWEINFYmhlqE6wBl7zjWIiceu3t8FfrCa57rZJvKsBtzpLgN
oGMQHKNK0O67yG7R2XMzuqF8/v2yRSuj7LU73RnV/lc6QKb+Ym0B445gvEfjC4g9wao1BMa/jqkL
c7pFzqHAyusDm7tl5NlmUCI8tx0cIosO97Mzxyq5JH2rvQJaTPaeAPj/fAxyBcsrJN1xpUYRtyru
UPldh/p2tIzcYV7q0oSVOZOpOqZyP1cj5zOvo6Yza8He40VU8qoXPycedD3PdYx4NXVzHJJ4D6eN
nCffSEzfe7jYHsI2VBp/6wsl6dgPZiE3dfyj1DKxX3Tt7yybWVqwwNGquKynG7fykFcch/ad3S+Q
ilh3VW2Ej9s3aNDdx9wIu/V8L/WMqRooTT59vChqPeF0h+JtjBKino59gAjG7U2jwcQRzuR3Fj9T
Gtp0IOk2MpOzWt9dyqNX0X93x6k9oO4ysyqoA82BvwuoJgkvPna+B3yt+Kg7XhTvF/geMP0/ORe3
kdYXP9Sf7lOIMQOs6cBrJwtcdVa3oXRpskiY5rV5fVqI/vVqQzeFMcJgN9tF319WmXS7ILmqPoGi
o17/nvpVg61ZlqY/FlWm4GRYL4BGYtXbRKeVOaYhZIS6Nds5KQTRVOnc5Clc6UoFD5MkELe1VDEn
7xHaitDhlcGPksANRxlCiJVEaSJZUjV56gfwULeGd45ZSfa+F/54bgWI0NLbSFPIZoFVf/QM4v9i
PKOvz6D4RbsUpg3ljv8W0d1IjEJRiG+qEr5pNR4ugiRgoXlRdEbr3eK88+5K9JGv0oKeoknrhOCJ
1R0vx+JlFD02FTCtAhiJ875acH8arn7bNyS1EnGb31YmgFlZQ2oOv10piICkXD6YwWu5utTh+Re/
odWYbypEEB2AN/moHG9bqqaS0shMExlwV25AiEu4YbcCvYBROG7uPLuQ7P0XGm14dIWNhcRMyfXk
JtDxmVFBcepWXcN1Qbtidgxp1tg0E6aLznxba7wZ6byiSTq7v5GS39DsMeMrvF19aNt+KeOVCMp4
ihYDxWgjIavsgxjTLfmf0VMx7diHzHHwu18SLEyelpkkk8pCOJorhq9fsGsFsElgx1OOpQ+lKwz8
fhFFL8zpKVhRModHTQA/id11iuKkNLHCDnHXrailzG03N7CjuhTW67g/vwHsRh1DqJfQGo+ef0Xv
9mmJofPNIQD79LkHF5X0sAayGowNMGmSdUvOand6tID/xDTXteLXU8FWUF/kw2KhujRbOD+bkQLX
aO3K+y9jVCbbpCJS6l1ksgF7NHGvAXo/zG18YKehFl15HmrrGlEb41IHoMPsj03w/wEhA2tkBbmL
VcpIBqrzbqYj6TIh4PI9UIEZxyNEWenUgGhSLjgIBAWWC1hogXpcM9buVT1jmz8MO2/g1gY6FK4y
SE9cGrmueJWFGlEyZzWyczgoxbQPWWwb9S7cKS7jUbyYjv34D97fKNlLxBIQ9ddp0LdOTPVDj1MW
uKv5C2Vph9o6K7JBidr/AjkhVsIhcWzre9sRM2PmpeGfJ378pUhxLLHDkxC3NG9BK8IVf1DG+IEc
08HYVeLEH3mvhJNhIBInKvf+0HCerfVUteEp9kg8O9fDpvGXvhsZGUNnmJ3VSnR4Pl434zpQ7C1f
OIpkxbjVSmzumofguZ8IxiORquI/6VqrAMAyZnNk3A2PbU9DOTI47uGrcbgY4UY5SC5PlvPIwuJU
9juu4EkFKc2rXwCoujG55QcZxmxGTmt/fTPoi/+ATulyuQkBbhTYkuxoQBGIApignPqy8ZVfm5yR
LVd3O8yVQfXT7oZw2WMQ5lNx8HoVTlzoX+WiF91jX0zebHM2xWyFHZjep1D+ksfMboNoi5hGLr9u
LHJtJOo9vSttIT5WcKkMpkUjzzTbaaOHeNlvb+nZQcC73VzX8MK7zSCsg7OyAuDo6+2av5DrWCP7
/LhKkCgg/p/Nk0EE+H46XAc3HfDyeUXMd/2y+omFzyWWj4dGWaAXQwFwFnnOxLbzlnZSyBm+vd+/
p1m8uoA5RQXgoTCJl9PtukhPGxvnlkPU1f9rE7qmlDIhQcamnIJT4Zfto69/2NzqeNjhg7c1Mjox
zbKuZF9dmd8QX9tNLX77Mw7meTAszh4Ede6t+E/9i7Dgj6q2c2MgPBjTdb2qN0zTYetQmLZ0Ut5J
pDPJT8/vKvud8VmJFfcCsojn1iNSxqGxNR3BpFESPuQbsSVXzxmCLC9IuTodII++EysShZGwiuNx
W4v2WNFVeUIZcp2QxE2aFFCOmypVBxPn1mCZU5hpbSykQ7l2Fl7xlFPfSU2saMxFpSosZqhkrWXh
vVzL9uBQ3EY4L+TsSv3vpc6LQAyGf4FaFyXZ1JLyZJDjosnNkFaeOYuGB3kBqEl76D78CncryOrA
7pvTyyUVmAQjl9v9Ti8SOK/ZbfcizduxxQ1SetNdrsehGBnJCHMCqNGUwH4HBTfwh7FHd9O3CsHT
Eivd296/MvXZYJ+0vpqcY/DoPf0/rwlQkO0nPhdFQGd2EIwbmU1S3GQvWVvy9hplzqIFqQEdI9rv
7RmFb/Vma7AwEBnL6k25vqUXjkwwGFPFrv7NXDUoMySww04H+rEtl+HHjlv99ZKuvvupy2crHTp1
SdSE/E+8CcA4WYWADs+XN9OQUL+627Ws5cc412/+f4yBieW2tNha7lpPGZwDET6KEEBdWNRQSc34
/NjQUPlmsRwmZx+FIP+tA3lIPGbCy5QyzxcvR/TQxVxFh7M96AxDyQQU3ymyg1066ueXqVqslNzb
CbG5akZABXrfKP/5tT0UrnB/C7DGpCxp/FBVEv9NkSlRmGyOvoI8upJplg6TWwQHACvvYdGMf0kV
FcfzTHWZ6BIuJ5c6F7fmD6hZKajqy6gwcTl/8zEyt0SYSgVi8AMEPERloLTT8UGAIjedOxPSXw+v
n53DIx/M+pp4WA3EGmVJ7StfrLV+FXRu9WHpr/9kn7twO8gxVrDubqh0NMBd3Inbo1O9tSFb1bVh
nJklmfZ7U8d0OQ3tMuEpd23C+xILpxQUb69JD8RH3oCk9ZIRTMzNMAsf4QcXuXOzqQehMWY9Ljn4
LUgCUojTN5ZBqDro/uak0wrJSDru37so9nkRs5cr5O/09ufUP0ZwO3UuQhMKti64tNERAzMlnbg5
iSzo9rfuwcyBh3n/fZgBcdT/jyUh3aGXxP7D9hvWCvvabYiC4gPrGr7T9GucnmT2WW9Cv/1qtZL6
ZWg3Jm8xYDztE4RMUXHjZRm68fVy8FAg1hb401OVDMbklsFlowd18T0vyGl6YsAELtWhyQjVUYlN
KQHjP+pVv+s3glmfcwstrhUTzuUjCdBMAzqsevgCw1SG/UB8Y1QOHuaTvBYDfp1B6PziYO2Zvd3P
txhJ/aV4h5g2P7dVhmQ6Bj1kjHEzkXHe+/m70y7kwwpxyiZ4i3GMBUnZIuRmSYhND+G16I/FQATB
qAlaWZRphnvCm3wd1Nc6tUv8tqq/Ogm9v0DS+70rzmrodGoBGyzqYVwdfl96tTBEOwAMoa9CbyAH
sMo+NBhsyHak+8lVp7/cjEA9f94nqsYTJLj586AV17FHiXfBysV3PInMglpK3Wht05Zn73GlVJ3w
LKDwuCLRDsKC7eW5l4ZgJbxO1hORGWHJOkJ5mxlJeCIIxgEWLEXrcfOnClVst1ekhHlkhdC2LVf5
FI+hrYnnTDTNh0vXdR9ue7yenu//wqmrrqdifb0NtVInv2YQM6NYaWuiyX1VT5DRAei8dAbv93mN
Y5ayNehSDmlI5vlmDM/xPdKsJHzodHqQ5L51lKvxiFjH9ajZfzad0s7RXJSwARs5z79TcEGEJX9d
CpSWwu9wHmoDpYe+T5SZkz70YjpT6MkygS8tAxMFSYZNqU8ZMrARN3AJlPfBrsv5apeYDybMSzDQ
/pTEbUCwYLu7VvLwDoxOrXtgiromeWJs8OPQ2rm8L5inzQ6sJ6vc+A/Kn+qpwaTVzIfI/ifd8gCd
qovj4RhOAu40CAgOWFot5Nn3QuPXqumm/eqvtrNaIClEIjUpDtfAiyszz5EUv1odgmvW9I+bkV0E
lDJnUOyVdyOxcyx9RWI6v9Iq/ZpS0HUqknH9ToX30OQHvnxAieGJgxjuPFCTSQNr0cBLX5nh7Xcd
KitxMmvhKpZD7kH1B3djG6Qv/+00ftALL6AC1q+7Gi4D785h67Afg9U/CX3lCaVvUuvJ0OtZ7xy1
VZ0qb+GeFVeDjOBhGuMAR2CoKHmkXmNxIx5byaIF5ezLAvDyuAQj51/iYTIll4M2WV0uPn271cD2
LMC1pQFSKZOd83bNcU/baOdGb9sMxWdpbBKlAHf40qlwUYi8k3ABt6NmtBVHGiHRotxqlxHB5opl
poJzmEgg7m+scj1Hq5EEJZ2swsX8zx0FrkJmyRsyTNHnG5c3m0ma1rKuzWYJzjnQ7Bs7E6FcKOAN
EuU2VjwKsbjDEm+IpFRhXNcqDSh1ZmFITerV8UopGzplZ1uRRCImojtI8G4DZb3lwv6XTFtTXBNt
yku2uTEuv9XrO0ZdEgMzqkLmon+V2uMTsuk0DSa6EQUqiwbBNBJLUrPB5PpgEGYvFFRCnbcaIIK5
j7OjRLktG21wX0EFTt7IZwVO7qUfXlN3i6wxttRLi0c7nSxzc2ANrgQcZ3JH9az1h/HYbR1wsRNR
wD+PC9xB4Qq7Bt3Vavu+1jGTSU2Po2EiWms9HthvIq2P7GWzSepGDO16zK7lxWWxsRgFH7ogP0mt
QhOOop/5A3JkvqVxbB2ga1Fhxqy7hkxJnF6ieOgUZfGcBipkDIKDqv/RW8NchlVwB9WDdDrIOQAl
zr0DLTtMGfhI291a9syiLKbG9T7CCinYsxZmMGPnVSPkNCNlAnVmzEkbapvNAxScX/9tsRayrn4s
jpJ1YD96KWSrp8I95IlG0ap7V26uuc/D1zmCKJWNIvtu4k536KcycxPHYUL3s5jl0+8OrgqnUMDc
fnNzs38+GC2NJizuh9FqI7sHIDw8QujnDenlzhjLGmlbZ/miJ01EGhi7kzVkOYxQCqhlQvj889tU
UJsW3+8mpRUmiTP9x+c4ff97nRu/9ofyFUy3XHTH5v2torDAj6n7BDKNVkr07ukvIhTfA7J79ROO
MY75kUPuDkuxilbqR/xZ/nptN/E6KqssVlC9G8gwyhvqZJaVHl2EXulN4AMvi3Ew1gvNZgmvYmwY
/LJHWNekhh2RfiJBlvAdUBuwL6A4WMF33zoxMoaoZ4oLFL20xMinYT1+6G+HTdpD7pxBSIMGT9vi
J1uu+XxvPcnEWS8KQTQEH4J1QVAMYXygsvyn43D2hsopGZPIFcL0iSdbQWaJJ9Fbj0nrm+d0GLsT
pmPvqsNc2hZ7spj4LvpipVtj8crACHzJsy1v5tyG+tI7+W9YjiwwXbDOz+lAoswZFs0NjMUoudD9
Dqwtm9ozhNoqhLLpsOs3k6Ylhws5RggVS8J2u81L+JJkPEQpuZx8hpZ/nS5MVHzeNQe+3ou6y2nH
E3TF4DPoJoMxQ2GWyXmtHYmnqvZAqbVLW5BOCLMCs6A8XEsxwyr4/TlrMwyk4HKoIvAMkmf8vmxF
GRJryC8nylt33ml4lyxNcn6HnY2U971RbHhQT897J2jM8PlDUERb9VkjM7gMp5+SFje5DdUEIB+S
9ATFq73SNWmkNl6vo0QJXYzUMaVh0XKOSBZ4q0QIM37sb6vwxX8Y2CxJnLr5J99w1VjW1QC9y+O5
wOU5DOwueegV6r2AouoPPjS2VDlARTV47FhPABcb6tdX1ZTvPk756v0UkWD0i4Br9fMis/BdusBj
UxVW1/NFp5MeUbwYNWEzJ0FbKa1jtqBxluQfG8BaLyuh8PSdgbgMxfwxuruCS4vzpOmM5LrB1zTg
0kwek5gWhh6ZVEioCSAAezOMfIjfb+INCxYzSynQVUTNw6mJnwiQseFmplfMKQJ4V4CBS3OwHIWS
iQifX/VO/Ug9FdLuKK9bo0TEpB73RnetGPiHQFqopdIw/LqVz3v5jit6azlhvzwsSScW5DdtwV7I
qAvkZzJJEojhglyHnQlARNvJOExvyErIHC7hOgYUmC23mBL5JccCtZr0vVNqyzgwPeXQYrSWJNi5
58yCvcFqkY/pU2pFryEZ7/F1SQv7wk3IKgSWavR014YoWUPNy9bvDSkpxOxGyOLVDOyWtEUk24P5
fmehumt4mUdmz8ikpc0OAxPze41riaUYBcUrmWpzq4v415jHJEG8Lsc+Z62iJ6J7c9SYvY5gyZOi
9MO2ch2gXW4RUIrodkVOSABQMVMnbxemfkyprTkMWiYiRslsj6D1J8h3Gp+N4iGKN5qMKD4ovEKH
rWM06opZ1T4osIfgOylC8ya2y3B4ElSX9F4AX6Gk6MhoyVF1wS0/kcPPvvO6s62s5UZnH+172+N9
fN2MNQ73wL9QcGxzmUaPznADh51X3o4qK7fbF+HE33AFtvM/0LOOdybEfGHWukBAZwUkaNIm0mV9
pbcv3y7CU+MT0ylruG8OYkxsECnHaZuJqANvE1C+wgfm9b6z2v0pFuqScE1XYckU9RmcPTTQYYI3
JLpTuZehJp0pbyUNJ71areUk/BXjlgb/Uci/aZ3Y+ifA2OZDyJr1wKAYG7UgdYEr3dOUaH0PV9WD
DOwEIoROYJDignJ9aDm9MuwEQTIsstH4k95bLMO9Cd3LY9vALYU51PYyrXa38HtKphBwBsVHdHPI
S3IXZdVnucyXtV5bmN+dEEUxDmoklH6/JKmgqlNaINue0C5rleM3R/uqsBBqOnxiY3WDJEbqtdf3
BdlArsZgjg0Z3hkRnUENkOwIg6VxBT2/p1azc/JoAaToqtx0oVeTPAeV9h0sEXjDYWy6wDD4NPd4
iP910gp7atIyzRrGbwxJXq2VlAi8H2/lo6WHTN9dBD/cUwY2j25Ut/QDl6+q+AkdLXVtDdCv5QQh
BHAFXszsup7MeVxva0fu41mBW6MecxMsqrCyT7uNbvfwXpWkHp3TcDPAoTckaq1hlCrc7HL8NLbJ
US4ZvfNn45MaqV+0iWsdtavwRZI563B1nj3IueoJWjV7Q+up+A8Co8mbFrVX/BuyZoulazE8wO+h
LCRqU1zXLZRRF1vfix6flNem3L3fiDmcZZMBAe/HmV6ePIHdy8+Up/86FjCuhbw7vRoIq2dSHWvy
ANmBSz7tMyjmT6KQVbEJTIZe1Ubu5pUZw1xKkuvFlxutkDWUdiwNIobLyAJAdb/l9pIj9PxXL4Yx
WevPgiZH0agrAek3KzsHdEuUZ/oKaNdkuitXAJ0KQ/aixHIJspmPwVJHgf+ICE4EDrILpvbjffIh
rpc62s6POgYi4ImZkV99bVA/BPbosaBt2vabq5amDBx+ISrVwGNURowP2QDeZJnIvMI5kOst/hws
qmjPMU5eMaZ2sjB5ThsYkizTAi/7sd46XBLa0mhWKe8KUrHM2V7jIHF7oxgiFG0hsGKzvUdkXrmu
+H/jYf69GKndC/kQ5Hc2U8J42kRSSyqBOcihrinzU/O8ID3oMGjfAzN720FvkAJpswtooCZ3jlqL
TF3Kjy994FNineCf5lj11yNj7dc1H/3/STgnlEIsQVvqj9Yy5uQzOmrf7MA/gjoFXlVpEBZGNJrW
4azqoKR4bVklHCbu7pP823x8/vHm+6CGTz4+C7Zg2qDuXavyei4bvQgbzhtLZ1ADLbWQFLYdB/6s
7i/WvppiiPTgyO1AvdTGR5C/ExlBHwiE9rVwPJXByxHccADSDZ8+o7VePpR5H0D6Dlrt9woacFbH
D4xfrgDn+lBm3fjJ3nlJv5tEiWX0NfXJr0cYKXRauuzbwhfBPEI8hQ6ys1mFZNRavPD1j1I401oa
DsKFrQWW6ywjcupAqSdWsCu2VZkRUh4VvXs/28+Z4iKOqXgVd8VAwAeT97GZBdCbe6f4i2MrHvX9
KPmlWn8ulTr35BDNU4RmLM1Mm12PchuHzqZ7/5MSB4MIn4OTxEZA0hXMvPzpCElvm9HZCdpxRT4j
zBxaktn4h0rvbJbMwNE6muprfk2ynDjpsp/aeH5q/dKKWxU2M/zLW7YIQqAWM9XrKDyTdXJt21xq
DqVQhqqR+W17gJErr8W+r9VWT3ZSjw/bV+yEohqfnfxPcHr0yVps/6WGG6DGQxy0Sv7UppnmG6bq
NFenD1ONPs1pREfFbcBlJ7mckMMv7H3akKOw0GFWjgoBozZDm+1bupnVLxXVNdMJB57DNYmNv+mw
vP99AjnmLyyLbK9Y34uA6fsGz7f5NnW2JJAxZsFA5uETfH8GuLdgp4GEu2iqZhCqh/M/pU4iXNta
CNuhY11vnlLw356BiPOpO9vY5RXNWwQlSvbz5GRudKF3pEFxwFy7R8vAXRYXBqIvFQg6npdzh/q+
7Tdw0DRY4hg9PymySGk95cEZdimQ/1SjZDH8Y/bfvBPVM88ApTXQpxj+GyaTHAK8FESbvv0Px1Hn
NtRptfhJZvQRp8xvW5PiiwznllVjJkW60VNiRWDEQ9SIHdj0GLPVoSd7CAWwmzPcMYPfRSpGpypz
sjJ9x1g1CP8JTnSPuOZKkCz3pDNgNa03/iQJifP+hxJk1/+RYRys8eUcj9M1un9Lm4gze/fkeY5D
2kpaAdudzPcg8ZYJd8j6g572SsOLLVFsR+sP6UTiiNdwLIne4e2N8IV9ztha57H64sVUA4Vd795P
qPZ3bXHXqX+SNHciWXbje9OIRviad1J49sYVQ5cBAyQ9viwgq1bp56FrvZ+ahB3uHeYLRHYi5ioi
hIALTTl3Pg8qhxPkoeXXEphxoWqsDSyAu2ZCN2YqWLJ36AjvngAqub5GmtowD6X7qn2L+yE1RsTM
1PtFDNDlFjfnGhpjJQvu9Czm8d/8Foez8liBZQmqUYWSsQk399TxzZMe0Hb2YsCn1jG4rgL+WO78
getI+VdI1EhT4F2piYY+bwB1o92cjcr/35kR3EixNdBzkmTqJnLTkfDPtzlM6+/VLzfscoIxYaES
TSnd9aCo4dq3Cco2uZMjPmcnP7qKkNRoNbkXBDC4TZlz2JaJXntWnY1WdAHM30c1exrfa0QnTen2
QyPgfstxoWS8bF9aV8s3mjFEpFvqua+fl6GEnRJ15zRuRljlw55+d21Ajct7kjBITfij/S1AjgZK
M//rGLByCblU9g+ywXbFisnTRFgfBbZCKUl9BjV6XoRP2kCjv1lzdiALRY1J0PLG5i9eYlPgUhUp
xs2lHFRU4sLvxjAqmPBTeeqOScWAyfokYCd6FUcq0EoiXkHtPWTzq6HMhcOBqHmuup5qkZGHEJzO
Ik6bj2eyJGxVPWLrXs7g3eC1hQo+zO3C+d1sRbmCKgAfyLcaux1Y5rgOZPT+RMBITmXSIF/WibUu
EzElf2TzyFYKPC46DaoaPSnkeKj3Ykp5IItBDPtxbkDyvfE4y11HY9iWQTSKR0bqdfCCYqjsAylV
VqU+RS19ci4Q8Vjnb839gcZL/O/WnuhKX6ygJLVZzc9sipewT+TIZBYbsSxM9idTie/uxh4R4ElB
5BwjmTIfR80gIaGciox+uGydoj99wMKH11YHU4b4fUNjM9vLdsAVmSXPSdsR0H0N2W7Z7e/RTcO4
mXdz1h5r8NFwU+gZlp2haq9VQfzwKBIiWaKgXrItmSeWF7Iqb0qHZStONAVxJS4z3YBDQ9FghvQR
a6eWqIM0nzSmMdZr8EVdzjT0z76fHtUE/Qf7eIuVUAETVMZYyhZVfjo+bckq3Hml4GMwRjKosgi9
iRtMYpIcLCVnI9S/duZpTKGZ9BuqGGvdr/3qdvgwFVJ3xqntDYEKZkzk3LRiAwfx70XSGFTSWRJq
euvBzm5IP/QANVj06C1OWkeC5cwO+uvDT3WFvFzu2dSQeXBFiUfjAp96BdlRyAOtpW9UX48nFC7U
W1RNqNTpSgWmORUSfVx7SDJ+obLdWYnD55pVJbDleWXi/vvYqGD2/P+PtNTsogJ/aPkkGF5wzZkT
Q2KuF1rgOUrDbcSoXyO6BUxxvKJ72uxuWh9a2gpd7o7j/5rElGzyvT693PM0zmfj72VRcBZKLREA
VPdcnWHGr2GoVqz6yOWnBe6QUeEBHpeO1HNNwBGsNwtFJXW2lWUvGvyhKK95pSdHf0XjHUZ+spVf
hiLygRX8iMIkJNoXZjkiNEwtrCzZU4aSDW3fGAG2KmY9/r3AE+znTt+GSzuN7BN7oC9pwASzmT05
hs9bbP/ElcLzxXb3Jaxx3oJFr4RzUxSmi1Cg8grmPeNJ5coY+aeT2didCCotkMAvctTSXJO1xy4M
Bf3eersbPTJcNd4ahcx0IOEVyQ27CU+mLtuHiXprBnqxIXJWlCC/ufA9lurlPlWxNS0f9Iw75oB6
PRoVJ3BoBLo6wvwCer46MC3d09N2qGBM9DDLA7+q7ZhrjNAS1kz4L3F1QkXUke3cs3pJNrrtJzRW
vLwWQ2P1KKA+G+zixQDs1hP2Ix8VlcDX4ulzdXkg+B0RH1B/XgcSwjN/zhPucplNaSola4e/nmdF
BJUGOQPlOTB3aJTkZg3V+k6M/1sN+6ca35tJ9kwejDG6Md9VQC57TlYr7l9pHAUWkd6e7XEigDZX
DF53HtDpSucvBzekCs4OIDtFAaSMSuUTaYtB/62vY6bqgWs9ir54rthn7NxdMFmV3FAJuNf548cU
8SJVMcPpnDtUblUfpokIwACZUmenVQfrGaZndof288esM60Y8QeHPmWiBmhYkkrDqKoKOM/2fqCl
jlQ+9RRZiWXwWNXK1dul5/yZhyKf8YvVFPh32nIfv670ZmHvzl5+WZwblvjGgVP6DljHaiwo2GiZ
Ac3HYVO8XTyV7qjSux+JKrchOyQmC5A2/vRHkAXF+BXjnxleGx5rMCX96wug1zyYnBC+kURSgXYd
LVP7iF6KZKsLUPvQff0nEnylWbJGlxFSNTud4Y2vNEr5CUQcAxnsQEiocHV25LQG7xfrPmBiwjIT
TeTFkgI5PwFZaPqUsqi6XvhiIzQ4DNKuVcwmfq2Wt+t7vvEoXZibFmtYhoWyBwCo8J5cUEUB91gt
YEmJiQ62JLYrEtsdUV/zyHy+GupsfN04BtlTA93owecK895DaQewW3+q8RjCaBL3ORY4h2uXVR4G
wxZSaLuvvGZoNpEv0JmoMkvBHrRldMzzvnkQsdeUf0FFLs4WAziVKZGGj+AKCDMWe/ig6oRfA5dJ
Q+M1+hWevR4R8AmD10T0674ktlvqb/3yYXL9G4/0fsqWIoBo9mbeDYop2ZPBnMioDv2s/uzdaomJ
uHMxrgwTZNdT3Ix/DDjtz+19fjbV3XMaaQEg96cvWV79rQ1fzsK/6dbADf4rJflTf+dr+BxaMCzM
SZQAMAngL6IYF3rV9mu8gP25zzXM0hYQMSKWL+F4cj8FcuWi5mH9Vlwq1gylNb1175hMlVJGfeXO
/Bu78srhygKyCPfQI3/9zr8DJOKL4VrubG8qf7Oez8xgKbZsgHOn1SskASIksTuU/G4wM5y2B829
XWms1MMk+vY/HsjnwWHeUYvBvScHXqB3XGA8BrYd11z8WCwxqx84aWQ0YEGmD9ZBVCXEhRLdrBw2
kfYknH0EZMVXi/Jgf4Fl37iQI6/JCFm6lVdtHShhWOlUtN56eypTBLbcbIu5/9tSZvjHosk2KrCy
I6rrF4cz9ONK39COhwyHhQZLdjfHEUnG3wmWEDJPLOX4Sbs3868cnuWEb66WDpIbfVubVDY8vQO/
Ku1fIAMWi7a1+5Ifj+aj44U1WvJMcxMTZhEww5/FFN3GC137Jz0xPdSB7Lt+ruchc0XCjUcAD2UH
sDPNDXCRwomXKFiuE4mjzMspx0l57AVFTxKshet+bWMkeKwJxgXIgB0kEvlR9AAdwUj+6MPWzj0n
Bg+e/1wriQCNQ+SyCGUYKJhsmzB+tmHP0TR3i1FaBw+wLBFJ/fq6LT48h2A3YnuSQJ9m+Hkhx/R7
aE3MZAnAUDYTnzijNR2Rq16kVtHarrVRxtvTw6xg8aGBYs6VAa2Wvs1S1sHe4MXiB7u6a7gpfzBP
R7QLcnAVpuN2tAyGb4cToozQZZhTjegBXmUqq14sZfaPeJrnvxB5HHv6FVIS5QunKvosXE6Dt+WF
UVWOms7ahbv/iQT/H3GiBi/ax6xHCaz8KpPX1F83UNutgzdUakpR9cyDuoVP75lwjocqj0ISdVsu
x54tGo1hrgho46qvFr0lQd3kQtxObI/i/vN5NRD/ZzfuQYHaW0pZAzWwI7lifnOJmRCSjo3EBtJe
+OzfagCSCM6uC/pgXkzaq4ok4u6uugAzs83F8ViZAPLT8N5xOmgUwehIXbTTdnh9LIgNTKZhTaJU
amUYGXmm8nZqQB1bm2BE69R2wNgShKwd1c+MFkEEnC2qToAbCJBHDB0Cvi4UJwg/AEuseF/JqX9e
/Lmo3N8YiLkPI3Np4apnEQ+J1fWvKfxJ3YoNOtBVFZ1oyEA4vzlDQSe1MsjUcI/glC3lro2b7TeS
cJdszwseS2++EFqzDMhqJLeh/fVTmtKwLBRVqi87hx5vD1kv3t8Uw/He9pgINuwqnvSg3IR5eVE0
0ksk0jk7qojm2qob7sjtx1FW6z0j+Hk4bM9SB9mb8jvJwUUvZbZazEXqrZSDhtaayAzBxhOlbZLx
/K/TWujOz7J07H9izH1S/dKdEfW8kVgTQpb3au3jKvfRBUKcZKxLe4YOOVV6k5rEZPPnEPhmTjoU
7pDOhNRpBtrC05PFCVBo/4PzhW7QELt/ibuajsOSUHoJ4qmFmIYqyHeIxfZAv9Aae2PZ+G6Kj8Sz
kcMtQGj2SDKF4WDrtgG2UbKmZOAWn7KzEx5OH1hsT4+VDzb2BNu2m/NMkWfErrZIGeAtDVoXRHtQ
xl1jhp07kSC4ientkjyYNqG8RX0JIbvOhVlABsLpyJRHZdqHAj4zHB/8biIa5ezQvIfSMyUT4R0I
n+YQGkhOe/4Qa+V9k3eTqcVYESH2RPEBqNaGBUpLsEEeUz+2ytirLNogNLDuy4piTRrzgHndCbPJ
+hO1F4cLfk7NoC/HAXIy327bkyXhanrnhdLeU/VH5QRFSPTzNM6+Vl6S22SRkzQTuhMfvZn4z7W5
Cs57n4Nma7a9Cw5+NACZa8x7ukYQYUCnLbKOn5KpQhm1gtKay7zYQRIYYQGUUirtooYmleJfn6hI
6pC0JffMTCPWcyLI8Z76T1t16O6IfASAmKDqtd8/xoO+cE7t8TXWLiFCOlGa/zYxt+ScrL0UA5xT
zofoDKxIYCGQ91Gi4gTXQE4gEHD7trAdZBNtoJeKsI+QQCWjLEAMGPfgpPBoq8gUj6A1oqHT7MkI
OQsrrFjkKkU3THkF7TIj7lENu/HlBPkSs7xdOYEU36vSekgDCn0T3omSQuN9xjIZ5keZS2M64bLq
Zn1GpVPmUuZQHqfDxS1YUlYf2R9QYQ8gk2gMl4kjKq3lj1WTFZ5FjcHUUvFW/T9GRjy3a9BED3qY
viJc2SaeVDcz/WWWan2PxvFn0wariSGk3b076UA/X7y+jnEN0V5ovUVMbcNLXXwkhmJM8vGL59y+
58UDogSZxkRyTS8Lz9BtUGLVjDz9aQ810Rec4qHXhr8ejeM6uHk9yvnBRWVqUzQRlBaoO6MpHbP9
xT2hkxjtViTx3wNQlygdhh4K1uZi+5NlYUCXsHZhWJeoRpNv94/D2aFMrx15L8aDZZrX9nk4Mtec
jiy2yu1ej1Nutyak7vHAwMe8w6FRyx9hk4GJ7BfMCPQ8cxJX9rpWzZ72PCuO+ExhCTSRABcMSa58
qEZqD5CAH3QzXhkZ3AlizraDWSt6boiLnB6D661MS0/XaKWcSK38Ey0TGGW6mg9QbTwF35sAZZ54
RvMcSImf54nFt4wqSbFCstHxoWAaw3ywHgt3lbhzZ0dcplviOMjyJNM1ABF2PM8zqfcVFdiJYT2g
nXGutj39+CGTnKuG7yBNAyUO/DOZpVPYQmYeB+mTlBQU5EjKv2Y5Gz7egAARpiVGYBfKw+2cMt5M
uOeW3fJC+bwfQEp3B6DKjeY+IYAm5lnN1jYjOTz38Ppx7DjrefVsqUw6kYdZ9EoehbyLHUdreZA3
l4n5lhBBVdydN2TpqRa4JbrirWCp2iDM194J8gZFfn2oNU05BGbExZ7tjYIbK1Lr1OXkPJWbegmm
WHOgmC84mIO4VYnodLnhHFEzUG5QdyMXVPzyAHbtjgUTY9UBif7ZmE46cJu13Gkeenlo1Vyt8VoI
4N2Jx8ubNW8swnF6if4K+F5Slu8Qlat+q2ZIojfd7wJKjKMHtYlj18Qal1klgk7/iTK9Oog3gU4/
ZVjJms5wNXUdBBZB26bFiO6evtMVOoXBgNtfQ0mBS+h8ZdJglKnbmuACr2SkTbbbnKml4coPeShF
rVDET3mIf4QVSl4kvZ2TcfdSTghjQfKfemJiikQOC1ve41tsI8sC9jRacNrbJnvePF71jetgSPtm
CNTs16gA1i8Kulnu8U4Fs51sUQWHeTfKa3VWv9iKgb10OxFCgeWI0JINV1QeE8p3KVy5LnNp4mAI
bbCigIIYcrOqFKbB1M6ImkzBkncdTQ9CtBRxRZg44tp7WSGm0ddiCHvBrdHhYzlJQj8Q//O1Mxbd
FGfYRVUOHeAD3I6exnvksM23rll+fBjRq7gKxFG78iHEVhKLeaIp/2BGP+FKAKcczXn0VToorX4K
YFXcYknT4qi09Ic2BWz5nI8OzixxMs3VaOvTB1l6wUb6/5TIcdm3qYnO9emA+d2cHgaiptnSss3S
8utiFUgD5jonQoijLPTUVLv+dZ0JuEO91X9DfonNPAfKS9Zw6/JAhglggDvCNO3JttnnXcdxCCnx
EK6fMQkNqiKJHJ+4yB44NvAfTcywlpBiPkVCzRWvpk4O8gIBHtw8G7GKcgFcIygbIRi9IovStT7L
KfEU/YWGv4YCPqV2yJ4ZxtP+d3c1Cj35hnyJKaNEo3vWWFD4Pb3KprLX5//ekREpggA5eb7Uy0l5
fwe5BVN6/ruym35rT9MdKMfrfyOYOhS9atN6sO+bCNfeNt6Ij66umiV2s9TgqlNalsNWzBsBE6UL
WYBP78nBkgaMSokBU2SLhGiACZKYzVz5iO2YTetlLC88KKyLxsyGksIdSq26D/hAAwBeYtxrvdHn
5aj4lUqAYa0walzD2k02OXmAyAlldAEmY0vdK1k6bRqS41Gpy+ib7KF1vl5C6pD3hQknyQNX88Ne
akzelzuOZeVUz7gNVIzd5Q==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
