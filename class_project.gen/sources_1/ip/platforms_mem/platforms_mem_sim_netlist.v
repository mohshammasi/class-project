// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sat Nov 20 23:56:50 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/alshma0b/Desktop/class_project/class_project.gen/sources_1/ip/platforms_mem/platforms_mem_sim_netlist.v
// Design      : platforms_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "platforms_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module platforms_mem
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.5432 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "platforms_mem.mem" *) 
  (* C_INIT_FILE_NAME = "platforms_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  platforms_mem_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18224)
`pragma protect data_block
TTkaROYrUJtN+o/LTBaGuIEutxc/ywa1iepmyPm2rvpyJ10G0FkpiMk5fPR+o9269dlDFW/T/730
5C382l3lmFVRHM/6luLcOP+Te50zdJ1tB8B0/cggu0tgjOQxufMWpaanA9qllT8I50XLJmhToRkO
0dwqL7ktzbuZDUSXEs4XUoiPedJjFgvetcIXzkEOGJtxubrvv5Ykt2crow48aV/J/mt7RxXUoO5c
Zgka7RQe3EZtmKHHxtKmQP2Xekb+zLbVtJ3zUorn3M/Vk9RJiOYXT+mgLIsX5iGKZqoJ3cEVGcef
NMSkpeva/j+pLAAbzwnApFNLaADoeH/BX5QuMn6HvoGnWsKVq2EakziF7yuBCon7u27JGbJ6vTsO
N+9UOQ9QRc3LkXznEgCi/V+MQU5k85hhkj73mDv5CJfCKUvV82ArPoTp2hi/YrdPXMT65l0N6V9R
HHBPfzjlutRXzFn308wNRhN1fbrpjoyL+Yr7rYVbv79xdOkshd3wLb8Q4NrAW4TYZQRXM+qHxK76
92mo/5b2m2yoCyg9Ngy7KK4fmcCVCTOW4aIHKZqDN38TeJd2LmWaFCFTqYCHbxknEY+GoDm673X6
BZXBKmgOEZ8tpdlUjavcNUMfTtvpU1o12d6tz7lEzR26tQ0zWF26Vuv3iTcxou5YTzgzrbP/qmYq
fFbs0vBUhpquAAVdGbMCzTGJvJFdNm9NlOFgRLthE6cbJxGlIRYyF4FYuM8ngNoe6PkzTeWnSQds
XLX7kr2q/uUIhcJ0BHiQBzbJDwMOgvCgmXwznLkoJ0Ot7KRszqXo6FPCJPdTERyatv/Gu3ET2L40
n8o43Elb/PYl8d7zfY2PoVVW57sVBDRTb0JJXnZRS/LpTVWPotnnYo/J/AIt2aBq6ikcpUnNFI63
QRFN1LlAx62HRevtZwobi6Ix9kOWStlwoq+/trgAxqrqLAIlIvCczOvPgVAf4FHuv/FWRmCsm3S2
/kBLji+j3lEjujHn13v/vWZRsrTR9v/0y2E5XMfGFU3P2WASJHrqau895aXMv+W7urg9aODUUUt8
6ed3jxdZWaXBBMT7JrfNdJEzkgxKjebHDCBJZzHmNO7UYPrkv0bdVk3Z4CkPKRP96vbx226qb13A
XzUiiUWJTkPOgw0jYIkRUGwecIv9IeOmCyOhcadG0DubTR3rNDjwBPXDE25TJMPUppDqpPPpIw0V
NZvLg+jS+D6yaAjZznZlqaEwu/2HYmLAeygCqUAnTgpD9xS4aORxtHnL55268F5RiE4FEVFQCtvP
72BL8xKcFAOUNlYxzD2eJwnrjiAh72F+e2/2/MfDOBuYxkVDk1wItL8UAIeLcv6qZpL3p2jBh7kY
FJHdYeF3PnvuTG5FBCmAJqjQe3bjOCiQMI8KX9faUxawjbqksUWflvLLvx1h+52wHCokADBOsflI
7aeHsd3J7TITPcCeHhNSpbpScNE6KWCqvVQcOe+cS9cYFaXPnCR7hqwQc/q6sYY5saX0hHASd3uP
uWBI4RMejSSIy+KXHkFI+l9CT/fbgT81R4M6FPufrp8fp8iQYFAFX3kZBNlEHqWfqk28t1lb1qjL
A5hHYXGfhdClA+UduBIox/N2DZm6Jb82Cz+jn9l+GR6h3qq1gU2iWNaL/8d1dYR9hcHeVvn9Ipoz
KeG8PevBaLaGVHTwtVwVI8RpQFszuI/FNiYv9lDO5gBHKoSqfof991udxQkjxYCaMCRYRnAYZ12O
uJC/qcbmTVdUHVF5OF3ceXojxyKKMHGFqVYhjKVrkxUh1045N78+GbZAFVbmgIGOYIg5vnv5c3jJ
Okf4K1zfUnt0cTQzuZl/DO8wv1g1S5YOlDxaKIoasaXa13MCqKwpIcdmAqDd7EmLUoBNGvz9hiWw
dqaLfNMB+SaDsoE/R2ntMO2Ku7BMaqRBf65zcaR7Jfnq7XdIyPx8jiR1/V8eVTMM2Tb/vkOtXx4s
hF/FzxUSs+T1gvWc0reNiAWYIbaYIvDXFsII3DV4KDhjgUT+mFkFBfmO8W9fbsW4Hz8K99YFQn7W
tWBDCamzcApZPGOBosSXqsdPjep9gGfYT9LCXy2PfMM/8u5i4zwt9F5uQzCbk8xeZpVGKS0HWtk+
oegJWfPRSi9DdDS7nKUVy+pb/8egrno5tVY8NOsc2e/wzK2wW2XswYqKZzeCfc7a3RSzBUdgVhpo
xq7MfLhf6GqynHxNgSm4jgcvprzNRRNax81+RaC2wH0WOZm8gLGYlO62X6UyvVDhm2iEX2mYMA/2
metsOKsuEiM+OaeXAnFeDSnh+bmTx+UFsi2Q+Cq5oBeNhHtbGnoC7T5xZWS9+SOJJyRIJTSoijig
0zmq2qG33DtwT83UgtSkRVo3qgYQYHJo61LBw2vS+yMwIl15T6lqj2I7GWov2wMZh4EHOQR7WtSE
J05IetfR6tVwduUvS+m8yjrt6SgsvTB9iorHO4cKbEEt65tyKZLZaG4GsSACn+kMDgKIfHt9HiHd
6L79G8f3XQkBCntXXbfaCciuZld1OtOvBLv8pXEuzI+mSXT+0dUT9BX3McFSW98XoxnREHEzc1pk
3odiQdiqYEK3sxSpFqdszVEVnsYPDyhnTraKnIDXqFiyWO/7c4Sv4MntDhuPC+/LjTR0nWMus88K
dax4gv1A6A9VMKg5ri+nT+DODrW36iLi82yNfBJms0qQRKdPRSXGcb9aJsHvKjZO5NlX6lDgujP9
CQP70H267u4GKx91UdVvD3G4FHdP9CG68TGRhHoeXCGz4zhEifSeRsVpSi+o+Un0IU9REu5OIVmB
PCDYCY9t7+fXmRv5TX7Hlp27lVJsMEbvm//ebeo2VLqTJ+tdKOqauweG5yPpvB8qFeViZYzw90g5
fH4S0vVyzilyJy3rihStic3drlGQkuYLqAYkcaDMevy5164NMxnnEIsHxpZeF6n2YLjZewc9KHQt
+Y7EVaFHjA+Uh2pr8P0ozzlghxLwaNek6MHbaiTtQ2y5zotHkTH6SnpCUWxJQeA99WnbpBqzoaYE
zNgFoQlH4jt265x8fSFb0PHVO7Plw2Wums1P3p7O+uXFdwTdJmNgrbLlrDs/KT1MkkNuCuJnvGgt
0HZ0t6w2R/Y7T0CrTOKakMY9RMZGIHppY+vD+TYdaU8mViBmop4NZSc0l11nuW7dpp2T6N/AkUq2
d0PdzcWqV5KkKSETx51jJpY5m2nemmzxH6FdgwNUO6ydkORPQo+1mrRGJplAwQjJuQNl11nuJcAm
7S0Vcqvubpvi/svVdg590Hb2eawvgUOe/ALBti/wIKcmM07LO4g4KDmCvtaLqfl2ayZNuWeZ6SrX
zYdsubyni0Y0k85aIwtcKO/m8JFUrPn/LZKyYaoUc/nvk44QY2WyxiiXkynhKT4IoeEliY9pGJv0
/31Ugvz827kTC9UOgPBpGrtzgXfMa53Waa9Ve+6jd4fXwRrTQliHZIwXECJEf0QFktNvZg2Uv5uc
5Huq9y2oXawKmey3zwcGg+mqdv9826ldSJu+Eee294pmJCZCYKlSqCohNqeI3eRycOAXXRFz0hTi
Lez40L33pLGQg2Ca1Peqql4+WOlRDmri6AYIRh06Q7T+A9CBZKqxF05IgqBi4M9algY3AoHfiGlV
g+3Ii1/KI4BcgXyIUqDOzkjMD6mxxvLaP43U6xsfG60unzfc2g83eaUzBIkk4FAMWJ3fxn3NrIEU
WRfyMfdtY3j5A8FTzaWsMpqQvXY//eIqPHG17Hsgr0KebLEoXmoOE0CwZXg+l/ILPD1b+d1GXGI6
rW6qsbpvCysBmxDvC42JcWyY7V/oBuIWEBeXmbAaGYyceMyE+3KPiEoSufmhMSYLf7jHFSkCw4bU
CkO2+nT1lqZKoebHFoQjAu+vU6upBDxrPUKGd4YVW4qlaVNXCLyyrdK7XOxe3cy8YwNFgRrRMM9f
kmhLuBIqfXx65EFexnZgQc9wKMMbvemxf1wDQhODlQOaeDu9YZtFDSoN3yZXrypCRXTubspKUK6t
DMt4Ti6TsijEL2+bN6FUwjbQ3CUo4s98auRxMf6botUxKZl5EibWNNz64Wh1HD62ZDqaNnG0ksqK
Yo7Vh0JoqP9kUpkEQz+g3/idpY6X/FwWsw1emfILeCpRub9PLN3WThbjpDge/SX9AwSd9bX58IiE
AtWsODVeQcMaUlw1tvr2UmTWTbpVSyaxvJqZYpQZs30HiwV3NlEWjAUw/L+f+xY0Zj+7cXOjnD9a
0h66hICQ240RdXtBrRbTTUyTDelTxHrRzHwg4qV9nbznnAehxBNbTlxHMdPs3b8CJ8XKJq2ltCdB
h6fX4Vhh56a4pXYjmcJCJP2EhxxoiWMkuIg4nxNVGszySe+HN75JC8Bc2kJtga6ZAmPY9W2p5VDR
/DzDghOLYpt0AcdrDHTtjyVo0kDATWdiHq5lS74YdD1aR7NA91ynqsyoUYJ6+Iz7kA3cV7Kg/vjr
VY6NS845+7YOqkLMwLbmDXQmixnK5xp4NHQW/lxzb+XzJQz+G/Lo3titLo9/q+RXNIkvtsWalz4Y
MbpJFg251H0mz4ONIcVBIvGvWYaGZyNgqJEQ2x0WJf6gIfWXMmE5LG3+aAf2oAv3JDI0GrOXVG8p
dJWJd0YyPUlSBPdv9H/4N1tS9cA9NXxOYvU7Xa2Wj6SXPnVrrzvo4q5kH8g4qMiyNNRfGyBNVBIq
/gFkMuSVsbTAzBZrJOMyguPsiwqytpxQ2P89vDdDfczvm1fTfEYfiEXzL+wF7qDUVZsUj9/zpWKa
sGnXc6ehBkJKKuO/RdkS5E5Mekv1SmobX4P7Nz0uPfPRVfk8q06JlKddK5Kl6ytgM82k5GokYHAL
wrh02i3F5N3h/iBnO9uirH2rbR1B3E/5Y7dQHM01+6G1rvQmxvLVJKjJuoyPtWYRpa07FcsgO43M
QZsh/j8AUOqLp6RS/PX/Fy46WFtf8KhsX7+8hFHaNRarCau5bmVlluKp5e6jaLHw9aN8QYCeWSt8
OCicyxnAKz6QG8l899RxGSLgozPv2F3sFym55NoSk8AvoBd3RggqmGp2NDFvdCsiOzqQ1NKNa1bI
jrBuRlBTJd6Z9mQ9Q7G4ZbsC/u2AFLIwANeG/E1969875CV4WiKkUqyRag9bMzwzsElBahfnSzXO
FSxHihmH53//RpXuHB+CFKTASoc/0peL1sXEzSC5nhuqSlth9Bixb/4btcP/bruvyrs6Gq7QDWB+
9zUOqHjeoUBsDvPYCvA8dNC3LDEuieKHzYTJj8Y8pETYVy6Yt6s7ijtSLzU+kOERqenLFW/bygRQ
dcjkkUcuXGL2lcsqowrqWG71LAdMRHPw40v5ENdvUfnCWqAWNXfV80QOenXEVQti2hciGKIVelWW
IAenKk9mLwc82rK5wUt92WKdklDHFh4GK/1/ZXb3ZcOXRoryT2igm4bZJuEWC4EjWVYsWkHscyNn
oVBVGJdC0O6k3gTCoVBgWtEY98vPNHarIcnB9M+ItAo3ryL+rFNpbgoEPQDHc/Vka7Fh3GKy7SGu
OU1IlCvwEZfDgdwwCXNlVXt4EQFZCRRLkH9s/quNvN3O482Va9slKPbZrqoLzFK8mGD8yLOSl0oQ
Ogwj5QO0w6KI7hXn8KXXz7PJm/EsLH67vDctjKRI3zEs9DakRQhYXXZ9jCo9503fSA6S5dQKNgkR
WgHRRC+Ylvh5q1vYyS8Gupzb2icH+IW3MvDl58cGe9CfU9oyIzVJO3kw64GHnJaM1hshKCiFDIne
+zqe/8i9AbwpNWOgDOUe/30zwBTmH0Hn40z8/iKdKgkwgDGTQZO1M71wnUNpWBD0DF5uuavdTBEc
o7nIz7sWoTHgSvX3PUqKMxkT+o5MRzDH6e+HZkUWaBoZv5sL9XdNgSQs61hRKXLar9DT44JACerZ
aokI5pO0+qc/3BYx2pzW6u8rlW0c+DAydRbppQ+YM6nBiJ549qYOmjjuGecfMFSxuRf1Zoszxr64
zmPhKCuqQgTGUh+G1cqdSY7cDyoqCggYVzMwnPpBdrXGsELADh5mzMMp0dwwXAhTEi281t/pqddm
gC0j28/YVrK+ld28ME0/znxJOPcPI1hQF85k+RWfNu65ebOuHA3KsLolUoAOr/pDU2yiQnXFfo/m
cGoSVUEb+5nRohNIImTvsvnQ/sEI6ClQO6wRKWpcP+uOnKxLUOEdjIHaa/I2dJyom1VpXe2dGOiX
DEp/d0w9IiiXQHsLsGAxtsMuHPkxmxmNlGLG8cGC0ErxyxAS+7Wjx0a+DH6p2h+xcLX3TNh3+1zq
BoQ8FQuJgRfJCBbvLohpfViv0XRRsxDqMxcMOeP4LLPRrwMUKftLkI3o/4cq59GCKLfbfARXehfq
/C8k/xwMtlXrwxR9hQ0544T8UD1XNufQ3wfSlv0X7JqmH/h9piFZt0hT1IMJKAz8RnR2BJGm5IgD
jsgs7uUbEFi2d+hHIwtwJ87X+XoVI7Mga9BlM3w4uoF6FT+1qNemXXehFWCFXlh3PBiJiFrbCkMt
a/itnZ/VKRAcIVCW6AZooal3vGGOcNtyYZI3at19253awUW09+7WwmodNx/glllhk8BB9eeXB1Kp
LK7mQRd9fewbLiinQvumHOtYSJ6Ex/3OlDdMGOTqNeDqcb9P7oEKX9yabyB5cW7HQOau3RGGdFQm
fSJTwshcu85nZb74yDhI7yn7fAbIlSCUXQlRk4b8x3iu99FVqIIqFcqvQqEg9Xiw0EjsfuD+0ewg
CW/uxuQxgmq/vZYgnpXpdsbrXxtHCE/F5SWoFp7rZrEZkHY2z54sRi4+YGG7SfHB8S0oGDLw0HEC
OuSUwWTWMjsacQeC0sWB7V+NbHC3odwHQ40psX6MJ1qBf1DCjcNZFld/gHIr3MpYvG6FLFZEIjU6
kISx+VLBXxNwNk6eVL94Op+dJ0JPERvjm+a4iJwvx63veGJTVDqnwpOlZtW5EgBeqdn5QxxT5q2I
7WSUEbpTfIfNkLlN0uF32qPXOSZOol2uS6ETACjE4kbkOTOIivXPGeHuYkA+ggYQUUYH3zv50OKw
pus4yY3mEG4/VGLbBnhb5mEVqoCysqVp9xa0lP0H9vEJ5b5jTG4Ecs2ldwGGEvs8IrWcjNjkyPs9
lLKGAFTqZx9vVc9qnbQ87Sfr8ZMwDs8EWw01JrUHzeI/Lfoaohv3xc76KNLQgbTopY8bvPbBoaDB
qIAmadDjTFbuAsfyYQSxtvN4oK8eIBXTgsuwZOvuT8tDw9oXM6n3pE12tLEqK9EY1s+n/z4JDpP+
DI8oHRhf7xwSh9tU4lNWvxnN2+Rs+FN/JaI50Lut4+Jrx9uVQ1gkjtvC50jS21JdZx19ttJVCN+5
lLckioAnRFHMmb/e6YSX1ba+pr//AZe6W0/ys+I+FmLC4oUFZAT+RIOdTxwzo1EGkedpnD9MXwhV
OPv0fFmN+0shnCwMULN083p8mdrzgkQ+kUyrknJmzjuBX3B2bIEdt6g5lAouFWhBoTg3a0N4r7oQ
CGIuL1VYD4v+gSNLT2AzKqBruBL4o4xnPXT8Shw0aeLausl9NHbH6rBX0dXufI8AWU6OTphPZ9Yx
NDx6QkQEbuknMCswttVXgDFcotTW7M4tAK2b08hoByblJVdhYg/4smb4mtPXpARijtwokWxtPBYk
wEO3UcQJHLQAtTQqmiKsnt77hxMfv4cY6ieWHGiOlj5PGkNKE/uhM5rbu68JQ8T7FF41HkHkYwFZ
JehOfVWQLL/GZz8eB8KuyhBtxXnaiUrEGKXlEIw+pu6g1QG7LCZVFh19HJ9L1xMJTG6zWsLWhltJ
KGn1/XYh14LdlCXR3laW05LVMz1Z8FzglqpSshqU4ea+x0j+YtSbwH7hT+pbtrSRllNPNCjg6p0D
ybZoy8SbON4zLAWixZmUbr/IFzBzcMWYgVrPyLrh/5rM41BM5/+jeSvJ7iYHSUiprc+n4OURb3hv
3v9JzpQI4WE5+Wj/8/E/y1lnMB0L47GG+eKNI9HMQFAVRLBXG61PFRHoIDk03IXRRD4eOQtW/mJz
Adrw9jeWiFWUp4oFCvuokHXCZIKa8BvzBocOXtFxmUXhjHLWUft8hLM3QqnB3RsQvIAgbuu9PFM3
CEsDwMYQYip7XrHowc7z87+bde2LlGVpKVpRzMNs/uLSCHcW0O17c/6thpbNft3kPyNFN495Jqnd
BSZAA87xCVP+YdT2o9EqqJeppWDfmUE2fxucGz9T2EXi/u52P5Jpx0aPMgpnsTK8l6LeJtcrBlFV
vNRG9GojTCh8wT0BBB0HXGMlwmmKxwyFBRKOhKhVZPuMy6b1lL1mFrC+pBLBrZvRIs2ekoept8bV
/IIlGX3uRJevEHDzSu3FPapSK6F7nTuOBG0Rh9NAcVw20nreGvvoiRJ+hVJ+M9uM5QGJtm2ZU+yG
AexbN/ma7/C+QKVU8GFgb1MuQ+haPfl2mLHb9rAW27W564fvx64K5NWpxSuR7sHsWMPmDTeQaOXo
QFe09KvQWdmBJWTamPK0sldh7PFfy1/wNXNAo6KROcJOEJBtmg4gtrU8GMivz00CYt96HR1mLKNG
L/qWzz5md4UePzq9g8xY7g9fCFH/rb2F9qy1ds7Idda7/hbkdRY4gBDLUI1gCif9218CC/jNrMPb
esw34IpULdjEfUODFrVzy6ehr/S3dB7gQPTA955GFqzlh9OytaYX1w++IU4ZW0Bk8vrljN3gqy94
GQpQw3WPOP10IpKWIlNxUMzYtYGyhJHVuZoET9Crb4giVhtYFqlxXA5QSxqu2N4H732nQVVg8wb5
ZMFrUtAzQ9uwCB0rRFORWeZemt6ziuEo4nmsggpEDQDnyfoH73iwali6u/1q+6+rbfib1HNZqbiT
V4dUj1eky6A9LTmW+R5Ez0xvwdEcJAlTxbe3cW4ILIjrNWQNnPi7bSmX4T74X3BSRg4E+cetrLhr
YfU3Y5NPGDFolODM3HjL5M4XqkqnkUwFjp60PlEOKb82vPhUQCDHgXmR7jDAXyF52PbAZNX5g11F
e7ADkQWicM6pDqAK7DtvV2azoy0OwZn3mQ1iNeFSwnfudzllieYK6LNTCD7oCmhoyxS5ZnpasYP0
J7ZzaYdA8NtodVJSF1usVh9ehLrDWUDktuUeN/1NO8Ct10FUAa4RhQVTII3l5LF+6hW3cwUL8gpF
rAt9O8dFN7VwMoDNeVDTHhylvcVykHmyxPIHvw8LoCpJLpkXXOcB1M16/XVlG2TBMyuxWQpg5Jvd
5w8ABJUgWmoE67axdEPnZmVhiRIZZ6fhdANhZFnAhBWicYhJ1DuBsIQrua3tVjU4dhW0pIN4mPeG
2ZO9ngB8sKNpp39Zk1QHo1oMk5UYApScwx0P/Mf9TJZ6Na5n8a1RPS91ZBD2HYitB66fGX8m+pzZ
8XuXD22Pr1bdYwKrmC6UgUy6iN63XqAA81YdkmXZ2eKXDGlQv0q/pq4n76zkSKsxlY/wXDQrUdWu
3Fuwo7MEOIlPoYhb158DKchu2Rgy+2e7hkdLVGVjrwB310GhB5dgiRryx6cFXwfFNKPshncnrE3M
NaLKDhIXzrHtZXayl60UqLnMs5t6HGWSLrMbxVFi2vOXQBeHApQYv6EHjKp4nPRwK/L8crT2wjXr
QO2jqbV9vTsTm7wl6CPy3+0bApsVX9cpCOqqDK8BU/A/w/kaosug0DDKdjmjtS895MrDcs5xyeuS
XlXwPF0YgraoRJKgYY6r6/XfPhZZpRVIfJjJqpzrxTY89xLcPTh1seKE78XL3JEVWTJfc6yaK+lQ
0yzqnfzJ9nQmt0xzbYwQJzGcmFTGYDnivPCUU6lImV2GPqQU8HTXnkVOBthZKJ0CY+DBw+97j9iW
pHZ1VdrBOifiwyFs7VQILUhrEC10YhhjcSqSZ9uQXPJL+wNmwFoDxhVy5tpZaS4NQ2TZbmPsang0
XDQNy1WWlElvZQbjuLTSRkmRN6lOENb47bthCRC7z2qJ2R4Eb9RfofIY+NasayCAKotfHltJwfnI
VW9rF3SqrjgGs5eQ1jMez9w3j67fm7MGMKlpTJQsf0DHdfC4AO2KAaSilH653ZTTLNFOJVsAIrQm
pOuLuw5WWccZqUj1eazd0o3weVjVaIcq6GqfRRc3m48ofR57fFUDJKTyD0y41eawjjXItNc2IaJW
k1uI/btX6/H6YsPTE0W7mvBsC1+lFbL3REBBjk01QfYVEJ94DmlpLhIjzxByTJFRaQ+cC8jvtcDy
M5GBovD1NXhhQHW3ocqtxntpaqSatiTGjRK2gKGpERpBAvQqwhKr+NPiOHeQTc+Fmpvzs7L4X7br
xSzSpn2GESF16IJ6wcwC52zn0/eVjJwTQIoZQIxRU2kYr+VKxLkgmcR9xUNblon6EF0zIjEpt127
flQnSRBl1nj/5rlcbBDTCaIrKDCRBchHp2D4bz8n7qktkj6aLBu6VtWg8CX5yV8m1wAEQDhJwuPz
itIVJEAQwxlAKeGJ81AmOKk+yQ26Sg4c71+l6JTXD2ETvn9HzwR6HARLvRBqb9zZnvDAvi0miR0d
7MZYMe9aCkKNBhRZMXvKvMrWPmap9TsSrqnT06HZCqIMcjy2pvSkUyqZM+ayF5fqkwnhzj2lSaLC
AXmMFLGGW/AhFuK2U7gM7Nec+rfOLLB0+R1SYyPFNtZeEV8Ct6NME4SlYrCMgQuQKnqCxfBUEBTe
mk3R3UFwbhx4qxyAmV9D+WVqKJgwcb8vL2Km6clEK3FUYqAaR/e5mJJlf4bRnm9X550k+iagnEXn
Kvq1Gxob6yOTBuC50p8g7r+9IRUkpUamsaKDdXZ1PwEvMqt9jetOolusmBR5MB4i9EqsmBvKohq7
5dsu2yFGUIMFQiDzjN/T8sKBQrjFVw4K2gMUILvRQl7yNFGkzp1Rsv47d/cZllR9M6aWailMDEaG
v1DrQp2UqTzlIcrUxalUty65ebA2LsNHxFDc6fcCpVF4qxw4RJvcE7yVCW0cGDPhJNGO+bQmtoFW
lcCi8alSjoDcLyMEx7xrled5ByeGXdh+WKErcbHAKx3EiB54FbH9rPMbiIhX44kQ3rQfxM3FF/XQ
QTJXNrFbwHauju7Nb6rhu9dDbbOrOE2vRySKDks6V7zzjcIjGBo0pTSCYx5XUlBm4O1UgfFDzWuT
tUuK+r3AMID8ULzJPTTmxdC4FYXMdRu4EzK68p1LaBxcflZE2gfJW8ZRg6lJdlfF15UzTiMBah4+
U5rNVa/IDPrUx0WC0yNbvM8tPjwGc6dWGlC3fEUo2RBhJ7cdAQ9/EUtNwsefKVelUhoo9814m5Nk
PXwwvGwoHYgNZeomZ0vb4YsqSfQcijFAM9WpjmbfEjIn33LZmVj2YIXw7fML2iVfF/roB1/ps0D4
txO7bdRndvfY3zHXHrS/XTBXDDbn65nCUZtfPh3S2gYijtznuQ3ewzFfERPxjpExFL1vyFRPmoNv
6yOx5uQ65uDjdLvSXOIGA2VU6djpwsyhm23VwHMG8cUyU8LzSGHujDhkMWyIJEp9Z49zEHwCla+5
SJCUXg/sXRDMw62HxDwjdFd34KGcUIvQVPrr+xt0EJFWWvNVQrZrq5Tw+fcoXrA3yR/W3nIYm0Ho
ZsNs/1Wf/5SJZVO90z6roKoBof0YwQnuq87ZS442Ii4UjlEo1gIXXdiv6RQawfzB6+X62ulIGo/X
eqVfAf6vF6Z/TU6FHunBHPE9Q87HeP7cxeJHi9iFxVyXy4fBVjQBvOR2amgmxEvgsa1f/Ps1I+tj
MnYbZRUQv79P02AtvJVfBegllUxF88SYr+M2p9ZpAWV0Z2/Zru+jcIhSqOIPZzbV3ZmblcgXl69y
4FVCVsaT/4qdrIhPmF2PAjxP7KtCvMkW3LhA2sOfo9HjA5E83jG/1f/PeSOoa/Z0a1iBkw1c0yof
WlpYeqRuWLxrbFGNYHkLnGW34wx+hUeLvPOFsg9FSgBjFHFPJNR8WwduRPRFaKsqkV/4Dcw/vQdC
dFgtKznWeadciPSc8+1nHE8v1WcXqJlsDKwDUxu/0oiFPQ6afpi0dq3kNeOMR/7r1M1eWjFKMy+d
NgQc0IbEsW9zlP9Cn08wSLNVVk8wdOQnpAwkC5U+WgkN16oG02IaDLoBXC31iaF45KlrvP1IEZLe
dJiLf4xxp7D4kRwW8viOWBmdcje7U2xq01DV+r242LQAVFw5G1EQ79/IQbdl/jWbi4FY3sNKk/os
/Z4833xHUiOzToiDHUols4PqLprSRLW2HoLqH0iAcP6bWu/g9Yj75DzsvK5U127JL5jZhVwySRls
+HKSjiWae5HGSURZ0CZF6yycQ8/z2TCTHL/Hz0IX72u+9FNSd2T6sxagDsJvz1E2WGlvNagzYKVZ
Qnh3J/NK9brNVu2hBqEAuoBmz9WzeDJIhwa/zed6cX/DJ9B5FI4OPgqrHdMvwotXhVDgE47VtaZf
bg6JdPjLhHTjFA3UnCRddY0BwxtGA8eHYlTqVo/J+mKZaGXqor6vlKZEs5gaYJQCTMrhuH0n1NMX
huKQu2chUSYJeGSZUbBLjAQZUq+QtkwARE8a37OeMuN2JceWo84cs6T/fWnzp9FIM6LGTjFjh3Ws
/k/KEYAWbn3bTjvVY5CXrAEkPEr+5hKQvYsk0Ue0avY3YrnzE/be+FofqwTGRLebS0hRs/uLXjZW
qiGeplFmldlmHXKkxTjClIYH0XxY2X6CzOQBH1wno6OUWhN/ERW0fkFnjv6oNWlyVai4C8BDiTNL
YtMTFSMhQu2BMfmWrGcZMlsQSIaefYnX8H0aH8bL+Z+1YQVmdYCpTq1rmHEq1Yy+Xl8HABvlMQSc
PkkWucrXGHptkwwVNz8LeL1kX6ipbKLWCBgQfxTWmFVB22p9I/rAsbLAvnHZXCiuYKKWeNIk0HlI
3FEnBpnPkfqGBlU6M+XvC/i+oSvL12md7PKgDi4ZSXbQe1ZpaUxs4MxMCQVezXa5XZ5Wgr8q99pn
NvA0QETz+39OjHseRI1TsSCbL+X7nx/Um4goBN8nkXp2oR1wULoZmKKCy574M6MgXDzt8y+GsaUa
yEqupVr29nLrL5R9lLcfrbBjzo3eZbuqjKP9byb6eq2eaygksbY7b5SBhhbotU0Wwz9/ZLu1tVs0
rSW0SHIG8Osxms6BJLi/icpbzSvVtvlLTmg98WuiGaFfGzy1jF9b3alhlbmGZ3C6Jk4U/Y86oGOg
EMuBc38ymhP9EFRYFL704yLVABdOwzhaVx8Hkqc/3JQfv34zWNO5FMMQO2uz71C52XettzQy6GnA
+bs68McHYNcnCPz6QfnCDeataGNL28cADJVte8+bsgR2Fl5BEaq0xyGHP68PNp9whmpcQwerdBut
pyHb1G29tPL4NtMKESn6mWsGtsi+jLbHTpCrD3lM8iQlgYFQcqF46Y3tWfsH8imxke4ghviNnQ+q
SZpDhGaeN1alF7dow2D7CeQYtU/XOzkRqDCSS8Wyfx03RXZJ2MEpdSFEVKCEGPBnAsnV6fGpUg4l
l2nTc/e/SjG+Fs7Xl21kDBLmDBBzdnOCIeB8eyHG1sQbtMLwbEl8B9bbyYVXYMOEFf5dDICcLlv+
unYJrJjXFYWuMp4Y5CyJQl+kGJcA65/JvskT53Ahw0m3V1VVoGRv1DcMCOkC9EHsYymWLcmWPsjX
PMmv41GEFAX6UYkYgKXO7yQ+Ci8zsd+W0uSVy7fNL2qhxsbXLnQqJYi4Y7HyM4/2/dME/yoMOOUt
xZWITj4rRYjgW3w21ZZHK9Z4RNU7kxJ9BiD/l0/CuS6Buf59qEqYYtSpxtYjv7LFaT3WApBd2wRX
wxKUU35euP+iYBcMQ0SzaOwJZYZcTe4G13Zt1hQ1w9ETSUHV2h9IbdbxQHEAz21qyXTept9WHnyz
XpaNU/e9sA9RL6LaZqZVsKWDw486YffShKGx4Xb3Dr7u501zug01K3T6rWSvrRXXOMF+iaOkK0vA
/PfLX7rDO+PjaLL8QsQgLBigo209b58xfiSywTTTzaboMZD8AkD3SMFVHeu/DV4Z+xYLP3KlNM1m
D7EXyQOgOJcD4kSCRYTbMdx970zUa/5n6PRiVct99z6QvDksCmAtywn5N6Lfbr8t0GusznLKWKaL
EnuPnCk8MF7Ss9TZeDcfYDSw5vbUHhA4DAYgD91d1WZ3AOPI0w30DrOQoC1273mdfqxEzADFAyy3
DUOGDu0XtPswla6O9xwAvkayVtf9ClApVuc/tN0AE24ozpqGJCdCZDcsIFcoVX7Inpwa9G9ormvd
idBQFNsCA2WCqqo5xB5EBMQ6Lw/CaCOxmCd71MS8aNZgj5imtwA2YZBNnT7UIAxQnj09j4Yg5aH2
uyS4Y8eg8xiwP43aVauYO7mdXW1rOqQNwwWGH5utpnsk+5X51tT55EU/rS8/gLHz/JAq45SZeBvv
hGoWrAj7geV7dQ1vAxWQYzOxuPQLBZjd6cLVsV5EnwLnNAel2ODNC/mFlY9Whzzg1Be9AHleRfcF
B7222lK2Klf0o4uCMTwSb7NK1k3Jm5MSiJN17kzKGWVj7AqcUMklUp5A3zXF7NkJhxhUL4e3vFuU
V369/XqcHPFdj43xP915UqG9hGNdu1spVO7OfhYITn3j8+gND9NH175wQrIP/b0IjQS+Mhn5of+Q
HdXXs5blZGrRaU2dEvRXXgY1DfG2HX6oWc1E8yUui3bJB+ckmSvRzljtor6bgFcCIkdx4HlVf894
FYR42QaOOA+cUeRXjUNdzHeMErJJnExQx2TJjU9TKNU2h9epIida1Egh3BSt2Y2pucskv9xv1G9l
uqDySg26SKMeKwlnuq+vVZKv0ITjXWoNLR1AabWizhTfCfJPt/79/12Azt5LMfaMXgK1c8E2LJQm
BxtFZJmpbcSW4CuUdCpTkgXTrwUu75JerrtiKWUBrWLfkzrZKAQwigTfd6qg4BVJW62PJi/5Tn5A
N8nQeiPA7RCH3KIbA//76ulnOc55lyjD1FlDHAL9Cs3AYQq3QdmFBlHivTOr8ggMxxC3fixM5KSS
GHKS96YxDw1DhlWSyskaysQhuQifI/oNLIqPNcLyfK8cev+PUTbUD8I/GPwwcXSY0oMW6/2oymlF
NX+84RvvnkvVsg/p2IndqI1ZbAi9ft6bH4w5TsYraw8hsEEYVn/78JZpr3s8WDyGYlskdserK4Wx
JYJg54ig/HkbxaQZ2LzNitWuYxvmIEp88/9ZoLYNdSRZ5HwdHemiCpOvgAk79D+DfUfInu7RbpXZ
s4UZLmnU8p4pvOPh8jhEqP60afd/5pZEyIew4QTG8haaLwD/rvyVe2ydQFoHv7x49xypJGcm4BCI
kE8kydZpaBc3xFFPzIAks7PFMPD4xX8ouQMXQGRQEF1JPQbGNvgNNfS4m7WFIn0kiEHnuSEVUmix
qHLSV1ZOeur6vw2IOCDLZneRF04rFVVQWMLtSm7lRT22rGM3QgxRCG+l67VR0Yp7bGWb7xlFG+Ls
YZcwRozPpIoBUmKNQvXGIpl5c/HbSJ2ErHkn/0imFt3wcJ/htGP4O2JSlDHmH4FKyNBgOZ9fFBQm
79kPHAj0ocnQAmKP1Tmn/G0QsagjRCHZ8COnv6vH5rQbeWiGHObbG/ylL8SdFTXrtc8nWAlSMSi9
/MKj3rxrsYAk6y5Ogldq4ZzefUVz713mHwEpYHWC68iue3oQObWfKlNEc5BpVOtMelBE9piTtKhN
WL+0M7PkzuwkElO8L03BZaGhPIiiQJMBqT3WNQt1SmgOrRnL3rTFuwDRFHdg0LFF9DPD20eJl2WR
k3zy1cLHCfw06TbFPgCGWVRJB30ny36Lgjy8Vb+vqrsa3AMBizVTzQatI8iHN4KIDhUuiFRrTCwi
tod7YAcxaemo/wRnvqqVpXK20No5p/FGo0uvk1ZtdP9L8KTKXguTM1tGAe5m365Y4oqLBTaC+HbZ
79du5coiHr1GqpV/m31o6dhdO43HbFSfzNPfei7E6q98fk56VWibZHjVnwQtb9w6oWFH+snzFN+I
Ja/s3E47Uz0t5OBY9sKTfdn+4URIimgrKnXDIFGIXpHziYGVSkzVNkweA5kmRvsqinJ8Rt1z/wCb
6iGWryzfypnMab4ZOfQS5zBUIk2FnkwUC6SApd5h0lJihuUUsab2PhdWLlNoUWdq3u+bzDqWev39
3e/GdJRqS0tfJb1KzQiy2w86Vk6g72PmdTI0U0KgXNoJM40PVgdNL+tF+b/Q68mB7JvzQk1WU6aM
Xuv2KSAT6B6gPyI8wJ0pvGANkKefhR5TGx9R7OGqxLjRYf0ySWZ9vJ8YqS+QIlqti3jzM4oDjdjA
vSt+24WRpgFyGAAZKGr7HK5EEAH8F8LJZ6W3IjNYWqjE6Z0zgYGDiMcOVlL5t3Rl9bJg1Nue3O+Z
whhMD1hbR9keB3nICL32XiHP77D03s+UWS4ITtLtqfLqMYUG3jnEanfHNZR7HoeJtoGBr3R/gmMj
XXaemaynNub/A+P+NQrvSFdUuJEzaBWZXdSg/0kAfQZy+W4ZtC2EEj7c3F8kC75OMpUPs659DvPq
QD7fZ5rX2B+IXpUQYSAi93bbxwyJObHZVWW2K0+DUD+DyCyaWePGaxMT/YWJ2sgX2Gr7THg1XPWd
FAhY9s3mK8ZsgrlKULuw/tCzkdv3iqv1QvU4lY9zcdYFUk7AvwDbL9PaGlVi3vWYtr32PnBOUcot
jkJBPf7/jSTu3/eSh35hxVEHR333qwKNrKNuTBHaT7wSGsye+/j/eQaLDzqeeIdyS+Dr7uhZPJlB
F11F5ny6vX9xzE1SdkA3qVnM3rePym/lEjQ3r5TsnvHAecPB8EuoBn1UqP7Ekw0lNPyjWL6gCyh0
kSeA8o383EiukTbh/kfN/fE0OEFpqKn5pojsPLdXiX9PkjGIsHet4IDKgECbY7qrndh+pBwR6n64
CadYaYF7PHqHfbvYZCbiNdMBpaOZGtiLiqCUcM8Ttb+24yhejuI8nlz/vaa+d/Z3Egw+pO3DRSdk
s1DaX9D4XUsH9zZQvEPZpKQGtlzWAF3VVRpTCreH7LBkmiy/w7JfhEuve5wgcMAHsz3Az6zC+fZG
PFv8nzdkpDRydpPHcCD3JOenFJlpPlg7a7hsYglG67Sjka2VtZAINVjS9wm6NrWYjo7yx6c/ht9t
dP5XuXTv1NZqpFtumqdVjmh23YuCqSy7yIL0ZNzmh7+EZhtAjArv3g7DUvfVFg30hgMkLTaQfasf
4MdB9JSZOopneTJ073PlkJsLZa5l4CgzavtwouwaPe5LUMIsiLfgniGIf1OXm588rK0ZfdZgfucb
CCO5IEZx8kqZogVDMys5qwQdVbpv54Wxmupv4rzzBApzSpFijyVDXlVFmtT3IqonFR21LyZ4I+Vb
G1v1tQdpUWp6Vag9uADrHRwBkSTGUmvcoHHLHRrR9GS8aelSSbgy7/aXOMa5nlLPIdWqw4AIY26K
cRCKMnpheAM34J2J+0wwBGmuTEyD5XJ9pwh5mnxW5QwSX/i2PJMyEcR1mUb3pfVzlxoOAvpa50Av
XQ9M+vVhD4pzDlDOE/XFAYpSAllt6txOVz/KWDUtYH/NWz0TcYfx77/o3boZR2sWiSL+gzwt9z1D
Az9+bVt71bnpBmi1mcSvRTi4t7ev+JFkHMpuLlY50KUglXdrnXkA5YDDL7ofzBtQR22G13To9xrQ
ehwiBqAR0hvF5K+v+E2F6xPtYfLbOzIBMIYXL/NUetRWh0YLW3SbX8OBzcGn7bLxfrfqOy0HHn0q
HapfHf2kXxCiEm4JhHsPQTlC0JvVtlqZEMtlXaywwFx1LEbkBfH8YBFe1nSLJRZrV4SU6BmOKn18
vsz2w8cxmgvs11WETNFOqj+HBQXdTjbiTGhaktgugarEfHVnIH1e/EE5xE5sNXZUu11E4zOKq/6v
4SEZn463vA5yta8fFmXxLQ+sTh7fW8+j4gH5Kx2AJCqk8BaJddDAANc4H1dueo9sDH6YhG5BpIG+
MC0sCG4mUgqCezPGOij2ZTo2RRTmCU0skhxufTqltSLhXR3+NOXnCFLew5HSIuCKXDvDa1BVWFie
4CmIiaVCZUykHNdH0a/qIjQjuUZx8luafFloCN7x8UY46sHa7SKPh9OjS75zJQ/x8pEdxEd5XxC+
/e5pwp23as8TIM/ssM2GE40Oj5vAuCeq7Ni/W/jWTSjQn4fduFRjOxvCTqBlDh4JoAHukq/mFcb9
2W1dLy70cojcz1+V7oUZ2TVXDZGuC4z0L3fcte+alqIxrjV1K/+Xdqtg5YaNQaOE550J6QAOOxRr
8Vc/Ps1q1wLy4ZRdtAIXSuMTcl6zANm1uUscFy08Yx6t7GDn2fGsCiAhgjYxrnzwz3uQNfOB+F+s
WStfp46wDj5CZs1KMJERyw1oq4UBFV6SZUXpsQzVc1SaqU2FbcVMfufcIp2hmCNrk9B05FTeKfoz
7Ufxn6JlcP5oYYE1yE8g4HDqRe+Tx8R20siSpv1P6hv626qtwZX3XcW8yCHzDI+ge+QILyW7lh+h
t828lKsob9YZ/262ntRrkJLrFbVTAL6LRD8uqsZMT+s+sZDKy9hKlaEFk5N0ME09en4FWwIzy9XK
DZBzb/2As8Sj14N7od5cTHYTzhF1JREPbb5jAJ1D+4aW+0bgvRnFJshyrAPGpsebhsPoR0A/HblI
yuQIeZMl9I6ROZ+J7NlW2MT9TZEnEbMN7fk0afMAFF6R5TFNY+3zpMb0ErE39hnaX/ySHjpeiI5d
9cLPVTfxBmUoWyPo0qWgU2sc3PV+Ci65zH4ApraUTxUEXl6a4u6uSIkfmRQkRP0JOz9g+aVyViar
qx3wsjh7+Yj1xV9cEOf7VpmI8D9HDKpQb0RjhjhFWQwG7MrtyfvbDi0bzfhtH6KxphirJdrV+N45
vIiyTOZM27d3ktcoLLQIjnbMqzA/D38DgpXRSfmoF7fTU/F4Wx3tsW2KdTgfCU9y5lHduOgCblAz
4mQZKuG9krLtM+RBhF3aQ+u03IPQwEcBGoqbsidYhRdkunhcDBZYCL74uKdRZXgntRKUfUcUW8Lr
ymyLqa6uVYHp0+uLdee4zrfNpUXL+Od85dEvkHY3d/elx8h3lW/LBKrH7OscmdnOPauj9zPjVxj8
LAjIGIz/OZuYQhw+bRsNPio23McupTWt3S19uWH0AO6fL4o9ALi7MrIKjSQbECTNpuDp01AQRUmq
C4omVfHbHYG7Lex9pwy748xImVUkxd6lVTTrdy8NJICDxPeZL5XEbM40eZe+HRTaJ8pBAr37y0Dr
lywvFp55m/MPad0q6+zYL7EUO/uwR6a1Ih4WUME7bJwmFSH9NSgosqu7uErXsa9BJRNGX8k3C/nI
QTm4bJeycChTMYtwhIhenTKqBIPj6FVGEeVINCIkDoMW9T1mMZog1nXtGjsbm17fr7rnyo5Y8yNz
IknRTKpaKGrzuCHU6myMjr6QzGQwtj/e+AtTLBtuPNlQWFEc/C+AJJ/Po6lMqk7z3GU9aTdwgtUR
r/MuxicI15MNOilpkP9nlqeAAVbzYN9uqo5/MaNOu2lPPMIKrsCUlBHMIs7oYs/RePCqSIPvhY5o
Y0BTbe/rzr2GUlKi+/mSgfbz8WwVmpIEk2VQHfj7m6ymxTGURK0DL7yt4AOgG7LR3T7Llk+X3X/H
Geo5TSPU6N5yY5mhv+hdD5zr7gVw24o51givAj9MY3zN4e08ChdHYYjOLvkMAYO6xflnh7m8tkkA
z5/kZLfd9w9mGA0kN1HoZr77WSlCjQhQLC+xduOZFZnHcdpgLZbXBP5ZDatN2z0uRJIaMVDcdp2j
4Bc5GDQgedlktkpVFlRaVn5JDA6ahO2wuXf7v980bHlCy8uCtBSxckTfqQkNICP4UWi0AuoMsspf
LfrZP6k26OoqZ9KqsK32ed/9BJ465xZoRufMU9OrYxO0iE/kEbGDbE28l5UARSOgglOyKZg9q0Gr
prYdrGsUpmlOncQHA0Qt+qK7Xn0J0e3X4Gj/KyMB8ITNdv+YViK9F035T2iFcc5rJO29hp/KrLDd
/qaZhBWNYPbGlZdjB8kfqkZ4Br+4PLgcNKgf6NoqOAWdbXgc2Hbj0MHc1xPl2DeE9r2sowq4Ezm6
iRqiLUPP/qwmPh09JeUyN7ja/2WHNJHlF7KIz23YYBVW8tnYBgElJoMPycnH62xhfR16If+4FdAH
yNPiiXxKtTEcPE2w+uLqAZlLmhvEKBldMy28XWpVBvSiz2ZZa8hxWQVumyKugBLe2HZxgKWM3zIR
BDJVe4+aIplfzEksqXqdk8LImLqW7a1pyuVMV8Lnd76nyqK088F55e/Ug4deSqSay6OkZohk6tO4
9hbpoZHpXb7rRoM3vDqkMlfVBVUV94WQae7CNf0PhHEavFWuTlerTFUE6AeWXGav3zH+euxihexl
xrMv5sbNRaN/frDUn67MVzlnFKN9t1Pe0XY5rOAZqzIkOw0aOamnBkONryGEkj5jzmi9M0P8goUO
41p3O4LaRTbtOfX/aKqDoDSeZ1SF07VYcR2cPt7pF6++9pB1CrI9zkAAsEk/3Z27LJx4jwN4s6KM
iqCB+vVziAsZ11eXsLx+AQLTO0yKr83ZNaqfD9npN6LF9FC/5+Z6DkucnstoOetAeAGBGVy97/jr
vnUyVASMWpz4XIJgf2l75rcFZX8rt6vvqpI904xtAUTRl4fkFs3CNTzYjAtWD/2O7mFG1WXt4MvN
1epG6Tg6//i154cmyd9sERMwSH8bLgf8A0LBAHpr5QSNQgSgfHbVHRyTWRfZ38rarVNoo1iNsShl
mb0ix2AC6NvhpnQZEiEqP6/B2xbg7JVpUUmfK6kjNCSPrS0ptBMkFlo2DADeKauBkLZ4HYIAq7TF
EKCjxIvDsVSZiQUhY/V1nEW2vTeG7mEhLqMvN5+cm3BdNE3RLo/QnonwHGACaUwvEioNbHl5qpNO
jKkogo+GvgBHWLY4AuXwe1vpSP6nWf7Qq2DQUoqo0T2vcpnNxKlW4qPmFzYZXIw3tBMOGB1fN3QM
KuRcLYoUWhGIcWAj9OXOXzkfkayj7eLyXnsSt/39K0SrwsGRbsYAbPqIcyFb797U8YxrSaUmZjNn
mTze/qlEonzzxC+Iab0DBKR4Snd0KNs+S3C4StyEDjzUmgbwj6b5gDR+I9/LzGLpYv8YfzwVqSPQ
iTp6Hxa3lK+ROsky/DlXYHqvZglE0bAz6BqbPn8xf8wtHvUIHErK3O0/USU5Ep/5p8/yDx9MruBi
wYKz7s9ojti/qLlDPbAy9cN2TjrxHOJF4SvzdSaRxPJKPZj/RpyUlh/7QxTFMie6MOMnwh1sCjCN
/2WViXI0UFUoqKfBYGzOzPML+e4KRWr7a1RMbCXwtXn84AFGCj4auuVEtz7khiQDMQv3j3/EdnyY
DUyj1O53yQoxuk7gGqFqJdgNO5As1BWzUjaa3QpFysrPOv2mSnOmGZ1TY10onjwhQrGnDYqFAwyW
YaauihfivON/w7ztwGI11I+ZbhE/TBv2sqyj1wSK+DQKahOX3liDOf7X3I+IYARf7xE9mJ0Dd/L2
U65mz/t1Tw2Pl0fAsepYsaZx7jW+q1F08o0Igho784DdQK43MLOZf4yKtQallJrW5fPHPnjI0Sxr
QoaWrRoHa6zmrWpOhocu+cfCEmfpYMCgNasF77jGBBfYgN/Nn1JCEJ8TblwYt7giQK57MqxxWNWa
D1B3Na2/OoS1KK3f+DnxgCUm39kU9p3OTQ6zf7V4RZ91j2N5KZIyEyfW1A2tKFHR095td4mrapZH
R2fA6FgVRfgLpb1xPhdTZr8gPeUbNKG4Fjr6lZ7GU8Nx8IX8+6yypYWLPyw43zh7R48U1oZQl7Y4
Mx4I2q7O2vnl4LGrexb0g8gg+7eZrvKQRVATjEBCUR3HxmrJdmJa7vftcRbl1ukxLwwqji8sT07m
wTzwlMjLTMp8jFJw+Go1xs70yxKipICoNO6TLqJ+56Gfw4bymvaK+tE/LYfGdsucPg2cyWYkL2/f
8UykCx9zY+UoDD9eBT6jBS+bKas6QC/Py3V+GQJ4c/mUYgWidDoWD0g5gwkxTLqV9aFNToNKAz3j
vdcHpVqtvI5FNr02BW4+310uwjt7Y0SSlZM3AGG/xyaMHgV3djXjRaPOVHoyQZYuxkhsx+JmBbAV
etLZFuqyqAurrKNOcZPLtCCjS5owT1fM7yx0rbyLqhUsTpBcMgbQztZgC7YzWU/vrrMCVTv5Rqg+
eqWq2XW6CEyHbKaPT0uR5/FWAWXqio2QyutRaDJSdfwDjE6WFghhM/AHB6yxqGU08iemKNlT4rR4
GnRKJJIiCWiuFRy2cM2jzW2e9fWrpnPkkEKz1/5a5JkdEh8pljcqzLzHvoPEGdLuFHS1hHNzm63k
IZnrtrA/OJyGQLcfA3m1mu8kRXpXIR1HaJNEqUXi/fI8usH62/IqfyGy9hvtDtlSl1JonSBSLA89
nRi9hAmJTPDNIfVebvsvEH96beKK1V8LRMtTYdG3G+dNICxodhIdPKReZrN8eMvZYS9O8vXwFUyt
j6fTo7G8zKNfRzUE5HgHHpzTY+L9WttxlYz9cy3mX+8bax7jp5XuuZloOPT5/OaSVXCLXh0dAeGo
vyLmh+WyLBPMWCacm0kXYSp2iH4xNEL9IK3wlg4fnAZxP9+24cou5Sp8AwITO6HdT4IvXkCVszGU
KdYDpi1pKuP7ooq4Wy0HxRqADvGnSW17Sq7JYXFoZf8SpDMeZElR0NiugNvI2evwkvNVEdaBsIni
LpfR7VsrTIzW2TlnfthIru7DluRPCgoQ9EhDheQxQJP81eBs0l9Nx0r+gL1PGjGodzWsYjyo7kQV
GYBrmOpjvKsUaBdHVbR15uoTH/CfzKjqEU3iRE5tMQbkfJ2qS7bJJNZz7a3ShbzqXCDimePGoR9G
92CzmVyiSrd710kk+FiJKmNsNHaD/TiWU0vZx2zhkyUQ743JlDfwygKdPkItwYbQFZFmYZk98uZD
MsOp4bd4DxxIAL4pTFb660bANkU0JiwOCC397yZPHkCw4xLgzuYo5oSwZZtKFvyXwKKmHFxnJdIj
qDI2PYRFWEn2MMi+la7iH1s6quskt9SZWxtYiux0b18plyoRyG4lxc/Ko0mKOJJ+q39LuCPoZr1G
n7szl6jnIzLj6Aoh+35BjnekCaIk3PiUt2hCsjrtRurrLbDjT0eesgmlD8/AE0MCIE5AwISWJynb
GNPQ1LQB2O+mGVKatDikRubPsfZnUbCVChAFzqN+V3RSdKbe214u0D8iO+xiC24DIAM8ok3WL602
+EL7MBIXvRMvvE4Iqs4f0nkhwhCx4fjAQLYvBfoNO5/kCkykjPF44Nw3eJBbDJIrR+9wKNQyGHlS
aLcMNFoi0RmvQQExj87lg4UPElFqNTvBqvD7U/zucltgKmYJq2T6lGxXZ3Qo2mWN1aUobrmSQzHu
fLLhEmUxMcgpedz2XigdWfjdcDOZImepIAw4LVz2YP6MPCsJ35gFSDJAI2FwIizIoLbiFYF9rgyU
Nno5/IRLrxrPxRT4uaPEbYxbRPBc+HaI9D6btQ7yiezM7N/T9QAIgjTiQxLmGBirFAHQv1beOb68
mt+/o+xHQIu7G4Sh3zxokiI1ZG6AyquKA8YzPxK9pTQ5K3Bfehizb53O5EHxFg1WUXRkAgLr/Tpq
iQ8OxKB6AEFnhQ33LikP18FwKIISWYnJaDl2hrMDdXE255wFDhsrBuPCyC2Q97uxiGOsRFxtCPEs
2JGkwKZ94coKnLOvLbDdlKU9v6iq9nqXSIbO6+qx8CZvLTIsAWtIHZ8i2MouISt8u0PhL9Uq/84k
1q0tGrlmio5sP1F5XZxMvoGnYr7ip0ACuEbERyU/S/l69Wic6CvEGU1P3onIRSLeK+fSIl6EaOQ2
PaVw5NQwYgF2/19kuOsoDfGVQf1YvbNC93zS1k56VZe3c2Xws4pD2YnO3wliZqWvCdYXLHXh+PpX
aqKO4KbZlNtHJ2RBHzMfa5LZY3A8Sal8rM/8nq00WB3GhnB8R1gH2ifgXD7BkxB7DMfcVcQmcZ2P
niymEXS6ShwbcFIrkagPaYkTAhAyjsUbUHe3unw9F5BrZWLNfj8GjFa8A9DyzgHl7ZRAgPM6YO2B
yOqhB72oMXmF5PnQYcUgcUAZ6y4po95u3GVPiV6Ag7AThNNbkiYv/hRhmK4wh+WSN5NRd7IoMRZH
nNwtROm4VsgZtGK89B9f1Oi7UQL8srWJPmR3kaOq85djrK4TPnMEtWU=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
