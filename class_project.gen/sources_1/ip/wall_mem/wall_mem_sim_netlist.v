// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sat Nov 20 01:43:12 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/alshma0b/Desktop/class_project/class_project.gen/sources_1/ip/wall_mem/wall_mem_sim_netlist.v
// Design      : wall_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "wall_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module wall_mem
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.5432 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "wall_mem.mem" *) 
  (* C_INIT_FILE_NAME = "wall_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  wall_mem_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18160)
`pragma protect data_block
HMXgyNpxklAlIv8bxjk3Athybus1KiqIuGcx14w1ZRzWTQv6CF9LzFcOSoZ+6pXYdmjEiYSyJ7yk
rqI7DLganruTAAopJf8SVQHtf6nRl8Tr9b3JQX6Jrgpd/5pXC7wc4gdV4j7/99AavDYtHa7XC7BU
fiozZAIWH1W6X1cXR6F8/B/PoIUapMg61YT17o4s80xf4HQxDxczlwrbNBigfAW+jSwFkFp+eKUU
xi/HgnAQbj+ByQsrGwJYdCfqg8rJpy2wFrHxzJH5gAIEq1WxsodLOgD552MRF1QvYPmenRNSMzy+
utMEL9p+bVqicR393zSjcS9ugyOb8PuJowPbS2v0FPMMn/Y8j+pQU54vXYi55sGaUlokAqAfvmS0
iZAocjcG6FJc55+U6RkDBkMVN4/ZY+0O8VpwvjpypXk3buHTw7jYTmNdIGWFp4kAbZLKMO9S24v1
yrTPy4qc2+oxp+tVlUD/lqfvaAFnCcYdIF/LeBgApN/xZjUFMKpSf0xvPsuNy5rpTz5tHI6EF6sr
FLYeOKBK6SGp3rohg4Qzz6soR/FyYy5SeSIpV+tZeEhCz4LjNz/+CWehxmmlsUiqZFMw6fSOCH+7
YZX7Fni42MQo4QH7ZKcSA31gbG7fk7oih5N0I/4ybK7QmKScvjSm6JKkgHlHeN9EPsFlrEmv0qiP
xcLLvkyBqjxd8pXn+Jbk1COhTcdGATWtegJ43P6cgnZQTkX0mFXJtyDETbLpGF2E0ajrjdMz//LO
izewZuiXU46F7ruhkFoNeqJA3OURc4kQaL1n6hmHzbqgTZ/xZUxkYZZzlZ3KN4VK9DV4AKO6Mdur
RV4W312G+rcG7LNaSrahfiM9UEbTplU24vAZcC40VVCw00e04nFHR9lVTlRspiHXSQky9hfW960J
VcxNos1xhL7+w3zHepCn/REQv5x8aIKEZNeSqDkL4ufD5EPoxv7oie/edOlRN1WWSvh2tc575y5C
luXC/+rTIW+l91LfBIRyFaN4Kk3h/PberUqhsCZOEfKtxmt8r7t6PwNXvlWKH42J7/AeYvW8wKQ1
G306Bbtuulztii9DGPjiiSWhI1MbHrzhYTcAOXA6YjOdVx66BGhvlPWxSyNQ4PO6F9lqRQDqIkFg
KHIeLxyNQ+yvURg+HrSsRLBd06kVEt/n3SQrlRxe2sMD1JNrJgdgHFE0vuGQJ9HnmSLj902Ba3Zc
h8YCWgWVp6CK6a7KgWfG7QVz+jRhOe0rxHmLduULeECf9bnM4zQjhBMFtljB0OEH2ORjU4yUIQ2C
pNYITxBIey+COAM56/1COxQjfFgVMXeJ39hMg+UufDlu6uztQye0lUZ4o04Fx6GliCopJ+6xsHAS
BKZ2a3EroWxvNXvr9Vigf//VVamTqGv1keKXoXoiA22dhtwM18mEEM089u4RX26GFhKq+3hO0G6L
9Rwy5CRPiZmY4KXiqsPazSWfbnOMc3Zy8zpD5sZHB1t2AGgl0cLRu2uJead+HrXtUhQZ5LdmbXXM
b2SsSb8edLygJjqBD05eBqNi0DdkqVwt57eVNQ6+jiCOflR4omC/0R1EcXZyKaWCqoIPNvnvAcbB
TzKC76po6bM4O87qO7QZkRAenkTp0PfBR8lE7aYJMZPmWBSwC4Rt9JrhPDPCS2Kp+oxEQYE6H79T
iCmIatTbdttV5ZmtiGRpIkLcRWhkb82KC4stv4lSKh8eBTWcVpz27TheoF/B+livinE0c3z5iXy6
W8gjI2SCi9fqIY29LDfwj0In8R+YTa3pmeYaH3Kxm+XmC4F3BNma/rvu+ZLP+SAi40rijnap02P4
j4dj5xWEE9ps4mRWmyMn6quCQACvdAB8s6j7cSODR7+8PMXroVqO5J1HgL2VcFDB/snzKZXAeTZW
bzKIdPtIaayQ8mgurXOMZmQZKeUImhewgXY+abwnEafyGfplQ7m7Hj0Z73XmKTMy7x7LTasep+d2
/4BHvcIhAM+78q4g21+cJWw8/HJL4C2Vy/k+Q5Y2SxlxwqR0t09ytlQKpoxPvxHeZcumX0zC8T7L
/w8LwbxJtMqZgw0aFfRmxwPWbV44NNqDrS+kxXGgQNp3mek+MBgtBSh2gEBkT57wV+rVbCobV6t9
K92dOh3CUnlvGPeejIYvlmenbK7JoignObr4eVABY3m/GajeaumjFI1TQpgMojkoU4jjNivaO5Fp
ygQUzaNz/nRgzIPB+ILSCc84m4s8724FSmuOsTYvcWidLGRX1P6E0o0cWVUC8cHz0QD+wVUM2Mr5
FZMfWWezOmKI5dCyEB7jRvnKcxT54yMoy8fOtLECUrxpwkAU3Vh4VOW0QwPGJ+RK2exA96Jo8ntS
4JD4e1MkikL2hOkxN58GIq6EeOGhaL+UqH/sFxu/p6y8A50PG5KoXLAFLFCb4S2n9aOiWdKahrH+
v/2GC7B0riWBxSf3iS5Mk3/CW05mk9eOrbhHNxi1UhVtJyOz0WSFYEj5cEpVTLIjQ7LcbWav6dP5
4jHUJkZQ7/NVhBOwEkw79gqkBc2n5F77ylZIaHr7vWe2FGX66nX78JJAM+SlflyUgi7nLfsOfidH
wrSotFlOYvyQFa0qSRziVPhvJeHh4fH06Sh+SuCmPs3GYydIaPU/q/KqOIkC1hGVK/r499G29ze+
Wlk4LxURx+v15eN1yKsH9nP5aiURDgrIo7kplAY0LlVjpLNITb8GJHEPT952krnryi3DBw/pB/1+
/0zlwq3nl+WktWhMpCF6mWbJDaeqMYWIZRU4MgcX2GKYxCIBrTmc8AdwMHWLcKkoYdWIup3CghcW
Lc6jj0CUSg1AZtKuw9zmGfUZJ7TumexDBrHeO2D7JWU2UlbSWvuPKAOji7/fDPZsVfav1oHIrAzx
sWyN6alP1/EkJ1k2yX6Hnzb/bR3z17pl/A6hj3udQIvamCQABNaDfiB278pZnL74fIMk7xvSuMsS
6H6yi0IcgQ6zmOhX8vmNL296yF9GdGzUKC8g5QyrSjZlQ9dR0CGtUzTlT3cryLKEgY1ORbsS+DvA
+C8RIVjIaqZ0TFEpXosRtuyZrAGcZpd1Bpzuz0e8BQt8SGQWxaiSW0evd1ubcFbQHej6zdhMup8b
4nGtduFB+DZzmsw1ICEUYpxUUpxZdWvzTNHm30n6wiCgyRtPcSgGp+3Wnxn23mBk5+hqE+cUFEqx
LtwATjgv/0FMm98hYOPWL0qKJ4lRSapPy/YXpyVRydJXvEAuwC+nDjf1SLe916Gc9wloqiwqQ0Wr
/eJrmpDsZqHNdp1gPJFJ9v7xWx8x8J0fNv0AB/LdtATdoBHKIBw9npZz0ULyO67SvvKkZL85KMDs
x2vwskT3YVOJ63NVqlY5sLMnib12VbMXw1RjkL6j0gk+o8sJCio77ZnwmIrTMzWMPMflEGHY6OVT
4Q6uaC9yO4KsWPYIVEQJZ+KA+qNLnAvclIRuwN5kW+Ft6Ja4Id2kjXN1UIij/pHEhcw+Qcfxzu7w
TSjC4PzMKNH7mjyk7LMseeWzm+zgKE7ATOj4iOGaDPTSReu9F83AygIUm7r5l/kPrp0uIUIHG8iP
vbQyvgVuczZr1wQLwC3Pt7z0P1lQzkOkp4cAcoMPaNmP9MmAkXC42MskZdGKohmvkrJSVsMYXJTx
wuUyeIo26t3sH1ORK7KssGrIrplZgCIQ0OWy2GRfDs3+VHFwD6OV+ftl6aBOUTklX7DLEhxq7ljx
C9Gy4XKll48aD1w9d+9eIzzTocbudu3faMeaD9t+BE/GzCeRHT34Ix2aEV0D3BQQkD6ZuENxnBIp
qXZzGJv1e0Bhpdk+A4rRVSaHxeYDM59d+apLHCpsWwyRdCe8pnAxzULSGTLFyjsrtxRUBpOu43/q
ulHvA+DEAqvUSX+YFV3yDGWsNAR0y4qgZTGfnOgjMJdhuOa24klN9JVWUlBPKhVSywJPBxG2AQ3C
0OtKzeSEKrSqagtX5GWY1IaCA1leBezn8Hjnl1kl/MIwUABgR/niYgY5Y7k8V+3TXdw0bGaeVxOx
4iU0gYLljqptxLocJxiC6IJU1fCOs8UTUKl/g9esexE8dAuMjsY1OF7RYb3YA1KOXjotGa7ToRzj
8uu+ga50h8XKYXFBjvcVIhrfUT4EHg1xWSc14h0QLuSN9U1NQjun8Otwv3rW2IUGzqRhGnsObKDp
KbO5/fEaTX5vlTuWPt5AZ4pxQEgn03BZurFQTxR5llEji4pzYO4FAYIGecdICpZFKpJZApfF3nDR
uRYZt/S1Dt3vsKXLYkoAKt1EFOaq1P7Ij9HPA+LReEbb/lA/aAhsvrkkrqEZssXbCcTGwNUAE7gO
PXn3JbEKoiOb0cDBgVoxgrXTB6NFCvYomYnrqt55HX4DyW5annxtosYNRIPba+GSOf95X/Ajti63
KqSbBC2+RH2aH+k9SlE1g/wX8c8i7WNlahOxy8UBqCZInJGe9xjwrqBrtlF9jj3eDoBO+uNsA8qQ
Kz4qC25bbiDkxtcrV8m3syamKfxotFqSqaskC/LWE+BccXTOWvTZyydd6NRB7b1XXrZP5ddeo1m7
KxiX7l2c61Eacun3FTvXxs9RMww5xRjigy4BczvYlBpbpUQTtxWaM2/6UKhn2AOSEhc7okzI+Htt
z0LHcBIlj6S+00DbgAGHOo+9z+jfODmOSxWy2mzMlvmzB39DJl7+taHQQto4lC2vUMqTDEmQQaVj
E/SkqreWV49DuBM9K2Guj0/hadmnlcgiAfOgmwXpz6PkNLf6nIoYZAnpfpNw2jTSCfYtQ0oQ3Cxd
xXV3QnDFg1leIFerRfVU3iTlJq8H3QuuWAYk3/YmueyOfNmwYJI7L+waPJXAjY5p6Nl8pi/y7msV
0vk9HbDg4wg77pKer10WQwQaEK+7GlgAxK8X8kK0jZHhCMMiT0m8trUTjsy0Qa/0O0IrHvlSqKff
5LzUYKrzLmVTQfMSNHyVLDu8UqZ2yMjbhxBT3FMKbhv/Uw61EpLHR7GE0YJoLFI3dicbva731hDE
HzLJeI1s4OM6PTK9KDgIRER+HErf5GYI/kuo67JGDhMDlZLYCtl6Y/05uciHlDFm1b1m4SmhrcIQ
1ow9x1GAEEeB2ffeXfqLRsMu4tnKbEkt6WLyBmv3YHjHwtVcKw4l7YSU8lqRNZgBxH0+uN4xgX4t
H1I+hle58R/POaQn7U2dSfxaGkTm5GLimPFdyMij+rxuXgBh0J/LRSOBq7MNdTQ2XuGvl7cri+L9
XPWXH/k0AqfGyfQQRn/elsG1kXdIZS1TRHPaFbYXdNB1xrA/0rs4gzfpxH4C0EyZah/Mz9uLG6KZ
QPwhL8uZzru44CrqttOD/NdKQQHjE9dDwPu9i2mYyDkaY6P0wSz3oKzRNElJIPAXbzA8i08zTPF6
AX8KYhW6eV32muoEqBJPmVLQ3Y51qoa7d1ixcvfojdYFsjLdl57ZjSHMtPLzNJwNkDxpC2UHDT2g
PxK7+UUvznJ13ElfzEVG8krSe5cxbfrI0U5/3wBsWJ8qB4QFKovefv/of7m8yHS0PFbHtJA1HE48
r8HAzLsVok9AFvpp1I2o7OWpvI8Fe53vTMSSJyNAtXpBzql/GOhmbI8RiYdUvAhQO4gKFNWj8UPX
cCEs5EUhFJ6IsrSHaHv3yUl2Uu8jbO0ZOBg1tggqeNJkdN93Kn4mGJzQagtqVdLiE6pXXXoXSTs0
BZpPN/1H6oMWyD3dxdQSzsqsCvBCscbzvVGSc2cLHYUNnU2zEZUy4v6OvR13W6vGwWJRLGxKH+Z/
9aREQ8gb/r90yYbrl7XAaqsAA7XjhOC+QvlH5QVuRROA77i/r87OgA0XQK+C2nWGsUz2YnX2C0/P
FeOhg+O2RI/yeZdOvuVd4Sdy1owdy1FQ55uufSgtZ8as760W8HHOKpTLPKOa8JEWETzBJrDgYTiQ
irNVquU8SIvhT5i9gGW0Wtv9l+7fBEZp1EGhqVuCJROyyDjZe9xNGwXBgn2rmAROfJX95XpFETVA
WN3Oe/SlDLcxFNPpbRfrao0KOTY2tUdGvMsW7sUTgKP61PayAxF6M0kDIqXb7FMpGsYRQxsQtXwc
ZJkOjvd4RzF37MZKmvPTIRK2q8gHqEcvd6rGU2A7L070lGkYCgYi/lBP35Bf7sPhy/SiwKsICXHs
i/+XA3CTry0XWlqgnZsUcuY5TW0x28/ZC8MU7SvAxpLIPBMoyDJd38saXoz4Fw2FV2Jl+95HsKTS
mnDVkkGngjN2TyBhItDJoVPrynZ5375tapGhdsj24e5e/+yHQhZU/xtUT2tqYbWK3AsxZZmnbbRW
g10fFBryRIfH4OncHOCzAlBgoUHEZmeiFBj6nLyD9UO4W8gXT8K38RJ54s1hn9+1VoE9xLz41XzG
b48Ibk2q187MTWne598Hhk9vGVT1Hxy7D4un4bzLUmCIa0Yl6ISMF1Qx74qd1DyFmtGKL3OQ81Qv
IIJxUOekhUSdErUBrMp7Uaa7GfpCOlTgMre5iHlmXGAaAOllfJ2X2jM11g3M3STJlax1mWhhjtQv
1uixzEN3Lf3hYk6wLQMl6H3TR7j66E38PmkMrNReHKIJHVvCsHBdeiic0JEKrEmSOL0yYXXTkKGj
c7XO4dIayG6aeQ3dFr5nbRDez4ih5D9xmLJC4vZ7Z/JQNkh4SWWOu+p7LarCwjQP6/CNe0C358yL
ULpompmp++eDxJCmSojfZpepcQ/OxsTishk7E5fSii+PHS9uhwtrskLBMGIEe0cc8sA2Xw+R9QIX
RxuV7qQiIJrg89vOaXUmGz+tlS0nBcVwRbCBFWcvs5KlamJs9CTrb9nY+A6+MyZhgkhyT2PiaC/i
vUnAbRNvH2yLCkEGh7xX9Vc8/wJZGWP8xS3QWcNzKxwoiNopf2U2GvQiRNin3CaEqWdDZyLjdpf0
eoivMHk9cZunFfrmes0WXoe4g/UXQ5VBN9I0ceHSHwGqFtJZv3s/tF871AQaqVLSwbc6P30TAr1Z
uBOEPSUpZ8YL9X7vZUSJntjL/0SjQPm/e7vyEjZvi0WxuYZ2HwsDJIYbInWqJ8SbRW94IkH0FljP
2RpKn3iRUcEEeN2mMu18qwMG5m6R9ZEge+gsvOIN13mL+HdffWxQ02zWfrcrJMwNLki4fqGtUT55
j6Cxwx9r192BDau0dM1NETc3KKYw4jHCNmUKwm2HHuIRdKrW42Bs/RkEWY5btWwVV0c8UhSA8uIn
3vwFqEbAj157DwpwYHX3OJ6Ttp0A1x0oYkZG/9ucPcd39ojHNCVk7HL5lKoXLUm5AT75RTRjqDTm
feNl/mIXYlv14CCPrIzWGCa9eaAXWApDubF4a3BYy30DNta5aUcvPFguv7AjZPgzCp+Y0vDTTdYV
vREqARfoSsUhLzshkCPuHijfDXmCwGGjfLQAFBxgkXnz89JKJxTStVhapUJ2clvdpxiPtB+eNdSz
bEdHO0sZh/HdvOIkjUIpIIV4dMXRbZqdCJugxUaPYS9+PYx4f1agabA9U1Jx7S0i6aMzKtjc4I+y
YHdBCR+ZHeo+3cxbWru53pW8aHYbjvK7uEiIXIEl4xTBpP0bV+R+MsmzhrYPLSDcfL35AFEnvFBJ
jd4U0MW4w6wUaJ4ngwchdES9Oks1luAIkcH/S2GpOehnUKt/yp3YwQRFm5tMXI7gbnUaoIwocNXS
pAN46O5BBBkfz+x/lsq/TkzzIh40mhe6MlByz6shNTs0PQyQXB9tOKjNd4j/SKqbNyJDp6XKsrJQ
8cAisLiWWOSF1/vWdfkqRKKvnWpTLGoVLD2QbCXH1FBXYFOPzr9hVhfFt0cDGcIjjS+Ht6sCVCvK
rCCHiR0EFTUgemBbLJ+AKIKBE6x1sV/m6j5h4w0D/krqP5CGUzplzbSUo5kuz4+YRM7qvqnbXYGn
E7VrDeQGlOWorky6Ed5nKl0d76UibPzlLNAPRQBaMtQk028OXAKll/b6+ZMkvoGsw7LiMGSrUcuS
uBW0aPGaBY2Uj2x6kZgkFv/aYoMlm9x421eNEgju5H5KGWOsdBrAlt057OrnpNulEEMN9iEH/2xg
fkF8VPudzsahY3t2qaN8E/kRBeY9vSXu6elYJlfc+4gU8W8FUtidnauOBr+UWlH1rUQL2/oQ9fhl
vPx7VbnIMXibH5XoyNA+prnteSiiyDX62znaetruvujSO6helfgBPoerb4U8olRyPH97xK6WSZ3O
EEcDCPd/pLBkdR5xWXaOpJ3ko66Klr8fOFfehmTfIWGaZsVa1bcsV095Cbz5plmzTwHpjeYDNgeX
c8Vp7LD0ViJztd7peus97FFbpkqVWxSsUfxBrd02OGUeUtJuNmq0588h+WE3CvoBDQs6AjEFNG8T
H0WMfMuHnQ9LNYkGpUUIuEGMV47rDqoFfDPIfMC6nYMpAlteIb0nczDRfDJRwF24SVqdt/9vfAEd
Ikm/vvo6I1e25zom9izAtbTHWtYS4l3b3vMcADbmW1cy7OcWYKGHH/wErX9ftfz/2t+fbcUW5+Bw
zAn9XmRg8yAGjbDq43SiN6hyZ/fTzeoWu6drgN+m1np/AcG/CWtrDyA1NcaKJer1IyABCeyZytDU
3uVsAN0B5jMkdawYTemvrRkdukSSCZKPhwC8kx1XWdTIsfstguvYh5t8RVZjfP4DfIeXyopqcMEE
q2Wf0qMw9CsYmp41wIp0C8j7HT9XCPdUv9HgiFqUo8itbXdSTJGNu/71Y1M7UtlwH2KjKbhrBq95
7nIL45Xv3eWjRlPhffEVogssk9gABRdM+TQeClO780wIH2lkmHtDGLZmI3kNOmzLjeTR8CWQfsat
b62UANmKcM8JEvGsLhV3Yt0kL4J8maEISlpjhK8c1ZLXiSVJwSa9Or962NNh2r1l1VrB8duyNnUh
UuLo6VKcDSpLk5DiDWHTsjpeAh5w5bpGh+Uv0XD9cXjk53d0unPbTaQOoduue3SOPq/C46PiqB6c
gUTaATt6ZtXZXaZa+fZyEzuTKvDrLZLa/r3IVk93wvPMiTjS4F5t1+7XvZ1wImkJpZRke6rYI47V
U04iE7ChNA24q9cSWopBEaANZQtmbwjyRg8iOFt5B7H1AjC8jxB7DMZPwUqRO7KKIcMp5q3tochR
DsHJWOShHNCl3tpR589LaANToFrTkKcD5uiINivpM3B754kRlBThVVfyZmtDDiHoGqaT8c1Hg719
6mxxBGEX1ZGzWH6KdOiVg3YnlTodvCr42viAkBm23LdVd9DGPZl8Wa80FreDeSOADjQH7XAtu54u
zqP0OZsHwVnq2+yaMLt2bbxvxKZMYMpJFMPEU+Ybm8IthLXTP3XAFLaZ4hlwzoaXbNPmsL7rkqUo
kd7D1TzE3RuwlRk/yBWramKymmzqSUn2dr0XKYweCBuNztafRBdSt8OXId/3c0IqxS/7GPq9jeZr
BG2wTXTHBir1DcsK1Sw6PgJ6xLzqladFzwVhnEBurNEKf5Qsk+tHPZs+mgpXe5D3QcgNds9WPZUx
QUFpB926JgO3TzM9tNtB6I50QrgtGwiCq1fwqMghQYxcRKi+tggs/0Q5pNEO/NJ8y0i+yigo3mM7
N4P1uEbHVeIG98Y25+hHp4JJQOIPclRn6ikPf+UfSh6RwkP/i2RGwz4wiQ3GIltl7FUJqQN1rzix
TmILqp3JSDcjEB705ixT91onNT+gr/Vx7c3Oj8+aws+2PTrnNmULF4J0FpFvsJgn8r61UT8J9Nnf
E96+SeQarOvf+fWLOlGKK2R7lHHNFW+4zMVKgox6fisA0WMZi6ljUKlVyyld0qdBBwvQ6r0CIuUr
S3gvwsPFBNTUgxkKv4LQ1mtIZ/AQrgQ0JLBUoYbVZ+LAzzqY2tqZPRM9K5BV+iZtV4XreVgw3CrL
dVwus9DTeNBHudVn5GKtlroUohyjkjVRwQYZH2Eb1uBclQ6/UDdEasopfgd2WXW+IaunNBE9gV57
fPWpRqdPH8mHgpRt2IYGV+ZE+IdInI8GftFsjgjCEJri/vgr78BIW2MbTuhE0Kq3TgQ5bh1vOY4Y
EfAlxMVZDIvWDuAOiTtNgGlh/UVW6QKI1EkBaeVGoIsM/9Ibhri6ADI+4Aui0bUiLEfeeOjrXU0b
RLd7Yv3dmLHx5W+3u48VHdwKNqDNRs1e8GjhNpEWiHHwW7kk0rmeAQHI3CdCrS9CCZD7o46aRMIz
qnemVwIbv0b3qzl8UNRc4pcj3lf8M8w5IdbFLfEDtwVidpiuY6/Y33xoNWYUJe80meU9xzUse+Q6
QYelbwD2NZmFKmXh/15EzKEeLl33+zv5hjwFccOW81zATUrG3HGEiBDSgVzEJ5bOPhMeBQoWee8n
lAoIjfFsvW7qwudWOs1QuelKonW6C3ETWMisSvNnmvZnLgU3lRQwFgVPFDmUfGKXjganhdJM6V6j
Kk7m83oozTwP+VccPhskEeMtZENKJYaDZgmEmassdwHufCkZjlixjLHVOmokKdaFL2HgenokLqBG
mYL8mxCgOlfKgiYCExkOqgocfwOoUPYN7UpbHIQQ+KdwN1jpnCJCQKWg0btavvGHH6aQv2baFBHH
zijaaze5qrWRBCix6MArwwEOwuN2t4TunPj4/0QJ5v7UuhLGD2i/GG1Bw6XjrGhlOQUcLVmW75Ai
sBN0zsIYspws38W4hbhIkj6j3jDDENnYAvSPh5dSk4x13nLmk9HlesdV09+3VoHChYlhKzLVE6Fg
6hZP+1VVPzrgXpmRoI+wpgAtAP+OedAcpb0NkVR0dxhSsLpRzpwSHQ0jCLnSzyPloHpk1BEu6hs9
g6r88blAHg+oRfzhruwZGDIsMUZ22H7TqVsNKzomb3whUMnQyzFG3VuHYu9h3xp2kif+0/XhkCdk
29aj6+QDxV95d6Tj/X4ni/JXn4/ZXR0Q2yod7VNg00g+VOsMHJsSJyQN5Biwpxx7zOBqf2XpBye2
01FWxLLgoC75ARrFWcgVZ5I0HDf8NfxI9odCO+AiuibCs2NgliZaXfcFwnh++/YOh1anmhDdg1ZG
LS8DA36Cab6SzzaUiuOgGmt3G/AeUY+C8loIcX7u50mB+NfOZifAgSZZ+FfDdchY1O/qAsFBqf39
3Os41qsJs3tXyHBAXUYJLyDnYI9mhae6e14fgRnF8M+y5wYJRv7YJYz8nioknF8sGTSMRT37zedp
VSHsnYlZPJ8MAomyKHQyC7adJn8u3xI59TLaEc3r+fbk/htXc4B2JmkXgjcO+a8//CpCR8yOlzhH
+KdkaQRFmbHYPxRkZdiXtaQ8MlH/ukC4Abs5SnSmge+P4R5NDVRcLUmSe+omXGDLN+kXpnxkXs6A
EncZPXLDzV4JR7VI/qvnczMmTzRNcieKYfCNrPQiHGXYhvotI2JRqbp2GsNFHoY4WnvOwQOw1U4X
QbOytJmMznFRNSuSlcqMMXvzkNWPg1Hoa2M9BF9ekrukqeip7Za9/dv8by+D5x1G4OrOo5vZHJ4K
Y943t5dqlmQWX0XUHWCaWYIoos2V3HnZVyA8x7KEpCSKgGCkhH+NVHBXxK1BJqGF6+XrwzjFbpKE
tCpRCn5ZUWZZgnj24/NyM3sxD1z9k4lqM85T2FnBP3Uck8BGxFYCqv+G9WWwjJ5TnvmcJOvtNL4s
lKyvpLsmjLc9RUG0MAPlVSrrVinnBoeI+fdcDNnIUNZL/dvxCQZE3Hw00Rx+3gJyPbIbu24joThC
wNnRPLCvUvrIKajRKIFMPn5RQTZszXmBMrJ5b170ZyV+5dG0I4uIp8FfnJT8eCCWOir6VX3v0/5l
DBqrZsrPz04H8ztyyAsGom5hIshdVnq7FvQMGdob0Nj+Esq5NzD//Sja1Z8e0d1Msz9jnptwLZ9A
3F5Ru8/ggDgiYaR8QY6ujSkLeEc9brdRIhKc5TXKMUOPlKvQWL3jwlwcjzVM4dpbA/wTL0WEtcIE
BwogusZoMkRasxzsuj/5soZAlr6tygPub4jq12D2x57m8FTBu6Q5e7aFRX5qa/AUyCa94aow7SKk
hhMebS/Lvg4dHzL5tsO/NbsrkpoaPREVlatDJJryG8zcpMCSC10jql2c7KXJ4HI1cbs+3DMTFkVN
+BGTkPriyClbroav5w6jES15YAuDfiDbKAdPxzVBDsdmpjawC+OngfZJkC0t805hJ9U2DhMmN/me
jA7M/I5M/ikGBEmIYTYQvSJHDyec/HxQv1IQzGJz6o/l+mM8aqCotyDnymnGYNzOE9auQMKtTpYz
z0jpjslK8oWXMSqiToOLb7h11t+cqKRiQ50n1L72sITPpfZK+TwcSox7ubrJdGjAbWHL/XNeLrkj
lrMjGArpJ4zwipd+ikzQcePP9gU+EkUbBS0ycdl6bQg9POVfA48kcy6P8NJvQbKWNErH5IbV9lMT
/WEemB2tZ/E9zf5ZW9im68xczpL6ealwYU4+YgV9WcdRp3Vn10GOQVtDG/kB3Jtfo9WG2jePk9/R
Ogv7iO1+mrS51PwfFYUJll3qCaZ9ADP38FRCbo7VD8oW6XFfK1Xma2RCEU4mUD6QLW0hwAmLBQLj
CcDxZZHkiG4POgRdqQlMgbojcaHAKB0fbjONZ8RaGz5l10BMYLWPN3pQN2HmJ+tkQdD+irmS9kd8
8OxL9IWaJ/dLBq7+iNUAcBfJd7Zr4kad/z2MeFsGIPHBHFwnyDSdhI8Iq4VeuQVSuD7SUhdUfc4U
r+/fmxQIX7JUurjrTBI+ldIZS62uYyI5lbAcJqA2QDqqgbbnxU2Z0AdD/BaJwn8sws+0tiDk5bcH
FuOU6bCOgNohCUX4h8eYG/HUh5bHkc5aysAx70VyaSrkxt5DpNQpI59pVAu7epOklG0t2EvNlwIR
79czk0DusE7Frb1ncmSGxvWg/aaIjwwAIWY1y3E0wPjsFZbFeCs35drxHWDt4sf91cSZaUlRdnkg
r6GeLqi3Ns1Dox9P4DASmCYlAWqRlMoIA1iOVW4wqfoxnXopb0+7/48de7EGb0ca7/BoDyGeWX2T
AXY9ncEp1RNiz6CJ50fn41eBa0RPEFw2R2R4eN2NKYX4lUR6KrTPtUcTDANW24iY4B+oPS2930LS
BlRTO7csH74wlJfrBWNyrlUy8OYtyOtaVAnY+SQfg8mfv1Sl5rIeaeoHg/yOmAOpgbLuIL3EZ+zV
S0vkfW0dOnTVxidL7nQkD8LSCxoVByv5eMbja/KQZoa1MEUIujYKM6duRtAMLZpezi66gP9qDe/C
qcrLpW3Q4lSNOu52fQQNooMYZmxBYVy0CA/oFejx4x0AKbu8i98YkHTNUIn/35Yz6ASwp4ejK6NM
bOSueqDC+3BOSoH+gjbWu4iG+ZSmITvmY9MDi6KhzwMPLzYsyrLptgNyrXhKKdNwfjyHF4CaR2yg
k3YRB+/pAivnSKLBOsSzr/dmeVHtbPpDEeT1xdUhM2691ZrgnkNoRPBlso+1Raa06SV1WIndu/Nj
uGqnoBfPAP9Wm6Tt30axWE/24KTK0CPBxHq6HMDFQD4Ds1aCd0ErhhQx5B1ihq2bQfvxnsUbGZR7
08kfeuFyFVdCjnNtLaVsH2D7WMDJOoioS5GtD8JNUUVcYrJUujIgjDq9yRcR1mKNZ48k1q9HHbA0
YzyhrVwfTdW4NiX3VZC/GpZPbJ4XFyve1vStOs6Fq9lPr7d8ELjwNRE68WdgRh4gXUJbowOWduMZ
6s1IOYEc2W+WNRPYvWKZIHDjLPj6mCW7dbru0uQE+6EpsF9OelTLtuHzXifg1I/D4eLI4BSTq1qh
z7wT0vKH5O5XZ5ZSH0qF4rWy63+bt6Jb/k7CN7hLl9UjO6nfFrHbyx5qoRH6OVFxdw3NjN38Tx8p
/cnYeuIROl0FowtkCk2lGFq/F8lYPvgy3tpwAjBm3fenlEi9mCF3z9NhIDwRmT4rWbyqkStYM8Xm
iHdBqYa+xkCJZecBT/1W4y/XFuP7MqmhjpsTnkknD9sO+ofESdX9deerALB1iQc/6eBt8Nn5UGFd
ctxMrhlF+JKGjy9ObQ7I78wBjYRZGm/7IEhtZFVgs7NDlviAEYIHR3svIcoJuBhlcRRtyyoQ5jWY
XLJoZZnDr9QrPba4zeH3FUjmzz9urvtsolJTjoYVWQ6wSn2XXpnJq7796i32ry4ZDMuS7TtsDlFy
XIE1BQFwvTDZdbzBkwyV7Mm5K58HYwAXoJuG8sUM3wweoWobGmLxqamE5ERbiwNysaSJmrrp5uIS
fxKQUfDN8vCX4qEpCYnKanNFVXuiuwOoPNZt4XSk0BqpAixh8LBnthq3CMyaqlDczG5g4Os903yq
A74EYsuhyBw2FK7QCi5gqeAtY+SdsETiDNej/nj/nksWGTxbqPapvTHmYBBaRwhxX82Flr98yP1l
jS1QjSaqE5xerfi2lCrsBJDKLL2brv8AH9zJ3i4AvNwspmx/Q7k2T7vBlo6CfPElq7Zhmj2LLDOJ
wI88rgIdlOPxn0hO4jJUxy8Lx0xaXyAIp6dqCeDHkhftxyA5h1oaXxmS+FrwTk4cvSulzaUIfH2n
qfQFmdvA4HsdN0e+CzLpuStY5LCuQefecL7iyYHOMmmTCsDmfq+wYFdJx1yQZBWEKypmJv8/LVdg
1Lft1bAfiBhq1qjNsQUcmaGXk1rVx21lXAc+xLN0Ok7+BGOELpqjMggohrd3yOQRO8JXcdXbt1Is
FrTj+yMqJiZjfbUhZPlG7SVg7JIsdZnr5ltQeQyiyp194UU9Tj54Qg9ITziTjyRGLjR7vC+X/2+o
5/yc42QsiAdE8Tm3Kw635pGT39SlQQEABhXpFo5quqetOXqsbhaeozi3m31ntm752prMsS5SCH9L
U8xux0lUIDmUocSqMruP8M95B/W0GPqVskGFqQipYzYMTB0xnJVh0hv7Ddjcb94VE7fI23UmAAG9
4+NvQBpDrOmGGO8SoTEZsJryZHOylwZr3M76dq4ChzdGXwswJ4YOgrdVyDtVz1zGEL28w2db6fvA
Of72qv6tHw8Tc1PHF7Wmqoark8E9OxiagiI3g3OBKYuz8mb4H+C7ZarUwDIXL4OuuYGP5gH8tVhG
aNztq+hS9Lxl/PMdyfSY5N4H6bgkCCQpCZuRLKNjuN/1af4DIitFolSrquommisdEWoLKFl5Jo0x
PKv4CiW/KaAda+mmouAe9BTv4ZnX6vYZ56uv+CaiMsim7a/PUELDnq73PDYk0WHh7G7vm23WbWVh
rkHADzeXW9Xuq53dbmWsZdV/MeYXdiJUl3uCEwNmfX/gjiOFxb7eeWrw7iYl0KemkRmsJIWKDQrX
aloOYmyVFe1RPy9Clz7sAn7TzQRC8W9eO8379LM8ks9uk7if52cqENuhXybUsxXSJ8UGRCIbkYEd
IDEagFyubJWwOMYituAuFCW3t0Grj/KDYIvk+/oAD3RCh38Wpe33GoUvQvXpQU2TNVSsPFo3/F2/
G8SW0EDjH3OAbNgojyvPiCuhSC0j7Nhs2c/bfxTJ9I+RUzmwPpJIrRJ3EipbaUleCBZxEiCj79hI
X9RuD0zc8Y++/poWwmHikRO6MRvi/8GpnyAAWkIwP5Ylii9578mSPr3tKi40m6qE7FAGfAobHGTK
sTP0BKYOfV3v/mr+4lXzO+n9p72xSY/Hd60ZiNoFHdLmsed7Ewio1mRdXnwOO6vxp/570iXVI27u
vKc5kXWpffoKNLOognQ07fLz5dpdHebyN5tm2WhhEfRh/72nPU9tkjBqJeBe6klY2udAUfUEXw+y
f7bUZzsaCPyNE2URwLhMSPG1vCeGheeiElV83/hX5upC4wvjIUTg+dcuRVOH9Ll+SypOjZ1RK9oH
qcNiaXK8Wmvc1q1UQZBl/gXF8lTNdLr1XvO1qqfi0ylULZnfMFo9Ewh+zsd6PaTgxL6vb+1hWQsV
TXs0Hr47aKybJ03izc4QMzxA/SBrO9K+SeJjceZqSqt3EkWl5hjrKNsR/IffE0csZi5mDKyVXEzp
p8Ug+8hlN6XQVDGMVuiWyhyI5uJKqxGabxVBB5sdfB3g53Nly1b+K6vAhqidGx8wERvUzlJNPeQo
1C2bJUrY4RL9TGXT9OzJi1UlUUJeEJtRmBKFeChfA9W387leFomCGZq2UKjLazN6FhT+0R9IKp40
ckpn5qWQbw4AQyIwgsNCLs+jRkQVOLVeQDCmRwQIJiKZhISxDvgS6YkvM750pEvDGeIYSwbMEk9G
TF6KlZWzrM3MA7ebCFoqan9etWBbWIXBw48vtKu6DyedB6IEHEmogQn/uekDtcOzhU6RVAxuscYt
PV7Q7joNzHUTtE601cODUokkfey6skrheJvIr9YhalKX3d08EplmFAjvrXGmYGmW/x3f0nBz0Tch
Vs6wWCzYDqyH9pp29YI6j1Ikh7xQTjbaTyQwVFflDmjwPnvr54egy9ZlE97Brk6zEEXLNA/j44EM
AQw82RBWEGb807TmrptGcmReM7YKI1ApeoTMyUvr/S2Cfube/iqioDg7mlr6MQkJ20Jc+ZovXj6S
K/Nr+ds6+szMXXykGNSVGFMgx1x9Y/N7igqxlP6sAOVuEUoJ6wii00fOifdG+HbpAYpfwba7/Ga0
YCrsFx+WR8wdZiDcT3TJsUhjM0Inz7FZHiRDI9JVVX60+3m3vGWIh8A+/3t+0l6NIYgdMGmNaG5K
/+ucj/td2FBTQeFlpNakhGev4G+n5sA9tb2atEs64NE2WlEYfSAjZxsfq+lFDyILcVroYA67SJHW
LfTZlpgg6w90Vd21dnb1Yh22KvZh/bKv209QfN7HQeiANoq6JTVCa/ou+cfgi9opDpdOT2RiIKn/
/4CI4pijaLdliao2x4Nqk9sDeTFXEybiMHVuRpKKSME8yZlrg4p2bdyoYkaqWy2ZHope/QJH3p1r
MKbIRkaizijkdHngLmReF8XdLZm90AWkOsDlxJaR/mextgt0WdGq2zJb5nQXqQj6TMLsOhRHjQcK
mAysFJVRi9Qga+HaVQR8J4c3OxEGD37d8o8qlXY+DAQKx1VmJiabMEL2h8sIw6Xsn+bBuQ+Dopjd
1V0PjekVYPVPoXaA4MooG3/YAKuKe2FxhGZvBVd3z8L1UGl1Rpi7+IX1PixW1T13M7S/QfXc+QYd
ri2p/kpZsbsnxPN58cS3rFdEoWJIqnSOIrR2f5lPPDnnzhqMA+stx42/WWkV42xbHxKCrvMzF7N8
blNrLgRYip3BJ4aBgURR6AcAlinnT8L4N6ORmA7h4T4pwlqE6BcbfGCPcK4jhbr4Giz5WrfNI3TH
aXqxm1ZiNFwKtR5AN7dYLxs7t7mhTyWFE+PtYzSeFafbWS5UQYxvoAHBQ6rTVRnAykRz20R1w6Xd
zavSAs5UwGEXRDLuwlawtNv8Y+NG9VClQL8Hzz5zER+YFtHnAOaY9s4IOZRAf5maLo95cyE5fU5y
XhIOPaeLVEGhQTD0TWOO1513em14W65xOpRSbbnhnQnF0YaMqW6w1Yx7RFypdESBaGtpWuOcahjE
eWJFhiaVRh29cSGSgMZ/Km/ZNQ9eqxAPar96NhIPWFHSkAEELnK1yqp/31Ff97I1fQ4jOZ7U+8Ux
d5hAyA9DewgL0vhwrWQ9QeydT6ZXzrD/qixB6XPdbSyduAJOhzEMeowwwRS699R7stMBefEu0bJC
nL+nBanVZtZWMqz7Tsc0FkPOGhQNZieKa+lx451MTe3lg3R5E9W1FEfHYdDYOmbc9+tAzaUbIhQ5
DegmJN26/FtdKSKw5+qlarhoit+cYx+lJl22oHlyzEjJGxjs4wcPa1qb46zk2hnjYnNqkr9fYOcZ
vIi3O6JOfNtoEHIKf43k0VZEVvcBTppK6q/OjS5s83dFEmWjjK3ZHEzxUANVUj/+mUhMjHe26yZa
C4VhmB1sITAOW/b4JEEmricDsFiBVX9ALDznl3+pz1NWRPgC4WjhrRfTCn3GDynf0kZxB/NO+les
B4tepMg4ccrumHXT837mGS6rPpASarmm1N6Q566FDJ5xyzKenmiAthLe+AmWmeZ+5IzzSteG6/Qt
6o2/LTGPheD1Jm5t/OGbklSY9fEU5IynQCnax0Z0eWb0/jfRaQf8U+oykH4XFJLFey8hy7EpoScL
aEhx5p389V4D3DL4aJBluKVafE4sc7Ih9gAZF301oCrtVwOCxUZnJZGjxs28clLu9Ib06EjaLYYe
Zc5M70TL9H5QfFcJUqqzRFqGXJ8+07b+qd4fOObrVwChicuAHIuj/eINwVzf6pOEAWpsB7QxMAS5
O6l3kAoZCdHx7oDLayHo/aIQhxpakpaHSrMT0+Iqf3l/o5sXUU+kVrBmwrW9byf/IVKgAvjFmnif
/TtuxOmjqvyRcyKOOQxBsl6c/21pYaUN82uhZIQAyKGRhkG4ek5DYikDF9Mdvrip4O5KpTlTHvUZ
nrbafXlAMvF70O/WrzIMb7rQjshxnEw10YpfcrurNy74wjsUr94bqeXx00QlWIfBLGcxJYNKotv5
XC73d/F4cnPn8oHOXOvlnYxn4wggKz5Cv7PjtKwrlVaREeNerl/5MJs4EC5LHT06cn9JeWtwg8+w
DvuR/R8fk/v8X9BxzE7VGigeKXHOuS9xbKuAWJca6Ybax1hDH5OK5z/1a/R07laQ5LLYE/Hu3lV0
4H8idyuWOOOugCVp5wosDEDZGTZgyRYuvbPewuaJY0G/fY+hZkotYcQqkOF0FpMRzQ/f8U+tdvi8
ZaktJMl6I5IFa4okYU3GVP/dj0FvJeoJd/slPD2UQzA0Didw5Ckogr4RxpG9kv8+JpdlXZxmwqD1
7DBOkk6nuOnwzm4Yty/LkzSgXMElihqntGSTAtiSaIc5T9UXRYrRsoXKd0UbdwR8l4CfoU25DcyQ
C5QTtwzHefecQomsVeBdR2cs6MkonXS5djBqCGW0MIGQOl6EAgsl9vodh85sW0tuB2vJ23PGoVQ5
bVfDFRiRff64PRpI6V3R0c8mlC4QTehgOPEoyyR7P3ssrEx0B9a8V+rLVqxLON+Uh1IPETDzyAzY
8ZScqxLGiCFdafHdsuFt5MiwlOPwYiV1p66U89yPwF/zmaMjycq+Ovavr2PT79YW1f5irTZuoXYY
WhUDeH35XY59NC8wH6MGSN3HnPc4GxaHqT5PzU+f+yzKIu7E8bzo8PWAkffs+0I5YwuXkF4lPNl4
fRmnFeiqLg07faSJo5LKkQR0n1jcEbV61Q1iFUfrVac61F11DuC+ILe4f5O9wTLxFjP3UVBTHDpK
y1GwPAbpYMb1k6fciF3GG4PKx/Qu7viHb+HWyetEjzQbdlG0/Lc1JE1nzqMjrYjd5tiM1yocSgJn
NSmILhcbIyzZi8FZSvhZ7AWR/AeXIMIpD7FMZPaWGPx1TmYBnFvlQljRsiRjRCfeDE0gimSToNBB
RrpCl4Xjb19jUPEoMO1q7qqmxsjz0Td6SLquPtU4tj5EeZpYLz+zl4Tmx21T/38CUkvowWjSGU6p
zF+u6LM9JhoBfZphfpcKdIkC4IlYj06yjMaOLVIfFepkqB7uvoCC+gG9U120/1+PED9nMuZRRzDU
w12PddMYKAWOfw1XzySDFdrR4yljNgnDgYmfODsMgZpNDZxOlKLB/0T3tTqlyb7G7+WPa+04SkqV
EHvhLrOSNimeyYcR60PdumVQoesIbIqZxk3ffJFbTne1Nh1QpEGhikdu8NbHWqxUKlM4fjVr/aud
ouOMC9o8T4Gdw6aExgwbxgUkXv9fQIICv+67J07varUJUS+Jv4hW8e8FFsyQ5b7uOwGrN7NinDH/
Y7LPTSbQ7Ai7quBGjN66ciwmoB8qVXKY0LlvxaC5CSA5wXxUiWchf5Zyp9l7iWEa/Wv931uICd60
smLNUE5Zq18wH5wZ7K7fiBUsm9S29Pwf1tgw7fjxHeKvTE+pUbMWf9Z+OpO/SCXW15aiFhtBAetE
oPtqJRAemY2PR9FVJ/yxA/Ucmfr/0hQdqqkz5G6sK9/tEAgETrK6l65QMH04NsKm034xqlRlf11S
DBgaF0SXBujtVJ/FcBvOh/MGlmOoU98Eik8Xccpz4G+qWEH7sM3KixkPvuXFOfmJXYIN5/vfRRfi
vyzIapA4jPgd+qZkZahWR82TxiasON8LtstTjaehPsooulDblkd34UlYu9AFeaT8yom/ji2FxNvZ
gAc24n7IbNC9fb58DgD3YGP2r3A9kAa+mujXOoIiXHYWX7ZeEGpCDNuvK31KO0Wese2XJKnFVe0g
Hqr/mKpWwfL+mnkGGXDbUS1eqCEIGGRLjo9Fyc3qe/BHyPMM9K9j8InuKPiHzpnIqQj98Jx5Wd63
NAEmX8WVnRHjRS4NCRAX1Pn1PPkcqjc3/ls1hV0g214YGSv2fY4S8BzF57pO5MDCwgb1FRLy9nLp
3UNyiffRDguVRPqW+htwWysXhqdJTKjMG2pW7a3QZ9o3BkjCTmu3mKzI+7VefzK4BpMoy3cgRrzG
C4jarIxa1Tv6T1GBExJ6Q67fgeQlNunOedXpcuL1g7tBeM+HYsAZS+QqyhdKG9unXGG178Tsgn/+
hg3cynD6Lrc+wc0xe5rOc1mfVe6sd0lvxrE3RffTZHQ1yEsWyfSFkjmbuwDX+hDGiEkNHzl70R6C
tWLKVaL2tBAKu3qvH6zUxaRoPJYOPMRpupht8UaE8LjCBQDnz/B292/BMBSB3PJ3Yi1WJ79GSH6k
kQv71ZY79hKZ8RFu9yU7sleVwef8G5A9C21n5Gq+PC1sgJU3QwS8brLONJYQUi1xHkknBu9kvwyB
+uLDExTB995OV5A3IkcTs3WeGMeqgAAxEqfD6NRPOzZRFawjx3iYEalyyV6AOoa+HUhbNJgmYf2Q
C1lsCGF8Fhxo7x+YLB98FqiLdcgr8/VHqbf80gT3oRzacqwUCLzEsidnK8MtzOiQ3JKKGwQosgIr
AmttS39kgjVM5T5JYjii6lUvtlsQqalKDZupi4l7AlJrBs73z0udVKzV7ovUo8pm7P7MJ9V54B9c
TeQOHkejjTE9OJQG/dTA5lF7OdvmUN8anFcJ/aaLJ8AGdDFrdDAZo9TGDABMnYG0wkZOnK9cZgPe
96Xxxq04aofNx/NJ0p0oYi/T2Q8PxVydFLY1TytfdQMCgTy1ZoOMOMQ05EuxpYRRUPluJMEeVGx5
9vopHs74xgtm13oQiWCoWaCrKkDUAkMwLX4bwrwAmTsFkGQOm+PGDqosL8Y9d1vxoUeTkjhVizyS
lh9qwy8zE6rvN2nM74yjOI9Wplc38ZGQxEXXZZ5KMYGsysT/8VMiQ2ijjrVFUZf9dUPTcBjup9F0
kgkpcTY9VH36qWzx8x2is7VCi9jfE3NFMwWqKhWGgBZBjkMW8/T8aJUh3FtZWUvyZECIXmcw7ZUK
IIcCN8Dxs8Y+az/W9EXEK70fSQhR7SJ2jefFH+vVUiZMA9Bir7V/HNkxkzqcgbtKeF71jQJR7RKP
F0TQ0kCKlVrGH92EWOSFaRsXrIESQGPHkDWmRco04d+5HFCmRmdbalV8tWXITRla7dZQAMppe/4R
86a2EM6oKXMAtvk6RMDlBdz40xSVROltSdfOM4qOOZQxqpPe/N9BUEhEnt+qTk4mkO10DqHsvtkY
D8fQajl38QTWQMJ4KNcd0wCqs2Y5suksU6TJOUbpiFEQ1tEmpukLyqfkhLCf5nL+W166eza5yB8R
4h/i9HGWu8T6FqUxeFCDVY0Wf8iwcfbbLXpZUGoLPudRlfYZ9aygOmGSlIzKLTqAr4zkBrYuMGeB
rK8x7RYHg+SUDR4Cz5MzCtgN7z74qsQhOZ1WxBavIpZn2ktAAChJm32T9C0gJKevc734MlugYXIh
mGBDf2QYWY2o9oYQmaVNKdOkFIoVAb/5F7wYGN+CPgsfTIJ9TUT5t4N9M/ZXXyFaXUKYduo3tA1H
ymHz4N2n/7B9gCkJouo9scQ+yFnKV0nE6SfWYZyuSlCDRqRZPNLeIL237ncc455cqawI0lKrwk5z
flmY0LXo5UAI+PCicvKufCKVkQvuxodrfVpJVbiLptFDSMWKg0fxJ1O3ZbKKF2Copy1CgCLGgAr4
mFBLof0+dC4qezCkjFqgBWEFwuA0+hJYPCMZuSgwM6VNZ0oKBgtJL7G4lDDPVN0o2cDSX2IXS2fZ
J3Szj1//gskvGAnofSdExQD2M8Gg9nEFkD5faXTWi3I8nJXZvVRdfvW5H/kR0cgt4PNhEk92IdNy
18AWHiyiDr0qHUMDNhMbTqEyI/U2/pdjEYRIaZJq2BxUsP9wjus9P1m7QsfQ3vIdo90uXB/7A28H
Njyz1lMXB26M8euNO0vbN5PtcXdJtS2G1QVkzhOiHwrS9+7Bz1eAMzNTUAyRln3IIWptMrfObuDc
gUxgJhgS3wDW5Sczw2+HWty6EpoVxQN2sq1tZxUdTFEDEspxToR7qrz4Cpwi6lCA+yzwplKKLe8Z
q0LA+E5IC/SdGFk2PXKaicxzuWxYGPVGw5B3jR9z5oXLAjZxTdJfQBUuznzqOjXQsjBJjx+T0+CJ
TsJ1V5DzeWgzLbsRkld5kxwATniNoeNUgH4Nw5bH/nbquCEY7fPf5f2b24z1fzYTAqrfUIXGQuu4
RT/NNzTfmJPT3d1KSm4siB3KxoOgGhIJAl9heUUXYwCHwjMJQZRrjcc7GWVaoO6SGFsUBMRhGNEA
Y96iisQJoRS83EXwcJlaWeL+cShZQQzUxu5+haIxNnqOmy4q/2r+le3G8Q1qAOLc4Y6wpt+rVJNb
G8wxlxEnvA7//3WsOU0oBvFNAyRnOltSJ9FRCD4qTWlbt5ab2GMwx2t+bHk2960g/59atKtMBppY
wgfkaBAEpmQNH1+/BHVk+HMQNq8oACPb2rIcvAZb19Uubr93Mi0R1nisc40fPJiIhkegrORWHHIZ
Upg93rl/VR2MeJn7cYBPOhcEKDmy6Ln6IfH0j97Zq22FI1cGTDrjY+eC31A5fCrSNi9FgsfsfXDP
INLQBjWCx4onN1UzS44+7Qlz8QRrSq1Eb6MMb1tE+sgRE04iQcoAvAwH251BAJJqnKUrCDn7mt8v
5rQtVp5t2IcA14djpsnyOj1wgiuBeUUGJFBYFIEMg2ey5/nOjsgS/c9CF/tYoJAU09lPYSWtoaal
SGtSj+1YPHgrVjDlVrigZYse+Rfkj2faLqv3IVWb/joT+Iu30zqed3ExjugiqXSkJfsuXxBgNrnz
o3p7TYLPktMINtzcomIklpf5dl1zfGrbvi5sDxxSN82bjfIBlJTMwzMEf3hpLCbSQH38TCQKQ0cJ
bGQMML+r/mt3Ub1COBYXoxKmybldKCcv9xrq1ztaKmN44kH1oXW8TV9IT2VWjsZeb07A9v7pSisI
JLWkHDTGrM/dtCreLKlVfka+jrd7zLWlmswqq/IhS/6G6Hz0ak8533OEkEyL31dMDofZB2WPEOFH
/Bt1tW/A8+j6E61hfwGhhe8vVZEKvR5wrpSatnXFCPz0nQbcpYXr3io35By2GM3XrE76+WmbVeD3
xlXh+bkZZFXeKe21gt/rAyeBbMJmLPfvQxq55FYNmgNYGSaevK/u9VMOvDx04Ez5U79cGoVrdgDr
VuDc/YfeMw2MXdlotLuTOYWge5MrdXrkfVgbvT8vXZdN5BvOe9HJVwhoDh8DqLztPcChrGW7RMKK
ac54SU3rgiB3jmjNIju4ZY9bh0zAuUMdsQjOnynJUpZ8Ir1wkUGd55eZgJWaFmtTwZuvxc0DyUPf
urQFgwEy6BKCM2bHE0TCDvxoHiEstNqOiSEtbay5JDjx+MuDs14wuPLQh7X1nhEiHv2YxCIZ147I
rh/wCgSSqgbhXZuY8cZJb7/SLJY5T0Apzhwd1EhuxQon0CkDQm1TMavc0iOVHkbZR64X/g0FNBGK
j12YHq9paueWbS8CROkLTCt+Y//HzkqMgwDvItaApOvNjesZc1qplrrcfY9Uf8wpsnMaroANmZXJ
3vv1yNWRp4tHiwZSZ3pMAXyjdk+6tCP11ELJsAqdw39U1PCqDQzOO9v4KhK7aX0D3qV0+BC/bupt
WHnHYQrDQAI1ERvg2nCuLT2D2TFKT2uXMRkEZBGLQ7W/GZiv6uNIDxFirtn64oY55+JNW1g4Nl1s
Onf4SOr/xDU3IO1JqyRXVeg8oTDwiNA048mWHzR3Ia4Fv9cB6KAxPvz1Z4zX/Y4RS/JJt41hAtkM
NQJp1iqAdS7PwIsDQXOrKm83CT6gqWvMzXPZqeQYhRv+4Q==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
