// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sun Nov 21 02:41:37 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/alshma0b/Desktop/class_project/class_project.gen/sources_1/ip/yoshi_left_mem/yoshi_left_mem_sim_netlist.v
// Design      : yoshi_left_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "yoshi_left_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module yoshi_left_mem
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [10:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [10:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [10:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [10:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "11" *) 
  (* C_ADDRB_WIDTH = "11" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "1" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.35235 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "yoshi_left_mem.mem" *) 
  (* C_INIT_FILE_NAME = "yoshi_left_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1344" *) 
  (* C_READ_DEPTH_B = "1344" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1344" *) 
  (* C_WRITE_DEPTH_B = "1344" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  yoshi_left_mem_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[10:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[10:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 26384)
`pragma protect data_block
iPawH/en3QjU9DS2sW9AmQu01YhQtzBRN/1gv/DMa/dc/AyTRCviDJk0m5XPVvI3sA6cXbub54Rt
fsmCrGDWHlXGhNymtjMEY74RT+PuGaL4GJ/lDLZCcf39/qlh+VuMmu31b0D/KlItOwPNVAOUUwCy
v8Iha4n3uigo1Bk7HKTQEZxb540Oulpl5E3A6GO6dH7qErxUEKP87oVY9dQyodGm+50XnwEZnUVF
yOZ/t53Yt6/rFok8UYLgdM3ENIlmANQcg/+0KbeuU2y6wxe3VtU6cOSzkeFlGdZGSZ9YSC9tmMyq
gQfsPdEhh6pfet6iaL+CcONwNdHHwfq1kCaHnz5b/jTaTGE287s0AGNr9+5P2jL12GBRu2a05Utq
x4PwgvRS1rdRqQt9MVY0utmQISQ6S6UJG29NCCpJC67fmkcykQyAaCbmoHf7sioO/oCiYlCSTuv9
PjQDacGbAqyMy384O10nGYe9rivmUzFJkYo0R6W6lj+oodS6Co9lfVcBnJ9z5kQeE5oHoDOwyWAW
sdjqGklPqPBHtRuRSA5orsmzJYTc4cnH2zUooKUE/gSwCrgToFe5Mael8HSD+HYjJD5Pz0+sxC4B
25+oHOp5liy9Fj3Mt1z59rquuWkR3Ya3zPoRBWteqtc1cwvibCmwN2+8qXaYvUx4mVUbgDs54Vzq
wLKupWcqvByrYGXwC9ejFJZp3aOfgBywCfCgtpFABgENcMmPz6W11RCLuZGejrNaszXA5B4LPBlB
mEMdGi1eiAFcejVZdjykU/UCSvyBgMAsSI6epDsZkrANqBHzQ8dRx/tUCKil2jdrK4QoIcm0Tobo
kZrRmTF+00PKoX6gXFmUfj3VaL50notFHe120nLPbZ6lRzbUHCwqTqQhl7DQq0oW3lnXvhXLaSRK
1d72ioHzMa8yNHkFxik5onOy9hfGUG1+7WJZ96ph+jHy0A6gjRvsQYL7aKvdzPi4ogrKEiC2nU8Q
u+v5kgIEh1zBDwth5fx0ht+QH9GL8yBqMbp+y+DuSmARcjW2DYf98VRwulLGd8C9mfasjk7248ZP
oIaZzqrLUD96l1s6f1vwKfGoqVNxpm96s7ZsiP2MDIp+h4eUTeALDxIR7Vbq3HXSWkpgJb/9YcmJ
6RQwDKee/YqUWCJeaf0tO51wd1HPUeXl+5FHE5/itn3lskplwlQosGfe6sZIaffmRixLYd/QKPXx
l9aaConEZNbOx6x65EI68aKq9oL+UvVUfNVCZXwPHLYeIYxmahn3gj4o1cVz2c8jedPzWfNjoBqK
XKYzyGSJtiQ4LO1CmBOVNc8lZBloyKbkiOonJ/lYmUV1Hqv45I9bwGGcrzsNmPiGTV2ZPVVl9b2b
blVTczz5y8Y3o5GlXWFdcA7j2vlSO/NbaVSq4wIwMuAN772yLspM0JFEFEKzvoZWQOQek0DWWadJ
DRUnijslmr+k2acm/jxbdIX2XoBGVnc4niE5+sfjYnIcP6RNpsy2iv7CevtqTGU0ksPmeQCdpVRh
b/UIi9zl85d5PCtujU9x3YsllkfT5egC+HBUPBjsORXf+twu+70Qq7oxA4ZGOyQ/Q7wsj5xNCt6b
9mYGIJ60qPK6ofREMAb7Ui0vFopOcFqet73sZD087OVbieKIotify2kMvzf6+0RoKsPxxv3Rh5P2
gFgL4MaZNRpcCHICB2ExT/O7qf+JgO9EtEXiApImU7DCUfjHRnAcgpX9NS6to4xyprBQy3Pvs9Jn
xVmDzCUaoo5a1qtJJciRlfWj3pbS7UgvZ9YP+1nxesAUeV9ZiX/DIBpnNN3iOJ9zNwXG5jqGsMhC
BI4NYHo0Z5FPDTtopsZVNiT1Mz7q9Y/i0xs25Y7MGpOgkf42J63bsX+ODOsSVfJ5EUwIlyg6LjN7
iKsqsYxeHfXTe1vZZg8Hl+fvc1K08/FkiYPvMeXd0hO3uwP+9qYC1GM4gtPbUByvuYB0KMAswq1W
DPNliLXhJ8pRIFr34WXnJonB73Wm8QdXPf6g7uDbVyga3xPTgOPqCYiOawFzxMDmvnZH3TTPmiVt
EYx36puAwv+eC1jka8zL7ti47TNdsOo1PvptSv/Q1J1iisRh5Udq0IZLiuz4h8uSDFUanDYdJUz0
YeFLFt9ejSHLP4CRJtvndrQnE4W1EurRkYo40EftN0/3vXjQpOh+t3k/cC3r8JDWAzF5OBP391OZ
dVJr7yfTBCGwMCih6GX3pFRX/QKl76Nb540DI7HtqXKwSaW/hNAuxF1uspUWUYu6qNnF45MSmhrO
xTJRMCY1/0e92NLrAT5qxcPMxUvLP3uSszHLxgRXlos1iB42eVMHpFmFV/SBe7DljNSXRt/HirMR
GaAdPF6nVJzKBaOTnNKNyGfYyZkGo+juRxrDVszXz19arearBM3d+2xfHLEP92jxqNPhIVjR6FWy
PORQjMTndADsp2EOSDdOp3++7zYZ+SzhDO1mUAyFNW91djkpFw5Rh2j3sSpVpGswsWIGB4nWsDUW
WUuCklRWnRPRIs3vL47EwjepEL99413RCQnBoK6nN8LWXb0/5OxsJTLKR2HpvbRM+oH9M3qHhkGY
2+8vavcQy+B3WF9oxseISnLQf+WyhExwumAbPqN6PXV0ak4AFxCClNBhV/RBHY4gseuaD+jTjAFO
vtbZhl6hreNFNbuu6mhrsyI3wapPLi6SVNq736egWHP6j1jDhsaFKbLFbD+LFjfmYIQ2Di5XrAzY
2E6U//TtN7agZKsPxs1ukU1t6ZqiIhoJbkN/L0s7I0b82m7GLuwcMGyw2Fpq+zTvDCabI0B4zBtC
EszvbgKWgr2Kwc6tQ2x9ODIeEQLGUdn/EkJpz01SqERtDhKl/OREMKa4IwCNeDOQMNu48Cr97sqW
7a7M/e0SZYVe6weY+YF0sa4Zz/DpeAj3Dc/tT81jjj7dl9Lr8sjg4r98yzk9wz85+8tUIqx6dE9y
xWn2qc8gOjmtmkz+upP1zbwIoglOVRMHUvS8B/WiaxFOR++X95D0BNTrNXErmF3CnDP9THGh5zxY
Pa9g3Cjfv9aLkDvA1jVxns35zEftldYAis7i7i4ZIxMcuYKGqaGNBZZfhRgfGJEl4c61LgtODda5
A/P3RUXvyRpImrq+3tGswUNdqljGs0jVnVrn/iphZ0lqdfIoFHXYGrYvJTJNLSP7h7mRtf3gmEHC
7zJc032jfUC7UtNo3EQdk5s0bV78GsEMQve/eQ6L7xjUFWDAXIYm+5dVag9Gay4hNZ+1FlvZ7kBp
ERjUzjMcj67pykR+h2U9oM50pOMcOIyn2Xy0hebkwEQbH49saQmmGbbgG8flDTpNnB/s0oMxNlzD
ENwwbBEHWmYfe8lMYkjrCHqpb23P+aIpb1zkyyfTBbNU01Obsi+WTv4oePdmzJWmcE+YHYDZdmoJ
A5XyYDBfK2ia+ZmwGSsfNMLF50sEyfGGTTysKFALxwQ/cl/TE5wlsits0c6Kp1h5hN0EUatjscRb
brWluer04DQFY7+jba7KdcoWBNS6D779Ekb9SIRo8hY0TqfFfeMEGrfP/g7Z5tslHFk74Mu3bS7Q
/lMXXqed+9bvu/l9lNCVRjLdYp8o9YKo+T8HGiXqBeKhA/iN48/kjPpIm33pxZwx/c32uZDIi6pF
w6VzJYylCzln3HvoP+a2If5fN2eWIGoaWAUmU1MBo/M6aplbjBJd/MHXLzV8tHLnelUX7gOE2d2p
d9572JP2tD46Yi2kLl+WkTGENqK5EtdKAr9C3JdEBbVzW1Hn7Ey9Z3S3SivRl8eCxP+dcPvBGmUF
nYLDxxl/HubEGLiAxULWcehMKLkGJ9mntCV8qkUSK/5zQubeo3yQ5Xo+3MiW2sYnNHo3bQG77afc
1X9qddf1qIVz6AHdoafAfyPLu0+nSftYOrERWu5mQ/OjJSVcFqp2ZY+EJmK9jKE+q1j+wScuruIc
O356ol8Iu/dxbOYxKZWtL/EuNuPGVgjnMkHjnhWFhhago0hKe7+EjkIX8amIG/Zw9cYkLI8osv5e
4vFr4YW4F3AjDbzlzb35wdC+g2vxmVFtNIkSJ4gWeLp3kZKRAqlWJMS7GKdXLXw4lux2NQhPWm34
KN1+jGdVu+NuiYWC/+VH+Rz40IUcjaJ/ZMY2tvK72H2P8TV2D2H4hr2/UWxCzIUrr0szTNa5sFCp
GivNAl3Tj+tlOSwweSUOefQPDijv5Mllyqqn9BeelOmEuadL1CapP+Jj0x8uTYa+4RAR82UCsjDm
3eYxSyf+CMKtzTdNjoFBBRiX5k2O/3YkdSPmNx01iieWSzH9wtdmlV26i9oSSWPW8nHIDxonyCrB
t9u4VLwZmA8JB/4OE+gCO9dUGePL8HM1d+TIPm2+boVbupBaQgWEJVmffq1tl/QMnIwCDCpTU9gf
TeCsHbL3GwR2P+d1baS/n841pXZQl+3FuRrGO0YfGZa+oCf1rD+G6hx/LnZeLYMiRgVGUzCkgVte
C4uL5Ul/61BGYTyreco+GmA6O+9lJQ8oXMh6PLuGYOU35dn9Da0dbl7js6rBdtjUUrOcqtnzbDPr
A7sjOrfRGXmJ95ADJBZtI1eAwbHU0y2OrTtpFI5dhejQQbMLC++JeRvqGLMOBu9Z23jt6kYmfW2v
qjX9jgiayQA6LpDcRGj04N7oFWipVH01wyDlq8vbOMdysbKbA7LTPD8JeZKyWbWQSbE3e3h8F0md
2d9SuIBFDcHYqfHuD+5MG2CdiqPOvFiqCslFjUmJCZTpEB7DnrrmwUBCVJVRkhcK3cpW2fSn7QJP
sdJO1MWVQsZv8mpb14l9JBU2NMa54sElE7yvRc7eap5vlcvr2JD/B9QqnHk48ot7pvCJfA9HN8Cq
FxdD3yLNfmszsp32tYPYMgK9EoGOuhsmViUZessVNLprf2cXCbsZ12Bz2pPhbLC48W7v1Hr5xbhT
NhdPkmQSV92T0sXaim1yTiIL4B3eZD//uAr9fvEaE0CsSPuR5JNJXyocbsGysST5ppnd87xCGr1e
sK0HQaTzPt3T9obrEDvSTiR8yOrstoewCfn87JtX04Ll9+M35RRfIYciusWhPfduejbAmjwosbeq
Bh4pwZnnv9KmFvjXFhBdvaTUymPuJG9tsXpqbbdG/IvCs40OVa5SsOGm7Bqj9eT0dE+2rYdG/BK3
9wPQcXGvJSWgyX3M1Nt16bfMRDKnfeznBDk509DWIOlOlq9KeGkPBsASE1xEZaLYKZjdbL4YKZV9
wmjY63j5tldMXiou/Q1FAOdDCUho0r2VCdo+lxvDSxGPZyeWQbZ6dSWxDqJVuTSooHq8NWMGRA8Z
XbMpKdJUY3YUjw+ceBjuXEVxLUItNv9o1IBywpXyCVhoiu/pALxYYrII0cUSSKxK2fPot3zp56pM
yEoh9qhyIzJDXrfAmTpbetLPGxJUHKa0HQ9YW5wr/124qBgvRhouiIZs7Uc4OPT5Nql4sDG+K8Qd
bwxa0zV65Vhg1ULi+QUGe76XMr9q3X8VDc9eT6Tf+WSr0JU9mAhSFwjUFGV9RB+3BC5K125qBUeN
MvKq+yWtUXknwrE9M2OWJQ9TAbnRY1q6nLrZVmSt23/DhtRwlMie3Kpn0yW/8BZckwXfHCSIe/t6
ny0mTUepsSnALcjEHCFpv2vlKcHPy7M1OLud64bknjKMa3OQTnFDeGl9OzQ30u7lcntzouCSm7rV
ljl2rlux0vsKTzCM066viPhWbM05rJZq3m6g9EFHw6rfCfDpFKYPwtFfC5vNk4g9vhgtP7cQrS54
xl6GBH3bPj+yMpYWswcT230hkYUswUEpIMsgpHIu9y75fBELB4sg9RSLlxP0EvTACjZ8I7+OC7iO
0HHgPSLrlL74FsJt34qfV8W3SPEFM/onPDXQYNh6buZC47w5TYPKqRnqTIG+whKL7NB/Ybq4cHPz
96GmfgtcpZKZixYTY7flKxRi1c8uueW7cPq86CgYidqg1SCBq3/9G4ie9PgVoIXUuqwvPz6uU5aV
j3KLAMZsliKUm2/cVpbWVcbgSUOlCJhkY1APm9tm349HgJCP3wFcts8OebxGiJRx+qW6Y90rAvFm
mn4NNwawchu06Cugu2oaqBXtFU6jLf349UUL09AyaAGG/gh+pFTg/3+sgnDP+KaT9FrTQY2osgS4
Yt2JDH46ggst/wmhCX5332inHJ7CuzNLXPHCPJ54N1L5QsP05FlUvT8HVLmml+VqCjDwFVlPuZBf
oIoTpyjDF/ZthPpBCe/K3Wm4P65/0/oPjQXgljW1WDpgpBhnYcMPok6lVFIZHT3UO+cH1gUNJ5rH
lOYwOwqhTVbc5MktYL2TvdNOjPtyJYhrsNJxVxDoT7T3mx2Xn9tZOGh0DaHHq/vMzconcdaGwCJJ
nKpuH/gRH5VXHVW7bv/7xJDxLJBrVcQu1M13tkMb7g4I8OhHNkxGgj/5L01L/I1uZUm7oUT+mjyV
AXp8og0nej730uAWmUigYyKpyNQ90+dlkt7cwR/EipPalTDmrZk1dRIAfj3uRns6fgGHg8JyZ8OA
FXKJdEk8roRYijcPpio/XWj825AR575/p3wt8IuEqGmDw8S559vAVOQOQOj6kpq3owd080GH3bjo
ntYP1w+uXQs78YoHUCFinX55WfGzz2k6iEWyEpJslWVsgTpC770OnYWlkeYlwUbTrTlmnsfd64vD
3jwH3TVFPbEK3/XSqqv8xUoT1g7yzQAKPGP9hjE+KtlYz1T6wdZdCDiMFhST4HazHrDvbpmgQac2
uaOOQWr7pjWWG3iqp/X6CJlC80GtGWmnfaoZOHt0fttA+z6kVVutPkgxGKmRfiLOiwjPrKtcMM5X
VLdflWW0FdjXb+tS9tT8zi4a/5Xe5mbvKyQSIIgimgpz2SwAzVlYsDuPGhyDUkEM/PITVsv6wvRN
bGgbx24aSteAc67HVyxyKrk7asq3QI6Wd6zy+rY5LiK/7shu4iVEJnBKKwyWZhcl5yAwncdYLA3E
MgAuIBqm17N+btyXC7whBqgpQIi1v9w1mMDUf74mG1GNhZzKkK2N6nOuFRB5QVCD8t6bDCWwoG5o
tLv08e0e4HNDMx0xFseem4mcYW/b+R17RXI+DSFcrWFSjWHm/Y5ueBaK391b39X0TCJvZYLJgkFY
c9eBw67shV6AuskDbW0s/G4BxTN8Q2DHYXteKFGJbV3XEoLLYczs0JlGHgwsDddhMADch6CUW+K3
Ki/hfY73PDxfUqZ/RUFOfqxhYsBcpgQ3w9eLg67kufEmYAzG66COw8RIN2T5ZBB9sbQQ9T4PmJtN
f4M9XpT2gE2LPhe/rOs/jxn/DKLi6gZhdXY9x1mKjduFiO4aLl23chg70AOVWK/VZ3C6d652rrdd
ivJ7TjsPHgihRfXG/TYQtwI4IKQkt608V4tE9F1wuTN3jYdqTdy9BtNtI3k5ikd0kZef5P8jUpfN
pQoncsZATHWS5k+OljeHGdDjVS8ScgZ60eJWjSC1S+Zl4ANVULyNdjAYz22q3ajWN/xdBFlVbbTG
Qj+5aJGS+o7M9qwVsSuANFU0FtvycmgDwmWzipj+1H5Nldyg8rtS0wEdHLGTWt97ujpjU8aB3VL8
cIvzekYffn4Pmt/2j4a5XC5HAMJLuEHovJFShseuHmpsO3RPz7kCjwSbX02rkwUIvQ/sZKogBU03
dfwXRWUI8WVxQ9JZNCMkYMDtPAAbsrgvtKZqegmb2c2dznhtdvrBVWSUukPHw/DdgfXurH4DXIKi
V/YRjGts8RrtIHORp4D5ecaxgRRtZuDSFx60e6E6BDlrkpZuD46m2Ma5l8O7F+JqX8nUb/pJO3wz
/SLWo6ilk9RW+G3WNsfAFcxArMio8jZSF2uIiPCyfddwIaeE6LSSanOeXeKAJMhmJkAxAiFGJe72
SLUTgbm4FNYI4G2omh/Abtd3FGDa7rb3N2OUDEdYg/RCYLkzusw/e8nVEoj3j00XTzjelCStGaH3
BqyY7NTMQlSv+PINR04XuKD96Z+okbWbR7/lEsgKh45fg2a2Idm4mL2c6Q0QJD/F87qYeF+fK8Tg
Su5QmnOCfhvq8wZy/fAlzSmy/PlDMD4Tnli+hbYe9dt6fAiZel1GR5ApIdDJXcbI5p9J0MqFds18
Cxea6R84xO9IughJnywBJgNl26Ld6gsDMsRTwJwMdfa2LVYqIV44CSpYYUAflCqkO5xtpjfQmlrr
NPlusNjyJp/JyqJbDxvvHGbj1yewi37kivf50xUAkWD4bujeNm9yUPyHcUp7aStSn1LBIYIeoOgQ
osGF6Ypx55nqfTAwQgStrXpRpkP+IUmSIpdeNKcE/DXJiLf+w8HQz/Bscz3sS/oLVkJvD+FE8gXQ
iyQZ0gRHNy3jvg2w9HPQli7FRJVwRV3Iw3amyXh6aI4ZlPGwenjUsDQIGtfy/UDd40iFJ1h2BZOQ
lciXj4UOWR+pBTN1sJ4jvsq1tUJZuWNv6qUiYJvyp9SL+figMEX9OOph77RXsUrg798wL7HWmks4
irm4E5z03/fchilbuf4lu4yl8KW9Nq+jkDRqpa5/hFYlJtwPp356v9QcWKoltDoEzHHykslYrGPs
E/bOlHGLJPeWxIAhi/YNTdww8CHy+nUmcBNciNB5XgMp+bpjA0aXbn8sNm/rwJeeLNmX13iR1iU7
ilxgl7M+pf42TRNC5vz5EKOQz6FiFx0GSA2dQsCbPTBPWaFp49GPdNE1M/kC67nySIKXYwU7Dfc9
s3gEhdih4GPoUtuerVNirjn9rOTalycPlzfH4jYpjnJ6r5PYuyY4NBkFH3Z1vinh7NbuZ5xYs+01
NsjUW06JDT58j29MZ6h5zcJ7hnu4bgPyyYyx9HNKxWrlM8Vlyt1vuZtm1Y8sEvzYAbU5KDL16/9U
tN8sn9OYVQD2gDJosJL+41bzZiIHdFECX74ZgS835hJ+XVeMuVmb31R1t3TbccCIfrfh4Bl2Dhob
oRiLYmFIqtRTABVY//+9Zgqa0mTEnM9jNu603+0ORIl/DnlscFx6R9f0jIrP33UzRHWrJuhQ1IB1
gqI59ELkii6auiGRxatS/J5hku9WhdnQtKF6sbPiJs1SllB6ekudRc6Zq2pPSnBaqnzm2QU+dY9Y
IxAL93oYbWS7Sfz/uL8yvzpbktBKiZg/u6dii1J8N0XMBvnYAWAeBS5rM+jY183YzhbdQHIaFXmD
PwM1vEtOp1z2DuyTXdxwsRfHYE2pLWVDEaLBH2ap5eha7XOprtBsUcktD6Wmd9qmQGxft5XepPB2
uK7D5w/WJP5/T4ow3kqvfkn1Yi2TmTIQrEIQBzy6ci/05+VqDKcfLsgkxtfFEt6cE2McUghY0e/A
2BWE1Q/hNZMMhA3XGsG635OIDOVjlGacElUareWyV7kfThC6+8mVCRecBmfeTXSIPHvt4he4yT4S
JJAlrQe+YUYEnBBJjKZf0TeLAoF4t1vO3/ySsZVWOhPQtEh0UWe4/CYfcpDTUwdTDdNKif2qTxly
VIIpP0idoCLMBx5E//O/9ZmbnRpqdtNRSh5w2UUdkfy67cehnxQyfPoQkDVyBGIrTFYbXU1+Za+P
nR5t6Jko7hs1hLIbEUkZrWGw6USnvK5ymRnYZAY5pZgJg56TujzwQxs/o5bSv0NMKFUF1gCcvlcn
dHtHuBSzXr51Eq4qxm84FEebGkPUVYwjW1GYFg3MAAN/t9OSeSpRSN42MvBEfsdq0fir1a5KDv2d
bEfD9taK0RFGMwLwgLnQIc5fvw5AGDiddwHwgEP0pchE689BltNH/l7sasLz67Rs/VNdOidhff0r
0gQkycCGg7RMB+dh/+h6HN1+/PzEmGEBHZMvne92IRKeTFA7d2N1KvVO9TyElsq6xfOgJ6E7RH1V
ykJFfMS+U8yx6kpHOk1yAayTQQgJwsYX32H8f4ePksDbWP30DknhP/zVcTKdI2os+9dQ7zYXRaZy
7P42+PnXY+f4Kaya0IDeR0HHhrBUUCYIjqd/0/CMJ/rTPvDkG/Gi5HH0XIjhisflGSOX93w1gqbt
nH8r9CYnFR7OOLz1sztET/0wdHNKT8LOFlETV9QbWBrw9DUn9waIliVR8tq49oLPkFyDiepPB7QS
mTZalhiu3LuerxYQwCdAipMqUuOaLnptKqHXU9lLxvh+aG1IvllvS2EslcBgdbySb+9weuvF3FH1
J0EYhQvOP5qzD0X53EoQllbpmsWgu19lpSMV9z9VS/KE4rfieNloSR/U8+U430IrII6RbiBGGRTG
y0tFOB35YH5B4UASrq90K27rcnu6tbePseD+8fZ7VMLqk/ecMpQo8By7fu07cb52hC1INWgjrGcG
0lLOIDWnersW5TsVKyMK27dm7NWju2OOhNRtgSl/XMTBzhkJ2vd0shCimZ/XacnQclto/BcXKWbF
W1LMVaR4upqqW050hNJpTc7j0RliPgpFHwMFM/xpocJ8Nwh/iCq53FY2W85aVswVgr2wxE1ILrpa
uKoUb2VIkgvMLgq0Gz6lTUipvYE1rCW0pxVrrX8z6ORWs5WJHwKIJRtn2kaZQANM40a3mK7czX3c
Z+RWeF9ihA2RH/ynFYlf9Y2j8qac+1gTStyXAZyXl2so31Abx+b9UlxYcx8Nxzr3jbnfFbmZiQht
GKomyv5tIKBRj5VtkBgAV3Afte+wni+gEQ2UWtup5F8pQERt3puNcAi0MUdpmXsqScNMzFB5FSd+
0FLC30Yw+u0hTqbxmcpVzyy9h3fHaUDS1VWdJsJ0+p8jxnuVtv3VW5JzknNKU52GRy7pQhNT1xGm
1T/1ryHsf0LZN4rxd1hTwFtsePEo6jx4UOONC+WooXdZoR1fL3EWw5XMQW7Ct9iRQURxUUqQtZRp
r59uPsv/2cgG9vYOT/TvQsn5DFWlJam4G+qQkkUtNtIURKMvJJz5v9z2SLkTZX+emzxkkPR17o5H
4Q0VGEwOb6G4n6RrXXF7fXumj/xK3SWSqArU7Ve2ayM552oPiiZlSXl5vzdi2jgxfW4g2/Qlaijw
nKbSa+0oEFYuiEbqj2OSeI885JiVdyxQQ2JF+0gZQEiAddSW1h+fcaf6PoJ3y/wNum7EtIsMJeU3
WiRlgOhVG7PchTZ0lu4dvnNU1FGGtB3/jSPDh4hTUGELf7RO/hRHvMpgoVnHKMNXrGwx+K22gz/X
yUECbA5g/+5Vdy3NFngEmpMI9hcr0K9K+vf4Zf7Qi+PcGuYiQJJ9JQhxWEsc0ByQSiy33h4tBoMJ
imdSpNLe/4ud6rgFbfN1VhuWfCrh6C9m3cTGXGKKPM7hDVKJnR09ABXlDSN+YTq1KY14sTsJ1JbF
kZt84Vtb7zgpNs5mTXmW1lguM8xa5m2UNW2l+WyRkVxj2v6se8udBeCOC67BdgAruwdUcFjVTssf
CpWlXG8rcUxZux+++qiImpoWO1bFHAM/zzMSCHGybxeWu2qSusqj8GfpGmF6S0u7bXGZ+YD5tdBX
JSLsvO9EABiESEhoPTtm22snhWFIKbvyq65J/Z80ShuxZPxxw7OsKugO+r+dDFQN5GXLlVyvWiCW
2KYjd8G6zPKeBM19iINi6EEYCUg7iih4NI26BytAUYG+hwIKb9SNySt8T1qaiHkftdiJ3rSx8ruK
6/R/fJVSwC8UU1zl+MZyqpc4iMUyKGIRkhpb2/v7kmJdA1+HwkaTULA/yefOQcbpV494RownUfZ7
iGtcambBSBpTHxy7FPUS4RZQvNyGL7upsto+2gkfvsOUV34IfQMo1BvISNav0xV6I3WSLEiJeiO9
JnBwINbFXHmQ5FMvGg327UbzdR0W63sQM6U3GNenhF8VHp8M4cgdA3E+xGO+RiII9jpDmT1Kpm7M
JMhmNJ1fNGT3bzWB/B0A3QHnDUOVyKkuaanKySIRm5U/qXAwpyJknIrFimwqydILn30KG8bIGFNx
xK6m0R4xAQ4g4jsRWZhrAWZ6Joh2TqJfh8fYEtEBcySRxMLh+futfyWFkZJ3y55kiAOKrzzchlRj
ME6c4+KsYnISq2ad8P2bqO/pusNvI+IhFyY97kKD03ktStlM+6ALpFYMz6VVaq4LJxnaKwo+yn/X
aXIV9uVTxgsrFZ5kb8VR4H3RXzzjG+74L50L+Jcdpq9z2kjpCqw67X70vDCVvR7dgpUZV+rlzslT
Ry8xpkleAOL/u8BPnDjrqxuHUb0PzfLmvslT0LxIalVBWFuzuQO1AEdw3V+hOnkNrkwZTftf2sWD
T1Zuq+3eS/xvZ9E1WbrV7yBK6xszgVHB1G9JKaP2UUekZas6sLE+/NXqez8OLx9waz4p9Q4Dega/
PdCt7IN73b+gei97Wl1606s42WldnEKaVRyqKsn/fgHjBfs7q8r9i0eKu1CZxAhSk8QQrYF48rQ1
iI4hLySsgSgyO7LFJmdPqr6C1jKHIvTNhtT4a8N/0han6duJv48XYKzFHi9gzAje3kAce9zvnZAs
pwwK7d9i1SuhUk4K3/GjU0rwCLoHn24v/h5YhQ73N5FwZHofvHy5byHV/RwtWsLlLOoX100mkEpB
1vLl2FdCgjN8taKzlvWgz/9GtNjyRMG6ybe12XmMGtCdrChKdDKIieqVjebQrvpc/ZVU4VDVDDpf
BfGVzXzzpTFkuLMeSrZaZ8S07PxLYwlpxvaIiLS95eK7TDPAvlIc0WmTpa9u1hZ/LfuzRZfVpBvU
aiv5Qzk707cIgTJ9Z6uC6mzgug8hKFz/WPqKeR05yXu99vcN6qdUzhPde64tjD8FzILBp9ry2VHt
PZt3vFKKW3/UcPQVFe7ymbu68qoavLhubEUx2QrON/IEULvFbldZmuqvVnLFdQPQuc6XYCqupL7Q
iCS2kqW/9eIHpWiq2l6o5QnazAjDUC9po60bZ8adkNxTzu9+uvXGH4LMswawmHtuepdY06u6LhIf
QLsOYvIK57DevzU/+6VApDTjdjPd96FVd9jP+qjsapD9dU3UJxuoNFTzdLqr4Yt2ugtyctK8Ajnq
GHafuRRxbrgq9V2NV1iyhlF3Q5WDEuZAy0S7lw4ng4nYYNX7PA0rmTO2S6hrbLc0ZuNd28pES0uY
kMschgaI5//4gPht5VxQ9d67LsMcbjrEiOMwom5EZZzHunA7bk4zQIMy66vgkDJG7EucUSDBAtm/
+GXpYh9ZF7b5qhPlQGrhBlMzT75NRM7ctun59EmbmUv6xnAfqKOeefdSpPVsGsNb9pTOqUR6HKTj
a0x9TgpGmk9Ynkj9HpbQKWNsZDsYlRDHwXpBGmBCzRMqIhmY3mkXNmZeaAXHsmCpmPCbWJJFrItV
A5sp2uh9xVtKEleKO9/PNuDdcLbdPiZ7gYiRLDb855rXs+0+g0Z013R/VMRSO+9UM7ZJSehoblJB
+mov00aVrgMHtfIwQM1H/RpcEATtbSnuRPLTc85hKROaipgOk0T1rao0ppR3ugNtbvxEWvypzf9u
lGmbIfYVfAxPx4gC1P8gBHqY+3EagJwar9zk/4q9oPNh6EUMGhACduK1/2upQrxZBn4DNLC/dlyH
w1R/bsgx9PRllF6l09vQiFVcNS8PmifqEc6bT50cYXATHH+pCwNRE+H0tCFMBQMAJezOgVOcqKQm
63HVDSZ2szFBAdzXSyHrfS9bilIWx+BtQ0f8cHWFsXUhRzYhBiqOqzhECI1JAqK7a6RLajxq/Yql
O9cJRB4dltEqfzvNWO/IHL3eDymcyv7Te0xBGxT8lTLJp267RXSyr8PKfwtWVqUKO/Xgdk+hkcpr
22+L6jzQSPVYaVKPzs1V/Tsp7ztD7JFhAGMNB+Jr4Hec9RcFOUsIY8pctn+Yg3Nk/y9e7KyIQpEb
7vI1izMcRj0s3jahTsv8eTTc1hF9lMY3hEU1aMDeK3pAojyIfpxThRHfECjhF0W0z5N/XvZlJOx0
XjvvwsPAyp+e4ftwmeGKS6UrBO8fObcT0ZMRpORMufRtnR7axSRqqVyuvBDtnptAHGfW7G/ot0hq
lbTJoVvu8ZL9MwNkGjcxhHp9hWttEMYL/lyE6nHIjhzffA7GMxB9A+tmb1d6Kr51rlr0S4vEBn53
qTzmljwyxcO7cUqr5i0mSsPAImIOFs/6C95Piq2+OMI5v85fMOiB6hD7tbTE6Y+Iq4ss8QHXfbM6
Jt1k82OxvQg7AfqTOttWFPKwwUuFF988ECxGLtQ09FLuQ7FkETDjfqPzSe4Dh3Sna67HPEyvsAr9
oYFAY/sEs5aihBrOyPoXKugfST8qljEzsXXc6cWRZcjcn706zoTHA7sQNGfLntlZgJQGbNReZt+e
ODW0eRskSwCSamZQJGLhc5aywNikL6Wwkrg1k16jemHH+AozUtUeCrvwlHtKd1VxZWuk8o8/vvQT
JLw1p5cGKhSceOUMzpOsXiLV8LbdN8X9EixCazl42nZh7UWzjgck8h3YPpM7/U0Q7vNotSMc18Su
hYJV7AcrKVUD5XQesWxUtYMdWYB/Pyy/3K6/SFZ1WZR0FpEdl6LuZ/TcMe05mWqCZuBsWf0YSclm
ki+vTmeLAaXitir1JqpgDOtStqhDjhODkzd76+5iGDoIyY6FaEjBeF/u3HtfZQDQsDgCd1eyaUNc
NEc2itXheNVUZu9xv9p/DS06gsxDHrwkcHqfcN/zuzHLOHwf2zOpWfBiXR5PFxQJLvcNYBpRTAiZ
ynFWm32hcsBaCcPjdP7oHels2xTWcbzV3d0Ya3/rJ0ag6FOweaAPsUfVgw+FHH/6hnv/Okosxg9u
m3XSFHKAAf7pyBg9Zo7gi0IWgbSxJ5H1OOLnb/x2xBaUBe3x3vS4kZa0rp6Dx1Bnp8R2h+PGCnhe
97oPOaUmO4HNhy5g6jy5XGkJtA78UqPOw1/QjlT92xhofG5EkpziL3LKhSw7iEDN426UM16m5WkA
vF468DMU5rQA2jU9lcfv0W1oKQBeY+S3EQFRw7xhmWvVm1sz4qs/tb3QZzT2H/CdKS3RhjYeSrjr
VGHtCvyP7q8GEv1CuquwJTrKQmvO16IlfxMyQCcnbMFL2v4ARtH/ECtJFJRkrQdarRyLEXikJ1jI
CF/v658wmYojCfGbs/iKrYmfa4+zi1Zrc0K5tWYwPCdHpO3N8bhojSefK1ywap3dhW2X3XrwNPti
+JBjKw6BxV9KI9A4OWzKtAjpyYeG7m/sOGQzIixwhmdneOx+h+m1sJBFD04E/We+RcvJ+9ivEwNM
b0GUpjsG5FbOjOkVZH0jjbL2q+Mh1BqW3i97lLe5hWBYNp//1rwZv0U+0Pa0M/oC3HnQ1S0W378w
SRPGNoxM33JBD9HHIYINWj4rbHac8MJtvE1AK4ck80EBkuImkK8Lx1Z2fUeOR6m4we9BSjB0gtCz
j019DyqJPhZQqNNUIaFcqF22tZs1YzvDFz9aed8ed1RkY1zaFYzqXJitI9cs1q5AzIpT0J8JepiY
ZxcZi3+ohtk9D1xyd/+jW/pvARXLhktlVjwBwdnpRwXetblyQtivWOfWO4HYg5H/8jMS5wTk9pTY
RD0EE6Y1enUYEle2w7/bvBfgyjFpBzoEceyQR5Tcoh3HM19DBVgBwufm0HyQqq8cg0TRnaqBOwy4
pcGo0DID6JW9Zl0eQ0S6Rh+GgcncGINZ/XmsUptzqNZGnIAWjqmas+aliDMXbYVQITsntt2ZI/Xp
3e2JqBXZ3NxLzKfdhFBN5ucJYwa2juPHcnX6qYZ2PS0dBEbjV6m298gsrow+reglYel+dlyExos6
q64ZFfPfZ95JyYdaDl/0G99/LcCpTTzEhfbaBTt0U5NGu2MahB9f7+nCwTrzWRpI90OH3qLfYDON
pHYu5vzEqqAt17fRVbTH17nREViDRmlBDVGG991M0gyipkYX468VBIktuTh12ZQU4pqlG+Tqdd7z
T3DP8VepvwOKC+PfWFmRtwi1gbTbTNno66FC4bgfYyHzg5VJkb/49pcYJSRuV43nNY9Kah5Q1JYX
C/mw3p/9leEc/aqKC54uCnmGHyh/Oz1ihB8HeMsh32frtlsH4+uGSmbZWnT835yYZ5l1cGfSEOIJ
EZXzavHA15NHzcRGYHfnjHsVKnb+HlVS9Fj0DaQ7ocGTpD9B7hybO1tBel0T7T8OX6FiLFyNn3Oy
EpE+XCvOCBdDdteQT8zjcyeUfMigjQscQnrAY9STaR0+Xvz4u1iA7pRo9sL+/RK/jWD1yeDCXr1N
Jq2Nb5ldqFuPqS9torma2PbREVhT27bVg/P3wegfMIu5LBKgtzSwBKHUjv8V5dHWhKN5bF9Nhjbh
jV6iMlkm19luZqGOuhrEXfEPA98umR0XCiaHthJyz/wqz16YExRiUMqdUpnUv6RFyqpll0FdCX+Q
fULKTlQQbIAUYATrUXsIv5MF6G9F4rWdBDNShPuxAMRfEsNpUZivm5wSQkU7Mfeyn69ndEurKnlA
Uqg2aZgC9ncf1Oum8Qbn6MfxjyiWlalzROGZyHr8r/VoikcKYkreHj0MMuM1FYnqWCTJ4qQY0OiM
iRWTTbhdlBFG8ewsYwJsGPzZqGXKSAlvGOGgbkbFNo2MfsykOjGETPE1h22iR27bjPqNY1H789ah
EZhpUH5NWCRQHpx3DpPh73GfCeJ7ZbRqzEXD6TmREO2AtAj1v5PEdNAGemHwWGKM/VIap+o7UKvs
690uQSw7dA3Aaru67bGLnIbQ8i0SYdQl0hVE6Gg0ESKXoGMOgTuxP/55BHvwco6Qb4FaCwfA4CYZ
GHvpeEJ7Zct8NQNEqjR5gMZ1y4cWPDvUtraiCjDMEInr7dKMyRJVNoC2/pMk7qaYMPjxunuYdcoT
uoLFBViKlti71de+tlUQSV2aHEc1Z8oyQoiLYqLD9s1JYyjsn6nZLunf8RsJ38G8Fce36naaO5z/
dKgntv2/TRHhESEyrCfZGlZAcBgTB0ER0XX8UnZVCYKT8qic+bh2/d1aByVbG+VaAWWmro2koxdh
cuuiSPx7IcJJBlG/4U41n9wVmsDI7q/3WnETb74jDyMVZE//5mnFSX4nKo9mAeialKNtUtmLkob4
J+4CjpfR47vnDwLwzixtW1YmB2DtSuD4HIUhME3iRtBu6ZhG8LuQqPRJy5+u964jYupoWP4/ZHCN
zEtTNYbrlY+DI5dN5LAs66VHbgki343YqUxyxoLaHpDiM3El/7f5COiRLpEkTYqZBXbSegvHz1w7
7EvAY5lMtwfuN/lO+KfmjvX5uJLbBTjCZ75sFQn4Sg7VJmNYUpqQLj/O9rVZs32gnz0N+hKbCmdv
EOCZVpdEIyMSRyXI+KbN1YtHsQlL6U5imAZQfHyRUwiX9tWLjDKdfaBbGP/tMEmYD/3q3YmyZF2c
CxEMk92WH2OJ/+klP0OJuebFWdzm+Tc5lE7Fmeb/eps+XVckOMkPAwcIFIEIjneXZ1CIv8QUAUYD
G/XHDpZexa6YeEM1ASu6giZKhHJ7w2ximf4Dil+sG4QFlkwrlDO22ulzVUh+OCeBIZQXaVpH5TH4
YjN9C9OL7FPNheGrDEhEoqwaf7CGbmbdaydX19kGNFzSoeBMNhRA3rE+ylq3klCRl/mRqN0pkwCX
bv5TtcUZwwgI0pDl4uZiiBaS59alCYNMQ4tKOShIARU3f7MQmdtrYqQ5bQliIHN/eKJz1xbPeVKR
q5SPfskmR4zl02I96T7aRPwwkpF8d7XQguZ7CErksT53xQmU1oezlVM5IEsBW5DPi8GwwpkaA6/L
XVuvJAUFqs7xASolTGCJojVR8fCiOauDCmaQscO5JaY6aadTs1BvoN/zjrYJlzu+JgK4zSQEETPl
jemjb8n0aaDRP32R8iu1T0oYER1L8KST9GjS6/DE3RH7WG9h9J85UqCj+FD30qi+30SmCmuG4rZ9
GnJ4h9fbGhe5nOGEAtfgc5PClPKLPHF+yIJ67OnVKjH5v9kULStGwa4puotsevWcQ6SWHmGR6GtN
4s//r0OcPqkpAsVkS6CCMCEgb8mn58HHxBS1YzwInU6ez1jH5xMy9yewLI5EKR9jan+b934o7OF8
qIXrpovxnCol+yxH8FQQYZw72aTuKfM8mDgfrz8CwZQkdA9h0ZX0Yqkv7uVIAh9uwIbuy0tgEj8k
HJ30WA//26Alpm12DH1IIiOLDwR7o3zqPIam9J2egUzQK8Vjd8fGsnnX4jlDoKvxau7hqomKpZYO
gGl4BUoCvHSpNgBxuIqSwAz9zVY46VEyvJtDUpPvtqU47vZMnHH7RKbW1Ej4vXhGYBBS7D9S4T8E
RA4QrBZy1WGen06WVpLjvJ+9kIDBP8K46yNrlnQ9lU2NTSgBDhnV2A+S3NQgRLWfaR5dPtiN9suR
5IMqBPY9xXFHsNCzFrhmRZOKwlO5NGa8pZR8TC7D0WV5NiHcCyfjMXBqGpyhWi+TN0NMQyUpyAys
raAaPVWscypgdB6r9wb1MIouuHO5DTogDcVJ2P7f4FEVeKM1YAH4MrC+IWvoSJisq2W1zP7T8G/u
9gAgPgL/6xtqRmaAqVVSStv+/5zq0DRQ7m06sTlsil7j+fviWeZJ6CWslrTG94TXra/KitVtgqrZ
p7abAU/O3V/xXVIl4aVMLArYTVFRxfJd06AeN/brzip+NKDyRu3un2EEAb+0jtarbQObAmEMC1zs
c+8V9nfhhegXmIPP3Crf0LwC6Gz6BRfmIXhavGH3kwqYqwuMpDBEyxV+hkKR8YAQVvneCeF6Ikta
Z58NtHqRAU1Gmkj1Oi/qLyQ6241Wnb0TiFlRvEqWEMCkxUfTxXeCr97feq8DR7m1uRIL/BPXawJZ
ATKzO44aCUoDvT0V8xJcXUjronA8HhiS4vYYKmmgybokdkoU463tfF0Xqgj4cSW812+m4TOGiFG6
otSaHCpvbDY+OWtExXLsTgxtx8N+eQelPl0GWnXcniamb5I+qNav1KdVhFDpTMJETcVwQT53q0No
y3lQtgbJviGLCQTNaI8fjo7V3KDWh+ov0eybLU0e4mA9SzHtt2KfT+e5U/9oWMuSmAgeVqGk9df5
UCpINcHVgBKFKbR8IrvffYTJUTaQUaXBFQnShLObe7D6TIPC9tfz1o5MiRYq33jz0e1KxmakQbJK
fvBy2q8xHq5Ye+89RjRRXtHyqt3bZm28fj8IZAQDINAhNg7MF3cG1x9UIUSNjREABLhdIDvpgHE2
ESvaeAhz4IolKCjFvw6jIF/+8c+lnqcLiNf52pBeY6aF17UGr2JR/YtK7iDmW04r7B8/gSk8s/7h
kyf5EVWCpmWrD2kNjIlGGVHIb69b+9tphmr1tUUPzEK2IgML6XlwjWRC+lNZukYvL2/3Np4pXBOX
EK+LFYDlyegpiW+rJBRXocfcgAjjfMF11C74brBSm4VkQZQ2TeWffXRECmCvoF7af398I/WrxLKT
vJTOdfKtdvVZHKmwAucQmDbWdMD7W8qqFhbQTE7ndDsorGC1Sx6A2wsXOALPUa+iC0U8iFgcuzlw
1mf63WyTQgJGMYkaIggHIPWlZJK+qXIhdawiC+OsnRiadt1j41dGsIzsQOJqe5xRJqylaTGO58FL
WGE6FVovFOLb4PQSLJXoBNf3vNTiZTa/h1/Kn9dEoqQJAUGNY7vt9r+Mum3rTVvx+aksPHXFmSBC
U/wFxcWn93zjB1/WLbXN8QArlthDcUkElVoO8QaESmfERPts2xJo6GShdnh4pe11g3JUDUdAVMtQ
ZV+crz2XiFdGJ+T3ge8TnhdZlu0JEkBh+DCwXFZzackcLmn2KIp967eN/tqQt/rSyM67iJhxL74K
+C22cfJ3eXgvumWDz76VXz/xquBUWULDOK5ZqegcIn/TZclqDo5MbIN9fnK9XVf/tscfrWc3PabE
CTGPZRm0wqbweJwhQzXsKjqalLX9vWVdwSLVsfZ4IXilxSReaepdO8/ofr/lULyQM7hxJapc0o+B
WL9QeAGYkg3Yx8lI7FvYVT6YopMKsfkN3U4bDYFTtFb3yqxwqXVNt7aBgxSOnNMxW8AG/CT49hvq
KWfjlb3WqWhjhYvklr+K+4nKiieQ7KH6c11U/1SXYt+7siwatBIEyp+K5Lca/D7xBm2adKQ6JyOB
1xLxQHxpKuC+mQYPhar2rNZXmwsevImeS+0O+vrrhojooQ3fg/rXY046QheN0GM7f3vIpfWxO7FU
uICOcqILziJRMWffndczTOxWxvdd1ja7yXF6WyAwNAmFgVpj9O6R688tvPqEIFTcVs1iUDhBpaKk
BVOz/oFgbxfAd4rHIS5hdXJ7kYVSYY84f9mQ51A5LViOw6kll7qDlyvkCZgbWt0bfVAn6UcIoQjQ
rjf7l3/BEXBvARVGkUObxOHrAVB1AHTthTSfQTq9NxByOFc5g0sIaOXoAELLPnRvkjVnOaJPIaVG
TVCHS8B6khrGRrzU+af3vFMWNGGrAtH7C70KYWE8/5eQ2bE1XFvNr69V2H8ix0R93sqano2nHIjN
xc2YP7EEp5oydFjaEtN6Q0g7Q/1/JxZT6eMHp983irygtgESW1EljqkqJjJMI/xhdZg5V/tDNYnk
w/DnEhQR2a9rMSuKz7d0o97PvSBIuR0hrHZb9snA5qyhD1syh3P+mKgHj1vvrhCyp5ZGQWW7H/Sq
BGVq0JSvp5ytX6Mml0dqqbKvEaYi9dDrM00pUXTCvvpsG88yAobemixw4BM/3kXnUqrIYu0xoPQq
tKAN0t7hNV95Zu2tc9G6mwk1y2xmB/S21OeI3RdXAjs5WVYh/lOvyaUiJkYOypsiUKhKxwXB2RYL
Os1BX6byj5NoKxtC7kDGH4/6DK2CcNRQyyUMreAaWNMUyhftK1BZOVEc0x2u9+wq/CNCkwVoILR4
Y0ACm/CA9lfVuFWaMgHSyub5/oTrC5oxsx+NacWt9LRmsR4fKToGTFuYLlW/WfK7NeMN1IS2Zz8u
P12cGRyywP2KBVqoHW5lW+IJ9hWep+zBEzuB9tebMljbGuEIWCbw9PD05S5wAPZNN5orGS911/Kk
EXQ7GSNxRTjyMs13J7BUz5Yhv2w8r1LWjOQw1S0iVIGM3BuGb6GoWJoYby2RXhTVrI6gGsSPpz8g
JSVvN4HtTLI8ol4wdwVEv2k8e3qzsi70CLgGj8b1ASKO8GPZhWGCMWIlmSnHVl1pQxsFKXDyXMrg
CsTzbcIeQFZcCXg8vv0RJy9ybDuZ9qpgCrCZZD9RCjJc1uamQkJ3Kp179DGI9oktlWZ+avIBg/jq
nkkJICNBxkXCcxLEOI+srf6gOYATsj3pPwtv+uZY+SYbuYnd9kPyHHS3Nv3Gki5sEUBRjwgFHq+n
puzmpNslvujPB4oixRDTYqGSBACG+xIrjTX4rndDnj0nHo7JO86JZiouALRWWtsaGpdD0ZlXNX4q
JrqEaSnXq0oUqXfnRWjYFM5bNGNvvx3sr/mcEukiT5V82sKY/F7KDmjsMvZsLKyl/x7a/aQZVpml
lm69BFZ2BN4I8mOutSd6FC2q9CQCBxEevn9pO1CG750YrH5v0z/wIS6Zo4Ll268pRknPgABKGrJo
U7FvSmjLnUeImFuRL5gKhs8FEBak2U1RjWljAUv/YjfsCNAqyp5m5GHnxFgrJg3v79+tr4iK2tXs
dsKj8Vi6871kGjAJyY04N0h0jkoRdeN8ohelNgGd6dDHHJrdCBZcD2dpBFPgi49czNyBYkzoPm9I
PK9bejv3kMZCxQW3qGVSVEexSjiOa5SXCMPBXL7m/LaBUFgKW1j0pUOrDH03AxA4bljD08vohWBS
dJi3HdvVsItBWaqALhYQr+UzmNEqgt9eu4HBlCFLZgyRdq4TeyS8JyMe87k20ruIbD9118Ps2k3c
eycZO8jGhVK6xYZgQVBfPB6GrTqMpUtJTQPItk1TvPPKTslnhDhcU12mLmu8seaps6StPYMKn9SN
ruIRqQHxZYcLMMIwTMUG+o63m4liuX2AM5cMTmMVSIj7WElRr67rah/Kh7a7Pg4R+0B6miOd9rSH
dOFy93hutHe3vB/ttjSb3woA2lnsWCxyK9C9XEH0BZzlmbo1S3EZuADP1VGGQ8FDwHz47gE0safs
2bKjXbxXhNA03iLLXoQD8nd4uyFbbzdUEy/OT7uI3g8Xc9vOLXK9FOKH+fNGL5OZ9s4fBDGYFbO0
nv6RY7aEqpfLjex/XJv4I705JAbi6kMguNJFEbSkdpsEl/d8ggpSNVEVkmEahZ6tvF8HZyz45ONZ
O3j5eEm1R+ubIl4pJHtcrcDgfJHgbkvIr6mtSXeRo2ylNyiwRCjbN+r/+9OwLkTAHesyZPi7Cbfc
zBTLRDW7Ont4gUZ2pEcGQ66FAojCc7YGxIALFnH/uEwVnL7vadmUbdk5VQZUIlSTUJGOUs+TFU+u
a1kVNWhoHdyKdShBPxlZVEH7QOsLNjf3PFLlqYaxkIlcZfWmS9AEXFOQZaCiCPQZ1tA4/cQgHzRK
dORNNAcahnYEcWI0qaSlMkkrukZqFfkIssosAoIlT3+jaP7c8hOusUJNFWUh6NZV2g53BiWgAPzc
SaETlzc76tKttdlAHwAcKGZ4W5DWw+rgms4MmXXqekkzTOwz/6Z3lh3sQ9Y6Y6rehLFsjh78nCa/
Bq8CR4/JqGnCKfYFd1nB8w7AN3dp0Kyd4pLMqeusi0WXg801dXhao7P0RVbH+mqWpfxZM5ufNbp9
/hhZbDxiwwC771lUeRXjl9+oxgn4ZgNcgt52yQ6tX1TWl+g+UgOXoouv78vlqd/eBztWs8w+F1qL
6qGgcLzrfxpUsf3Zv8mbY6g4ur73qe3T4oau6XKvJUqJftfD3/DGlmqgDuAmFJijva8rbgRGtq0C
O1be3eeSSg2HfK91MOReX1tUtou98j1MVyJz1oWZ7+gfoen5isSX2LmOq6PH2NIhZ/vUrLJ+LAco
3QQTli2g4eZYNU+JtsyL3muOc0RWJIQgYkP15D9a8T1zxuTtCqE1ZpMpwnIdzUKRtyEMj2I8/6fD
nhxfBOa0aDfoGB2GLKGcdc05F+CyiN7zJZj0Ho37iF/xXYdtZuXCfvT/Qv14yyWVd7sqtwiCwghr
PM5LWiPxoMjv8qDtOnlvwdYT42GDhVWyoi3/DRVrQ77kJpAdL8mVNYdP5/KYBAQj5rnrD0ppmjnk
xZptloA0z42uF6JMxL0vkGTnyZW6NZH/AV2bADcmfehfu+k7xQmVevt8ewbpk+fxZpKVABg9dQdL
qJyQGzTHkrO9E5dtc4azQqCpoOMrBtu/M0899in7P1fqvUaVOsIJkZyrx3RVTGOKTDoMAlRRHjLz
BMU8KWcU06vspkGoJwBgEi3WTuJDhhgIIkwZk6leCRVGGLnvcjvv/wTIOU3gf/pdT/m+RVqxmRZe
Az7lOFhauFQSI69ps6jTqs/DULf2JlJakTwAt1RKrsDfGgM7j67BOJkv8KPDZGXtLJcCYgfIArjq
/PJ9rrNeIeNJsAMrMwAQm5JJHrwb7JUILBeyx8Fgz5g+5Nu/RPJM2F2TEVjTE9P4PlNeb6r3ZWZl
LS5La9VEUe7/XKPfEx8REZwhFPZoBnYvQHPaekNMuJ/D34aD3UR8YaP9oAIXmyVPN9V+g03yYddm
5HqmmfixWsrK7cvfiGq9ckiQ7qNp10fwIDs7B5P3nPkdk+GlaMN1Z1x2mZ9JR2MOvHhYNUJRh/1V
BpUnSRS3NJPnmh3e2FFkmM3cbe1UJqzcju+12NkIwoeipyWLTdLUcSm01J8BVdBIEw+nDqCyMTQ8
0Y7CA95PHcYeHY7z5P3yAUx4JYz7uYk4ETFmDb83pZLlL2Ls0V9WmHjq7vKWf/8uY+AAzcM69dTa
zJ05qvdlb3YvxSCzzfLgDScexGDlnSHt+vWUuugehY0kF5B6yjVJB0E2o8x0XJkLLwVAJ9aa2c0s
mnIBjDNGKJAgdg7svScoQsTc2uxURaaZuXFqUXh1BZBzzEB4CG6hLYXHuJHBR+o4N6oaMiNFm9TF
EKynNseZLXLZLnfjYzGRhINHuk6+kZ1nbTT/a06HPtrRGA5Jio3GNk23/+q21xL8SawbsYZrRsEG
mxHAMDD2gtSA+R+q/zlAuOtZA2FZjLDO7yageAu4TjQHtYfB9OJwZaBdukv08FlBAlB5crRvGTdU
cdR9Q2NBL2Ue0zPSOQuY0/geravm6NFW9VLxSccMGESwYNKWwMJnVs7kKRVKh2qBaHafjxtYBbZ1
vXB9Hm3tvXnbe6y41bJoda0hBqZNDvAiZrLQ+FIS0nvDCf0P6fYy2vMhMygv4OG8aHuLRgsd8DAE
O7ZP3dkzwU8HIvLsV7J75dwotcZJBe1LNoSeqdfOQPdY8xVQguPtimz8YZnYtXDe92Llt+UeUPqd
mlBguCCCBEVUoVZIGvDsXHfc6YFd+wiIPigUdS/k9w94nkb4QI2PsKCRiCFiSZh05XFdJzqp7GYB
Lkg3+uTuiOC/L62OoVEJnTR8b/PAfx6yxCWqgYvtJmJkth4HJA+vehNuZ2lsf5i70JXItmeDTLGn
O28DuP4+VAjlXW6igSbtmNwsFKplzDWbPvp20KtszFUdh9KLDvwUxsfZwRSYFkat9ubPebHslZ7D
7AAs4bTMT71HvLL2ol2/9Oa+eXmAyLuh6B5QpHqvPgNb/Usl5DMV7CIwgpsQppHdSNNpMBFW7M61
EglBrDKvVmMExVdFzvbPIcbTzRMfva4dA4338DwkZooACVHQEc10GMf7uf6MrDVe5Wj+5FOMMs3j
QjqFH/WTYzm349Ru7n8NryB/YRicG3lFC26Tc4r3dBnCvKQ2LqrDNRDgrpyhJdb1IytyLy/strZ/
oKZM78sUUprFe6g/pQYYw+XiNwcl0rmhmFF3T2YFwjCX6R30ZhVdbjIcJT4e3/CjWzWKUp8+3DFK
1PG9HgthVcLO2xw2MAC+z3fnXjuMn6yE+Q1UJcRDxN5t+A/ZePuFlHJH9cjMUebDm/bd3HMEPf2e
HtbP7Ys6K6e42JwGisP+LRoadqkIq/K0l/psAL8sCClhi71VkqkA8wOl3dpv07OE5zOS3PueLUBa
kqxBNmF7CwR2tmqAt+esBGqO/3oTz2Y3gRjfrwO4Uiw2DezFON/V7eacMpPT0LoMF1TxJGHOq2gv
uKDOI1QyqUz/wLQbbnZv4vqKs0wvZlBc0UipAKLpXFuOosuuDndO2AQod17cFJfkpHV2wlEVOM+M
uLEn0GhdjiqduMxiAuQbGnBB9B7Xa8zWZ53mrZbUkZXtAr/EHcWZWkZ5rU80159aKwUJ26PVb8jO
yT+qYDtSajc+/O6PwJfpvB2kf9PB0KYqqJswL+Q9lckNTMQ8YydUzhK4wUft2ikpcEoHj0lZeBae
En5+YWHm/aFhAG4i1z/Rbcaf3zc7Y8GKzxOGYzvvDLvaZUAkMIpns4gJ9BzC6FBiKHNWkNj5r6DD
yS5c3kiOP+/l00uQsDeQwAiWh1ULLKqvNGWPapfKWf9GV2NLW4SDYe1PUFTZYVDuQRYsggmOaM5u
q/OXvbd1hNSRiknE+TPktukBX87qf+r6LwNeq3blMCIMYQdE0L17dDY+7aFISzbGipsGDXAxmEZA
uWH2Jgf7mXxT8UWj/aLkYp0OqnL7UKykMb+VhT3k/Ezwne2eXhxrHYK1UgwiPVBim5DK2XDCuZAq
MBedS6ZpkxN+fZBHygtKChWWwHw38DsaI8UbZHgQ/9beTw0FIdrWscMeFrVZgBLRt0fRar1HHScq
HpmgPelYSrl1r5pUgv0YtxTLzKXpw9FMfAnJMiJc+YzFWFMbgNMc03b8AIM46NcJzUzvBKPs2tRs
r9Tgcfc8Sj2rJGZ//NBUTL4vKlm+wbolLo6uxxWWMoUEPs42EBk99aiTX+WiG+s0ZIpOKfgobRoP
bQiCXo1QGNn+UTZTYCwTZS1T9jhulKS7mwfYtEZFSA3Up4VHsFYuloAOM4vDj4TSeng6+wOTsay5
3mdsIzKJwomodmvxQp0ifM+bPz8naH/QSq3cyhwT5BKedqL5gjuBHjNYWf59afVVsnLCG3Ny4c+a
XrIBMEcxM5C3xdm0BMOxRH9JbPWWe2tsXZGueYIKLB1tAFlPT9V8jNjxU9ZF7PZr8xbjUnYkO9KM
a+UsiGzu2jOGc1u+iRGHc1FP7Ps31r5ma5jYcHy9/SNKGFHFkdbCJki7nUub6KDXMmp1iM+ih1Xe
9Txm5czPOzzhQtk/EoYF4GdVGqDSSdZ6vAuQt2jo5MT6wAgasT1zjVZT5xeS5ZE2I0ZQ5ft2+OdB
29QhI2SGrJzRAupXdxo8IW7xSj7PSM9ufLKnvFY5gTHRWRZs41e9Y/FLe0oFi5sMSyrN0DtWWW/i
m7n3z+eLbgYYhkxwum36a0Oq2mS+DDeRpd9Ww1Tu5gSjiYcrYjOt1rktRgr1AYPpCFN2h61rk5Sy
iwoBfY05ek5W+xdhLQ1OLVU5Cx3XQE4qCSqLmunth6N1t3W0lw0n83BXwA/yUHPcm7EXB6J4exxi
4w0hL6dHO8glDu3tWYX4x3vQXCriOPzTPeu/LNQcfvUV4GQ36XzHf0jSUz6cOBltspY/neP3Dr4j
wf1dOZan3zADNqHoLsyBI7cu/Lzwm69tXEqC4prPK8/6q6vojONf3nUwY+6EXodwDKv+ZVG2S7PL
dyca7llyPKgMkJGz/ge6AkbNSQ9xC9+5c3JlbWg4alKVHMoweFu14D5egH5qbaqxm8FdT2vnesqT
PGnOOu5o/UapsWfO0R0bgqQJH76tuzQpRjUn7a/5krHrMt5V+8umS5wphhFGFp9pZe5Pe7PTajDM
x2in5rV1+04Zq07IdP5Abj3kfsaeamkjeoElOsLrWBqc3CzFbhLL+IRuD3AZlnzK2Nqd+Qq0pHvV
5Zz5ivOFwvgVcgJgoWRzeUt/wJ+Ylx9CpTEjFVIafUvl40KnyKJToXACiUT0iuhkvVbmNcOpDzA+
YmFphgBX/tkYg64kwqtaSmkftjtJ5lGWRQfMmP1CsECdzrsOCAUdnUhf9iU3IkJcNB4NZamAdJjb
6uWdRCIKjJeSBVffmQY9qEFRM8SMCF5AogYJ8gmde1LlYGVt5qPKxpvegBqCsI74+F/h+Meli248
PUcJPDe7boGL11PSAb6FVaQ+46fK7Mc3PuUC7CC5Cafw4jGkDVOmk5JIMrFxxAwfpFonMP2iwimd
WpBh+BJPPdCb3O/5pMBmXyAX93sGmEz7w52xsWn38sXhj3YY66v/S0RAoMwY6JH2oGrQzZ7oHRlG
6d/ozbhrQ+nCX7VQbiqqisaYDRvkaqk7AL8efa3YjkjfQkm3myzTz8Ms7ZBOqWRGSDRM1G+s/WTm
qHO3olemmSD+hocblS0y/1EkW9L6A30Y7Nr6d/RwQWVxvme/HNcpHv7Sm+43Rp4k5KP2F0hkhAea
fpzMDUaKJrXgbpiOoZKPySf/zzVcPTb6BGyWATkQ+5VFN8ccLoDucSp3DTjpgnvJUMawYI8dkwDb
zikZi+QrgGdOJTyOqoHqTc8WJ0PsD6QhwbNyJUGd1ck7AR/3Lgq/l5C3+itZxRB0GHwCI2yXdlcE
+cbc03vn1YBlqAO0H5Bb8IOBcCGh4yIURTnMTAD44bAjv0xaBTLnHwDcdWM3MB0iy7nhrBscqrjL
qf3hHkoxoLKiME52NFrAChGIVF1w1OgvmCgU4AHJIT24rfBNWi4W+LvqJVlr9GdOhkh619Bf1qD0
HjQMUKb+PZpsjQgCV7R2fzgBfM+o4uyLi6E40gQg4etoRoDUZY1Yx06G3MqQVSAwT0zTW98pp0Jw
qS1zdmE7QfLVZblt03PWgqjsaRhOSlJes9s1hWbwc0B9aVBkzcJJlPRcgNYluKDi9Tq4MBm5ghHp
qjmDO/5BMRwJ/v14U+fQX5jl0TwQugj8eoIaHHvnsTgpbt2OqJhMC86FIZj2KKdQYFC49G14i//U
Zvq/ELVZ7vZPOteuHi0B/rjqVJ3V72IIxyRNXMHHdPvlKvkRf+KCl4++H6Z4mbODOUMS3IZeLEMx
FeznAuAfzfnTlmUJ281KtijBaxcn1TgmnCYaB/VvFY3eNmc2AeqeMyBneWsSdjz4Ss4vWojwYRk9
d8Tftq1Nkv6PZaWr+RO2JJCyW/HPcKa3iMOcBWRyzXSCiO23ErWoRXOREd4m/CV0fdQ3HnqK/cMT
lDWr51OAjjAf2tnYKZN0dbE8/yK6J3Upkx6WHFFWIxBumvRO90CddHvH3G2t2Ol18zmYDLH4Idej
G26JMiHmsM9M8j0wMwcvBbxx6XyT024w474ESrRmip/ZcL2jSbGYaQ6Xag1/Y9XZBOV3ZnuysqCx
VUfFc9in/NzyF5eMZeUVcAYDeCqqXaCh9X0568Ey8LNPMiTZIXqXu+Ry21azEKXRgiV9kNNZyrCM
pA9TeDy+c2LUHOZxVqBvQHXIN2hlP+u8rrVnyPr/nxOHJJLkjqHQN05VgONYcDQimdiym723NGCd
uldhv50c+A06JAt7ta6BEiGbNGEtZa8RBhzzoqsaYq1WKLq+3k0B6lZl5vYTJmV3pEUS2O/nuh24
9HzhzuYLYKpUuH6G7nSgzfBbFjceRXqi6dDFb7DYSp8vjqswaamDQQKyFCpfoxSJmOc0HyPUgz+7
+z8cJSC+KUTPFM8ej5pXoQ6DeRJUpZ7sMmCbXo9c+U1/T2yp69r+8YhwffJYGVZ32TsRiUza1rLP
nSvBqztDoK+f3C5xO9Yx4VlkDTrTIYnXhv/Vu6kjiILmH+JH2YyyZMe3MMVTz7ByBwp9RbzFPsu8
wdp2/PLs3KGp87KsX2TaAv/D2v/sjJS0Iw4iodcuNYzSKBhzpse7G8DekA/EVkryYPL5k9jaZkyH
1+f8GpCkpzXi+H0qxJ/0ooqQdb9toHR2SduZCmOqVsVfXbvOXpJxeG3Lhqr2kuKB28QTm8xfphnl
jxWUZKpDzeomdMevnOkhWaeX2u7f6bqgF7A/s/KINTzxGaxQdFi7nsDYxAQP3mt5Xz9Ckz3a6wPO
eH9yeYVy5J7D7KIQFuKyL5INPiZQLgHnRPifIT1Bgb13CX9Lct5LDJT2perLob2zMvqn/B+4yPL9
CqiQqMvV2Zoz/JSX7aS4lQflaIwmnKrDT4Up1y29hmT/rcmigUMxEw+u4frAg0m+0Mt0urWAW9LM
+Ox8Hr50qDFbxAoKqoLdWIXOOJj/nrNqitPzqRwkpECbq9IimQJo/GlSVxerIdghmWKhAnYXWswW
fKSOm5aLSt6LTLattggKw2C4F9lt23eWryGmjiXZYOBrbQmUsYr36thsemQKYvwbKwh1L/M7Hmva
ItHfItfGuGxhrevX/yHR/4zNzVvrzEVefmtc6G5Cz855AqyW+s10POj5byX4el9Q6nEbQjSMy2P6
sz+E0IHMnvB79krrHzfJjkBEQTT8Bobg6Tubi7VGpr+RjA8+ZSqETb8RiA9cT3FYmPRyHh0wouER
8mVetjYeMthDA85XODlAU0pVbQTmQLRxShovhXQYVq7Ftk2ny0ontfv8sZqEOBQ09wbSs05O1z95
vquC+DpdwseADTYrH6pQUdBdUAHdAkFvwdUwow/XOQCxaEV7MA58tcpkFDw1POC1A/cYexLkzbOL
iUtrEIpbpTeVQwCkBN+pTDwZvh1bOzC0PohyKW5wR98EVJaX2apYnpjtV9GeIGMHM1nMH8gtYvzr
tQW1sd3uwD1R7EbBzAKTqowp8Q7F0cuxXhx/Dzd4pxZIbF73IgkmWsVTn0VRFfmHVesCR6xhlDzs
H2byeJ3L5y4n5QTuhI/sxjFHz2PY5wPcB3wHp3C5DHopuwKge/WZxrO/89x0xSHQjbUQ0o8kWLS5
DegqakKNRDUz/BIfVhUE2Og1YcPgnzZnwH3nwjLhyEd6rnCWzmL9CLvKrhEPscTud4yKfIRzYNUs
YXFnqdmAZVQ/YSLWSVgj01jee30QbrSIkWC8KES5NZpfWomBPJ6rHvps1r+6Erz8PqcqHgTHnKrd
0D1l0mUgcqZlw+srqxDnN96IKVZ9nt+KRZOH8G4xYgE1sbC3E+5rA8c66XNZcy2mI9b8q2Jlv1fi
1RLgaN0G2DmwqZo3jp5utGPjHnRieCZVf6nnQUmOGWZdXoIQsC4vvv1JNwWfhPgVUPFadLpRJWiK
m4mk3OEGxwwuu8lix0O04XjPT4rte/EcPxziymvSqfRewLtB7pDH2Td6BlW7f4SF4OEyqIIwZnJt
mE8KJzJoa9Hsp3EwKLw9muBayVC8aZ8d4l0fVXJvENf8jARrZNNE2Pta2SkU6NMJZdeQcW8AvDS+
jP7f/xGj8nCi2/znogQ1kIrH/SOwiqKj8UUZgj7+tQ1kSwUx5uuvsvY/SkZIs9yQZJ0UkIZSrkDB
QAnoVbawRn16Zq3GAXf/NWdvgtG2bwFkgJOD3kZFnpMMhYUJAIM5XtkpgrdT/LPbaPIH/2/21fAQ
R4J7DSYWLK3wTFtggHB+4gkq+K8CLky36RShp5xh6997s3DeIiGOdVnLgmGz5j/gMqWrwP7B2mLJ
aHN9mkg6a/cqUwe0lp/silYT7jPOPUg8zkfaC+fhRm/hr0NmTiIFPaDOBWWNqR6LHONYlDEWaK0u
OZ51fIkCEiSbZy8MQx9gBZ5IAVvAyRHVO97RQeN6iwdOt3XM3PxDD4vp0zRoECrwugwQlLBiM0X7
r1ESOn7oVUZ/SphRNqSE0uW1jxPkwTrQRxS3ce6bDtM6H5X34wneI9WasZIyw1TZTWofVk14tTeE
IBNul2XRd4JXPJk/aNsSaw5YyidxKt02Xu6lr6wVijNoNApPy3CS2/+mFbQorCtrEqpHBBmwL2cb
Dorn8W8SDnr/4WLcmaD2jOQtDz4CXYOrOMqmm63/8VrzPD7S61b0uVRN+sbiBM0HsL2+ca/hHRKB
MFqsTMPsMM0VjSFuctn686m9khdL1Ktq+rYDVqMbbDDsi63TrAeQmZbMtkOuTeANVclKyEtdXF1A
JzIbCezdVEC0Oq5c9diD/LNc/gV79cnhhqQOxTVjDHidLUml2nH6aB4+V3aztmEsxApxh5OIrt/g
wWpk/TGd8vqhomxHvDpaWgOmMio11hj2gg7KbxQ2OtQ1F7vOd0SZav+9IYrEnNyzY2EiahHgjTk9
l0U7QpqJXLveknV2yi9J+vY+eBFPCIXRRUxdHRrRnRBMLGcZsgACF1iVEwgaUI9V1S2PcCFrO9MD
AuipONHITQnFFTTCoCpualYX+luKvnS6qY2HHMPp/L0mn9FqE2Ppg0p7mVBT/DRktuqL2LpMLMUH
NU5JSzCKoJChlfXwQLLK2xSAxOao4jtrR8QR7Ajb4Obk4mWakbImZQK1uFTwb1NKlym2tuAdJr67
d8nX1YZE601a31VmdhPlNLiPdrHo2QgXLsUXdd3Z0TIJwmYP5pJq5dAfn4pawA5UEIJhFpqAu57Q
lHgU7KZKpttTEHmVzp8pZBRQD2OiV6jiLzFL2GRkNjiHwlsDWL2gBJFzXCobEVqhJPdZIrQNqgqT
mA/uosBougLBDxShZ/5WMbo4wVmhB4bfPYxdlXaUMLNgHwZ1STAKYgPXkXvZECalDPyMxzq7I9tF
4sRTYj8fyjMfMqEE82MYzhls+PHAj6PR3qhNCMvHMGu08ACe0bPZHLBZDwq+swgkttltGGS6Nrq9
laVjljSTHH4eGzMX9XgiZuZBg4TetfygGwAURcEBmxNY/psYqWKdMMVKj8bPd9wJJgW1jdymwrx2
cFTWVMDbmNp/Hv7x4fmRCObBVUPRQPmC2RtcGqEy+Gl1FHTIc+GB7cqKj2yFb+CEUFr64n4DYR1d
oBAbaDqoPEWT4kf7/mScpFYlR5B8qdkuEFH+/u0/Atxby/+3z+YkW8i6jd0jC1VvdnyLpZxCZX5X
rP2381r5rxuPuAJhvHvaysP40QLOp6/eOSUvKmq4E8dceGkjCtex9pcN/f+mhLGzdPOwLhxosKRe
F7rnOcJeHlARb84BBuobW75GwGfSmOUXwBg12S5tht8x7ikROQCNiSG8m4Tv9qv6lIGuiNqk7nzE
nNib/TnUkUE1lKz36LmvsPrN0UVr/YzrZZMC3q4u497xpR8MHri6+A7VeC1o5JF7zEDnHx002GfB
DVINW4JQ7rxPBiZqJYCAfoVdY6R06TjYEgLyBnJfjP/2dxLAA2ztSN0llWxhxa7PxOzBOWPExEiz
vVTvaRUyxJYcBT8VYtsTx/cc1z6xG7lpmlz8rVwdk2OIFM4JndNGG9YgbVPTVq3xv44Ryz428uea
5uRhB4RqCg9T7gMjYqw3sofDXVydqxsBaP4VxKLKZGQGlHjSftS2aOHzFghKgT20evRdOIlknrLI
HiawkvspyvXK/Xng3cOSm0fheLPC1XsumvVeqNgxG5puXzddQjuDRhyKvfnrIAXQ3vnNaR+SfYBc
igiia9GYrXl4zykRRKQGyUe6/g1e+gW/ZIrEkC2vhAXCOj5XV0/KxxZH/oYeAN/l5oWj1oVpsLES
8kafQTCyW4Xi8dH6eTMbc+fk5NYZ1mc10CErkY0G7PuF95o8RNPUMeHNgBkBEAr3g3an3Hp+PTis
2iCr1WaVODpGD4+l5VvXhU+PPLugJubgWr8sp0EvTS2Nk8bY4z/j0Nfg1WVDZ6CPZ0dOrehqirUt
gPMi4YsMQbc35FmyGWn3GZD1JgEnlg4nE1g/tfmA2+oyu+AgZTyxj52Wj6kugX15/pgfYNBsK22W
SmMJF7Qn/e4m24ZbMOYG5UR1RIroJg3CYD1WvXdBoSMnze43xJBii1uqxr1uoT8aDOMK337DfmhG
hWZTA2b28h1Dwe+2i7bE/yUUT7S0TVOeRiWz89A8B/yxbE5Yv/4YIqmgblcxZxxOJVXddg1DT7mJ
N9/tNtM2vUGjFzpGS21C21seJJWMyh7QHRTt03knVb99Ab7Id2g7T+SSSImiIc+3PJTJeD3IADQY
j1/cGjtSDZp/QNNI2kdhjHvL9SfK++WOSAOScfzld0BXgMLmq0tBR92c7wF4yKaTaoGlNJUmzxIP
Z37uSpTWmyN5S7S9NDBLZQvlHyQLYcXiqEUr8/Eg5U7x4io1B7sRO5CNz1KgiBo6sJIEYqPzGYSj
H29LmOhAYtcNDrYt6HfkOsKjH4aMLRDOrOQDjHyh1LeIRZjrBcJbM+P3MdC+ArlvpLJUcDzA+ScY
CHw/nG+qkJcBtGsJ8j86ErDN9cor9siqtRf5Wuj6Qhw6qSVy+/NQcRRRnPnqO1NAzsL21Spiyc3F
d2IRwxebeQAdlJ/SCAYDmM3Ah7bCgyKmeJU1pBGF/9V2KvMd8BVxj2jjjIwzhtiY0a/7BIv9PzcO
JDbeOHJQbF5U/JUZT6W/eXXVVtf7MKP1TOCd8GluCxGHL2L4BXvi3fIilSwZqhehmzA9WYSW0IRR
Z0DfsUpGVx6u2iWMpPSIumCBRWjV4P7ktgeFIC/2vRp3nWXuQIxeYBqTGzKjcNypcDqm3HixC/xb
JsxS8DTx9FIoZmtOBLbzT37KIaNUBgX30AnUGUzsg4hiT9xpvRFxfd2mCe1jGTyJnjCa9a8qSyYf
XgMa7tlUIY3tjdlQuxLBmk3YMYHbwVYLupPwPFfDOb5okQSjAqx9Ev0pgG8hWJk5+amQEHB1zARL
00aarfzKMOwbDGwZJjnmUqUEd0lUP6bR7+Ili8HsEmau9RrwmDtxNiGXQSPdBS4piq1e+Pw1ltYp
dq8B4MMOK5ldJxKX1vWxyNTgFOi3UkAopBakm7p04/D905VFeS4hr6HDlF5i+QFfRGvEJLPpe9Dz
ysHfkIDMk7AG3h7e4W0pzb2brv95ahC61W22PrLKbFJFwBIjMOR9w8rhriC3IgARA6loi+NVZ7ys
32nqXL+9Xmpen7OQiw2D6aXydr0d7ncPhASEYPtiPkeRvRoN5CMOQjJZZfuH8gFBmuDdTNvUwtby
469M0179LWpZljCWF5icMLSWG70Mlfak37l18MPoAc2296Fe0Rm/EtjaD0xq7sq1/aajqA+/vgce
iKIwF69EvG1/+fO0U9xAYK7RJjl0c7BksjZFPf8wln6/fvLAQ+N5ES8p5UF0GiQkSZqqf4LcVmwK
rFtDDJa7r96BT5Se6paxG9sI7QfVgYMMivAlU4wMx16AmNCJiboqfKygLaOsOP70QCcQwdPloSgV
wc9EL3vuMRDSLV0SHc/2s3AdCJ8dCTGrHYnlUH8Ju9gM7V3e5cEjgSuERf6WwBT77Ao3XJs32caL
8MGpddP/dU8YkXrtD7tUngrB31ULHBJAD3EYxZxZMRBgKVG08oNgCpmQB871I0XzuCxeQ9N2bHv2
mOY6d6VI51CraemmDUv9O0PyT6S5naX4y+mLkDyX+fae9xnTqLbb7dad2refztSNPRpjSgpf9nea
pSlUdmNWOBsCY99BYlzwcM2aBR9LmrvuNoDT1DoqmQQa5v0Ghg3VvXcMpBFPaLxMNSvTNY52WiTI
wKb7AEO+BB+5gVrFO9LueSXpjUM4L8F+7my1T+9PifdicjDk5lhhobeJ8Zyx7c+v32P+BpFgZY+y
GzAjRIn7EPR6mGsqtonCnLu3Cqawwsklmwpa8epOrcxoori+hTfEYOBHi8KSchPH099qjOF7E1hK
LFrcVyYMRbAFkQRmEbRakM3TqBrPu2gCAMzrYZ3TToGxckmtwhLnir+21ENgolwA1P/oKrsofgYp
U2j6+1XGqMOGdNkfxF4tu6tbvGC9zJTByRhS8WW7973Tz0PerbSDQPZRuXmrnEhbJnkNQ/3kVFOC
mvb/7pr/BJ8KcIdVtmJpouYmlI5lQIzvZ6sDlVvfPYCk5WMeddh0H9QYFtZJ1V4kwcDPFQmYISk2
vpAMrx8qY+lhgAFzw3/iWegHbOOnAJk8wB3JcVmQH2hE+3glMXPkhFvLMRbtLlF9KxCiIGuAffnE
StPrIl8sjhnyUy/sjNkCPv6UTJ2Qjla+1NE3taxXAz4DzrN4sQYZmSpthjDRx8r3d7fI5VE343vj
lRZBJL0x0RjQFxziMWFewfAtBfNK5OOWZzpTbthEFZ+0YUxligKoJlIdkAVgt7B+iu2mLZ426L2U
WTSnMxahbljh6TLc7jGuRYq4bVJHFhBQAg6dyvn/sfZ8s8J1ys0AJBcPdCaKJuqIwmN0R7F13fHt
aya7pzgqZ+qPRQNwQZKcb7Xm+if+Hv9Vkrd6HH5WKA0BdCxTfRp6IH5ii86WNkcOHzM=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
