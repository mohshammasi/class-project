// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sun Nov 21 02:42:26 2021
// Host        : rsws13.kaust.edu.sa running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/alshma0b/Desktop/class_project/class_project.gen/sources_1/ip/yoshi_right_mem/yoshi_right_mem_sim_netlist.v
// Design      : yoshi_right_mem
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "yoshi_right_mem,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module yoshi_right_mem
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [10:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [10:0]addra;
  wire clka;
  wire [11:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [10:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [10:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "11" *) 
  (* C_ADDRB_WIDTH = "11" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "1" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.35235 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "yoshi_right_mem.mem" *) 
  (* C_INIT_FILE_NAME = "yoshi_right_mem.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1344" *) 
  (* C_READ_DEPTH_B = "1344" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1344" *) 
  (* C_WRITE_DEPTH_B = "1344" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  yoshi_right_mem_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[10:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[10:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 26400)
`pragma protect data_block
fpuhYjR8VF5BnyUHqwZvN7h2DDq8vtdJmMnfS9lSBEyNQaC1ZZ9iyPMY+WpxTiKG5D7Y+hFafJ3M
SsA5C/hRWSMLvMeLgUK20aeIhrDFL+054skDmorq9KpIzQzCyF/EeyGAmLFVshFgWFhLSOTUwrq6
XiNFl3B+BFuYbBv7ykenisDtEAShmgNzSRxxQFHrmQoS57aGEvBIYkL1twaKIiCHEaky3FjllqAL
UeBOcXNQMcp8GB7cJVDc9/Xx6niPz3lw2wAhb7p4fii4QQbwGt35oe0YiJG4iIwU2xb8rGWXAEqx
GHjWi9lvxzJULIpYJXkdOIbVOXtsRGzq0ZXveXRxmXa+zDFqY/qTP7R3Fw8NnYibef+6S52TmUvm
qKu1qeLwA2/L+Ti47d8HiHuvdG/2iNXFLhfUWzN3/RMdj+yxiAhNe92qk0dtWxqe3zzH4LVvLwtY
7o2EBH/hnxuJgVwIlNHF0J3g8/elhpvrKBFMoq3ddqt1fp7wivJIP0AlWjxrTfRFK+kYvKzWssXW
avw//Z0HmWjqDULf0PhJi9iq8l61TXJyefV6JAL77Q1Bw8qvEpW5GD4CKYwhWGLRfbXEWb6sF0VB
WKjdJr/16xV+e+V8IRCE+sWI+fjhPNH8kUV+pOgrWHlnktw8R3navsyKw72z96ryeo5AMu3suRhV
t6YY5OV5rX96WBzv6x8wFBmwCKfUiZEXxO30dBU1DUbkY5TJh2Z2R31Q7Re5asSyUNWGDz+88W2T
BdG5tIjSX9MlRw5/b7klUAJYgM43yJTh3TIVhS3yh6s3nhyPmh6zLwYRwJeUXPBd4IKTrkq4ivZf
v5qNZakvIFPXSB1ZA0LKVuvXVmTFrdPFx5w+7jAu/EYdwUyGt3YRXZsPW5627snaI05Qae6j9ooa
tigClRGG3nLlkJM6SpgdMNNcjpyWZWfHoUrWYWvSUG/kSHC4DXi9P0ZkOePXMtTTTqjeji/gSdk9
iIcq2JF02lraIElc5lPErRyRtP8VteX/SNaRSn5WABJVRxgf4v+s4uiNEhbY7vGxwxP3kVeRyeAs
OOqaN+ARqO2KINkRRF1+vypVhc7hKr/lYu5OeHOsmoCeQ+ztkUx0rcfLKS6+IU2TreWz+I4e416T
x13FqZ9Ka4R3o6YFLiogk0a2by4D+g3nQ7jjrjQZGOWJWy363zjGMq9+/Vd839wQxKX79AUh1524
+jz9xGvJ+vfghVwpMmsc2ZQYRDFVOeyCT92L19a1goXxtvPjBFPFlDdFz6iAevx/WqMzzpqoscyw
VpwenVd1EQSX7tjYCc9xRR7643w2WIQ7+uOHejQZUmws9uziO0A7pyjEjTAlq+HgbCgeE1P2DxfD
fe2pOCKmn7ksn0lrWkxZhhm89PIZ0IH4iFw1weF1oS2gJNCMAohjw/D6xFRDpQTgJLfOmHVE4/VL
lF5aONGwfe+6TsgePXQdROxHy0av6fG0zggTLRvO6gweGxmzsXAsv7sKCWsE1OX4ud3g0OkkCtF5
tkCbwzjy2BYbbiUUzlti5CI3208C+k+CUqA9mC6wzwMDuwEYP2D+3KlL+NBTPjXH5lWiiezyKBhA
FBfmzjdgYy5buwI+90LSPECPTymT938B64Y6QO83p7eDMcKOc59CYjirUEYaE52K1dkqgF6qA0YM
+YI3ufy7B3uk46f4D+qS5M9TvAwyByhjK+bSRyps8TzVoPktqj8kTWK3cgZ0QC4Kq/i1Z9TID/IY
ZdczSixVBRlzzzVnBSC7mRkmf7aXOnu9VY6ScUuM06ULjcVxocvPcTDnloFpTeZeHUblb7hEP8TC
4fBiXsniHKbnh4bGz1G6VMr65ds/8rHCWvYMMPdYhcLGmcQRi0Zzk/jGZQHtOAGAPFPAOGfdEqsz
p1Q5vXi7tHrLZ6Pa6KKqAPJ3k3eK5Mi5MAgkGgoq0BGxpIAksz0JQe4Qv8j+waXn1Jeqj5lBxmak
AxBNnOOXJ40T7O3bEGQNcMa5HtG91dZKqtVUamA/CDhgybmYqGwovgPYQSwfhwMKnsRutuJnyir9
abnjkDvMZrRFiQPbV49BxMs195wz0dx47JWIPaAFyXxfRCrRgtnp/FTfISO8E1hQ/cxdr86l8ErK
lBsrEGkk2O4KnKGWXRkjmHZCdWYWPZPd0h/wuS88Ubz+uykS7ezT59uj+YRYD3MzQ2m/4ed/0+d9
dyCYZk4MD28Dasog1Sx1SDcQRsW9bhCjee6QgZR67t/Ed0YNP8nW0JdfwQDx+fglBlENTltOewsZ
Hbi3ao45ApKUPZU5dnGmivJh8Se/9BlPr+3ILrPlhSKuJHiZZcRAwVfW7XygWmI7d4WCVlQlIuO/
soLsrWCVYuVZxoXsFqrFzrK4kF2YURmSqs06Vj+YnkpMVmJ/dckQZayXFja3VBhiO/FW44tJGDI0
VUBg7pnhWciSWHcPhwaZFmo+rIj3boabmEuA87LwZCTvyT8pwpH5VRLA6XszsWMCRmNdIVetpxlk
BFJdeGg1RDkezZ+wv9kJPF2wXeTKenC5mjTOSh8eiI4XAyJ11GxwoklXvmB3xzFoTUcrWAzofgUG
OWpiNuqk0NN5l2FAwI5woWpWVCYoC5ld7EMTFWFOXufM0gBnD6ZhY8JhWH8H6XMMBvkgfXlCog4j
yX0Kwh5cyVW0qUJdChSCFUTuFKP+hl6GA03uxXRBo0r/5jg0nSUjyKKXzqmPcU3C4NU7ar2qOrtI
plKPEQfG1rL0RG5kCrlkbSbjeauVrY7uBINFN8vBPYETj6kxpp4Z2yT60EyCcbzjmgC/GikmeHJF
pb0d69bbc/YufE8N1uCYSBw5ih9uRUxboUwpcn3p1l6YKLhU3GuJhR0Acs9/fJy2+LRuNHSRSUoj
dW9XmwMbV0EVekG3KC+qjUKmj6hX4bSpE04jHhjVz06W1gwCtF97Hqgoe/YN4J72JCtRdiA0h0D+
VevfpRJ9xkjIG2GC5wMz25bQYR3V0Bf8wWwn7fXzTx+KzmWyfnT3VZXGFfiW9PviMPa94OZUwzTX
/d0nX9NqzNzbh2lQN0cpfeUUEmY97GJMZZQQbsXO6cKSR3h0+dDewEV9nqAbGMu0lv1ppRDCgqoR
i9MbXz09XxguFemxsLrpAd1W79bSYACy1BqVE+Zc7ml4cn4r3wZXuJb2t4UNaElQysDVX+BGQ9Ci
yuDVUHTaL4OAQLTNJ3gPh03D6M/4b3KHQeQxlYe7uE/Oxpuwif4EOhqnHBWYxAvOLR6baa+UY68C
9VyRdlwZnq9zSYHuYH9/VNHlJ3lhxVELokStS7iMwCMpJLZ3ZmQTCtx6codIYLFfOK50MQD4ItEM
XWYBgkYQ39p48ExoV/0V/FTe4XaJa73WPwnc3tjA+xWX3xzy479pdHwsNyxXSpfsxTkrKShJ2SnZ
RpijYwz5Se9F6vtaZSBTpUqsvCoaiEWwszSXU3PzWrgtNFTlonFtQqV+hKOaWUeOEEcYz020TmAR
Jn6uLs7QaGoNKGw9WO0Y/whn3gQg3cqD2fnCDaXQrobBGBTjkesk4Z6M9tv+rzEwpLHIyILkONWe
703AJWIZLnxx/gd7g8akWM1yb8MBcjgtozdRHxIVxUuHUbZp6WXnBuGWLyKD1FaL+uxM9GX97J5o
++7q2Ec0YkiOAlWeYVqIxVaIgR5U6gIzELjEMkVvDxlDCnuEJjmn9PQhDI1ZegrPtHUqp3crJ2Tz
BJZJJ8ouZc8wA10Lfd4Z2TdsrwPT3D4ZbFNdVdBu4yXKTn/mxaCezynLWg7cDAD2CgTvEBNyGkty
Zqk14wBI58eLXhb4CoJMINuW7Iot/vCgjLpN141GWFGoaMUzlGbfM1BtWuQM+DXGVVfnkI+FQWyH
GSLDqYAbWZxqMLxZ5IHPEHUlhBZBV8yxC1jUOCz0vXN1590CVDOPLRO0ed+JMfuQVwNmNKHpNONw
Q5dlQwz/klGopQ1OS2HXjmrL6x1FZQ6HniPQFuTYLGLOU40TdX5iFV6raWC1PYpeiBg7ZbYtFx2x
yF7D2AbgXTkz2bZiz+6+qczDPi93dCAUtXq6G7bErHtQDs7MW13uh9zvkknHBMVzIBeaIrYmg/HW
MzARJNd7tJKH+F4toW+7OVHKjwa/zkZME1RalzDmetMwuzvwPSoKOZAK4hKWdt8O69tlsnu4vJYv
h3tTQilOCv+dp0ZvHgwHQ6CT0+T0uPYzJXUl+nrXs3KPHcAsa3rDfvZ06gF0O3V5Z6sYyCku2z5q
S4JoK1gi0C0lRuYjzzf7m79GkxNaxmzRnqhwH4i9TV+1DT/eYPtMVuBhxeJ99/jaJjsuJ/RceRW+
GIoxlrp0waomaFOOq7IWpZRinLHkbdyeeFRBAAXaqrEdMNTrPXAa2WbG4EQsuZnhqTp2dW53eC9M
jtFY2HfGxzv5nIWAK+NqGaeyNyOpJjQSGIby2Rw+jb+jekLwDKmXdar3Lxdgx2V28ENSfj7SQZWx
vwZBCIDZa7id6ROWAyP2q32fCXq+KGCeb9bGBem2iW5lzMJ5fKO/NL/0Q2swf1UPEE2uF0msQewA
6z0UEjL1X5iHp2ZQXn2DUE9otQKUkfjfpF1xY8am1HyTWvAHg42WCrsvuLoiKKj7CLmFqKThJy/E
ezgvDQWRqGvGZcsI+9zObIWbbgwanxSjb0jRPwXU9R6Kpb1KIU5l6riIa6+doO/bcKbPeKBgSDqk
tT+iiyXsD5AA+qbdF2YHNDuyg3dQwya5c+9a30tKWjC1sC7iGIoTByNilSHWta/UgvFh/W6Gzb9U
PyS3r8vUhVcTrJSXzC1z3Ebb+m/WeLkI+InRzW3w1OP60YvCdNx9kjs5OtVL2nsGZ0HIvc1OFWkV
Ab03La9KiaNXt2n2mUGMl+kTzNSX1ZpF5iZcqdHzrVhElg7TuJvnVyX6yKQVXuisGetHpz93yrBK
tNog7GNrTsMfMQJ96ykQhFAnQTM/DW/lWG5wAn5ZEP3blHgHZkHFQTC2sznB9voXKTKtnLXwm8T9
DO0SVHMDNtR+IQp5dakQ3WP7ucC4wW8ATG9AvFGuObagMbVI4RT/rKW20uh7z0egRd0MJSACLuV9
jWYab299Brv4uzP3gKKj4HI645oBkvTNDcTifF9mny5UuOgzPYVj/6eN0THmElOJJbU9y0yLVxn6
K3AdxiiO4pYaA7nMybjDxhUF7QDd0N/tTgUoafWi3Pfce7At+PVyoPP92MDe9aGTlUyE1tO/Pwm5
GwrJ3TeY6na/Gk2bd5cp4xQjxGdPOhMRBAY30EnikyO8Gzuv79Nkcy0qzNFcvefe8e0hkXtZPu7p
5i9guqHgCpz42jxzxPJMyoFKG+NgKxkrkI6KWpOG27N5k80pEFbJXXGQ2RSrrlNWaO8Pev4iXioN
3xP/yf+CEASWdv5YSyx2IHoAgteCET5Qzri6hcL/vXcpmZh8Fj+8m5pij59x/whorX1DtwHqlizs
I+XftfF5DyLBQKJhZZnw5BzDqx9k6eD7+t4LJMk6fhyYtfvjntKGFt9PbbVDsaZgfy5hn1MENZif
/VR5Cm4yMo/lRkRtGBhWOANW7ffwrDWDpOsLmckCvqHyvVH8M8UiwCsd/ClXyxvI/feXrnujLA94
dOPVkPv0hyhzENWfzCvxaWAduCbkGkvRoigKI9u2In53HjBHOS8BofCVl0ypdjYd9kxBpCIeGvdE
JgI6gEkr7YvoaqboLcwmx3CKdj/FFtK5Sg6LTQPgCg1xo2vnSwbPQWnZk1v5P58vTlZQ2hvKjeKm
CLu6fiwauipFolNCyuG+Q/uEXWrlYXiIUyvNV7woEz7ysv/aVopyhhEFdxbPNlXjniu5Qf2FY4RF
Kv01AlvOmBBSuD++wW0NQ/Ii+58AOpmLWwYRqvVPaUb5iVMCe5XkkoDpsnORepejrOmL9Nn5HYPw
tck9FtYnhkwqRsv+R4NDxhhBMqFYxTn4vHkvvS4CzDNmCES1PALnyb4WSKl2AGp6R2jEZfvuZnt5
7nNnqAI8QVgCrS4bMwhTecU5V65ONjh0daOr1MxANx/+z3bi7z5cV9hJ4olwjPZsTN9AMdOxzltv
eJd3E83cGuUDWnCUaXJMAcAAxw5+cSJ4cQDgZDWktD8QNnZ/dMTr3l87RE2i7RQgdd59ZtBKXXtn
X48BA6UAV3kCjUaEJJIgj8ltdM0JA5b/00oS1fZYOSstpgDDtyH6G1I/cFPdnU5ptdvJXrYDW4JV
U3RHkUUCaj1bZZVzuCaqml0E0lCkxMpZmeBRFFdv/Vu0xrKz2fYsHIGHo/ZEAAFw4y9Koye5akqs
Ohsy5TtoR/q/xYI+1PgjhPDdgTLrFqYE5pj8pQ2cp94rUEMxqL3OdTp9lBqEAf0Y0QqOZGXNoojJ
ppcXN4OOrT6/6oukiYidyU5PPk/05uTpI/DfdUGuV9r8/i2+8mT4jv7D2lYXrYvBtc8Y4/pC2OCr
lsn02kJH7Zq0qgzKoIu2e+6WHIRUEdN/0SHCn7W3ukX1tmgBqvpXjhSWf3iLXHFrPvw97Z3LaebJ
PXxHtMaJhNUxDJvQKCJkOPVKcS4tGqPvi5aU5SrYvD/MUOkQWSC2R02AG94UZxTovkJM68S1xrxN
bdTXuTcjaNrMYPEsw95fG52Ra4Mtz0by8a0ZWNhd6dlSO5xM2Uim3dg+7K54cVMUaXjPrX5IGi8b
t6MaUqVAvUl4Y5yzZf8HbV3p8SoLCeybbFkIqYQMVvBmUlGUlCjyJ0EguGhk3askBdW9F6FxEe6c
0VsWsqKY/ydgCOyOINZdKZj72haZS1BVS26etIM9xiJ0HRBJ5Iie9mGTSnjr3wTTYF1l3l5o2M/6
dmU13+3rnR7f1BJ33MJfBFEcpkfBo1F/n1Y/dvwuAlVCDrt04yOGL7sdGOOrHUtrhL11iijEC4h+
FYfvuFx9pqNGx7FU7956kjWescvkLxEVU+3L5OPmx2lOAfRAj2Xsx0Np9vj6dYSNUtSqnfhNvIN6
SWVfjPoeIH9YTbpHJcwVCQvM4SWnaijvo36qzLgHujBp9AFzg1ZAUblevNYOKnwqzBWJehWgm3Y8
ZC6cvXdnGxnfg1c2+giA11Y7esFPyMSA9FbJa3t4xUxQk/OtQ7H3A9q8Vl0YCuNqrjvAYUCRUlsR
Kt52rO/eWKiSqqafh707hn59NllSra7wlHil+TZIVwtxZqRrqaJ0w+e5en7VW4hDmTa3FY0m7+TK
XL0vsMjoTVmdwM6w3y1RlPUUpI+LlupeZ8gQVWt/BIdKfsTx1+c0JlzFkmJFwXVdKrcRPHH9OWpM
mtmFgnhgZv8fmlF8kJ+ND2MPIp3LkMP2JPkowY/QIQfo9uEvKaCYlWBFrQqh7Hz3ngjiu+EWJqmn
TRIPkX6HuQDzjejUZSUk7WBVL2Vfdxv4/2HEUWbVDtTtc7V4scMZflwCv4xtYunUUilaryEE+pFj
RYTdxlv5U5G/PCktKGNoWxoXT1Xdv+t0N58JoenBHo4uEgVHkTP88lY+HUMC/atv7rgp0+4B0Tji
1wRj0n2Ooo5D/qfllru8JUOCwzOCg9V8lYP8w7hDaRRmqd9vdzdDYnYoeFGVjNBpw6JFml6cjsJi
tjN7fAxFrD6lX5Jy+6hYmq78Bgz/JvXEy2yVaneN0QKJpLL2EEQz6pDG+dnMV3gM8bfinErckRvq
x1hKM7S/dZPfRbN8zhEcSxOt2mkMEtpY+k9K1BGQzCstVWQsGfCWQhezBr7d2BpeEaYtDKutyDgz
3oAQ1NDShm4A9Nh0qV48cblRg6tl5mu2YogMrdRPUiqdrEHjEFo34SYEUXy3b2GjjslQrei51GL2
zMqlNhsF2Yt9KyPP2MuFRMD8eWxC69g6582aJq4JCUmtyMh033D7PDkqGtJR9ZOHuXsvxHpBShje
XS0WMqqpwPPmimQgeuewp55AOLgUVar6oV4QhSjEOqLkMQUe76ZtvIuWe7g1NVY5a98dnSgONXvd
QvFX/96iBHeGQnxQrV/OA8utyA/ksVrW7oKQ72AdOllkns1+D+hlTkHt0I7C0KkramBpmIjedl9M
uPq4vFeWia0aji87vF5tOJrjpK34zlG8lyj/LoGcd7AgusX9uWhPzh14pzYosbsVrU6YABRgHJHz
q0cs3P88kYtvLHwhe1+4viK+sIGZYMP8KBepmmlgVQfg/B/XSM8QqupfBiq2WnB9Hbkt/AAxHNum
xQ7BLhKtH4uzvR2qveNp8Ti+F3QH/rSjUK+UDOV2l/GOzaHTUxg6rjsFABl0PH2pVAGCoKuPd+LE
tqrFM4WJj7QhpWXaMQ4sEn7zaXnoUwl0jV3urL2o0b2EJlaG3J8KDVYCZbmbKsDyDl2bz4PsJ85T
BoWTv2NNhaXIB0M5DJGZXUBsC+doM9RZ2olcFmWOnavTBxkblOgk9Fe0I0qSZrW9IMyMqbOKX2wt
WwfQaPApHOnCo8AfcaCZ0q37jY6ZDD19dtI3KyYv9KSlMrICeFsGtWhn0414Ik3FtUPhoh+e01oT
PEPuWq8H0TqKagA1953UTHaYsvk3PtixxMn0eyoLWB/0D9F91h3htv8vKdoGcV7VJof5naDSP4k4
eL0nx34hdkQ1Ec0cR0NGqZEid6chpyaLwhNcroBGCRTVo1DdBGBgrzsGhFgalYpSuz9CM6G1naeM
tI5Qm4J4IMFRaH5i6UUt+4MHl43haH7tG5+dt0Cw68YAS+BoYxvFBEIYZmUDiKKH784aHVS94tAX
8y1BEjISWiw/xCc0CyeMe6l+W+xSNmN10rtX+TECHdMXSMtP/NzsBNS4EBAhwueaUNYQjXcMmDnp
7Mg7BJ5FPRBIWvjYlRAn9Dfgel9xJX5TLEWQU4tWWT+IjYuV1Idr+qxO1+duCS3zgX+GbrZf28Ok
MIKd0b2S6bWg/iJwZcfqs0QWauxc5JXk1BvqF+G9dORbi4PDWwjQ5EhBMWNshhC3Nkgb5I0CR49X
XYcALQUysH98l5JG+V40hEbVLzbBOaiDnpllRXuTyJQYW/5LQyIGipJoDExLT6aOBEzVqERhWY9o
eWMIgsHf0yHVgkzYDp9iJFU6wKV+fx4okDDLExkKmUB0UEsJFkb4p5tBMi3t1g9l2IDZr8YeY3xK
/ypIpes9Y7/mZga36xDtYu4kYsg/vcEiVMcUl0lC8V8thuuZ6hjriMPh43v6KmCC3jOEQWfZkKqX
DppAnw44z153BSpP/XtIOlmaGzoCGUGnVNpzz0F5hSGhplSzrIJhoptMspQiWy5S11sLiZ+HOzp4
hXdnS5IrEyzbpb0yfhpMnHJGGi0/5xzF2TUeoIiOsiWrirSiudQfCzZ2Oq+2yXDYO+iexGWBXkd3
Q5jNTHHyQyV3/inGs8xOT4zRAUzF4iWVIbAjYhirK+jITOE9CgYnAPa5KWQtu8dLpSrI0z0dqbhM
tZ//i9BF+2AR3PuhUcKvtvO7AZY2/aT+o8N4H1/Oeea83F2AfAkL1vsvxFWh9yP8yKnrLFA83+lk
E+uWZxg/S3g1IeRAZ99rNlIWMS/cBEVnU+HuLDQbG3nbFH49mBzunMG5Il7rhT59/tbyLj7v7tBp
Kaha17IGCeBErgXzHdgi8E5BkJfkfWRfdXopNkZAG4jGBh/fhc37w5kXOR/pDruGLpjQ4n4fYk98
AbpIidmrN+1PooCt5qvyKXGK6ynZYMJxp4UeNCcvgKLGN2Bkw9JTVTVeAcsfjc/xLXX+4BO7C6Bo
2YS0BIcUjQPLwUpyV9IkjtYKQrnzAnSxbNUbkWEgTBYAzkbhuB/fc+DdKDbT0JTqv9mj1kq0IxIH
eV0wC0uYaGQRHxEpONcB8jLd93VkD0ax+2I1d52L7lDNCHyvBr4iCPbNRXJg61VMJHZ6zRCiS4yA
iEH2+t4ROm5L6bwxFdhYaS44EElzv5jHMRSABoc9lBlnYqQh86+5GnkOC3P4sHvwJqEISL/WP0KM
a6rteEx/vHdfFV6lUgOYl0O25RWabJcsNPo/111wiI3834DPWdLvpNQEV+wfNk+iVrK+Tv4ezAVJ
iPk57GRRldmaB9RvsKiz2XwgHaJhG01H8ds1Eqt20SPHSIWYuGBb8UedEeUMpfvXks2LmvxU26+I
XOP3axMZJgdwuZbb0ZD+FXPZ+8fmnl2op47bLJuF4qrn0/i/jRm+rwETUQE07BP/e7/qWl2D2sWL
e1OGind11EpqCyhb8iqw2AIa7tETaNMdSXwP4yFLmSE59gHBI/NyLNCp7poUROKw52EmzpBPRcOo
Loxku6iR1u8GYoPXxjaCGSZVrvSpX3Nd2cKEb1YLqkWSj2u3+mCu68XliyYqjPxe9iWCvOr0Yxfo
tVrfVmiE6ZNSrQA5xnJL5ow0UiqilVnGo2Q0Sk3xC4xAOzaw2FO2AqDH2cV6QMfBQlg7efSrDG2O
SHZdQXLFcniQW5c7Cun+fwtz39EYzcXnRIAZHtARXZ4JTbgeuRVPZVjDLIC+9snUjqZdUU1HJVFR
G8Js+C4shlnRIxFXfzrTE3TyY6lo/AcmfVLgA8BOqOLvtAOqcRYXnRxXGYrgoGyp0jZTCThX3IGy
rZATPECnFCjASz9UnPwEt9Hp4yVstK7S77Wzkltv9JEZae+RbG2fAV/o8ugKbLQPI5vzw+XjQ7vE
Gbe5Y5uzqepr5qjGc5OC4yWwqyKeU3gAPjTxo2VSIvROX6fViKBFszz4AO8LoKcIYvbH+xRKtV0k
L2Kq2SyvqAVLhOyQ/bUNIdoI+14mHfKkg5E1u05vz2yiC8cFUa1Q0VuP88t+S5N9DvYzHntN2jOB
Y+0TY6SpOCJtEdilyFKyyzLyiGJ9PvnwMYXi7jYWlhoKMz7RSQ4tqF4TC4UtQtJ+zeGzAwZ2jWvt
C9JPO7dRCCahfNBMSeVkGlcbemxvdrbm1fioaP2zOzIFmJqXHbqSsk+nqVFgBWzxQLX5xmNxiCNI
bALsbxkqBstIQ7r3gor7ANrEWDkRncKwBHj9q3ofthFCnIcjNuHwPe6sZ7Ha/d84h4+damPbbjnS
yKtkTnMX+N6za0+wXgkyJv6vzmPe81K0V8ElotvR1zfbLk7A5UoxMmqIxEm2yXcam7SBtXRRLjr1
fEbSwbbOMSygfiMIEKrtm+DpK1EG08XvvndQ9OZ1Zlh/IeQvOe56XAdPjM9/JFYXvNcL0oYwjLvr
SQ+Q1LXsU62f12k/sxloGqWxh/X9a+qi34ly0SFz1bqOi+XmTofJ4yVZftO+dPHfHMMRxZ+qDW87
BMCJAw10uWCSIz1RVCOQMKPaFsIyTyZ1fz3LOtlhqWMZY/Y0h3nJhjKUFfIkSial014rKnh/CKrD
nj6eD05BctG19GfRy+vLAmlR4ZiQ9LU6mB+LRuEM9zwliOi/VcmoyhSV9PcLjRZNg6fdCUcASia1
PnyvaUYXIrxvBA3qfxfw46kWDZci1cw27+8QmPPDUai1LibOxcPQQIBMljo6KXxTImVCQZAD5BtZ
7/j0GkN/4PpCx0fb2oTrpklnVXoxrA20PXyJkRoOnvpSWXBUslkbD85qJafndBMLS4EIDosulff1
f8rm0EPRAJRWHZbmSlu3wG9yYfCZSMsrLaPzWXBKOGP78UHDYoGT/vndHgWXdKvQX9hLNQJGohkl
i2x5wSetYs2f2zGrJKkE6FR2h4pn6PdZgEd2E6cqzTasWANVE4vch7Xj/jz8ne1L/8TaQHyIQBFg
Eim4bzNMGqxsxgHbYi5UydKFV3wbqczbx/h6hU6H/3HaUVN0FLuY2lPmdUbuZmsDW2eQ7UercTbY
NaaymYsf+Na1m27C+71Mf6vGBcO6stM0SQrTK305f0Bnvt47ZLhgfTY6vF1wvMHwuh6QuQ6z0cXK
+qIIOhA6yN4fHqbyrtBEBdFKODdIjK10q1Gt+IWd+o/kSyxxxmLCORQCLyfLaXjYwU5zTuXs8DJ3
5qI6Fbu+0j6st/ZUXbCXrQW4ar3cN2zVvHmNoYPbK8ASfcJ0mN1nIdln6WgJmBroH1cGqVXteVDO
/sf7gejsg1LjwWSd+5FuNzMaymlBA+hDyzGLswWJT48VKQ6vJCL0WDbS5tkcKZwPOmCvgbszKnME
JzxbvadcVLdeIs2aF2NQB70h21ARX7aawVoU74xe14JEVVFqAaXU3zS73pZFlQ9lToK7VLIjVVud
kMrMwLTT0aakFhvHAZzJy7+Lcjk06Y4hW8RdmG5zM/4Xr8H14OAagKGHG1n7b+Z3tdSecbir+0Rq
qdPsEpVZbYwKQ254HZrsjIiQL4/v3UJJ9VNTgGR/chQd23ph9DU95j0gmHuB/aaQZxWOuzLHpEe4
KYufzKZpL6iScxD96q0x7ZSYXJ3gDMLDwPKNHAn0AoQH/hGpnGBzkpb+MiGRkzyyqsGyCM6W15yd
lic9sIjK8A3C2GWWkIzTEyfEEqE8qdmSXpNDdHtflEYhEKPCS9oadICccRo0X154y9SXTMpjWwls
AMJXI7RrtmJCAd06rj6vuGyuiswPYVIOK2r76UCihfMyHX469sY2shQA06YoWmKBTikB0cxdIWsf
pW9OqdINUXrnFxj938fWy0RVqUP2SeUqia7ZM/Lhv+2aiVexl6q3Ik5+dBewSLbA/oI+WY0xc8iA
mxh2K+e4lMh6H8S6gaNxLQR62lgjcKiGkb0o+x5q83q0+2Macxq8fNAUg5B0Gvu6g1dPmC0Ao+27
iWzuiK9H69vXPVj/zK57HF70liBPsmV+xzXH5GEtjjPsRJARqkj5zxdVyONqoX7VNHXQF8Fix01E
D4YDbBkX0UMJXYYM19dkddQ3G8cz/Rdc71nTlNdlRW/PzHAtqFFtW1BiUa6p9qnCMnxyT+AzEtF8
61hhBq7APiahPvPPkh7JJDNLsDV8mSS/GYtK0T9PVAx1E/aG/7zNjF50g1myxi3MTgVwfC6UAKqf
oqymskwQWPILZIfQk8+rJ12W05t39TB0LDgNYf+xxfvIoFvrcVtN0zIxWNHaQwCat/pN2EYp98kU
nr6KWMiYJaHoxM1glY4cIiLA7BtiKmQjJ694kgP7PWxZBKB8Ai/THTh1dlQM+5HV+zg6lRT8bS5c
WoUjHTZjoFo9vxc42LxSqAxXRemd1t5bij/2JQwVjjLJsavJBQc349i0L6D3/6f2uUR92fBnipBg
hAskNtMLYElpfLTdf29pEkxgd+wJOCaxCIVcTT0YLnY22BZn7tG6/wf0hDeHGo+VtWxTDaeAF8/u
RWFqvGeV/afOY0t/BUIdb6Il0558RLpkC30AkC/1wVwZuKgnuoCh7qsp931B3Q6oyNg9vzknQMUu
4EoyHPFGUYjobU1BQrvatwNWFwfK2E8dKr2Ydfm1bLK1bot9a9YaboRY2CyibyQwhzvVN/qiol/I
a0So3Mxh4pL5m2FSCVlYYK0G5ZcjeCBV0Rucg/lpSjbZ8M/OSoLG+MnSeTxB3SqgI88f8WTaGJGl
FKPl7KGvU4PK9XOOnwtvC7KUytQWCmY1/LcRA1I3s4Oz+CQelxhP4m3ir6w0iGXatiUbnS3UrYrE
tIdvTvQ47NRW/uGe7/Bc1EeFnsLKAA77VLyWXGcQZUFjzOKSnxv/8FmKrPA++Y5XKf7BH9gHc/pl
B48cgT7p2bmD5/oR6guzTMM0sot37U93/mm+7ONT95jXGmUoN9Ye11SGrjeegEC5Y76tWuaxTswE
hXoM2q8WR0mOV3OeIO6LVZFFkuj/TJTnNnXGGg3n5DVArkYw1zqWF38hD4Xbt8RSlJRBBo7SGlZs
Nb1CZLOCQQVolGLiY3idr9eg43PYnQ7Icd7p+RtUHpgV974ei/e9XdjcR9NX8mvWiYejXiLAwuWl
JTsntKigzLT4SDYaZuoxjO0tak/j9PbW3aMZTX0tt5/HHv3gq2GrHjTX2/Xq2jxRiuYTlBXpell4
TG+t5qWrdFe2/9Q2UUUotlcPCqBYSoxa95Jdcs0g26AnnIOlb3865HuA9shYp8/KVPUUMa4prg0b
3DD581x7+51w5FVxYvJeXYHQI5UWhFil7ejLz3KODKFIIc88cMaUQ3xe8ke6Esnb8hi/eo+TYDAn
mn/xWMIpE9Z9TcrAA6YZabAB3/eHlq3IaKz2mqsZleJpTLpC1ZeI2TTveHUBo49WfZK8Cd9lS22u
Y6I5tsX2gDQuIoWYL1RNpKfy3mKl140N9etlkw4d14homcW4mFVd1vmgd0ChhmUxieG1v+OrvmBL
w6f5Her0zl3mC80YmVTQOHIX4O7wPVR4bmFX82OOF4FdIBcmY/Y0ehpj9+GNij8rrYkKUR1X72Pw
TuXnVjlty5Mk50ksTkxGSPiEXFPbKHeQuX+AFJpGNY/P2p4g/BgvCLpOy8Nv3Wsl+OCDWQFS3AUZ
jwFsHmamJSVGOHBdkKzfn9CuKyVNIL/6UxduHhnaoot/pKlZbOiT77aKWMukUKxcVKZPwL7Vu+AX
/LaJKpYVVnupnEM+USxrVetDCbNcxk60ZvKNWDavk4VRQ/pniPrp2rsw1cq8Y33xjpX9oJs6KrTJ
DZCkMYMKHuAHQ0VI15a4aq49LOIfR1EGlW3yGbYKpeSSINoN73H8GrE+NQGUeDQCP2aBSMHH8CjH
Z9cYUoSk4AE05Su3YJfv7RwWxjnId5rq+lqp0h40JWG/5U57wzJj6Fpl+hK+g5VmceTLZeuVtrtm
SKNv6G9K8eR2ww+KvgdA+Yz6H8wu/lzoEVowznNACWqXeXpJmu5SlFaEme5rAnIA/ITQ95RcoJkd
yI0wIimxyJ+N0HJBIG2Cs2HPwy0H+5G0apTwQ117Skuf2lj2Ho8wPhPwBjbMfOhonL41wSTb6szl
ovsHQIq/BR5FOcP0thn0FXR2hxjxW8DwlPJkMlR6ygBzzIW5h3aHuVLCk+sp7wsVfGm9Vadyerg+
ouBiOQyvbZ8hsWcNcLj8d/9E8BX1vXeoBLluRwNW4rsLeAAwZ+Agp3YN8N0q6u0NkJkPqHyPbgnb
l7iinr/hwqBTCnWAZ2ng1Qp5jeQID22yWD6dz1n4SXc7dW9Q4ULCR2YTn1DRVDt5yO+Nq+EEiSLg
l45/NaU6d+3aaXiqbu/KiTayNgKI6b+Em4N5dCUmLLuvNHi5X9De0BTB2Ccj5GxnLmmDj+qSQp7u
Tz+I7X7llMfZdVikZ4vYSjLrAhejtGWXcNa8RCbXNbzpG0xgZQgtU5Aox7DZhCp3vAVdVKf/Zg/9
9Vy1OvA2g8dWHadJzPlMv4dRBdXfw/evqXF3C3G3pJrmcE+9mrmAUDJX42AdDqA6Ozia3yjXnBgq
NCaNcB8r48AXlQFdwnYzi+/lt0fwuqhGcoPOaJZHv2TDNzxxAE6H6659HzmWuv2dbT+a7fGELkzc
8w+jcnxEHbuXpbFVk/dEeVOhk77nedqgCoXLuG7GGmypdOsViT5FjkS+N+ZIQc8CBndZKLnXYq7/
Fdx2jVNZK2sr2rUUx322V4ilp2DtSD1Ng+oa/C6U/cKSXdc0EV7trseA4fLpMu+RxWpI1zc4NAjV
YKP71IBY/3wqOERMmnw0ZJ+oI80AU/3K+lIidn9v+UeZoGww7Mnf+uIQDb99RzXED/bQDOUiGj4F
wi7Xr0+kNvP/1I7U8LCowG+XSttg9Z0jMSTaNs8ZgKlZ0358xNkIouyRzmFb7gLfMdLtyDybVVL6
w7gseTTgmxPxiCh1pA+WldpLgj4aW0EVeRuWf9vh2Z+QwfKA/IlXM9+qkLarCfTE+zZGKmf+NXFo
tqk5a8dB7lEkAuaV9lZ8txEMjuqp416sX+3KWsmylpkgA47NT9gK0SFxLDLd9TCkCNPzpm8r5XqG
+x8r1/4o8KQXd1Qqd9EL+KkkQwBSNs2yVtHU1C/OQCooxu9rirUQXx/e+I6IbWy3bS6u54RhxdPf
6X3mHquPj2/lozmA+5K77fAkj6jMCkFVy9AlLYmNYNeKbBY2EvTNSO6Fl/LW2a6CC9ybSgDDrIBv
sHDJGVv3/3u+fCfvSEC3kgqP0sIJx/igoIP1Uy0Te9bFjd3AE6tkiDYnt+ne/sjSA/P+gRnXvzkf
KQGqCThpU3ccRrBxaCvtaezs3mrlrVKbLcSHxBivtXlIs4wfQ8X1zSwB8fPWxvGnsByBDISFZA6O
Ukb1m7WxjrdG/yy7rEEsW9lhYuLaND2vF7KA+pFs2fntU/d1/9Muli438cD1Cn9NH2J4hZDfh1nr
KfsTojKFmEdhPo8CcHpgefsZ4NxlsKFU8B+7C13ngYLlKCk48cbYl1tDfhNvFZlgWe/cBo/Fq9dl
+yMb5a4TuOfpwRhNSjv7C2crjlIF+5uHLGxJ1BvP5G7U4SaP9En5CLofOVdVe4DkTwad0fYwbshc
AmFoC32kCTH3T6fkFuA8tQvK/S2VtyjkxqBu/XuC+qq6K6tT3v6KQhjz2sFrcldC5Z5LwcTtNXOi
3ab5XAuiJUxWsfq1Cgd8hctKuwXczf+agW0xx7BK28zDRG3UjRqpS7QCkGXLGjcWzhMP1S2D5pZx
oxsgMHyY6tTBBrA8ZDHbnm5eYM4ZdvDAMXIxnGBoW2LssssJmrZAfJ/FYr3W1vz/w8lsV4FaHObH
5IluYof6aTJ4w/Cx/vnKtct7G2+2KkBAEy66t8liR5Q1rKtQtpSdFZC37PrClk9m4io7c32znubc
OzTNOOsWRi9622h58DVkLPTXLP1fm7B0kG7WaJ6daee4jn4aVg5TCselHrP8V2eliCkauxZab0kG
YDsfisf8A9mjmUQK/PcIC82KIEcaybzio3IyeBuGvILhw+ZpqRKv2Ps1FWHfmMVxrdqi/n/Ispvi
tPlAbiYzsBoFYr8hMNi0A8/kaz6GOXL+npAjrVjLnGb/i/2O0qfDjVgK49AkK33fKZeBpTRjON5B
5+Sphj6le9vjK2+aD/U3lxsqXBIahYFS5sAPQUPFg/0rHiei/1HlXNl1/FKzcXBVVOawcyb59VjK
s+ugVqBPug6c/qkJW5Wp8qwyfUwsxKf24NFhZVjkBb9Al2ETy4w2X0NzIv+w5khzlWBgu8tKXXuK
5eNlN0Ge2hPk0inaA6KtiTPESi0RROuTZaBASfIFw5q78AyFVCZNHmqMv138mq6HOYgvHCI/1TY4
zrTtebHBqMC9KxgLmXuHkeXLo4BiXO3lbXywiJ8ThfRdTkbiALDmKN4TBwV8b0jSaVkc7oseuPYb
UnHhETRyGBwvLZAcCLPmAkpofj0NxzSwPTHoFIL+OvizZn1u5jVAprWMDPeqoU3Fr4OJdcc0Jhqg
a7l1M5rr7dk7mBOkic3r9cCoT7Bh+XsgDpM+q4P7Fg86x603AKyfW13Y0xtv9UTKNtAOhYlG3Sqy
34OzVTe9zPv9lK0wqCyHAp0jnCC/MTNMcZKEMgXIE1WafgeLJQHN3B/0qFZA8cqoIdAUPTfCsOnt
dPFrATdNoJ/OuuQONOX3iJCXx7/WC6ERXbZO4xXDJFSWvx0HMQ/FJCgb9FnRZhz5JcPATvJzqPdx
moUUbKMdxjKJ1iEw/76I+clzzRcZzi6/wfa1RVh1tVRVHKuFG1lATgrULCcrvVofdYvu8xb5YoGD
bnL9fw5Xnvz3yS0NdwLXeMVb7NP8QKYR9KVTZS0Ma969AA5aUbWE8CQKMdAAihwVYfoGR1A4RrMf
WOMoHbJltO3KHh6YNW8eXplqzjqajbfQWMpJMgMX/Gr9tkdIB/RU+1GzZWiprcTQz6MCmrfV9Ivg
KzHfznvCs4zg8pJQVG/Gyt8a7njqhyOuxQHnJV0+akuD8PCvAO/uyu9ZloTZb/kfQtcOnF36j5ZY
ZzrruuIUmd5chCFkd2kevdAjz4qkIz2DhuymDYJfMhqNAHzvp6g00aoph1xLLku5YY9q3L68Vb48
bOUIJE22NdZW/HG+1bQOqBTLXg+xfO7CKsC/J/TMuWWoqGNZ9GM5r6i+bmF7JDt/EBCdvY80Wm0u
xkZHVX0p0f/W1UgqUQ/NntJzCw359bXh3pm07Z1QIW7u8JP/TnBbnbVQX13y7+Sk+3uYIViXZ0Wd
HbtYhc6NzrvNz0x0kTx2XI4wQIwuOqv4GPCX3J4ouLMXNEeaHhxthW2Pym/fA3i4Tgw+cMLE5Lh6
le9KfLU+t/GmW2pxRJFExrwVxWbOejJRtBPtxDyfYgCyPA1WJfRejxWA/8uZYQM4GfdUxW8XHA8N
qAIxgYuOY7pw2EE0Zq0lGfSuK0wXagPWOETwYjOjpdN2FmOddAW0zBO+1ZFgmM8yahIBG/+tSbF+
AihxbtQYZs+q10+1gVJ5vbbFT+IqCcXF53z4x7MWZEv4qJbvi3iQbgdpldmPF2zIPD0U1V1/KeTh
YQPXlC3jzgZs9qTUrpSLjK3ZCmgKglqcnKSDL+8n9vl0lrmuCId9zfJzyv402lFaYoO1/srY2wN/
10vTSCMsH1j7J7l0wZVjmhgGAbPH7c2oc/xJ9oxb9pi6Fe+ZWluaeT/QJ6SXiAxulO8fAG9/0qxb
lg2/XAJrpSCzqWCNcJZB3GX3eX/KL5BddxxSTgKiRp0+36mwblWqrZvurJquPmHOwXIncscNAL3H
Bbnp8twIKsDJy20gtPiw4dtzDbq4FwNjx+7A5l6Q7cpsyRpk/83CyhfXVeGdt4KTb+Hx2AS+J6gy
I+I/nn8rOngcBSHe2wg5UZE7kRMjE2hLvbzoS+l58EkDVy25PkaJVBDS3wx8DrN+zCca8S+bjJiJ
bJEkuP/KrFila1Ih5gJpV+0SSsWNRpgFtJHZX3XSmRyLjgO2bkwxsGTSgRdxD0foG8wfe7LQiDgc
GaS0k5ovFdAAONnFSOPa5I6UYKjjGvS5G8weIODFc0ugegJfjjPTJYS1WojQVTFCnylls7xelwJ3
4l73VhLFuyzcDRdPa2XsgIICKu+q012flik/kk1m8ySkfYW4e7jcVgJkrHAD4uGLcQO4IDpe+WuU
k0vQb88eM80ig26ki6OCBdJ94P5JOnwbhDjvDv6cnflPxRVGkjzG/ZSmYCx5gyXjw1k7iRbHPe6y
BPXjy9LKFyxACIZ0lUUeSYtVeDKJ/brkMKGeUvjBddvfRz4vR/i2Qt6o0/PU+EpsRvkPunl0IyVM
aRcgAek0Mdd5656N7YGnh83Q4fGX3NYhHYrDAiApw+pp18HwlhETvIb5ino0QajrtRpn8akNVYD1
IN9mWD7qC6EJsiWvSRnou6CXKYGcS15FzRYxUwdNNMXs7r6FSCA4UGVUV0iMl1OQSEl02ZnOacsb
Rc31DivqlfQ38qRUDLMLCIFHo4GlSqfEHxYbRCqxsU8Z1khHGX3d4cVHwzLVbIc2VfmQXlsD670F
h//MuvOE7FE8ClJzGNWjl3zyIbBW1AmUlNPJpTVk7Avzp8xwdIgHloQCv5fB40neBMx4Nd/IOeg6
NhDbjal7WylQ8XmNW8UPEGRQqDWYDzup/kkb7BLbM39Gw6vkOLuezNbEXguwPPFS1t/dM+x4cH4g
FtTa+wVXnv3IVvZgBNWX4ZLfK+M0W3pwrEojRiSjrmfINH4LMHOiy38HHsBaxyfABlQfPvDdkzJ7
IqzbtfuXVN2MlVM//4s4P0X0AS9SIQ2QMUHdXHOFzwjiep4k2FoAPKIuADV7oFXHG2YTG6G2D0dW
yqTYKXPTgi+9URyBQ6qGmQLk3ym8fgnHYMgD97h/fzUAPNQ+nr65tkWT5laHNoOWJHvimkVhhz8V
XNZzS9AoOdUMju7UC7WHADEFaBxRx4vPwX8pgPOcIpMx694Ch9zXYjOZotrNjaGEaHFX4aHyIDeH
jrAJikbcNRvCsppYW6B+R7GAIxBxJ4cVfYxWKaNjRunw5PKlWk/5/UGEtBElm0+dTe1cJSjdof+D
UQVhgKfkHckXiKbeuNlGZ/lWZrmJtVHnkWsLxVNYecvWfFFL2lJUYoat+daRtGbIrBHx0/I5Qs/2
YF/ngja04dphIuxxTQjStmcYenXkQCxz2HLn9b1W8JiLdCF4HL7TIa5A1gbqaQtXm/DQ3h9YlI7L
Eu4jHUTr1igv6YTjNJ2M6tdkxb/WRREnEKIByRVSh38iQVU9WZDy8Ii6SCVDt0KzpBnOYOohd7/Q
B50XaVqysnJHSDCr1J/JkEwzPFWsIdoCHtX9CD0aCnhJQADjUCz0inGyqvPZPMmgamqrmBCYAWDo
zxrdO3FOmTVeVeEOGPutRET8dMuwuIB++hBAo8iqRKs3rgjIKMCTuCHkac2vIcluBlexbjpMJtmd
BYkopqwMfzpK7aqgQpHqXOyugvZVGc9CmrqSDK+1z/kK1TfkfIM1OokZUn6hkJGPdFTmvaKvk2BM
CDNxhsjJKXLlNXxTYw/q8blmojAp0PgvmjsXaHBJNQbk33jmiTTfCnNoKBGqTi2AcJMo6+auTHz9
LERL7mD2ntPe1GhwPbspBrora44BDpHy6kob97LIQSpnFWPPMfAwJZv3YHo+CRld3tXq5F412Dy1
9KNZa1YT3ThGU8rSJ+uzYFbumY7Q5DVWMtxerVUqO1/odM+zSASVTIadzMxrHyyfqQUazJ4h3xfl
LJWBp8RiNRf2HXoYFTUNQJD8YXGFM9E9MHnzLEZ/cvN0LdnE8dh8oRMIoyOrigz7phA3p/le5wCk
E1TpLm6j6yFXjfeQQyMUBDQHHWxO9GXw7GF2zyoce++CbV9vXkuqEHb8Q7DxTkewaPQoPD6KZmVY
4gxJBxtYJPtjaPPKbkogVRBVOh63B2mqCBLqu5DMIc5y3H7tivOio4BJqC9i+OTmBM8FMg6Q9Jyh
8e3RYw/tOdV9cf6xWIlW9QUTjKQRSjmuLez240pSky8DBSMkn6v2FRIfZMfHWLYReqm3jbageUdS
vTtRtHvaXrKHg1Ct+Rg8NTHkRcnJcncX6MtVp97pblk+sKNCbNUDvuhCHVHrbFBc1y0/UKgPBYQi
VNkHYAW3L/g6TcDRgIqdnsOA7EUy4rszOkKv578xapUytf1/4kDW7GBYCVEM4PwTuBUVKssAVNAf
shFg9hvsVk2cSw2B92l93l0KPSrQMvPFJbnixP7tCbWAom4mNoldMB1LFZCaCVSEJMC7r1Q8gfnL
CiSSG1eX0Hf1O3Gf5LUX0a/dPm5FkpPa08i8LXLynCWDDvYTsGcRXaIBOQHJd2LOUKMagtOwGdaY
oP+KKqcSKvtmjC3rjf88JX5YoKdDdXhGQnDQryH331ghSPEmP4kFL89UxHzSXaZ6VWBONrhVDuS3
Sf92V0fVm0uYjfuU8mKuxyMGRzGgiIYzidI08/wtudNA6Rv8jS0UzHSLndUjHqK7acXPEX1iRFqx
F3hWKkso2GfT0Uvk40d5csvRe1CLzrlTsTg19QQ/23uJof55UoXujFE744BFl+QQtr5qJnm6ZRXJ
JfCGkkz0JRANOyYZY3+jXv8HnhXvjWC3S3uXlfiNvNARyEkOJvupLSDq3/lG+cIYl7qJdzQttHga
qW/T8mm2yyv/121m1JwV4X1b7heT4l/orA+pZEhiwTn7laIky2y2gWgjzNGjOOV0YojuTK24shP+
/jcepVkrCtB1PDI0nYbLoCNmLM+MK5GeobakKB8xkCgo7GIXicW2ro81znPRdOYwkpyCgVifbx/y
UvSBlWP/849xIgaCm03zHL91mTXvRe22tM+i33dsmyNJkWnIMn4dkVT9pgKxE5CjgR1OFx8nw4V2
K20fBE22oJBiRtt1GtXCfoCoHZOiWekLEEk6VbZbYLc6oRksB7bkPdl0D5M/jn4Ft98Em8u6lrZc
ao7f+h6hAVR64gehALNkhEm2BPBSCOt9Bi5tIvZ/bY00YA9JzYB3CxZ0vs6VkqdKBb4IoaxjrjnM
3Rnpo6+harXzyiaKHYqEZVK5I2onVgLfQKYYT+gubZ12v/RZcMHNXNpgp7I6q2FGqxYpeRF35756
1RnO1UbhMYqDYWuWh9ywng+G0v5v1SKkBBE8TwAhsH6uzOu9ojAmEZD1feXp7rlnjBSNioMviEwh
gERuTOVXUKl48jv8pZSDnzVtCzGyKP+VRWjw5IAEx15XsHW+HqTIPszW7mdMjKyiR3pjFeB5T9l5
6n++NQMWlPDtR3TVw9Ma3ldVAwBFXrqU9afZHFsB7MoJ1OVeYdWo1knNCBw3ob71Dq4w9yOngMYK
HRtjt2bTHdawevnj0/iZlX2zndJysE7vlP7K2rXZkjSInCJTRSurdO6AGY3hpgjJihMCL6wqh7oo
W9+uw5EQ5BnyJjeznyiPrtYmpsU2NSOLg1ElSTznsPGdiSmy3Lpg+6Wzr7No+Kf/mGiDzXmISooS
guOKId82P1Y0Z1vET2JI2N7szDiNgfrvN3EHLzn16KOyIFrqmAxajuWxtklcSBP0PXnWfg38O3je
EFwLqb1wJQEiGlNU4cwPq9DjdrlD4uHMWsOVq9LouxYbYPq39EogJbcQBPTxK6/cm537N5HBDThc
n/eapjuQZwnA8sZdNgWpC4g3GnCoFLc29tdDUZ3Mr8WyZh5flw5e8rb/bCjrlzT3Co/UvCkZrTkF
j/JylDiYuv/pGUMcXsyNfEkDWTuWsrxlEXiERpZwOQjUoAqjVNSNzeaoZgGMDtpbyFnkxEVzBXC/
BMHnLmu82EXez77dawponOYqRivy5SftyBIwLilGDwQ/9GOpS7+j3ByOYJLDtbZ0oT8BvSjJBUTZ
DKdz2zbWmwLtLdbqhYwbfc2Aei+Ktwhnr42i9YAniAtXK0QY4H5nWHaNrgswv8ZydUZ7f5lhZ0ke
mIXLm6HxiwZONwCGlRC/eZl+7ZeJHbgtrVn1Py8CMnmaKbH/R9us4I3qkGaCjz8VfiQQw1Q43ljh
XuBiq80A+rgKKnscu6LPCEz1pP/r3Oc38MvJqBqMdbXFeyf2SPdUI7keSSPniHjgHtABePcl+c2I
JD5a5LUhiXgf/RbPgwUyFAq4SDxOr2ExvgNHBFe65RS2YvT7OEWSWXhYFXzpZOyz5GTZn2omIAba
oQKlLYRnq/NjgMrUY9h/x1vugk//TNx2ARwn2Z3jjm3yQpn18ExbJXxKqwi4LLkYfc6MQk8Q6YTS
cPdKvQSLC71XES0jtoHADOSEOH2QMpM0pFbgmZOQwiyoxHiv82OgbXi+x9Kanh4TY8ScE957FtKc
oxgAUZnsViteUoxpt244YiThPq2/CKxTKFDcudz3Shbjjm+pRvgIRcxoEmsgDJWRD1Df7oUy3xbe
I6YpgRxn4Yk8n0da11PjtGGhA90bvzgFeYuzFU37ve7UerBXQKgyGrW0TivQD5rp4ylN8t6jU2yD
4mzFV7xg6ZR9CfGrVTiSvoTSr1z4i5mIHLWLsv/9spuf2Vxy4D2k1j6wgAxz/YkWxJWYg6GVjqKF
DmHDHtutlsCCNFhPAcGfQe5tYzy4+T1ZQ9OT5ZShgO8MLI+HjTTc071jsE6VYtKZtofS1Nfvm14Y
MFnCT5sHgyPpDo+/dUFh6hGRVrZClNfUJLxP0lIMTim55tw6RRQkjqWnAtfUbblCnDnuGp96uhNQ
sgiUuGAX5IeTwQBp3ZwJUeYy2Yru6hsMTedsMZAEfyKkvURL7U8bK1sd3qpOKP+/4Wuk2fVOQaSO
xONc9LHfJjQHbHD9vmk7dQ/yVTRDzEZw0zJ8zfJUoBS4xxt+J59eBiwUfOl24hdfMjTJ3srFQMc5
XpRftBb7nGMsmRd87LVcCc5yrnNOl6esy4ulvYQCvLzoSG563vScwiYylOE4vViUAENnk7zCSL7t
7kz6rVsAc+/Ib3Hvbg7aH+v939EcBluALovlTjn9aaMryIGRDL3jgtTX/tNfGulA2tGdO5xXKgJv
CXTc/yU21IZ7sPSnS87s3xF+0g+tuVSK36Xs/16C4+9Q2B5eK9kkrhz+R9WLqhL2uCpWzmoy/CRL
VfFxg71pk/Ol0BD1qgLhAfpSqbnRWyAsDKlbBjlYlbjKziLyPt6Uvbd4NiUbTFDaOHexuqHy9EFV
Wa9/+M9GgHAVJMLBLwHBX/TVKwJT995efXCiC81oqCRpDFCZv9eQ0AFNXd5g0QsoKfLW5pC1FRG3
qWZpeo/bmfE98AYAPDVQf5/Pb+EkY6sl9C1zDaK7wjdhJRVlSeSGtZMKLUNGnV1cwllqui9W4/ti
5rP27DHLvgMlJ4Y7ai9vDczCAVJZi2z3dwYOxipxmB122kINDAbXWBCpckWadO7AVsLzNdNAJ4eh
XXmGi8W0A3TUTyP68ZU0dBnt5EuowE4Wy5/dn7nCAR2VKEp8BUFHUvT5OdHFGhwoFllHnYxiE501
URogwGQyKy6P1ZhaGGDc7N9YbQuMIziYSVjye/ScT0bg1Cf6M8CyT+iKRZ/RzbYWhzaOsI1OrN+D
EnptA4QdVrQIlGbHOOJfFHAcRqpPy4G6RflQ8ScCDXLlR/p8Db2KWinWUMI19KtYqkGzI0VjWFDv
sNMaZKI4N5jRL80JDH8/imNismvvbR45n1/eeFKvJ0VUTy2zOeMr/K+FS7nZLqqF+Rc5FwyDptYZ
q2iQeIM3iJb7BXiK86ZwgYKnpqtof/kpRtaHBXQ/fzW/0vYlqzhQEFbQlUF0JfMthnvL0Mug7bjS
0DKGXY7tcg6smsjUpaVgLaDZWugCsVIT/H3gq7upiUHC3zzA4TIITebcK7hA3FecnZW6Oq7wG6vZ
MwTLbXz4fRKg1IXgM2mAbYREPugQZTE1apXdT/agFKPpdxMxGoZiHgW20lQ4DxCDBWdd9y4SPunQ
zo0DQoPg+TUv239h4jW2bC1L6ZYz0ggEaVs6eX1aGbNXB1DucaNC6nloV0AbF6P4f2oKjT7Ax2tS
FzL2cWY7i8BtShgFE6V4itvEuB1lkv7tjfBHiOOQG6Qtg1D6iLH6M4EQk/ui5/OsgJruHNrqn5pE
+YV517g1guUj3mXtuCpzkyS9fzXrLkuK3fx2wKhpzHPtX8StcxoNlRRcV/aiCQXZHiqyAeFfQc5K
q3KnX6ymrfOpRv/8Rhhw2Y49ADn1pBH+M4DLl1uWY3IJwXeVTdPsYBbuYX65PJSsCWA+ilXbmcs4
KWjdlAU8x6kqsZ3dFq0FDxXdZKxzWFVnelPbtgrIOPTUSIzVFc4Xz3pKp6IJx1WiotBFkS7Bq+Qi
SyVaBcg1OvQ0KoyaNWbM8EDyOIUnm6Xjnblqc3qTOIVxT2vN04GCPYGl9tG5i7W5rZ9nthiv1C4z
POgoRKOFE/zJbIBHjhxoJ0TF13we9o7HLahOdnnp7xAXRcOm4UX08V73VYVSIljaCYe9tyT9mzYm
1o7dbQOqjdSEDkmkyE5kyIH7VNo48NRr4Sf7wkdUSPOOzijvbMUJtlaz1fw02eLRIhyzrWjVleDj
cq+haYlfirnsFF8IhE2fSsGptg/+wjkVi7gTC2aSSYBaypf+VVCKyiUZJgcA+TumSfL2Kx7Ay+AR
mNT1o3NxavYy6iQMMUlGiEPEAq7VqbOwA+SM0653RxhvTpwn8hXqOjSPweMC7MzruNqGzOgGG4mG
mTng8rH0nCvm2DmHhit2/jDFT91ARrCCAKD7Lz145gnR1pL/wt9LXUSzgKWHqcelNC4sL1XdGF1+
eWmQQDEm4mYp3aqhoBiyM7m0TvokebPMEe9pMdHp2Hg7qAItW5pYIV8lHhOc4KXSNQTMx16Rb1DC
fJQYFrU9/uwcVqafXVGW46gmD9d/I03XcwXzeuBB4HMbLiskSyoktr5foREyxJHXb0ZSns4DhPEs
NDUtoM287emG4gG7k1mTYl3I1ZoyZCuIs/445euqii1xMreisA2mqvKPM1burs5Cx/NSIXZhKCNw
ADc6jBG3sMhpu2RGbCkFL2ZlS+dlDU39ZLajVs7sHmIB2tnnFcXL6rQNc/7gyUJMh9cyILfGPJaT
paC1ZqnBxUGb4afthp6I/8Y+bfuiM6b8qVXNMQWsoe4sCWid2yXxoSFyTVC0bqWaZKXTbvwzYEQZ
oEGVOMLle4MKnCl0UA09gZQspkXf8LAaSgwdnVchW8zG8pkfomQP9dMD3zUy6jE+bj9rLrObxb6D
1V4fHLvAXaoTHfFsfhWtaVqFob04PN4/tdQdUozWaUAak1ouWejX9FVd4452AR0H+LTLHMbHKLeG
X/BaiMVg5buCK+6IK27i1zRpvZ2/140CuRGdKe27KNJyZVgUaCxuZgN3YLphDpUrYPNWb6pUI8fr
287aGmKC5Cp/ecJW3NNywtsP/w2vqg/vwhuFo1KphbxM1BfW3CuVnQMF9ihOtsJOK+0LUoFDsMQR
b8A4kITqQqGZczlOMPmJ5ZXMAcytSYU4Bw8hCIKdipB5uNRVQ36lHA8Uriah2ahsJVjgeQoGXpla
39sj23IUwdL79ecLNgYWElLIOU+JadIwsCHcb/YVyhWkaHALGoRt3eVluQPcGX6hboX2hjq2vscJ
ONXmqGn7GyiwmBBxXymR8wx6tXfd4tml5GZk5Qy2FphWP6/NcbZ3fUaTq5rOpyVbrRIsqjI0Uh9s
viwRmtzucKxogkB8va9Z33UJNhZ5M8Yp6S1Qryc3z5BWG0v8wBGLmgqEFsCSM9eRU44WT/CNvHod
ETj9YQhW8B0HCkr1D+XMEVbxkkvyhYNcjdzbw0htycnV36B8ehrUrgB3LxucvF3nnsdbihkFm1ii
xCzk7hTz8f+AzhDlKqPX5834Rj34HV4kAetIrvWPbnbN6SVQQ7DWalBVEJ8l0nDJ0/5JRLBlTRLx
wZOD5lESb0QhblHcgi94grgYVB3k0J5EoaiIq2u7EGzyG1Ax73TlsieK6s+2/oaHEVY0k4UeI2aW
lsj9eXpkrqPE/PRAau4D5puWk7IN6lyMQGCR+ZRWmSAylLMws/Vz1RPW9GCHEgf2QUSF7FPspfYL
UVcCPueCH/5ROTBmAHRK5jLxcM7AW/QM4OC/pqjpQrIItIzmn5uL5GTFCr4BOqdVlq4Ym19bo7US
IArWU+APCzNq0/VFDsqq+nRXqns/u60fwwZ94jAcw/viveIs365LZ+CDarY1NBEJyP304nzuvHJ+
WNAdLEFnr38Xl54OSX1mh2BK4QfYALETUcxRHKXHxiSQYfHcya1KbS2w+8ZHTnJoSgMJIV+P/XjW
hX/W/9psS8BJq1cXoGygsl0WdMrL+gVW6PPwXVZFuN9fx0WrdetXcIqxelz+xJZc9vn129eB2oSx
//ebUFaqdijbVnD7o/xqAS5e2Qgx5gWByeWJfmx13DRAdwP5E239gWIIrhJBv94cow6EJa8lgv8B
VYr2RpcKILCCIMKCHV+hy9MHF3L57vXSCa2ZbDbNy0q2vVjLKuTcEyDD4arDPEv6/ynUz/lZ2myb
C0a0WshxPoHxkat1gvaK2kK8+l6u67XSn1RjTuMFnSWq9zAsU37vTjmyMnylLoPAe1D0FoNC3saT
HK6TXf6vrHPJ03wKDxJMX02Lj8oRSlO0FaW6oQ7tkqAcIW4f53N/ah+mMPxMIrQPh6aaJIDqpvud
fgSE4ym4K0oF0Qgjju+GslDE7Z9zdOnD7fkalgxLaPNEz5qlgXbGLp6BiKTIeppywK3yx4AwtTXB
F0nLO5OJpxKo7OYEtbYgctiEJnz45kTM1cuuJrXePtWCMuR0jGvn5BCfYab7JGyPm/BdTcxairAK
6z8l0YyFTZnhV4IfoQLcrGttBvVu+Aet8RQ5G88r/rQfmUcne8gFlBSfTBLoU6/nBnuPyM2qeI0r
raVF4XsHj0DmDI3/4xucLnTqx2NgAUAIFWZlBkACPPDAjXLGa7uf4CqxsBTo54O5Cre1JlVBeKdZ
sTlIF5aVVz0CgAN2JLf9CUlYoAQ+XEqnW7wkRgFgsZcnq89bh1lO5KpuVeKJHsh5UYVmC3Ap9bqD
E7DulZ4V5deRMcb2Pj0NWy0cA3S4fUqhEjF2MSI7RkS1KnTr1f5VLTM+b4z/SaEGL6D83vZIhg0K
xb1VB4YqSQn/aTCI+XG3Pr/Xjq2OWrwz1rGI58uDAx2qJErwXP2JUjBHAttY01Iopx3h6jw+wdvn
FryI15Z/xWGVvJeg2Kh+EkHJDODBVSe4Cpa8pbr0x5NVrLwCJR6V0lqRFPwqW/3EunuecrAwC2s4
MI7nVqbsK7RlFYIgnFm5vckXSn6rLp9Atw4W+2pMNgkkdxECeXJi+qIU4QvxBOogh+Nvt6juhQh6
Gvj1E/gPwI7qNlyqwFvW7BEjbr0iiL/9Q5d5S9rHG8sh5YsOtSpw9JyHKWrz5R01iKpvleLXfxnr
fl0WoTkk+EHEN71rVMb23cF88VWXzrtNmlzX2+1UEgP5j/sOspKbNIglx1IvkeuxiLJkIVHLaXrL
6c1G7ub0p8UZfXShcyYkaTLWvkFINTeazpCrogMFUnD/N7JQ1YM0EeOY/dTRB+WDji3J8YN1tKvy
LAXxTjB4exO+DZUvBEsTJVSYNgMyN1rY4GoQkiBdIoFQ9gif/z7XnrYLKnKy3mMclnNtgaizjjIS
t+xdUbzgto4i1SGkG2rJzLJRiPyVFESW2ZCH4yTNbGUeZ8prhoc00mFQTFmVI1qyKoNYZ0TptveS
EoRyMIVid7nGC5lui1oVcR3os5+1xYDhNjxmyQ4Nvme6JgXaq9Dn9xVT6JJY+QYkyKWZpZewf7no
5p69O89DjSVy+tSQ40bDnS6EYRQO1k6KCwIECKtgaB6IFrblEJ90c1rZD6032ozV91SiXUgFWMJZ
wjlKKTWPX7H6iQCZyKSyXNd+kfXwjmPEV1cBrYZBBHmM6t9V8OJdoUXghq9flH5r6XktUttOb0I/
0fOl177r8TpYttnY3lv0q7wcPKQ0B0oJ8PeZEP8zD/4zGfCchuf1wbcH+63BkpAkFJBvwRAzajq1
Ue/UqBP71djpop84cx11u7H4r0cyctjOZ10yUX8gd1veFYSlVKN8H26G6n9ZF7E3FkHoC3ttwfRr
+d0tW6J6Wmoz+XcgxJom7snielbip6Kmnm904jWFL2q0huXJ9a3+oApTNmOGWxG0RSnqeoj+aSHM
ncSEG5IPPLK6SJzzI4dd3moQohG7B5WiqSRuKNahpXmJdFFA5Vrbos6tyStnwufuDYNq5EpQt0eV
189Z9ZSFB5+V5M16ruZiH2Dwc+ot8h62FbRA1UqAhpmkjXbImcxDIZ8QRjBL7pJ/TqUeQWHxl/n+
BmVCIxiGATSBicpAyDHjD73VDQa8/S5DPdSWURur5EIOUDVtB4RZGXwjJSyzPvDX3i3x8cM8bAWx
H3w3d0WlfwAfj9cLEmbbNUgZK3NpOkbkmxK31Mnyammdt1Qtzj5G6levmgK1O/x5YOpoQMtfKPDL
8CmOLfcEibbrKd49DXhKG3KNK1A+pDgSdbVpcYX5AHjfxN0y/oJkgPGun/PWmYKya3S3gDkOEYTY
eb9QdaGQfNRhnu7DE9jl3LUW5YoaNH+dV0xm3ozbnsmkeYgqDAJsD0h7wz5fQU+6c/BQf1jPQmPN
AqxeEcmbfVm2S7vfZlGaCGfhU67p7FKXTYjbK48YzRUmsDPHR5/lFxxq5VSbYqQY0GoV5UYfOalx
ACgzNxU5gnSAXriGlqgttRKqj+YR6lUG2V6VvKZYETlsfa/DqpR7CRwqlhV6EDLuW1zSMTlGI+Jg
U6GJH6LH8bquBxYvn1ZyTS6Kove4UhoJJruP5Nb8NqsRA/8azpyDBYIKC4DUCpQk0cAGznjuz6K1
k0I/EOE1Nhb8ZIkIaKQkDUAGnlt0QG1NK6u1e1iARSUX8sK+5A7D4YFBbxorl72Y+IxyS0wxYsLu
71ryLZxPP/P4yGb4z/lImvzatfzzcgxwuAelJ4WWEzRXgnUA+NpFPp0oBDNqW80yZg0qj58FaRIf
jVsrvjQC7PIburs1bVEkXJhIJkR26+iMTrMwPf98uyCE9wC6Seo9jGdZ5jdO0oT8DJDZMIsI20BT
vKrggSwVZ69JGd4cXhsv9QzF7t0qTQZerWskdJNDlLJmrduHpn6Rzrv57KmBWdNryj79rCbkEbQJ
cQ+9pTpnIiWZYRURmrVOesOWGR3QCoy+iWo2gQwTi6Lw30ePMFJidZBoHwIHoV9Rx0pzjCPQQt0R
RBESMrjygTAB4m021r5PiHMGIIMxcUf3JFkQS3CYu0F8MzRcVKEJ+0lnspzD56pe43ujQ9C7zczG
HLKZ3cQDEkL8hSSIHJflgFE0gMVVytqBgPgW2+8ROaAMh3AoGlxwLLI8cBYTp3qJWLBmnoqXIYGK
BAb6qsg7GpM5Sc0PZhqAfvHlRS9tTkGW+1grtz4ake/wVS0Wy/K1vhtYI95hPyS/IdEkFqSYEVSM
ICKDoJyLf6TU2WN1ZGpbuSrX/E6grIkwugIr3CXsNQJYo3fA900IuGnN3VGTkam/UkkAnJ0C+aqu
S3Vtf7KnN9Fx5TfnXqXeP/kDKRW1sy0ieqg5fg/5eirz4zKLp5mVRpJ9doVGo6dy71qE/RC57DP0
aCArpS5sCgbRWDhMJ7rZYYwp+wxKl0O279JmbDaiv9swEW7iMis/L9yOMfv914fyCAvjEQvjiKr5
kbmDcA6gKI08oujgYtF07ev3Sd1rLEnzdnyVniZNc0usVHNEnb5aKloD6CO3kMyK/0oWwAphbsPw
zNyOKxSOSPw3+4eKOe00VD5RZaQfD///lKIHpSREOK+z3KkERese7jyGvMtyTEgnqYo1bB2CEBlC
nqPh6GnIO2bGjW4Zs0HQbrSL11QAbBXEeuLwjJstzl2jXEKV/a/3aQMP/myEY3N6PxPLs16tgDuK
1RNIgqPPoTxOLpNethYVLlgUk5lpaQ5JdcaVc0sGpRK06bGHHOnpuJNIcGMk4KR7HrASuDJ9IC1F
QbmT/OGtIy32KJqHfAj71UJm3Zj36S2k2mf8NG35gJ7APjASODodEDufhy2zSgSk0BxkUgsdD7bV
Fd91ZpqP1gAATcWyq5P2SAfRyS9wyHN/uuQtVfI6TqNsjIXulHjV8XzWEt17GXDXjFFIo5+eZcAY
sVQlPC1W94XKNHUorrwgbo+y8KelcD9tFzsG4NxyBk5BXiJc8Reoj8JLOeyRD1rYkVEHnQI5/6ia
hFUO3O7mlSzdqVCZ86fXFC//PnYhu2iD4FQ1HMSQUa6O2KZguroASb7TMhgsHGQEg/OYvh2Rq1Pm
rHJCMrPxQ32NhFTcHhVjfWQi8VAlMPOn+tkyY63hKCFoKSF5OXE9iKU7McG9t/pBql8G0kCktcQI
+qS7UqTnyngh9x5PYQ8+4G3A5jAdKpi6u6EvCiC9glSeBgmuUaXzokfPaZQRsgINOR7urSdwmWfE
yayZcE/B7OuYdDAuYch5LVKXuwGrSGEA7yCNmhnE8r2iDpHKQJH/yIkvZIf3D8tYzmaLXOxpLwle
uMgfo5IGyeftvaoAQsdbp5IaPh7e4oxrbsb6fD7GI6oS+PtMaXG5HpaHj+7t+VmjTIUgy3QGQr4f
2SigFVSSd75E1XuhrVJo1xGUYLb27sVm4gAL6OULHW2Dgf26DPykD2VXkVbtMEzjSQ32N06b5ymJ
eQeamWr+xEJam4/SaCn40CX774vdaP8xHCDwTpV18A2b9Xo82HG3/fSbVB8DJwFHfHv9iNwQ6gIP
/KHzuByr0nUZIFVcEBvcHpi0MNyd5Vk9sIln68ZjEyGtlHU1wVj/UnTCfpFK3SOKRXR++P82kBOZ
+P+r5vcf3m6c7prMUwACXp6aI6H7tX4vvDFygVi2JL1q1nQq74iMlM40v0/wNRI753BkpoRkJD2g
vWZYfqUzCPZHveXSSmnP7JgGSO3Mq1bEU+0JGCAGaePpnReaXtyGl+W8nlRb7DFn4dZD6s+rAvVc
KixVJzLhgfHrsRXuVb8ZvZ7FaMlg2tzAGLJY4qqPR2175Av6X673QFCMjfA1fgBtigMHMg2HTOUe
4qqiDeEKUC2yXnBCU+L4eFM69pln5acEuoQpcvs7HmBhUEreJGPZUXfbRI0HFa7+0be70SYG0Z0j
xv69w7Xa/1Lx/RPO3nqbT62gOhxzRh3k0iSglOALLNIqDr6Rr6zHeY6Kyq7asfhezcnLsCIeBbwU
Ryge1ByToruxxtylfEJqEvaSSgNJLvE7xy+PFExxdSE2I2iJ2fWcoeL6wcTf77F81i1iyQ5tD313
hVCpy+mctnTZi5YIOB0Z0AwJYBDbdAhXyG4XJYawINkFvsikitQOZaoToUTFd5Vzv009AxdtLWhJ
pBWkYvdGUR8fTpJt+g9k39TGY6xOas95+HwCj0bd4oxZNnqXPUvhiTvMBca4oKdx3QB37BPCgC2a
n6/70RjxTZ1nexuDzguRIdY/42cFZ5Ptlq98Hr2IO5M9rqAiEegVjdcSnZe8twv5Von0zy6Ra1/U
3EAiq4SZ1e7wHEG2iSvQuj6rK2p6huNBiAWXOW9jkIcUZ7ZsXXdn6mzhjAyrrsshMNFOHPHroE/i
A29FHrYbT1+9fsF1gsxkeCmmkpC65Q8yXLMx93nijmLQxY36HXDcPIp9e3LTWX1g8tqc5hkHXF4V
ASRIEw0BpmDsG/T0x5+cn2+nrWPTBS0aFLHDqOLectbwlIi7IUCcbI0wAFu9eaAIy8BkmtYWHI17
LpiaA61LbqHZZcy8HstZAJjmhSH4cdI1vCExdy0bhf+RWrzi+lblGXAPueXe5gSxDLcmh5M96NvH
WD+szbMe4FoKGEbaFsCdLDE8ZjFGPpkbJwJnFz8cqDe+GMzgr0zYQkzcdKJ42uBcm3aVglsCAgWy
ys3jn6vuffJ5ScyOkvN2/nfiSDweb+eGJu8SUxeK6pOpfA5ZNo9C1DJ0N+RSnphbngMpj0ryBjcZ
Jecdnoq6Y67SvvRCUPStGMQIDQCeJDytqxyTzKhJCWUSatfOvjMPvepX4tGBvsUAzJfajEwydHM2
7ckxjxonxab03Pjxka2/mzg50BLU0lyJM22ZqlZROqq7ygkkUHOoZWpOmpgfJLEtMr/LiRylzC6i
SsQVXnxhYVncvlVRlh+d5AC6Qyq5D7TPx+6//WSFfdSruY8uR07z+PTRlMHx2E+X2dgNghPO1I3C
3BKn26aiha3MiuUn9o/VHkwKCPwnzupM0eh3Rv4DT/9BWmuOS3tYU59PIgN4Ry95hnhFaBDZQG0l
mJ20M0SjpD4M34h02cK3MVyWYpXYhhxKJ7b9h0ptWfnvCI1pG3Phv1hF4OJZOgCaVtJXYH/aBDgo
7pIIY6CYzmdaKnhR8/TD6QG0n+OkW2QuhwwQpcn+r7a/8gtloABbcJ6UfqF3JOCwp5ANAwaT6fcT
fLzX9IdILSHx9ME0wS+PmInwXQHzsvmwWicjOSzQXvoIJbqZLzHzRk4uNDaiLoHGbxwPwuph3Vp/
DVzo3Ydu0W1EnGZEz9tP2ZHM4MsOFKxqXsnCWatwuS3VxQ5xH7vD6hd7CDH2bEDAzUkz7eEplyDj
LEnXySLui3bF1uRQhClNGYaNO0SnIiXijXeakMKpDEAUqA+WngbI+X1PWBk7piwOUdalzjqtmZYP
+soIZEKc2ezZKZvCuQZ5rgSHHhNuWbrYzA3MR91UMEbFl13uXmEE/WoVH4VWdGTapDpJMyQBq6+d
FIXVr7MUE9rPPuZxq704o4TIakMDAUlWyMS8UIitP7uU20HqjyZ5Z6pGw/RoAJRPFvyPmLn6NUAq
43vt0RSxxy4AFrLsqhLCDddgDtHC5Pyq2TYzpINKrwNAUnp+S+LX0Y2e7va3T1Oe7mXPxARfhS7n
hsPSb2uY5IrL8NWXUSOkAdnAgnRFV6JqXMpGtyqVUtnhInJsgR2q80Pu73qSPhUJyWrlj3LgKy//
GCz4pTlRVXg3K19GKCFFkFSzQxWr8+35If6uONrijiX91YZ4YwNfJwOrb/qkeHRYxn79zgUJyyJC
OcsB2CqqEE6SpEDq5v9IWn+xFWcwPaHt4+Ns1pI3H+DmiPRYhFAJ6Q3v7QbP5EQwVlyWuHwB/oJO
zWzu3bDlykteajfqaBYZbR91a6MGgfsTdr6EfJ7T4Gehb6dmo79P22jin5otCDajHofUafzT0nWQ
+8mUkrKMVjQyL8cg6jXcGpbkTSjKHfst1uhrEem+qW6H6AAxRHXN6h8AHNSSc+D0Nj027Ul4z0Uw
aTjUCymSLhZu3Avpnr4Tzj1yVu9h7oNjWfO9Vn70dlWYXb9v0i9DQc2hUp/tC8rAK4ceVHUFgOsA
RR7u7nnEt0JdrCgSK3+knf4Iw1QU9V8CAkal5ZciSqko8stpgISPRhCsUmyCzSBUKFMRj2TEcLHp
BNKjMGWI8ZRwkRPyBRPPUN6pX+NsVyyP2Z2gXR4ccIOnTh2QsUbUKDqxDe/AsODHqI7aE4tyFB35
5r5/bQVsKRRucvW8jLjq+O1SgWMtz5CPjVdcl1UzijrcRPcPrtRW9V9SC033YmavHYwtmTziBJ8l
axYXrkIlLou3wXpdXdseKAFgp7oCI1VgcGqbDLEbdJTO3K/KeZzpGMPPPMAIGUAe/Yb/G4e/PYPe
008rGYa6uEIMuw2Hncj7eqvAX662HYy/XvBXiHLCgTWPcbxIncCS1dxhWgGIdTpUfuVig5XRaIJc
Zb5wxB+PvvFPw7q0ye71d1o5fdv+dgWyaWl6y3QC8NLzPWc0sfURtXoFi21OCP3qtCXIQz5YZ0C1
nJuivrM7oH5FPceCBSCDTiJpPiHpRZxzHKwZ2VeeI+nPH+uD6bw/dqm4j6noVx5xq1xfzxpM/K0p
1gDJmDQLixZpPqvmy/MQWx+EplqzCQuztdjrM3dHxULUURDgwhHxuqWGNlZZbiorg4qgTeJSSbz3
9BIq2bkxoDdDPFG9m2p9UKjpfwrrjlzHG/cuIWQH8OrGinq2UC6GZpSrI1hLATaftW3naTlMEbiT
BtnjD4C50EKaW+jZ1VQXxNUVPG8ARzbPTRMJVcYbw5o3JerGvRWQBPToVrdDVREPpxf9oF8m7fhD
ODYW3H3Ghy9LdXnPwDeci3jXkAAt1ovcg2nCE0OgJ1iteCPdJY35noCkA4zJTIRD/BUm38VZlHQc
Yv5J89OfkZEMRTjHd+DHHRz0J7RVGm9xaWREbjlavb+95XAdF/ee1xTFZi9LVeBUH+P06KJyZw4S
67eMCQjUhRc0G2WLyNvYNv68Ne8fU4pZ46L+mwt8Nb5ASDigsSslUHQ00SjNCvouMXgpmrYnV1gE
/5ddloIP+4F/VqAYVG2lgklZCf+uB4fdgB3i8YV2Y90zQDcA3yFjEnb18UtW0nn3ROpvEKlTdQpb
iau2rExbMjOk
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
