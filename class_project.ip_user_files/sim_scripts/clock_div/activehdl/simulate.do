onbreak {quit -force}
onerror {quit -force}

asim +access +r +m+clock_div -L xpm -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.clock_div xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {clock_div.udo}

run -all

endsim

quit -force
