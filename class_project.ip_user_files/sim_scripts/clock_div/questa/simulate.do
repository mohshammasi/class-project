onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib clock_div_opt

do {wave.do}

view wave
view structure
view signals

do {clock_div.udo}

run -all

quit -force
