onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib ghost_left_mem_opt

do {wave.do}

view wave
view structure
view signals

do {ghost_left_mem.udo}

run -all

quit -force
