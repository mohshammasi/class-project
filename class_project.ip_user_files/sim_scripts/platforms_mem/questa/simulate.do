onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib platforms_mem_opt

do {wave.do}

view wave
view structure
view signals

do {platforms_mem.udo}

run -all

quit -force
