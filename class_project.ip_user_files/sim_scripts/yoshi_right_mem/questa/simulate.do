onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib yoshi_right_mem_opt

do {wave.do}

view wave
view structure
view signals

do {yoshi_right_mem.udo}

run -all

quit -force
