#
# Vivado(TM)
# htr.txt: a Vivado-generated description of how-to-repeat the
#          the basic steps of a run.  Note that runme.bat/sh needs
#          to be invoked for Vivado to track run status.
# Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
#

vivado -log platforms_mem.vds -m64 -product Vivado -mode batch -messageDb vivado.pb -notrace -source platforms_mem.tcl
