`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/27/2021 11:26:49 AM
// Design Name: 
// Module Name: color_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module color_tb;
    
    reg clk;
    wire [3:0] r, g, b;
    reg [3:0] r_input, g_input, b_input;
    wire hsync, vsync;
    
    vga_out UUT (.clk(clk), .pix_r(r), .pix_g(g), .pix_b(b), .hsync(hsync), .vsync(vsync), .red_v(r_input), .green_v(g_input), .blue_v(b_input));
    
    initial
    begin
        clk = 1'b1;
        
        #5 r_input = 4'b1111; g_input = 4'b0; b_input = 4'b0;
    end
    
    always #5
        clk = ~clk;
        
endmodule
