`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/27/2021 06:22:04 PM
// Design Name: 
// Module Name: pixelcount_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pixelcount_tb;

    reg clk, rst;
    reg [3:0] r_input, g_input, b_input;
    wire [3:0] r, g, b;
    wire hsync, vsync;
    wire [10:0] curr_x;
    wire [9:0] curr_y;
    
    vga_out UUT1 (.clk(clk),
                 .rst(rst),
                 .pix_r(r), 
                 .pix_g(g), 
                 .pix_b(b), 
                 .hsync(hsync), 
                 .vsync(vsync), 
                 .red_v(r_input), 
                 .green_v(g_input), 
                 .blue_v(b_input),
                 .curr_x(curr_x),
                 .curr_y(curr_y));
    
    initial
    begin
        clk = 1'b1;
        rst = 1'b1;
        
        #5 rst = 1'b0; r_input = 4'b1111; g_input = 4'b1010; b_input = 4'b0;
    end
    
    always #5
        clk = ~clk;
endmodule
